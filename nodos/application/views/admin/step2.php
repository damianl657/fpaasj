  
<script type="text/javascript">
	//FUNCION PARA VACIAR LA TABLA HTML NIVELES
	function vaciar_niveles(){				
		//var rowCount = $('#ListNiveles > tr').length;
		/*var rowCount = document.getElementById("ListNiveles").getElementsByTagName("tr").length;
		for(var i=0; i<(rowCount - 1) ; i++)
			$("#ListNiveles tr").remove();*/
        $(".arrastrable").remove(); 
	}			
	function vaciar_niveles_cargados_step2(){
		$(".droppable").parent('td').parent('tr').remove();
	}
	
    
    //FUNCION PARA TRAER  MOSTRAR TODOS LOS NIVELES
    function mostrar_niveles_step2(){   

			var url = urlApi+"/nivel/niveles_cargados";     
            $.ajax({
                    url: url,
                    type: 'GET',
                    headers: {                    
                        'APIKEY' : passApiKey,
                        'userid' : idUser
                    },
                success: function(data){
                    vaciar_niveles_cargados_step2();
                    var datos = jQuery.parseJSON( data );
                    for(var i=0; i <  (datos.length) ; i++)
                    {
                        text="<tr><td>"+
                              "<div class='droppable ui-widget-header' name='suelta' id='niveles_"+datos[i]['idnodonivel']+"'>"+datos[i]['nombre']+
                                "<input type='hidden' name='nivel_step2' value='"+datos[i]['idnodonivel']+"'/>"+ 
                              "</div>"+
                             "</td></tr>";
                        $('#ListNiveles tbody tr:last').after(text);
                        $(".droppable").data("numsoltar", 0);
                        
                        //defino elementos donde se puede Soltar
                        $( ".droppable" ).droppable({
                            activeClass: 'ui-state-hover',
                            hoverClass: 'ui-state-active', 
                           
                            drop: function( event, ui ) {
                                    /*if (!ui.draggable.data("droppable")){
                                        ui.draggable.data("droppable", true);
                                         var div = ui.draggable[0];
                                         
                                         alert(ui.draggable.attr('id') + ' was dropped from ' + ui.draggable.parent().attr('id'));
                                    }*/
                                    var id_especialidad = ui.draggable[0].id;
                                    var especialidad = id_especialidad.split('especialidad_'); // divide la cadena en 2 para obtener la especialidad-
                                   // alert(especialidad[1] + ' was dropped from ' + ui.draggable.parent().attr('id'));
                                    $( this ).addClass( "ui-state-highlight");
                                    // Move the dragged element into its new container
                                    ui.draggable.attr('style','position:relative');
                                    $(this).append(ui.draggable);
                                    return false;          
                            },
                            out: function( event, ui ) {
                                    if (ui.draggable.data("droppable")){
                                        ui.draggable.data("droppable", false);
                                        var elem = $(this);
                                        elem.data("numsoltar", elem.data("numsoltar") - 1);
                                        elem.html("Llevo " + elem.data("numsoltar") + " elementos soltados");
                                    }
                            }
                        });
                    } // fin for  
                }, // Fin succes
                error: function(response){
                    console.log(response);
                }
            }); 
    }
	//----------------------------------------------------------------------
	function listar_especialidades_cargadas(){
		
		var url = urlApi+"/especialidad/especialidades_cargados";     
        $.ajax({
                url: url,
                type: 'GET',
                headers: {                    
                    'APIKEY' : passApiKey,
                    'userid' : idUser
                },
               data: {idcolegio: idcolegio},
               
               beforeSend: function() {
                      $('#ListNiveles').after("<img id='loadingEsp' src='<?=base_url('assets/images/loading.gif');?>' height='50' width='50' />");
                },        
                success: function(data){
                    //console.log(data);
                    $("#loadingEsp").remove();
                     
				    //vaciar_niveles_cargados_step2();  // descomentar
					var datos = jQuery.parseJSON( data );
					
					$('#niveles').after(datos);
					//console.log($('#ListNiveles').html());
					for(var i=0; i < (datos.length) ; i++)
					{  //defino elementos donde se puede Soltar
						$( ".sueltaP2" ).droppable({
							activeClass: 'ui-state-hover',
							hoverClass: 'ui-state-active', 
							 
							drop: function( event, ui ) {
				
								var id_especialidad = ui.draggable[0].id;
								var especialidad = id_especialidad.split('especialidad_'); // divide la cadena en 2 para obtener la especialidad-
                                
                                var li = "<li><span></span></li>";                          
                                $(this).append(li);

                                var li = $(this).find('li:last span');
                                ui.draggable.appendTo(li).css({
                                    top: '0px',
                                    left: '0px',
                                    width: '90%',                                    
                                    float: 'left'                                   
                                });

								//$( this ).addClass( "ui-state-highlight");
								// Move the dragged element into its new container
								ui.draggable.attr('style','position:relative; border: inherit');
								//$(this).append(ui.draggable);
								
								return false;          
							},
							
							out: function( event, ui ) {
									if (ui.draggable.data("droppable")){
										ui.draggable.data("droppable", false);
										var elem = $(this);
										elem.data("numsoltar", elem.data("numsoltar") - 1);
										elem.html("Llevo " + elem.data("numsoltar") + " elementos soltados");
									}
							}
						});					
					}
					
					$('.closeStep2').on('click', function() {
						    var especialidad = $(this).parent('div').attr('id');
							var remover_especialidad = $(this).parent('div');
							
							var nodoespecialidad = $(this).parent('div').find('input');
							var id_nodoespecialidad = nodoespecialidad.val();
							//alert(nodoespecialidad.val());
			
							var id_especialidad = especialidad.split('especialidad_'); // divide la cadena en 2 para obtener la especialidad-
                          
    						bootbox.confirm(' Desea eliminar de forma permanente la especialidad?', function(result) {
								if(result){
										BajaEspecialidadCargada(id_especialidad[1]);
										//BajaEspecialidadCargada(id_nodoespecialidad);
										remover_especialidad.remove();		
								} 		               
					        				  		
	                        })
					})
					 				 
				}, // Fin succes
                error: function(response){
					
                   console.log(response);
                }
        }); 
	}
				
	//----------------------------------------------------------------------
	function listar_especialidades_cargadas_anteriror(){
	 
		var url = urlApi+"/especialidad/especialidades_cargados";     
        $.ajax({
                url: url,
                type: 'GET',
                headers: {                    
                    'APIKEY' : passApiKey,
                    'userid' : idUser
                },
                success: function(data){
				//console.log(data);
                //vaciar_niveles_cargados_step2();
                var datos = jQuery.parseJSON( data );
                for(var i=0; i <  (datos.length) ; i++)
                {
                    text="<tr><td>"+
                            "<div class='droppable ui-widget-header' name='suelta' id='niveles_"+datos[i]['nodonivel_id']+"'>"+datos[i]['nombre_nivel']+
                            "<input type='hidden' name='nivel_step2' value='"+datos[i]['nodonivel_id']+"'/>"+ 
                            "</div>"+
                            "</td></tr>";
                    $('#ListNiveles tbody tr:last').after(text);
                    $(".droppable").data("numsoltar", 0);
           
                    //defino elementos donde se puede Soltar
                    $( ".droppable" ).droppable({
                        activeClass: 'ui-state-hover',
                        hoverClass: 'ui-state-active', 
                         
                        drop: function( event, ui ) {
			
							var id_especialidad = ui.draggable[0].id;
							var especialidad = id_especialidad.split('especialidad_'); // divide la cadena en 2 para obtener la especialidad-
							
							$( this ).addClass( "ui-state-highlight");
							// Move the dragged element into its new container
							ui.draggable.attr('style','position:relative');
							$(this).append(ui.draggable);
							
							return false;          
                        },
                        
						out: function( event, ui ) {
                                    if (ui.draggable.data("droppable")){
                                        ui.draggable.data("droppable", false);
                                        var elem = $(this);
                                       elem.data("numsoltar", elem.data("numsoltar") - 1);
                                       elem.html("Llevo " + elem.data("numsoltar") + " elementos soltados");
                                    }
                            }
                        });
                } // fin for  
                }, // Fin succes
                error: function(response){
                    console.log(response);
                }
            }); 
		
	}
	//----------------------------------------------------------
	//-- Eliminar la especialidad de la tabla NodoEspecialidad.
	//----------------------------------------------------------
	
	function BajaEspecialidadCargada(id_esp){
		    var url = urlApi+"/especialidad/baja_especialidad";

			$.ajax({
	                url: url,
	                type: 'POST',
	                headers: {				      
				        'APIKEY' : passApiKey,
				        'userid' : idUser
				    },
				    data:{id_esp:id_esp},
	                success: function(data){ //console.log(data);
	                	
		                if(data['status']==false)
                        {
                          if(data['message'] == 'NO LOGUIN')      
                             location.href = "<?php echo base_url('login'); ?>";
                          else
                          {                                         
                            $('.top-right').notify({
                                message: { text: data['message'] },
                                fadeOut: { enabled: true, delay: 5000 },
                                type: 'danger2'
                            }).show(); 
                          }
                        }else{
                             $('.top-right').notify({
                                message: { text: data['message'] },
                                fadeOut: { enabled: true, delay: 5000 },
                                type: 'success2'
                            }).show(); 
                        }     
					},
                error: function(response){
                    //console.log(response);
                    
                }
					 	
	        });	
	 }
	
	//---------------------------------------------------------------------	
	//------------  ESPECIALIDADES ----------------------------------------
    //---------------------------------------------------------------------
    //FUNCION PARA VACIAR LA TABLA HTML ESPECIALIDADES
    function vaciar_especialidad(){              
        //var rowCount = $('#ListNiveles > tr').length;
       /* var rowCount = document.getElementById("ListEspecialidad").getElementsByTagName("tr").length;
        for(var i=0; i<(rowCount - 1) ; i++)
            $("#ListEspecialidad tr").remove();*/
		$(".draggable").parent('td').parent('tr').remove();
    }   
   
    //FUNCION PARA TRAER  MOSTRAR TODOS LAS ESPECIALIDADES
    function mostrar_especialidad_step2(){
        vaciar_especialidad();  //.. revisar si va.
        var url = urlApi+"/especialidad/especialidades";      
        $.ajax({
                url: url,
                type: 'GET',
                headers: {
                    'APIKEY' : passApiKey,
					'userid' : idUser
                },
				beforeSend: function() {
                      $('#especialidadesP2').after("<img id='loadingEsp' src='<?=base_url('assets/images/loading.gif');?>' height='50' width='50' />");
                },
                success: function(data){
                    					
                    //console.log(data);
					//$('#especialidadesP2').remove();
					$('#loadingEsp').remove();
					
					if(data['status']==false)
                   	{ 	
                   		if(data['message'] == 'NO LOGUIN')		                                  	
		                    location.href = "<?php echo base_url('login'); ?>";
                   	
					}else{
						var datos = jQuery.parseJSON(data);
						for(var i = 0; i <  (datos.length) ; i++){
							/* text="<tr><td>"+    
							"<div id='especialidad_"+datos[i]['id']+"' class='arrastrableP2'  name='arrastrable'>"+datos[i]['nombre']+
							"<input type='hidden' name='especialidad_step2' value='"+datos[i]['id']+"'/> "+ 
							"</div>"+   
							"</td></tr>"; 
							$('#ListEspecialidad tbody tr:last').after(text); 

                            //------------ loa anterior funcionaba bien                                
								 
                            /*text="<tr><td>"+    
                                   "<div id='especialidad_"+data['id']+"' class='draggable' class='ui-widget-content' name='arrastrable'>"+newn+
                                    "<input type='hidden' name='especialidad_step2' value='"+data['id']+"'/>"+ 
                                   "</div>"+   
                             "</td></tr>"; 
                        
                            $('#ListEspecialidad tbody tr:last').after(text);*/
								//volver
							text="<li  class='soltableN'  id='especialidad_"+i+"'>"+
						 			"<div id='especialidad_"+datos[i]['id']+"' name='arrastrable' class='arrastrableP2'>"+datos[i]['nombre']+
									"<input type='hidden' name='especialidad_step2' value='"+datos[i]['id']+"'/> "+ 
								   "</div>"+
						 		 "</li>";
					
						    $('#ListEspecialidadesP2 li:last').after(text);
						}  // fin for 
						
						//------ Function para Drogable ---------------------------
						//------ Defino los elementos que se pueden arrastrar -----
							 $('.arrastrableP2').draggable({ 
							//$('#especialidad_'+ datos[i]['id']).draggable({
								
								start:  function(ui) { $(this).data("soltado", false); 
							    		    $(this).addClass('ui-state-hover'); 
							    		},
							    stop:   function(ui) { $(this).data("soltado", true );    
							    			$(this).removeClass('ui-state-hover');	   
							    		}		
								  					
							}).each(function(){
								    var top = $(this).position().top;
									
									var left = $(this).position().left;
									
									$(this).data('orgTop', top);
									$(this).data('orgLeft', left);
							});
							
							//$('#especialidad_'+ datos[i]['id']).data("soltado", false); 
					}  
                },
                error: function(response){
                    console.log(response);
                }
        }); 
    }
   //---FUNCTION PARA INSETAR UNA NUEVA ESPECIALIDAD ----
    function add_especialidad_step2(newn){
		
        var urlRequest = urlApi+"/especialidad/especialidad";                                                      
        var parametros = {
            'nombre': newn     
        };
        var stringJson = JSON.stringify(parametros);    
        $.ajax({
                url: urlRequest,
                type: 'POST',
                headers: {      
                    'APIKEY' : passApiKey,
					'userid' : idUser
                },
                data: {data: stringJson},
				beforeSend: function() {
                  	$('#add_especialidad_step2').after("<img id='loadingEspStep2' src='<?=base_url('assets/images/loading.gif');?>' height='30' width='30' />");
                },
                success: function(data){
                    //console.log(data);
					$('#loadingEspStep2').remove(); // eliminar el reloj de loading..
					
                    if(data['status']== false){
                        //-- Mostrar el cartel de error o de Warning.
                        if(data['message'] == 'NO LOGUIN')                                          
                                location.href = "<?php echo base_url('login'); ?>";
                        
                        if(data['message'] == 'existe')                                         
                        { 
                            $('.top-right').notify({
                                message: { text: 'Atención: La Especialidad ya Existe' },
                                fadeOut: { enabled: true, delay: 5000 },
                                type: 'warning2'
                            }).show();      
                        }
                    }
                    else{
                        //--- Mostrar cartel Satisfactorio
                        $('.top-right').notify({
                                    message: { text: 'Especialidad Guardada' },
                                    fadeOut: { enabled: true, delay: 5000 },

                                    type: 'success2'
                                   
                        }).show();

                         /*text="<tr><td>"+    
                                   "<div id='especialidad_"+data['id']+"' class='draggable' class='ui-widget-content' name='arrastrable'>"+newn+
                                    "<input type='hidden' name='especialidad_step2' value='"+data['id']+"'/>"+ 
                                   "</div>"+   
                             "</td></tr>"; 
                        
                        $('#ListEspecialidad tbody tr:last').after(text);*/
						
						/*text="<li id='sueltaN_"+data['id']+"' class='soltableN'> "+
							 	"<div id='especialidad_"+data['id']+"' class='arrastrableP2' name='arrastrable'>"+newn+
                                "<input type='hidden' name='especialidad_step2' value='"+data['id']+"'/>"+
                                "</div>"+
							"</li>";*/

                        text="<li id='sueltaN_"+data['id']+"' class='soltableN'> "+
                                "<div id='especialidad_"+data['id']+"' class='arrastrableP2' name='arrastrable'>"+newn+
                                "<input type='hidden' name='especialidad_step2' value='"+data['id']+"'/>"+
                                "</div>"+
                            "</li>";

						// $('#especialidad_'+data['id']).data("idDivorigen", 'sueltaN_'+data['id']			   
							    
					    $('#ListEspecialidadesP2 li:last').after(text);
						
                        //------ Function para Drogable ---------------------------
                        //------ Defino los elementos que se pueden arrastrar -----
                       
					   // $('#especialidad_'+ data['id']).draggable(); 
                       // $('#especialidad_'+ data['id']).data("soltado", false); 
					   
                        $('#sueltaN_'+ data['id']).draggable({ 
							//containment: '#well',	
							start: function(ui) { $('#nivel_'+data['id']).data("soltado", false);
							    	   $(this).addClass('ui-state-hover'); 
							    	},
							stop: function(ui) { $('#nivel_'+data['id']).data("soltado", true ); 
							    	$(this).removeClass('ui-state-hover');											   
							      },
							     //drag: function(ui) { console.log( $(ui.target).parent('div') ); },
								        					    
						}).each(function() {
							    var top = $(this).position().top;
								var left = $(this).position().left;
								$(this).data('orgTop', top);
								$(this).data('orgLeft', left);
					    });						
                    } // FIN ELSE
					$('#loadingEspStep2').remove();	
                },
                error: function(response){
                    //console.log(response);
                    
                }
            });
    }     
    
	//-----------------------------------------------------------
	function guardar_step2(parametros){
        //--- Guarda en la tabla nodoEspecialidad.
        //console.log(parametros);
            var urlRequest = urlApi+"/especialidad/insertespecialidades";
            var stringJson = JSON.stringify(parametros);
			
            $.ajax({
                url: urlRequest,
                type: 'POST',
                headers: {      
                    'APIKEY' : passApiKey,
					'userid' : idUser
                },
                data: {data: stringJson},
                success: function(data){
                    
                    //console.log(data);
                    if(data['status']==true)
                        // alert(data['message']);
                       $('.top-right').notify({
                                message: { text: 'El Nivel ya Existe' },
                                fadeOut: { enabled: true, delay: 5000 },
                                type: 'success2'
                        }).show();  
                    else{
                        if(data['message'] == 'NO LOGUIN')                                          
                             location.href = "<?php echo base_url('login'); ?>";
                        else                                    
                            $('.top-right').notify({
                                message: { text: data['message'] },
                                fadeOut: { enabled: true, delay: 5000 },
                                type: 'danger2'
                            }).show();                                  
                    }
                },
                error: function(response){
                   // console.log(response);
                    
                }
            });
	} // fin function Guardar_step2

    //---------------------------------------------------------------------
    $(document).ready(function(){
        //$('#paso2').css('display','block'); //esto despues hay que correrlo
		
        $('#addnivel').click(function(){  // Button Agregar Niveles
           $('#newnivel').css("display",'block'); 
           $('#newnivel').focus();
        });
        //-- MOSTRAR Especialidades     
        
        $('#add_especialidad_step2').click(function(){  // Button Agregar Especialidad
            $('#newespecialidad').css("display",'block'); 
            $('#newespecialidad').focus();
        });
        //---------------------------------------------
        $('#newespecialidad').keypress(function(e) {
            if(e.which == 13) {
               var newn = $('#newespecialidad').val();

			 if(newn != ''){                                                           
                add_especialidad_step2(newn);  // invoca function para agregar especialiad
                $('#newespecialidad').css("display",'none');
                $('#newespecialidad').val('');
             }
            else alert('ingrese Especialidad');
            }
        });
        //----  BOTON  Next -------------------------------------------------------
        /*PASO 1 - ENVIA y GUARDA la asignacion de Especialidades con Niveles */   		
        //-------------------------------------------------------------------------
        $('#next2').click(function(e){//volver
			var parametros = Array(); 
            console.log("entro 1");
            $(".sueltaP2").each(function (index) { // Recorre la tabla de Niveles que tiene asignada las Especialidades-
                var hiddens_nivel = $(this).find('[name="nivel_step2"]');
                var num_nivel = $(hiddens_nivel).val();
                console.log("entro 2 "+num_nivel);
                $(this).find("li").each(function (index2) {
                    console.log("entro 3 "+index2);
                    //switch (index2) {
                        //case 0:
                            var arre_especialdad = Array();  // arreglo de especialidades
                            var hiddens = $(this).find('[name="arrastrable"]').find('input');
                            for(var i=0; i <  (hiddens.length) ; i++){   
                               arre_especialdad.push( $(hiddens[i]).val() );
                               console.log("entro 4 "+i+" "+$(hiddens[i]).val());
                            }
                            var fila = {
                                  //'nodocolegio_id': colegio,
                                  'niveles_id': num_nivel,
                                  'especialidades_id': arre_especialdad
                            };                           
                            parametros.push( fila );
                            //break;
                    //} // fin switch
                  
                });
            }); // fin each Recorrido de la tabla de Niveles
            
            //---- Nota: Falta agregar validacion que si no tiene nada cargado que no llame a guardar_step2
			guardar_step2( parametros);
            listar_nivelesEspecialidadesAnios();
            $('#progress_text').html('50% Complete');
            $('#progress').css('width','169px');
                 
            $('#paso1').css('display','none');
            $('#paso2').css('display','none');
            $('#paso3').css('display','block');
            $('#paso4').css('display','none');                               
			
        }); // fin function next2
        $('#atras2').click(function(){               
            mostrar_nivelesP1();
            listar_niveles_cargados();  
            //update progress bar
            $('#progress_text').html('0% Complete');
            $('#progress').css('width','0');
                                               
            $('#paso1').css('display','block');
            $('#paso2').css('display','none');
            $('#paso3').css('display','none');
            $('#paso4').css('display','none');            
        });
        //------- FIN FUNCTION API -------//
        //------ function para Drogable ----
        //--------------------------------------
    });
</script>
<!------------------------------------------------------------------>
<!------------------ COMIENZA EL HTML del Paso #2 ------------------>
<!------------------------------------------------------------------>
<div class = "paso2" id="paso2" style='display:none'>
	
	<!-- Indicadores de Pasos -->
		<div class="container-fluid nopadding">
			<div class="row nopadding center indicadores-pasos">
				<div id="paso-nivel" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding ok"> <!-- Modificar el Ok traer variable paso-->
					Paso 1: Niveles <span><i class="fa fa-check-circle-o"></i></span>
				</div>
				<div id="paso-especialidad" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding"> <!-- Modificar el Ok-->
					Paso 2: Especialidades <span><i class="fa fa-check-circle-o"></i></span>
				</div>
				<div id="paso-ano" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding">
					Paso 3: A&ntilde;os <span><i class="fa fa-check-circle-o"></i></span>
				</div>
				<div id="paso-division" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding">
					Paso 4: Divisiones <span><i class="fa fa-check-circle-o"></i></span>
				</div>
			</div>
		</div>
        
		<div class="container-fluid">
			<!-- Descripción Paso -->
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center vspace2">
					<h3>Paso 2</h3>
					<h6>Asigne las especialidades seg&uacute;n nivel</h6>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center vspace">
					<p>Asigne a cada nivel cargado la especialidad correspondiente.<br>Arrastre las especialidades precargadas en la columna izquierda hacia los niveles de la columna derecha.</p>
				</div>
			</div>
			
			<!-- Funcionalidad -->
			<div style="overflow: hidden; height:auto">
			    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="box-config">
					    <h3>Especialidades Disponibles</h3>
					   <div class="inner-box-config"  id='especialidadesP2'>
                        	<ul id="ListEspecialidadesP2" >
                                <li></li>
							 <!-- AQUI AGREGO LOS LI CON JQUERY -->
					    	</ul>
                        </div> 
				
					    <a id='add_especialidad_step2' class="btn-principal-azul" href="#">Agregar Especialidad</a>
					
						<!--<input id='newespecialidad' placeholder=' Ingrese la especialidad | Presione Enter' style='display:none'/> -->
					    <input id='newespecialidad' class='form-control input-sm' placeholder='Ingrese la Especialidad | Presione Enter' style='display:none'/>     
					</div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="box-config">
						<h3>Niveles de Instituci&oacute;n</h3>                                                    
                            <div class="inner-box-config niveles" id='niveles'>
                                <!-- AQUI AGREGO LOS LI POR CADA NIVEL CARGADO -->
                            </div>
				    </div>
			    </div>

                <!-- PLANTLLA -->
                <!--<div class="inner-box-config"  name="suelta" id='niveles'>
                    <div class="box-drag">
                        <h4>Educaci&oacute;n Primaria</h4>
                        <div class="inner-box-drag">
                            <ul id='ListNiveles'>
                                <li></li>
                            </ul>
                        </div>                                  
                    </div>
                    <div class="box-drag">
                        <h4>Educaci&oacute;n Secundaria</h4>
                        <div class="inner-box-drag">
                            <ul>
                                <li><span>Econom&iacute;a</span> <span class="delete"><i class="fa fa-trash-o"></i></span></li>
                                <li><span>Naturales</span> <span class="delete"><i class="fa fa-trash-o"></i></span></li>
                                <li><span>Qu&iacute;mica</span> <span class="delete"><i class="fa fa-trash-o"></i></span></li>
                            </ul>
                        </div>                                  
                    </div>

                </div>-->                
				
			</div>
	
        
    
     <!-- //Bottonera previa al diseño //-->
    <!--<button  class="btn btn-warning " id='atras2' class="next" style='float:left; margin-left:5%'>Atras</button>
    <button  class="btn btn-danger " id='next2' class="next" style='float:right; margin-right:5%'>Siguiente</button> -->
	
    <!-- Anterior/Siguiente -->
			<div class="row vspace4">
				<div id='atras2' class="col-lg-offset-3 col-lg-3 col-md-offset-3 col-md-3 col-sm-offset-3 col-sm-3 col-xs-6">
					<a class="btn-principal-azul" href="#">Anterior Paso</a>
				</div>
				<div id='next2' class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a class="btn-principal-azul" href="#">Siguiente Paso</a>
				</div>				
			</div> 
    </div> <!-- Fin Div Conteiner-->
	<br>
  
</div>  <!-- Fin Div final -->
 
<style type="text/css">
    
	.sueltaP2{
        /*padding: 10px;
        margin: 10px;
        border: 1px solid #ddd;
        width: 200px;
        height: auto;
        float: right;
        text-align: center;*/

        min-height: 50px;
        
    }
	
	.soltableN{
        /*padding: 10px;
        border: 1px solid #ddd;*/
        width: 80%;
        height: auto;

        /*float: right;*/
        text-align: center;
        margin: 10px;
    }
 
  
 	.arrastrableP2{
 		height: auto;
 		text-align: center;
 		padding-top: 2%;
 		border: 1.5px solid #CCC;
 		border-radius: 2px;
 		cursor: move;
 		z-index: 100;
		/*background-color: rgb(151, 219, 55);*/
	
        width: 90%; 
 	}
    
	.glyphicon-trash
	{
		cursor:pointer;
	}
	
</style>