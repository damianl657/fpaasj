<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';


    class Nivel extends REST_Controller {

    function __construct()
    {
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        
        parent::__construct();
        $this->load->model("modelo_nivel");
        $this->load->model("modelo_usuario");
        $this->load->model("Modelo_permisos");
        // Configurar limites para cada uno de los metodos, no solo controlador
        // La tabla limits tiene que estar creada y la opcion limits TRUE en application/config/rest.php
        
        //$this->methods['tutores_get']['limit'] = 1; // 500 peticiones por hora por usuario/key
        //$this->methods['user_post']['limit'] = 100; // 100 peticiones por hora por usuario/key
        //$this->methods['user_delete']['limit'] = 50; // 50 peticiones por hora por usuario/key
    }

    public function niveles_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $result = $this->modelo_nivel->obtener_niveles();
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
                $data = json_encode($result);
                $this->response($data, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron niveles'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }


    public function nivel_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
                $data = json_decode($_POST['data']) ;

                $result0 = $this->modelo_nivel->obtener_nivel($data->nombre);
                if (!$result0) // para NO insertar filas repetidas
                {
                    $result = $this->modelo_nivel->insertar_nivel($data);          
                    if ($result)
                    {
                        $ultimoId=$this->db->insert_id(); 
                        $this->response([
                                'status' => TRUE,
                                'message' => 'Nivel dado de Alta',
                                'id' => $ultimoId 
                            ], REST_Controller::HTTP_OK);  
                    }
                    else
                    {
                        $this->response([
                            'status' => FALSE,
                            'message' => 'Error al dar de alta'
                        ]); // NOT_FOUND (404) being the HTTP response code
                    }
                }
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'existe'
                    ]); // NOT_FOUND (404) being the HTTP response code
                }
        }
    }


    public function insertniveles_post()
    {
        $userid = $this->utilidades->verifica_userid();
    
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $data = json_decode($_POST['data']) ;
            $colegioId = $_POST['idcolegio'];
            $idusergrupo = $_POST['idusergrupo'];

            $cicloa_id = $this->utilidades->insertar_planestudio();  //tranquilo!... esto verifica y luego inserta si es necesario. actualiza cicloa tambien.
         
           
            $yearA=date('Y');
            foreach ($data as $key ) { 
                $key->cicloa_id = $cicloa_id; //agrego un campo al arreglo stdclass
                $key->nodocolegio_id = $colegioId;
                
                $res = $this->modelo_nivel->get_nodonivel($colegioId, $key->niveles_id, $cicloa_id); 
                //var_dump($res);
                if (!$res) //si no existe inserto
                    $result = $this->modelo_nivel->insertar_nodonivel($key); 
                else{ //existe
                    foreach ($res as $row ) 
                        if ($row->estado==0) //si existe pero fue dada de baja, vuelvo a habilitar
                           $this->modelo_nivel->update_estado_nodonivel($row->id,1); 
                    }
            }
                
            $result2 = $this->modelo_usuario->update_pasos($idusergrupo,1); 

            if ($result2)
            {
                $this->response([
                        'status' => TRUE,
                        'message' => 'Paso 1 Guardado', 
                    ], REST_Controller::HTTP_OK);  
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Error al Guardar Paso 1'
                ]); // NOT_FOUND (404) being the HTTP response code
            }   
        }    
    }


    /*public function niveles_cargados_get() //NODONIVEL
    {       
        //echo "idEncriptado: ".$this->utilidades->encriptar('1');

        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {

            $idcolegio = $_GET['idcolegio'];  

            $idcicloa = $this->utilidades->get_cicloa();
            
            $result = $this->modelo_nivel->obtener_niveles_cargados($idcolegio, $idcicloa);

            // Si existe mas de un resultado, lo mando          
            if ($result)// Si existe mas de un resultado, lo mando
            {
                $data = json_encode($result);
                $this->response($data, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron niveles cargados'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }*/
      
    

     public function baja_nivel_post()
    {
        $userid = $this->utilidades->verifica_userid();
    
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $idnodonivel = $_POST['idnodonivel'] ;

            $result2 = $this->modelo_nivel->update_estado_nodonivel($idnodonivel, 0); 
                      
            if ($result2)
            {
                $this->response([
                        'status' => TRUE,
                        'message' => 'Nivel dado de baja', 
                    ], REST_Controller::HTTP_OK);  
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Error al dar de Baja'
                ]); // NOT_FOUND (404) being the HTTP response code
            }   
        }    
    }

    public function obtener_NivEspcargadas_get() //NODONIVEL
    {  
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {            
            $idcolegio = $_GET['idcolegio'];  //aca debo traerlo a partir del id de usuari
            $nombregrupo = $_GET['nombregrupo']; 
            
            //$anioA = date('Y');
            $anioA = $this->utilidades->obtener_planestudioaXcolegio( $idcolegio );  
            //$anioA = '2016';
            $planest_Id = $this->utilidades->get_planestudio( $anioA ); 

            //$arre=array();
            //$arre2=array(); 

            //print_r($anioA); print_r('_'.$planest_Id);
            //Administrativo Gral
            /*if( ($nombregrupo=='Institucion') || ($nombregrupo=='Directivo') || ($nombregrupo=='Nodos')|| ($nombregrupo=='Administrativo') || ($nombregrupo=='Gabinete') || ($nombregrupo=='Administrativo Gral') || ($nombregrupo == 'Secretaria Inicial') || ($nombregrupo == 'Secretaria Primaria'))              
            {   //todos los niveles del colegio
                /*$result = $this->modelo_nivel->obtener_NivConEsp($idcolegio,  $anioA);  
                $result2 = $this->modelo_nivel->obtener_NivSinEsp($idcolegio,  $anioA);         
                $resultado = array_merge($result, $result2);  */

                 
               /* if($planest_Id)
                    $resultado = $this->modelo_nivel->obtener_niveles_cargados($idcolegio,  $planest_Id); 
            }*/
            $div_todas = $this->Modelo_permisos->get_permiso_div_todas($idcolegio,$nombregrupo);
            if($div_todas == true)
            {
                if($planest_Id)
                    $resultado = $this->modelo_nivel->obtener_niveles_cargados($idcolegio,  $planest_Id);
            }
            else if($nombregrupo=='Docente')
                 {  
                    $this->load->model("modelo_division");
                    $diviDoce = $this->modelo_division->divisionesXdocentes($idcolegio,  $planest_Id, $userid);
                    if( count($diviDoce)>0)
                    {
                        foreach ($diviDoce as $key) {
                            $arre[] = $key->divisiones_id;
                        }
                        $divisiones = join(',',$arre);
                        
                        $this->load->model("modelo_anio");
                        $anioDivi = $this->modelo_anio->aniosXdivisiones($divisiones);
                        foreach ($anioDivi as $key) {
                            $arre2[] = $key->anios_id;
                        }
                        $anios = join(',',$arre2);

                        $resultado = $this->modelo_nivel->nivelesXanios($idcolegio, $anios,$planest_Id);
                        /*$result2 = $this->modelo_nivel->nivelesSEXanios($idcolegio, $anios);
                        if( ($result1)&&($result2) )
                            $resultado = array_merge($result2, $result1);
                        else  if($result1)
                                $resultado = $result1;
                              else 
                                if($result2)
                                    $resultado = $result2; */
                    }
                }
                 else if($nombregrupo=='Preceptor')
                     {
                        $this->load->model("modelo_division");
                        $diviPrece = $this->modelo_division->divisionesXpreceptores($idcolegio,  $planest_Id, $userid);
                        $query_preceptores = $this->db->last_query();
                        if( count($diviPrece)>0)
                        {
                            foreach ($diviPrece as $key) {
                                $arre[] = $key->division_id;
                            }
                            $divisiones = join(',',$arre);
                   
                            
                            $this->load->model("modelo_anio");
                            $anioDivi = $this->modelo_anio->aniosXdivisiones($divisiones);
                            foreach ($anioDivi as $key) {
                                $arre2[] = $key->anios_id;
                            }
                            $anios = join(',',$arre2);

                            
                            $resultado = $this->modelo_nivel->nivelesXanios($idcolegio, $anios,$planest_Id);
                            /*$result2 = $this->modelo_nivel->nivelesSEXanios($idcolegio, $anios);
                            
                            if( ($result1)&&($result2) )
                                $resultado = array_merge($result2, $result1);
                            else  if($result1)
                                    $resultado = $result1;
                                  else 
                                    if($result2)
                                        $resultado = $result2; */
                        }else print_r("ELSE");
                     }


            if (isset($resultado))
            {
                $data = json_encode($resultado); 
                $this->response($data, REST_Controller::HTTP_OK); 
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'no se encontraron niveles',
                    //'query_preceptores' => $diviPrece,
                    //'query_div_Anio' => $anioDivi
                ]); // NOT_FOUND (404) being the HTTP response code
            } 
        }
    }
    public function get_all_niveles_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
           // print_r("aaa");
            $result = $this->modelo_nivel->get_all_niveles();
            if($result->num_rows() > 0)
            {
               
                //$result = utf8_decode($result);
                //print_r($result);
                //echo "<br><br>";
               
                $data = array
                    ( 
                        'status' => 1,
                        'message' => 'ok',
                        'cant_niveles' => $result->num_rows(),
                        'niveles' => $result->result(),
                    );
                $data = json_encode($data);
                $this->response($data, REST_Controller::HTTP_OK); 
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => 1,
                    'message' => 'ok',
                    'cant_niveles' => $result->num_rows(),
                    'niveles' => $result->result(),
                ], REST_Controller::HTTP_OK); 
            }
        }
    }




}
