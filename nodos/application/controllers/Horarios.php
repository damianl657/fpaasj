<?php //session_start();
 
class Horarios extends CI_Controller {
      
	function __construct(){
		parent::__construct();
			//$this->output->enable_profiler(TRUE);
		if(!$this->session->userdata("logged_in")){
               redirect("login");
         }
	}
	

	public function get_divisiones_x_colegio($urlApi, $passApiKey, $idUser, $idcolegio, $nombregrupo)  
    {  
    	$url = $urlApi."/division/obtener_divisiones_x_colegio/idcolegio/".$idcolegio."/nombregrupo/".$nombregrupo;
		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser") ,
			CURLOPT_SSL_VERIFYPEER => false,
           
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		
		//print_r($response); die();
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}

	public function get_horarios($urlApi, $passApiKey, $idUser, $idcolegio)  
    {  
    	$url = $urlApi."/horario/horariosXcolegio/idcolegio/".$idcolegio;  

		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser") ,
			CURLOPT_SSL_VERIFYPEER => false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		
		//print_r($response); die();
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien


	}

	public function get_dias($urlApi, $passApiKey, $idUser)  
    {  
    	$url = $urlApi."/horario/dias";  

		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser") ,
			CURLOPT_SSL_VERIFYPEER => false,
           
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
	
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}


    public function index(){         
        
       // $this->load->view('include/vista_inc_header');
		
		$urlApi = $this->variables->get_urlapi(); //creamos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		$data['urlApi'] = $urlApi;
		$data['passApiKey'] = $passApiKey;
		$data['idusuario'] = $this->session->userdata('idusuario');
		$data['idcolegio'] = $this->session->userdata('idcolegio');
		$data['idgrupo'] = $this->session->userdata('idgrupo');
		$data['nombregrupo'] = $this->session->userdata('nombregrupo');
		$data['ordengrupo'] = $this->session->userdata('ordengrupo');
		

		//var_dump($this->session->userdata);
		
		$data['opcion_menu'] = 'horarios';
		$band = 0;
		$controlador = "Horarios";
		foreach ($this->session->userdata('menu') as $menu) //validar permiso de controlador y metodo
        {
        	if(($menu->controlador == $controlador) && ($menu->metodo == 'index'))
        	{
        		$band = 1;// encontro el controlador y el metodo en la sesion, el user tiene permiso
        	}
        }

        if($band == 1)
        {
    	
	    	//$this->load->view("panel/funciones_calendario",$data); 	
		    //$this->load->view('panel/vista_horarios',$data);
		    $tutor = true;
			$alumno = true;
			foreach ($this->session->userdata('colegios') as $colegio) 
			{
				//var_dump($colegio['rol']);
				if($colegio['rol'] != 'Tutor')
				{
					$tutor = false;
					//die();
				}
				if($colegio['rol'] != 'Alumno')
				{
					$alumno = false;
					//die();
				}

			}
				# code...
			
			if($tutor == true)
			{
				//echo "hola tutores";
				$divisiones = $this->get_divisiones_x_colegio($urlApi, $passApiKey, $data['idusuario'], $data['idcolegio'], $data['nombregrupo']);	
				$data['divisiones'] = json_decode($divisiones);

				$horarios = $this->get_horarios($urlApi, $passApiKey, $data['idusuario'], $data['idcolegio']);	
				//$data['horarios'] = json_decode($horarios);
				//print_r($data['horarios']);

				$dias = $this->get_dias($urlApi, $passApiKey, $data['idusuario']);	
				$data['dias'] = json_decode($dias);

				$data['opcion_menu'] = 'horarios';
				$data['contenido'] = 'panel/horarios/vista_horarios_tutor';
				$this->load->view('include/template_tutores',$data);
			}
			else if($alumno == true)
			{		
				
				$divisiones = $this->get_divisiones_x_colegio($urlApi, $passApiKey, $data['idusuario'], $data['idcolegio'], $data['nombregrupo']);	
				$data['divisiones'] = json_decode($divisiones);

				$horarios = $this->get_horarios($urlApi, $passApiKey, $data['idusuario'], $data['idcolegio']);	
				//$data['horarios'] = json_decode($horarios);
				//print_r($data['horarios']);

				$dias = $this->get_dias($urlApi, $passApiKey, $data['idusuario']);	
				$data['dias'] = json_decode($dias);
				$data['opcion_menu'] = 'horarios';
				$data['contenido'] = 'panel/horarios/vista_horarios_alumnos';
				$this->load->view('include/template_tutores',$data);
				
			}
		    /*if($this->session->userdata('nombregrupo') == 'Tutor')
			{
				//die();
				//echo "hola tutores";
				$divisiones = $this->get_divisiones_x_colegio($urlApi, $passApiKey, $data['idusuario'], $data['idcolegio'], $data['nombregrupo']);	
				$data['divisiones'] = json_decode($divisiones);

				$horarios = $this->get_horarios($urlApi, $passApiKey, $data['idusuario'], $data['idcolegio']);	
				//$data['horarios'] = json_decode($horarios);
				//print_r($data['horarios']);

				$dias = $this->get_dias($urlApi, $passApiKey, $data['idusuario']);	
				$data['dias'] = json_decode($dias);

				$data['opcion_menu'] = 'horarios';
				$data['contenido'] = 'panel/vista_horarios_tutor';
				$this->load->view('include/template_tutores',$data);
				//$this->load->view('panel/vista_principal_tutores',$data);
			}*/
			/*else if($this->session->userdata('nombregrupo') == 'Alumno')
			{
				$divisiones = $this->get_divisiones_x_colegio($urlApi, $passApiKey, $data['idusuario'], $data['idcolegio'], $data['nombregrupo']);	
				$data['divisiones'] = json_decode($divisiones);

				$horarios = $this->get_horarios($urlApi, $passApiKey, $data['idusuario'], $data['idcolegio']);	
				//$data['horarios'] = json_decode($horarios);
				//print_r($data['horarios']);

				$dias = $this->get_dias($urlApi, $passApiKey, $data['idusuario']);	
				$data['dias'] = json_decode($dias);
				$data['opcion_menu'] = 'horarios';
				$data['contenido'] = 'panel/vista_horarios_alumnos';
				$this->load->view('include/template_tutores',$data);
			}*/
			else
			{
				//echo "hola no tutor"; die();
				$divisiones = $this->get_divisiones_x_colegio($urlApi, $passApiKey, $data['idusuario'], $data['idcolegio'], $data['nombregrupo']);	
				$data['divisiones'] = json_decode($divisiones);


				//$horarios = $this->get_horarios($urlApi, $passApiKey, $data['idusuario'], $data['idcolegio']);	
				//$data['horarios'] = json_decode($horarios);

				$dias = $this->get_dias($urlApi, $passApiKey, $data['idusuario']);	
				$data['dias'] = json_decode($dias);

				$data['opcion_menu'] = 'horarios';
			    $data['contenido'] = 'panel/horarios/vista_horarios_por_rol';
			    //$data['contenido'] = 'panel/vista_horarios';
				$this->load->view('include/template_colegio',$data);
			}	
		}
		else redirect(404);

    }
}
?>
