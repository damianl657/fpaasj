<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';


    class Colegio extends REST_Controller {

    function __construct()
    {
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        parent::__construct();
        $this->load->model("modelo_colegio");
        // Configurar limites para cada uno de los metodos, no solo controlador
        // La tabla limits tiene que estar creada y la opcion limits TRUE en application/config/rest.php
        
        //$this->methods['tutores_get']['limit'] = 1; // 500 peticiones por hora por usuario/key
        //$this->methods['user_post']['limit'] = 100; // 100 peticiones por hora por usuario/key
        //$this->methods['user_delete']['limit'] = 50; // 50 peticiones por hora por usuario/key
    }
    
    public function get_colegios_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $colegios = $this->modelo_colegio->obtener_colegios();
            $planes_estudio = $this->modelo_colegio->obtener_planesestudio();
            $ciclos_lectivos = $this->modelo_colegio->obtener_ciclos_lectivos();
            $tipos_colegios = $this->modelo_colegio->obtener_tipos_colegios();
            

            $data = array
                    ( 
                        'status' => 0,
                        'message' => 'ok',
                        'colegios' => $colegios->result(),
                        'cant_colegios' => $colegios->num_rows(),
                        'planes_estudio' => $planes_estudio->result(),
                        'cant_planes_estudio' => $planes_estudio->num_rows(),
                        'ciclos_lectivos' => $ciclos_lectivos->result(),
                        'cant_ciclos_lectivos' => $ciclos_lectivos->num_rows(),
                        'tipos_colegios' => $tipos_colegios->result(),
                        'cant_tipos_colegios' => $tipos_colegios->num_rows(),
                    );
            $data = json_encode($data);
            $this->response($data, REST_Controller::HTTP_OK); 
        }
    }
    public function registrar_colegio_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {  // $datos = $this->post('nombre');
         //   print_r($datos);
         //   die();
            $data = array
                    ( 
                        'nombre' => $this->post('nombre'),
                        'tipo' => $this->post('tipo'),
                        'areas' => $this->post('area'),
                        'planestudio' => $this->post('planestudio'),
                        'cicloa' => $this->post('cicloa'),
                    );
            $this->modelo_colegio->registrar_colegio($data);
            $ultimoId = $this->db->insert_id();

            $data = array
                    ( 
                        'status' => 1,
                        'message' => 'ok',
                        'ultimoId' => $ultimoId,
                    );
            $data = json_encode($data);
            $this->response($data, REST_Controller::HTTP_OK); 

        }
    }
    public function subir_foto_colegio_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {  // $datos = $this->post('nombre');
            print_r($_POST);
         //   die();
            
            $data = array
                    ( 
                        'logo' => $this->post('fileName'),
                        //'ultimoId' => $ultimoId,
                    );
            $data = json_encode($data);

            $this->response($data, REST_Controller::HTTP_OK); 

        }
    }
    public function registrar_foto_colegio_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {  // $datos = $this->post('nombre');
           // print_r($_POST);
         //   die();
            
            $data = array
                    ( 
                       'logo' => $this->post('fileName'),
                        //'ultimoId' => $ultimoId,
                    );
           // $data = json_encode($data);
            $idfile = $_POST['idfile'];
            $this->modelo_colegio->update_colegio($data, $idfile);
            $data = array
                    ( 
                        'status' => 1,
                        'message' => 'ok',
                        
                    );
            $data = json_encode($data);
            $this->response($data, REST_Controller::HTTP_OK); 

        }
    }
    public function get_colegio_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {  // $datos = $this->post('nombre');
           // print_r($_POST);
         //   die();
            
            $colegio = $this->modelo_colegio->get_colegio2($this->post('idcolegio'));
            $data = array
                    ( 
                        'status' => 1,
                        'message' => 'El Colegio fue dado de Alta',
                        'colegio' => $colegio
                    );
            $data = json_encode($data);
            $this->response($data, REST_Controller::HTTP_OK); 

        }
    }

    /*public function colegios_cargados_get()
    {       
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $nodos = $this->modelo_usuario->IsRol($userid, 'Nodos'); //pregunto si es usuario nodos
        
            if($nodos['sies'])
            {
                $result = $this->modelo_colegio->obtener_colegios_activos();

            }

            
                 
            if ($result)// Si existe mas de un resultado, lo mando
            {
                $data = json_encode($result);
                $this->response($data, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron niveles cargados'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }*/

    

   


}
