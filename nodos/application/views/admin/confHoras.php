
<script type="text/javascript">


var passApiKey = '<?php echo $passApiKey; ?>';
var urlApi = '<?php echo $urlApi; ?>';


var idUser =  '<?php echo $idusuario; ?>';
var idcolegio =  '<?php echo $idcolegio; ?>';
var idusergrupo =  '<?php echo $idusergrupo; ?>';

var datos = Array();

function listar_horas(){
              
    var rango = $('#rangeselected').val();
     
    var hora1 = $('#horaI').val();
    var hora2 = $('#horaF').val();

    //console.log(hora1 +'--'+hora2);

    if (hora1.indexOf('PM')!=-1) { //si es PM le sumo 12 horas, porque el plugins trabaja hasta 12 horas :)
       var aux = summarHoras(hora1,'12:00');
       /*if( (compararHoras('00:00',aux) == -1) || (compararHoras(aux,'01:00') == -1) )  // (00:00 < aux < 01:00)
           hora1 = hora1.replace("PM", "")
         else*/
           hora1=aux; 
    }
    if (hora2.indexOf('PM')!=-1) {
       //hora2 = summarHoras(hora2,'12:00');
       var aux = summarHoras(hora2,'12:00');

       //alert('sum: '+aux);

       /*if( (compararHoras('00:00',aux) == -1) || (compararHoras(aux,'01:00') == -1) )  // (00:00 < aux < 01:00)
           hora2 = hora2.replace("PM", "")
         else*/
           hora2=aux;
    }
    if (hora1.indexOf('AM')!=-1) { //si es AM le quito el 'AM', el de arriba tambien le quita el PM :)
       hora1 = hora1.replace("AM", "")
    }
    if (hora2.indexOf('AM')!=-1) {
       hora2 = hora2.replace("AM", "")
    }

    //alert(hora1); alert(hora2);
    

     if( (hora1) && (hora2) )
     {
          if( compararHoras(hora1,hora2) == 1 )  //hora1>hora2
              $('.top-right').notify({
                  message: { text: 'Hora Inicio debe ser Menor a Hora Fin' },
                  fadeOut: { enabled: true, delay: 5000 },
                  type: 'warning2'
              }).show(); 
          else
          {
               $('#tablaConfhora tbody tr').remove(); 
               datos=0;

               var hours = Math.floor( rango / 60);          
               var minutes = rango % 60;  
               var sumar = hours+':'+minutes    

               var acum = hora1;
               var cont=1;
               var parametros = Array();
               while( (compararHoras(acum,hora2) == -1) )  //acum<hora2 && acum>hora2    
               {   //console.log( summarHoras(acum,sumar) );
                   var aux = summarHoras(acum,sumar);

                   if( compararHoras(aux,hora2) == 1) //aux > hora2         
                        texto="<tr> <td>"+cont+"<td> <td>"+acum+"<td> <td>"+hora2+"<td> </tr>";         
                   else
                        texto="<tr> <td>"+cont+"<td> <td>"+acum+"<td> <td>"+aux+"<td> </tr>";

                   $('#tablaConfhora tbody').append(texto);

                    
                   var fila = {           
                    'hora_inicio': acum,
                    'hora_fin': aux,
                    'numero': cont,
                    'colegio_id': idcolegio
                   };
                   parametros.push( fila );
                    
                   acum = aux;
                   cont ++;
               }
               datos=parametros; //datos lo uso al momento de guardar
               //console.log(datos);             
          }
     }
}

function summarHoras(inicio, fin)
{  
      var arre, arre2 = new Array();
      arre = inicio.split(":");
      arre2 = fin.split(":");

      inicioMinutos = parseInt(arre[1]);
      inicioHoras = parseInt(arre[0]); 

      finMinutos = parseInt(arre2[1]);
      finHoras = parseInt(arre2[0]);

      transcurridoMinutos = finMinutos + inicioMinutos;
      transcurridoHoras = finHoras + inicioHoras;
      
      if (transcurridoMinutos >= 60) {
        transcurridoHoras++;
        transcurridoMinutos = transcurridoMinutos - 60;
      }

      if ( transcurridoHoras > 23 )
          transcurridoHoras = transcurridoHoras - 24;
      
      horas = transcurridoHoras.toString();
      minutos = transcurridoMinutos.toString();
      
      if (horas.length < 2) {
        horas = "0"+horas;
      }
      
      if (minutos.length < 2) {
        minutos = "0"+minutos;
      }
      
      return horas+":"+minutos;
}

function compararHoras(sHora1, sHora2) { 
     
    var arHora1 = sHora1.split(":"); 
    var arHora2 = sHora2.split(":"); 
     
    // Obtener horas y minutos (hora 1) 
    var hh1 = parseInt(arHora1[0],10); 
    var mm1 = parseInt(arHora1[1],10); 

    // Obtener horas y minutos (hora 2) 
    var hh2 = parseInt(arHora2[0],10); 
    var mm2 = parseInt(arHora2[1],10); 

    // Comparar 
    if (hh1<hh2 || (hh1==hh2 && mm1<mm2)) 
        return -1;  //menor
    else if (hh1>hh2 || (hh1==hh2 && mm1>mm2)) 
        return 1;  //mayor
    else  
        return 0; //iguales
} 

//FUNCTION PARA GUARDAR DATOS
  function guardarConfHoras(){
      var urlRequest = urlApi+"/horarios/inserthorarios";                                 
        
      var stringJson = JSON.stringify(datos);  
      
      $.ajax({
          url: urlRequest,
          type: 'POST',
          headers: {    
              'APIKEY' : passApiKey,
              'userid' : idUser
          },
          data: {data: stringJson, idcolegio:idcolegio, idusergrupo:idusergrupo},
          beforeSend: function() {
                $('#nextHora').after("<img id='loadingNivC' src='<?=base_url('assets/images/loading.gif');?>' height='30' width='30' />");
          },
          success: function(data){
              //console.log(data);

                $('#loadingNivC').remove();
                
                if(data['status']==true)
                    {  
                      $('.top-right').notify({
                            message: { text: data['message'] },
                            fadeOut: { enabled: true, delay: 5000 },
                            type: 'success2'
                        }).show();                                     
                    }
                    else{
                        if(data['message'] == 'NO LOGUIN')                                        
                            location.href = "<?php echo base_url('login'); ?>";
                        else                                    
                            $('.top-right').notify({
                                message: { text: data['message'] },
                                fadeOut: { enabled: true, delay: 5000 },
                                type: 'danger2'
                            }).show();                        
                          }
                },
                error: function(response){
                    console.log(response);
                    
                }
            });
  }



$(document).ready(function(){

      $.noConflict(); 

      $('#horaI').ptTimeSelect({
           hoursLabel: 'Hora',
           minutesLabel: 'Min',
           popupImage:     "<img src='<?=base_url('assets/images/black-clock.png');?>' width='25' height='25'>",
           //popupImage:     "<img src='<?=base_url('assets/images/blue-clock.jpg');?>' width='25' height='25'>",
            onClose: function(){             
                 listar_horas();
            }
      });
       $('#divH2 input').ptTimeSelect({
           hoursLabel: 'Hora',
           minutesLabel: 'Min', 
           popupImage:     "<img src='<?=base_url('assets/images/black-clock.png');?>' width='25' height='25'>",    
           onClose: function(){             
                 listar_horas();
            }
      });


   
      /*SLIDER: PARA EL RANGO DE MINUTOS*/
      $(".slider")
        .slider({
            max: 90,
            min: 20,
            step:5,
            change: function(event, ui) { 
              //alert(ui.value);
              $('#rangeselected').val(ui.value);
                listar_horas();
              } 
         })
        .slider("pips", {    
            rest: "label",
            //prefix: "$",
            suffix: "min"
      })

      $('#rangeselected').val('20');

      $('#nextHora').click(function(e){ guardarConfHoras(); });

  });

</script>


<div class = "container">

    <div id="cuerpo" >
  	       
          <div class = "confHora" id="confHora" >
             <br>
            <div class="alert alert-dismissible alert-info">Atenci&oacute;n: indique Hora de inicio y fin de actividades.</div>          
                
                <section> 
                  <article id="aSearch">
                  <div>
                     <label for="horaI">Inicio de Activiades</label>
                     <div id='divH1'><input placeholder="HH:MM" id="horaI" name="horaI" /> </div>
                    
                     <label for="horaF">Fin de Activiades</label>                              
                     <div id='divH2'><input placeholder="HH:MM" id="horaF" name="horaF"/></div>
                    
                     <div class="slider"></div>
                     <input type="hidden" id="rangeselected"/>                  
                  </div>
                  <br>
                  <br> 
                  <table class='table table-striped table-hover' id="tablaConfhora">
                      <thead>
                        <tr>             
                          <th><div align="center">Hora</div></th><th><div align="center">Inicio</div></th><th><div align="center">Fin</div></th><th><th></th>
                        </tr>
                      </thead>
                      <tbody>                          
                      </tbody>                
                  </table>
                   <?// echo form_close();?>
                  </article> 
                </section>   
                <br>
                
              <button  class="btn btn-danger " id='nextHora' class="next" style='float:right; margin-right:5%'>Finalizar</button>    
          </div>
              
              
  	</div>

  	
    <br>

    <div class='notifications top-right'></div>
    
</div>


