<?
defined('BASEPATH') OR exit('No direct script access allowed');

class Modelo_torneo extends CI_Model {

 public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->load->database();
    }
 
public function get_obtener_torneoxclub($idclub)
{
	if ($idclub==1) {
       $sql="SELECT inscripcion_club.id_club, inscripcion_club.id_torneo, torneo.nombre, torneo.organizador, torneo.descripcion,torneo.inicio, torneo.fin FROM inscripcion_club 
        JOIN nodocolegio ON nodocolegio.id=inscripcion_club.id_club 
        JOIN torneo on torneo.id=inscripcion_club.id_torneo
        WHERE torneo.estado=1
        GROUP BY inscripcion_club.id_torneo"; 
    } else {
    
	$sql="SELECT inscripcion_club.id_club, inscripcion_club.id_torneo, torneo.nombre, torneo.organizador, torneo.descripcion,torneo.inicio, torneo.fin  FROM inscripcion_club 
		JOIN nodocolegio ON nodocolegio.id=inscripcion_club.id_club 
		JOIN torneo on torneo.id=inscripcion_club.id_torneo
		WHERE nodocolegio.id=$idclub and torneo.estado=1 GROUP BY inscripcion_club.id_torneo";
        
    }
    
	$query = $this->db->query($sql);
	if ($query->num_rows()!=0) {
		return $query->result();
	}else return false;
}
public function obtener_torneo_x_id($torneo)
{   
    $sql="SELECT * FROM `torneo` WHERE `id` = $torneo";    
    $query = $this->db->query($sql);
    if ($query->num_rows()!=0) {
        return $query->result();
    }else return false;
}
public function get_obtener_torneos(){
	$sql="SELECT torneo.*, nodocolegio.nombre AS institucion FROM `torneo` JOIN nodocolegio on nodocolegio.id=torneo.organizador where torneo.estado=1";
	$query= $this->db->query($sql);
	if ($query->num_rows()!=0) {
		return $query->result();
	}else return false;
}
 public function guardar_inscripcion_a_torneo($arre){

            $this->db->insert('inscripcion_a_torneo', $arre);
            $var=1;
            return $var;
        }

public function obtener_inscriptos_torneo($torneo,$deportista,$especialidad,$division,$club){

            $sql="SELECT *  FROM `inscripcion_a_torneo` WHERE `torneo` = $torneo AND `deportista` = $deportista AND `especialidad` = $especialidad AND `division` = $division AND `club` = $club ";
            
            $query= $this->db->query($sql);
            if ($query->num_rows()>0) {
            	$estado=1;
            	return $estado;
            }else{
            	$estado=0;
            	return $estado;
            }
}
public function lista_inscriptos_torneo($torneo,$club){
/*
            if ($club==1) {
                $sql="SELECT inscripcion_a_torneo.* FROM `inscripcion_a_torneo`
                JOIN users_groups on users_groups.id = inscripcion_a_torneo.deportista";
            }else{

            }*/
          
            if ($club==1) {
               
            	$sql="SELECT inscripcion_a_torneo.id,users.fechanac,users.first_name,users.last_name,users.documento,especialidades.nombre as nespecialidad, divisiones.id as iddivisiones ,divisiones.nombre as ndivision,nodocolegio.nombre AS nclub, torneo.nombre as ntorneo,torneo.id as idtorneo,nodocolegio.id as idclub, eficiencia.nombre as eficiencia,inscripcion_a_torneo.categoria 
            FROM `inscripcion_a_torneo` 
            JOIN users_groups on users_groups.id = inscripcion_a_torneo.deportista 
            JOIN users on users.id = users_groups.user_id 
            JOIN torneo ON torneo.id = inscripcion_a_torneo.torneo 
            JOIN divisiones on divisiones.id = inscripcion_a_torneo.division 
            JOIN eficiencia on eficiencia.id = inscripcion_a_torneo.eficiencia
            JOIN especialidades on especialidades.id = inscripcion_a_torneo.especialidad 
            JOIN nodocolegio on nodocolegio.id = inscripcion_a_torneo.club
            WHERE inscripcion_a_torneo.torneo = $torneo and torneo.estado=1 and inscripcion_a_torneo.estado=1";
            }else{
            	$sql="SELECT inscripcion_a_torneo.id,users.fechanac,users.first_name,users.last_name,users.documento,especialidades.nombre as nespecialidad, divisiones.nombre as ndivision,nodocolegio.nombre AS nclub, torneo.nombre as ntorneo,torneo.id as idtorneo,nodocolegio.id as idclub  
            FROM `inscripcion_a_torneo` 
            JOIN users_groups on users_groups.id = inscripcion_a_torneo.deportista 
            JOIN users on users.id = users_groups.user_id 
            JOIN torneo ON torneo.id = inscripcion_a_torneo.torneo 
            JOIN eficiencia on eficiencia.id = inscripcion_a_torneo.eficiencia
            JOIN divisiones on divisiones.id = inscripcion_a_torneo.division 
            JOIN especialidades on especialidades.id = inscripcion_a_torneo.especialidad 
            JOIN nodocolegio on nodocolegio.id = inscripcion_a_torneo.club 
            WHERE inscripcion_a_torneo.club = $club AND inscripcion_a_torneo.torneo = $torneo and torneo.estado=1 and inscripcion_a_torneo.estado" ;
            
            }
            //print_r($sql);
            $query = $this->db->query($sql);

            if($query->num_rows() > 0){
                
                return $query->result();
                
            }else{
                return false;
            };
}
public function buscar_torneo($torneo){
    $sql="SELECT torneo.*, nodocolegio.nombre AS institucion FROM `torneo` JOIN nodocolegio on nodocolegio.id=torneo.organizador
    where torneo.id = $torneo and torneo.estado=1";
    $query= $this->db->query($sql);
    if ($query->num_rows()!=0) {
        return $query->result();
    }else return false;
}
public function eleminar_insc_club($torneo){
    $sql="DELETE FROM `inscripcion_club` WHERE `id_torneo`=$torneo";
    $query=$this->db->query($sql);
}
public function updarte_torneo($idtorneo,$torneo,$organizador,$descripcion,$inicio,$fin){
    $sql="UPDATE `torneo` SET `nombre`=$torneo,`organizador`=$organizador,`descripcion`=$descripcion,`inicio`=$inicio,`fin`= $fin WHERE id = $id_torneo";
    
    $query=$this->db->query($sql);
}
public function obtener_club_x_torneo($idtorneo){
    $sql="SELECT nodocolegio.* FROM `inscripcion_club` JOIN nodocolegio ON inscripcion_club.id_club=nodocolegio.id where inscripcion_club.id_torneo=$idtorneo and estado=1";
    
    $query=$this->db->query($sql);
    if ($query->num_rows()!=0) {
        return $query->result();
    }else return false;
}

}?>

