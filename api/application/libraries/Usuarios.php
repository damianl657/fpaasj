<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    /**
     * Clase para almacenar funciones varias comunes a todos
     * los controladores y metodos
     */
    class Msg
    {
        public $title;
        public $body;
        public $priority;
        public $vibrate;
        public $sound;
        public $badge;
        public $channelId;
        public $to;
    }
    class Usuarios
    {
        private $CI; // Por si hace falta acceder desde algun metodo a la instancia global de CodeIgniter

        public function __construct()
        {
            $this->CI = get_instance();
        }

        public function enviar_notificacion($registration_ids, $data)
        {
            //Obtengo la apikey gcm seteada en la libraries utilidades
            $apikey_gcm = $this->CI->utilidades->get_apikey_gcm();
            //Google cloud messaging GCM-API url
            $url = 'https://android.googleapis.com/gcm/send';
            $fields = array(
                //'to' => '/topics/my_little_topic',
                'registration_ids' => $registration_ids,
                'data' => $data
            );
            // Google Cloud Messaging GCM API Key

            $headers = array(
                'Authorization: key=' . $apikey_gcm,
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );

            //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
            //curl_setopt($ch, CURLOPT_TIMEOUT, 5); //timeout in seconds
            
            $result = curl_exec($ch);
            //print_r($result); 
            if (!$result) {
                echo 'Curl failed: ' . curl_error($ch);

                /*if(curl_errno($ch) == 28){ //timeout
                  echo 'timeout,';
                  curl_close($ch);
                  return '{"success":0}';
                }*/
            }
            curl_close($ch);
            return $result;            
        }


        public function enviar_notificacion_expo($data)
        {
            $namecolegio = $data['colegio'];
            $title = $data['title'];
            $token = $data['token'];


            $tokenvalid = substr($token, 0, 18) ===  "ExponentPushToken[" && substr($token, -1) === ']' ; 
            if (!$tokenvalid)
              return array('success'=>0, 'error'=>'DeviceNotRegistered');
           
            $url = 'https://exp.host/--/api/v2/push/send';
            
            // Build the notification data
            $sounAndroid = [
                  //'sound'=> 'default',
                  'sticky' => true,
                  'color' => 'blue',
                  'priority'=>'max',

                ];

            $notification = [
                'title' => $namecolegio, 
                'body' => $title, 
                'priority'=>'high',
                'vibrate'=> true,
                'sound'=> 'default',
                'badge'=> 1,
                //'android'=> $sounAndroid,
                "channelId"=>"comunicados", //android 8.0 later
                'to'=> $token,
              ];

            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'accept: application/json',
                'content-type: application/json',
            ]);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($notification));
            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );

            $result = curl_exec($ch);

            if (!$result) {
                echo 'Curl expo failed: ' . curl_error($ch);

            }
            curl_close($ch);
            
            $pushStatus = json_decode($result,true);

            if($pushStatus['data']['status']=='ok'){
                $success = 1;
                $error = "";
            }
            else 
                if($pushStatus['data']['status']=='error'){
                    $success = 0;
                    $error = $pushStatus["data"]['details']["error"];
                }
                else {
                      $success = 0;
                      $error = "";
                    } 

            return array('success'=>$success, 'error'=>$error);           
        }


        function enviar_email($destinos,$mensaje, $from1, $from2, $asunto)
        {                
              //$CI = & get_instance();

              // Configure email library
              $config = array();
              $config['protocol'] = 'smtp';
              $config['smtp_host'] = 'smtp.e-nodos.com';
              $config['smtp_port'] = 9025;
              $config['smtp_user'] = 'smtpenodos';
              $config['smtp_pass'] = ']OUQDEzdZO{I';
              $config['mailtype'] = 'html';
      
              $config['charset']  = 'utf-8';
              $config['newline']  = "\r\n";
              $config['wordwrap'] = TRUE;


               
              $this->CI->load->library('email');

              $this->CI->email->initialize($config);
               
              $this->CI->email->from($from1, $from2);
              //$this->CI->email->set_mailtype('html');
               
               //$CI->email->to();
               
               $this->CI->email->bcc($destinos);
               $this->CI->email->subject($asunto);
              
               $this->CI->email->message($mensaje);
               if($this->CI->email->send()){
                    $data['enviado'] = 1;
                    
               }
               else{
                       $data['enviado'] = 0;
                       $data['error'] = $this->CI->email->print_debugger();
               }
               return  $data;
        }  

        function IsRol($iduser, $nombrerol)
        {
            //$data = json_decode($_POST['data']) ;
            $SiES=0;

            $this->CI->load->model("modelo_usuario");
            $roles = $this->CI->modelo_usuario->get_rolesXidusuario($iduser);
            if ($roles)
            {
                foreach ($roles as $rol)
                {
                    if($rol->nombregrupo==$nombrerol) //solo un usuario nodos puede hacerlo
                    {
                        $idusergroup = $rol->id;

                        $idgrupo = $rol->idgrupo;
                        
                        $SiES=1;
                    }
                }
            }else $data['error']="NoRoles";

            if($SiES)
            {
                $data['idrol']=$idgrupo;
                $data['idusergroup']=$idusergroup;
                $data['sies'] = 1;
            }
            else
            {
                $data['sies'] = 0;
            }

            return $data;
        }

        function get_mispermisos($iduser)//este estaria bueno para nodosweb (session)
        {
            $this->CI->load->model("modelo_usuario");
            $reglas = $this->CI->modelo_usuario->get_permisosXidusuario($iduser);
            if ($reglas)
            {
                foreach ($reglas as $regla)
                {
                    $idsAcciones = $regla->id_menus_acciones;

                    $acciones = $this->CI->modelo_usuario->get_menu($idsAcciones);

                    $regla->acciones = $acciones;
                }

                return $reglas;
            }else return "vacio";
            
        }  

        function check_permiso($iduser, $idcolegio, $permiso)//chequea si un usuario tiene un permiso en un colegio dado
        { 
            
        } 
        
        //Nuevos metodos para enviar el evento (mensaje) de forma masiva
               public function batch_enviar_notificacion($arr_data_gcm)
        {
            if((count($arr_data_gcm)<=0) || (!is_array($arr_data_gcm)))
                return false;
            
            $registration_ids = array();
            foreach ($arr_data_gcm as $key => $value) {
                if($value['active'] == 0)
                {
                    $arr_data_gcm['enviado'] = 0;
                    $arr_data_gcm['error'] = 'no_activo';
                    continue;
                }
                else if($value['pago'] == 0)
                {
                    $arr_data_gcm['enviado'] = 0;
                    $arr_data_gcm['error'] = 'no_pago';
                    continue;
                }
                else if($value['recibir_notificacion'] == 0)
                {
                    $arr_data_gcm['enviado'] = 0;
                    $arr_data_gcm['error'] = 'no_recibir_notificacion';
                    continue;
                }
                $registration_ids[] = $value['token'];
            }
            $data = $arr_data_gcm[0];
            //Obtengo la apikey gcm seteada en la libraries utilidades
            $apikey_gcm = $this->CI->utilidades->get_apikey_gcm();
            //Google cloud messaging GCM-API url
            $url = 'https://android.googleapis.com/gcm/send';
            $fields = array(
                //'to' => '/topics/my_little_topic',
                'registration_ids' => $registration_ids,
                'data' => $data
            );
            // Google Cloud Messaging GCM API Key

            $headers = array(
                'Authorization: key=' . $apikey_gcm,
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
            //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
            //curl_setopt($ch, CURLOPT_TIMEOUT, 5); //timeout in seconds
            $result = curl_exec($ch); //"Error=DeprecatedEndpoint"
            //print_r($result); 
            if (!$result) {
                echo 'Curl failed: ' . curl_error($ch);
                /*if(curl_errno($ch) == 28){ //timeout
                  echo 'timeout,';
                  curl_close($ch);
                  return '{"success":0}';
                }*/
            }
            curl_close($ch);
            if((is_array($result)) && count($result)>0)
            {
                $cant  = count($arr_data_gcm);
                $aux_pushStatus =  $result;
                for ($i=0; $i < $cant ; $i++) {
                    $errorJson = explode("=",$aux_pushStatus[$i]);
                    if($errorJson[1]=="DeprecatedEndpoint"){
                        $arr_data_gcm[$i]['enviado'] = 0;
                        $arr_data_gcm[$i]['error'] = "DeprecatedEndpoint";
                    }
                    else{
                        $arr_data_gcm[$i]['enviado'] = 1;
                        $arr_data_gcm[$i]['error'] = '';
                    }
                }
            }
            else
            {
                $cant  = count($arr_data_gcm);
                for ($i=0; $i < $cant ; $i++) {
                    $arr_data_gcm[$i]['enviado'] = 0;
                    $arr_data_gcm[$i]['error'] = '';
                }
            }
            return $arr_data_gcm;            
        }

        public function batch_enviar_notificacion_expo($data)
        {
            $cant  = count($data);
            for ($i=0; $i < $cant ; $i++) { 
                    $data[$i]['enviado'] = 0;
                    $data[$i]['error'] = '';
            }
           
            $sounAndroid = [
                'sticky' => true,
                'color' => 'blue',
                'priority'=>'max'
              ];
           
            $url = 'https://exp.host/--/api/v2/push/send';
            $array_notificacion = array(); //Arreglo con los mensajes q se enviarán
           // $notSend = array(); //resguardo posicion y datos de los q no serán notificados
            $token = 0;
            $tokenvalid = 0;
            $n = 0; //index of noti_arr
            foreach ($data as $key => $value)  {
                $token = $value['token'];
                $tokenvalid = substr($token, 0, 18) ===  "ExponentPushToken[" && substr($token, -1) === ']' ; 
                if($value['active'] == 0 )
                {
                  $data[$key]['error'] = 'no_active_user';
                  continue;
                }
                else if( $value['pago'] == 0)
                {
                  $data[$key]['error'] = 'no_pago';
                  continue;
                }
                else if($value['recibir_notificacion'] == 0)
                {
                  $data[$key]['error'] = 'no_recibir_notificacion';
                  continue;
                }
                else if(!$tokenvalid)
                {
                    $data[$key]['error'] = 'invalid_token';
                    continue;
                }
                else if($value['token']) //verifico que el token esté una sola vez
                {
                    $yasta = 0;
                    foreach ($array_notificacion as $k => $v) {
                        if($token == $k['to'])
                        {
                            $yasta = 1;
                        }
                    }
                    if($yasta == 1)
                    {
                        $data[$key]['error'] = 'duplicated_token';
                        continue;
                    }
                }
                $namecolegio = $value['colegio'];
                $title       = $value['title'];
                $token       = $value['token'];

                $Msg = new Msg();
                $Msg->title = $namecolegio;
                $Msg->body = $title;
                $Msg->priority = 'high';
                $Msg->vibrate = true;
                $Msg->sound = 'default';
                $Msg->badge = 1;
                $Msg->channelId = "comunicados";
                $Msg->to = $token;

                $array_notificacion[$n]['data'] = &$Msg;
                $array_notificacion[$n]['index'] = $key;
                $array_notificacion[$n]['response'] = 0; //respuesta
                $n++;
            }
            //Se puede enviar un máximo de 100 registros a la vez
            $veces_a_enviar = 0;
            $registros = count($array_notificacion); //568
            if($registros>100)
                $veces_a_enviar = ceil(($registros / 100));
            else
                $veces_a_enviar = 1;

            $pushStatus = array();//Contendra los resultados de los envios.
            $j = 0; //Indice global
            $z = 0; //Indice para reccorer el array "array_notificacion"
            for ($i=0; $i < $veces_a_enviar ; $i++) {
                $send_array = array();
                for ($x=0; $x < 100; $x++) {
                    if($j>=$registros) break;
                    $send_array[] = $array_notificacion[$x]['data'];
                    $j++;
                }
                //var_dump($send_array);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'accept: application/json',
                    'content-type: application/json',
                ]);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($send_array));
                // Disabling SSL Certificate support temporarly
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
                $result = curl_exec($ch);
                if (!$result) {
                    $error = 'Curl expo failed: ' . curl_error($ch);
                    echo 'Curl expo failed: ' . curl_error($ch);
                    break;
                }
                else{
                    $pushStatus[] = json_decode($result,true);
                    if(isset($pushStatus[0]['data']))
                    {
                        foreach ($pushStatus[0]['data'] as $key => $value) {
                            if(isset( $value['status'] ))
                            {
                                $array_notificacion[$z]['response'] = $value;
                            }
                            else
                            {
                                $array_notificacion[$z]['response'] = 'error';
                            }
                            $z++;
                        }
                    }
                }
                curl_close($ch);
            }
            //Una vez q termina de armar el "array_notificaciones" debo completar el 
            //arreglo "data" que es el que tiene originalmente todos los destinatarios
            //de envios de mensajes via "movil"
           //
           //$array_notificacion[$i]['data]     -> datos del mensaje enviado
           //$array_notificacion[$i]['index]    ->  indice de "data"
           //$array_notificacion[$i]['response] -> estado del envio del mensaje
           //
            foreach ($array_notificacion as $key => $value) {
                $i = $value['index'];
                if($value['response']['status']=="ok")
                {
                    $data[$i]['enviado'] = 1;
                    $data[$i]['status_id'] = $value['response']['id'];
                }
                else{
                    $data[$i]['enviado'] = 0;
                    $data[$i]['status_id'] = 0;
                    $data[$i]['error'] = $value['response']['details']['error'];
                }
               
            }
            return $data;          
        }

    }