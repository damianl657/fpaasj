<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php if(isset($titulo_pagina) && (!empty($titulo_pagina))) { echo $titulo_pagina; } else { echo "NODOS WEB | Dashboard"; } ?></title>
		  <title>Nodos Comunicaci&oacute;n escolar m&aacute;s simple</title>
		 <link href="<?= base_url('assets/images/favicon.ico') ?>" rel="shortcut icon" type="image/x-icon" /> 
		 
		 <link href="<?=base_url('tpl/css/bootstrap.min.css');?>" rel="stylesheet">
		 <link href="<?=base_url('tpl/font-awesome/css/font-awesome.css');?>" rel="stylesheet">
		 
		 <!--<script src="<?=base_url('tpl/js/jquery-2.1.1.js');?>"></script> -->
		 
		  <!-- nodos js -->
	    <script src="<?=base_url('assets/js/jquery-2.1.4.min.js')?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/js/jquery-ui.js');?>"></script>
	    <script src="<?=base_url('tpl/js/bootstrap.min.js');?>"></script>
		<script src="<?=base_url('assets/js/bootbox.min.js');?>"></script> 



        <!-- Estilo de Time Select 
		<script src="<?=base_url('assets/js/jquery.ptTimeSelect.js');?>"></script> 
		<link href="<?=base_url('assets/css/jquery.ptTimeSelect.css');?>" rel="stylesheet">-->
        <!--<script src="<?=base_url('assets/js/timepicker/jquery.ui.core.min.js');?>"></script> 
        <script src="<?=base_url('assets/js/timepicker/jquery.ui.widget.min.js');?>"></script> 
        <script src="<?=base_url('assets/js/timepicker/jquery.ui.tabs.min.js');?>"></script> 
        <script src="<?=base_url('assets/js/timepicker/jquery.ui.position.min.js');?>"></script> 
        
        <script src="<?=base_url('assets/js/timepicker/jquery.ui.timepicker.js?v=0.3.3');?>"></script>
        <link rel="stylesheet" href="<?=base_url('assets/css/timepicker/jquery.ui.timepicker.css?v=0.3.3');?>" type="text/css" />-->

       

  
    		 
		 <!-- barra para seleccionar -->
		<script src="<?=base_url('assets/js/jquery-ui-slider-pips.js');?>"></script> 
		<link href="<?=base_url('assets/css/jquery-ui-slider-pips.css');?>" rel="stylesheet">
        
          <!-- JS NODOS -->
		<script src="<?=base_url('assets/js/main.js');?>"></script> 
		<script src="<?=base_url('assets/js/jquery-latest.min.js');?>"></script> 
		<script src="<?=base_url('assets/js/respond.min.js');?>"></script> 
		 

	    <!-- Mainly scripts -->
        <script src="<?=base_url('tpl/js/plugins/metisMenu/jquery.metisMenu.js');?>"></script>
        <script src="<?=base_url('tpl/js/plugins/slimscroll/jquery.slimscroll.min.js');?>"></script>
        
      		     
        <!-- Data Tables CSS -->
		<link href="<?=base_url('tpl/css/plugins/dataTables/dataTables.bootstrap.css');?>" rel="stylesheet">
		<link href="<?=base_url('tpl/css/plugins/dataTables/dataTables.tableTools.min.css');?>" rel="stylesheet">
		<link href="<?=base_url('tpl/css/plugins/dataTables/dataTables.responsive.css');?>" rel="stylesheet">
		
       
        <!--Toastr-->
		<link href="<?=base_url('tpl/css/plugins/toastr/toastr.min.css');?>" rel="stylesheet">
        <script src="<?=base_url('tpl/js/plugins/toastr/toastr.min.js');?>"></script>
       
		<link href="<?=base_url('tpl/css/animate.css');?>" rel="stylesheet">
		<!-- todo el estilo de la plantilla -->
        <link href="<?=base_url('tpl/css/style.css');?>" rel="stylesheet">	
		
		<!-- nodos css --> 
		<link href="<?=base_url('assets/css/jquery-ui.min.css');?>" rel="stylesheet">
		<link href="<?=base_url('assets/css/jquery-ui.css');?>" rel="stylesheet">
		<link href="<?=base_url('assets/css/general.css');?>" rel="stylesheet">
		<!--Stilos Nodos estilos eze -->
	<?php $this->load->helper('html');
            $this->load->helper('url');
            echo link_tag(base_url('assets/css/burguer.css'));
           // echo link_tag(base_url('assets/css/webapp.css'));
        ?>
     <!--   <link href="<?=base_url('assets/css/main-nodos.css');?>" rel="stylesheet"> 
        <link href="<?=base_url('assets/css/nodos-config.css');?>" rel="stylesheet"> -->
	 
		<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">-->
       

       
        <style type="text/css" media="screen">
            .datepicker {
                z-index:2051 !important;
            }   
        </style>

        


        <!-- FULL CALENDAR -->
         <!--<link href="<?=base_url('assets/css/fullcalendar/fullcalendar.css');?>" rel="stylesheet">-->
        <link href="<?=base_url('assets/css/fullcalendar/fullcalendar.print.css');?>" rel="stylesheet"  media='print'>
        <script src="<?=base_url('assets/js/fullcalendar/moment.min.js');?>"></script>
        <script src="<?=base_url('assets/js/scrollspy/scrollspy.js');?>"></script>
        <!--<script src="<?=base_url('assets/js/fullcalendar/fullcalendar.min.js');?>"></script>
        <script src="<?=base_url('assets/js/fullcalendar/lang/es.js');?>"></script> -->
        <script type="text/javascript" src="<?=base_url('tpl/js/plugins/ckeditor/ckeditor.js')?>"></script>
        <link rel="stylesheet" href="<?php echo base_url('tpl/js/plugins/fullcalendar/fullcalendar.min.css')?>" media="screen">
        <script src="<?php echo base_url('tpl/js/plugins/fullcalendar/fullcalendar.min.js')?>"></script>
        <!--<script src="<?php echo base_url('tpl/js/plugins/moment/moment-with-locales.min.js')?>"></script> -->
        <script src="<?php echo base_url('tpl/js/plugins/fullcalendar/lang/es.js')?>"></script> 



         <!--SELECT2 v3 (v4 no funca)-->
        <script src="<?=base_url('tpl/js/plugins/select2/select2-3.5.4/select2.js');?>"></script>
        <link rel="stylesheet" href="<?=base_url('tpl/js/plugins/select2/select2-3.5.4/select2.css');?>" type="text/css" />
        <link href="<?=base_url('tpl/css/select2-bootstrap.css');?>" rel="stylesheet">

        <!--SELECT2 v4 
        <script src="<?=base_url('tpl/js/plugins/select2/select2-4.0.2/js/select2.full.js');?>"></script>
        <link rel="stylesheet" href="<?=base_url('tpl/js/plugins/select2/select2-4.0.2/css/select2.css');?>" type="text/css" />-->

        <!--Notify-->
        <link href="<?=base_url('assets/css/bootstrap-notify.css');?>" rel="stylesheet">
        <script src="<?=base_url('assets/js/bootstrap-notify.js');?>"></script>

        <!--slider pips-->
        <link href="<?=base_url('assets/css/jquery-ui-slider-pips.css');?>" rel="stylesheet">
        <!--<link href="<?=base_url('assets/css/jquery.ptTimeSelect.css');?>" rel="stylesheet">-->

        
         <!-- UPLOAD -->
        <link href="<?=base_url('tpl/css/plugins/upload/jquery.filer.css');?>" type="text/css" rel="stylesheet" />
        <link href="<?=base_url('tpl/css/plugins/upload/jquery.filer-dragdropbox-theme.css');?>" type="text/css" rel="stylesheet" />
        <script src="<?=base_url('tpl/js/plugins/upload/jquery.filer.min.js');?>"></script>


        <script type="text/javascript">
            jQuery(document).ready(function($) {

                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "progressBar": true,
                    "preventDuplicates": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "showDuration": "400",
                    "hideDuration": "1000",
                    "timeOut": "7000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };        
                <?php 
                    $mensaje_exito = $this->session->flashdata("mensaje_exito");
                    if ((isset($mensaje_exito)) && (!empty($mensaje_exito)))
                    {
                ?>
                        toastr.success("<?php echo $mensaje_exito; ?>","¡Perfecto!");
                <?php 
                    } 
                ?>
                <?php 
                    $mensaje_error = $this->session->flashdata("mensaje_error");
                    if ((isset($mensaje_error)) && (!empty($mensaje_error)))
                    {
                ?>
                        toastr.error("<?php echo $mensaje_error; ?>","¡Atencion!");
                <?php 
                    } 
                ?>
            });
        </script>
         </script>
  

    </head>
<body>

    <div id="wrapper">
        

         <div class='notifications top-right' style="margin-top: 40px">
            </div>
            <?php //$this->load->view("include/particicles");?>
        <?php $this->load->view("include/vista_inc_menu"); ?>
                 <!--<button type="button" aria-hidden="true" class="" data-notify="dismiss">×z</button>-->  
        