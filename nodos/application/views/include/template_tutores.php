<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	
	<title>Nodos Comunicaci&oacute;n escolar m&aacute;s simple</title>
	<meta name="description" content="App para una comunicaci&oacute;n escolar m&aacute;s simple" />
	<meta name="author" content="sietedefebrero.com">
	<!-- Favicon -->
	
	<link href="<?= base_url('assets/images/favicon.ico') ?>" rel="shortcut icon" type="image/x-icon" />
	
	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic|Abel' rel='stylesheet' type='text/css'>
	<!-- Font Awesome -->
	<link href="<?=base_url('tpl/css/bootstrap.min.css');?>" rel="stylesheet">
	<link href="<?=base_url('tpl/font-awesome/css/font-awesome.css');?>" rel="stylesheet">	
	
	
	<script src="<?=base_url('tpl/js/jsb/segment.min.js');?>"></script>
	<script src="<?=base_url('tpl/js/jsb/ease.min.js');?>"></script>
	
	
		  <!-- nodos js -->
	    <script src="<?=base_url('assets/js/jquery-2.1.4.min.js')?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/js/jquery-ui.js');?>"></script>
	    <script src="<?=base_url('tpl/js/bootstrap.min.js');?>"></script>
		<script src="<?=base_url('assets/js/bootbox.min.js');?>"></script> 
		 
        
          <!-- JS NODOS -->
		<script src="<?=base_url('assets/js/main.js');?>"></script> 
		<script src="<?=base_url('assets/js/jquery-latest.min.js');?>"></script> 
		<script src="<?=base_url('assets/js/respond.min.js');?>"></script> 
		<!-- nodos css --> 
		
		<link href="<?=base_url('assets/css/general_tutores.css');?>" rel="stylesheet">
		<!--Stilos Nodos estilos eze -->
		<style type="text/css" media="screen">
            .datepicker {
                z-index:2051 !important;
            }   
        </style>


        <link href="<?=base_url('assets/css/fullcalendar/fullcalendar.print.css');?>" rel="stylesheet"  media='print'>
        <script src="<?=base_url('assets/js/fullcalendar/moment.min.js');?>"></script>
        <script src="<?=base_url('assets/js/scrollspy/scrollspy.js');?>"></script>
       
        <script type="text/javascript" src="<?=base_url('tpl/js/plugins/ckeditor/ckeditor.js')?>"></script>
        <link rel="stylesheet" href="<?php echo base_url('tpl/js/plugins/fullcalendar/fullcalendar.min.css')?>" media="screen">
        <script src="<?php echo base_url('tpl/js/plugins/fullcalendar/fullcalendar.min.js')?>"></script>
        <script src="<?php echo base_url('tpl/js/plugins/fullcalendar/lang/es.js')?>"></script> 

		 <!--Notify-->
      <link href="<?=base_url('assets/css/bootstrap-notify.css');?>" rel="stylesheet">
     <script src="<?=base_url('assets/js/notify.js');?>"></script>
    <script src="<?=base_url('assets/js/push.min.js')?>"></script>

        
        
		<!-- Mainly scripts -->
        <script src="<?=base_url('tpl/js/plugins/metisMenu/jquery.metisMenu.js');?>"></script>
        <script src="<?=base_url('tpl/js/plugins/slimscroll/jquery.slimscroll.min.js');?>"></script>
       
       
		<link href="<?=base_url('tpl/css/animate.css');?>" rel="stylesheet">
		<!-- todo el estilo de la plantilla -->
        <link href="<?=base_url('tpl/css/style.css');?>" rel="stylesheet">	
		<?php //$this->load->view("include/particicles");
  //$this->load->view("include/navbar_tutores");?>
  <?php $this->load->helper('html');
            $this->load->helper('url');
            echo link_tag(base_url('assets/css/burguer.css'));
            //echo"holla";
            if ($aula_v!= null) {
              echo link_tag(base_url('assets/css/webappAV.css'));
            }
            //echo link_tag(base_url('assets/css/webapp.css'));
        ?>

</head>

<body style="padding-top: 75px; ">	
	
	<div id="wrapper">

        

         <div class='notifications top-right' style="margin-top: 40px">
                 <!--<button type="button" aria-hidden="true" class="" data-notify="dismiss">×z</button>-->  
        </div>
        <?php 
        
        $this->load->view("include/particicles");
		    $this->load->view("include/navbar_tutores");
		 ?>
        <?php
        if(!empty($contenido))
                            $this->load->view($contenido);
        
        if(!empty($contenido2))
                            $this->load->view($contenido2);
        ?>
	<footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center">
                        <center><img src=<?=base_url("assets/images/i/footer-config.png")?> width="74" alt="NODOS"/><p>&copy; <? echo date("Y"); ?></p></center>
                        
                    </div>
                </div>
            </div>
    </footer>
     <!-- Mainly scripts -->
     <script src="<?=base_url('tpl/js/plugins/metisMenu/jquery.metisMenu.js');?>"></script>
     <script src="<?=base_url('tpl/js/plugins/slimscroll/jquery.slimscroll.min.js');?>"></script>
     
     <script src="<?=base_url('assets/js/jeditable/jquery.jeditable.js');?>"></script>
  
    <!-- Data Tables -->
     <script src="<?=base_url('tpl/js/plugins/dataTables/jquery.dataTables.js');?>"></script>
     <script src="<?=base_url('tpl/js/plugins/dataTables/dataTables.bootstrap.js');?>"></script>
     <script src="<?=base_url('tpl/js/plugins/dataTables/dataTables.responsive.js');?>"></script>     
     <script src="<?=base_url('tpl/js/plugins/dataTables/dataTables.tableTools.min.js');?>"></script>

  
      <!-- Custom and plugin javascript -->
    <script src="<?=base_url('tpl/js/inspinia.js');?>"></script>  <!-- usa -->
    
    <script src="<?=base_url('tpl/js/plugins/pace/pace.min.js');?>"></script>

    <?php 
     if ((isset($carga_select_dt)) && ($carga_select_dt == true)) { ?>
    <link href="<?php echo base_url('tpl/css/plugins/dataTables/select.dataTables.min.css') ?>" rel="stylesheet"> 
    
    <script src="<?=base_url('tpl/js/plugins/dataTables/dataTables.select.min.js');?>"></script>
   
    <?php } ?>

     <script src="<?php echo base_url('tpl/js/general.js');?>"></script>

     <script src="<?=base_url('assets/js/jquery.validate.min.js')?>"></script>

    <!-- DataTime Master Picker -->
    <script src="<?=base_url('tpl/js/plugins/datatimepicker/jquery.datetimepicker.full.js');?>"></script>
    <link href="<?=base_url('tpl/js/plugins/datatimepicker/jquery.datetimepicker.css');?>" rel="stylesheet">

     <!--SELECT2 v3 (v4 no funca)-->
      <script src="<?=base_url('tpl/js/plugins/select2/select2-3.5.4/select2.js');?>"></script>
      <link rel="stylesheet" href="<?=base_url('tpl/js/plugins/select2/select2-3.5.4/select2.css');?>" type="text/css" />
      <link href="<?=base_url('tpl/css/select2-bootstrap.css');?>" rel="stylesheet">
        

      <script src="<?php echo base_url('tpl/js/bootstrap.min.js');?>"></script>
    <style>
        .notifyjs-corner{
            margin-top:53px;
        }
    </style>
    
  </body>
</html>