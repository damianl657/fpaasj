
<div class="jumbotron" style="background-color: #FFFFFF;">

  <div class="wrapper wrapper-content animated fadeInRight" id="general2">
    <form id="altatorneo" action="<?=site_url('torneos/crear_torneo')?>" method="post">
      <div class="form-group">
        <label for="nametorneo">Torneo</label>
        <input type="text" class="form-control" id="campo_nametorneo" name="campo_nametorneo" placeholder="Ingrese nombre de Torneo" required autocomplete="off">
      </div>
      <div class="form-group">
        <label for="organizador">Organizador de la competencia</label>
        <select class="form-control select2" name="campo_organizador" id="campo_organizador" required>
         <option value="0">Selecione el organizador</option>
         <?foreach ($club as $key){?>

          <option value="<?=$key->id?>"><?=$key->nombre?></option>

          <?}?>
        </select>
      </div>
      <div class="form-group">
        <label for="campo_clubesparticipante">Clubes a participar</label>
        <select multiple=multiple class="form-control select2" id="campo_clubesparticipante" name="campo_clubesparticipante[]" placeholder="ingrese Clubes a participar" required>
          <?foreach ($club as $key){?>
            <option value="<?=$key->id?>"><?=$key->nombre?></option>
            <?}?>
          </select>
        </div>
        <div class="row">
         <div class="col">
          <label for="fechacomienzo">Fecha de Comienzo</label>
          <input type="data" class="form-control calendario" name="fechacomienzo" placeholder="Fecha de alta del torneo para inscribirse" autocomplete="off" id="fechacomienzo">
        </div>
        <div class="col">
         <label for="fechafin">Fecha de Finalizaci&oacute;n</label>
         <input type="data" class="form-control calendario" name="fechafin" placeholder="Fecha de fin del torneo para inscribirse" autocomplete="off" id="fechafin" style="margin-bottom: 9px;">
       </div>  
     </div>
     <div class="form-group">
      <label for="campo_descripcion">Descripci&oacute;n del torneo</label>
      <textarea class="form-control" id="campo_descripcion" name="campo_descripcion" rows="3"  required></textarea>
    </div>

    <input type="button" id="btnguardar" value="Guardar" class="btn btn-success">
  </form>
  
</div>
</div>
<div class="jumbotron" style="background-color: #FFFFFF;">
  <h3 id="mostrarlista">Lista de torneos<small class="badge badge-primary">Click</small></h3>
  <div class=" table table-responsive">
  <table class="table table-striped" id="tablatorneo" width="100%">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nombre</th>
        <th scope="col">Organizador</th>
        <th scope="col">Descripci&oacute;n</th>
        <th scope="col">Inicio</th>
        <th scope="col">Fin</th>
        <th scope="col">Acciones</th>

      </tr>
    </thead>
    <tbody id="listatorneos"> 

    </tbody>
  </table>
    
  </div>
</div>




<!-- Modal -->
<div class="modal" id="modaleditartorneo" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Modificar torneo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="formeditar"  method="post">
        <input type="hidden" class="form-control" id="editar_idtorneo" name="editar_idtorneo">
          <div class="form-group">
            <label for="editar_nametorneo">Nombre Torneo</label>
            <input type="text" class="form-control" id="editar_campo_nametorneo" name="editar_campo_nametorneo" placeholder="Ingrese nombre de Torneo" required autocomplete="off">
          </div>
          <div class="form-group">
            <label for="editar_editar_organizador">Organizador de la competencia</label>
            <select class="form-control" name="editar_campo_organizador" id="editar_campo_organizador" required>
             <option value="0">Selecione el organizador</option>
             <?foreach ($club as $key){?>

              <option value="<?=$key->id?>"><?=$key->nombre?></option>

              <?}?>
            </select>
          </div>
          <div class="form-group">
            <label for="editar_campo_clubesparticipante">Clubes a participar</label>
            <select multiple=multiple class="form-control select2" id="editar_campo_clubesparticipante" name="editar_campo_clubesparticipante[]" placeholder="ingrese Clubes a participar" required>
              <?foreach ($club as $key){?>
                <option value="<?=$key->id?>"><?=$key->nombre?></option>
                <?}?>
              </select>
            </div>
            <div class="row">
             <div class="col">
              <label for="editar_fechacomienzo">Fecha de Comienzo</label>
              <input type="data" class="form-control calendario" name="editar_fechacomienzo" placeholder="Fecha de alta del torneo para inscribirse" autocomplete="off" id="editar_fechacomienzo">
            </div>
            <div class="col">
             <label for="edita_fechafin">Fecha de Finalizacion</label>
             <input type="data" class="form-control calendario" placeholder="Fecha de fin del torneo para inscribirse" autocomplete="off" id="editar_fechafin" name="editar_fechafin" style="margin-bottom: 9px;">
           </div>  
         </div>
         <div class="form-group">
          <label for="editar_campo_descripcion">Descripci&oacute;n del torneo</label>
          <textarea class="form-control" id="editar_campo_descripcion" name="editar_campo_descripcion" rows="3"  required></textarea>
        </div>

      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <input type="button" id="btn_editar" value="Guardar" class="btn btn-success" data-dismiss="modal">
      
    </div>
  </div>
</div>
</div>

<div class="modal" id="modaleliminar" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Eliminar torneo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <input type="hidden" class="form-control" id="eliminar_idtorneo" name="eliminar_idtorneo">
        <div class="alert alert-danger" role="alert" align="center">
         <h3> SEGURO QUIERE ELIMINAR EL TORNEO</h3>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btn_eliminar" class="btn btn-success" data-dismiss="modal">Eliminar</button>
      
      </div>
  </div>
</div>
</div>

<div class="modal" id="modallistarclubes" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Clubes inscriptos al torneo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <table class="table table-striped">
          <thead>
             <tr>
              <th scope="col">ID</th>
              <th scope="col">club</th>
              
            </tr>
          </thead>
          <tbody id="campo_torneos">
            
          </tbody>
         </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btn_eliminar" class="btn btn-success" data-dismiss="modal">Eliminar</button>
      
      </div>
  </div>
</div>
</div>

<script>

   $(function () {
                $('.calendario').datetimepicker({
                format:'d-m-Y',
                step:30,
                lang:'es',
                language: "es"
                });
            });
   function convertDatenumero(string) {
  var info = string.split('-');
  var fecha= info[2] + '-' + info[1] + '-' + info[0];
  
  return fecha;
}
  function guardartorneo(){
    var url ='<?=base_url("/torneos/crear_torneo") ?>';    

    $.ajax({
      url: url,
      type: 'POST',
      data: $("#altatorneo").serialize(),
      beforeSend: function () {
        $("#btnguardar").css('display', 'none');
        $.notify("Guardando Torneo","info");
      },
      success: function(data){
        console.log(data);
        $("#campo_nametorneo").val("");
        $("#campo_organizador").val(0);
        $("#campo_clubesparticipante").empty() ;
        $("#campo_descripcion").val("");
        $.notify("Se guardo el torneo","success");
        $("#btnguardar").css('display', 'block');
        $("#altatorneo").empty();
      }
    }) 


  }
  function listarclubxtorneo(torneo){
      var url ='<?=base_url("/torneos/obtener_club_x_torneo") ?>';    

      $.ajax({
        url: url,
        type: 'POST',
        data: {tor: torneo},
        success: function(data){
          //alert(data);
          data= JSON.parse(data);
          console.log(data); 
                  
          $('#campo_torneos').html("");
          $.each(data, function(index, val) {                       
                          
                         
                          $('#campo_torneos').append('<tr><th>'+val.id+'</th> <td>'+val.nombre+'</td></tr>');
                       });  
        }
      }) 
      
      
  }
  function listartorneo(){
    var url ='<?=base_url("/torneos/obtener_torneos") ?>';    

    $.ajax({
      url: url,
      type: 'POST',

      beforeSend: function () {

        $.notify("Listando torneos","info");
      },
      success: function(data){
        data= JSON.parse(data);
        console.log(data);
        $("#listatorneos").html("");
        $.each(data, function(index, val) {
         $("#listatorneos").append(
          '<tr>'
          +'<th class="badge badge-primary scope="row">'+val.id+'</th><td>'+val.nombre+'</td><td>'+val.institucion+'</td><td>'+val.descripcion+'</td><td>'+convertDatenumero(val.inicio)+'</td><td>'+convertDatenumero(val.fin)+'</td>'
          +'<td>'
          +'<div class="btn-group" role="group" aria-label="Basic example">'
          +'<button type="button" class="btn btn-outline-warning" data-toggle="modal" onclick="listarclubxtorneo('+val.id+')" data-target="#modallistarclubes">Listar</button>'
          +'<button type="button" class="btn btn-outline-success" data-toggle="modal" onclick="buscar_torneo('+val.id+')" data-target="#modaleditartorneo">Editar</button>'
          +'<button type="button" class="btn btn-outline-danger" data-toggle="modal" onclick="id_torneo_eliminar('+val.id+')" data-target="#modaleliminar">Eliminar</button>'
          +'</div>'
          +'</td>'
          +'</tr>'
          );
       });
        
      }
    }) 


  }

  function eliminar_torneo(id){
    var url ='<?=base_url("/torneos/eliminar_torneo") ?>';    

    $.ajax({
      url: url,
      type: 'POST',
      data: {torneo:id},
      beforeSend: function () {
        
        $.notify(" Procesando....","info");
      },
      success: function(data){
        $.notify("Torneo Eliminando","info");
         $('#listatorneos').empty();

           listartorneo();
      }
    }) ;

  }
function id_torneo_eliminar(id){
  $("#eliminar_idtorneo").val(id); 
 }
  function buscar_torneo(id){

   var url ='<?=base_url("/torneos/buscar_torneo") ?>';    

   $.ajax({
    url: url,
    type: 'POST',
    data: {tor:id},

    success: function(data){

      data= JSON.parse(data);
      console.log(data);                   

      $.each(data, function(index, val) {
       
        $("#editar_campo_organizador").val(val.organizador);
       // $('#editar_campo_organizador').find(':selected').data('val.organizador');
        $("#editar_idtorneo").val(val.id);
        $("#editar_campo_nametorneo").val(val.nombre);
        $("#editar_campo_descripcion").val(val.descripcion);
        $("#editar_fechacomienzo").val(convertDatenumero(val.inicio));
        $("#editar_fechafin").val(convertDatenumero(val.fin));
      });
    }
  }) 
 }
  function update_torneo(idtorneo,tnombre,torganizador,tfechaini,tfechafin,tdescripcion){
     var url ='<?=base_url("/torneos/editar_torneo")?>';  

      
      $.ajax({
        url: url,
        type: 'POST',
        data:$("#formeditar").serialize(),
         beforeSend: function() 
          {
            
            $.notify("Prosesando...","info");
           
          },
        success: function(data){


           $.notify("Guardado con exito....","success");
           $('#editar_campo_clubesparticipante').val();
           $('#listatorneos').empty();
          // window.location="<?=base_url("/Torneos")?>";
          
           listartorneo();

        }
      }) 
  }

   $(document).ready(function(){


    $("#tablatorneo").hide();
    $('#campo_organizador').select2();
    //$('#editar_campo_organizador').select2();
    $("#modalmodificar").modal('hide');
    $('#campo_clubesparticipante').select2();
    $('#editar_campo_clubesparticipante').select2();
    $('#btnguardar').click(function(){
     guardartorneo();
     listartorneo();
   });
    $('#btn_editar').click(function(){
      var idtorneo= $("#editar_idtorneo").val();
      var nomtorneo=  $("#editar_campo_nametorneo").val();
      var torganizador= $("#editar_campo_nametorneo").val();
      var tfechafin= $("#editar_fechafin").val();
      var tfechaini= $("#editar_fechacomienzo").val();
      var tdescripcion= $("#editar_campo_descripcion").val();

    update_torneo(idtorneo,nomtorneo,torganizador,tfechaini,tfechafin,tdescripcion);

   }); 

    $("#mostrarlista").click(function()
    {
      $("#tablatorneo").toggle()
    });
     $("#btn_eliminar").click(function()
    { 
       var id =$("#eliminar_idtorneo").val();    
      eliminar_torneo(id);
    });   
    listartorneo();
  });  

</script>
<div>