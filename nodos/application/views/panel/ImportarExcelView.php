
<link href="<?=base_url('assets/css/fullcalendar/fullcalendar.css');?>" rel="stylesheet">
<link href="<?=base_url('assets/css/fullcalendar/fullcalendar.print.css');?>" rel="stylesheet"  media='print'>

<script src="<?=base_url('assets/js/fullcalendar/moment.min.js');?>"></script>
<!--<script src="<?=base_url('assets/js/fullcalendar/jquery.min.js');?>"></script>-->
<script src="<?=base_url('assets/js/fullcalendar/fullcalendar.min.js');?>"></script>
<script src="<?=base_url('assets/js/fullcalendar/lang/es.js');?>"></script>



<script type="text/javascript">

var passApiKey = '<?php echo $passApiKey; ?>';
var urlApi = '<?php echo $urlApi; ?>';

var idUser = '<?php echo $idusuario; ?>';
var idcolegio = '<?php echo $idcolegio; ?>';
var idusergrupo = '<?php echo $idusergrupo; ?>';

//alert(idUser);
//alert(idcolegio);

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yy = today.getFullYear();
today = yy+'-'+mm+'-'+dd;


function traer_niveles()
{
      var url = urlApi+"/nivel/obtener_NivEspcargadas";   
      
      $.ajax({
                  url: url,
                  type: 'GET',
                  headers: {              
                'APIKEY' : passApiKey,
                'userid' : idUser
            },
            data:{idcolegio:idcolegio},
            success: function(data){  
              if(data['status']==false)
              { 
                //console.log(data['message']);
                if(data['message']=='NO LOGUIN')
                  location.href = "<?php echo base_url('login'); ?>"; //no està logueado
                //terminadoP4(1); terminadoP4(2); terminadoP4(3);
              }
              else
              { 
                var data = jQuery.parseJSON( data );        
                //console.log(data);             
                var selecMat = "<select id='nivel' name='nivel' lass='form-control input-md' multiple >";

                for(var i=0; i < data.length ; i++) //aca solo listo niveles y especialidades
                {                                                                   
                    if(data[i]['esp_id'])               
                    {
                      var nombre_nivel = data[i]['nombre_nivel']+ ": &nbsp<span>"+data[i]['nombre_especialidad']+"</span>";   
                      selecMat = selecMat + "<option value='"+data[i]['nodonivel_id']+"'>" +nombre_nivel+ "</option>" ;
                    }
                    else
                    {
                      var nombre_nivel = data[i]['nombre_nivel'];
                      selecMat = selecMat + "<option value='"+data[i]['nodonivel_id']+"'>" +nombre_nivel+ "</option>" ;
                    }                   
                }

                selecMat = selecMat + "</select>";

                $('#div_select_nivel ').append(selecMat);
      
             }                             
          },
          error: function(response){
              console.log(response);                   
          }           
      }); 
}

function traer_anios(){

  var spanes_nivel = $('#div_select_nivel').find('span');
  //var Anios = Array();

   $('#div_select_anio').find('select').remove('option');
  
  var contAjax = 0;
  for (var i=0; i < spanes_nivel.length ; i++) //
  {   
      var idnodonivel = $(spanes_nivel[i]).attr('id');
      //var nombrenodonivel = $(spanes_nivel[i]).text();

      /*var busq = idnodonivel.indexOf("_");
      var arre=Array();
      if(busq!=-1) //lo encontró
      {
          arre = idnodonivel.split('_');
      }  
      else arre = Array(idnodonivel, -1); */

      var url = urlApi+"/anio/obtener_aniosBynodoniv";    

      $.ajax({
                url: url,
                type: 'GET',
                headers: {              
              'APIKEY' : passApiKey,
              'userid' : idUser
          },
          data: {idnodonivel:idnodonivel},
          success: function(data){ //console.log(data)
                        
              if(data['status']==false)
                {
                  console.log(data['message']);
                  if(data['message']=='NO LOGUIN')
                    location.href = "<?php echo base_url('login'); ?>"; //no està logueado    

                  if(fin == 1)
                  { 
                      terminadoP4(2); //terminadoP4(3); 
                  }
                }                       
                else
                {
                  var data = jQuery.parseJSON( data ); 
                                     
                  for(var i=0; i < data.length; i++) //aca solo listo niveles y especialidades
                  {                           
                      /*$options = $('#div_select_anio').find('option');
                      $targetOption = $options.filter(
                         function () {return jQuery(this).text() == data[i]['nombre']}
                      );//esto es para buscar un option por text. manso lio pero no hay otra.                         
                      if($targetOption.length == 0)
                      {  
                          var nombre=data[i]['nombre'];
                      }
                      else var nombre=data[i]['nombre'] +" - "+ nombrenodonivel;*/

                      var nombrenodonivel = $(spanes_nivel[contAjax]).text(); 

                      var nombre=data[i]['nombre'] +" - "+ nombrenodonivel;

                      var opcion = "<option value='"+data[i]['id']+"'>" +nombre+ "</option>" ; //anio_id 
                      $('#div_select_anio').find('select').append(opcion); 
                  }


                  $('#anio option').click(function(){       
                      var idnodoanio = $(this).val();
                      var nombanio = $(this).text();
                                 
                      var td = $(this).parent('select').parent('div').parent('td');
                      var div = $(this).parent('select').parent('div');
                      var existe= $(td).find('[id="'+idnodoanio+'"]');  //busco para que no agergue repetido
                      if(existe.length < 1)
                      {
                          var texto = "&nbsp;<span id='"+idnodoanio+"'>"+nombanio+"<a class='close2'> </a> </span>";
                          $(div).append(texto);

                          $('.close2').click(function(e) {
                            $(this).parent('span').remove();  
                          });
                      }  
                      //else alert('ya està');                                
                  }); 
                                                
                }// fin else
                  
                contAjax++;         
              },  // fin success         
              error: function(response){
                  console.log(response); 
                  contAjax++;                     
              }
          
       });  
  
    //Anios.push( fila );

  }
}


function traer_divisiones(){

  var spanes_anio = $('#div_select_anio').find('span');

  //var Anios = Array();

  $('#div_select_divi').find('select').remove('option');
  
  var contAjax2=0;
  for (var i=0; i < spanes_anio.length; i++)
  {   
      var idnodoanio = $(spanes_anio[i]).attr('id');
 
      
      var url = urlApi+"/division/divisiones_cargadas";

      $.ajax({
                  url: url,
                  type: 'GET',
                  headers: {              
                'APIKEY' : passApiKey,
                'userid' : idUser
            },
            data:{idanio:idnodoanio},
            success: function(data){  //console.log(data);  
              
                if(data['status']==false)                         
                  {               
                  if(data['message'] == 'NO LOGUIN')                                        
                       location.href = "<?php echo base_url('login'); ?>";
              }
              else
              {
                  var data = jQuery.parseJSON( data );        

                  for(var i=0; i < data.length; i++) //aca solo listo niveles y especialidades
                  {                           
                      /*$options = $('#div_select_divi').find('option');

                      $targetOption = $options.filter(
                         function () {return jQuery(this).text() == data[i]['nombre']}
                      );//esto es para buscar un option por text. mucho lio pero no hay otra.

                      if($targetOption.length == 0)
                      {  
                        var opcion = "<option value='"+data[i]['id']+"'>" +data[i]['nombre']+ "</option>" ; //anio_id 
                        $('#div_select_divi').find('select').append(opcion);   
                      }*/
                      var nombrenodoanio = $(spanes_anio[contAjax2]).text(); 
                      var nombre=data[i]['nombre'] +" - "+ nombrenodoanio;

                      var opcion = "<option value='"+data[i]['id']+"'>" +nombre+ "</option>" ; //anio_id 
                      $('#div_select_divi').find('select').append(opcion);   
                  }


                  $('#divi option').click(function(){       
                      var iddiv = $(this).val();
                      var nombdiv = $(this).text();
                                 
                      var td = $(this).parent('select').parent('div').parent('td');
                      var div = $(this).parent('select').parent('div');
                      var existe= $(td).find('[id="'+iddiv+'"]');  //busco para que no agergue repetido
                      if(existe.length < 1)
                      {
                          var texto = "&nbsp;<span id='"+iddiv+"'>"+nombdiv+"<a class='close2'> </a> </span>";
                          $(div).append(texto);

                          $('.close2').click(function(e) {
                            $(this).parent('span').remove();  
                          });
                      }  
                      //else alert('ya està');                                
                  }); 
              
              }     
              contAjax2++;            
          },
                error: function(response){
                    console.log(response);
                    contAjax2++; 
                }
            
         });           
  }
}


function traer_usuarios()
{
      var url = urlApi+"/usuario/obtener_usuarios";   
      
      $.ajax({
                  url: url,
                  type: 'GET',
                  headers: {              
                'APIKEY' : passApiKey,
                'userid' : idUser
            },
            data:{idcolegio:idcolegio},
            success: function(data){  
              if(data['status']==false)
              { 
                //console.log(data['message']);
                if(data['message']=='NO LOGUIN')
                  location.href = "<?php echo base_url('login'); ?>"; //no està logueado
                //terminadoP4(1); terminadoP4(2); terminadoP4(3);
              }
              else
              { 
                    var data = jQuery.parseJSON( data ); 
                                       
                    for(var i=0; i < data.length; i++) //aca solo listo usuarios
                    {                           
                        $options = $('#destinatarios').find('option');

                        $targetOption = $options.filter(
                           function () {return jQuery(this).val() == data[i]['user_id']}
                        );//esto es para buscar un option por value. manso lio pero no hay otra. para que no agregue repetido

                        if($targetOption.length == 0)
                        {  
                          var nombre = data[i]['first_name'] +" "+ data[i]['last_name'];
                          var opcion = "<option value='"+data[i]['user_id']+"'>" +nombre+ "</option>" ; //anio_id 
                          $('#destinatarios').append(opcion);   
                        }
                    }


                    $('#destinatarios option').click(function(){       
                        var usuarioid = $(this).val();
                        var usuarionomb = $(this).text();
                                                  
                        var div = $(this).parent('select').parent('div');
                        var existe= $(div).find('[id="'+usuarioid+'"]');  //busco para que no agergue repetido
                        if(existe.length < 1)
                        {
                            var texto = "&nbsp;<span id='"+usuarioid+"'>"+usuarionomb+"<a class='close2'> </a> </span>";
                            $(div).append(texto);

                            $('.close2').click(function(e) {
                              $(this).parent('span').remove();  
                            });
                        }  
                        //else alert('ya està');                                
                    }); 
             }                             
          },
          error: function(response){
              console.log(response);                   
          }           
      }); 
}


//FUNCTION PARA INSERTAR UN NUEVO EVENTO
function guardar_evento(newE){ //alert('llega'); console.log(newE);
   
    var parametros = Array();   
    if( $('#ftodos').is(':checked') )
        var filtro = 't'; //t = todos
    else if( $('#fpersonalizado').is(':checked') )
          {
              var filtro = 'p'; //p = personalizado
              
              var arreniveles=Array();
              var spanes_nivel = $('#div_select_nivel').find('span');             
              for (var i=0; i < spanes_nivel.length ; i++) //
              {   
                  var idnodonivel = $(spanes_nivel[i]).attr('id');
                 
                  /*var busq = idnodonivel.indexOf("_");
                  var arre=Array();
                  if(busq!=-1) //lo encontró
                  {
                      arre = idnodonivel.split('_');
                  }  
                  else arre = Array(idnodonivel, -1); */

                  /*var arreaux = {
                        'seleccion': 'n',
                        'id': idnodonivel
                  };*/
                  arreniveles.push(idnodonivel);
              }

              var arreanios=Array();
              var spanes_anio = $('#div_select_anio').find('span');             
              for (var i=0; i < spanes_anio.length ; i++) //
              {   
                  var idnodoanio = $(spanes_anio[i]).attr('id');
                           
                  /*var arreaux = {
                        'seleccion': 'a',
                        'id': idnodoanio
                  };*/
                  arreanios.push(idnodoanio);
              }

              var arredivi=Array();
              var spanes_divi= $('#div_select_divi').find('span');             
              for (var i=0; i < spanes_divi.length ; i++) //
              {   
                  var iddiv = $(spanes_divi[i]).attr('id');
                           
                  /*var arreaux = {
                        'seleccion': 'd',
                        'id': iddiv
                  };*/
                  arredivi.push(iddiv);
              }

              parametros = Array(arredivi, arreanios, arreniveles);

          }//....personalizado
          else if( $('#fdirijido').is(':checked') )
                {
                    var filtro = 'd'; //d = dirijido

                    var spanes = $('#destinatarios').parent('div').find('span');
                    if( $(spanes).length > 0 )
                    {
                       $(spanes).each(function (index2){ 
                          var idusu = $(this).attr('id');                         
                          parametros.push( idusu );
                        });
                    }  
                }

    var stringJson = JSON.stringify(parametros);

    console.log(stringJson);
    
    var urlRequest = urlApi+"/evento/evento";

    $.ajax({
          url: urlRequest,
          type: 'POST',
          headers: {    
          'APIKEY' : passApiKey,
          'userid' : idUser
        },
        data: {data: newE, filtro: filtro, idcolegio:idcolegio, parametros:stringJson}, 
       
        success: function(data){
               
                console.log(data);
              if(data['status']==false)
              {  //alert(data['message']);
                  if(data['message'] == 'NO LOGUIN')                                        
                      location.href = "<?php echo base_url('login'); ?>";   
                  
              }
              else{                                 
                      /* $('.top-right').notify({
                          message: { text: 'Guardado' },
                          fadeOut: { enabled: true, delay: 5000 },
                          type: 'success2'
                      }).show();  */
                  }           
        },

        error: function(response){
            console.log(response);               
          }

    });
} 

//FUNCION PARA ABRIR UN POPUP PARA EL ALTA DE EVENTO
function abrir_dialog(start,end)
{ 
  
  var inicio = new Date(start);
  var dd = (inicio.getDate()+1).toString(); //no se si està bien sumarle 1
  var mm = (inicio.getMonth()+1).toString(); //January is 0!
  var yyyy = inicio.getFullYear();
  if (dd.length < 2) {
      dd = "0"+dd;
    }
 
  if (mm.length < 2) {
    mm = "0"+mm;
  }
  var fechaI=yyyy+'-'+mm+'-'+dd;


  var hh = ( (inicio.getHours()+3)%24 ).toString();
  var min = (inicio.getMinutes()).toString() ;
  if (hh.length < 2) {
      hh = "0"+hh;
    }
  if (min.length < 2) {
    min = "0"+min;
  }
  var horaI =  hh+":"+mm;

  var fechayhora = fechaI+' '+horaI;

  bootbox.dialog({
              backdrop: true,
              title: "Nuevo Evento...",
              message: '<div class="row">  ' +
                  '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12"> ' +
                  '<form id="Nevento" name="Nevento" class="form-horizontal" method="post"> ' +

                    '<div > <table class="pasos"><tr><td>'+
                      '<div class="form-group"> ' +
                        '<label class="col-md-4 control-label" for="name">Titulo</label> ' +
                        '<div class="col-md-4"> ' +
                          '<input id="titulo" name="titulo" type="text" placeholder="Titulo..." lass="form-control input-md" required /> *' +
                        '</div> ' +                      
                      '</div> ' +

                      '<div class="form-group"> ' +
                        '<label class="col-md-4 control-label" for="confirm">Confirmar</label> ' +
                        '<div class="col-md-4"> ' +
                          '<input id="confirm" name="confirm" type="checkbox" lass="form-control input-md" /> ' +
                        '</div> ' +                      
                      '</div> ' +

                      '<div class="form-group"> ' +
                        '<label class="col-md-4 control-label" for="desc">Descripción</label> ' +
                        '<div class="col-md-4"> ' +
                          '<textarea id="desc" name="desc" lass="form-control input-md" /> ' +
                        '</div> ' +                      
                      '</div> ' +
                  
                      '<div class="form-group"> ' +
                        '<label class="col-md-4 control-label" for="lugar">Lugar</label> ' +
                        '<div class="col-md-4"> ' +
                          '<input id="lugar" name="lugar" type="text" lass="form-control input-md" required> *' +
                        '</div> ' +                      
                      '</div> ' +

                    '</div>'+ 

                    '<div >'+

                      '<div class="form-group">' +
                        '<label class="col-md-4 control-label" for="fechaI">Inicio</label> ' +
                        '<div class="col-md-4"> ' +
                          '<input id="fechaI" name="fechaI" type="date" placeholder="aaaa-mm-dd" lass="form-control input-md" required value="'+fechaI+'"/> *' +
                        '</div> ' +                      
                        '<div class="col-md-4" id="divH1"> ' +
                          '<input id="horaI" name="horaI" type="time" placeholder="hh:mm" lass="form-control input-md"  required  value="'+horaI+'" /> *' +                  
                        '</div> ' +                      
                      '</div> ' +

                      '<div class="form-group"> ' +
                        '<label class="col-md-4 control-label" for="fechaF">Fin</label> ' +
                        '<div class="col-md-4"> ' +
                          '<input id="fechaF" name="fechaF" type="date" lass="form-control input-md" /> ' +
                        '</div> ' + 
                        '<div class="col-md-4" id="divH2"> ' +
                          '<input id="horaF" name="horaF" type="time" placeholder="hh:mm" lass="form-control input-md"  /> *' +
                        '</div> ' +                       
                      '</div> ' + 

                    '</td></tr></table></div>'+ 

                    //FILTRO
                    '<div style="border-top: 1px solid #e5e5e5;">  </div>'+

                    '<div class="form-group"> ' +
                      '<table><tr>'+
                        '<td><label class="" for="ftodos">Todo el Colegio</label> ' +                  
                          '<input id="ftodos" name="radio_filtro" type="radio" lass="form-control input-md" checked> </td>' +                    
                        '<td><label class="" for="fpersonalizado">Personalizado</label> ' +  
                          '<input id="fpersonalizado" name="radio_filtro" type="radio" lass="form-control input-md"> </td> ' +                      
                        '<td><label class="" for="fdirijido">Dirijido</label> ' +  
                          '<input id="fdirijido" name="radio_filtro" type="radio" lass="form-control input-md"> </td> ' +                                      
                      '</tr></table>'+
                    '</div> ' +

                    '<div class="form-group" id="un_destino" style="display:none"> ' +
                      '<table class="pasos"><tr><td>'+                   
                          '<label class="col-md-4 control-label" for="destinatarios">Destinatario</label> ' +
                          '<div class="col-md-4"> ' +
                            '<select id="destinatarios" name="destinatarios" lass="form-control input-md" multiple/> *' +
                          '</div>'+ 
                    '</td></tr></table></div>'+  

                    '<div id="varios_destinos" style="display:none">'+
                      '<table class="pasos">'+
                        '<tr>' + //<div class="form-group" > 
                          '<td><label class="col-md-4 control-label" >Nivel</label></td>'+
                          '<td><input id="fnivel" name="fnivel" type="checkbox" lass="form-control input-md" checked/></td>'+ 
                          '<td><div class="" id="div_select_nivel"> ' +
                            //'<select id="nivel" name="nivel" lass="form-control input-md" required /> *' +
                          '</div></td>' +                                                      
                        '</tr> ' + //</div>

                        '<tr> ' +  //<div class="form-group" >
                          '<td><label class="col-md-4 control-label" for="anio">A&ntilde;o</label></td> ' +
                          '<td><input id="fanio" name="fnivel" type="checkbox" lass="form-control input-md" > </td>' +
                          '<td><div class="col-md-4" id="div_select_anio"> ' +
                            '<select id="anio" name="anio" lass="form-control input-md" multiple  /> *' +
                          '</div></td> ' + 
                        '</tr> ' + //</div>

                        '<tr> ' + //<div class="form-group" >
                          '<td><label class="col-md-4 control-label" for="divi">Divisi&oacute;n</label> </td>' +
                          '<td><input id="fdivi" name="fnivel" type="checkbox" lass="form-control input-md" > </td>' +
                          '<td><div class="col-md-4" id="div_select_divi"> ' +
                            '<select id="divi" name="divi" lass="form-control input-md" multiple  /> *' +
                          '</div></td> ' + 
                        '</tr>' +  //</div> 
                    '</table></div>'+  
                                                                                        
                  '</form>'+
                  ' </div>  </div>',
              buttons: {
                  success: {
                      label: "Guardar",
                      className: "btn-success",
                      callback: function (e) {  

                          var $myForm = $('#Nevento');
                          if ($myForm[0].checkValidity()) 
                          {
                            var titulo = $('#titulo').val();
                            var confirmar = $('#confirm').is(':checked');
                            var descripcion = $('#desc').val();
                            var lugar = $('#lugar').val();
                            var fechaf = $('#fechaF').val();
                            var horaf = $('#horaF').val();
                            var fechai = $('#fechaI').val();
                            var horai = $('#horaI').val();

                            var parametros = {
                              'titulo': titulo,
                              'confirm': confirmar,     
                              'descripcion': descripcion,     
                              'lugar': lugar,
                              'fechainicio':  fechai+' '+horai,
                              'fechafin':  fechaf+' '+horaf
                            };                                            
                            var stringJson = JSON.stringify(parametros);
                            guardar_evento(stringJson);

                            //acá agrego al calendario el nuevo evento
                            var eventData;
                            if (titulo) {
                              eventData = {
                                title: titulo,
                                start:  fechai+' '+horai,
                                lugar: lugar
                                //end: end
                              };
                              $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
                            }
                            $('#calendar').fullCalendar('unselect');
                          }
                          else {alert('Ingrese los campos obligatorios');
                                return false;
                                }
                                                                 
                      }//fin calback
                  }//fin success
              },//fin Buttons

              onEscape: function() {return ;},
        });  //FIN DIALOG

   traer_niveles();


    $('#fpersonalizado').click(function(){
          $('#un_destino').css('display','none');
          $('#varios_destinos').css('display','block');

          //$('#nivel').attr('disabled',true);
          $('#anio').attr('disabled',true);
          $('#divi').attr('disabled',true);


          $('#fnivel').click(function(){ 
            if($(this).is(':checked'))
            { 
                  $('#nivel').attr('disabled',false);  

                  /*Al hacer click en un option del SELECT NIVEL*/
                  $('#nivel option').click(function(){       
                    var idniv = $(this).val();
                    var nombniv = $(this).text();
                               
                    var td = $(this).parent('select').parent('div').parent('td');
                    var div = $(this).parent('select').parent('div');
                    var existe= $(td).find('[id="'+idniv+'"]');  //busco para que no agergue repetido
                    if(existe.length < 1)
                    {
                        var texto = "&nbsp;<span id='"+idniv+"'>"+nombniv+"<a class='close2'> </a> </span>";
                        $(div).append(texto);

                        $('.close2').click(function(e) {
                          $(this).parent('span').remove();  
                        });
                    }  
                    //else alert('ya està');                                
                });
            }
            else
            {
                $('#nivel').attr('disabled',true);
                $(this).parent('td').parent('tr').find('span').remove();                       
            }

          });

          $('#fanio').click(function(){
              if($(this).is(':checked'))
              {
                  traer_anios();
                  $('#anio').attr('disabled',false);                
              }
              else
              {
                  $('#anio').attr('disabled',true);
                  $(this).parent('td').parent('tr').find('option').remove();
                  $(this).parent('td').parent('tr').find('span').remove();
              }
            });

          $('#fdivi').click(function(){
              if($(this).is(':checked'))
              { 
                  traer_divisiones();
                  $('#divi').attr('disabled',false);  
              }
              else
              {
                  $('#divi').attr('disabled',true);
                  $(this).parent('td').parent('tr').find('option').remove();
                  $(this).parent('td').parent('tr').find('span').remove();
              }
          });
         
    });

    $('#fdirijido').click(function(){
        $('#un_destino').css('display','block');
        $('#varios_destinos').css('display','none');

         traer_usuarios();
    });

    $('#ftodos').click(function(){
        $('#un_destino').css('display','none');
        $('#varios_destinos').css('display','none');
    }); 
}

//FUNCION PARA ABRIR UN POPUP PARA EL DETALLE DE EVENTO
function abrir_dialogDetalle(evento)
{ 
  var titulo=evento.title; 
  var confirm=evento.confirm; 
  var lugar=evento.lugar; 
  var creadorid=evento.creadorid;
  var creador=evento.creador;
  var inicio=Array();
  inicio = (evento.inicio).split(" "); 
  var fin=Array();
  if(evento.fin!=null)
  {  
    fin = (evento.fin).split(" ");
  } 
  else 
    {
      fin[0]='';  fin[1]='';
    }
  var alta=evento.fecha_alta;
  var desc=evento.descripcion;


  
  bootbox.dialog({
              backdrop: true,
              title: "Detalle del Evento...",
              message: '<div class="row">  ' +
                  '<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12"> ' +
                  '<form id="Nevento" name="Nevento" class="form-horizontal" method="post"> ' +

                    '<div > <table class="pasos"><tr><td>'+
                      '<div class="form-group"> ' +
                        '<label class="col-md-4 control-label" for="name">Titulo</label> ' +
                        '<div class="col-md-4"> ' +
                          '<input id="titulo" name="titulo" type="text" placeholder="Titulo..." lass="form-control input-md" required value="'+titulo+'"/> *' +
                        '</div> ' +                      
                      '</div> ' +

                      '<div class="form-group"> ' +
                        '<label class="col-md-4 control-label" for="confirm">Confirmar</label> ' +
                        '<div class="col-md-4"> ' +
                          '<input id="confirm" name="confirm" type="checkbox" lass="form-control input-md" /> ' +
                        '</div> ' +                      
                      '</div> ' +

                      '<div class="form-group"> ' +
                        '<label class="col-md-4 control-label" for="desc">Descripción</label> ' +
                        '<div class="col-md-4"> ' +
                          '<textarea id="desc" name="desc" lass="form-control input-md"> '+desc+' </textarea>' +
                        '</div> ' +                      
                      '</div> ' +
                  
                      '<div class="form-group"> ' +
                        '<label class="col-md-4 control-label" for="lugar">Lugar</label> ' +
                        '<div class="col-md-4"> ' +
                          '<input id="lugar" name="lugar" type="text" lass="form-control input-md" required value="'+lugar+'" /> *' +
                        '</div> ' +                      
                      '</div> ' +

                    '</div>'+ 

                    '<div >'+

                      '<div class="form-group">' +
                        '<label class="col-md-4 control-label" for="fechaI">Inicio</label> ' +
                        '<div class="col-md-4"> ' +
                          '<input id="fechaI" name="fechaI" type="date" placeholder="aaaa-mm-dd" lass="form-control input-md" required value="'+inicio[0]+'"/> *' +
                        '</div> ' +                      
                        '<div class="col-md-4" id="divH1"> ' +
                          '<input id="horaI" name="horaI" type="time" placeholder="hh:mm" lass="form-control input-md"  required  value="'+inicio[1]+'" /> *' +                  
                        '</div> ' +                      
                      '</div> ' +

                      '<div class="form-group"> ' +
                        '<label class="col-md-4 control-label" for="fechaF">Fin</label> ' +
                        '<div class="col-md-4"> ' +
                          '<input id="fechaF" name="fechaF" type="date" lass="form-control input-md" value="'+fin[0]+'"/> ' +
                        '</div> ' + 
                        '<div class="col-md-4" id="divH2"> ' +
                          '<input id="horaF" name="horaF" type="time" placeholder="hh:mm" lass="form-control input-md"  value="'+fin[1]+'"/> ' +
                        '</div> ' +                       
                      '</div> ' + 

                      '<div class="form-group"> ' +
                        '<label class="col-md-4 control-label" for="creador">Creador / Fecha</label> ' +
                        '<div class="col-md-4"> ' +
                          '<input id="creador" name="creador" type="text" lass="form-control input-md" required value="'+creador+'" disabled/> ' +
                        '</div> ' +  
                        '<div class="col-md-4"> ' +
                          '<input id="f_alta" name="f_alta" type="text" lass="form-control input-md" required value="'+alta+'" disabled/> ' +
                        '</div> ' +                     
                      '</div> ' +

                    '</td></tr></table></div>'+ 
                                                                                                        
                  '</form>'+
                  ' </div>  </div>',
              buttons: {
                  success: {
                      label: "Guardar",
                      className: "btn-success",
                      callback: function (e) {  

                          var $myForm = $('#Nevento');
                          if ($myForm[0].checkValidity()) 
                          {
                            var titulo = $('#titulo').val();
                            var confirmar = $('#confirm').is(':checked');
                            var descripcion = $('#desc').val();
                            var lugar = $('#lugar').val();
                            var fechaf = $('#fechaF').val();
                            var horaf = $('#horaF').val();
                            var fechai = $('#fechaI').val();
                            var horai = $('#horaI').val();

                            var parametros = {
                              'titulo': titulo,
                              'confirm': confirmar,     
                              'descripcion': descripcion,     
                              'lugar': lugar,
                              'fechainicio':  fechai+' '+horai,
                              'fechafin':  fechaf+' '+horaf
                            };                                            
                            var stringJson = JSON.stringify(parametros);
                            guardar_evento(stringJson);

                            //acá agrego al calendario el nuevo evento
                            var eventData;
                            if (titulo) {
                              eventData = {
                                title: titulo,
                                start:  fechai+' '+horai,
                                lugar: lugar
                                //end: end
                              };
                              $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
                            }
                            $('#calendar').fullCalendar('unselect');
                          }
                          else {alert('Ingrese los campos obligatorios');
                                return false;
                                }
                                                                 
                      }//fin calback
                  }//fin success
              },//fin Buttons

              onEscape: function() {return ;},
        });  //FIN DIALOG

    if(confirm==1)
    {  
      $("#confirm").prop('checked', true);
    //else var confirm='';
  }

   
}

function hjkhj(){

 }


function traer_eventos(){   
        
        var urlRequest = urlApi+"/evento/eventosTodos";                           
    
        $.ajax({
              url: urlRequest,
              type: 'GET',
              headers: {              
               'APIKEY' : passApiKey,
               'userid' : idUser
        },
        data: {},
        beforeSend: function() {
            $('.container').append("<img id='loadingNiv' src='<?=base_url('assets/images/loading.gif');?>' height='50' width='50' />");
        },
        success: function(data)
        {  
         
          if(data['status']==false)
          { 
            if(data['message']=='NO LOGUIN')
              location.href = "<?php echo base_url('login'); ?>"; //no està logueado
          }
          else
            {
               var datos = jQuery.parseJSON(data);
               console.log(datos); 

              var eventosArre = Array();
              for(var i=0; i < datos.length ; i++) //aca solo listo niveles y especialidades
              { 
                  var elemento = {
                      'id': datos[i]['eventid'],
                      'title': datos[i]['titulo'],
                      'start': datos[i]['fechaInicio'],
                      'start': datos[i]['fechaInicio'],
                      'inicio': datos[i]['fechaInicio'],
                      'end': datos[i]['fechafin'],
                      'fin': datos[i]['fechafin'],
                      'fecha_alta': datos[i]['fechaCreacion'],
                      'confirm':  datos[i]['confirm'],   
                      'lugar':  datos[i]['lugar'],                       
                      'creadorid': datos[i]['userid'],                     
                      'creador': datos[i]['first_name']+' '+datos[i]['last_name'],  
                      'descripcion': datos[i]['Descripcion']                     

                      };

                  eventosArre.push( elemento );
              } 

              //console.log(eventosArre); 

              $('#calendar').fullCalendar({
                      lang: 'es',
                      theme: true,
                      header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                      },
                      defaultDate: today,
                      businessHours: true, // display business hours. esto hace mas oscuro sabados y domingos
                      editable: true,
                      eventLimit: true, // allow "more" link when too many events
                      selectable: true,
                      selectHelper: true,
                      select: function(start, end) {
                        //var title = prompt('Event Title:');

                      abrir_dialog(start, end);
                     
                      },
                      /*events: [
                        {
                          title: 'Long Event',
                          start: '2016-04-07',
                          end: '2016-04-10'
                        },
                        {
                          id: 999,
                          title: 'Repeating Event',
                          start: '2016-04-09T16:00:00'
                        },
                  
                        {
                          title: 'Conference',
                          start: '2016-04-11',
                          end: '2016-04-13'
                        },
                       
                        {
                          title: 'Click for Google',
                          url: 'http://google.com/',
                          start: '2016-04-28'
                        }
                      ]*/
                      events: eventosArre,

                      /*eventRender: function(event, element) {
                            element.attr("confirm",event.confirm)
                        }*/

                      eventClick: function(calEvent, jsEvent, view) {
                          /*alert('Event id: ' + calEvent.id);
                          alert('title id: ' + calEvent.title);
                          alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                          alert('View: ' + view.name); */

                          abrir_dialogDetalle(calEvent); //le envio todos los datos del evento

                          // change the border color just for fun
                          $(this).css('border-color', 'red');
                       }
                      
              });                    
               
            }
            $('#loadingNiv').remove();
        }, //--fun success
        error: function(response){
            console.log(response);                      
        }
            
      });


  }




$(document).ready(function() {

   traer_eventos();

    //abrir_dialog();

    
    
  });

</script>


<body>
     
    <?php include("application/views/include/navbar-menu.php"); 
    //include("application/views/include/navbar.php");?>

    <div id='barra_lateral' > algooo <br><br><br><br> algo</div>

    <div class="container">

        <div class="container-fluid">    
          <!-- Descripción Paso -->
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center vspace2">
                <h3>Calendario de Eventos</h3>
                <h6>click sobre una casillero</h6>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center vspace">
                <p></p>
              </div>
            </div>

            <!-- Funcionalidad --> 


            <div id='calendar'></div>
                   

                                     
                <br>
                
              <button  class="btn btn-danger " id='nextHora' class="next" style='float:right; margin-right:5%'>Finalizar</button>    
        </div>
                    
  
  	</div>


    <div class='notifications top-right' style="margin-top: 40px">
      <!--<button type="button" aria-hidden="true" class="" data-notify="dismiss">×z</button>-->  
    </div>

<?php include("application/views/include/footer.php"); ?>	
<?php //include("application/views/include/websocket.php"); ?>   
    
</body>




<style type="text/css">
    
  I/*body {
    margin: 40px 10px;
    padding: 0;
    font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
    font-size: 14px;
  }*/

  #calendar {
    max-width: 700px;
    margin: 0 auto;
    
  }

  .modal-dialog {
    width:70% !important;
  }

  .close2 {
  width: 12px;
  background: url('<?=base_url('assets/images/chosen-sprite.png'); ?>') -42px 1px no-repeat;
  cursor:hand;
  /*position: absolute;*/
  top: 2px;
  margin-right: -10px;
  margin-top: -3px;
  display: block;
  height: 12px;
  font-size: 1px;
  background-position: -42px -10px;
  }

  .close2:hover {
  background-position: -42px -10px;
}

  #barra_lateral{
    float:left;
    margin-top: 20%;
    position:absolute; 
    
  }

  select
  {
      min-width: 100px;
  }

</style>

