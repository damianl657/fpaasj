/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	//config.embed_provider = '//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}';
	//config.autoEmbed_widget = 'customEmbed';
	/*config.extraPlugins = 'autoembed';
	config.extraPlugins = 'autolink';
	config.embed_provider = '//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}';
	config.autoEmbed_widget = 'customEmbed';*/
	//config.extraPlugins = 'autoembed,embedsemantic';
	config.extraPlugins = 'autolink';
//config.autoEmbed_widget = 'customEmbed';
//config.autoEmbed_widget = 'embedSemantic';
//config.embed_provider = '//iframe.ly/api/oembed?url={url}&callback={callback}&api_key=MYAPI';
//config.embed_provider = '//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}'
};
