<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_areas extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }



        public function obtener_areas($data)
        {
            $this->db->where($data);
            $query = $this->db->get("area");

            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }


        public function obtener_materias_By_areas($areas, $planestudio)
        {
            $sql = "SELECT area_materias.id, aniomateria_id, nombresmateria.id as idmateria, nombresmateria.nombre as nombremat, nodoanios.id as nodoanioid, anios.nombre as nombreanio, area.nombre as areaname, niveles.nombre as nombrenivel, especialidades.nombre as nombreespecialidad
                    FROM area_materias
                    JOIN area ON area.id = area_materias.area_id
                    JOIN anios_materias ON area_materias.aniomateria_id = anios_materias.id
                    JOIN nombresmateria ON anios_materias.nombresmateria_id = nombresmateria.id
                    JOIN nodoanios ON anios_materias.nodoanios_id = nodoanios.id
                    JOIN anios ON nodoanios.anio_id = anios.id

                    JOIN nodonivel ON nodonivel.id = nodoanios.nodoniv_id
                    JOIN niveles ON niveles.id = nodonivel.niveles_id
                    LEFT JOIN especialidades ON especialidades.id = nodonivel.esp_id

                    WHERE area_materias.area_id IN ($areas)
                    AND area_materias.planestudio = $planestudio
                    ORDER BY nombresmateria.nombre, anios.nombre, niveles.nombre, especialidades.nombre ASC";
            //echo $sql;
            $query = $this->db->query($sql);

            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }

        /*public function obtener_materias_By_docente($areas, $idusergroup,$planestudio)
        {
            $sql = "SELECT area_materias.id, aniomateria_id, nombresmateria.id as idmateria, nombresmateria.nombre as nombremat, nodoanios.id as nodoanioid, anios.nombre as nombreanio, niveles.nombre as nombrenivel, especialidades.nombre as nombreespecialidad
                    FROM area_materias
                    JOIN anios_materias ON area_materias.aniomateria_id = anios_materias.id
                    JOIN nombresmateria ON anios_materias.nombresmateria_id = nombresmateria.id
                    JOIN nodoanios ON anios_materias.nodoanios_id = nodoanios.id
                    JOIN anios ON nodoanios.anio_id = anios.id

                    JOIN nodonivel ON nodonivel.id = nodoanios.nodoniv_id
                    JOIN niveles ON niveles.id = nodonivel.niveles_id
                    LEFT JOIN especialidades ON especialidades.id = nodonivel.esp_id

                    JOIN horariosmaterias ON horariosmaterias.materias_id = anios_materias.id

                    WHERE area_materias.area_id IN ($areas)
                    AND horariosmaterias.docente_id = $idusergroup
                    AND area_materias.planestudio = $planestudio
                    ORDER BY nombresmateria.nombre, anios.nombre, niveles.nombre, especialidades.nombre ASC";
            $query = $this->db->query($sql);

            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }*/
        public function obtener_materias_By_docente($areas, $idusergroup,$planestudio)
        {
            $sql = "SELECT area_materias.id, aniomateria_id, nombresmateria.id as idmateria, nombresmateria.nombre as nombremat, nodoanios.id as nodoanioid, anios.nombre as nombreanio, niveles.nombre as nombrenivel, especialidades.nombre as nombreespecialidad
                FROM area_materias
                JOIN anios_materias ON area_materias.aniomateria_id = anios_materias.id
                JOIN nombresmateria ON anios_materias.nombresmateria_id = nombresmateria.id
                JOIN nodoanios ON anios_materias.nodoanios_id = nodoanios.id
                JOIN anios ON nodoanios.anio_id = anios.id
                JOIN nodonivel ON nodonivel.id = nodoanios.nodoniv_id
                JOIN niveles ON niveles.id = nodonivel.niveles_id
                LEFT JOIN especialidades ON especialidades.id = nodonivel.esp_id
                JOIN nodo_doc_div_mat_alu ON nodo_doc_div_mat_alu.id_materia = anios_materias.id
                 JOIN nodocolegio on nodocolegio.id = nodo_doc_div_mat_alu.id_colegio
                WHERE area_materias.area_id IN ($areas)
                AND nodo_doc_div_mat_alu.id_docente = $idusergroup and nodocolegio.cicloa = nodo_doc_div_mat_alu.ciclo_lectivo";
            $query = $this->db->query($sql);

            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }

        /*public function obtener_areas_By_docente($idusergroup,$planestudio)
        {
            $sql = "SELECT area.*
                    FROM area_materias
                    JOIN horariosmaterias ON horariosmaterias.materias_id = area_materias.aniomateria_id
                    JOIN area ON area.id = area_materias.area_id
                    WHERE horariosmaterias.docente_id = $idusergroup
                    AND area_materias.planestudio = $planestudio
                    ";
            $query = $this->db->query($sql);

            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }*/
        public function obtener_areas_By_docente($idusergroup,$planestudio)//ver esta funcion hooopeee
        {
            $sql = "SELECT area.*
                    FROM area_materias
                    JOIN nodo_doc_div_mat_alu ON nodo_doc_div_mat_alu.id_materia = area_materias.aniomateria_id
                    JOIN area ON area.id = area_materias.area_id
                    WHERE nodo_doc_div_mat_alu.id_docente = $idusergroup
                    AND area_materias.planestudio = $planestudio
                    ";
            $query = $this->db->query($sql);

            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }

        /*public function obtener_docentes_By_areas($areas, $aniosmaterias, $planestudio)
        {
            if($aniosmaterias){
                 $sql = "SELECT horariosmaterias.docente_id
                    FROM area_materias
                    JOIN horariosmaterias ON horariosmaterias.materias_id = area_materias.aniomateria_id
                    WHERE area_materias.area_id IN ($areas)
                    AND area_materias.aniomateria_id IN ($aniosmaterias)
                    AND area_materias.planestudio = $planestudio
                    GROUP BY horariosmaterias.docente_id";
            }
            else {
                $sql = "SELECT horariosmaterias.docente_id
                    FROM area_materias
                    JOIN horariosmaterias ON horariosmaterias.materias_id = area_materias.aniomateria_id
                    WHERE area_materias.area_id IN ($areas)
                    AND area_materias.planestudio = $planestudio
                    GROUP BY horariosmaterias.docente_id";
            }

            $query = $this->db->query($sql);

            if($query->num_rows()>0)
                return $query->result();
            else
                return false;
        }

        public function obtener_docentes_By_areas2($areas, $planestudio)
        {

            $sql = "SELECT horariosmaterias.docente_id as id, users_groups.user_id, users_groups.group_id
                FROM area_materias
                JOIN horariosmaterias ON horariosmaterias.materias_id = area_materias.aniomateria_id
                JOIN users_groups ON users_groups.id = horariosmaterias.docente_id
                WHERE area_materias.area_id IN ($areas)
                AND area_materias.planestudio = $planestudio
                GROUP BY horariosmaterias.docente_id";


            $query = $this->db->query($sql);

            if($query->num_rows()>0)
                return $query->result();
            else
                return false;
        }*/
        public function obtener_docentes_By_areas($areas, $aniosmaterias, $planestudio)//ver esta funcion hooopeee
        {
            if($aniosmaterias){
                 $sql = "SELECT nodo_doc_div_mat_alu.id_docente
                    FROM area_materias
                    JOIN nodo_doc_div_mat_alu ON nodo_doc_div_mat_alu.id_materia = area_materias.aniomateria_id
                    JOIN nodocolegio on nodocolegio.id = nodo_doc_div_mat_alu.id_colegio
                    WHERE area_materias.area_id IN ($areas)
                    AND area_materias.aniomateria_id IN ($aniosmaterias)
                    AND area_materias.planestudio = $planestudio
                    and nodocolegio.cicloa = nodo_doc_div_mat_alu.ciclo_lectivo
                    GROUP BY nodo_doc_div_mat_alu.id_docente";
            }
            else {
                $sql = "SELECT nodo_doc_div_mat_alu.id_docente
                    FROM area_materias
                    JOIN nodo_doc_div_mat_alu ON nodo_doc_div_mat_alu.id_materia = area_materias.aniomateria_id
                    JOIN nodocolegio on nodocolegio.id = nodo_doc_div_mat_alu.id_colegio
                    WHERE area_materias.area_id IN ($areas)
                    AND area_materias.planestudio = $planestudio
                    and nodocolegio.cicloa = nodo_doc_div_mat_alu.ciclo_lectivo
                    GROUP BY nodo_doc_div_mat_alu.id_docente";
            }

            $query = $this->db->query($sql);

            if($query->num_rows()>0)
                return $query->result();
            else
                return false;
        }

        public function obtener_docentes_By_areas2($areas, $planestudio)//ver esta funcion hooopeee
        {

            $sql = "SELECT nodo_doc_div_mat_alu.id_docente as id, users_groups.user_id, users_groups.group_id
                FROM area_materias
                JOIN nodo_doc_div_mat_alu ON nodo_doc_div_mat_alu.id_materia = area_materias.aniomateria_id
                JOIN users_groups ON users_groups.id = horariosmaterias.docente_id
                JOIN nodocolegio on nodocolegio.id = nodo_doc_div_mat_alu.id_colegio
                WHERE area_materias.area_id IN ($areas)
                AND area_materias.planestudio = $planestudio
                and nodo_doc_div_mat_alu.ciclo_lectivo = nodocolegio.cicloa
                GROUP BY nodo_doc_div_mat_alu.id_docente";


            $query = $this->db->query($sql);

            if($query->num_rows()>0)
                return $query->result();
            else
                return false;
        }


        /*public function obtener_alumnos_By_areas($areas, $aniosmaterias, $planestudio, $cicloa)  //se va a tabla inscripcion
        {
            if($aniosmaterias){
                 $sql = "SELECT inscripciones.alumno_id
                    FROM area_materias
                    JOIN horariosmaterias ON horariosmaterias.materias_id = area_materias.aniomateria_id
                    JOIN inscripciones ON inscripciones.divisiones_id = horariosmaterias.divisiones_id
                    WHERE area_materias.area_id IN ($areas)
                    AND area_materias.aniomateria_id IN ($aniosmaterias)
                    AND area_materias.planestudio = $planestudio
                    AND inscripciones.cicloa = $cicloa
                    GROUP BY horariosmaterias.docente_id";
            }
            else {
                $sql = "SELECT inscripciones.alumno_id
                    FROM area_materias
                    JOIN horariosmaterias ON horariosmaterias.materias_id = area_materias.aniomateria_id
                    JOIN inscripciones ON inscripciones.divisiones_id = horariosmaterias.divisiones_id
                    WHERE area_materias.area_id IN ($areas)
                    AND area_materias.planestudio = $planestudio
                    AND inscripciones.cicloa = $cicloa
                    GROUP BY horariosmaterias.docente_id";
            }

            $query = $this->db->query($sql);

            if($query->num_rows()>0)
                return $query->result();
            else
                return false;
        }*/
        public function obtener_alumnos_By_areas($areas, $aniosmaterias, $planestudio, $cicloa)  //se va a tabla inscripcion//ver esta funcion hooopeee
        {
            if($aniosmaterias){
                 $sql = "SELECT inscripciones.alumno_id
                    FROM area_materias
                    JOIN nodo_doc_div_mat_alu ON nodo_doc_div_mat_alu.id_materia = area_materias.aniomateria_id
                    JOIN inscripciones ON inscripciones.divisiones_id = nodo_doc_div_mat_alu.id_division
                    WHERE area_materias.area_id IN ($areas)
                    AND area_materias.aniomateria_id IN ($aniosmaterias)
                    AND area_materias.planestudio = $planestudio
                    AND inscripciones.cicloa = $cicloa
                    GROUP BY nodo_doc_div_mat_alu.id_docente";
            }
            else {
                $sql = "SELECT inscripciones.alumno_id
                    FROM area_materias
                    JOIN nodo_doc_div_mat_alu ON nodo_doc_div_mat_alu.id_materia = area_materias.aniomateria_id
                    JOIN inscripciones ON inscripciones.divisiones_id = nodo_doc_div_mat_alu.id_division
                    WHERE area_materias.area_id IN ($areas)
                    AND area_materias.planestudio = $planestudio
                    AND inscripciones.cicloa = $cicloa
                    GROUP BY nodo_doc_div_mat_alu.id_docente";
            }

            $query = $this->db->query($sql);

            if($query->num_rows()>0)
                return $query->result();
            else
                return false;
        }


        public function obtener_jefes_By_areas($ids_areas, $rol)
        {
            switch ($rol)
            {
                case 'jefe':
                        $sql = "SELECT jefe_id
                            FROM area
                            WHERE area.id IN ($ids_areas)
                            AND nivel_id IS NULL
                            AND jefe_id IS NOT NULL
                           ";

                        break;

                case 'regenteB':
                        $sql = "SELECT area.jefe_id
                            FROM area
                            JOIN
                                (SELECT area_padre
                                FROM area
                                WHERE area.id IN ($ids_areas)
                                ) T
                            ON T.area_padre = area.id
                            WHERE nivel_id = 1
                            AND jefe_id IS NOT NULL
                            GROUP BY area.jefe_id
                           ";

                        break;

                case 'regenteO':
                         $sql = "SELECT area.jefe_id
                            FROM area
                            JOIN
                                (SELECT area_padre
                                FROM area
                                WHERE area.id IN ($ids_areas)
                                ) T
                            ON T.area_padre = area.id
                            WHERE nivel_id = 2
                            AND jefe_id IS NOT NULL
                            GROUP BY area.jefe_id
                           ";

                        break;

                default:
                        $sql = "SELECT jefe_id
                            FROM area
                            WHERE area.id IN ($ids_areas)
                            AND nivel_id IS NULL
                            AND jefe_id IS NOT NULL
                           ";
                        break;
            }


            $query = $this->db->query($sql);

            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }

        /*public function insertar_aniosmaterias2($data, $data2)
        {
            $this->db->where($data);
            $query = $this->db->get("anios_materias");

            if($query->num_rows()>0){
                return false;
            }
            else{
                return $this->db->insert("anios_materias",$data2);
            }
        }



        public function get_aniomateria($data)
        {
            $this->db->where($data);
            $query = $this->db->get("anios_materias");

            if($query->num_rows()>0){
                return $query->row();
            }
            else{
                return false;
            }
        }


        public function delete_aniosmaterias($data)
        {
            $this->db->where($data);
            return $this->db->delete("anios_materias");
        }*/




	}