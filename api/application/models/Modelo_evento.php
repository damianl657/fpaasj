<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_evento extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }
        //nuevo para el datatable con ajax
        var $table = 'eventos';
        var $column = array("CONCAT_WS(' ',users.last_name,users.first_name)","eventos.titulo","eventos.fechaCreacion");
        var $column2 = array("users.id","eventos.titulo","eventos.fechaCreacion");
       var $order = array('eventos.fechaCreacion' => 'DESC');
        private function _get_datatables_query($userid,$privilegios,$eventos_roles)
        {
            //$this->db->select('eventos.id as eventid,  
                   //    titulo,confirm, fechaCreacion, fechaInicio, fechafin, descripcion, lugar, users.id as userid, users.first_name, users.last_name, nodocolegio.id as colegioid, nodocolegio.nombre as nombrecolegio');
                   // $this->db->where('eventos.users_id',$userid);
                    $this->db->from($this->table);
                    $this->db->join('users','eventos.users_id = users.id');
                    $this->db->join('nodocolegio','eventos.colegio_id = nodocolegio.id');
                    $this->db->join('groups', 'groups.id = eventos.group_id');
                    $this->db->join('users_groups', 'groups.id = users_groups.group_id AND users.id=users_groups.user_id');
                    $this->db->where('eventos.estado',1);
                   // print_r($privilegios);
                    //die();
                    /*if($privilegios['privilegio'] == 1)
                    {
                        $this->db->where_in('eventos.colegio_id',$privilegios['colegios']);
                    }
                    else if($eventos_roles['eventos_roles'] == 1)
                    {
                        $this->db->where_in('eventos.group_id',$eventos_roles['roles']);
                        //$this->db->or_where('eventos.users_id',$userid);
                    }
                    else
                    {
                        $this->db->where('eventos.users_id',$userid);
                    }*/
                   // $this->db->where('eventos.users_id',$userid);
                    //$this->db->join('users','eventos.users_id = users.id');
                    //$this->db->join('nodocolegio','eventos.colegio_id = nodocolegio.id');
                    //
                    $this->db->where('YEAR(eventos.fechaCreacion) >= nodocolegio.cicloa');
                    //
            $i = 0;
        
            foreach ($this->column as $item) 
            {
                if(isset($_POST['search']['value']))
                {
                    ($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
                    $this->db->where('eventos.estado',1);
                    //$this->db->where('eventos.users_id',$userid);
                    if($privilegios['privilegio'] == 1)
                    {
                        $this->db->where_in('eventos.colegio_id',$privilegios['colegios']);
                    }
                    else if($eventos_roles['eventos_roles'] == 1)
                    {
                        //$this->db->where_in('eventos.group_id',$eventos_roles['roles']);
                       // $this->db->or_where('eventos.users_id',$userid);
                        $whe_in = ''; 
                        $aux = 0;
                        
                        foreach($eventos_roles['roles'] as $ev_rol )
                        {
                            if($aux == 0)
                            {
                                $whe_in = $ev_rol;
                                $aux = 1;
                            }
                            else
                            {
                                $whe_in = $whe_in.' ,'.$ev_rol;
                            }
                             
                        }
                        $whe_in = $whe_in.' ,0';
                        $where = "(eventos.group_id in ($whe_in) OR eventos.users_id= $userid)";
                        $this->db->where($where);
                    }
                    else
                    {
                        $this->db->where('eventos.users_id',$userid);
                    }
                    
                }
                $column[$i] = $item;
                $i++;
            }
            
            if(isset($_POST['order']))
            {
                $column_order = array('users.last_name','titulo','fechaCreacion');
                $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
               // $this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            } 
            else if(isset($this->order))
            {
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }

      

        function get_datatables($userid, $privilegios,$eventos_roles)
        {//sub query
            /*$this->db->select('users_groups.colegio_id');
            $this->db->from(' users_groups');
            $this->db->join(' groups','groups.id = users_groups.group_id ');
            $this->db->join(' users','users.id = users_groups.user_id ');*/
            /*$this->db->where(' groups.name', "Directivo");
             $this->db->or_where(' groups.name', "Nodos");*/
            /*$this->db->where("(groups.name = 'Directivo' OR  groups.name = 'Nodos' OR  groups.name = 'Representante Legal' )");
            $this->db->where(' users.id', $userid);
            $this->db->where('users_groups.estado', 1);
            $subQuery = $this->db->_compile_select();
            $this->db->_reset_select();*/
            //
            //var_dump($this->db->last_query());
            //var_dump($subQuery);
           // print_r($privilegios);
           // die();
            $this->_get_datatables_query($userid,$privilegios,$eventos_roles);
            //$colegios = $this->get_ev_all_escuelas($userid);
            if(isset($_POST['length']))
            {
                if($_POST['length'] != -1)
                $this->db->limit($_POST['length'], $_POST['start']);
            }
            else
            {
                $this->db->limit(10, $_POST['start']);
            }
                
            $this->db->select('eventos.id as eventid,  
                       titulo,confirm, eventos.fechaCreacion, fechaInicio, fechafin, descripcion, lugar, users.id as userid, users.first_name, users.last_name, nodocolegio.id as colegioid, nodocolegio.nombre as nombrecolegio, eventos.group_id, eventos.standby, eventos.cont_eventosusers, eventos.cont_vistos, eventos.cont_negados, eventos.cont_confirmados, eventos.cont_me_gusta,eventos.confirm,
                        groups.name as rol_grupo,
                        users.sexo sexo_creador,
                        users_groups.group_alias nombre_grupo_alias,
                        groups.aliasM nombre_grupo_aliasM,
                        groups.aliasF nombre_grupo_aliasF, 
                        reenviadoId
                       ');

            /*$this->db->join('users','eventos.users_id = users.id');
            $this->db->join('nodocolegio','eventos.colegio_id = nodocolegio.id');
            $this->db->join('groups', 'groups.id = eventos.group_id');*/
            /*if($privilegios['privilegio'] == 1)
            {
                $this->db->where_in('eventos.colegio_id',$privilegios['colegios']);
                $this->db->or_where('eventos.users_id',$userid);
            }
            else if($eventos_roles['eventos_roles'] == 1)
            {
                $this->db->where_in('eventos.group_id',$eventos_roles['roles']);
                $this->db->or_where('eventos.users_id',$userid);
                
            }
            else
                {
                    $this->db->where('eventos.users_id',$userid);
                }*/


            
            //$this->db->or_where_in('eventos.colegio_id',$subQuery);

            //$this->db->or_where("eventos.colegio_id IN ($subQuery)");
            $this->db->where('eventos.estado',1);

            //$this->db->group_by('eventos.id');
            //$this->db->group_by('eventos.fechaCreacion');
            //$this->db->order_by("eventos.id", "desc");
            $this->db->where('YEAR(eventos.fechaCreacion) >= nodocolegio.cicloa');
            $this->db->order_by("fechaCreacion", "desc");  

            $query = $this->db->get();
            //var_dump($this->db->last_query());
            //die();
            return $query->result();
        }
        function get_ev_all_escuelas($userid)
        {
            $sql = "SELECT users_groups.colegio_id FROM users_groups 
            join groups on groups.id = users_groups.group_id 
            join users on users.id =users_groups.user_id where groups.name = 'Directivo' and users.id = $userid
            AND users_groups.estado = 1";
            $query = $this->db->query($sql);
            return $query->result();
        }
        

        function count_filtered($userid,$privilegios,$eventos_roles)
        {
            //sub query
           /* $this->db->select('users_groups.colegio_id');
            $this->db->from(' users_groups');
            $this->db->join(' groups','groups.id = users_groups.group_id ');
            $this->db->join(' users','users.id = users_groups.user_id ');
            //$this->db->where(' groups.name', "Directivo");
            $this->db->where("(groups.name = 'Directivo' OR  groups.name = 'Nodos' OR  groups.name = 'Representante Legal')");
            $this->db->where(' users.id', $userid);
            $this->db->where("users_groups.estado", 1);
            $subQuery = $this->db->_compile_select();
            $this->db->_reset_select();*/
            //
            
           // $this->_get_datatables_query($userid);
            $this->db->select('eventos.id as eventid,  
                       titulo,confirm, eventos.fechaCreacion, fechaInicio, fechafin, descripcion, lugar, users.id as userid, users.first_name, users.last_name, nodocolegio.id as colegioid, nodocolegio.nombre as nombrecolegio');
            $this->db->from('eventos');
           // $this->db->where('eventos.users_id',$userid);
            
            
            $this->db->join('users','eventos.users_id = users.id');
            $this->db->join('nodocolegio','eventos.colegio_id = nodocolegio.id');
            //$this->db->where('eventos.users_id',$userid);
            if($privilegios['privilegio'] == 1)
                    {
                        $this->db->where_in('eventos.colegio_id',$privilegios['colegios']);
                        $this->db->or_where('eventos.users_id',$userid);
                    }
            else if($eventos_roles['eventos_roles'] == 1)
            {
                $this->db->where_in('eventos.group_id',$eventos_roles['roles']);
                $this->db->or_where('eventos.users_id',$userid);
            }
            else
                    {
                        $this->db->where('eventos.users_id',$userid);
                    }
            
          //  $this->db->or_where("eventos.colegio_id IN ($subQuery)");
            $this->db->where('eventos.estado',1);
            //$this->db->or_where_in('eventos.colegio_id',1);
            //$this->db->group_by('eventos.id');
             $this->db->where('YEAR(eventos.fechaCreacion) = nodocolegio.cicloa');
            $this->db->order_by("eventos.fechaCreacion", "desc");

            $query = $this->db->get();
            return $query->num_rows();
        }

        public function count_all($userid,$privilegios,$eventos_roles)
        {
            //sub query
            /*$this->db->select('users_groups.colegio_id');
            $this->db->from(' users_groups');
            $this->db->join(' groups','groups.id = users_groups.group_id ');
            $this->db->join(' users','users.id = users_groups.user_id ');
            //$this->db->where(' groups.name', "Directivo");
            $this->db->where("(groups.name = 'Directivo' OR  groups.name = 'Nodos' OR  groups.name = 'Representante Legal')");
            $this->db->where(' users.id', $userid);
            $this->db->where('users_groups.estado', 1);
            $subQuery = $this->db->_compile_select();
            $this->db->_reset_select();*/
            //
            $this->db->select('eventos.id as eventid,  
                       titulo,confirm, eventos.fechaCreacion, fechaInicio, fechafin, descripcion, lugar, users.id as userid, users.first_name, users.last_name, nodocolegio.id as colegioid, nodocolegio.nombre as nombrecolegio');
            $this->db->from($this->table);
            //$this->db->where('eventos.users_id',$userid);

            $this->db->join('users','eventos.users_id = users.id');
            $this->db->join('nodocolegio','eventos.colegio_id = nodocolegio.id');
            //$this->db->where('eventos.users_id',$userid);
            if($privilegios['privilegio'] == 1)
                    {
                        $this->db->where_in('eventos.colegio_id',$privilegios['colegios']);
                        $this->db->or_where('eventos.users_id',$userid);
                    }
            else if($eventos_roles['eventos_roles'] == 1)
            {
                $this->db->where_in('eventos.group_id',$eventos_roles['roles']);
                $this->db->or_where('eventos.users_id',$userid);
            }
            else
                    {
                        $this->db->where('eventos.users_id',$userid);
                    }
            //$this->db->or_where_in('eventos.colegio_id',1);
            // $this->db->or_where("eventos.colegio_id IN ($subQuery)");
             $this->db->where('eventos.estado',1);
             //$this->db->group_by('eventos.id');
             $this->db->where('YEAR(eventos.fechaCreacion) = nodocolegio.cicloa');
             $this->db->order_by("eventos.fechaCreacion", "desc");
             $query = $this->db->get();
            return $query->num_rows();
        }

        /*public function get_by_id($id)
        {
            $this->db->from($this->table);
            $this->db->where('id',$id);
            $query = $this->db->get();

            return $query->row();
        }*/

        //



        /*public function obtener_eventosNew($userid) //ANDRES   trae los que no he visto
        {
            $sql = "
                SELECT eventos_id, eventosusers.users_id, visto, aceptado,   
                       titulo,confirm, fechaCreacion, fechaInicio, descripcion, eventos.users_id, lugar
                FROM eventosusers              
                JOIN users ON eventosusers.users_id = users.id
                JOIN eventos ON eventosusers.eventos_id = eventos.id
                WHERE eventosusers.users_id = $userid
                AND eventosusers.visto=0
                AND eventos.estado=1
                ORDER BY fechaInicio ASC";
            $query = $this->db->query($sql);
            return $query->result();
        }*/

        public function obtener_eventosTodos($idcolegio) //ANDRES   trae todos (año actual), solo administrador
        {
            $anioA= date('Y');
            //$anioA= 2016;
            $datosCole=$this->modelo_colegio->get_colegio($idcolegio);
            if($datosCole)
                foreach ($datosCole->result() as $cole) {
                    $anioA=$cole->cicloa;
                }
                    

            $sql = "
                SELECT eventos.id as eventid,  
                       titulo,confirm, 
                       fechaCreacion, 
                       fechaInicio, 
                       fechafin, 
                       descripcion, 
                       lugar, 
                       users.id as userid, 
                       users.first_name, 
                       users.last_name, 
                       nodocolegio.id as colegioid, 
                       nodocolegio.nombre as nombrecolegio,
                       groups.name
                FROM eventos              
                JOIN users ON eventos.users_id = users.id
                JOIN groups ON eventos.group_id = groups.id
                JOIN nodocolegio ON eventos.colegio_id = nodocolegio.id
                WHERE YEAR(eventos.fechaCreacion) = $anioA
                AND eventos.colegio_id = $idcolegio
                AND eventos.estado=1
                AND eventos.standby=0
                GROUP BY eventos.id
                ORDER BY fechaCreacion ASC";
            $query = $this->db->query($sql);
            return $query->result();
        }

        public function obtener_eventosCreados($userid, $idcolegio) //ANDRES   trae los creados por mi (año actual)// NO MODIFICAR POR QUE LA VOY A USAR YO DAMIAN 
        {
            $anioA= date('Y');
            //$anioA= 2016;
            $datosCole=$this->modelo_colegio->get_colegio($idcolegio);
            if($datosCole)
                foreach ($datosCole->result() as $cole) {
                    $anioA=$cole->cicloa;
                }

            $sql = "SELECT eventos.id as eventid,  
                       titulo,confirm, 
                       fechaCreacion, 
                       fechaInicio, 
                       fechafin, 
                       descripcion, 
                       lugar, 
                       users.id as userid, 
                       users.first_name, 
                       users.last_name, 
                       users.sexo,
                       nodocolegio.id as colegioid, 
                       nodocolegio.nombre as nombrecolegio,
                       groups.name,
                       groups.aliasF,
                       groups.aliasM,
                       users_groups.group_alias
                    FROM eventos              
                    JOIN users ON eventos.users_id = users.id
                    JOIN groups ON eventos.group_id = groups.id
                    JOIN users_groups ON eventos.group_id = users_groups.group_id AND users_groups.user_id = eventos.users_id
                    JOIN nodocolegio ON eventos.colegio_id = nodocolegio.id
                    WHERE YEAR(eventos.fechaCreacion) = $anioA
                    AND eventos.users_id = $userid
                    AND eventos.colegio_id = $idcolegio
                    AND eventos.estado=1
                    AND eventos.standby=0
                    GROUP BY eventos.id
                    ORDER BY fechaCreacion ASC ";

            $query = $this->db->query($sql);
            return $query->result();
        }
         public function obtener_eventosCreadosX_rol($userid, $idcolegio,$eventos_roles) //damian --->>>> no la toquen gatos
        {
            $anioA= date('Y');
            //$anioA= 2016;
          //  print_r($eventos_roles);
            $roles = '';
            foreach ($eventos_roles['roles'] as $key ) {
              // $roles = $roles.
               // print_r($key);
                 $roles =  $roles.$key.',';
            }
            $roles =  $roles.'0';
            //die();
            $datosCole=$this->modelo_colegio->get_colegio($idcolegio);
            if($datosCole)
                foreach ($datosCole->result() as $cole) {
                    $anioA=$cole->cicloa;
                }

            $sql = "SELECT eventos.id as eventid,  
                       titulo,confirm, 
                       fechaCreacion, 
                       fechaInicio, 
                       fechafin, 
                       descripcion, 
                       lugar, 
                       users.id as userid, 
                       users.first_name, 
                       users.last_name, 
                       nodocolegio.id as colegioid, 
                       nodocolegio.nombre as nombrecolegio,
                       groups.name
                    FROM eventos              
                    JOIN users ON eventos.users_id = users.id
                    JOIN groups ON eventos.group_id = groups.id
                    JOIN nodocolegio ON eventos.colegio_id = nodocolegio.id
                    WHERE YEAR(eventos.fechaCreacion) = $anioA
                    AND eventos.group_id in ($roles)
                    AND eventos.colegio_id = $idcolegio
                    AND eventos.estado=1
                    AND eventos.standby=0
                    GROUP BY eventos.id
                    ORDER BY fechaCreacion ASC ";
                //    print_r($sql);
            $query = $this->db->query($sql);
            return $query->result();
        }
        public function obtener_eventosRecibidos($userid, $idcolegio) //ANDRES   trae los recibidos (año actual)
        {
            $anioA= date('Y');
            //$anioA= 2016;
            $datosCole=$this->modelo_colegio->get_colegio($idcolegio);
            if($datosCole)
                foreach ($datosCole->result() as $cole) {
                    $anioA=$cole->cicloa;
                }
        
            $sql = "
                SELECT eventos.id as eventid,  
                       titulo,confirm, 
                       eventos.fechaCreacion, 
                       fechaInicio,fechafin, 
                       descripcion, 
                       lugar, 
                       users.id as userid, 
                       users.first_name, 
                       users.last_name, 
                       users.sexo, 
                       nodocolegio.id as colegioid, 
                       nodocolegio.nombre as nombrecolegio,
                       groups.name,
                       groups.aliasF,
                       groups.aliasM,
                       users_groups.group_alias             
                FROM eventosusers    
                JOIN eventos ON eventosusers.eventos_id = eventos.id
                JOIN users ON eventos.users_id = users.id
                JOIN groups ON eventos.group_id = groups.id
                JOIN users_groups ON eventos.group_id = users_groups.group_id AND users_groups.user_id = eventos.users_id
                JOIN nodocolegio ON eventos.colegio_id = nodocolegio.id
                WHERE YEAR(eventos.fechaCreacion) = $anioA
                AND eventosusers.users_id = $userid
                AND eventos.colegio_id = $idcolegio
                AND eventos.estado=1
                AND eventos.standby=0
                GROUP BY eventos.id
                ORDER BY fechaCreacion ASC";


            $query = $this->db->query($sql);
            return $query->result();
        }

        
        public function obtener_eventos_creados_de_la_escuela()// todavia esta incompleto
        {
            $sql = "eventos.id as eventid,  
                       titulo,confirm, fechaCreacion, fechaInicio, fechafin, descripcion, lugar, users.id as userid, users.first_name, users.last_name, nodocolegio.id as colegioid, nodocolegio.nombre as nombrecolegio
                    FROM eventos
                    JOIN users ON eventos.users_id = users.id
                    JOIN nodocolegio ON eventos.colegio_id = nodocolegio.id


            ";
            $query = $this->db->query($sql);
            return $query->result();

        }
      

        public function obtener_eventos($userid, $limit)//SEBA
        {
            /*$this->db->select("T.id evento_id,
                    T.titulo,
                    T.descripcion,
                    users.first_name nombre,
                    users.last_name apellido,
                    T.fechaInicio,
                    T.confirm,
                    T.fechaCreacion,
                    eventosusers.aceptado,
                    eventosusers.visto,
                    eventosusers.id,
                    eventosusers.me_gusta,
                    eventosusers.destinatario,
                    cantfiles,
                    IFNULL( CONCAT('".$this->utilidades->get_url_img()."fotos_usuarios/', users.foto,'') ,
                        CONCAT('".$this->utilidades->get_url_img()."fotos_usuarios/', 'default-avatar.png','' ) ) foto,
                    ux.first_name nombre_creador,
                    ux.last_name apellido_creador,
                    ux.sexo sexo_creador,

                    IFNULL( CONCAT('".$this->utilidades->get_url_img()."fotos_colegios/', nodocolegio.logo,'') ,
                        CONCAT('".$this->utilidades->get_url_img()."fotos_colegios/', 'default_esc.png','' ) ) logo,
                    groups.name nombre_grupo,
                    users_groups.group_alias nombre_grupo_alias,
                    groups.aliasM nombre_grupo_aliasM,
                    groups.aliasF nombre_grupo_aliasF");  
            $this->db->from('eventosusers');
            $this->db->where('eventosusers.users_id',$userid);
            $this->db->join('users','eventosusers.destinatario = users.id');

            $this->db->join('(SELECT eventos.*,COUNT(files.id) as cantfiles FROM eventos LEFT JOIN files ON eventos.id = files.   evento_id WHERE 1 GROUP BY eventos.id) T','eventosusers.eventos_id = T.id');
            //$this->db->join('eventos','eventosusers.eventos_id = eventos.id');

            $this->db->join("users ux", "T.users_id = ux.id");
            $this->db->join("users_groups", "users_groups.user_id = ux.id");

            $this->db->join("groups", "groups.id = T.group_id");
            $this->db->join('nodocolegio','nodocolegio.id = T.colegio_id');

            $this->db->where('eventosusers.alta != ','b'); 
           // $this->db->where('eventosusers.medio !=','n');                      

            $this->db->where('users_groups.estado', 1);
          //  $this->db->where('users_groups.estado', 1);

            $this->db->where('YEAR(T.fechaCreacion) = nodocolegio.cicloa');

            $this->db->where('T.standby', 0);

            $this->db->group_by('eventosusers.id');
            $this->db->group_by("eventosusers.eventos_id, eventosusers.users_id, eventosusers.destinatario");
            
            $array = explode(",", $limit);
            $this->db->limit($array[1] , $array[0]);
            $this->db->order_by("eventosusers.id", "desc");
            $this->db->order_by("eventosusers.fechaCreacion", "desc");*/

            //$query = $this->db->get();
            //print_r($this->db->last_query());

           /* $sql = "SELECT T.id evento_id, 
            T.titulo, 
            T.descripcion, 
            users.first_name nombre, 
            users.last_name apellido, 
            T.fechaInicio, 
            T.confirm, 
            T.fechaCreacion,
            eventosusers.aceptado,
            eventosusers.visto, 
            eventosusers.id, 
            eventosusers.me_gusta, 
            eventosusers.destinatario, 
            cantfiles, IFNULL( CONCAT('".$this->utilidades->get_url_img()."fotos_usuarios/', users.foto, ''), 
            CONCAT('".$this->utilidades->get_url_img()."fotos_usuarios/', 'default-avatar.png', '' ) ) foto, 
            ux.first_name nombre_creador, 
            ux.last_name apellido_creador, 
            ux.sexo sexo_creador, IFNULL( CONCAT('".$this->utilidades->get_url_img()."fotos_colegios/', 
            nodocolegio.logo, ''), 
            CONCAT('".$this->utilidades->get_url_img()."fotos_colegios/', 'default_esc.png', '' ) ) logo, 
            groups.name nombre_grupo, 
            users_groups.group_alias nombre_grupo_alias, 
            groups.aliasM nombre_grupo_aliasM, 
            groups.aliasF nombre_grupo_aliasF
            FROM eventosusers
            JOIN users ON eventosusers.destinatario = users.id
            JOIN (SELECT eventos.*,COUNT(files.id) as cantfiles FROM eventos LEFT JOIN files ON eventos.id = files.   evento_id WHERE 1 GROUP BY eventos.id) T ON eventosusers.eventos_id = T.id
            JOIN users ux ON T.users_id = ux.id
            JOIN users_groups ON users_groups.user_id = ux.id
            JOIN groups ON groups.id = T.group_id
            JOIN nodocolegio ON nodocolegio.id = T.colegio_id
            WHERE eventosusers.users_id = $userid
            AND eventosusers.alta != 'b'
            AND users_groups.estado = 1
            AND YEAR(T.fechaCreacion) = nodocolegio.cicloa
            AND T.standby =0
            GROUP BY eventosusers.id, eventosusers.eventos_id, eventosusers.users_id, eventosusers.destinatario
            ORDER BY eventosusers.id DESC, eventosusers.fechaCreacion DESC
            LIMIT 5";
            $query = $this->db->query($sql);*/

            //$sql = "CALL obtener_eventos(".$userid.")";
            $sql = "SELECT T.id evento_id, 
            T.titulo, 
            T.descripcion, 
            T.fechaInicio, 
            T.confirm, 
            T.fechaCreacion,
            T.group_id,
            T.users_id,
            eventosusers.aceptado,
            eventosusers.visto, 
            eventosusers.id, 
            eventosusers.me_gusta, 
            eventosusers.destinatario,
            nodocolegio.logo
            
            FROM eventosusers
            JOIN eventos T ON eventosusers.eventos_id = T.id
            JOIN nodocolegio ON nodocolegio.id = T.colegio_id
            WHERE eventosusers.users_id = $user_id
            AND eventosusers.alta != 'b'
            AND YEAR(T.fechaCreacion) >= nodocolegio.cicloa
            AND T.standby =0
            GROUP BY eventosusers.id, eventosusers.eventos_id, eventosusers.users_id, eventosusers.destinatario
            ORDER BY eventosusers.id DESC, eventosusers.fechaCreacion DESC
            LIMIT 5";
            $query = $this->db->query($sql);
            $resultado = $query->result();
            mysqli_next_result( $this->db->conn_id );
            return $resultado;
        }
        public function obtener_eventos_hijos($destinatarios_id,$tutor_id)//SEBA
        {
            

           // $sql = "CALL obtener_eventos_hijos(".$userids_hijos.",".$userd_id_tutor.")";
            $sql = "SELECT T.id evento_id, 
            T.titulo, 
            T.descripcion, 
            T.fechaInicio, 
            T.confirm, 
            T.fechaCreacion,
            T.group_id,
            T.users_id,
            eventosusers.aceptado,
            eventosusers.visto, 
            eventosusers.id, 
            eventosusers.me_gusta, 
            eventosusers.destinatario,
            eventosusers.notificado,
            eventosusers.motivo,
            eventosusers.medio,
            eventosusers.favorito,
            eventosusers.fecha_notificado,
            T.destacado,
            T.fecha_modificacion,
            T.modificado,
            nodocolegio.logo
            
            FROM eventosusers
            JOIN eventos T ON eventosusers.eventos_id = T.id
            JOIN nodocolegio ON nodocolegio.id = T.colegio_id
            WHERE (FIND_IN_SET(eventosusers.destinatario, $destinatarios_id) or eventosusers.users_id = $tutor_id)
            AND eventosusers.alta != 'b's
            AND YEAR(T.fechaCreacion) >= nodocolegio.cicloa
            AND T.standby =0
            GROUP BY eventosusers.eventos_id
            ORDER BY eventosusers.id DESC, eventosusers.fechaCreacion DESC
            LIMIT 5";
            $query = $this->db->query($sql);
            $resultado = $query->result();
            // mysqli_next_result( $this->db->conn_id );
            return $resultado;
        }


        public function buscar_eventos($userid,$palabra)//JOSE
        {
            $sql = "CALL buscar_eventos(".$userid.",".$palabra.")";
            $query = $this->db->query($sql);
            $resultado = $query->result();
            mysqli_next_result( $this->db->conn_id );
            return $resultado;
        }

        /*public function obtener_eventos($userid, $limit)//SEBA
        {
            $sql="SELECT eventos.id evento_id,
                    eventos.titulo,
                    eventos.descripcion,
                    users.first_name nombre,
                    users.last_name apellido,
                    eventos.fechaInicio,
                    eventos.confirm,
                    eventos.fechaCreacion,
                    eventosusers.aceptado,
                    eventosusers.visto,
                    eventosusers.id,
                    eventosusers.me_gusta,
                    eventosusers.destinatario,
                    IFNULL( CONCAT('".$this->utilidades->get_url_img()."fotos_usuarios/', users.foto,'') ,
                        CONCAT('".$this->utilidades->get_url_img()."fotos_usuarios/', 'default-avatar.png','' ) ) foto,
                    ux.first_name nombre_creador,
                    ux.last_name apellido_creador,

                    IFNULL( CONCAT('".$this->utilidades->get_url_img()."fotos_colegios/', nodocolegio.logo,'') ,
                        CONCAT('".$this->utilidades->get_url_img()."fotos_colegios/', 'default_esc.png','' ) ) logo,
                    groups.name nombre_grupo 
                FROM eventosusers
                where eventosusers.users_id = $userid
            JOIN users ON eventosusers.destinatario = users.id
            JOIN eventos eventosusers.eventos_id = eventos.id 
            JOIN sers ux ON eventos.users_id = ux.id
            JOIN users_groups ON users_groups.user_id = ux.id
            JOIN groups ON groups.id = users_groups.group_id
            JOIN nodocolegio ON nodocolegio.id = eventos.colegio_id     
            group_by eventosusers.id     
            order_by eventos.fechaCreacion desc ":          

           
            $query = $this->db->query($sql);
            return $query->result();
        }*/

    

        public function obtener_eventosenviados_por_ideventos($userid,$id_evento)
        {
            $this->db->select("eventos.id evento_id,
                    eventos.titulo,
                    eventos.descripcion,
                    users.first_name nombre,
                    users.last_name apellido,
                    eventos.fechaInicio,
                    eventos.confirm,
                    eventosusers.aceptado,
                    eventosusers.visto,
                    eventosusers.id,
                    eventosusers.destinatario,
                    eventosusers.fechaConfirmacion,
                    eventosusers.fechaVisto,
                    eventosusers.me_gusta,
                    IFNULL(users.foto, 'http://e-nodos.com/img/fotos_usuarios/default-avatar.png') foto,
                    
                    IFNULL(nodocolegio.logo, 'http://e-nodos.com/img/default_esc.png') logo,
                    groups.name nombre_grupo,
                    users_groups.group_alias nombre_grupo_alias,
                    groups.aliasM nombre_grupo_aliasM,
                    groups.aliasF nombre_grupo_aliasF,
                    ux.sexo sexo_creador
                    ");  
            $this->db->from('eventosusers');
            $this->db->join('users','eventosusers.users_id = users.id');
            $this->db->join('eventos','eventosusers.eventos_id = eventos.id');
            $this->db->join("users ux", "eventos.users_id = ux.id");
            $this->db->join("users_groups", "users_groups.user_id = ux.id");            
            $this->db->join("groups", "groups.id = users_groups.group_id");
            $this->db->join('nodocolegio','nodocolegio.id = eventos.colegio_id');                      
            //$this->db->where('eventos.users_id',$userid);
            $this->db->where('eventos.id',$id_evento);
            $this->db->where('users_groups.estado', 1);
             $this->db->where('eventos.estado', 1);

            $this->db->group_by('eventosusers.id');
            
            $this->db->order_by("eventosusers.id", "desc");
            $query = $this->db->get();
            return $query->result();
        }
        public function obtener_eventosenviados_por_ideventos2($userid,$id_evento)
        {
            $sql ="
                 SELECT eventos.id evento_id, 
                     eventos.titulo, 
                     eventos.descripcion, 
                     eventos.fechaInicio, 
                     eventos.confirm, 
                     eventos.colegio_id,
                     users.id user_id, 
                     users.first_name nombre, 
                     users.last_name apellido, 
                     users.sexo, 
                     eventosusers.aceptado, 
                     eventosusers.visto, 
                     eventosusers.id, 
                     eventosusers.destinatario, 
                     eventosusers.fechaConfirmacion, 
                     eventosusers.fechaVisto, 
                     eventosusers.me_gusta, 
                     eventosusers.enviado,
                     eventosusers.motivo,
                     eventosusers.medio
                 FROM eventosusers 
                 join users on users.id = eventosusers.users_id 
                 join eventos on eventos.id = eventosusers.eventos_id   
                 where eventosusers.eventos_id = $id_evento 
                 and eventosusers.alta <> 'b' 
             ";

            /*$sql ="SELECT eventos.id evento_id, 
             eventos.titulo, 
             eventos.descripcion, 
             users.first_name nombre, 
             users.last_name apellido, 
             eventos.fechaInicio, 
             eventos.confirm, 
             eventosusers.aceptado, 
             eventosusers.visto, 
             eventosusers.id, 
             eventosusers.destinatario, 
             eventosusers.fechaConfirmacion, 
             eventosusers.fechaVisto, 
             eventosusers.me_gusta, 
             eventosusers.users_id,
             groups.name as grupo_rol
             FROM eventosusers 
             join users on users.id = eventosusers.users_id 
             join users_groups on users_groups.user_id = users.id 
             join groups on groups.id = users_groups.group_id 
             join eventos on eventos.id = eventosusers.eventos_id 
             where eventosusers.eventos_id = $id_evento 
             and users_groups.estado = 1 
             and eventosusers.alta <> 'b' 
             and eventos.colegio_id = users_groups.colegio_id
            GROUP BY eventosusers.id order by groups.orden desc
             ";*/
            // $sql = "call obtener_eventos_enviados($id_evento) ";
            $sql = "SELECT eventos.id evento_id, 
            eventos.id evento_id, 
            eventos.titulo, 
            eventos.descripcion, 
            eventos.fechaInicio, 
            eventos.confirm, 
            eventos.colegio_id,
            eventosusers.aceptado, 
            eventosusers.destinatario, 
            eventosusers.visto, 
            eventosusers.id, 
            eventosusers.users_id, 
            eventosusers.fechaConfirmacion, 
            eventosusers.fechaVisto, 
            eventosusers.me_gusta, 
            eventosusers.enviado,
            eventosusers.motivo,
            eventosusers.notificado,
            eventosusers.favorito,
            eventosusers.fecha_notificado,
            eventos.destacado,
            eventos.fecha_modificacion,
            eventos.modificado,
            eventosusers.medio
            FROM eventosusers 
            join eventos on eventos.id = eventosusers.eventos_id
            where eventosusers.eventos_id = $id_evento 
            and eventosusers.alta <> 'b'";

            $query = $this->db->query($sql);

            $resultado = $query->result();
            mysqli_next_result( $this->db->conn_id );
            return $resultado;
          
        }
        public function obtener_eventosenviados_por_ideventos3($userid,$id_evento)
        {

        }

        public function cargar_mas_eventos($ultimo_id, $userid)//DAMIAN
        {
           /* $this->db->select("T.id evento_id,
                    T.titulo,
                    T.descripcion,
                    users.first_name nombre,
                    users.last_name apellido,
                    T.fechaInicio,
                    T.confirm,
                    T.fechaCreacion,
                    eventosusers.aceptado,
                    eventosusers.visto,
                    eventosusers.id,
                    eventosusers.me_gusta,
                    eventosusers.destinatario,
                    cantfiles,
                    IFNULL( CONCAT('".$this->utilidades->get_url_img()."fotos_usuarios/', users.foto,'') ,
                        CONCAT('".$this->utilidades->get_url_img()."fotos_usuarios/', 'default-avatar.png','' ) ) foto,
                    ux.first_name nombre_creador,
                    ux.last_name apellido_creador,
                    ux.sexo sexo_creador,

                    IFNULL( CONCAT('".$this->utilidades->get_url_img()."fotos_colegios/', nodocolegio.logo,'') ,
                        CONCAT('".$this->utilidades->get_url_img()."fotos_colegios/', 'default_esc.png','' ) ) logo,
                    groups.name nombre_grupo,
                    users_groups.group_alias nombre_grupo_alias,
                    groups.aliasM nombre_grupo_aliasM,
                    groups.aliasF nombre_grupo_aliasF");  
            $this->db->from('eventosusers');
            $this->db->where('eventosusers.users_id',$userid);
            $this->db->join('users','eventosusers.destinatario = users.id');
            
            $this->db->join('(SELECT eventos.*,COUNT(files.id) as cantfiles FROM eventos LEFT JOIN files ON eventos.id = files.   evento_id WHERE 1 GROUP BY eventos.id) T','eventosusers.eventos_id = T.id');

            $this->db->join("users ux", "T.users_id = ux.id");
            $this->db->join("users_groups", "users_groups.user_id = ux.id");

            $this->db->join("groups", "groups.id = T.group_id");
            $this->db->join('nodocolegio','nodocolegio.id = T.colegio_id');              

            //$this->db->where('eventosusers.alta','a'); 
            $this->db->where('eventosusers.alta != ','b'); 
           // $this->db->where('eventosusers.medio !=','n');  
            $this->db->where('users_groups.estado', 1);
            $this->db->where('eventosusers.id < ', $ultimo_id);

             $this->db->where('T.estado', 1);

             $this->db->where('T.standby', 0);

             $this->db->where('YEAR(T.fechaCreacion) = nodocolegio.cicloa');

            //$this->db->group_by('eventosusers.id');
            $this->db->group_by("eventosusers.eventos_id, eventosusers.users_id, eventosusers.destinatario");
            $this->db->limit('5');

            //$this->db->group_by('eventosusers.id');
            //$this->db->limit(5);
            $this->db->order_by("T.fechaCreacion", "desc");
            $query = $this->db->get();*/
             //print_r($this->db->last_query()); die();
            $sql = "SELECT T.id evento_id, 
            T.titulo, 
            T.descripcion, 
            T.fechaInicio, 
            T.confirm, 
            T.fechaCreacion,
            T.group_id,
            T.users_id,
            eventosusers.aceptado,
            eventosusers.visto, 
            eventosusers.id, 
            eventosusers.me_gusta, 
            eventosusers.destinatario,
            eventosusers.notificado,
            eventosusers.motivo,
            eventosusers.medio,
            eventosusers.favorito,
            eventosusers.fecha_notificado,
            T.destacado,
            T.fecha_modificacion,
            T.modificado,
            nodocolegio.logo
            FROM eventosusers
            JOIN eventos T ON eventosusers.eventos_id = T.id
            JOIN nodocolegio ON nodocolegio.id = T.colegio_id
            WHERE eventosusers.users_id = $userid
            AND eventosusers.alta != 'b'
            AND YEAR(T.fechaCreacion) >= nodocolegio.cicloa
            AND T.standby =0
            AND eventosusers.id < $ultimo_id
            GROUP BY eventosusers.id, eventosusers.eventos_id, eventosusers.users_id, eventosusers.destinatario
            ORDER BY eventosusers.id DESC, eventosusers.fechaCreacion DESC
            LIMIT 5";
            $query = $this->db->query($sql);
            //$sql = "CALL cargar_mas_eventos(".$userid.",".$ultimo_id.")";
            //$query = $this->db->query($sql);
            $resultado = $query->result();
           // mysqli_next_result( $this->db->conn_id );
            return $resultado;
        }
        public function cargar_mas_eventos_hijos($ultimo,$destinatarios_id, $tutor_id)//DAMIAN
        {
            //$sql = "CALL obtener_mas_eventos_hijos(".$userd_id_hijos.",".$userd_id_tutor.",".$ultimo_id.")";
            $sql = "SELECT T.id evento_id, 
            T.titulo, 
            T.descripcion, 
            T.fechaInicio, 
            T.confirm, 
            T.fechaCreacion,
            T.group_id,
            T.users_id,
            eventosusers.aceptado,
            eventosusers.visto, 
            eventosusers.id, 
            eventosusers.me_gusta, 
            eventosusers.destinatario,
            eventosusers.notificado,
            eventosusers.motivo,
            eventosusers.medio,
            eventosusers.favorito,
            eventosusers.fecha_notificado,
            T.destacado,
            T.fecha_modificacion,
            T.modificado,
            nodocolegio.logo
            
            FROM eventosusers
            JOIN eventos T ON eventosusers.eventos_id = T.id
            JOIN nodocolegio ON nodocolegio.id = T.colegio_id
            WHERE (FIND_IN_SET(eventosusers.destinatario, $destinatarios_id) or eventosusers.users_id = $tutor_id)
            AND eventosusers.alta != 'b'
            AND YEAR(T.fechaCreacion) >= nodocolegio.cicloa
            AND T.standby =0
            AND eventosusers.id < $ultimo
            GROUP BY eventosusers.eventos_id
            ORDER BY eventosusers.id DESC, eventosusers.fechaCreacion DESC
            LIMIT 5";
            $query = $this->db->query($sql);
            $resultado = $query->result();
            //mysqli_next_result( $this->db->conn_id );
            return $resultado;
        }
        public function obtener_eventos_recientes($user_id)//DAMIAN, es para el socket trucho
        {
            /*$this->db->select("T.id evento_id,
                    T.titulo,
                    T.descripcion,
                    users.first_name nombre,
                    users.last_name apellido,
                    T.fechaInicio,
                    T.confirm,
                    T.fechaCreacion,
                    eventosusers.aceptado,
                    eventosusers.visto,
                    eventosusers.id,
                    eventosusers.me_gusta,
                    eventosusers.destinatario,
                    cantfiles,
                    IFNULL( CONCAT('".$this->utilidades->get_url_img()."fotos_usuarios/', users.foto,'') ,
                        CONCAT('".$this->utilidades->get_url_img()."fotos_usuarios/', 'default-avatar.png','' ) ) foto,
                    ux.first_name nombre_creador,
                    ux.last_name apellido_creador,
                    ux.sexo sexo_creador,

                    IFNULL( CONCAT('".$this->utilidades->get_url_img()."fotos_colegios/', nodocolegio.logo,'') ,
                        CONCAT('".$this->utilidades->get_url_img()."fotos_colegios/', 'default_esc.png','' ) ) logo,
                    groups.name nombre_grupo,
                    users_groups.group_alias nombre_grupo_alias,
                    groups.aliasM nombre_grupo_aliasM,
                    groups.aliasF nombre_grupo_aliasF");  
            $this->db->from('eventosusers');
            $this->db->where('eventosusers.users_id',$userid);
            $this->db->join('users','eventosusers.destinatario = users.id');
            
            $this->db->join('(SELECT eventos.*,COUNT(files.id) as cantfiles FROM eventos LEFT JOIN files ON eventos.id = files.   evento_id WHERE 1 GROUP BY eventos.id) T','eventosusers.eventos_id = T.id');

            $this->db->join("users ux", "T.users_id = ux.id");
            $this->db->join("users_groups", "users_groups.user_id = ux.id");

            $this->db->join("groups", "groups.id = T.group_id");
            $this->db->join('nodocolegio','nodocolegio.id = T.colegio_id');              

            //$this->db->where('eventosusers.alta','a'); 
            $this->db->where('eventosusers.alta != ','b'); 
            $this->db->where('eventosusers.visto','0'); 
            $this->db->where('eventosusers.notificado','0'); 
            //$this->db->where('eventosusers.medio !=','n');  
            $this->db->where('users_groups.estado', 1);
            

             $this->db->where('T.estado', 1);

             $this->db->where('T.standby', 0);

            //$this->db->group_by('eventosusers.id');
            $this->db->group_by("eventosusers.eventos_id, eventosusers.users_id, eventosusers.destinatario");
            

            //$this->db->group_by('eventosusers.id');
            //$this->db->limit(5);
            $this->db->order_by("T.fechaCreacion", "desc");
            $query = $this->db->get();
            return $query->result();*/
            // $sql = "CALL obtener_eventos_recientes(".$userid.")";
             $sql = "SELECT T.id evento_id, 
            T.titulo, 
            T.descripcion, 
            T.fechaInicio, 
            T.confirm, 
            T.fechaCreacion,
            T.group_id,
            T.users_id,
            eventosusers.aceptado,
            eventosusers.visto, 
            eventosusers.id, 
            eventosusers.me_gusta, 
            eventosusers.destinatario,
            nodocolegio.logo
            
            FROM eventosusers
            JOIN eventos T ON eventosusers.eventos_id = T.id
            JOIN nodocolegio ON nodocolegio.id = T.colegio_id
            WHERE eventosusers.users_id = $user_id
            AND eventosusers.alta != 'b'
            AND eventosusers.visto = 0
            AND eventosusers.notificado = 0
            AND YEAR(T.fechaCreacion) >= nodocolegio.cicloa
            AND T.standby =0
            GROUP BY eventosusers.id, eventosusers.eventos_id, eventosusers.users_id, eventosusers.destinatario
            ORDER BY eventosusers.fechaCreacion DESC";
            $query = $this->db->query($sql);
            $resultado = $query->result();
            //mysqli_next_result( $this->db->conn_id );
            return $resultado;
        }
        public function obtener_eventos_no_leidos($userid)//DAMIAN, es para el socket trucho
        {
            $this->db->select("T.id evento_id,
                    T.titulo,
                    T.descripcion,
                    users.first_name nombre,
                    users.last_name apellido,
                    T.fechaInicio,
                    T.confirm,
                    T.fechaCreacion,
                    eventosusers.aceptado,
                    eventosusers.visto,
                    eventosusers.id,
                    eventosusers.me_gusta,
                    eventosusers.destinatario,
                    cantfiles,
                    IFNULL( CONCAT('".$this->utilidades->get_url_img()."fotos_usuarios/', users.foto,'') ,
                        CONCAT('".$this->utilidades->get_url_img()."fotos_usuarios/', 'default-avatar.png','' ) ) foto,
                    ux.first_name nombre_creador,
                    ux.last_name apellido_creador,
                    ux.sexo sexo_creador,


                    IFNULL( CONCAT('".$this->utilidades->get_url_img()."fotos_colegios/', nodocolegio.logo,'') ,
                        CONCAT('".$this->utilidades->get_url_img()."fotos_colegios/', 'default_esc.png','' ) ) logo,
                    groups.name nombre_grupo,
                    users_groups.group_alias nombre_grupo_alias,
                    groups.aliasM nombre_grupo_aliasM,
                    groups.aliasF nombre_grupo_aliasF");  
            $this->db->from('eventosusers');
            $this->db->where('eventosusers.users_id',$userid);
            $this->db->join('users','eventosusers.destinatario = users.id');
            
            $this->db->join('(SELECT eventos.*,COUNT(files.id) as cantfiles FROM eventos LEFT JOIN files ON eventos.id = files.   evento_id WHERE 1 GROUP BY eventos.id) T','eventosusers.eventos_id = T.id');

            $this->db->join("users ux", "T.users_id = ux.id");
            $this->db->join("users_groups", "users_groups.user_id = ux.id");

            $this->db->join("groups", "groups.id = T.group_id");
            $this->db->join('nodocolegio','nodocolegio.id = T.colegio_id');              

            // $this->db->where('eventosusers.alta','a'); 
            $this->db->where('eventosusers.alta != ','b'); 
            $this->db->where('eventosusers.visto','0'); 
            
            //$this->db->where('eventosusers.medio !=','n');  
            $this->db->where('users_groups.estado', 1);
            

             $this->db->where('T.estado', 1);

            //$this->db->group_by('eventosusers.id');
            $this->db->group_by("eventosusers.eventos_id, eventosusers.users_id, eventosusers.destinatario");
            

            //$this->db->group_by('eventosusers.id');
            //$this->db->limit(5);
            $this->db->order_by("T.fechaCreacion", "desc");
            $query = $this->db->get();
            return $query->result();
        }
        public function registrar_noficacion($id_evento)//damian // es antiguo funciona con la web. damian 03/10/2019
        {
            //$time = time();
            $this->db->set('notificado', 1);
            
            $this->db->where('id', $id_evento);
           // $this->db->where('users_id', $userid);
            $this->db->update('eventosusers');
        }
        public function set_notificado($id_evento)//damian nuevo para la app movil del jose
        {
            //$time = time();
            //contador de vistos, obtener eventouser para sacar el id del evento
            

            $this->db->set('notificado', 1, FALSE);
            $fecha = date("Y-m-d H:i:s");
            $this->db->set('fecha_notificado', "'".$fecha."'", FALSE);
            $this->db->where('id', $id_evento);
           // $this->db->where('users_id', $userid);
            $this->db->update('eventosusers');
            

        }
        public function cargar_mas_eventos_nuevos($ultimo_id, $userid)//Seba
        {
            $this->db->select("T.id evento_id,
                    T.titulo,
                    T.descripcion,
                    users.first_name nombre,
                    users.last_name apellido,
                    T.fechaInicio,
                    T.confirm,
                    eventosusers.aceptado,
                    eventosusers.visto,
                    eventosusers.id,
                    eventosusers.me_gusta,
                    eventosusers.destinatario,
                    cantfiles,
                    IFNULL(nodocolegio.logo, 'http://e-nodos.com/img/default_esc.png') logo,
                    IFNULL(users.foto, 'http://e-nodos.com/img/fotos_usuarios/default-avatar.png') foto,
                    ux.first_name nombre_creador,
                    ux.last_name apellido_creador,
                    ux.sexo sexo_creador,

                    groups.name nombre_grupo,
                    users_groups.group_alias nombre_grupo_alias,
                    groups.aliasM nombre_grupo_aliasM,
                    groups.aliasF nombre_grupo_aliasF");  
            $this->db->from('eventosusers');
            $this->db->join('users','eventosusers.destinatario = users.id');
            
            $this->db->join('(SELECT eventos.*,COUNT(files.id) as cantfiles FROM eventos LEFT JOIN files ON eventos.id = files.   evento_id WHERE 1 GROUP BY eventos.id) T','eventosusers.eventos_id = T.id');

            $this->db->join("users ux", "T.users_id = ux.id");
            $this->db->join("users_groups", "users_groups.user_id = ux.id");

            $this->db->join("groups", "groups.id = T.group_id");            
            $this->db->join('nodocolegio','nodocolegio.id = T.colegio_id');           

            $this->db->where('eventosusers.users_id',$userid);
            $this->db->where('users_groups.estado', 1);
            $this->db->where('eventosusers.id > ', $ultimo_id);
            $this->db->where('eventosusers.visto',0);
           // $this->db->where('eventosusers.medio !=','n');

             $this->db->where('T.estado', 1);

            
            $this->db->group_by("eventosusers.eventos_id, eventosusers.users_id, eventosusers.destinatario");

            $this->db->limit('5');
            $this->db->order_by("eventosusers.id", "asc");
            $query = $this->db->get();
            return $query->result();
        }
             
        
        public function obtener_eventosuser_ByIdevento($idevento)//SEBA
        {
            $sql = "
                SELECT * 
                FROM eventosusers    
                JOIN users ON eventosusers.users_id=users.id     
                WHERE eventosusers.eventos_id = $idevento
                AND eventosusers.alta = 'a'
                GROUP BY users_id"; //esto esta bien asi, lo usamos en el modificar evento. :)
            $query = $this->db->query($sql);
            return $query->result();
        }

        public function obtener_eventosuser_ByIdevento2($idevento)//ANDRES
        {
            $sql = "
                SELECT * 
                FROM eventosusers    
                JOIN users ON eventosusers.users_id=users.id     
                WHERE eventosusers.eventos_id = $idevento
                GROUP BY users_id"; //esto esta bien asi, lo usamos en el modificar evento. :)
            $query = $this->db->query($sql);

            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
    
        }

        public function obtener_eventosuser_ParaEnviar($idevento) //basado en medelo_usuario->get_usuariosParaEnviar, misma logica distinta fuente de datos.
        {
            $sql = "
                SELECT eventosusers.*,  
                        users.recibir_notificacion,
                        users.email,
                        users.active,
                        users.pago,
                        groups.name,
                        T.alumno_id,
                        T.pago_alumno
                FROM eventosusers    
                JOIN users ON eventosusers.users_id = users.id   
                JOIN groups ON eventosusers.group_id=groups.id 
                LEFT JOIN 
                    (
                     SELECT users.id as alumno_id, pago as pago_alumno
                     FROM users
                    )T
                    ON eventosusers.destinatario = T.alumno_id AND eventosusers.users_id<>eventosusers.destinatario
                WHERE eventosusers.eventos_id = $idevento
                AND eventosusers.alta = 'a'
                AND eventosusers.motivo = 'no_auto'
                AND eventosusers.enviado = 0
                GROUP BY eventosusers.users_id"; //lo usamos en el publicar evento :)
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }

         public function obtener_eventosuser_ParaEnviar_Debug($idevento) //basado en medelo_usuario->get_usuariosParaEnviar, misma logica distinta fuente de datos, solo para nodos en caso de error, volver a enviar las notif. SOLAMENTE a los que no se envió
        {
            $sql = "
                SELECT eventosusers.*,  
                        users.recibir_notificacion,
                        users.email,
                        users.active,
                        users.pago,
                        groups.name,
                        T.alumno_id,
                        T.pago_alumno
                FROM eventosusers    
                JOIN users ON eventosusers.users_id = users.id   
                JOIN users_groups ON users_groups.user_id=users.id 
                JOIN groups ON users_groups.group_id=groups.id 
                LEFT JOIN 
                    (
                     SELECT users.id as alumno_id, pago as pago_alumno
                     FROM users
                    )T
                    ON eventosusers.destinatario = T.alumno_id AND eventosusers.users_id<>eventosusers.destinatario
                WHERE eventosusers.eventos_id = $idevento
                AND eventosusers.alta = 'a'
                AND eventosusers.medio = 'n'
                AND eventosusers.motivo = 'no_pago'
                AND eventosusers.enviado = 0
                GROUP BY eventosusers.users_id"; //lo usamos en el publicar evento :)
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }

        /*public function obtener_eventosuser_ByIdevento_visto($idevento, $groupby = 1)//ANDRES
        {
            $sql = "
                SELECT *
                FROM eventosusers    
                WHERE eventosusers.eventos_id = $idevento
                AND eventosusers.alta <> 'b'
                AND eventosusers.visto = 1
                AND eventosusers.enviado = 1
                "; //esto esta bien asi, lo usamos en el modificar evento. :)

            //JOIN users ON eventosusers.users_id=users.id   

            if($groupby)
                $sql .= " GROUP BY users_id";

            $query = $this->db->query($sql);
            return $query->result();
        }*/

        public function obtener_eventosuser_ByIdevento_visto($idevento, $groupby = 1, $colegio)//obtiene no solamente los vistos sino tambien chequea que tenga al menos un rol activo dentro del colegio. Esto se usa para el EDITAR EVENTO, la idea es que no envie a aquellos user que ya no pertenecen al colegio.
        {
            $sql = "
                SELECT eventosusers.*, users.email 
                FROM eventosusers
                JOIN users_groups ON eventosusers.users_id = users_groups.user_id 
                JOIN users ON eventosusers.users_id=users.id 
                WHERE eventosusers.eventos_id = $idevento
                AND eventosusers.alta <> 'b'
                AND eventosusers.visto = 1
                AND eventosusers.enviado = 1
                AND users_groups.colegio_id = $colegio
                AND users_groups.estado = 1
                AND users.active = 1
                AND users.recibir_notificacion = 1
                GROUP BY eventosusers.id
                "; //esto esta bien asi, lo usamos en el modificar evento. :)
 
            //echo $sql;
            if($groupby)
                $sql .= " GROUP BY users_id";

            $query = $this->db->query($sql);
            if($query->num_rows()>0)
                return $query->result();
            else
                return false;
        }



        public function obtener_eventosuser_ByIdevento_delete($idevento)//ANDRES
        {
            $sql = "
                SELECT * 
                FROM eventosusers   
                WHERE eventosusers.eventos_id = $idevento
                "; 
            $query = $this->db->query($sql);
            return $query->result();
        }

        public function obtener_filtro($idevento)
        {
            $sql = "
                SELECT * 
                FROM filtroevento    
                WHERE evento_id = $idevento LIMIT 1
                ";
            $query = $this->db->query($sql);

            if($query->num_rows()>0)
                return $query->row();
            else
                return false;
        }
        
        public function obtener_eventos_mesanio($mes, $anio, $userid)//SEBA
        {
            $sql = "
                SELECT DISTINCT eventos.id evento_id,
                    eventos.titulo,
                    users.first_name nombre,
                    users.last_name apellido,
                    eventos.fechaInicio,
                    eventos.confirm,
                    eventosusers.aceptado,
                    eventosusers.visto,
                    users.foto
                FROM eventosusers              
                JOIN users ON eventosusers.destinatario = users.id
                JOIN eventos ON eventosusers.eventos_id = eventos.id
                WHERE (eventosusers.users_id = $userid) AND 
                      (MONTH(fechaInicio) = $mes) AND 
                      (YEAR(fechainicio) = $anio)
                AND eventos.estado = 1  
                ORDER BY fechaInicio DESC";
            $query = $this->db->query($sql);
            return $query->result();
        }


        public function obtener_eventos_fecha($fecha, $userid)//SEBA
        {
            $sql = "
                SELECT DISTINCT eventos.id evento_id,
                    eventos.titulo,
                    users.first_name nombre,
                    users.last_name apellido,
                    eventos.fechaInicio,
                    eventos.confirm,
                    eventosusers.aceptado,
                    eventosusers.visto,
                    users.foto
                FROM eventosusers
                JOIN users ON eventosusers.users_id = users.id
                JOIN eventos ON eventosusers.eventos_id = eventos.id
                WHERE (eventosusers.users_id  = $userid) AND 
                      (fechaInicio LIKE '%$fecha%')
                AND eventos.estado = 1
                ORDER BY fechaInicio DESC";
            $query = $this->db->query($sql);
            return $query->result();
        }

        public function obtener_evento($id_evento)//damian
        {
            /*
            $sql = "
                SELECT eventos.id as eventid,  
                       titulo,confirm, fechaCreacion, fechaInicio, fechafin, descripcion, lugar, users.id as userid, users.first_name, users.last_name, nodocolegio.id as colegioid, nodocolegio.nombre as nombrecolegio
                FROM eventos              
                JOIN users ON eventos.users_id = users.id
                JOIN nodocolegio ON eventos.colegio_id = nodocolegio.id
                WHERE eventos.id = '$id_evento'
                LIMIT 1
            ";*/

            $sql = "SELECT eventos.id as eventid,  
                       titulo,confirm, 
                       fechaCreacion, 
                       fechaInicio, 
                       fechafin, 
                       descripcion, 
                       lugar, 
                       users.id as userid, 
                       users.first_name, 
                       users.last_name, 
                       nodocolegio.id as colegioid, 
                       nodocolegio.nombre as nombrecolegio,
                       groups.name,
                       users.sexo,
                       groups.aliasF,
                       groups.aliasM,
                       users_groups.group_alias,
                       groups.id as id_groups,
                       standby
                    FROM eventos              
                    JOIN users ON eventos.users_id = users.id
                    JOIN groups ON eventos.group_id = groups.id
                    JOIN users_groups ON users_groups.group_id = groups.id AND users_groups.user_id=users.id

                    JOIN nodocolegio ON eventos.colegio_id = nodocolegio.id
                    WHERE eventos.id = '$id_evento'
                    AND eventos.estado = 1
                    LIMIT 1 ";
             

            $query = $this->db->query($sql);
            return $query->row();
        }

        public function obtener_datosEvento($id_evento)//damian
        {

            $sql = "SELECT *
                    FROM eventos              
                    WHERE id = '$id_evento'
                    LIMIT 1 ";

            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->row();
            }
            else{
                return false;
            }
        }

        public function obtener_evento_users($event_id, $user_id)//damian
        {
            /*$this->db->select("T.id evento_id,
                    T.titulo,
                    T.descripcion,
                    users.first_name nombre,
                    users.last_name apellido,
                    T.fechaInicio,
                    T.confirm,
                    T.fechaCreacion,
                    eventosusers.aceptado,
                    eventosusers.visto,
                    eventosusers.id,
                    eventosusers.me_gusta,
                    eventosusers.destinatario,
                    cantfiles,
                    IFNULL( CONCAT('".$this->utilidades->get_url_img()."fotos_usuarios/', users.foto,'') ,
                        CONCAT('".$this->utilidades->get_url_img()."fotos_usuarios/', 'default-avatar.png','' ) ) foto,
                    ux.first_name nombre_creador,
                    ux.last_name apellido_creador,
                    ux.sexo sexo_creador,
                    

                    IFNULL( CONCAT('".$this->utilidades->get_url_img()."fotos_colegios/', nodocolegio.logo,'') ,
                        CONCAT('".$this->utilidades->get_url_img()."fotos_colegios/', 'default_esc.png','' ) ) logo,
                    groups.name nombre_grupo,
                    users_groups.group_alias nombre_grupo_alias,
                    groups.aliasM nombre_grupo_aliasM,
                    groups.aliasF nombre_grupo_aliasF");  
            $this->db->from('eventosusers');
            $this->db->where('eventosusers.users_id',$id_user);
            $this->db->where('eventosusers.eventos_id',$id_evento);
            $this->db->where('users_groups.estado', 1);
            $this->db->where('T.estado', 1);
            $this->db->join('users','eventosusers.destinatario = users.id');
            
            $this->db->join('(SELECT eventos.*,COUNT(files.id) as cantfiles FROM eventos LEFT JOIN files ON eventos.id = files.   evento_id WHERE 1 GROUP BY eventos.id) T','eventosusers.eventos_id = T.id');

            $this->db->join("users ux", "T.users_id = ux.id");
            $this->db->join("users_groups", "users_groups.user_id = ux.id");
            $this->db->join("groups", "groups.id = users_groups.group_id");
            $this->db->join('nodocolegio','nodocolegio.id = T.colegio_id'); 

            $this->db->where('T.standby', 0);               
            
            $this->db->group_by("eventosusers.eventos_id, eventosusers.users_id, eventosusers.destinatario");

            //$this->db->order_by("eventosusers.id", "desc");
           
            $query = $this->db->get();
            return $query->result();*/

            $sql = "SELECT T.id $evento_id, 
            T.titulo, 
            T.descripcion, 
            T.fechaInicio, 
            T.confirm, 
            T.fechaCreacion,
            T.group_id,
            T.users_id,
            eventosusers.aceptado,
            eventosusers.visto, 
            eventosusers.id, 
            eventosusers.me_gusta, 
            eventosusers.destinatario,
            eventosusers.notificado,
            eventosusers.motivo,
            eventosusers.medio,
            eventosusers.fecha_notificado,
            eventosusers.favorito,
            T.destacado,
            T.fecha_modificacion,
            T.modificado,
            nodocolegio.logo
            FROM eventosusers
            JOIN eventos T ON eventosusers.eventos_id = T.id
            JOIN nodocolegio ON nodocolegio.id = T.colegio_id
            WHERE eventosusers.users_id = $user_id
            AND eventosusers.alta != 'b'
            AND eventosusers.eventos_id
            AND YEAR(T.fechaCreacion) = nodocolegio.cicloa
            AND T.standby =0
            GROUP BY eventosusers.eventos_id, eventosusers.users_id, eventosusers.destinatario";
            //$sql = "CALL obtener_evento_user_id(".$userid.")";
            $query = $this->db->query($sql);
            $resultado = $query->result();
            //mysqli_next_result( $this->db->conn_id );
            return $resultado;
        }

        public function marcar_visto($id_evento)//SEBA
        {
            //$time = time();
            //contador de vistos, obtener eventouser para sacar el id del evento
            $sql_eventosusers = "SELECT * FROM `eventosusers` where id = $id_evento";
            $sql_eventosusers = $this->db->query($sql_eventosusers);
            
            if($sql_eventosusers->num_rows()>0)
            {
                $sql_eventosusers = $sql_eventosusers->row();
                if($sql_eventosusers->visto == 0)
                {
                    //obtener evento
                    $sql_eventos = "SELECT * FROM `eventos` WHERE id = ".$sql_eventosusers->eventos_id;
                    $sql_eventos = $this->db->query($sql_eventos);
                    if($sql_eventos->num_rows()>0)
                    {
                        $sql_eventos = $sql_eventos->row();
                        $cont = $sql_eventos->cont_vistos;
                        $cont++;
                        $sql_update = "UPDATE eventos SET cont_vistos= $cont WHERE id = ".$sql_eventos->id;
                        $sql_update = $this->db->query($sql_update);

                        
                    }
                }
                    
                    
            }

            $this->db->set('visto', 1, FALSE);
            $fecha = date("Y-m-d H:i:s");
            $this->db->set('fechaVisto', "'".$fecha."'", FALSE);
            $this->db->where('id', $id_evento);
           // $this->db->where('users_id', $userid);
            $this->db->update('eventosusers');
            

        }
        public function marcar_favorito($id_evento)//damian
        {
            //$time = time();
            //contador de vistos, obtener eventouser para sacar el id del evento
            

            $this->db->set('favorito', 1, FALSE);
            //$fecha = date("Y-m-d H:i:s");
           // $this->db->set('fechaVisto', "'".$fecha."'", FALSE);
            $this->db->where('id', $id_evento);
           // $this->db->where('users_id', $userid);
            $this->db->update('eventosusers');
            

        }
        public function desmarcar_favorito($id_evento)//damian
        {
            //$time = time();
            //contador de vistos, obtener eventouser para sacar el id del evento
            

            $this->db->set('favorito', 0, FALSE);
            //$fecha = date("Y-m-d H:i:s");
           // $this->db->set('fechaVisto', "'".$fecha."'", FALSE);
            $this->db->where('id', $id_evento);
           // $this->db->where('users_id', $userid);
            $this->db->update('eventosusers');
            

        }

        public function marcar_visto_detalle($datos)//SEBA
        {
            //$time = time();
            $this->db->set('visto', 1, FALSE);
            $fecha = date("Y-m-d H:i:s");
            $this->db->set('fechaVisto', "'".$fecha."'", FALSE);
            $this->db->where('token_id', $datos["token_id"]);
            $this->db->where('eventosusers_id', $datos["eventosusers_id"]);
            $this->db->update('eventosusers_detalle');            
        }

        public function marcar_aceptado($id_evento)//SEBA
        {
            $sql_eventosusers = "SELECT * FROM `eventosusers` where id = $id_evento";
            $sql_eventosusers = $this->db->query($sql_eventosusers);
            if($sql_eventosusers->num_rows()>0)
            {
                if($sql_eventosusers->row()->aceptado != 1)
                {
                    $fecha = date("Y-m-d H:i:s");
                    $this->db->set('fechaConfirmacion', "'".$fecha."'", FALSE);
                    $this->db->set('aceptado', 1, FALSE);
                    $this->db->where('id', $id_evento);
                    $this->db->update('eventosusers');
                    $sql_eventosusers = "SELECT * FROM `eventosusers` where id = $id_evento";
                    $sql_eventosusers = $this->db->query($sql_eventosusers);
                    
                    if($sql_eventosusers->num_rows()>0)
                    {
                        $sql_eventosusers = $sql_eventosusers->row();
                        //obtener evento
                        $sql_eventos = "SELECT * FROM `eventos` WHERE id = ".$sql_eventosusers->eventos_id;
                        $sql_eventos = $this->db->query($sql_eventos);
                        if($sql_eventos->num_rows()>0)
                        {
                            $sql_eventos = $sql_eventos->row();
                            $cont_confirmados = $sql_eventos->cont_confirmados;
                            $cont_confirmados++;
                            $sql_update = "UPDATE eventos SET cont_confirmados= $cont_confirmados WHERE id = ".$sql_eventos->id;
                            $sql_update = $this->db->query($sql_update);

                            if($sql_eventos->cont_negados > 0)
                            {
                                $cont_negados = $sql_eventos->cont_negados;
                                $cont_negados--;
                                $sql_update = "UPDATE eventos SET cont_negados= $cont_negados WHERE id = ".$sql_eventos->id;
                                $sql_update = $this->db->query($sql_update);
                            }

                            
                        }
                            
                    }
                }
            }
                    
        }

        public function marcar_aceptado_detalle($datos)//SEBA
        {
            $fecha = date("Y-m-d H:i:s");
            $this->db->set('fechaConfirmacion', "'".$fecha."'", FALSE);
            $this->db->set('aceptado', 1, FALSE);
            $this->db->where('token_id', $datos["token_id"]);
            $this->db->where('eventosusers_id', $datos["eventosusers_id"]);
            $this->db->update('eventosusers_detalle');
        }

        public function marcar_rechazado($id_evento)//SEBA
        {
            $sql_eventosusers = "SELECT * FROM `eventosusers` where id = $id_evento";
            $sql_eventosusers = $this->db->query($sql_eventosusers);
            if($sql_eventosusers->num_rows()>0)
            {
                if($sql_eventosusers->row()->aceptado != -1)
                {
                    $fecha = date("Y-m-d H:i:s");
                    $this->db->set('fechaConfirmacion', "'".$fecha."'", FALSE);
                    $this->db->set('aceptado', -1, FALSE);
                    $this->db->where('id', $id_evento);
                    $this->db->update('eventosusers');
                    $sql_eventosusers = "SELECT * FROM `eventosusers` where id = $id_evento";
                    $sql_eventosusers = $this->db->query($sql_eventosusers);
                    if($sql_eventosusers->num_rows()>0)
                    {
                        $sql_eventosusers = $sql_eventosusers->row();
                        //obtener evento
                        $sql_eventos = "SELECT * FROM `eventos` WHERE id = ".$sql_eventosusers->eventos_id;
                        $sql_eventos = $this->db->query($sql_eventos);
                        if($sql_eventos->num_rows()>0)
                        {
                            $sql_eventos = $sql_eventos->row();
                           

                            if($sql_eventos->cont_confirmados > 0)
                            {
                                $cont_confirmados = $sql_eventos->cont_confirmados;
                                $cont_confirmados--;
                                $sql_update = "UPDATE eventos SET cont_confirmados= $cont_confirmados WHERE id = ".$sql_eventos->id;
                                $sql_update = $this->db->query($sql_update);
                            }

                            
                                $cont_negados = $sql_eventos->cont_negados;
                                $cont_negados++;
                                $sql_update = "UPDATE eventos SET cont_negados= $cont_negados WHERE id = ".$sql_eventos->id;
                                $sql_update = $this->db->query($sql_update);
                            

                            
                        }
                            
                    }
                }
            }
        }
        
        public function marcar_rechazado_detalle($datos)//SEBA
        {
            $fecha = date("Y-m-d H:i:s");
            $this->db->set('fechaConfirmacion', "'".$fecha."'", FALSE);
            $this->db->set('aceptado', -1, FALSE);
            $this->db->where('token_id', $datos["token_id"]);
            $this->db->where('eventosusers_id', $datos["eventosusers_id"]);
            $this->db->update('eventosusers_detalle');
        }

        public function marcar_megusta($id_evento)//SEBA
        {
            $fecha = date("Y-m-d H:i:s");
            $this->db->set('fechaMegusta', "'".$fecha."'", FALSE);
            $this->db->set('me_gusta', 1, FALSE);
            $this->db->where('id', $id_evento);
            $this->db->update('eventosusers');

            $sql_eventosusers = "SELECT * FROM `eventosusers` where id = $id_evento";
            $sql_eventosusers = $this->db->query($sql_eventosusers);
            if($sql_eventosusers->num_rows()>0)
            {
                $sql_eventosusers = $sql_eventosusers->row();
                //obtener evento
                $sql_eventos = "SELECT * FROM `eventos` WHERE id = ".$sql_eventosusers->eventos_id;
                $sql_eventos = $this->db->query($sql_eventos);
                if($sql_eventos->num_rows()>0)
                {
                    $sql_eventos = $sql_eventos->row();
                        $cont_me_gusta = $sql_eventos->cont_me_gusta;
                        $cont_me_gusta++;
                        $sql_update = "UPDATE eventos SET cont_me_gusta= $cont_me_gusta WHERE id = ".$sql_eventos->id;
                        $sql_update = $this->db->query($sql_update);
                }
                    
            }

        }

        public function marcar_megusta_detalle($datos)//SEBA
        {
            $fecha = date("Y-m-d H:i:s");
            $this->db->set('fechaMegusta', "'".$fecha."'", FALSE);
            $this->db->set('me_gusta', 1, FALSE);
            $this->db->where('token_id', $datos["token_id"]);
            $this->db->where('eventosusers_id', $datos["eventosusers_id"]);
            $this->db->update('eventosusers_detalle');
        }

        public function desmarcar_megusta($id_evento)//SEBA
        {
            $fecha = date("Y-m-d H:i:s");
            $this->db->set('fechaMegusta', "'".$fecha."'", FALSE);
            $this->db->set('me_gusta', 0, FALSE);
            $this->db->where('id', $id_evento);
            $this->db->update('eventosusers');
            $sql_eventosusers = "SELECT * FROM `eventosusers` where id = $id_evento";
            $sql_eventosusers = $this->db->query($sql_eventosusers);
            if($sql_eventosusers->num_rows()>0)
            {
                $sql_eventosusers = $sql_eventosusers->row();
                //obtener evento
                $sql_eventos = "SELECT * FROM `eventos` WHERE id = ".$sql_eventosusers->eventos_id;
                $sql_eventos = $this->db->query($sql_eventos);
                if($sql_eventos->num_rows()>0)
                {
                    $sql_eventos = $sql_eventos->row();
                    if($sql_eventos->cont_me_gusta > 0)
                    {
                        $cont_me_gusta = $sql_eventos->cont_me_gusta;
                        $cont_me_gusta--;
                        $sql_update = "UPDATE eventos SET cont_me_gusta= $cont_me_gusta WHERE id = ".$sql_eventos->id;
                        $sql_update = $this->db->query($sql_update);
                    }
                }
                    
            }
        }

        public function desmarcar_megusta_detalle($datos)//SEBA
        {
            $fecha = date("Y-m-d H:i:s");
            $this->db->set('fechaMegusta', "'".$fecha."'", FALSE);
            $this->db->set('me_gusta', 0, FALSE);
            $this->db->where('token_id', $datos["token_id"]);
            $this->db->where('eventosusers_id', $datos["eventosusers_id"]);
            $this->db->update('eventosusers_detalle');
        }

        public function insertar_evento($data)//ANDRES
        {
            $query = $this->db->insert("eventos",$data);
            return $query;
        }

        public function insertar_eventousers($data)//ANDRES
        {
            $query = $this->db->insert("eventosusers",$data);            
            return $query;
        }

        public function insertar_eventousers2($data)//ANDRES
        {
            $query = $this->db->insert_batch("eventosusers",$data); //inserta varios registros    
            return $query;
        }

        public function insertar_eventousers_detalle($data)//SEBA
        {
            $query = $this->db->insert("eventosusers_detalle",$data);            
            return $query;
        }  

        public function insertar_eventousers_detalle2($data)//SEBA
        {
            $query = $this->db->insert_batch("eventosusers_detalle",$data);            
            return $query;
        }       

        public function update_evento($data, $id)//ANDRES
        {
            $this->db->where('id', $id);
            $query = $this->db->update("eventos",$data);

            //var_dump($query);
            return $query;
        }

        public function update_eventosusers($data, $id){//SEBA
            $this->db->where('id', $id);
            $query = $this->db->update("eventosusers",$data);
            return $query;
        }

        public function update_eventosusers2($data){
            //$this->db->where('id', $id);
            $query = $this->db->update_batch("eventosusers",$data,'id');
            return $query;
        }

        public function cambiar_campo_alta_eventosusers($idevento,$valor,$users_id)
        {
            $sql = "UPDATE eventosusers SET alta='$valor' 
                    WHERE eventos_id = $idevento and users_id = $users_id";
            $query = $this->db->query($sql);
            //print_r($this->db->last_query());
           // die();
            return $query;
        }

        public function cambiar_campo_alta_eventosusers2($valor,$ids)
        {
            $sql = "UPDATE eventosusers SET alta='$valor' 
                    WHERE id IN ($ids)";
            $query = $this->db->query($sql);
            //print_r($this->db->last_query());
           // die();
            return $query;
        }

        public function cambiar_campo_alta_eventosusers_detalle($valor,$ids,$idevento)
        {
            $sql = "UPDATE eventosusers_detalle SET alta='$valor' 
                    WHERE evento_id=$idevento AND user_id IN ($ids)";
            $query = $this->db->query($sql);
            return $query;
        }

        public function delete_evento($idevento)
        {
            $sql = "UPDATE eventos SET estado=0 WHERE id = $idevento";
            $query = $this->db->query($sql);

            $sql2 = "UPDATE filtroevento SET estado=0 WHERE evento_id = $idevento";
            $query2 = $this->db->query($sql2);

            return $query;
        }
        
        public function delete_eventouser($idevento)
        {
            $sql = "DELETE FROM eventosusers WHERE eventos_id = $idevento";
            $query = $this->db->query($sql);
        
            return $query;
        }

        public function delete_eventouser_detalle($ids_eventosusers)
        {
            $sql = "DELETE FROM eventosusers_detalle WHERE eventosusers_id IN ($ids_eventosusers)";
            $query = $this->db->query($sql);

            return $query;
        }

        public function delete_eventouser_detalle2($idevento)
        {
            $sql = "DELETE FROM eventosusers_detalle WHERE evento_id = $idevento";
            $query = $this->db->query($sql);
        
            return $query;
        }

        public function update_enviado_eventousers($eventoId,$medio,$alta,$valores)//ANDRES
        {
            $this->db->where('eventos_id', $eventoId);
            $this->db->where('medio', $medio);
            $this->db->where('alta', $alta);
            $this->db->where("(motivo != 'email_invalid' OR motivo IS null)");
           
            $query = $this->db->update("eventosusers",$valores);
            //print( $this->db->last_query() ); die();
            return $query;
        }

          public function insertar_filtroevento($data)//ANDRES
        {
            $query = $this->db->insert("filtroevento",$data);            
            return $query;
        }

        public function obtener_privilegios_especiales($userid)
        {
            $sql = "SELECT * FROM groups JOIN users_groups on users_groups.group_id = groups.id where users_groups.user_id = $userid and (groups.name = 'Representante Legal' or groups.name = 'Directivo' or groups.name = 'Nodos') GROUP BY users_groups.colegio_id";
            $query = $this->db->query($sql);

            return $query;
        }
        public function obtener_privilegios_eventos_roles($userid)
        {
            $sql = "SELECT reglas_comunicacion.roles_eventos FROM `reglas_comunicacion` JOIN users_groups on users_groups.group_id = reglas_comunicacion.group_id WHERE users_groups.user_id = $userid and reglas_comunicacion.roles_eventos <> ''";
            $query = $this->db->query($sql);

            return $query;
        }
        public function obtener_cant_me_gusta_x_evento($evento_id)
        {
            $sql = "SELECT COUNT(eventosusers.me_gusta) as cant FROM eventosusers where eventos_id = $evento_id and me_gusta = 1";
            $query = $this->db->query($sql);
            $query = $query->row();
            return $query->cant;
        }


        public function obtener_eventos_autorizar($colegioId, $rolesId)
        {
            $sql = "
                SELECT eventos.id,
                       titulo,
                       confirm, 
                       fechaCreacion, 
                       fechaInicio, 
                       fechafin, 
                       descripcion, 
                       lugar, 
                       users.id as userid, 
                       users.first_name, 
                       users.last_name, 
                       groups.name as rolname
                FROM eventos   
                JOIN users ON users.id = eventos.users_id  
                JOIN groups ON groups.id = eventos.group_id  
                WHERE eventos.standby = 1
                AND eventos.estado = 1
                AND eventos.colegio_id = $colegioId
                AND eventos.group_id IN ($rolesId)
                GROUP BY eventos.id
                ORDER BY eventos.fechaCreacion"; 
           // echo $sql;
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }

         public function obtener_eventosCreados_autorizar($usersId, $groupsId, $idcolegio) 
        {

            $sql = "SELECT eventos.id,  
                       titulo,confirm, 
                       fechaCreacion, 
                       fechaInicio, 
                       fechafin, 
                       descripcion, 
                       lugar, 
                       users.id as userid, 
                       users.first_name, 
                       users.last_name, 
                       groups.name as rolname
                    FROM eventos              
                    JOIN users ON eventos.users_id = users.id
                    JOIN groups ON eventos.group_id = groups.id
                    WHERE eventos.standby=1
                    AND eventos.users_id IN ($usersId)
                    AND eventos.group_id IN ($groupsId)
                    AND eventos.colegio_id = $idcolegio
                    AND eventos.estado=1
                    GROUP BY eventos.id
                    ORDER BY eventos.fechaCreacion";

           // echo $sql;

            $query = $this->db->query($sql);
           
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }


        public function get_eventouser($data)
        {
            $this->db->where($data);
            $query = $this->db->get("eventosusers");

            if($query->num_rows()>0){
                return $query->row();
            }
            else{
                return false;
            }    
        }

        public function get_eventosusers($data)
        {
            $this->db->where($data);
            $query = $this->db->get("eventosusers");

            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }    
        }



        public function obtener_grupo_menorOrden($userId, $idcolegio) 
        {
            /*$sql = "SELECT 
                       groups.id,
                       groups.name,
                       groups.aliasM,
                       groups.aliasF,
                       users_groups.group_alias
                    FROM users_groups              
                    JOIN groups ON users_groups.group_id = groups.id
                    WHERE users_groups.estado=1
                    AND users_groups.user_id = $userId
                    AND users_groups.colegio_id = $idcolegio
                    ORDER BY groups.orden ASC LIMIT 1";*/
            $sql = "SELECT 
                       groups.id,
                       groups.name,
                       groups.aliasM,
                       groups.aliasF,
                       users_groups.group_alias
                    FROM users_groups              
                    JOIN groups ON users_groups.group_id = groups.id
                    WHERE users_groups.user_id = $userId
                    AND users_groups.colegio_id = $idcolegio
                    ORDER BY groups.orden ASC LIMIT 1";

            $query = $this->db->query($sql);
           
            if($query->num_rows()>0)
                return $query->row();
            else
                return false;
        }
        public function get_alumnos_eventosusers($division_id,$id_evento)
        {
            $sql_alumnos = "SELECT users.id as alu_user_id,users.first_name, users.last_name,users.foto, eventos.confirm ,eventosusers.* 
            FROM eventosusers
            JOIN users on users.id = eventosusers.users_id
            join users_groups on users_groups.user_id = users.id
            JOIN inscripciones on inscripciones.alumno_id = users_groups.id
            join eventos on eventos.id = eventosusers.eventos_id
            join nodocolegio on nodocolegio.id =  eventos.colegio_id
            JOIN groups on groups.id = users_groups.group_id
            where inscripciones.divisiones_id = $division_id 
            and eventosusers.eventos_id = $id_evento and inscripciones.cicloa = nodocolegio.cicloa
            and groups.name = 'Alumno'
            group by users.id ORDER BY `users`.`last_name` ASC  ";
            $query = $this->db->query($sql_alumnos);
            return $query->result();
        }
        
        public function get_alumnos_eventosusers_x_tutores($division_id,$id_evento)
        {
            $sql_alumnos = "SELECT users.id as alu_user_id,users.first_name, users.last_name, users.foto,eventos.confirm ,eventosusers.* 
            FROM eventosusers
            JOIN users on users.id = eventosusers.destinatario
            join users_groups on users_groups.user_id = users.id
            JOIN inscripciones on inscripciones.alumno_id = users_groups.id
            join eventos on eventos.id = eventosusers.eventos_id
            join nodocolegio on nodocolegio.id =  eventos.colegio_id
            JOIN groups on groups.id = users_groups.group_id
            where inscripciones.divisiones_id = $division_id 
            and eventosusers.eventos_id = $id_evento and inscripciones.cicloa = nodocolegio.cicloa
            and groups.name = 'Alumno'
            group by users.id ORDER BY `users`.`last_name` ASC  ";
            $query = $this->db->query($sql_alumnos);
            return $query->result();
        }
        public function get_tutores_alumno_eventosusers($id_evento,$alumno_id)
        {
            $sql_tutores = "SELECT users.last_name, users.first_name, users.foto,eventosusers.*
                            FROM users
                            JOIN users_groups on users_groups.user_id = users.id
                            join eventosusers on eventosusers.users_id = users.id
                            JOIN tutoralumno on tutoralumno.tutor_id = eventosusers.users_id
                            where eventosusers.eventos_id = $id_evento 
                            and tutoralumno.alumno_id = $alumno_id group by tutoralumno.tutor_id";
            $query = $this->db->query($sql_tutores);
            return $query->result();


        }
        
         //Actualiza eventosusers de los usuarios q envio por movil / mail
        public function update_eventosusers_batch($data){ //van los "id" en el array "$data"
                $arre_eventosusers_ids = array();
                $sql = 'select id,eventos_id,users_id from eventosusers where eventos_id = '.$data[0]["eventos_id"].' ';
                $sql_eventos = $this->db->query($sql);
                if($sql_eventos->num_rows()>0)
                {
                    $arre_eventosusers_ids = $sql_eventos->result_array();
                    
                    $items = count($data);
                    for ($i=0; $i < $items ; $i++) { 
                        $idUsr = $data[$i]["users_id"];
                        foreach ($arre_eventosusers_ids as $key => $value) {
                           if($value['users_id'] ==  $idUsr )
                           {
                                $data[$i]["id"] = $arre_eventosusers_ids[$key]['id'];
                                break;
                           }
                        } 
                    }
                }
                $query = 0;
                $updateData = array();
                foreach ($data as $key => $value) {
                    $yasta = 0;
                    foreach ($updateData as $key2 => $value2) {
                        if(in_array( $value['id'], $updateData[$key2]))
                        {
                            $yasta = 1;
                            break;
                        }
                        else{
                            $yasta = 0;
                        }
                    }
                   
                    if($yasta== 0 )
                    {
                        $updateData[] = array(
                            'id'=>                $value['id'],
                            'eventos_id' =>       $value['eventos_id'],
                            'users_id'=>          $value['users_id'],
                            'destinatario'=>      $value['destinatario'],
                            'medio'=>             $value['medio'],
                            'alta'=>              $value['alta'],
                            'motivo'=>            $value['motivo'],
                            'token_id'=>          $value['token_id'],
                            'enviado'=>           $value['enviado']
                            );
                    }
                    
                }
                if(count($updateData)>0)
                {
                    $query = $this->db->update_batch('eventosusers',$updateData,'id');
                }
                return $query;
        }

        public function update_eventosusers_detalle_batch($data){ //van los "id" en el array "$data"
                $arre_eventosusers_ids = array();
                $query = 0;
                if(isset($data[0]["evento_id"]))
                {
                    $sql = 'select id,evento_id,user_id from eventosusers_detalle where evento_id = '.$data[0]["evento_id"].' ';
                    $sql_eventos = $this->db->query($sql);
                    if($sql_eventos->num_rows()>0)
                    {
                        $arre_eventosusers_ids = $sql_eventos->result_array();
                        
                        $items = count($data);
                        for ($i=0; $i < $items ; $i++) { 
                            $idUsr = $data[$i]["user_id"];
                            $key = array_search($idUsr, array_column($arre_eventosusers_ids, 'user_id'));
                            if($key === false) continue;
                            if($key>=0)
                            {
                                $data[$i]["id"] = $arre_eventosusers_ids[$key]['id'];
                                $arre_eventosusers_ids[$key]['user_id'] = null; //por si hay otro con el mismo usuario pero otro id
                            }
                        }
                    }
                   
                    $updateData = array();
                    foreach ($data as $key => $value) {
                        $updateData[] = array(
                                        'id'=>               $value['id'],
                                        'evento_id' =>       $value['evento_id'],
                                        'alta'=>             $value['alta'],
                                        'token_id'=>         $value['token_id'],
                                        'user_id'=>          $value['user_id'],
                                        'enviado'=>          $value['enviado'],
                                        'motivo'=>           $value['motivo'],
                                        'status_id'=>        $value['status_id']
                                        );
                    }
                    if(count($updateData)>0)
                    {
                        $query = $this->db->update_batch('eventosusers_detalle',$updateData,'id');
                    }
                }
                return $query;
        }

            


	}