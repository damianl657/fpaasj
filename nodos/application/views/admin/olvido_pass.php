<script>

 var passApiKey = '<?php echo $passApiKey; ?>';
 var urlApi = '<?php echo $urlApi; ?>';
 </script>
 <body>

	<!-- count particles -->
	<div class="count-particles">
		<span class="js-count-particles">80</span> particles
	</div>

	<!-- particles.js container -->
	<div id="particles-js"><canvas class="particles-js-canvas-el" width="1059" height="599" style="width: 100%; height: 100%;"></canvas></div>

	<link rel="shortcut icon" href="http://works.sietedefebrero.com/nodos/dev/favicon.ico?v=2" type="image/x-icon">
	<link rel="icon" href="http://works.sietedefebrero.com/nodos/dev/favicon.ico?v=2" type="image/x-icon">
	
	<link href="<?=base_url('assets/css/style.css');?>" rel="stylesheet">	
	<!-- Google Fonts -->
	<link href="<?=base_url('assets/css/css');?>" rel="stylesheet" type="text/css"> 

	<!-- scripts -->
	<script src="<?=base_url('assets/js/particles/particles.js');?>"></script>
	<script src="<?=base_url('assets/js/particles/app.js');?>"></script>

	<!-- stats.js -->
	<script src="<?=base_url('assets/js/particles/stats.js');?>"></script>
	<script src="<?=base_url('tpl/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('tpl/js/general.js');?>"></script>
    <link href="<?=base_url('assets/css/bootstrap-notify.css');?>" rel="stylesheet">
    <script src="<?=base_url('assets/js/bootstrap-notify.js');?>"></script>
	<script>
	var count_particles, stats, update;
	stats = new Stats;
	stats.setMode(0);
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.left = '0px';
	stats.domElement.style.top = '0px';
	document.body.appendChild(stats.domElement);
	count_particles = document.querySelector('.js-count-particles');
	update = function() {
		stats.begin();
		stats.end();
		if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
			count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
		}
		requestAnimationFrame(update);
	};
	requestAnimationFrame(update);
	</script>
	<!-- Modal de Error-->
        <div class="modal fade" id="modal_correo" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titulo_error"></h4>
              </div>
              <div class="modal-body">
                  
                    <div class="form-group">
                      <center><label id="error_msj_pass" class="control-label" ></label></center>
                      <h4></h4>
                    </div>
                  
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" id="cerrar_modal_error"  data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
	
	<div class="container">
		<div class="row">
			<div class="col-lg-offset-4 col-lg-4 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-12">
				
				<div class="login">
					<img src="<?=base_url('assets/images/logo-login.png');?>" width="90" alt="NODOS">

					<!--<form>
						<input type="email" id='email' placeholder="email">
            <input type="text" id="username" placeholder="username">
						<input type="password" id="password" placeholder="Contraseña">
            <input type="text" id="nombre" placeholder="nombre">
            <input type="text" id="apellido" placeholder="apellido">
            
						<input id='create' type="button" value="registrar">
            <input id='encriptar' type="button" value="encriptar">
					</form>-->
					<form method="post" action="login/enviar_correo" id="form_olvido_pass">
			            <input id='correo' type="email" name="correo" placeholder="Ingrese su e-mail" required>
			            <input id='acepta_envio' type="button" value="Enviar">
			        </form>
				</div>
				
			</div>
		</div>
	</div>
	
  <div class='notifications top-right'></div>
<script type="text/javascript">
	$(document).ready(function()
    {
    	$('#acepta_envio').click(function()
    	{
    		if($('#correo').val() == '')
        	{	
        		document.getElementById("titulo_error").innerHTML = 'Error';
	            document.getElementById("error_msj_pass").innerHTML = 'Debe Ingresar un E-mail';
	    		$('#modal_correo').modal('show');
	    	}
	    	else
	    	{
	    		var urlRequest = urlApi+"/Login/enviar_correo";
                    $.ajax
                    ({
                            url: urlRequest,
                            dataType:'text',
                            type: 'POST',
                            dataType: "JSON",
                            headers: {              
                                   'APIKEY' : passApiKey,
                                   'Access-Control-Allow-Origin': '*'
                            },
                            data: $('#form_olvido_pass').serialize(),
                            success: function(data)
                            {
                                //console.log('success entro');
                                console.log(data);
                                document.getElementById("titulo_error").innerHTML = 'Exito';
                                document.getElementById("error_msj_pass").innerHTML = data['message'];
                                $('#modal_correo').modal('show');
                                jQuery.fn.reset = function () 
                                {
                                    $(this).each (function() 
                                        { this.reset(); 
                                        });
                                }
                                $("#form_olvido_pass").reset();
                            },
                            beforeSend: function() {
                                    $('.top-right').notify({
                                        message: { text: 'Cargando...' },
                                        fadeOut: { enabled: true, delay: 2500 },
                                        type: 'success2'
                                    }).show(); 
                                    //$('.login').after("<img id='login src='<?=base_url('assets/images/loading.gif');?>' height='30' width='30' />");
                            },
                            error: function(response)
                            {
                                //hacer algo cuando ocurra un error
                              if(response['responseText'])
                              {   
                                console.log(response);
                                response = JSON.parse(response['responseText'],true);
                                document.getElementById("titulo_error").innerHTML = 'Error';
                                document.getElementById("error_msj_pass").innerHTML = response['message'];
                                
                                
                                jQuery.fn.reset = function () 
                                {
                                    $(this).each (function() 
                                        { this.reset(); 
                                        });
                                }
                                $("#form_olvido_pass").reset();
                                $('#modal_correo').modal('show');
                                console.log('error entro');
                                //console.log(response);
                              } 
                            }
                    });
	    		/*document.getElementById("titulo_error").innerHTML = 'Correo Enviado';
	            document.getElementById("error_msj_pass").innerHTML = 'Se a Enviado un e-mail a su correo, siga las Instrucciones.';
	    		$('#modal_correo').modal('show');*/
	    	}
    	})
    })
</script>
</script>
</body></html>