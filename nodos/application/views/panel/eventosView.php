<?php $this->load->view("panel/funciones_calendario"); ?>
<?php include ('funcionesReenviar.html');?>

<script type="text/javascript">

  var passApiKey = '<?php echo $passApiKey; ?>';
  var urlApi = '<?php echo $urlApi; ?>';
  var idUser = '<?php echo $idusuario; ?>';



  function generar_datatable2()
  {
    if ($("#datatable-nodos2").length) {
      $("#datatable-nodos2").DataTable({

      //"scrollX": true,scrollY:        "300px",

        scrollCollapse: true,
        columnDefs: [
              { width: 200, targets: 0 }
                    ],

        dom: "Bfrtip",
        buttons: [
              {
                extend: "copy",
                className: "btn-sm"
              },
              {
                extend: "csv",
                className: "btn-sm"
              },
              {
                extend: "excel",
                className: "btn-sm"
              },
              {
                extend: "pdfHtml5",
                className: "btn-sm"
              },
              {
                extend: "print",
                className: "btn-sm"
              },
            ],


      });//fin datatable
    }
  }


  /*function form_alta_evento()
  {
    //alert(nombregrupo);
      
      $('#contenido').append( retorna_formularioaltaevento('') );

      $('#contenido').parent('div').after( retorna_pieFormEvento() );
      //alert('aa');
      //editor

      CKEDITOR.replace( 'desc', {

     toolbarGroups: [
      { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
      { name: 'colors', groups: [ 'colors' ] },
      { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
      { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
      { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
      { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
      { name: 'styles', groups: [ 'styles' ] },
      { name: 'links', groups: [ 'links' ] },
      { name: 'tools', groups: [ 'tools' ] },
      { name: 'others', groups: [ 'others' ] },
    ],
     removeButtons :'Print,Preview,NewPage,Save,Templates,Source,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Subscript,Superscript,Outdent,Indent,CreateDiv,BidiRtl,BidiLtr,Language,Image,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Maximize,About,Font,ShowBlocks,Cut,Copy,Paste,PasteText,PasteFromWord,Find,FontSize',
     // uiColor: '#DF0101'
    } );

    
    crear_select_colegios();

    datepicker(); //convierto los input en datetimepicker

    //crear_upload();
    $('input[type=file]').on('change', function(event) { prepareUpload(event); });

    $('#guardarEven').click(function(e){  //alert('aguante');

          e.preventDefault();
          e.stopPropagation();


          var $myForm = $('#Nevento');
          if ($myForm[0].checkValidity())
          {
            var titulo = $('#titulo').val();
            var confirmar = $('#confirmYes').is(':checked');
            //var descripcion = $('#desc').val();
            var lugar = $('#lugar').val();
            var fechaf = $('#fechaF').val();
            //var horaf = $('#horaF').val();
            var fechai = $('#fechaI').val();
            //var horai = $('#horaI').val();
            var editor = CKEDITOR.instances['desc'];
            var descripcion = editor.getData();
            console.log(editor.getData());
            //alert(editor.getData());
          

            var parametros = {
              'titulo': titulo,
              'confirm': confirmar,
              'descripcion': descripcion,
              'lugar': lugar,
              'fechaInicio':  fechai,
              'fechafin':  fechaf
            };

            //console.log(parametros);
            guardar_evento(parametros);
            //alert(result);

            return false;
          }
          else {
                  bootbox.alert("Ingrese los campos obligatorios");

                  if( $('#fdirijido').is(':checked') )
                  {
                      var roles = $('#roles_diri').val();
                      if(!roles)
                        $('#s2id_roles_diri').notify("Ingrese al menos un rol", {className:"error", autoHide: false});
                  }
                  else
                    if( $('#fpersonalizado').is(':checked') )
                    {
                      var divi = $('#divi').val();
                      if(!divi)
                        $('#s2id_divi').notify("Ingrese al menos una division", {className:"error", autoHide: false});

                      var roles = $('#rol').val();
                      if(!roles)
                        $('#s2id_rol').notify("Ingrese al menos un rol", {className:"error", autoHide: false});
                    }
                    else
                        if( $('#farea').is(':checked') )
                        {
                          var areas = $('#area').val();
                          if(!areas)
                            $('#s2id_area').notify("Ingrese al menos un area", {className:"error", autoHide: false});

                          var rolesareas = $('#rolarea').val();
                          if(!rolesareas)
                            $('#s2id_rolarea').notify("Ingrese al menos un rol", {className:"error", autoHide: false});
                        }

                return false;
                }
    });

    inicioFormavento=1;
    actions_altaEvento();

  }*/

  function form_alta_evento()
  {

    if(edita_calendario == 1){
      abrir_dialog('', 'Nuevo Comunicado');
    }
  }

  function ver_registros_de_lecturas(id2,titulo)
  {
    $('#loading3'+id2).show();
    $('#boton_registro_lecturas'+id2).hide();

    document.getElementById("titulo_evento").innerHTML = 'Título: '+titulo;
      //console.log(id2);
      var urlRequest = urlApi+"/evento/eventos_enviados_eventosusers/";
      $.ajax({
              url: urlRequest,
                type: 'GET',
                headers: {
                 'APIKEY' : passApiKey,
                 'userid' : idUser,
                 'Access-Control-Allow-Origin': '*'
              },
              data :{id: id2},

              success : function(data)
              {
                  $("#datatable-nodos2").dataTable().fnDestroy();
                  $("#filas_eventos2").empty();
                  $('#loading3'+id2).hide();
                  $('#boton_registro_lecturas'+id2).show();
                  //console.log(data);
                  document.getElementById("titulo_evento").innerHTML = 'Título: '+data.titulo;
                  var datos = jQuery.parseJSON(data.eventousers);
                  var contador = 0;
                  for(var i=0; i < datos.length ; i++) //aca solo listo niveles y especialidades
                  {
                      var id_evento = datos[i]['id'];
                      var confirm = datos[i]['confirm'];
                      var aceptado = datos[i]['aceptado'];
                      var fecha_visto =datos[i]['fechaVisto'];
                      if(fecha_visto == null)
                      {
                          var icono_leido = ' <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>';
                          fecha_visto =icono_leido+" No Leido";
                      }
                      else
                      {
                          var icono_leido = ' <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>';
                          fecha_visto = icono_leido+fecha_visto.substr(8,2)+'/'+fecha_visto.substr(5,2)+'/'+fecha_visto.substr(0,4)+' - '+fecha_visto.substr(11,5)+'hs';

                      }
                      var fecha_confirmacion = datos[i]['fechaConfirmacion'];

                      if(confirm == 1)
                      {
                          if(aceptado == 1)
                          {
                              var icono_confirmacion = '  <span class="glyphicon glyphicon-check " aria-hidden="true"></span>';
                              aceptado = icono_confirmacion+" Aceptado";
                              fecha_confirmacion = fecha_confirmacion.substr(8,2)+'/'+fecha_confirmacion.substr(5,2)+'/'+fecha_confirmacion.substr(0,4)+' - '+fecha_confirmacion.substr(11,5)+'hs';
                          }
                          if(aceptado == -1 )
                          {
                              var icono_confirmacion = ' <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
                              fecha_confirmacion = fecha_confirmacion.substr(8,2)+'/'+fecha_confirmacion.substr(5,2)+'/'+fecha_confirmacion.substr(0,4)+' - '+fecha_confirmacion.substr(11,5)+'hs';
                              aceptado =icono_confirmacion+" Declinado";
                          }
                          if(aceptado == 0)
                          {
                            var icono_confirmacion = ' <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>';
                              aceptado =icono_confirmacion+" No Respondio";
                              fecha_confirmacion =icono_confirmacion+" No Confirmó"
                          }
                      }
                      else
                        if(confirm == 0)
                        {
                            aceptado = "No Corresponde";
                            fecha_confirmacion = "No Corresponde";
                        }
                      var me_gusta = datos[i]['me_gusta'];
                      if(me_gusta == 1)
                      {
                        megusta = 'SI';
                        contador = contador + 1;
                      }
                      else
                      {
                         megusta= '---';
                      }
                      //console.log(id_evento);
                     //var prueba = 'datos[i]['last_name']'+'+datos[i]['titulo']';

                      var fila = '<tr>'+
                                      '<td>'+datos[i]['apellido']+' '+datos[i]['nombre']+'</td>'+'<td>'+datos[i]['grupo_rol']+'</td>'+'<td>'+fecha_visto+'</td>'+'<td>'+fecha_confirmacion+'</td>'+'<td>'+aceptado+'</td>'+'<td><center>'+megusta+'</center></td>'+
                                  '</tr>';
                      $("#filas_eventos2").append(fila);
                      //data[i]['visto']
                  }

                  generar_datatable2();
                  document.getElementById("resultado").innerHTML = contador;
                  $("#evento_registro_de_lectura").modal('show');
              },
              error: function(response)
              {
                  //hacer algo cuando ocurra un error
                  console.log(response);
              }
      });
  }

  function ver_registros_de_lecturas_x_alumno(id2,idcolegio2)
  {
    $("#alumnos_tutores_registro_lectura").empty();
    $('#loading4'+id2).show();
    $('#boton_registro_lecturas_x_alumno'+id2).hide();
    $("#contenido_tab_dir_prec").empty();
    //document.getElementById("titulo_evento").innerHTML = 'Título: '+titulo;
      //console.log(id2);
      var urlRequest = urlApi+"/evento/registro_lectura_obtener_div/";
      $.ajax({
              url: urlRequest,
                type: 'GET',
                headers: {
                 'APIKEY' : passApiKey,
                 'userid' : idUser,
                 'Access-Control-Allow-Origin': '*'
              },
              data :{id: id2, idcolegio: idcolegio2},

              success : function(data)
              {
                  $('#loading4'+id2).hide();
                  $("#filas_eventos2").empty();

                  $('#boton_registro_lecturas_x_alumno'+id2).show();
                  //console.log(data);
                  document.getElementById("titulo_evento_x_alumno").innerHTML = 'Título: '+data.titulo;
                  if(data.status == false)
                  {
                    $("#contenido_tab_dir_prec").append('<h3><center><strong>Comunicado no destinado a Alumnos y Tutores</strong></center></h3>');
                  }
                  else
                  {
                    var escuela_id = data.id_colegio;
                    var id_evento = data.id_evento;
                    var tab_dir_pre = '<div id="tab_dir-'+escuela_id+'" class="tab-pane active">'+
                                                      '<div class="panel-body table-responsive">'+
                                                          '<div class="row">'+
                                                '<div class="col-sm-6">'+
                                                  '<form id="form_dir'+escuela_id+'" autocomplete="on" method="post" class="form-horizontal form-label-left">'+
                                                    ' <div class="form-group ">'+
                                                                '<label>Seleccione una Divisi&oacute;n: </label>'+
                                                          '</div>'+
                                                          '<div class="form-group ">'+
                                                            '<select name="divisiones" id="dir_prec_'+escuela_id+'" class="select2 form-control">'+
                                                            '</select>'+
                                                          '</div>'+
                                                  '</form>'+
                                                '</div>'+
                                                '<div class="col-sm-6" style="margin-top:35px">'+
                                                      '<button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="btn_alumnos" onclick="get_eventos_alumnos_x_division('+escuela_id+','+id_evento+')" type="button"><strong>Listar</strong></button><div type="hidden" id="loading_horarios_dir_esc_'+escuela_id+'">'+
                                                        '</div>'+
                                                      '</div>'+
                                              '</div>'+
                                                      '</div>'+
                                                  '</div>';
                  $("#contenido_tab_dir_prec").append(tab_dir_pre);
                  var datos = jQuery.parseJSON(data.divisiones);
               // var datos = data.divisiones;
                //console.log(datos);
                  for(var i=0; i < datos.length ; i++) //aca solo listo niveles y especialidades
                  {
                      //console.log('gggg');
                      if(datos[i]['nombre_especialidad'])
                      {
                        var nombre_nivel = datos[i]['nombre_nivel']+" "+datos[i]['nombre_especialidad']+" "+datos[i]['nombre_anio']+"</span>";

                        var opcion = "<option value='"+datos[i]['division_id']+"_"+datos[i]['nodoanios_id']+"_"+escuela_id+"'>" +nombre_nivel+"</option>" ;
                      }
                      else
                      {
                        var nombre_nivel = datos[i]['nombre_nivel']+" "+datos[i]['nombre_anio']+" "+datos[i]['nombre_division'];
                        var opcion  = "<option value='"+datos[i]['division_id']+"_"+datos[i]['nodoanios_id']+"_"+escuela_id+"'>" +nombre_nivel+"</option>" ;
                      }
                      //console.log(opcion);
                      $('#dir_prec_'+escuela_id).append(opcion);

                      $('#dir_prec_'+escuela_id).select2({
                        theme: "bootstrap",
                        placeholder: "Seleccione la Division",
                        allowClear: true
                      });

                  }
                }

                  $("#evento_registro_de_lectura_x_alumno").modal('show');

              },
              error: function(response)
              {
                  //hacer algo cuando ocurra un error

                  console.log(response);
              }
      });
  }

  function get_eventos_alumnos_x_division(id_colegio,id_evento)
  {
    $("#alumnos_tutores_registro_lectura").empty();
    $("#alumnos_tutores_registro_lectura").html("<center><img  id='' src='<?=base_url('assets/images/loading.gif');?>' height='40' width='40' /></center>");

    var diviid = $('#dir_prec_'+id_colegio).val();
        var arre = Array();
        arre=diviid.split('_');
        //alert(id_colegio);
        //alert(diviid);
        //console.log(roles);
        $('#tabladatos_'+id_colegio+' tbody').find('.divcasilla').remove();

        //completar_calendario(arre[0], arre[1], arre[2]); //id division, id nodoanio, id_colegio

        $("#tabladatos_"+arre[2]).dataTable().fnDestroy();
        $("#tabladatos_body_"+arre[2]).empty();
        $("#modales_"+id_colegio).empty();
    $(".alumnos_tutores").html("");
    $('#modales_eliminar').empty();
      var url = urlApi+"/evento/registro_lectura_alumnos_x_div";

      $.ajax({
          url: url,
        type: 'GET',
        headers: {
          'APIKEY' : passApiKey,
          'userid' : idUser,
        },
        data:{idcolegio: arre[2], division_id: arre[0], id_evento: id_evento },
        success: function(data)
        {
          //console.log(data);
         // var data = jQuery.parseJSON( data );
         $("#alumnos_tutores_registro_lectura").empty();
          $("#alumnos_tutores_registro_lectura").html(data.registro_lectura_div);



        },
        error: function(response)
        {
          console.log(response);
            var string = "No hay alumnos asignados";
            $(".alumnos_tutores").html(string);
          }
      });
    }


  function detalles_evento(id2, elem)
  {   //alert(id2);
    //$('#loading2').show();

    //$("#evento_detalle").modal('show');

    var urlRequest = urlApi+"/evento/evento/";
    $.ajax({
        url: urlRequest,
          type: 'GET',
          headers: {
           'APIKEY' : passApiKey,
           'userid' : idUser,
           'Access-Control-Allow-Origin': '*'
      },
      beforeSend: function()
      {
          var loading_vista_evento_img = 'loading_vista_evento_img_'+id2;

          $(elem).css('display','none');
          $(elem).after("<img  id='"+loading_vista_evento_img+"' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");
      },
      data :{id: id2},//la última id
      success : function(datos)
      {
          //console.log(datos);
            $('#loading_vista_evento_img_'+id2).remove();
            $(elem).css('display','inline');

            if( (datos['fechaInicio']!=null )&&(datos['fechaInicio']!='0000-00-00 00:00:00') )
            {
              var inicio = datos['fechaInicio'].split(" ");
              var fechaaux=(inicio[0]).split('-');
              inicio = fechaaux[2]+'/'+fechaaux[1]+'/'+fechaaux[0]+' '+inicio[1];
            }
            else
              {
                inicio='';
              }

            if( (datos['fechafin']!=null )&&(datos['fechafin']!='0000-00-00 00:00:00') )
            {
              var fin = datos['fechafin'].split(" ");
              fechaaux=(fin[0]).split('-');
              fin = fechaaux[2]+'/'+fechaaux[1]+'/'+fechaaux[0]+' '+fin[1];
            }
            else
              {
                fin='';
              }

            if( (datos['fechaCreacion']!=null )&&(datos['fechaCreacion']!='0000-00-00 00:00:00') )
            {
              var alta=datos['fechaCreacion'].split(' ');
              fechaaux=(alta[0]).split('-');
              alta = fechaaux[2]+'/'+fechaaux[1]+'/'+fechaaux[0]+' '+alta[1];
            }
            else
              {
                alta='';
              }


           var parametros =
            {
              'idevento': datos['eventid'],
              'titulo': datos['titulo'],
              'confirm': datos['confirm'],
              'descripcion': datos['descripcion'],
              'lugar': datos['lugar'],
              'propietario': datos['owner'],
              'creadorid': datos['userid'],
              'creador': datos['last_name']+' '+datos['first_name'],
              'creadorrol': datos['name'],
              'colegio': datos['nombrecolegio'],
              'fechaAlta':  alta,
              'fechaInicio':  inicio,
              'fechaFin':  fin
            };

            //console.log(parametros);

            abrir_dialogDetalle(parametros);

      },
      error: function(response)
      {
          //hacer algo cuando ocurra un error
          console.log(response);
      }
    });

  }

  function ver_estado_autorizacion(id2, elem)
  {   //alert(id2);
    //$('#loading2').show();

    //$("#evento_detalle").modal('show');

    var urlRequest = urlApi+"/evento/evento/";
    $.ajax({
        url: urlRequest,
          type: 'GET',
          headers: {
           'APIKEY' : passApiKey,
           'userid' : idUser,
           'Access-Control-Allow-Origin': '*'
      },
      beforeSend: function()
      {
          var loading_vista_evento_img = 'loading_vista_evento_img_'+id2;

          $(elem).css('display','none');
          $(elem).after("<img  id='"+loading_vista_evento_img+"' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");
      },
      data :{id: id2},//la última id
      success : function(datos)
      {
          //console.log(datos);
            $('#loading_vista_evento_img_'+id2).remove();
            $(elem).css('display','inline');

            if( (datos['fechaInicio']!=null )&&(datos['fechaInicio']!='0000-00-00 00:00:00') )
            {
              var inicio = datos['fechaInicio'].split(" ");
              var fechaaux=(inicio[0]).split('-');
              inicio = fechaaux[2]+'/'+fechaaux[1]+'/'+fechaaux[0]+' '+inicio[1];
            }
            else
              {
                inicio='';
              }

            if( (datos['fechafin']!=null )&&(datos['fechafin']!='0000-00-00 00:00:00') )
            {
              var fin = datos['fechafin'].split(" ");
              fechaaux=(fin[0]).split('-');
              fin = fechaaux[2]+'/'+fechaaux[1]+'/'+fechaaux[0]+' '+fin[1];
            }
            else
              {
                fin='';
              }

            if( (datos['fechaCreacion']!=null )&&(datos['fechaCreacion']!='0000-00-00 00:00:00') )
            {
              var alta=datos['fechaCreacion'].split(' ');
              fechaaux=(alta[0]).split('-');
              alta = fechaaux[2]+'/'+fechaaux[1]+'/'+fechaaux[0]+' '+alta[1];
            }
            else
              {
                alta='';
              }


           var parametros =
            {
              'idevento': datos['eventid'],
              'titulo': datos['titulo'],
              'confirm': datos['confirm'],
              'descripcion': datos['descripcion'],
              'lugar': datos['lugar'],
              'propietario': datos['owner'],
              'creadorid': datos['userid'],
              'creador': datos['last_name']+' '+datos['first_name'],
              'creadorrol': datos['name'],
              'colegio': datos['nombrecolegio'],
              'fechaAlta':  alta,
              'fechaInicio':  inicio,
              'fechaFin':  fin
            };

            //console.log(parametros);

            //abrir_dialogDetalle(parametros);
            var mensaje = datos['estado_publicacion'];
            bootbox.dialog({
        backdrop: true,
        title: "Estado de Comunicado...",
        message: mensaje,
          buttons: {
              success: {
                  label: "Aceptar",
                  className: "btn-success",
                  callback: function (e) {
                          return ;
                  }//fin calback
              }//fin success
          },//fin Buttons

          onEscape: function() {return ;},
      });  //FIN DIALOG

      },
      error: function(response)
      {
          //hacer algo cuando ocurra un error
          console.log(response);
      }
    });

  }
  


  var table;
  var urlRequest = urlApi+"/evento/ajax_list";

  $(document).ready(function() {
   
    if(edita_calendario){

        $('#altaeventohome').css('display','block');
        //form_alta_evento();
    }
    //else
      //$('#altaeventohome').css('display','none');

    //$("#altaeventohome .ibox-content").css('display', 'none'); //obligo a que aparezcan minimizados


    table = $('#table').DataTable({
      "serverSide": true,
      "scrollX": true,
      "scrollY": "400px",
      "scrollCollapse": true,
      "scroller": {
      "displayBuffer": 2
      },
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "language": {
                              "sProcessing":     "Procesando...",
                              "sLengthMenu":     "Mostrar _MENU_ registros",
                              "sZeroRecords":    "No se encontraron resultados",
                              "sEmptyTable":     "Ningún dato disponible en esta tabla",
                              "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                              "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                              "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                              "sInfoPostFix":    "",
                              "sSearch":         "Buscar:",
                              "sUrl":            "",
                              "sInfoThousands":  ",",
                              "sLoadingRecords": "Cargando...",
                              "oPaginate": {
                                  "sFirst":    "Primero",
                                  "sLast":     "Último",
                                  "sNext":     "Siguiente",
                                  "sPrevious": "Anterior"
                              },
                              "oAria": {
                                  "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                  "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                              }
                            },

      // Load data for the table's content from an Ajax source
      "ajax": {

          "url": urlRequest,
          "type": "POST",
          "headers": {
              "APIKEY" : passApiKey,
              "userid" : idUser
            },
          /*"success": function(data)
          {
              console.log(data);

          },*/
      },

      //Set column definition initialisation properties.
      "columnDefs": [
      {
        "targets": [ -1,-2], //last column
        "orderable": false, //set not orderable
      },
      ],
      "order": [[ 2, 'desc' ]]
    });



  });


  function reload_table()
  {
    table.ajax.reload(null,false); //reload datatable ajax
  }


</script>


<div class="wrapper wrapper-content animated fadeInRight" >

      
      <!--<div class="row" id="altaeventohome" style="display: none;">
          <div class="col-lg-12 ">
              <div class="ibox collapsed float-e-margins">
                  <div class="ibox-title collapse-link2" id="tituloaltaevento" style="cursor: pointer;">
                      <h5>NUEVO COMUNICADO <small> Agregar un nuevo comunicado</small></h5>

                      <div class="ibox-tools">
                          <a class="">
                              <i class="fa fa-chevron-up"></i>
                          </a>
                      </div>
                  </div>
                  <div class="ibox-content dragupload" id='dragupload' style="display: none;">
                      <div class="row ">
                         <div class="col-sm-12 b-r">
                          <h4><label class="pull-left" id="colegio_selecionado_label"></label></h4>
                          <h4><a id="cambiar_colegio"><label class="pull-right" id="ref_cambiar_colegio"></a></label></h4>
                          </div>
                      </div>

                      <div class="row row2 " >

                          <div class="col-sm-12-1 col-sm-12-2 b-r b-l" id="contenido">
                          </div>

                      </div>
                  </div>

              </div>
          </div>
      </div>-->

      <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="ibox collapsed float-e-margins">
                  <div class="ibox-title "> <!--collapse-link2-->
                      <h5>MIS COMUNICADOS <small> Ver Comunicados Enviados</small></h5>
                       <div class="row">  
                              <button class="btn btn-sm btn-primary pull-right m-t-n-xs" id="altaeventohome" onclick="form_alta_evento()" type="button" style="margin-right: 20px"><i class="glyphicon glyphicon-plus "></i>Crear Comunicados<strong></strong></button>
                      </div>
                      <!--<div class="ibox-tools">
                          <a class="">
                              <i class="fa fa-chevron-down"></i>
                          </a>
                      </div>-->
                  </div>
                  <div class="ibox-content" id="ibox_content" style="display: block;">

                     

                      <div class="row">
                          <table id="table" class="table table-striped table-bordered dt-responsive nowrap " width="100%">
                            <thead>
                                <tr>
                                  <th>Remitente</th>
                                  <th>Titulo</th>
                                  <th>Fecha de Creacion</th>
                                  <th>Rol</th>
                                 
                                  <th style="">Acciones</th>
                                </tr>
                              </thead>
                              <tbody>
                              </tbody>



                          </table>
                      </div>
                  </div>

              </div>
          </div>
      </div>

</div>



<!-- Modal de eventos enviados para registro de lectura-->
        <style>
          #mdialTamanio{
            width: 80% !important;
          }
        </style>

        <div class="modal" id="evento_registro_de_lectura" role="dialog">
          <div class="modal-dialog modal-lg" role="document" id="mdialTamanio">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <center><h4 class="modal-title" id="">Registro de Lectura</h4></center>
                <h4 class="modal-title" id=""><label class="" id="titulo_evento"> </label> </h4>
                <h5 class="modal-title" id="">A: <label class="" id="resultado"> </label> Personas le gusta este comunicado</h5>
              </div>
              <div class="modal-body">

                <form action="#" id="form_rechazar" method="post" class="form-horizontal">
                  <input type="hidden" value="" name="eventosid22"/>
                    <div class="form-group table-responsive">
                      <table id="datatable-nodos2" class="table table-striped table-bordered dt-responsive nowrap" style="width: 100%;">
                                  <thead>
                                    <tr>
                                      <th>Destinatario</th>
                                      <th>Rol</th>
                                      <th>Fecha de Lectura</th>
                                      <th>Fecha de Confirmacion</th>
                                      <th>Confirmación</th>
                                      <th>Me Gusta</th>
                                    </tr>
                                  </thead>
                                  <tbody id="filas_eventos2">

                                  </tbody>
                                </table>
                    </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

              </div>
            </div>
          </div>
        </div>

        <div class="modal" id="evento_registro_de_lectura_x_alumno" role="dialog">
          <div class="modal-dialog " role="document" id="">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <center><h4 class="modal-title" id="">Registro de Lectura</h4></center>
                <h4 class="modal-title" id=""><label class="" id="titulo_evento_x_alumno"> </label> </h4>

              </div>
              <div class="modal-body">

                <form action="#" id="form_rechazar2" method="post" class="form-horizontal">
                  <input type="hidden" value="" name="eventosid22"/>
                    <div class="form-group table-responsive">

                      <div  id="contenido_tab_dir_prec" style="margin-left: 5px; margin-right: 10px;"></div>
                      <div  id="alumnos_tutores_registro_lectura" style="margin-left: 5px; margin-right: 10px;"></div>
                    </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

              </div>
            </div>
          </div>
        </div>



<style type="text/css">

  .row2{
    /*max-height: 390px;*/
    min-height: 411px;
  }

  #contenido{
    /*max-height: 360px;
    min-height: 300px;*/
  }

</style>
