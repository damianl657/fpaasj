<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_preceptor extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }

        public function get_preceptoresXciclo($ciclo)
        {
            $sql = "SELECT *
                    FROM preceptores
                    WHERE planesestudio_id = $ciclo";
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }

        public function  insert_preceptor_div($datos)
        {
            $query = $this->db->insert("preceptores",$datos);
            return $query;
        }

	}