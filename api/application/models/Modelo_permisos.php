<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_permisos extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }

        public function get_reglas($idcolegio,$idgrupo)
        {
            $sql = "SELECT *
                    FROM reglas_comunicacion 
                    WHERE colegio_id = $idcolegio 
                    AND group_id = $idgrupo LIMIT 1
                    "; //WHERE name <> 'Nodos'
            //$query = $this->db->query($sql);
            
            //$sql = "CALL get_reglas(".$idcolegio.",".$idgrupo.")";
            $query = $this->db->query($sql);
            $resultado = $query;
           // mysqli_next_result( $this->db->conn_id );
           

            if($resultado->num_rows()>0)
                return $resultado->row();
            else
                return false;
            
        }

        public function get_reglas3($idcolegio)
        {
            
            $sql = "SELECT reglas_comunicacion.*, groups.name as rolname
                    FROM reglas_comunicacion 
                    JOIN groups ON groups.id = reglas_comunicacion.group_id
                    WHERE colegio_id = $idcolegio 
                    "; 
            $query = $this->db->query($sql);
 
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }

        }
        

        public function insert_regla($data)
        {
            $this->db->where($data);
            $query = $this->db->get('reglas_comunicacion');
            if($query->num_rows()>0)
                return 0;
            else
                return $this->db->insert('reglas_comunicacion', $data);   
        }


        public function delete_regla($data)
        {
            $this->db->where($data);
            return $this->db->delete('reglas_comunicacion'); 
        }

        /*public function update_group($id,$data)
        {
            $this->db->where("id",$id);
            return $this->db->update("groups",$data);
        }*/


        public function set_reglas($data)
        {   
            $colegioId = $data->colegioId;
            $grupoId = $data->grupoId;
            $roles = json_decode( $data->roles );
            if(isset($data->noroles))
                $noroles = json_decode( $data->noroles );
            $campo = $data->campo;

            //print_r($grupoId);
            //print_r($roles);

            switch ($campo) {
                case 'reglas':
                    $datos = array(
                        'roles_id'=>join(',',$roles)
                    );
                    break;
                case 'eventos':
                    $datos = array(
                        'roles_eventos'=>join(',',$roles)
                    );
                    break;
                case 'supervisa':

                    for ($i=0; $i < count($roles); $i++) { 

                        $data2 = array(
                            'colegio_id'=>$colegioId,
                            'group_id'=>$roles[$i],
                        );
                        $this->insert_regla($data2); //inserta por si no esta', pero chequea

                        $reglas = $this->get_reglas($colegioId, $roles[$i]);
                        if($reglas){
                            if($reglas->supervisado==null){
                                $arreSuper = $grupoId;
                            }
                            else {
                                $arreSuper = explode(',',$reglas->supervisado);

                                if(!in_array($grupoId, $arreSuper) ) {
                                    $arreSuper[] = $grupoId;
                                    $arreSuper = join(',',$arreSuper);
                                }
                                else continue;
                            }
                        }
    

                        $datos = array(
                            'supervisado'=>$arreSuper
                        );
                        $where = array(
                            'group_id'=>$roles[$i],
                            'colegio_id'=>$colegioId,
                        );
                        $this->db->where($where);
                        $this->db->update("reglas_comunicacion",$datos);

                    }

                    for ($i=0; $i < count($noroles); $i++) {
                        $data2 = array(
                            'colegio_id'=>$colegioId,
                            'group_id'=>$noroles[$i],
                        );
                        $this->insert_regla($data2); //inserta por si no esta', pero chequea

                        $reglas = $this->get_reglas($colegioId, $noroles[$i]);
                        if($reglas){
                            $arreNo = explode(',',$reglas->supervisado);
                            if (($key = array_search($grupoId, $arreNo)) !== false) {
                                unset($arreNo[$key]);
                                if(count($arreNo)>0){
                                    $arreNo = join(',',$arreNo);
                                    $datos = array(
                                        'supervisado'=>$arreNo
                                    );
                                }
                                else {
                                    $datos = array(
                                        'supervisado'=>null
                                    );
                                }

                                $where = array(
                                    'group_id'=>$noroles[$i],
                                    'colegio_id'=>$colegioId,
                                );
                                $this->db->where($where);
                                $this->db->update("reglas_comunicacion",$datos);
                            }
                        }
                    }

                    return true;
                    break;
                case 'menu':
                    $datos = array(
                        'id_menus_acciones'=>join(',',$roles)
                    );
                    break;
                default:
                    # code...
                    break;
            }

            $data2 = array(
                'colegio_id'=>$colegioId,
                'group_id'=>$grupoId,
            );
            $this->insert_regla($data2);
            
            $where = array(
                'group_id'=>$grupoId,
                'colegio_id'=>$colegioId,
            );
            $this->db->where($where);
            $result = $this->db->update("reglas_comunicacion",$datos);
            

            return $result;
        }


        public function get_menuacciones()
        {
            $sql = "SELECT id_menu,nombre,descripcion,tipo,id_padre_menu
                    FROM menu_acciones 
                    WHERE id_menu <> 38 
                    "; 
            $query = $this->db->query($sql);
 
            if($query->num_rows()>0)
                return $query->result();
            else
                return false;
            
        }
        public function get_permiso_div_todas($id_colegio,$rol)
        {
            $sql = "SELECT reglas_comunicacion.* FROM `reglas_comunicacion` join groups on groups.id = reglas_comunicacion.group_id where groups.id_colegio = $id_colegio and groups.name = '$rol' and reglas_comunicacion.div_todas = 1";
            $query = $this->db->query($sql);
            if($query->num_rows()>0)
                return true;
            else
                return false;

        }
        public function get_quien_autoriza_a_rol($id_rol)
        {
            $sql = "SELECT reglas_comunicacion.supervisado FROM reglas_comunicacion where reglas_comunicacion.group_id = $id_rol";
            $query = $this->db->query($sql);
            if($query->num_rows()>0)
            {   
                $regla = $query->row();
                if($regla->supervisado == null)
                {
                    return false;
                }
                $regla = $regla->supervisado;

                $groups = "SELECT * FROM `groups` where id in ($regla)";
                $groups = $this->db->query($groups);
                return $groups;
            }
            else
            {
                return false;
            }
            
        }
       
	}