
<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_tutoralumno extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }


        public function set($data)
        {
            $this->db->insert('tutoralumno', $data);
            $error = $this->db->error();
            if($error['code']=="0")
            {
                return $this->db->error();
            }   
            else return $this->db->error();
        }

        public function get_alu_tutor($id_alumno)
        {
            $sql ="SELECT users.* 
            		FROM users 
            		JOIN tutoralumno on tutoralumno.tutor_id = users.id 
            		WHERE tutoralumno.alumno_id = $id_alumno";
            $query = $this->db->query($sql);
            return $query->result();

        }

           
          

       

	}