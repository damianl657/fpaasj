    
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header" align="center">
				<div class="dropdown profile-element">
                    <span>
                            <a href="<?php echo base_url('index.php/Login/home');?>">
                              <img alt="Nodos Web" src=<?=base_url('assets/images/i/nodos-logo-white.png');?> width="98" alt="Nodos Logo"/>
                            </a>    
                    </span>
                    
                    
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">
                                <?php 
                                    
									echo $this->session->userdata('nombreusuario');
									/*$user = $this->ion_auth->user()->row();
                                    echo $user->first_name ." ". $user->last_name;*/
									
                                ?>
                                </strong>
                            </span> 
                            <span class="text-muted text-xs block"><?//echo $this->session->userdata('nombreusuario') ?> <b class="caret"></b></span> 
                        </span> 
                    </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="<?php //echo base_url('auth/edit_user/'.$this->ion_auth->user()->row()->id); ?>">Mis Datos</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url('index.php/Login/logout'); ?>">Salir</a></li>
                        </ul>
                    
                </div>
                <div class="logo-element" >
					  <img src=<?=base_url('assets/images/i/icon-isologo-nodos.png');?> width="45" alt="Nodos Logo"/>
                </div>
                   
                </li>
                <li<?php if(isset($opcion_menu) && !empty($opcion_menu) && $opcion_menu == "home") { echo ' class="active"'; } ?>>
                    <a href="<?php echo base_url('index.php/Login/home'); ?> "><i class="fa fa-home"></i><span class="nav-label">HOME</span></a>
                </li>
                
                <li<?php if(isset($opcion_menu) && !empty($opcion_menu) && $opcion_menu == "calendario") { echo ' class="active"'; } ?>>
                    <a href="<?php echo base_url('index.php/Calendario'); ?>"><i class="fa fa-calendar"></i><span class="nav-label">Calendario</span></a>
					 
                </li>
                <!--
                <li<?php if(isset($opcion_menu) && !empty($opcion_menu) && $opcion_menu == "usuarios") { echo ' class="active"'; } ?>>
                    <a href="#"><i class="fa fa-users"></i><span class="nav-label">Usuarios</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="<?php echo base_url("usuario/alumno"); ?>"><i class="fa fa-mortar-board"></i> Alumnos</a></li>
                        <li><a href="<?php echo base_url("usuario/padre"); ?>"><i class="fa fa-users"></i> Padres</a></li>
                        <li><a href="<?php echo base_url("usuario/profesor"); ?>"><i class="fa fa-university"></i> Profesores</a></li>
                    </ul>
                </li>
                <li<?php if(isset($opcion_menu) && !empty($opcion_menu) && $opcion_menu == "configuracion") { echo ' class="active"'; } ?>>
                    <a href=""><i class="fa fa-cog"></i><span class="nav-label">Configuracion</span></a>
                </li>
                <li<?php if(isset($opcion_menu) && !empty($opcion_menu) && $opcion_menu == "informes") { echo ' class="active"'; } ?>>
                    <a href=""><i class="fa fa-line-chart"></i><span class="nav-label">Informes</span></a>
                </li>-->
                <li<?php if(isset($opcion_menu) && !empty($opcion_menu) && $opcion_menu == "horarios") { echo ' class="active"'; } ?>>
                    <a href="<?php echo base_url('index.php/Horarios'); ?>"><i class="fa fa-clock-o"></i><span class="nav-label">Horarios</span></a>
                </li>
                <!--<li<php if(isset($opcion_menu) && !empty($opcion_menu) && $opcion_menu == "materias") { echo ' class="active"'; } ?>
                    <a href=""><i class="fa fa-list-ol"></i><span class="nav-label">Materias</span></a>
                </li>
                -->
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
       <div class="row border-bottom">
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href="#"><i class="fa fa-bars"></i></a>
                    <form role="search" class="navbar-form-custom" method="post" action="<?php echo base_url("consulta"); ?>">
                        <div class="form-group" style="margin-top: 14px">
                            <input type="text" placeholder="Buscar..." class="form-control" name="bus_nombre" id="bus_nombre">
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>

                        <a href="<?php echo base_url('index.php/Login/logout'); ?>">
                            <i class="fa fa-sign-out"></i> Salir
                        </a>
                    </li>
                </ul>
            </nav>
        </div> 
    
