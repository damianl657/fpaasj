<?php //session_start();

class Areas extends CI_Controller {


	function __construct(){
		parent::__construct();
			//$this->output->enable_profiler(TRUE);
		if(!$this->session->userdata("logged_in")){
               redirect("login");
         }
	}


    public function index(){
        //$data['action'] = base_url('auth/validar');
        //$this->load->view('admin/niveles',$data);

       /* $this->load->view('include/header');

		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		$data['urlApi'] = $urlApi;
		$data['passApiKey'] = $passApiKey;
		$data['idusuario'] = $this->session->userdata('idusuario');
		//$data['idusuario'] ="SDRiBRjRqUAeolpGhMP7XXMlE1XjVOrHwL/SBNmM7MUBScHCKNgtKYSAw+7Ok0x6acosQcoRyXpDvFViNbwz3w==" ;
		$data['idcolegio'] = $this->session->userdata('idcolegio');
		//$data['idcolegio'] = 1;
		$data['idusergrupo'] = $this->session->userdata('idusergrupo');
		$data['nombrecolegio'] = $this->session->userdata('nombrecolegio');
		//var_dump($this->session->userdata);
		$this->load->view('panel/eventosView',$data);*/
    }


    //---------------------------------------- CURL request to API -------------------------------------------

    public function get_area_x_colegio_curl($urlApi, $passApiKey, $idUser, $idcolegio, $nombregrupo, $idgrupo)
    {
    	//echo $idcolegio; echo $nombregrupo;
    	$url = $urlApi."/area/obtener_areas";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('idcolegio' => $idcolegio,'nombregrupo' =>$nombregrupo,'idgrupo' =>$idgrupo),
			CURLOPT_SSL_VERIFYPEER=> false,
		);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		//print_r($response); die();
		//$response = substr($response, 3);

		//print_r($response);

		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}

	public function get_materias_x_area_curl($urlApi, $passApiKey, $idUser, $areas, $idcolegio, $nombregrupo, $idgrupo)
    {
    	//echo $idcolegio; echo $nombregrupo;
    	$url = $urlApi."/area/obtener_materias";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('areas' => $areas, 'idcolegio' => $idcolegio,'nombregrupo' =>$nombregrupo,'idgrupo' =>$idgrupo),
			CURLOPT_SSL_VERIFYPEER=> false,
		);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		//print_r($response); die();
		//$response = substr($response, 3);

		//print_r($response);
		//$response = json_decode($response);

		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}


	public function get_usuarios_area_curl($urlApi, $passApiKey, $idUser, $data, $idcolegio, $nombregrupo, $idgrupo)
    {
    	//echo $idcolegio; echo $nombregrupo;
    	$url = $urlApi."/usuario/obtener_usuarios_area";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('data' => $data,'idcolegio' => $idcolegio,'nombregrupo' =>$nombregrupo,'idgrupo' =>$idgrupo),
			CURLOPT_SSL_VERIFYPEER=> false,
		);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		print_r($response); die();
		//$response = substr($response, 3);


		//$response = json_decode($response);

		//return json_decode($response);
		return $response;

	}




	//---------------------------------------- AJAX request from WEB -------------------------------------------

	public function get_area_x_colegio_ajax(){

	    $urlApi = $this->variables->get_urlapi();
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');

		$idcolegio = $_POST['idcolegio'];
        $nombregrupo = $_POST['nombregrupo'];
        $idgrupo = $_POST['idgrupo'];



		$result=$this->get_area_x_colegio_curl($urlApi, $passApiKey, $idusuario, $idcolegio, $nombregrupo, $idgrupo);

		print_r($result);

		/*if( (isset($result->status)) && ($result->status=='') ){
			$arre = array(
				'status' => 0,
             	'message' => 'No se encontraron areas'
			);
			$arre=json_encode($arre);
			print_r($arre);
		}
		else
			print_r($result);*/
	}

	public function get_materias_x_area_ajax(){

	    $urlApi = $this->variables->get_urlapi();
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');

		$idcolegio = $_POST['idcolegio'];
        $nombregrupo = $_POST['nombregrupo'];
        $idgrupo = $_POST['idgrupo'];

		$areas = $_POST['areas'];

		//echo $areas; die();
		$result=$this->get_materias_x_area_curl($urlApi, $passApiKey, $idusuario, $areas, $idcolegio, $nombregrupo, $idgrupo);

		//print_r($result); die();

		if( (isset($result->status)) && ($result->status=='') ){
			$arre = array(
				'status' => 0,
             	'message' => 'No se encontraron materias'
			);
			$arre=json_encode($arre);
			print_r($arre);
		}
		else
			print_r($result);
	}

	public function get_usuarios_area_ajax(){

	    $urlApi = $this->variables->get_urlapi();
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');

		$data = $_POST['data'];
		$idcolegio = $_POST['idcolegio'];
        $nombregrupo = $_POST['nombregrupo'];
        $idgrupo = $_POST['idgrupo'];


		$result=$this->get_usuarios_area_curl($urlApi, $passApiKey, $idusuario, $data, $idcolegio, $nombregrupo, $idgrupo);


		if( (isset($result->status)) && ($result->status=='') ){
			$arre = array(
				'status' => 0,
             	'message' => 'No se encontraron usuarios'
			);
			$arre=json_encode($arre);
			print_r($arre);
		}
		else
			print_r($result);
	}




}
?>
