    <?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';

    class Torneo extends REST_Controller {

   

    function __construct()
    {    
        //inserto cabeceras para que no me de error cors
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
            
        parent::__construct();
        $this->load->model("modelo_usuario");
        $this->load->model("modelo_permisos");
        $this->load->model("modelo_horarios");
        $this->load->model("modelo_horario");
        $this->load->model("modelo_inscripciones");
        $this->load->model("Modelo_tutoralumno");
        $this->load->model("Modelo_groups");
        $this->load->model("Modelo_tutor");
        $this->load->model('ion_auth_model');
        $this->load->model("Modelo_torneo");
        
        

    }

    /**
     * Recibe el token gcm (google cloud message) y lo guarda en la bd
     * @param  $registerid_gcm
     * @return boolean true/false
     */

  

    public function guardar_torneo_post()
    {   
      
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {  $torneo = null;                     
           $torneo = array(
            
            'nombre'      =>$this->input->post('txt_torneo') ,
            'organizador' =>$this->input->post('txt_organizador') ,
            'descripcion' =>$this->input->post('txt_campo_descripcion') ,
            'inicio'      =>$this->input->post('txt_fechacomienzo') ,
            'fin'         =>$this->input->post('txt_fechafin')             
             );
          
            $this->db->insert('torneo',$torneo );
            $torneo_id=$this->db->insert_id();
            $arre = $this->input->post('txt_cparticipantes');
            $partic=null;
            $partic=explode(",", $arre);

            foreach ($partic as $key) {
                $inscrip_club = array(
                                'id_club' => $key ,
                                'id_torneo' => $torneo_id );
                $this->db->insert('inscripcion_club', $inscrip_club);
            }
            
           $this->set_response(REST_Controller::HTTP_OK);
           
        } 
    }

     public function listartorneo_post()
    {   
      
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {  
          $torneos=$this->Modelo_torneo->get_obtener_torneos();
         
           $this->set_response($torneos,REST_Controller::HTTP_OK);
           
        } 
    }
    
    




    /*-------------------------fin de torneo----------------------------------------------------------------*/
    public function delete_usergroup_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            if( ($this->input->post('idusergroup')!='NULL') && ($this->input->post('idusergroup') != 0) )
            {
                $were = array(
                    'id' => $this->input->post('idusergroup'),
                );

                $set = array(
                    'estado' => 0
                );
                $result = $this->modelo_usuario->update_usergroup($were, $set);

                $message = [
                    'status' => TRUE,
                    'message' => 'El Rol fue dado de baja',
                ];
                $this->set_response($message, REST_Controller::HTTP_OK);
            }
            else
                {
                    $message = [
                            'status' => false,
                            'message' => 'not id'
                    ];
                    $this->set_response($message, REST_Controller::HTTP_NOT_FOUND);
                }
        }
    }

    public function guardar_alumno_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $doc_aux = str_replace(".", "", $this->input->post('documento_txt'));
            //$ingreso_mail = 1;

            if($this->input->post('email_txt') == "")
            {
                $email = null;
            }
            else 
                $email = $this->modelo_usuario->obtener_email($this->input->post('email_txt'));
            $data = null;
            $id_groups = null;

            $documento = $this->modelo_usuario->obtener_documento($doc_aux);
            if( ($documento == null) && ($email == null) )
            {
                //if($email == null){
                    //INSERTA USER
                    $pass = $this->ion_auth_model->hash_password($doc_aux);
                    $data = array(
                        'first_name' => $this->input->post('nombre_txt'),
                        'last_name' => $this->input->post('apellido_txt'),
                        'email' => $this->input->post('email_txt'),
                        'sexo' => $this->input->post('sexo_txt'),
                        'documento' => $this->input->post('documento_txt'),
                        'password' => $pass,
                        'active' => '1'
                    );
                    $result = $this->modelo_usuario->set($data);
                    $alumnoId = $this->db->insert_id();
        
                    
                    //INSERTA USER_GROUP
                    $id_groups =$this->modelo_usuario->obtener_id_rol_alumno_colegio($this->input->post('id_colegio'));
                    $data_group = array(
                        'user_id' => $alumnoId,
                        'group_id' => $id_groups,
                        'colegio_id' => $this->input->post('id_colegio'),
                        'estado' => 1,
                    );
                    $result = $this->modelo_usuario->set_user_group($data_group);
                    $id_user_groups = $this->db->insert_id();
                    
                    
                // }
                /*else
                {
                    $message = [
                            'status' => TRUE,
                            'existe' => false,
                            'message' => 'El Correo ya existe',
                            'post' =>  $this->input->post,
                            'email' =>$email,
                        ];
                        $this->set_response($message, REST_Controller::HTTP_OK);
                }*/
            }
            else
            {
                /*$message = [
                    'status' => TRUE,
                    'existe' => false,
                    'message' => 'El Documento ya existe',
                    'post' =>  $this->input->post,
                ];
                $this->set_response($message, REST_Controller::HTTP_OK);*/

                $alumnoId = $documento->id;
                $colegioId = $this->input->post('id_colegio');

                $usergroup = $this->modelo_usuario->obtener_users_group_alumno2($alumnoId,$colegioId);
                if($usergroup){
                    $id_user_groups = $usergroup->id;
                    if( $usergroup->estado == 0){
                        $set = array(                           
                            'estado' => 1,
                        );
                        $where = array(                           
                            'id' => $id_user_groups,
                        );
                        $this->modelo_usuario->update_usergroup($where,$set);
                    }
                }
                else{
                    //INSERTA USER_GROUP
                    $id_groups =$this->modelo_usuario->obtener_id_rol_alumno_colegio($colegioId);
                    $data_group = array(
                        'user_id' => $alumnoId,
                        'group_id' => $id_groups,
                        'colegio_id' => $colegioId,
                        'estado' => 1,
                    );
                    $this->modelo_usuario->set_user_group($data_group);
                    $id_user_groups = $this->db->insert_id();
                }
            }

            //INSERTA INSCRIPCION, chequea antes
            $cicloA = $this->utilidades->obtener_cicloaXcolegio( $this->input->post('id_colegio') );

            $insc_Data = array(
                'alumno_id' => $id_user_groups,
                //'activo' => 1,
                //'divisiones_id' => $this->input->post('id_division'),
                'cicloa'=> $cicloA,
            );
            $insc_Data2 = array(
                'alumno_id' => $id_user_groups,
                'activo' => 1,
                'divisiones_id' => $this->input->post('id_division'),
                'cicloa'=> $cicloA,
            );
            //print_r($insc_Data);
            $inscripcion = $this->modelo_inscripciones->get($insc_Data);
            //print_r($inscripcion); die();
            if(!$inscripcion){
                $this->modelo_inscripciones->set($insc_Data2);
                $mensaje = "El Usuario fue Registrado con Exito";
                $existe = false;
            }else {
                 $insc_Update = array(
                    'activo' => 1,
                    //'divisiones_id' => $this->input->post('id_division'),
                );
                $this->modelo_inscripciones->update($inscripcion->id,$insc_Update);
                $mensaje = "El Alumno ya estaba inscripto";
                $existe = true;
            }

            //$tutores = $this->Modelo_tutoralumno->get_alu_tutor($id_nuevo_usuario);
            $alumno = null;
            $alumno = $this->modelo_usuario->get_users_id($alumnoId);
            $message = [
                'status' => TRUE,
                'existe' => $existe,
                'message' => $mensaje,
                'post' =>  $this->input->post,
                'data' => $data,
                'group_id' => $id_groups,
                'id_division' => $this->input->post('id_division'),
                'id_colegio' => $this->input->post('id_colegio'),
                'alumno' => $alumno
            ];
            $this->set_response($message, REST_Controller::HTTP_OK);

        }
    }
    public function guardar_usuario2_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $doc_aux = str_replace(".", "", $this->input->post('documento_txt'));
            $email = $this->modelo_usuario->obtener_email($this->input->post('email_txt'));
            $documento = $this->modelo_usuario->obtener_documento($doc_aux);
            if($documento == null)
            {
                if($email == null)
                {
                    
                    
                    $pass = $this->ion_auth_model->hash_password($documento);
                    $data = array(
                        'first_name' => $this->input->post('nombre_txt'),
                        'last_name' => $this->input->post('apellido_txt'),
                        'email' => $this->input->post('email_txt'),
                        'sexo' => $this->input->post('sexo_txt'),
                        'documento' => $this->input->post('documento_txt'),
                        'password' => $pass,
                        'active' => '1'
                    );

                    //$result = $this->modelo_usuario->set($data);
                    //$id_nuevo_usuario = $this->db->insert_id();
                    /*$data_group = array(
                        'user_id' => $id_nuevo_usuario,
                        'group_id' => $this->input->post('select_rol'),
                        'colegio_id' => $this->input->post('id_colegio_sel'),

                    );*/
                    //$result = $this->modelo_usuario->set_user_group($data_group);
                    $message = [
                            'status' => TRUE,
                            'existe' => false,
                            'message' => 'El Usuario fue Registrado con Exito',
                            //'post' =>  $this->input->post,
                            'data' => $data,
                            'group_id' => $this->input->post('select_rol'),
                            'colegio_id' => $this->input->post('id_colegio_sel')
                        ];
                        $this->set_response($message, REST_Controller::HTTP_OK);
                }else
                {
                    $message = [
                            'status' => TRUE,
                            'existe' => false,
                            'message' => 'El E-mail ya esta registrado'
                            
                        ];
                        $this->set_response($message, REST_Controller::HTTP_OK);
                }
            }
            else
            {
                $data_group = array(
                        'user_id' => $documento->id,
                        'group_id' => $this->input->post('select_rol'),
                        'colegio_id' => $this->input->post('id_colegio_sel'),

                    );
                    $result = $this->modelo_usuario->set_user_group($data_group);
                $message = [
                            'status' => true,
                            'existe' => true,
                            'message' => 'El Usuario fue Registrado con Exito'
                            //'message' => $documento->id
                        ];
                        $this->set_response($message, REST_Controller::HTTP_OK);
            }
                

            
        }

    }
    public function no_pago_cuota_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {   
            $fecha = date("Y-m-d H:i:s");
            $data = array(
                            'pago' => 0,
                            'fecha_pago'=> $fecha
                        );
             $id_usuario = $_GET['id_user']; 

            $this->modelo_usuario->borrar_pago($id_usuario,$data);
            $message = [
                            'satus' => TRUE,
                            'message' => 'Se Borro el pago con Exito',
                            'users' => $id_usuario
                        ];
                        $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }
    public function pago_cuota_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {   
            $fecha = date("Y-m-d H:i:s");
            $data = array(
                            'pago' => 1,
                            'fecha_pago'=> $fecha
                        );
             $id_usuario = $_GET['id_user']; 

            $this->modelo_usuario->registrar_pago($id_usuario,$data);
            $message = [
                            'satus' => TRUE,
                            'message' => 'Se pago con Exito',
                            'users' => $id_usuario
                        ];
                        $this->set_response($message, REST_Controller::HTTP_OK);
        }

    }
    public function activar_users_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {   
            
            $data = array(
                            'active' => 1
                        );
             $id_usuario = $_GET['id_user']; 
             $id_rol = $_GET['id_rol']; 
            $this->modelo_usuario->activar_users($id_usuario,$data);
            $message = [
                            'satus' => TRUE,
                            'message' => 'Se Activo con Exito',
                            'users' => $id_usuario,
                            'id_rol' => $id_rol
                        ];
                        $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

    public function desactivar_users_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {   
            
            $data = array(
                            'active' => 0
                        );
             $id_usuario = $_GET['id_user']; 
             $id_rol = $_GET['id_rol']; 
            $this->modelo_usuario->desactivar_users($id_usuario,$data);
            $message = [
                            'satus' => TRUE,
                            'message' => 'Se Activo con Exito',
                            'users' => $id_usuario
                        ];
                        $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }
    

    public function obtener_roles_colegio_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {  
            //$idcolegio = $_GET['idcolegio']; 
            $idcolegio = $this->get('idcolegio'); 

            $roles = $this->modelo_usuario->obtener_roles_delcolegio($idcolegio); // obtiene los roles del colegio seleccionado
            // Si existe mas de un resultado, lo mando
            if ($roles)
            {
                 $this->response([
                        'status' => TRUE,
                        'data'=>$roles,
                        ], REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron Roles',
                    //'query' => $this->db->last_query()
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }

    public function obtener_roles_colegio_menorOrden_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {  
            if($_GET['idcolegio'])
            {    
                $idcolegio = $_GET['idcolegio']; 

                $mayor = $this->modelo_usuario->obtener_rol_mayorPrioridad($idcolegio,$userid); //obtiene el rol de mayor prioridad, de ese usuario en ese colegio

                foreach ($mayor as $key ) {
                   $orden = $key->orden;
                }

                $roles = $this->modelo_usuario->obtener_roles_delcolegio_menorOrden($idcolegio,$orden); //obtiene los roles de menor prioridad del colegio seleccionado
               
                if ($roles)
                {
                    //print_r($eventos);
                    //$eventos["query"] = $this->db->last_query();
                    $this->response([
                        'status' => TRUE,
                        'data'=>$roles,
                        ], REST_Controller::HTTP_OK);
                }
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'No se encontraron Roles',
                        //'query' => $this->db->last_query()
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                }
            }
             else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'no colegio',
                        //'query' => $this->db->last_query()
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                }
        }
    }

    public function obtener_users_colegio_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {   $id_rol = $_GET['id_rol']; 
            $idcolegio = $_GET['idcolegio']; 
            $usuarios = $this->modelo_usuario->obtener_users_colegio($idcolegio,$id_rol); // obtiene los eventos de el usuario X
            // Si existe mas de un resultado, lo mando
            if ($usuarios)
            {
                //print_r($eventos);
                //$eventos["query"] = $this->db->last_query();
                $this->response($usuarios, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron Usuarios',
                    'query' => $this->db->last_query()
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }
    public function gcmToken_post()
    {
        //verifico que el usuario
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {   //si el usuario es correcto     
            $registerid_gcm = $this->input->post("registerid_gcm");
            // Si no vienen seteado
            if ((empty($registerid_gcm)))
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Debe proveer el id de registro del Google Cloud Message'
                ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code            
            }
            else
            {// Si vienen los datos
               
                //me fijo si ya existe el token y el usuario
                if($this->modelo_usuario->existe_registerid_gcm($userid, $registerid_gcm)){
                    // si ya existe no hago nada y devuelvo OK
                    $message = [
                        'satus' => TRUE,
                        'message' => 'El token gcm ya existe'
                    ];
                    $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
                }
                else
                {   //Si no existe token y usuario en la tabla push procedo a guardar 
                    if ($this->modelo_usuario->guardar_registerid_gcm($userid, $registerid_gcm))// gcm = google cloud message
                    {                       
                        // Se guardo correctamente
                        $message = [
                            'satus' => TRUE,
                            'message' => 'Id del gcm guardado'
                        ];
                        $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code                      
                    }
                    else
                    {   //Error al guardar
                        $this->response([
                            'status' => FALSE,
                            'message' => 'El id del gcm no pudo ser guardado'
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                    }
                }
            }
        }
    }
    public function obtener_usuario_get() //identico a "post", solo que este no recibe parametro. la otra si, porque necesita traer datos de otro usuario
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            //$usuario = $this->modelo_usuario->get_users_id($this->session->userdata('user_id'));
            $usuario = $this->modelo_usuario->get_users_id( $userid );
            $this->response($usuario, REST_Controller::HTTP_OK);
        }

    }

    public function eliminar_tutor_alumno_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $this->modelo_usuario->delete_alu_tutor($this->input->get('id_alumno'),$this->input->get('id_tutor'));
            $tutores = $this->modelo_usuario->get_tutores_alu($this->input->get('id_alumno'));
            $cont_tutores = false;
            if($tutores)
            {
                $cont_tutores = true;
            }
            $this->response([
                    'status' => TRUE,
                    'cont_tutores' => $cont_tutores,
                ], REST_Controller::HTTP_OK); 
        }
    }

    public function actualizarDatosCel_post(){
        //verifico que el usuario
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {   //si el usuario es correcto     
            $registerid_gcm = $this->input->post("registerid_gcm");
            // Si no vienen seteado
            if ((empty($registerid_gcm)))
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Debe proveer el id de registro del Google Cloud Message'
                ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code            
            }
            else
            {// Si vienen los datos
                $model = empty($this->input->post("model"))?null:$this->input->post("model");
                $imei = empty($this->input->post("imei"))?null:$this->input->post("imei");
                $sdk_v_r = empty($this->input->post("sdk_version_release"))?null:$this->input->post("sdk_version_release");
                $sdk_version = empty($this->input->post("sdk_version"))?null:$this->input->post("sdk_version");
                $version_name = empty($this->input->post("version_name"))?null:$this->input->post("version_name");
                $version_number = empty($this->input->post("version_number"))?0:$this->input->post("version_number");
                $platform = empty($this->input->post("platform"))?null:$this->input->post("platform");

                $source = empty($this->input->post("source"))?null:$this->input->post("source");
                $instalacion_id = empty($this->input->post("instalacion_id"))?null:$this->input->post("instalacion_id");

                $array = array(
                    "model" => $model,
                    "imei" => $imei,
                    "sdk_version_release" => $sdk_v_r,
                    "sdk_version" => $sdk_version,
                    "version_name" => $version_name,
                    "version_number" => $version_number,
                    "plataforma" => $platform,
                    "source" => $source,
                    "instalacion_id" => $instalacion_id
                );

                if($this->modelo_usuario->guardar_datos_cel($array,$userid,$registerid_gcm)){
                    $message = [
                        'satus' => TRUE,
                        'message' => 'Los datos del cel se guardaron correctamente'
                    ];
                    $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
                }
                else{
                    $message = [
                        'satus' => TRUE,
                        'message' => 'Error al actualizar datos del cel'
                    ];
                    $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code                        
                }
            }
        }
    }

    public function pasos_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            
            $idusergrupo = $_GET['idusergrupo']; 

            $result = $this->modelo_usuario->obtener_pasos($idusergrupo);
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
                $fila = $result->row();  
                //$this->response($fila->pasos, REST_Controller::HTTP_OK);

                $this->response([
                            'status' => TRUE,
                            'paso' => $fila->configuracion, 
                        ], REST_Controller::HTTP_OK);  
                
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Error al consultar paso'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }
    public function verificar_codigo_recuperacion_post()
    {
        
        $code = $this->input->post('code');
        $user = $this->ion_auth->forgotten_password_check($code);
        $user = json_encode($user);
        $this->response($user, REST_Controller::HTTP_OK);
    }
    public function obtener_roles_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $result = $this->modelo_usuario->get_rolesXidusuario($userid);
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
                $result = json_encode($result);
                $this->response($result, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron roles'
                ], REST_Controller::HTTP_OK); 
            }
        }
    }  



    public function traer_usuariosXrolesIds($idcolegio, $nombregrupo, $ids_roles, $userid)  //hay una copia exacta en controlador evento. no se como llamar a una funcion desde otro controller.
    {    
        $anioA = $this->utilidades->obtener_planestudioaXcolegio( $idcolegio ); 
        $planest_Id = $this->utilidades->get_planestudio( $anioA ); 


        $roles = $this->modelo_usuario->get_rolesXrolesid($ids_roles, $idcolegio);         

            
        $demasRoles = array(); $idsalu = 0; $arreDoce = 0; $arrePrese = 0; $idstutor = 0;
        $divisiones = null;
        
        if($nombregrupo=='Docente')
        {  
            $this->load->model("modelo_division");
            $this->load->model("modelo_alumno");
            
            $diviDoce = $this->modelo_division->divisionesXdocentes($idcolegio,$planest_Id, $userid);

            if( count($diviDoce)>0)
            {
                $arre=array();
                foreach ($diviDoce as $key) {
                    $arre[] = $key->divisiones_id;
                }
                $divisiones = join(',',$arre); //las divisiones del preceptor
            } 

            //a partir  de aqui el codigo es exacamente igual que para preceptor
            
            foreach ($roles as $rol)
            {
                switch ($rol->name)
                {
                    case 'Alumno':
                        if(count($divisiones))
                        {
                            $cicloA = $this->utilidades->obtener_cicloaXcolegio( $idcolegio );
                            $alumnos = $this->modelo_alumno->get_alumnosXdivisiones($divisiones,$cicloA);
                            if( count($alumnos)>0 )
                            {
                                $arreAlum=array();    
                                foreach ($alumnos as $key) {
                                    $arreAlum[] = $key->user_id;
                                }
                                $idsalu = join(',',$arreAlum); //los alumnos del preceptor                                   
                                //print_r('IDSALUMNOS: '.$idsalu.'$$');
                            }
                            else $idsalu = 0;
                        }
                        else $idsalu = 0;
                        
                        break;
                    case 'Tutor':
                            if(count($divisiones))
                            {
                                $cicloA = $this->utilidades->obtener_cicloaXcolegio( $idcolegio );
                                $alumnos = $this->modelo_alumno->get_tutoresXdivisiones($divisiones,$cicloA);
                                if( count($alumnos)>0 )
                                {
                                    $arreAlum=array();    
                                    foreach ($alumnos as $key) {
                                        $arreAlum[] = $key->user_id;
                                    }
                                    $idstutor = join(',',$arreAlum); //los alumnos del preceptor                                   
                                    //print_r('IDSALUMNOS: '.$idsalu.'$$');
                                }
                                else $idstutor = 0;
                            }
                            else $idstutor = 0;
                            
                            break;
                    case 'Docente':
                        if(count($divisiones))
                        {
                            $docentes = $this->modelo_alumno->get_docentesXdivisiones($divisiones);
                        }
                        else {  //si no tiene divisiones asignadas, traigo todos los docentes del colegio
                                $docentes = $this->modelo_usuario->get_usuariosXrol($idcolegio,"Docente");
                            }


                        //if( count($docentes)>0 )
                        if( $docentes )    
                        {
                            $arreDoce=array();
                            foreach ($docentes as $key) {
                                $arreDoce[] = $key->user_id;
                            }
                            $arreDoce = join(',',$arreDoce);  
                        }
                        else $arreDoce = 0;

                        break;

                    case 'Preceptor':
                        if(count($divisiones))
                        {  
                            $preceptores = $this->modelo_alumno->get_preceptoresXdivisiones($divisiones);
                            
                            if( ! $preceptores )
                            {  //si no tiene divisiones asignadas, traigo todos los preceptores del colegio
                                $preceptores = $this->modelo_usuario->get_usuariosXrol($idcolegio,"Preceptor");
                            }   
                        }
                        else {  //si no hay divisiones cargadas, traigo todos los preceptores del colegio
                                $preceptores = $this->modelo_usuario->get_usuariosXrol($idcolegio,"Preceptor");
                            }

                        //print_r($preceptores);
                        if( $preceptores)
                        //if( count($preceptores)>0 )
                        {
                            $arrePrese=array();
                            foreach ($preceptores as $key) {
                                $arrePrese[] = $key->user_id;
                            }
                            $arrePrese = join(',',$arrePrese);
                        }
                        else $arrePrese = 0;

                        break;
                    
                    default:
                        $demasRoles[]=$rol->id;
                        break;
                }
            }

            $idsaux=$idsalu.",".$arrePrese.",".$arreDoce.",".$idstutor;

            $resultAlgo = $this->modelo_usuario->get_usuariosEspecificos2($idcolegio, $idsaux, $ids_roles);
            //print_r("A VER: ". $resultAlgo); 
            if( $resultAlgo )
            {
                if( count($demasRoles)>0 )
                {
                    $demasRolesId = join(',',$demasRoles); 
                    $resultOtro = $this->modelo_usuario->get_usuariosColegio2($idcolegio, $demasRolesId);
                    if( $resultOtro )
                        $Todos = array_merge($resultAlgo, $resultOtro);
                    else
                        $Todos = $resultAlgo;

                    $result=$Todos;
                }
                else $result=$resultAlgo;
            }
            else
                if( count($demasRoles)>0 )
                {
                    $demasRolesId = join(',',$demasRoles); 
                    $resultOtro = $this->modelo_usuario->get_usuariosColegio2($idcolegio, $demasRolesId);                   
                    $result=$resultOtro;
                }

        }
        else 
            if($nombregrupo=='Preceptor')
            {//if preceptor
                $this->load->model("modelo_division");
                $this->load->model("modelo_alumno");
                $diviPrece = $this->modelo_division->divisionesXpreceptores($idcolegio,$planest_Id,$userid);
                
                if( count($diviPrece)>0)
                {
                    $arre=array();
                    foreach ($diviPrece as $key) {
                        $arre[] = $key->division_id;
                    }
                    $divisiones = join(',',$arre); //las divisiones del preceptor
                }                     

                
                foreach ($roles as $rol)
                {
                    //print_r($rol);
                    switch ($rol->name)
                    {
                        case 'Alumno':
                            if(count($divisiones))
                            {
                                $cicloA = $this->utilidades->obtener_cicloaXcolegio( $idcolegio );
                                $alumnos = $this->modelo_alumno->get_alumnosXdivisiones($divisiones,$cicloA);
                                if( count($alumnos)>0 )
                                {
                                    $arreAlum=array();    
                                    foreach ($alumnos as $key) {
                                        $arreAlum[] = $key->user_id;
                                    }
                                    $idsalu = join(',',$arreAlum); //los alumnos del preceptor                                   
                                    //print_r('IDSALUMNOS: '.$idsalu.'$$');
                                }
                                else $idsalu = 0;
                            }
                            else $idsalu = 0;
                            
                            break;
                        case 'Tutor':
                            if(count($divisiones))
                            {
                                $cicloA = $this->utilidades->obtener_cicloaXcolegio( $idcolegio );
                                $alumnos = $this->modelo_alumno->get_tutoresXdivisiones($divisiones,$cicloA);
                                if( count($alumnos)>0 )
                                {
                                    $arreAlum=array();    
                                    foreach ($alumnos as $key) {
                                        $arreAlum[] = $key->user_id;
                                    }
                                    $idstutor = join(',',$arreAlum); //los alumnos del preceptor                                   
                                    //print_r('IDSALUMNOS: '.$idsalu.'$$');
                                }
                                else $idstutor = 0;
                            }
                            else $idstutor = 0;
                            
                            break;
                        case 'Docente':
                            /*if(count($divisiones))
                            {
                                $docentes = $this->modelo_alumno->get_docentesXdivisiones($divisiones);
                                if( count($docentes)>0 )
                                {
                                    $arreDoce=array();
                                    foreach ($docentes as $key) {
                                        $arreDoce[] = $key->user_id;
                                    }
                                    $arreDoce = join(',',$arreDoce);  
                                }
                                else $arreDoce = 0;
                            }
                            else $arreDoce = 0;*/

                            if(count($divisiones))
                            {
                                $docentes = $this->modelo_alumno->get_docentesXdivisiones($divisiones);

                                if(! $docentes)
                                {  //si no tiene divisiones asignadas, traigo todos los docentes del colegio
                                    $docentes = $this->modelo_usuario->get_usuariosXrol($idcolegio,"Docente");
                                }
                            }
                            else {  //si no hay divisiones cargadas, traigo todos los docentes del colegio
                                    $docentes = $this->modelo_usuario->get_usuariosXrol($idcolegio,"Docente");
                                }

                            //if( count($docentes)>0 )
                            if( $docentes )    
                            {
                                $arreDoce=array();
                                foreach ($docentes as $key) {
                                    $arreDoce[] = $key->user_id;
                                }
                                $arreDoce = join(',',$arreDoce);  
                            }
                            else $arreDoce = 0;

                            break;

                        case 'Preceptor':
                            /*if(count($divisiones))
                            {
                                $preceptores = $this->modelo_alumno->get_preceptoresXdivisiones($divisiones);
                                
                                $arrePrese=array();
                                if( $preceptores)
                                {
                                    foreach ($preceptores as $key) {
                                        $arrePrese[] = $key->user_id;
                                    }
                                    $arrePrese = join(',',$arrePrese);
                                }
                                else $arrePrese = 0;
                            }
                            else $arrePrese = 0;*/

                            if(count($divisiones))
                            {
                                $preceptores = $this->modelo_alumno->get_preceptoresXdivisiones($divisiones);
                            }
                            else {  //si no tiene divisiones asignadas, traigo todos los preceptores del colegio
                                    $preceptores = $this->modelo_usuario->get_usuariosXrol($idcolegio,"Preceptor");
                                }

                            if( $preceptores)
                            //if( count($preceptores)>0 )
                            {
                                $arrePrese=array();
                                foreach ($preceptores as $key) {
                                    $arrePrese[] = $key->user_id;
                                }
                                $arrePrese = join(',',$arrePrese);
                            }
                            else $arrePrese = 0;

                            break;
                        
                        default:
                            $demasRoles[]=$rol->id;
                            break;
                    }
                }


                $idsaux=$idsalu.",".$arrePrese.",".$arreDoce.",".$idstutor;
                //print_r($idsaux);
                $resultAlgo = $this->modelo_usuario->get_usuariosEspecificos2($idcolegio, $idsaux, $ids_roles);
                 
                if( $resultAlgo )
                {
                    if( count($demasRoles)>0 )
                    {
                        $demasRolesId = join(',',$demasRoles); 
                        $resultOtro = $this->modelo_usuario->get_usuariosColegio2($idcolegio, $demasRolesId);
                        if( $resultOtro )
                            $Todos = array_merge($resultAlgo, $resultOtro);
                        else
                            $Todos = $resultAlgo;

                        $result=$Todos;
                    }
                    else $result=$resultAlgo;
                }
                else
                    if( count($demasRoles)>0 )
                    {
                        $demasRolesId = join(',',$demasRoles); 
                        $resultOtro = $this->modelo_usuario->get_usuariosColegio2($idcolegio, $demasRolesId);                   
                        $result=$resultOtro;
                    }

                
            }//fin if preceptor
            else 
                $result = $this->modelo_usuario->get_usuariosColegio2($idcolegio, $ids_roles);


        if (isset($result))
            return $result;
        else return false;
    }   
    


    public function obtener_usuarios_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {//else 1
            $idcolegio = $_GET['idcolegio']; 
            $idgrupo = $_GET['idgrupo'];
            $nombregrupo = $_GET['nombregrupo'];
            $ids_roles = json_decode($_GET['ids_roles']);
            
            
            /*print_r('idcolegio: '.$idcolegio);
            print_r('idgrupo: '.$idgrupo);
            print_r('nombregrupo: '.$nombregrupo);
            print_r($ids_roles);*/

            if(count($ids_roles)>0)
            {
                $ids_roles = join(',',$ids_roles);
                //print_r($ids_roles);
            }
            else 
                {
                    $reglas = $this->modelo_permisos->get_reglas($idcolegio,$idgrupo);
                    if($reglas)
                        $ids_roles = $reglas->roles_id;
                    else $ids_roles = 0;
                    //print_r('else:'.$ids_roles);
                }

            $result = $this->traer_usuariosXrolesIds($idcolegio, $nombregrupo, $ids_roles, $userid);

            //print_r($result);
            if ($result)
            {
                $result = json_encode($result);
                $this->response($result, REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No_usuarios'
                ], REST_Controller::HTTP_OK); 
            }
        }
    }


    public function obtener_usuarios_area_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {   
            $this->load->model("modelo_areas");

            $idcolegio = $this->post('idcolegio'); 
            $idgrupo = $this->post('idgrupo'); 
            $nombregrupo = $this->post('nombregrupo');
            $data = json_decode($this->post('data'));

            
            $areas = $data->areas;
            $materias = $data->materias;
            $roles = $data->roles;
            //$radioalu = $data->radioalu;


            $anioA = $this->utilidades->obtener_planestudioaXcolegio( $idcolegio );         
            $planest_Id = $this->utilidades->get_planestudio( $anioA ); 
            

            if(count($areas) > 0){
                $ids_areas = join(',',$areas);

                if(count($materias) > 0)
                {
                    $ids_materias = join(',',$materias);
                }
                else {

                    /*if($nombregrupo == 'Docente')
                        $materias = $this->modelo_areas->obtener_materias_By_docente($ids_areas, $idcolegio, $idgrupo, $userid);
                    else
                        $materias = $this->modelo_areas->obtener_materias_By_areas($ids_areas);

                    //agregar cond docente.
                    $auxmat = array();
                    foreach ($materias as $mat) {
                        $auxmat[]=$mat->aniomateria_id;
                    }

                    if(count($auxmat) > 0)
                        $ids_materias = join(',',$auxmat);
                    else $ids_materias = 0;*/

                    //HACER QUE SIEMPRE TRAIGA MSTERIAS, si no selecciona que traiga todas las opciones
                     $ids_materias = 0;
                }

            }else $ids_areas = 0;


            if(count($roles) > 0)
            {
                $idroles = join(',',$roles);

                $roles = $this->modelo_usuario->get_rolesXrolesid($idroles, $idcolegio); 
                
                $users_groups = array();
                foreach ($roles as $rol)
                {
                    switch ($rol->name)
                    {
                        case 'Alumno':
                                //$arreAlumnos = array();
                                //$arreTutores = array();
                                $cicloa = $this->utilidades->obtener_cicloaXcolegio( $idcolegio );

                                $result = $this->modelo_areas->obtener_alumnos_By_areas($ids_areas, $ids_materias, $planest_Id, $cicloa);
                                if($result)
                                    foreach ($result as $row)
                                        $users_groups[]= $row->alumno_id; //alumnos


                                /*$idsalumnos = join(',',$arreAlumnos); 
                                $this->load->model("modelo_alumno");                                
                                $tutores = $this->modelo_alumno->get_tutoresXalumnos($idsalumnos);
                                foreach ($tutores as $key) {
                                    $arreTutores[] = $key->tutor_id;
                                } 

                                switch ($radioalu) {
                                    case 'ambos':
                                        $users_groups[] = $arreAlumnos[];
                                        $users_groups[] = $arreTutores[];
                                        break;
                                    case 'soloalumno':
                                        $users_groups[] = $arreAlumnos[];
                                        break;
                                    case 'solotutor':
                                        $users_groups[] = $arreTutores[];

                                        break;
                                    default:
                                       $users_groups[] = $arreAlumnos[];
                                        break;
                                }*/

                                break;

                        case 'Docente':
                                $result = $this->modelo_areas->obtener_docentes_By_areas($ids_areas,$ids_materias,$planest_Id);
                                if($result)
                                    foreach ($result as $row)
                                        $users_groups[]= $row->docente_id;
                                break;

                        case 'Jefe_Area':
                                $result = $this->modelo_areas->obtener_jefes_By_areas($ids_areas, 'jefe');
                                if($result)
                                    foreach ($result as $row)
                                        $users_groups[]= $row->jefe_id;
                                break;

                        case 'Regente_Basico':
                                $result = $this->modelo_areas->obtener_jefes_By_areas($ids_areas, 'regenteB');
                                if($result)
                                    foreach ($result as $row)
                                        $users_groups[]= $row->jefe_id;
                                break;

                        case 'Regente_Orientado':
                                $result = $this->modelo_areas->obtener_jefes_By_areas($ids_areas, 'regenteO');
                                if($result)
                                    foreach ($result as $row)
                                        $users_groups[]= $row->jefe_id;

                                break;
                    }
                }
            }
            else $ids_roles = 0;

            $usersIds = array();
           
            /*if(count($users_groups) > 0){
                $usergroupIds = join(',',$users_groups);  
                $result2 = $this->modelo_usuario->obtener_users_group2($usergroupIds);
                if($result2)
                    foreach ($result2 as $rowrow)
                        $usersIds[]=$rowrow->user_id;
            }

            if(count($usersIds) > 0){
                $usersIds = join(',',$usersIds);
                $result3 = $this->modelo_usuario->get_users_ids($usersIds);
            } */

            if(count($users_groups) > 0){
                $usergroupIds = join(',',$users_groups);  
                $result3 = $this->modelo_usuario->obtener_usersgroup_users_roles($usergroupIds);
            }

           

              //print_r($result);
            if (isset($result3))
            {
                //$result3 = json_encode($result3);
                $this->response($result3, REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron usuarios'
                ], REST_Controller::HTTP_OK); 
            }
        }//fin else 1
    }
    


    public function obtener_reglas_comunicacion_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {   
            $reglas = $this->modelo_permisos->get_reglas($this->input->post('id_colegio'),$this->input->post('rol'));
            if($reglas)
                $reglas = json_encode($reglas);

            $this->response($reglas, REST_Controller::HTTP_OK);  
        }
    }
    public function obtener_menu_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {   
            $reglas = $this->modelo_usuario->get_menu2($this->input->post('id_menus'),$this->input->post('key'));
            //$sql = $this->db->last_query();
            //$sql = json_encode($sql);
            $reglas = json_encode($reglas);

            $this->response($reglas, REST_Controller::HTTP_OK);  
        }
    }
    public function obtener_menu_general_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {   
            $reglas = $this->modelo_usuario->get_menu_general($this->input->post('key'));
            //$sql = $this->db->last_query();
            //$sql = json_encode($sql);
            $reglas = json_encode($reglas);

            $this->response($reglas, REST_Controller::HTTP_OK);  
        }
    }
    public function tutores_colegio_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {   
            $tutores = $this->modelo_usuario->get_tutores_colegio($this->input->get('id_colegio'));
            //$sql = $this->db->last_query();
            //$sql = json_encode($sql);
            //$tutores = json_encode($tutores);


            $this->response([
                    'status' => TRUE,
                    'tutores' => $tutores,
                    
                ], REST_Controller::HTTP_OK);  
        }
    }
    public function asignar_tutor_alumno_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {   
            $id_alumno = $this->input->get('user_id');
            $id_tutor = $this->input->get('id_tutor');
            $result = $this->modelo_usuario->set_tutor_alumno($id_alumno,$id_tutor);

            if($result) {
                $user = $this->modelo_usuario->get_users_id($id_tutor);

                $this->response([
                        'status' => TRUE,
                        'users' => $user,
                    ], REST_Controller::HTTP_OK);  
            }
            else 
                $this->response([
                        'status' => false,
                    ], REST_Controller::HTTP_OK);  
        }
    }
    public function obtener_rolesTodos_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {   
            $idgrupo = $_GET['idgrupo'];
            $idcolegio= $_GET['idcolegio'];

            //print_r('idgrupo:'.$idgrupo);
            //print_r('idcolegio:'.$idcolegio); 

            $reglas = $this->modelo_permisos->get_reglas($idcolegio,$idgrupo);
            if(!$reglas)
                 $result = $this->modelo_usuario->get_rolesTodos($idcolegio);
            else { //print_r($reglas->roles_id);
                 $result = $this->modelo_usuario->get_rolesXrolesid($reglas->roles_id, $idcolegio);

                    }
                   // var_dump($reglas);
            //$permisos = $this->modelo_usuario->get_permisos();
            /*$arre=array(
                        'roles'=>$result,
                        'reglas_cominicacion'=>$permisos,
                        );*/ 
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
                $result = json_encode($result);
                $this->response($result, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron roles'
                ], REST_Controller::HTTP_OK); 
            }
        }
    }

    /*public function comprobar_pin_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $pin = $_POST['pin']; 
            $existe = $this->utilidades->verifica_pin($pin);//verifico que el pin ingresado por el usuario es correcto
            
            if ($existe == 1)
            { 
                $this->response([
                            'status' => TRUE,
                        ], REST_Controller::HTTP_OK); 
            }
            else
            {
                $this->response([
                'status' => FALSE,
                'message' => 'invalido'
                ], REST_Controller::HTTP_OK);  
            }
        }
    }*/ 
   //---------------------------------------------------------------
  public function obtener_nombre_usuario_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $result = $this->modelo_usuario->get_nombre_usuario($userid);
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
                $result = json_encode($result);
                //$result = $result;

                $this->response($result, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontro el usuario'
                ], REST_Controller::HTTP_OK); 
            }
        } 
  } 

    public function obtener_usuariosXrol_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {   
            $nombregrupo = $_GET['nombregrupo'];
            $idcolegio= $_GET['idcolegio'];

            $users = $this->modelo_usuario->get_usuariosXrol($idcolegio,$nombregrupo);

        
            /*$result = array();
            foreach ($users as $key ) {
                $data=array(
                    'id'=> $key->id,
                    'first_name'=> utf8_decode( $key->first_name),
                    'last_name'=> utf8_decode($key->last_name),
                    'user_id'=> $key->user_id,
                );

                $resutl[]=$data;
               
            }

            var_dump($resutl);*/
            
            // Si existe mas de un resultado, lo mando
            if ($users)
            {
                $users = json_encode($users);
                $this->response($users, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron roles'
                ], REST_Controller::HTTP_OK); 
            }
        }
    }
    //multicolegio


    public function obtener_torneoxclub_post()
    {   
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $result = $this->Modelo_torneo->get_obtener_torneoxclub($this->input->post('idclub'));
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
                // $result = json_encode($result);
                
                $this->response($result, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron Torneo'
                ], REST_Controller::HTTP_OK); 
            }
        }
    }
    public function obtener_torneo_x_id_post()
    {   
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {

            $result = $this->Modelo_torneo->obtener_torneo_x_id($this->input->post('id'));
          
                
            $this->response($result, REST_Controller::HTTP_OK);
            
         
            
        }
    }
    public function get_roles_x_colegio_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $idcolegio = $_POST['idcolegio']; 
            $result = $this->modelo_usuario->get_roles_colegio_users($userid, $idcolegio);
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
                $result = json_encode($result);
                $this->response($result, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron roles'
                ], REST_Controller::HTTP_OK); 
            }
        }
    }
    public function get_rolex_colegios_all_users_get()///para la app sirve
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            
            /*$ciclo_a = $this->utilidades->obtener_planestudioaXcolegio($id_colegio);
            $id_plan_est_actual = $this->utilidades->get_planestudio($ciclo_a);*/

            $result = $this->modelo_usuario->rolex_colegios_all_users($userid);
            $roles_agrupados = $this->modelo_usuario->roles_colegios_all_users_group_by($userid);
            $horarios = $this->modelo_horarios->get_horarios_all_user_id_colegio_all($userid);
            $dias = $this->modelo_horario->get_dias();
            $escuelas = $this->modelo_usuario->get_escuelas_all_userd($userid);
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
                $result = json_encode($result);
                $roles_agrupados = json_encode($roles_agrupados);
                $horarios = json_encode($horarios);
                $dias = json_encode($dias);
                $escuelas = json_encode($escuelas);
                 $this->response([
                    'status' => TRUE,
                    'roles' => $result,
                    'roles_agrupados' => $roles_agrupados,
                    'horarios' => $horarios,
                    'dias'=> $dias,
                    'escuelas' => $escuelas
                ], REST_Controller::HTTP_OK); 
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron roles'
                ], REST_Controller::HTTP_OK); 
            }
        }
    }

    public function obtener_usuarios_rol_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $id_rol = $_GET['id_rol']; 
            $es_tutor = $this->Modelo_groups->get_name_rol($id_rol);
            $result = $this->modelo_usuario->get_users_rol($id_rol);
            
               // print_r('es tutor');
                if ($result)
                {
                   // $hijos = $this->Modelo_tutor->get_hijos_de_tutores($id_rol);
                    //$result = json_encode($result);
                    
                     $this->response([
                        'status' => TRUE,
                        'users' => $result,
                        'rol' => $es_tutor->name,
                        //'hijos' => $hijos
                       
                    ], REST_Controller::HTTP_OK); 
                }
                // Sino, envio respuesta con 404
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'No se encontraron roles'
                    ], REST_Controller::HTTP_OK); 
                }
            
        }

    }

    public function obtener_hijos_tutor_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $id_tutor = $_POST['user_id_tutor']; 
            $id_colegio = $_POST['id_colegio_modal_hijos']; 

            $result = $this->Modelo_tutor->get_hijos_de_tutor($id_tutor,$id_colegio);
            if ($result)
                {
                   // $hijos = $this->Modelo_tutor->get_hijos_de_tutores($id_rol);
                    //$result = json_encode($result);
                    
                     $this->response([
                        'estado' => TRUE,
                        'hijos' => $result,
                       // 'query' => $this->db->last_query(),
                       
                        //'hijos' => $hijos
                       
                    ], REST_Controller::HTTP_OK); 
                }
                // Sino, envio respuesta con 404
                else
                {
                    $this->response([
                        'estado' => FALSE,
                        'message' => 'No se encontraron hijos asignados'
                    ], REST_Controller::HTTP_OK); 
                }    
            
        }
    }
    public function obtener_tutores_de_un_alumno_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $id_alumno = $_POST['user_id_hijo']; 
            $id_colegio = $_POST['id_colegio_modal_tutores']; 

            $result = $this->Modelo_tutor->get_tutores_de_un_alumno($id_alumno);
            if ($result)
                {
                   // $hijos = $this->Modelo_tutor->get_hijos_de_tutores($id_rol);
                    //$result = json_encode($result);
                    
                     $this->response([
                        'estado' => TRUE,
                        'tutores' => $result,
                       // 'query' => $this->db->last_query(),
                       
                        //'hijos' => $hijos
                       
                    ], REST_Controller::HTTP_OK); 
                }
                // Sino, envio respuesta con 404
                else
                {
                    $this->response([
                        'estado' => FALSE,
                        'message' => 'No se encontraron hijos asignados'
                    ], REST_Controller::HTTP_OK); 
                }   

        }
    }

    public function obtener_user_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $id_user = $_POST['id_user']; 
            $usuario = $this->modelo_usuario->get_users_id($id_user);

            //print_r($usuario->last_login);

            /*$id_user_group = $_POST['id_user_group'];
            $usuario = $this->modelo_usuario->get_users_id_groups($id_user, $id_user_group);*/

            $this->response($usuario, REST_Controller::HTTP_OK);
        }
    }


    public function obtener_user2_post() //igual que obtener_user_post, pero agrega los user_group
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $id_user = $_POST['id_user']; 

            $colegiosId = $_POST['colegiosId']; 


            $usuario = $this->modelo_usuario->get_users_id($id_user);

            $user_group = $this->modelo_usuario->obtener_users_group($id_user,$colegiosId);

            //print_r($usuario->last_login);

            $this->response([
                    'status' => true,
                    'usuario' => $usuario,
                    'roles' => $user_group,
                ], REST_Controller::HTTP_OK); 
        }
    }



    public function update_usuario_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {   
                $noActualizar=0;
               
                $data = json_decode($this->input->post('data'));

                //print_r( $data);
    
                //$rol = $this->Modelo_groups->get_name_rol($this->input->post('group_id_user_edit'));
                if( $this->input->post('user_id_edit') )
                {
                    $id_user = $this->input->post('user_id_edit');
                }
                else  $id_user = $userid;


                //verifico existencia de username
                //if( $this->input->post('username_txt_edit') )
                if( isset($data->username) )
                {
                    $username = $data->username;

                    if( $this->modelo_usuario->verifica_username($username, $id_user) )
                    {
                        $message = [
                            'status' => false,
                            'existe' => true,
                            'message' => 'El Nombre de Usuario NO está Disponible!',
                            'campo' =>'username',
                        ];
                         $noActualizar=1;
                    }
                }

                //verifico existencia de email
                if( isset($data->email) )
                {
                    $email = $data->email;

                    if( $this->modelo_usuario->verifica_email($email, $id_user) )
                    {
                        $message = [
                            'status' => false,
                            'existe' => true,
                            'message' => 'El EMAIL ya existe!',
                            'campo' =>'email',
                        ];
                         $noActualizar=1;
                    }
                }


                //verifico existencia del dni
                if( isset($data->documento) )
                {
                    $doc_aux = str_replace(".", "", $data->documento);

                    $data->documento = $doc_aux;
               
                    if( $this->modelo_usuario->verifica_dni($doc_aux, $id_user) )
                    {
                        $message = [
                            'status' => false,
                            'existe' => true,
                            'message' => 'El DNI ya existe!',
                            'campo' =>'dni',
                            //'rol' => $rol->name
                        ];
                        $noActualizar=1;
                    }
                    
                }
                else {
                    $message = [
                        'status' => false,
                        'existe' => true,
                        'message' => 'El DNI es obligatorio!',
                        'campo' =>'dni',
                    ];
                    $noActualizar=1;
                }

            
                if($noActualizar == 0)
                {
                    /*$data = array(
                            'first_name' => $this->input->post('nombre_txt_edit'),
                            'last_name' => $this->input->post('apellido_txt_edit'),
                            'email' => $this->input->post('email_txt_edit'),
                            'sexo' => $this->input->post('sexo_txt_edit'),
                            'username' => $username,
                            'documento' => $doc_aux,
                            'active' => '1'
                        );*/

                     //print_r($data);
                    /*$id_user_groups =  $this->input->post('id_user_group_edit');
                    $usuario = $this->modelo_usuario->get_users_id_groups($id_user,$id_user_groups);*/

                    $this->modelo_usuario->update_users($id_user,$data);


                    $usuario = $this->modelo_usuario->get_users_id($id_user);

                    //esto es para volver a insertarlo en el mismo lugar que estaba en la tabla
                    if( $this->input->post('idfila') ) 
                        $usuario->idfila =$this->input->post('idfila');


                    $message = [
                            'status' => TRUE,
                            'existe' => false,
                            'message' => 'El Usuario fue Modificado con Exito',
                            'id_user' =>$id_user,
                            'usuario' =>$usuario,
                            //'rol' => $rol->name
                        ];
                }

               
                $this->set_response($message, REST_Controller::HTTP_OK);
            
        }
    }

    public function reset_clave_usuario_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $id_user = $this->input->post('user_id_reset');
            $usuario = $this->modelo_usuario->get_users_id($id_user);
            if($this->input->post('clave_txt') == "email")
            {
                if($usuario->email == null)
                {
                    $message = [
                            'status' => false,
                            'existe' => TRUE,
                            'message' => 'El Usuario No tiene Email',
                            'id_user' =>$id_user,
                            'usuario' =>$usuario,
                        ];
                        $this->set_response($message, REST_Controller::HTTP_OK);
                }
                else
                {
                    $email_array = explode("@", $usuario->email);

                    $pass = $this->ion_auth_model->hash_password($email_array[0]);
                    $data = array(
                        'iteration' => 0,
                        'password' => $pass,
                        'active' => '1',
                    );
                    $this->modelo_usuario->update_users($id_user,$data);

                    $message = [
                            'status' => TRUE,
                            'existe' => TRUE,
                            'message' => 'La clave se Reseteo con Exito',
                            'id_user' =>$id_user,
                            'usuario' =>$usuario,
                        ];
                        $this->set_response($message, REST_Controller::HTTP_OK);

                    

                }
            }
            else
            {
                if($usuario->documento == null)
                {
                    $message = [
                            'status' => false,
                            'existe' => TRUE,
                            'message' => 'El Usuario No tiene DNI',
                            'id_user' =>$id_user,
                            'usuario' =>$usuario,
                        ];
                        $this->set_response($message, REST_Controller::HTTP_OK);
                }
                else
                {
                    $dni = $usuario->documento;

                    $pass = $this->ion_auth_model->hash_password($dni);
                    $data = array(
                        'iteration' => 0,
                        'password' => $pass,
                        'active' => '1',
                    );
                    $this->modelo_usuario->update_users($id_user,$data);

                    $message = [
                            'status' => TRUE,
                            'existe' => TRUE,
                            'message' => 'La clave se Reseteo con Exito',
                            'id_user' =>$id_user,
                            'usuario' =>$usuario,
                        ];
                        $this->set_response($message, REST_Controller::HTTP_OK);

                    

                }
            }
        }
    }
    public function validar_datos_perfil_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $sql = "SELECT users.* FROM `users` JOIN users_groups on users_groups.user_id = users.id WHERE users_groups.colegio_id = 9 and users.id = $userid";
            $query = $this->db->query($sql);
            if($query->num_rows()>0){

                $query = $query->row();
                $datos_completo = 1;
                if(($query->email == '') or ($query->email == NULL))
                {
                    $datos_completo = 0;
                }
                $message = [
                                'status' => TRUE,
                                'datos_completo' => $datos_completo,
                            ];
            }
            else {
                $message = [
                            'status' => FALSE,
                        ];
            }
            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }
    public function obtener_nombre_usuario_app_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $result = $this->modelo_usuario->get_nombre_usuario($userid);
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
              
                foreach ($result as $key ) 
                {
                   $datos_ = $key;
                }
               
                $this->response($datos_, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontro el usuario'
                ], REST_Controller::HTTP_OK); 
            }
        } 
    }
   function cargar_eventos_filtro_todos_x_id_user($id_user,$id_user_group,$id_colegio)
   {
        $sql = "SELECT eventos.* 
        FROM eventos 
        join filtroevento on filtroevento.evento_id = eventos.id 
        join nodocolegio on nodocolegio.id = eventos.colegio_id
        where eventos.colegio_id = $id_colegio 
        and filtroevento.tipo = 't' 
        and YEAR(eventos.fechaCreacion) = nodocolegio.cicloa ";
        
        $query = $this->db->query($sql);
        $cont_insert = 0;
        foreach ($query->result() as $key) 
        {
            $data = array( 'eventos_id'=>$key->id, 
                'users_id'=>$id_user, 
                'destinatario'=>$id_user, 
                'enviado'=>0,
                'medio'=>'n', 
                'alta'=>'a' , 
                'group_id'=>$id_user_group,

            );
            $this->db->insert("eventosusers",$data);
            //echo "id ".$this->db->insert_id();
            //echo "<br>";
            $cont_insert++;

        }
        return;
   }
   public function obtener_usuarios_rol_post()//la funcion existe como get tambien
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $id_rol = $_POST['id_rol']; 

            $es_tutor = $this->Modelo_groups->get_name_rol($id_rol);
            $result = $this->modelo_usuario->get_users_rol($id_rol);
            
               // print_r('es tutor');
               /* if ($result)
                {
                    $array_result = array('users' => $result,  'sql' => $this->db->last_query(), 'existen'=> 1);
                    $array_result = json_encode($array_result);
                    $this->response($array_result, REST_Controller::HTTP_OK);
                }
                else
                {
                     $array_result = array('users' => $result,  'sql' => $this->db->last_query(), 'existen'=> 0);
                    $array_result = json_encode($array_result);
                    $this->response($array_result, REST_Controller::HTTP_OK);
                }*/
             $result = json_encode($result);
                $this->response($result, REST_Controller::HTTP_OK);   
            
        }

    }

    public function obtener_usuario_post()
    {
        $userid = $_POST['userid'];
        $usuario = $this->modelo_usuario->get_users_id( $userid );
        $this->response($usuario, REST_Controller::HTTP_OK);
    }
    
    
    public function desactivar_alumno_tutores_post() 
    {
        $res = $this->modelo_usuario->desactivar_alumno_tutores( $this->input->post() );
        if(!$res){
           
            $this->response([
                'status' => FALSE,
                'message' => 'Alumno no dado de baja o usuario ya inactivo.'
            ], REST_Controller::HTTP_OK); 
        }
        else{
            $response = json_encode($res);
            $this->response( $response , REST_Controller::HTTP_OK);
           
        }   
    }
 public function inscribir_a_torneo_post(){
    $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {   
            
            $f_incrip=$_POST['fechainscr'];
            $f_inicio=$this->torneos->get_fecha_inicio_x_t($_POST['idtorneo']);
            $f_fin=$this->torneos->get_fecha_fin_x_t($_POST['idtorneo']);
            $f_inicio_num=strtotime($f_inicio);
            $f_fin_num=strtotime($f_fin);

           /* echo "inicio->".$f_inicio."<br>incripcion->".$f_incrip."<br>fin->".$f_fin."<br>";
            echo "<br>incripcion".date("Y-m-d");
            echo "<br>inicio".$this->torneos->get_fecha_inicio_x_t($_POST['idtorneo']);
            echo "<br>fin".$this->torneos->get_fecha_fin_x_t($_POST['idtorneo']);*/
            
            if ($f_incrip>=$f_inicio_num && $f_incrip<=$f_fin_num) {
                
            $arre = array(
                    'torneo'=>$_POST['idtorneo'],
                    'deportista'=>$_POST['iddeportista'],
                    'division'=>$_POST['iddivision'],
                    'especialidad'=>$_POST['idespecialidad'],
                    'eficiencia'=>$_POST['ideficiencia'],
                    'categoria'=>$_POST['idcategoria'],
                    'club'=>$_POST['idclub']
                    );
            $result= $this->Modelo_torneo->guardar_inscripcion_a_torneo($arre);
            $this->response( ['estado'=>$result,
                'status' => TRUE,
                 'message' => 'Se inscribio correctamente'] ,
                  REST_Controller::HTTP_OK);
                
            } else if(($f_incrip<$f_inicio_num)) {
                $this->response([
                'fecha'=>$this->utilidades->fecha_esp($f_inicio),
                'estado'=> 2,   
                'status' => TRUE,
                'message' => 'No esta abirta la inscripción'
            ], REST_Controller::HTTP_OK);     
            } else if (($f_incrip>$f_fin_num)) {
                $this->response([
                'fecha'=> $this->utilidades->fecha_esp($f_fin),   
                'estado'=>3,
                'status' => TRUE,
                'message' => 'Inscripción cerrada'
            ], REST_Controller::HTTP_OK);     
            }
            
        }
 }
 public function obtener_inscriptos_torneo_post(){
    $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {         
           
                    $torneo=$_POST['idtorneo'];
                    $deportista=$_POST['iddeportista'];
                    $division=$_POST['iddivision'];
                    $especialidad=$_POST['idespecialidad'];
                    $club=$_POST['idclub'];
                   
            $result=$this->Modelo_torneo->obtener_inscriptos_torneo($torneo,$deportista,$especialidad,$division,$club);
             
            $this->response(['estado' =>  $result,] , REST_Controller::HTTP_OK);
        }
 }
 public function listar_inscriptos_torneo_post(){

    $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
                        
                    $torneo=$_POST['idtorneo'];
                   
                    $club=$_POST['idclub'];

                  
            $result=$this->Modelo_torneo->lista_inscriptos_torneo($torneo,$club);
             
            $this->response( $result , REST_Controller::HTTP_OK);
        }
 }
public function buscar_torneo_post(){
     $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
                        
                    $torneo=$_POST['idtorneo'];
                   
                    

                   
            $result=$this->Modelo_torneo->buscar_torneo($torneo);
             
            $this->response( $result , REST_Controller::HTTP_OK);
        }
}

public function update_torneo_post()
    {   
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else{
            
            $idtorneo = $this->input->post('txt_idtorneo');
                           
            $torneo = array(
            
            'nombre'      =>$this->input->post('txt_torneo') ,
            'organizador' =>$this->input->post('txt_organizador') ,
            'descripcion' =>$this->input->post('txt_campo_descripcion') ,
            'inicio'      =>$this->input->post('txt_fechacomienzo') ,
            'fin'         =>$this->input->post('txt_fechafin')             
             );

            $this->db->where('id', $idtorneo);
            $this->db->update('torneo', $torneo);

            $this->db->where('id_torneo', $idtorneo);
            $this->db->delete('inscripcion_club');
        
            
            $arre = $this->input->post('txt_cparticipantes');

            
            $partic=null;
            $partic=explode(",", $arre);

            foreach ($partic as $key) {
                $inscrip_club = array(
                                'id_club' => $key ,
                                'id_torneo' => $idtorneo );
                $this->db->insert('inscripcion_club', $inscrip_club);
            }
            
        
           $this->set_response(REST_Controller::HTTP_OK);
           
        } 
    }
    public function obtener_club_x_torneo_post(){

    $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
                        
                    $torneo=$_POST['idtorneo'];
                   
                   

                   
            $result=$this->Modelo_torneo->obtener_club_x_torneo($torneo);
             
            $this->response( $result , REST_Controller::HTTP_OK);
        }
 }
 public function eliminar_torneo_post(){

    $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
                        
                    $torneo=$this->input->post('idtorneo');
                   
                   $arre = array('estado' => 0);

            $this->db->where('id', $torneo);       
            $this->db->update('torneo', $arre);
             
            $this->response(REST_Controller::HTTP_OK);
        }
 }
 public function eliminar_inscripto_post(){

    $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
                        
                $inscr=$this->input->post('idinsc');
                $arre = array('estado' => 0); 
                  

            $this->db->where('id', $inscr);       
            //$this->db->delete('inscripcion_a_torneo');
            $this->db->update('inscripcion_a_torneo', $arre); 

            $this->response(REST_Controller::HTTP_OK);
        }
 }
 public function traer_categoria_inscripcion_post(){

    $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
                        
                $inscr=$this->input->post('idinsc');
                //print_r($_POST); 
                //echo $inscr;
                $result= $this->modelo_inscripciones->traer_categoria_inscripcion($inscr);

             
            $this->response($result,REST_Controller::HTTP_OK);
        }
 }
 
}  // fin controller usuario