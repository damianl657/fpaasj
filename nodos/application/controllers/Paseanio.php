<?php //session_start();
 
class Paseanio extends CI_Controller {
      
	function __construct(){
		parent::__construct();
			//$this->output->enable_profiler(TRUE);
		if(!$this->session->userdata("logged_in")){
               redirect("login");
         }
	}
	


    public function index(){         
        
       // $this->load->view('include/vista_inc_header');
		
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		$data['urlApi'] = $urlApi;
		$data['passApiKey'] = $passApiKey;
		$data['idusuario'] = $this->session->userdata('idusuario');
		/*$data['idcolegio'] = $this->session->userdata('idcolegio');
		$data['idgrupo'] = $this->session->userdata('idgrupo');
		$data['nombregrupo'] = $this->session->userdata('nombregrupo');
		$data['ordengrupo'] = $this->session->userdata('ordengrupo');*/
		

		//var_dump($data);
		
		$data['opcion_menu'] = 'paseanio';
		$band = 0;
		$controlador = "Paseanio";
		$metodo = "index";
		/*foreach ($this->session->userdata('menu') as $menu) //validar permiso de controlador y metodo
        {
        	echo $menu->controlador; echo "--";
        	if(($menu->controlador == $controlador) && ($menu->metodo == 'index'))
        	{
        		
        		$band = 1;// encontro el controlador y el metodo en la sesion, el user tiene permiso
        	}
        }*/

        $colegios = $this->session->userdata("colegios"); 
        
		foreach ($colegios as $cole)
		{
			$idcolegio = $cole["id_colegio"];
			$menus = $cole["menu"];
		 	foreach ($menus as $menu)
			{	
				if( ($menu->controlador === $controlador) && ($menu->metodo === $metodo))
				{
				    $band = 1;
				}
			}
		}

        

        if($band == 1)
        {
		    $data['contenido'] = 'admin/vista_paseanio';
		    //$data['contenido'] = 'panel/vista_horarios';
			$this->load->view('include/template_colegio',$data);
				
		}
		//else redirect(404);

    }


}
?>
