<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_especialidad extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }

        public function obtener_especialidades()
        {
            $sql = "
                SELECT *
                FROM especialidades
                
                ORDER BY nombre ASC
            ";
            $query = $this->db->query($sql);
            return $query->result();
        }

        public function obtener_especialidad($name)
        {
            $sql = "
                SELECT id
                FROM especialidades
                WHERE nombre = '$name'           
            ";
            $query = $this->db->query($sql);
            return $query->result();
        }
		
   	 //----------------------------------------------------------
		/*public function delete_nodoespecialidad($id) 
		{   
			$sql = "DELETE FROM nodoespecialidad WHERE id='$id' ";
			$query = $this->db->query($sql);
			return $query;
		}*/
 
        public function insertar_especialidad($data)
        {
            $query = $this->db->insert("especialidades",$data);
            //$query = $this->db->query($sql);
            return $query;
        }
        //----------------------------------------------------------------------
		
        /*public function insertar_nodoespecialidad($data)
        {  
		    $sql= "SELECT * FROM nodoespecialidad WHERE nodonivel_id=".$data['nodonivel_id']." AND especialidad_id=".$data['especialidad_id'];
			$query_nivel = $this->db->query($sql); 
			
            if($query_nivel->num_rows() > 0){ // si el par ya exite que NO inserte dos veces.--//
				$query= ""; 
			}else{
			    //-- insertar el par (Nivel - Especialidad) --//
			    $query = $this->db->insert("nodoespecialidad",$data);		
			}
			
			return $query;
        }*/
		
		//--------------------------------------------------------------------
		/*public function obtener_especialidades_cargados($id_colegio, $anioA)
        {   
			$sql = "SELECT nodoespecialidad.*, especialidades.nombre as nombre_especialidad, niveles.nombre as nombre_nivel 
                    FROM especialidades 
                    JOIN nodonivel ON especialidades.id= nodonivel.esp_id
					JOIN niveles ON niveles.id=nodonivel.niveles_id 
                    AND nodonivel.cicloa=$anioA
					AND nodonivel.nodocolegio_id=$id_colegio
                    AND  nodonivel.estado=1             					
            ";
            $query = $this->db->query($sql);
            return $query; 
		}*/

        //--------------------------------------------------------------------
        public function get_especialidades_x_nivel($idcolegio, $idplanE, $idnodonivel){
            $sql = "SELECT especialidades.nombre as nombre_especialidad, 
                            niveles.nombre as nombre_nivel 
                    FROM nodonivel
                    JOIN niveles ON niveles.id=nodonivel.niveles_id 
                    JOIN especialidades ON especialidades.id= nodonivel.esp_id
                    AND niveles.cicloa_id=$idplanE
                    AND nodonivel.nodocolegio_id=$idcolegio
                    AND nodonivel.estado=1
                    AND nodonivel.id = $idnodonivel
            ";
            $query = $this->db->query($sql);
            return $query;             
        }
        public function get_all_especialidades()
        {
            $sql ="SELECT * FROM `especialidades`";
            $query = $this->db->query($sql);    
            return $query;
        }
		
	} //--fin Modelo_especialidad