<script src="<?=base_url('assets/js/fancywebsocket.js');?>"></script>


<script> 
      var Server;

      function log( text ) {
        /*$log = $('#log');
        //Add text to log
        $log.append(($log.val()?"\n":'')+text);
        //Autoscroll
        $log[0].scrollTop = $log[0].scrollHeight - $log[0].clientHeight;*/


        $('.top-right').notify({
            message: { text: text },
            fadeOut: { enabled: true, delay: 7000 },
            type: 'info2'
        }).show(); 
      }

      function send( text ) {
        Server.send( 'message', text );
      }

      $(document).ready(function() { //$.noConflict();

        //log('Connecting...');
        Server = new FancyWebSocket('ws://10.0.0.2:9300');

        /*$('#message').keypress(function(e) {
          if ( e.keyCode == 13 && this.value ) {
            log( 'You: ' + this.value );
            send( this.value );

            $(this).val('');
          }
        });*/

        //Let the user know we're connected
        Server.bind('open', function() {
          //log( "Connected." );
        });

        //OH NOES! Disconnection occurred.
        Server.bind('close', function( data ) {
          //log( "Disconnected." );
        });

        //Log any messages sent from server
        Server.bind('message', function( payload ) {
          log( payload );
        });

        Server.connect();
      });
  </script>