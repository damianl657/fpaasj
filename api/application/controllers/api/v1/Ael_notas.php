<?php



    defined('BASEPATH') OR exit('No direct script access allowed');



    require APPPATH . '/libraries/REST_Controller.php';



    class Ael_notas extends REST_Controller {



    function __construct()

    {

        if (isset($_SERVER["HTTP_ORIGIN"])) {

            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

            header("Access-Control-Allow-Credentials: true");

            header("Access-Control-Max-Age: 86400");

        }

        // Access-Control headers are received during OPTIONS requests

        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {

            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))

                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))

                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

            exit(0);

        }

        parent::__construct();

        $this->load->model("modelo_alumno");

        $this->load->model("modelo_tutor");

        $this->load->model("modelo_usuario");

        $this->load->model("modelo_inscripciones");

        $this->load->model("modelo_ael_notas");

    }



    public function obtener_notas_get()

    {

        $userid = $this->utilidades->verifica_userid();

        if ($userid == -1)

        {

            $this->response([

                'status' => FALSE,

                'message' => 'NO_LOGUEADO'

            ], REST_Controller::HTTP_BAD_REQUEST);

        }
        else
        {
            // Hago lo que debe hacer el metodo especificamente
            $idalumno = $this->get('alumno_id'); // obtengo el id del alumno, si es que vino en la url
            $user = $this->modelo_ael_notas->obtener_datos_alumno($idalumno);
            if($user != false)
            {
              //  print_r( $user);
                //ahora hay que hacer CURL A AEL
                $link_ael = $user->link;
                if($user->id_colegio == 9)
                {
                	//$url = $link_ael."/public/index.php/api_notas_nodos/notas/mostrar_notas/".$user->documento;
                	//$url = "https://industrialsarmiento.edu.ar/ael_api/principal/obtener_notas/".$user->documento;
                	//$url ="https://industrialsarmiento.edu.ar/public/index.php/api_notas_nodos/notas/mostrar_notas/";
                    $url ="https://industrialsarmiento.edu.ar/api_ael/index.php/notas/mostrar_notas/".$user->documento;
                     $ch = curl_init();
                    $options = array(
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_URL => $url,
                       // CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","code: $code"),
                        CURLOPT_POST => true,
                        //CURLOPT_POSTFIELDS => array('documento' => $user->documento),
                        CURLOPT_SSL_VERIFYPEER=> false,
                        );

                    curl_setopt_array( $ch, $options);
                    $response = curl_exec($ch);
                    curl_close($ch);
                    $response2 = json_decode($response);
                    $this->response(['status' => true,
                        'message' => 'OK',
                        'data' => json_decode($response),
                        'dato_alu' => $user,
                        'data_sin_json' => $response,
                       // 'url' => $url,
                        ], REST_Controller::HTTP_BAD_REQUEST);
                }
                else
                {
                    if($user->documento == 987654321)
                    {
                        $url = $link_ael."/ael_api/principal/obtener_notas/44249408";
                    }
                    else
                    {
                        $url = $link_ael."/ael_api/principal/obtener_notas/".$user->documento;
                    }

                	
                
                
                    //print_r( $url);
                    $ch = curl_init();
                    $options = array(
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_URL => $url,
                       // CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","code: $code"),
                        CURLOPT_POST => true,
                       // CURLOPT_POSTFIELDS => array('code' => $code),
                        CURLOPT_SSL_VERIFYPEER=> false,
                        );

                    curl_setopt_array( $ch, $options);
                    $response = curl_exec($ch);
                    curl_close($ch);
                    $response2 = json_decode($response);
                   // print_r($response2->padre_bloqueado);
                    if(isset($response2->padre_bloqueado))
                    {
                        if($response2->padre_bloqueado == 1)
                        {
                            if(isset($response2->falta_de_pago))
                            {
                                if($response2->falta_de_pago == 1)
                                {
                                    $ar = array();
                                    $this->response([
                                    'status' => FALSE,
                                    //'data' => json_decode($ar),
                                    'data' => '',
                                    'message' => 'Usuario Bloqueado por Falta de Pago'
                                    ], REST_Controller::HTTP_BAD_REQUEST);
                                }
                            }
                            else
                            {
                                $ar = array();
                                $this->response([
                                'status' => FALSE,
                                //'data' => json_decode($ar),
                                'data' => '',
                                'message' => 'Usuario Bloqueado'
                                ], REST_Controller::HTTP_BAD_REQUEST);
                            }
                                
                        }
                        //print_r($response2->padre_bloqueado);
                    }
                    else
                    {
                        $this->response(['status' => true,
                        'message' => 'OK',
                        'data' => json_decode($response),
                        'dato_alu' => $user,
                        'data_sin_json' => $response,
                        'url' => $url,
                        ], REST_Controller::HTTP_BAD_REQUEST);
                    }
                }
                //var_dump($response);
                //die();    
            }    
            else
            {
                $ar = array();
                $this->response([
                'status' => FALSE,
                //'data' => json_decode($ar),
                'data' => '',
                'message' => 'Nodos sin Ael o Documento Incorrecto'
                ], REST_Controller::HTTP_BAD_REQUEST);

            }



        }

    }

    

    



    

}