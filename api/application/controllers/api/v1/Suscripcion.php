<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';

    class Suscripcion extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model("modelo_suscripcion");
    }

    public function guardarEmail_post()
    {
        if(isset($_POST["email"])){
            $respuesta = $this->modelo_suscripcion->guardar_email($_POST["email"]);
            if($respuesta == 2){//email repetido
                echo "El mail ingresado ya fue registrado anteriormente";
            }
            elseif($respuesta == 0){//se guardo
                $destinos[] = "seba.avila88@gmail.com";
                $mensaje = "Una persona se registro en nodos con email: ".$_POST["email"];
                $from1 = "info@e-nodos.com";
                $from2 = "Web de Nodos";
                $asunto = "Suscripción";
                $this->usuarios->enviar_email($destinos, $mensaje, $from1, $from2, $asunto);
                echo "Gracias por registrarte";
            }
            else{//ocurrio un problema
                echo "Ocurrio un problema al intentar registrar email";
            }
        }
        else{
            echo "Error";
        }
    }
}
