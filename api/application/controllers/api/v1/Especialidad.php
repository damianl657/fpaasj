<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
class Especialidad extends REST_Controller {

    function __construct()
    {
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        
        parent::__construct();
        $this->load->model("modelo_especialidad");
	    $this->load->model("modelo_colegio");
		$this->load->model("modelo_nivel");
		$this->load->model("modelo_anio");
		$this->load->model("modelo_usuario");

        // Configurar limites para cada uno de los metodos, no solo controlador
        // La tabla limits tiene que estar creada y la opcion limits TRUE en application/config/rest.php
        
        //$this->methods['tutores_get']['limit'] = 1; // 500 peticiones por hora por usuario/key
        //$this->methods['user_post']['limit'] = 100; // 100 peticiones por hora por usuario/key
        //$this->methods['user_delete']['limit'] = 50; // 50 peticiones por hora por usuario/key
    }

    public function especialidades_get()
    {  
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {  
	        $result = $this->modelo_especialidad->obtener_especialidades();
			// Si existe mas de un resultado, lo mando
			if ($result)
			{   $data = json_encode($result);
				$this->response($data, REST_Controller::HTTP_OK);
			}
			// Sino, envio respuesta con 404
			/*else
			{
				$this->response([
					'status' => FALSE,
					'message' => 'No se encontraron especialidades'
				], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
			}*/
		}	
    }// fin function

   

    public function especialidad_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        { 
			$data = json_decode($_POST['data']) ;
           
			$result0 = $this->modelo_especialidad->obtener_especialidad($data->nombre);
			if (!$result0) // para NO insertar filas repetidas
			{
				$result = $this->modelo_especialidad->insertar_especialidad($data);          
				if ($result)
				{
					$ultimoId=$this->db->insert_id(); 
					$this->response([
							'status' => TRUE,
							'message' => 'La Especialidad dada de Alta',
							'id' => $ultimoId 
						], REST_Controller::HTTP_OK);
				}
				else
				{
					$this->response([
						'status' => FALSE,
						'message' => 'Error al dar de alta'
					]); // NOT_FOUND (404) being the HTTP response code
				}
			}
			else
			{
				$this->response([
					'status' => FALSE,
					'message' => 'existe'
				]); // NOT_FOUND (404) being the HTTP response code
			}
		}
    }//-- fin function
	
	//------------ Insertar Niveles con sus Especialidades ---------------
	                
	public function insertespecialidades_post()
    {   
	    $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {     
			$data = json_decode($_POST['data']);
			$colegioId = 1;  // revisar para que el idcolegio lo traiga una sola vez en el arreglo.
			$anioA = date('Y');
			//-- obtener y borrar los niveles que ya tienen especialidad cargada.
			/*$result = $this->modelo_especialidad->obtener_especialidades_cargados($colegioId,$anioA);
			// Si existen especialidades cargadas. primero las recorro y luego Delete.
			if ($result)
			{  
		        if($result->num_rows() > 0 ){
					foreach($result->result() as $row_nodoesp){
						$result = $this->modelo_especialidad->delete_nodoespecialidad($row_nodoesp->id);   
					}
				}else{
			       	//...	
				}
			}else{
				//...
			}*/
			var_dump($data);
			foreach ($data as $key ){ // recorrer los datos del  formulario $_POST
				$cant_especialidad = count($key->especialidades_id); 
				if( $cant_especialidad > 0 ){
					for ($i = 0; $i < $cant_especialidad; $i++){
						$campos = array(
										'nodonivel_id'=> $key->niveles_id,
										'especialidad_id'=> $key->especialidades_id[$i]
						);
						$result = $this->modelo_especialidad->insertar_nodoespecialidad($campos);
					}
				}else{
					 
				}
			} // fin foreach
			 
			$result2 = $this->modelo_usuario->update_pasos($colegioId,2); 

            if ($result2)
            {
                $this->response([
                        'status' => TRUE,
                        'message' => 'Paso 2 Guardado', 
                    ], REST_Controller::HTTP_OK);  
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Error al Guardar Paso 2'
                ]); // NOT_FOUND (404) being the HTTP response code
            }   
			
		 
	    }
		
    } // fin insertespecialidades_post.
	
	//------------------------------------------------------ STEP 2
	public function especialidades_cargados_get(){
		$userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {   
            $idcolegio = $_GET['idcolegio'];  


            $anioA = $this->utilidades->obtener_planestudioaXcolegio($idcolegio);
            $idplanestudio = $this->utilidades->get_planestudio($anioA);

			$result_niveles = $this->modelo_nivel->obtener_niveles_cargados($idcolegio, $idplanestudio);
		
			// Si existe mas de un resultado, lo mando
			$fila = "";
			if ($result_niveles)
			{
					foreach($result_niveles as $row_nodoniv){
						$result_esp = $this->modelo_especialidad->get_especialidades_x_nivel($idcolegio, $idplanestudio, $row_nodoniv->idnodonivel);

						//$fila .= "<tr><td>";
					    //$fila .= "<div class='droppable ui-widget-header' name='suelta' id='niveles_".$row_nodoniv->idnodonivel."'>".$row_nodoniv->nombre_nivel;
                        //$fila .= " <input type='hidden' name='nivel_step2' value=".$row_nodoniv->idnodonivel.">";

						//Armo el box del nivel
			            $fila .= '<div class="box-drag" >';
						$fila .= '<h4>'.$row_nodoniv->nombre_nivel.'</h4>';						
						$fila .= '<div class="inner-box-drag">';
						$fila .= '<ul class="sueltaP2" name="suelta" id="niveles_'.$row_nodoniv->idnodonivel.'"	>';
						$fila .= "<input type='hidden' name='nivel_step2' value=".$row_nodoniv->idnodonivel.">";
						$fila .= '<li></li>';
						if($result_esp->num_rows() > 0){ // Si tiene cargado en Nodoespecialidad
							foreach($result_esp->result() as $row_esp){
							    //--- obtiene la parte de especialidades.
								  
								//<div  id='especialidad_".$row_esp->id_nodoesp."' class='draggable ui-draggable ui-draggable-handle' name='arrastrable' style='position:relative'>

								$fila .= "<li>
										<span>
								  <div  id='especialidad_".$row_esp->id_nodoesp."' class='draggable ui-draggable ui-draggable-handle' name='arrastrable' style='position:relative'>
								  ".$row_esp->nombre_especialidad."
								  <input type='hidden' name='especialidad_step2' value =".$row_esp->especialidad_id.">
								  </div></span></li>";	                                                 
								 //---
								  $fila .= "<button class='closeStep2'></button> </div>"; 
							}
						}else{  // si no existen especialidades cargadas para los Nivels						
						}
						$fila .= '</li>';
						$fila .= '</div>';								
					    $fila .= '</div>'; 
					}
				
			}else{
				$fila .=" - ";
			}
			
			$data = json_encode($fila);
           			
 			$this->response($data, REST_Controller::HTTP_OK);			
		}
		 
	} // fin function
    	
	public function especialidades_cargados_get_anterior()
    {  
     	$userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {  
			$idcolegio = 1;  //aca debo traerlo a partir del id de usuario
            $anioA = date('Y');
            //$result = $this->modelo_especialidad->obtener_especialidades_cargados($idcolegio, $anioA);
			$result_nivConEsp = $this->modelo_anio->obtener_NivConEsp($idcolegio,  $anioA);  
            $result_nivSinEsp = $this->modelo_anio->obtener_NivSinEsp($idcolegio,  $anioA);         
            $result = array_merge($result_nivConEsp, $result_nivSinEsp);
		   
		    if($result){ // tiene cargado en la tabla nodoespecialidad. 
				$data = json_encode($result);
				$this->response($data, REST_Controller::HTTP_OK);
						
			}else{  // si no tiene cargada especialidades.
			    
				$result_niveles = $this->modelo_nivel->obtener_niveles_cargados($idcolegio, $anioA);
				
				// Si existe mas de un resultado, lo mando
				if ($result_niveles)
				{
					$data_nodonivel = json_encode($result_niveles);
					$this->response($data_nodonivel, REST_Controller::HTTP_OK);
				}			   
			   // Sino, envio respuesta con 404
				else
				{
					$this->response([
						'status' => FALSE,
						'message' => 'No se encontraron niveles cargados'
					], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
				}
			}
			
		}
	}
	//------------------------------------------------------ STEP 3
	  
	 public function anios_especialidades_cargados_get()
    {  
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {  
            $colegioId = $_GET['idcolegio'];  //aca debo traerlo a partir del id de usuario
            $anioA = date('Y'); 
            
		    $result_nivConEsp = $this->modelo_nivel->obtener_NivConEsp($colegioId,  $anioA);  
            $result_nivSinEsp = $this->modelo_nivel->obtener_NivSinEsp($colegioId,  $anioA);         
            $result = array_merge($result_nivConEsp, $result_nivSinEsp);   
			
			//$result = $this->modelo_especialidad->obtener_especialidades_cargados($idcolegio, $anioA);
		    $fila = "";
		    foreach($result as $row_detalle){
			  
			   //$result1 = $this->modelo_anio->get_anios_BynodonivANDnodoesp($row_detalle->nodonivel_id, $row_detalle->especialidad_id );  
				
				$result_anios = $this->modelo_anio->get_all_anios();	// obtiene todo los anios
				
				//$fila .= "<tr class='item_filas' id ='".$row_detalle->id."'name= 'nivel_step2'>";  
				$fila .= "<tr class='item_filas' id ='".$row_detalle->nodonivel_id."'name= 'nivel_step2'>";  
			
				$nivel = isset($row_detalle->nombre_nivel)? $row_detalle->nombre_nivel : '';
				$especialidad = isset($row_detalle->nombre_especialidad)? $row_detalle->nombre_especialidad : '';		
				$fila .= "<td><div align = 'center'>".$nivel."</div></td>";
				$fila .= "<td><div align = 'center'>".$especialidad."</div></td>"; 	
				 
				$fila .= "<td><form>";
				foreach($result_anios->result() as $row_anios)
				{ // Recorre Todos los Años.
					$is_checked = 0;
					if(isset($row_detalle->nodoesp_id)){ // Controla si tiene especialidad cargada.
						$result1 = $this->modelo_anio->get_anio_in_NodoAnio($row_anios->id, $row_detalle->nodonivel_id, $row_detalle->nodoesp_id);
                        if($result1 == TRUE){
				            $is_checked = 1;			
						}
						 						
					}
					//if($is_checked == 0)
					else{ 
				        $result2 = $this->modelo_anio->get_anio_in_NodoAnio_con_NodoEspNull($row_anios->id, $row_detalle->nodonivel_id);         		
						if($result2 == TRUE){
				            $is_checked = 1;			
						}
						//else  echo "NO entraa";					
					}
					
					if($is_checked ==1){
						$ch = 'checked = true';
					}else{
						$ch = '';
					}
						
					//is_null($row_detalle->nodoesp_id) 
					//isset Determina si una variable está definida y no es NULL
					$nodoesp_id = isset($row_detalle->nodoesp_id)? $row_detalle->nodoesp_id : '0';

					/*<div>
						1º
						<input type="checkbox" name="anios" value="1">
					</div>*/
										
					$fila .= "<div class=''> ".$row_anios->nombre." <input type='checkbox' name =".$nodoesp_id."  id =".$row_anios->id ."  class = 'check' value = ".$row_anios->id." ".$ch."></div>"; 
				}
				
				$fila .= "</form></td></tr>";
				
			}
		
			$data = json_encode($fila);
			$this->response($data, REST_Controller::HTTP_OK);
	    }
    } // fin get especialidades_cargados
	
	//----------------------------------------------------------------------------
	public function baja_especialidad_post()
    {
        $userid = $this->utilidades->verifica_userid();
    
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {   
            $id_especialidad = $_POST['id_esp'] ;
            $result2 = $this->modelo_especialidad->baja_especialidad($id_especialidad); 
                      
            if ($result2)
            {
                $this->response([
                        'status' => TRUE,
                        'message' => 'La especialidad se dio de baja', 
                    ], REST_Controller::HTTP_OK);  
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Hubo error al cargar paso 2'
                ]); // NOT_FOUND (404) being the HTTP response code
            }   
        }    
    }
    public function get_all_especialidades_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
           // print_r("aaa");
            $result = $this->modelo_especialidad->get_all_especialidades();
            if($result->num_rows() > 0)
            {
               
                //$result = utf8_decode($result);
                //print_r($result);
                //echo "<br><br>";
               
                $data = array
                    ( 
                        'status' => 1,
                        'message' => 'ok',
                        'cant_especialidades' => $result->num_rows(),
                        'especialidades' => $result->result(),
                    );
                $data = json_encode($data);
                $this->response($data, REST_Controller::HTTP_OK); 
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => 1,
                    'message' => 'ok',
                    'cant_especialidades' => $result->num_rows(),
                    'especialidades' => $result->result(),
                ], REST_Controller::HTTP_OK); 
            }
        }
    }
	
	
} //-Fin Controller
