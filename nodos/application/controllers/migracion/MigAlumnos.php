<?php


    class MigAlumnos extends CI_Controller {

    function __construct(){
        parent::__construct();
            
        if(!$this->session->userdata("logged_in")){
               redirect("login");
        }

        $this->load->model("modelo_migracion");
    }   



    public function alumnos (){
        $url = $_SERVER['DOCUMENT_ROOT']."/nodosweb/application/controllers/migracion/";
        $file = $url."AlumnosModeloPrimaria.csv";

        $aux=array();

        if (($fichero = fopen($file, "r")) !== FALSE) {
            while (($datos = fgetcsv($fichero, 1000)) !== FALSE) {
                // Procesar los datos.
                // En $datos[0] está el valor del primer campo,
                // en $datos[1] está el valor del segundo campo, etc...

                if($datos[0]!='')
                {
                    //APELLIDO, NOMBRE,SEXO,DNI,CURSO,Id division,NACIM,TEL
                    $apellido = utf8_decode($datos[0]);
                    $nombre = utf8_decode($datos[1]);
                    $sexo = utf8_decode($datos[2]);
                    $documento = utf8_decode($datos[3]);
                    $division = utf8_decode($datos[5]);
                    $fechanac = utf8_decode($datos[6]);
                    $telefono = utf8_decode($datos[7]);

                    if($fechanac!=''){
                        $arre = array();
                        $arre = explode('/',$fechanac);
                        $fechanac = $arre[2].'-'.$arre[1].'-'.$arre[0];
                    }

                    $datosAlu = array(
                        'last_name' => $apellido,
                        'first_name' => $nombre,
                        'documento' => $documento,
                        'sexo' =>$sexo,
                        'phone' =>$telefono,
                        'fechanac' =>$fechanac,
                        'active' =>0,
                        'pago' =>1,
                    );   

                    if(0)
                    {
                        $IdAlu = $this->modelo_migracion->insert_user($datosAlu);
                        if($IdAlu)
                        {
                            //$IdAlu = $this->db->insert_id();

                            $datosUserG = array(
                                'user_id' => $IdAlu,
                                'group_id' => 83,
                                'colegio_id' =>7,
                                'estado' =>1,
                            );   

                            $IdUSerG=$this->modelo_migracion->insert_user_group($datosUserG);
                            if($IdUSerG)
                            {
                                //$IdUSerG = $this->db->insert_id();

                                $datosInsc = array(
                                    'alumno_id' => $IdUSerG, //user_group
                                    'divisiones_id' => $division,
                                    'planesestudio_id' =>3,
                                    'activo' =>1,
                                );   
                                $IdInsc = $this->modelo_migracion->insert_inscripcion2($datosInsc);
                                if(!$IdInsc)
                                    echo "No inserta inscripcion: ".$documento;
                            }
                            else echo "No inserta user group: ".$documento;
                        }
                        else echo "No inserta user: ".$documento;

                    }

                }


            }

            echo "<br><br><br>";
            print_r($aux);
            //Array ( [0] => 45546392 [1] => 44915289 [2] => 44915382 [3] => 42990851 )

        }

    }


    public function crear_pass($pass)
    {
        //echo $pass;
        $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
                    $passApiKey = $this->variables->get_apikey();
                    //$correo = "nodos";
                    $url = $urlApi."/login/prueba_encriptar";
                    $ch = curl_init();
                    $options = array(
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_URL => $url,
                                CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey"),
                                CURLOPT_POST => true,
                                //CURLOPT_POSTFIELDS => json_encode($fields),
                                //CURLOPT_POSTFIELDS => $fields,
                                CURLOPT_POSTFIELDS => array(
                                                            'data' => $pass
                                                    ),  
                                CURLOPT_SSL_VERIFYPEER=> false,
                            );
                      curl_setopt_array( $ch, $options);
                      $response = curl_exec($ch);
                      curl_close($ch);
                     $response = json_decode($response, True);
                    
                     $arre = (array) $response; //convierto el obj en un arreglo.
                     $pass_encriptado = $arre['pass'];
                     //echo $pass_encriptado;
                     return $pass_encriptado;
    }

    public function alumnos_tutores (){
        $url = $_SERVER['DOCUMENT_ROOT']."/nodosweb/application/controllers/migracion/";
        $file = $url."datos-padres-primaria-22-3-18.csv";

        $aux=array();

        if (($fichero = fopen($file, "r")) !== FALSE) {
            while (($datos = fgetcsv($fichero, 1000)) !== FALSE) {
                // Procesar los datos.
                // En $datos[0] está el valor del primer campo,
                // en $datos[1] está el valor del segundo campo, etc...

                //if($datos[0]!='')
                if(true)
                {                 
                   //CURSO ;APELLIDO;NOMBRE;SEXO;DNI ALUMNO;APELLIDO  PAPA;NOMBRE PAPA;DNI PAPA;CORREO PAPA;APELLIDO MAMA;NOMBRE MAMA;DNI MAMA;CORREO MAMA
                    /*$apellido = utf8_decode($datos[1]);
                    $nombre = utf8_decode($datos[2]);
                    //$sexo = utf8_decode($datos[2]);
                    $documento = utf8_decode($datos[3]);
                    $division = utf8_decode($datos[4]);
                    
                    $apellidopadre = utf8_decode($datos[5]);
                    $nombrepadre = utf8_decode($datos[6]);
                    $dnipadre = utf8_decode($datos[7]);
                    $emailpadre = $datos[8];


                    $apellidomadre = utf8_decode($datos[9]);
                    $nombremadre = utf8_decode($datos[10]);
                    $dnimadre = utf8_decode($datos[11]);
                    $emailmadre = $datos[12];*/

                    $array_div["1A"] = 192;
                    $array_div["1B"] = 193;
                    $array_div["1C"] = 194;
                    $array_div["1D"] = 195;

                    $array_div["2A"] = 196;
                    $array_div["2B"] = 197;
                    $array_div["2C"] = 198;
                    $array_div["2D"] = 199;

                    $array_div["3A"] = 200;
                    $array_div["3B"] = 201;
                    $array_div["3C"] = 202;
                    $array_div["3D"] = 203;

                    $array_div["4A"] = 204;
                    $array_div["4B"] = 205;
                    $array_div["4C"] = 206;
                    $array_div["4D"] = 207;

                    $array_div["5A"] = 208;
                    $array_div["5B"] = 209;
                    $array_div["5C"] = 210;
                    $array_div["5D"] = 211;

                    $array_div["6A"] = 212;
                    $array_div["6B"] = 213;
                    $array_div["6C"] = 214;
                    $array_div["6D"] = 215;                    

                    $apellido = $datos[1];
                    $nombre = $datos[2];
                    $sexo = $datos[3];
                    $documento = $datos[4];
                    $division = $array_div[$datos[0]];
                    //$fechanac = $datos[6];                    
                    
                    $apellidopadre = $datos[5];
                    $nombrepadre = $datos[6];
                    $dnipadre = $datos[7];
                    $emailpadre = $datos[8];
                    //$ocup_padre = $datos[8]


                    $apellidomadre = $datos[9];
                    $nombremadre = $datos[10];
                    $dnimadre = $datos[11];
                    $emailmadre = $datos[12];
                    //$ocup_madre = $datos[11];

                    /*if($fechanac!=''){
                        $arre = array();
                        $arre = explode('/',$fechanac);
                        $fechanac = $arre[2].'-'.$arre[1].'-'.$arre[0];
                    }*/

                    $passAlu = $this->crear_pass($documento);                    

                    $datosAlu = array(
                        'last_name' => $apellido,
                        'first_name' => $nombre,
                        'documento' => $documento,
                        'sexo' => $sexo,
                        //'phone' =>$telefono,
                        //'fechanac' =>$fechanac,
                        'password'=> $passAlu,
                        'active' =>1,
                        'pago' =>1,
                    );

                    if(true)
                    {
                        $IdAlu = $this->modelo_migracion->insert_user($datosAlu);
                        if($IdAlu)
                        {
                            //$IdAlu = $this->db->insert_id();

                            $datosUserG = array(
                                'user_id' => $IdAlu,
                                'group_id' => 83,
                                'colegio_id' =>7,
                                'estado' =>1,
                            );   

                            $IdUSerG=$this->modelo_migracion->insert_user_group2($datosUserG);
                            if($IdUSerG)
                            {
                                //$IdUSerG = $this->db->insert_id();

                                $datosInsc = array(
                                    'alumno_id' => $IdUSerG, //user_group
                                    'divisiones_id' => $division,
                                    'planesestudio_id' =>3,
                                    'activo' =>1,
                                );   
                                $IdInsc = $this->modelo_migracion->insert_inscripcion2($datosInsc);
                                if(!$IdInsc)
                                    echo "No inserta inscripcion: ".$documento;
                            }
                            else echo "No inserta user group: ".$documento;


                            //PADRE
                            if($emailpadre!='' || $dnipadre!=''){
                                if($emailpadre!=''){
                                    $email_arrayPa = explode("@", $emailpadre);
                                    $passPa = $this->crear_pass($email_arrayPa[0]);                               
                                }

                                if($dnipadre!=''){
                                    $passPa = $this->crear_pass($dnipadre);                                   
                                }

                                $datosPa = array(
                                    'username'=> $email_arrayPa[0],
                                    'password'=> $passPa,
                                    'last_name' => $apellidopadre,
                                    'first_name' => $nombrepadre,
                                    'active' =>1,
                                    'pago' =>0,
                                );

                                if($emailpadre!=''){
                                    $datosPa['email'] = $emailpadre;
                                }

                                if($dnipadre!=''){
                                    $datosPa['documento'] = $dnipadre;
                                }

                                if($dnipadre!=''){
                                    $IdPadre = $this->modelo_migracion->insert_user($datosPa);                                   
                                }                                
                                else if($emailpadre!=''){
                                    $IdPadre = $this->modelo_migracion->insert_user2($datosPa);
                                }

                                if($IdPadre)
                                {
                                    $datosUserG2 = array(
                                        'user_id' => $IdPadre,
                                        'group_id' => 82,
                                        'colegio_id' =>7,
                                        'estado' =>1,
                                    ); 
                                    $IdUSerG2=$this->modelo_migracion->insert_user_group($datosUserG2);

                                    if($IdUSerG2)
                                    {
                                        $turoralu = array(
                                            'alumno_id' => $IdAlu,
                                            'tutor_id' => $IdPadre,
                                        ); 
                                        $this->modelo_migracion->insert_tutoralumno($turoralu);
                                    }
                                    else echo "No inserta usergroup Padre".$IdPadre;
                                }
                                else echo "No inserta Padre".$emailpadre;
                            }
                            else echo "no email Padre".$emailpadre;

                            //MADRE
                            if($emailmadre!='' || $dnimadre!=''){
                                if($emailmadre!=''){
                                    $email_arrayMa = explode("@", $emailmadre);
                                    $passMa = $this->crear_pass($email_arrayMa[0]);                                   
                                }

                                if($dnimadre!=''){
                                    $passMa = $this->crear_pass($dnimadre);                                   
                                }

                                //var_dump($passMa);
                                $datosMa = array(
                                    'username'=> $email_arrayMa[0],
                                    'password'=> $passMa,
                                    'last_name' => $apellidomadre,
                                    'first_name' => $nombremadre,
                                    'active' =>1,
                                    'pago' =>0,
                                );   

                                if($emailmadre!=''){
                                    $datosMa['email'] = $emailmadre;
                                }

                                if($dnimadre!=''){
                                    $datosMa['documento'] = $dnimadre;
                                }

                                if($dnimadre!=''){
                                    $IdMadre = $this->modelo_migracion->insert_user($datosMa);                                   
                                }                                
                                else if($emailmadre!=''){
                                    $IdMadre = $this->modelo_migracion->insert_user2($datosMa);
                                }

                                if($IdMadre)
                                {
                                    $datosUserG3 = array(
                                        'user_id' => $IdMadre,
                                        'group_id' => 82,
                                        'colegio_id' =>7,
                                        'estado' =>1,
                                    ); 
                                    $IdUSerG3=$this->modelo_migracion->insert_user_group($datosUserG3);

                                    if($IdUSerG3)
                                    {
                                        $turoralu = array(
                                            'alumno_id' => $IdAlu,
                                            'tutor_id' => $IdMadre,
                                        ); 
                                        $this->modelo_migracion->insert_tutoralumno($turoralu);
                                    }
                                    else echo "No inserta usergroup Madre".$IdMadre;
                                }
                                else echo "No inserta Madre".$emailmadre;
                            }
                            else echo "no email Madre".$emailmadre;
                        }
                        else echo "No inserta Alumno: ".$documento;
                    }
                }
            }

            echo "<br><br><br>";
            print_r($aux);
            //Array ( [0] => 45546392 [1] => 44915289 [2] => 44915382 [3] => 42990851 )

        }

    }

    function actualizar_password_modelo_primaria(){
        $sql = "SELECT users.*
                FROM users 
                JOIN users_groups ON users.id = users_groups.user_id
                WHERE users_groups.colegio_id = 7 AND group_id = 82 AND iteration = 0";
        $lista = $this->db->query($sql);
        foreach ($lista->result() as $key => $value) {
            if($value->documento != ''){
                $pass = $this->crear_pass($value->documento);
                $datos = array(
                    "password" => $pass
                );
                $this->db->update("users",$datos,"id = ".$value->id);
            }
        }
    }

    public function importar_docentes()
    {
        $group_id = 81;
        $colegio_id = 7;
        $plan_estudio_id = 3;
        $cont = 0;

        $url = $_SERVER['DOCUMENT_ROOT']."/nodosweb/application/controllers/migracion/";
        $file = $url."Personal_Primaria_23_03_18.csv";

        $aux=array();

        if (($fichero = fopen($file, "r")) !== FALSE) {
            while (($value = fgetcsv($fichero, 1000)) !== FALSE) 
            {
                $sql_check= "SELECT * FROM users WHERE documento='".$value[2]."'";
                $query_ch = $this->db->query($sql_check);
                if($query_ch->num_rows() == 0)
                {
                    $pass_alu = $this->crear_pass($value[2]);
                    if($value[3] == null)
                    {
                        $email = "";
                    }
                    else
                    {
                        $email = $value[3];
                    }

                    $insert_docente = "INSERT INTO users (first_name, last_name, active, documento, sexo, password,email)
                            VALUES ('$value[1]', '$value[0]',1, '$value[2]', 'F','$pass_alu','$email')";
                                
                   if($this->db->query($insert_docente))
                   {
                        $id_user_docente = $this->db->insert_id();                    
                        //insertar en users_groups
                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id)
                                VALUES ($id_user_docente, $group_id, $colegio_id)";
                        $this->db->query($insert_users_groups);
                         $cont = $cont + 1;
                           
                    }
                } // fin if el alumno ya existe.
                else{
                    $docente = $query_ch->row();
                    $id_doc = $docente->id;

                    echo "EL Docente".$value[2]." ".$value[0]." ".$value[1]." Existe en USERS <br>";
                    $sql_check_user_group= "SELECT * FROM users_groups WHERE user_id='".$id_doc."'and group_id = $group_id";
                    $query_ch_user_group = $this->db->query($sql_check_user_group);
                    if($query_ch_user_group->num_rows() > 0)
                    {
                        echo "EL Docente YA ES DOCENTE EN LA ESCUELA SELECCIONADA<br>";
                    }
                    else
                    {
                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id)
                                VALUES ($id_doc, $group_id, $colegio_id)";
                        $this->db->query($insert_users_groups);
                         //$cont = $cont + 1;
                    }
                    
                }
                
            } // fin for recorrido de los alumnos.
            echo "se insertaron: ".$cont." Docentes<br>";
        }
    } // fin table importar_docentes_st    
}
