    <?php //$this->load->view("vista_inc_header"); ?>

 

        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-9">
                <h2>Sistema</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="<?php echo base_url("login/principal"); ?>">NODOS</a>
                    </li>
                    <li class="active">
                        <strong>Calendario</strong>
                    </li>
                </ol>
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center m-t-lg">
                        <h1>
                            Calendario y Editor WYSIWYG
                        </h1>
                        <h3>
                            Ejemplos de implementacion
                        </h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <p>&nbsp;</p>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>CALENDARIO <small>Prueba de implementacion de FullCalendar</small></h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="agenda_visitas"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>EDITOR WYSIWYG <small>Prueba de implementacion de CKEditor</small></h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#">Config option 1</a>
                                    </li>
                                    <li><a href="#">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <textarea id="editor1" name="editor1">Aqui carga un texto enriquecido CKEDITOR</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>

    <!--<script type="text/javascript" src="<?php echo $this->config->item('path_views_admin'); ?>js/plugins/ckeditor/ckeditor.js"></script>
    <link rel="stylesheet" href="<?php // echo $this->config->item('path_views_admin'); ?>js/plugins/fullcalendar/fullcalendar.min.css" media="screen">
    <script src="<?php// echo $this->config->item('path_views_admin'); ?>js/plugins/moment/moment-with-locales.min.js"></script>
    <script src="<?php// echo $this->config->item('path_views_admin'); ?>js/plugins/fullcalendar/fullcalendar.min.js"></script>
    <script src="<?php// echo $this->config->item('path_views_admin'); ?>js/plugins/fullcalendar/lang/es.js"></script> -->

    <script type="text/javascript">
        
        jQuery(document).ready(function($) {
            CKEDITOR.replace( 'editor1' );

        });

        jQuery('#agenda_visitas').fullCalendar({
            weekends: true,
            header: {
                left: 'prev,next,today',
                center: 'title',
                right: 'month,agendaWeek,basicDay'
            },              
            defaultView: 'month',
            editable: false,
            firstDay: 1,
            hiddenDays: [ 0 ],

            businessHours: {
                start: '7:00', // a start time (10am in this example)
                end: '21:00', // an end time (6pm in this example)
                dow: [ 1, 2, 3, 4, 5, 6 ]
                // days of week. an array of zero-based day of week integers (0=Sunday)
                // (Monday-Thursday in this example)
            },
            minTime: '07:00:00',
            maxTime: '21:00:00',
            lang: 'es'
        });


    </script>

    <?php //$this->load->view("vista_inc_pie"); ?>