<?php 
 
class Alumno extends CI_Controller {	
      
	function __construct(){
		parent::__construct();
			
		if(!$this->session->userdata("logged_in")){
               redirect("login");
         }
	}		

    //-------------------------------------------------------

    public function index(){ 

	    $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		$data['urlApi'] = $urlApi;
		$data['passApiKey'] = $passApiKey;
		$data['idusuario'] = $this->session->userdata('idusuario');
		$data['idcolegio'] = $this->session->userdata('idcolegio');
		$data['idgrupo'] = $this->session->userdata('idgrupo');
		$data['nombregrupo'] = $this->session->userdata('nombregrupo');
		$data['ordengrupo'] = $this->session->userdata('ordengrupo');
        
		//$data['idusergrupo'] = $this->session->userdata('idusergrupo');
		/*$response = $this->get_divisiones_x_colegio($urlApi, $passApiKey, $idUser, $idcolegio,$data['nombregrupo']);
		
		$data['divisiones'] = json_decode($response);

		
		/*if( (isset($$response->status)) && ($$response->status==false) && ($$response->message=='NO LOGUIN') ){
			
		}else{
			$response = json_decode($response);
			
			$data['response'] = $response;
		}*/

		$data['opcion_menu'] = 'Alumno';
		$band = 0;
		$controlador = "Alumno";
		foreach ($this->session->userdata('menu') as $menu) //validar permiso de controlador y metodo
                {
                	if(($menu->controlador == $controlador) && ($menu->metodo == 'index'))
                	{
                		$band = 1;// encontro el controlador y el metodo en la sesion, el user tiene permiso
                	}
                }
        if($band == 1)
        {
    	
	    	//$this->load->view("panel/funciones_calendario",$data); 	
		    //$this->load->view('panel/vista_horarios',$data);
		    $tutor = true;
			$alumno = true;
			foreach ($this->session->userdata('colegios') as $colegio) 
			{
				//var_dump($colegio['rol']);
				if($colegio['rol'] != 'Tutor')
				{
					$tutor = false;
					//die();
				}
				if($colegio['rol'] != 'Alumno')
				{
					$alumno = false;
					//die();
				}

			}
				# code...
			
			if($tutor == true)
						{
							//echo "hola tutores";
							$divisiones = $this->get_divisiones_x_colegio($urlApi, $passApiKey, $data['idusuario'], $data['idcolegio'], $data['nombregrupo']);	
							$data['divisiones'] = json_decode($divisiones);

							$horarios = $this->get_horarios($urlApi, $passApiKey, $data['idusuario'], $data['idcolegio']);	
							//$data['horarios'] = json_decode($horarios);
							//print_r($data['horarios']);

							

							$data['opcion_menu'] = 'horarios';
							$data['contenido'] = 'panel/alumnos/index';
							$this->load->view('include/template_tutores',$data);
						}
			else if($alumno == true)
			{
				
							$divisiones = $this->get_divisiones_x_colegio($urlApi, $passApiKey, $data['idusuario'], $data['idcolegio'], $data['nombregrupo']);	
							$data['divisiones'] = json_decode($divisiones);

							$horarios = $this->get_horarios($urlApi, $passApiKey, $data['idusuario'], $data['idcolegio']);	
							//$data['horarios'] = json_decode($horarios);
							//print_r($data['horarios']);

							
							$data['opcion_menu'] = 'horarios';
							$data['contenido'] = 'panel/alumnos/index';
							$this->load->view('include/template_tutores',$data);
				
			}
		    
			else
			{
				//echo "hola tutores";
				$divisiones = $this->get_divisiones_x_colegio($urlApi, $passApiKey, $data['idusuario'], $data['idcolegio'], $data['nombregrupo']);	
				$data['divisiones'] = json_decode($divisiones);
                



				//$horarios = $this->get_horarios($urlApi, $passApiKey, $data['idusuario'], $data['idcolegio']);	
				//$data['horarios'] = json_decode($horarios);

				
				$data['opcion_menu'] = 'horarios';
			    $data['contenido'] = 'panel/alumnos/index';
			    //$data['contenido'] = 'panel/vista_horarios';
				$this->load->view('include/template_colegio',$data);
			}	
		}
		else //redirect(404); 
			redirect("login");

	    //$data['opcion_menu'] = 'usuarios';
		//$data['contenido'] = 'panel/alumnos/index';
		//$this->load->view('include/template_colegio',$data);
		//$this->load->view('panel/alumnos/index.php',$data);
	}	
		
    /**
    * Obtiene las divisiones cargadas de la api y verifica si es institucion o no. 
    * Peticion controller web a controller api. uso de curl 
    * @params $idUser que es el id del usuario
	* return las divisioes- nombre anio - especialidad - nombre nivel.
	* 
    */
    public function get_divisiones_x_colegio($urlApi, $passApiKey, $idUser, $idcolegio, $nombregurpo)  
    {
    	$url = $urlApi."/division/obtener_divisiones_x_colegio/idcolegio/".$idcolegio."/nombregrupo/".$nombregurpo;
		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser") ,
			CURLOPT_SSL_VERIFYPEER => false,
           
		);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}

    //----------------------------------------------------------------



	public function obtener_especialidad(){
		
        $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas 
        $passApiKey = $this->variables->get_apikey();       
        $idusuario = $this->session->userdata('idusuario');
        $url = $urlApi."/Alumno/obtener_especialidad";
        $ch = curl_init();
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => " " ,
            CURLOPT_SSL_VERIFYPEER=> false,
            );
        curl_setopt_array( $ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);
        //var_dump($response);
        //$response = json_encode($response);
        //return $response;      
        print_r($response);
		
		
	}
	public function obtener_division(){
		$especialidad=$this->input->post('id_esp');
        $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas 
        $passApiKey = $this->variables->get_apikey();       
        $idusuario = $this->session->userdata('idusuario');
        $url = $urlApi."/Alumno/obtener_division";
        $arre = array('esp_id' =>$especialidad);
        $ch = curl_init();
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $arre ,
            CURLOPT_SSL_VERIFYPEER=> false,
            );
        curl_setopt_array( $ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);
        //var_dump($response);
        //$response = json_encode($response);
        //return $response;      
        print_r($response);
	}
    public function obtener_eficiencia(){
        $division=$this->input->post('id_div');
        $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas 
        $passApiKey = $this->variables->get_apikey();       
        $idusuario = $this->session->userdata('idusuario');
        $url = $urlApi."/Alumno/obtener_eficiencia";
        $arre = array('div_id' =>$division);
        $ch = curl_init();
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $arre ,
            CURLOPT_SSL_VERIFYPEER=> false,
            );
        curl_setopt_array( $ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);
        //var_dump($response);
        //$response = json_encode($response);
        //return $response;      
        print_r($response);
    }

	public function obtener_datos_user(){
		$docu=$this->input->post('doc');
        $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas 
        $passApiKey = $this->variables->get_apikey();       
        $idusuario = $this->session->userdata('idusuario');
        $url = $urlApi."/Alumno/obtener_datos_user";
        $arre = array('documento' =>$docu);
        $ch = curl_init();
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $arre ,
            CURLOPT_SSL_VERIFYPEER=> false,
            );
        curl_setopt_array( $ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);
        //var_dump($response);
        //$response = json_encode($response);
        //return $response;      
        print_r($response);
		
		
	}
	public function guardar_inscripcion(){
		
        $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas 
        $passApiKey = $this->variables->get_apikey();       
        $idusuario = $this->session->userdata('idusuario');
        $url = $urlApi."/Alumno/guardar_inscripcion";
        $arre = array('datos' =>$this->input->post('userdiv'));
              
        $ch = curl_init();
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $arre,
            CURLOPT_SSL_VERIFYPEER=> false,
            );
        curl_setopt_array( $ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);
        //var_dump($response);
        //$response = json_encode($response);
        //return $response;      
        print_r($response);
		
		
	}

	public function listar_inscriptos(){
		
        $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas 
        $passApiKey = $this->variables->get_apikey();       
        $idusuario = $this->session->userdata('idusuario');
        $url = $urlApi."/Alumno/listar_inscriptos";
        $arre = array('idclub' => $this->input->post('club'));
        
        $ch = curl_init();
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $arre,
            CURLOPT_SSL_VERIFYPEER=> false,
            );
        curl_setopt_array( $ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);
        //var_dump($response);
        //$response = json_encode($response);
        //return $response;      
        print_r($response);
		
		
	}
	public function buscar_user(){
		$user=$this->input->post('user');
        $division=$this->input->post('div');
        $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas 
        $passApiKey = $this->variables->get_apikey();       
        $idusuario = $this->session->userdata('idusuario');
        $url = $urlApi."/Alumno/buscar_user";
        $arre = array('id' => $user,'div'=>$division);
        $ch = curl_init();
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $arre ,
            CURLOPT_SSL_VERIFYPEER=> false,
            );
        curl_setopt_array( $ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);
        //var_dump($response);
        //$response = json_encode($response);
        //return $response;      
        print_r($response);
		
		
	}
    public function buscar_participante(){
        $particip=$this->input->post('particip');
        $id_club=$this->input->post('club');
       
        $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas 
        $passApiKey = $this->variables->get_apikey();       
        $idusuario = $this->session->userdata('idusuario');
        $url = $urlApi."/Alumno/buscar_participante";
        $arre = array('participante' => $particip,
                        'idclub'=> $id_club);
        $ch = curl_init();
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $arre ,
            CURLOPT_SSL_VERIFYPEER=> false,
            );
        curl_setopt_array( $ch, $options);
        $datodeport = curl_exec($ch);
        curl_close($ch);
        //var_dump($response);
        //$datodeport = json_encode($datodeport);
        //return $response;              

        print_r($datodeport);
        
    }
    public function obtener_categoria()
    {
        $edad=$this->input->post('edad');
        $division=$this->input->post('div');
        
       
        $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas 
        $passApiKey = $this->variables->get_apikey();       
        $idusuario = $this->session->userdata('idusuario');
        $url = $urlApi."/Alumno/obtener_categoria";
        $arre = array('edad' => $edad,
                        'division'=> $division);
        $ch = curl_init();
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL            => $url,
            CURLOPT_HTTPHEADER     => array("APIKEY: $passApiKey","userid: $idusuario"),
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $arre ,
            CURLOPT_SSL_VERIFYPEER => false,
            );
        curl_setopt_array( $ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);
        //var_dump($response);
        //$response = json_encode($response);
        print_r($response);
        
       
    }

     public function update_inscripcion()
    {
        $idinscrip=$this->input->post('iscrip');
        $div=$this->input->post('division');
       
       
        $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas 
        $passApiKey = $this->variables->get_apikey();       
        $idusuario = $this->session->userdata('idusuario');
        $url = $urlApi."/Alumno/update_inscripcion";
        $arre = array('insc' => $idinscrip, 'div' =>$div );
        $ch = curl_init();
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL            => $url,
            CURLOPT_HTTPHEADER     => array("APIKEY: $passApiKey","userid: $idusuario"),
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $arre ,
            CURLOPT_SSL_VERIFYPEER => false,
            );
        curl_setopt_array( $ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);
        //var_dump($response);
        //$response = json_encode($response);
        //print_r($response);
        
       
    }
    

    
   
		
		
	
	 
} // fin Controller Alumno.