<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');


    /**
     * Este modelo sirve para consultas especificas
     * que no encierran en algun controlador o entidad especificos
     */
    class Modelo_varios extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }

        /**
         * [existe_userid Convierte y busca en la base de datos un userid]
         * @param  [type] $userid [userid encriptado en md5 que se recibe en HEADER]
         * @return [type]         [-1 si no existe, id si existe]
         */
        public function existe_userid($userid)
        {            
            $iduser = $this->utilidades->desencriptar($userid);
            $sql = "
                SELECT *
                FROM users
                WHERE (users.id = $iduser) LIMIT 1
            ";
            $query = $this->db->query($sql);
            if ($this->db->affected_rows() > 0)
            {
                return $query->row()->id;
            }
            else 
            {
                return -1;
            }
        }

        /**
         * [existe_pin Verifica que exista pin y que corresponda al usuario]
         * @param  [type] $userid [userid encriptado]
         * @param  [type] $pin    [pin de 4 digitos]
         * @return [type]         [0 si no existe, 1 si existe]
         */
        public function existe_pin($userid, $pin)
        {
            $iduser = $this->utilidades->desencriptar($userid);
            $sql = "
                SELECT *
                FROM users
                WHERE (users.id = '$iduser') 
                AND (users.pin = '$pin') LIMIT 1
            ";
            $query = $this->db->query($sql);
            if ($this->db->affected_rows() > 0)
            {
                return 1;
            }
            else 
            {
                return 0;
            }
        }        

	}