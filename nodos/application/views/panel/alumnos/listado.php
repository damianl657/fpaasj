
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

</head>
<body style="padding: 0 40px;">
	<?	
	
    $fec=explode('-', $fecha);
	
	switch ($fec[1]) {
		case '01':$mes="<strong>Enero</strong>";break;
		case '02':$mes="<strong>Febrero</strong>";break;
		case '03':$mes="<strong>Marzo</strong>";break;
		case '04':$mes="<strong>Abril</strong>";break;
		case '05':$mes="<strong>Mayo</strong>";break;
		case '06':$mes="<strong>Junio</strong>";break;
		case '07':$mes="<strong>Julio</strong>";break;
		case '08':$mes="<strong>Agosto</strong>";break;
		case '09':$mes="<strong>Septiembre</strong>";break;
		case '10':$mes="<strong>Octubre</strong>";break;
		case '11':$mes="<strong>Noviembre</strong>";break;
		case '12':$mes="<strong>Diciembre</strong>";break;
		
		default:
			# code...
			break;
	}?>
<p><img src="https://nodosapi.com.ar/api_comunicacion/img/fotos_colegios/<?=$alumno[0]->logo?>" width="80"  height="100"/></p>
<p>Ministerio de Educaci&oacute;n</p>
<p>Secretaria de Educaci&oacute;n Privada.</p><p><strong><?=$alumno[0]->nombre_colegio?></strong></p><p>Educaci&oacute;n Primaria</p><p>Urquiza 426 Sur Capital. Telefono 4224222</p> 

<p style="text-align: right;">San Juan, <strong><?=$fec[0]?></strong> de <?=$mes?> de <strong><?=$fec[2]?></strong>.</p><br>
<p align=justify style="line-height: 3em;"><strong><?=$alumno[0]->nombre_colegio;?></strong>, certifica a alumno/a <?="<strong>".$alumno[0]->last_name." ".$alumno[0]->first_name."</strong>";?> DNI <?="<strong>".$alumno[0]->documento."</strong>";?> ha iniciado <?="<strong>".$alumno[0]->nombre_anio."</strong>";?><?= ($alumno[0]->colegio_id==25) ? " " : " Grado " ;?><?="<strong>".$alumno[0]->nombre_division."</strong>";?> , del ciclo lectivo 2020, de Educaci&oacute;n primaria, en este Establecimiento Educativo.</p>

<p align=justify style="line-height: 2em;">Se extiende el presente certificado para ser presentado ante las autoridades &nbsp;
que as&iacute; lo requieran:</p>
<br><br><br>
<p><img src="<?=base_url('assets\images\firma_'.$alumno[0]->colegio_id.'.png');?>" height="200"/>
	