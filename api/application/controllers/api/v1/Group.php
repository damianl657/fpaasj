<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';


    class Group extends REST_Controller {

    function __construct()
    {
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        
        parent::__construct();
        $this->load->model("modelo_groups");
       
    }


    public function add_group_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $datos = json_decode($this->post('data'));

            //print_r($datos); die();
            
            if( (isset($datos->id)) && ($datos->id>0) ){
                $data = array(
                    'name'=>$datos->nombre,
                    'aliasM'=>$datos->alias1,
                    'aliasF'=>$datos->alias2,
                    'description'=>$datos->desc,
                    'orden'=>$datos->orden,
                    //'estado'=>1,
                    //'id_colegio'=>$datos->colegioId,
                );
                $group = $this->modelo_groups->update_group($datos->id, $data);
            }
            else{
                $data = array(
                    'name'=>$datos->nombre,
                    'aliasM'=>$datos->alias1,
                    'aliasF'=>$datos->alias2,
                    'description'=>$datos->desc,
                    'orden'=>$datos->orden,
                    'estado'=>1,
                    'id_colegio'=>$datos->colegioId,
                );
                $group = $this->modelo_groups->insert_group($data);
                
                if($group){
                    $data2 = array(
                        'colegio_id'=>$datos->colegioId,
                        'group_id'=>$group,
                    );
                    $this->load->model("modelo_permisos");
                    $group = $this->modelo_permisos->insert_regla($data2);
                }

            }

           
            if ($group)
            {
                $arre = array(
                    'status' => 1
                );
                $data = json_encode($arre);
                $this->response($data, REST_Controller::HTTP_OK);
            }
            else
            {
                $arre = array(
                    'status' => 0,
                    'message' => 'error'
                );
                $data = json_encode($arre);
                $this->response($data, REST_Controller::HTTP_OK);
            }
        }
    }


    public function delete_group_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $datos = json_decode($this->post('data'));

            $group_id = $datos->rolId;
            $colegio_id = $datos->colegioId;

            
            $data0 = array(
                    'group_id'=>$group_id,
                    'estado'=>1,
                );
            $usersgroups = $this->modelo_groups->get_usersgroups($data0);
            //print_r($usersgroups); die();
            if(!$usersgroups){

                $data = array(
                    'estado'=>0,
                );
                $group = $this->modelo_groups->update_group($group_id, $data);
                
                if($group){
                    $data2 = array(
                        'colegio_id'=>$colegio_id,
                        'group_id'=>$group_id,
                    );
                    $this->load->model("modelo_permisos");
                    $resDel = $this->modelo_permisos->delete_regla($data2);


                    $arre = array(
                        'status' => 1
                    );
                    $data = json_encode($arre);
                    $this->response($data, REST_Controller::HTTP_OK);
                }
                else
                {
                    $arre = array(
                        'status' => 0,
                        'message' => 'error'
                    );
                    $data = json_encode($arre);
                    $this->response($data, REST_Controller::HTTP_OK);
                }

            }
            else{
                $arre = array(
                    'status' => 0,
                    'message' => 'has_users'
                );
                $data = json_encode($arre);
                $this->response($data, REST_Controller::HTTP_OK);
            }
 
        }
    }
    public function get_id_rol_docente_post(){
        $userid = $this->utilidades->verifica_userid();
        //$nombregrupo = $this->get('nombregrupo'); 
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $id_colegio = $this->post('idcolegio'); 
           
            
            
            $result = $this->modelo_groups->get_id_rol('Docente',$id_colegio);
                

            
            
            // Si existe mas de un resultado, lo mando
            if ($result->num_rows() > 0)
            {
                $array_result = array('id_rol' => $result->row()->id,  'sql' => $this->db->last_query() );
                $array_result = json_encode($array_result);
                $this->response($array_result, REST_Controller::HTTP_OK);
            }
            else
            {
                $array_result = array('id_rol' => '0',  'sql' => $this->db->last_query() );
                $array_result = json_encode($array_result);
                $this->response($array_result, REST_Controller::HTTP_OK);
            }
        }
        
    }


}
