<? include("application/views/template/cabecera.php"); ?>
<style>
	
</style>
<!-- page content -->
<div class="right_col" role="main">
<form id="cursos" autocomplete="on" method="post" class="form-horizontal form-label-left">
  
  <div class="form-group">
    <label class="col-sm-3 control-label">Seleccione Divisiones</label>

    <div class="col-sm-9">
                          
      <div class="input-group">
        <select class="select2_multiple form-control"  name="id_anio">
        <?php
            foreach($lista->result() as $row)
            {
                $modalidad=$this->db->query("SELECT descripcion FROM modalidades where codigo=".$row->modalidad);
					if($modalidad->num_rows() > 0){
						foreach($modalidad->result() as $row_modalidad){
							$descripcion_modalidad=$row_modalidad->descripcion;
						}
					}
					else{
						$descripcion_modalidad="";
					}
					echo $row->nivel." ".$descripcion_modalidad;
                    $id_anio=$row->codigo;
                    
                ?>
                                
                <option value="<?php echo $row->codigo; ?>"><?php echo $row->descripcion;?></option>
                <?php
            }
        ?>
                            
        </select>
        <span class="input-group-btn">
        <button id="calcular" type="button" class="btn btn-primary">Calcular</button>
        </span>
      </div>
    </div>
  </div>
</form>
<!-- top tiles -->
<div class="row tile_count">
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Total de Alumnos</span>
    <div class="count" id="div_contador_de_alumnos"><p id="contador_de_alumnos"> </p></div>
    <span class="count_bottom"><i class="green"><p id="contador_de_alumnos_anio">  </p></i> </span>
   
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Total de Varones</span>
    <div class="count" id="div_contador_de_varones"><p id="contador_de_varones"> </p></div>
    <span class="count_bottom"><i class="green"><p id="contador_de_varones_anio">  </p></i> </span>
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Total de Mujeres</span>
    <div class="count" id="div_contador_de_mujeres"><p id="contador_de_mujeres"> </p></div>
    <span class="count_bottom"><i class="green"><p id="contador_de_mujeres_anio">  </p></i> </span>
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Reprobados Escuela</span>
    <div class="count" id="div_porcentaje_total_escuela"><p id="porcentaje_total_escuela"> </p></div>
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Reprobados del B&aacute;sico</span>
    <div class="count" id="div_porcentaje_total_basico"><p id="porcentaje_total_basico"> </p></div>
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Reprobados del Orientado</span>
    <div class="count" id="div_porcentaje_total_orientado"><p id="porcentaje_total_orientado"> </p></div>
  </div>
  <!--<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Total Females</span>
    <div class="count">4,567</div>
    <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last Week</span>
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Total Collections</span>
    <div class="count">2,315</div>
    <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Total Connections</span>
    <div class="count">7,325</div>
    <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
  </div>-->
</div>
<!-- /top tiles -->


<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="dashboard_graph">

      <div class="row x_title">
        <div class="col-md-6">
          <h3>Resumen <small>por division</small></h3>
        </div>
        <div class="col-md-6">
          
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xs-12">
        <div id="placeholder33" style=" display: none" class="demo-placeholder"></div>
        <div >
          <div id="grafico_promedios_trimestres" class="container_graficos" style=" height:800px;"></div>
        </div>
      </div>
      

      <div class="clearfix"></div>
    </div>
  </div>

</div>

<br />
<div class="row">


  

  


  
</div>


<div class="row">
  


  <div class="col-md-8 col-sm-8 col-xs-12">



    
  </div>
</div>
</div>
<!-- /page content -->

<? include("application/views/template/pie.php"); ?>
<script >
  $(document).ready(function(){
    $('#calcular').click(function(event){
        //alert('a'); 
        document.getElementById("contador_de_alumnos").innerHTML='';
        document.getElementById("contador_de_mujeres").innerHTML='';
        document.getElementById("contador_de_varones").innerHTML='';
        document.getElementById("porcentaje_total_escuela").innerHTML='';
        document.getElementById("porcentaje_total_basico").innerHTML='';
        document.getElementById("porcentaje_total_orientado").innerHTML='';
        $('#grafico_promedios_trimestres').empty();
     
        
        //ajax promedios 
       // console.log(array_colores);
        $.ajax
          ({
              url: '<?php echo site_url("gestion/gestion/traer_datos_desaprobados_x_anio_select"); ?>',
                  
                  type: 'POST',
                  dataType: "text",
                  
                  data: $('#cursos').serialize(),
                  beforeSend: function() {
           
                     $('#grafico_promedios_trimestres').append("<img  id='loading_scroll1' src='<?=base_url('vendors/loading.gif');?>' height='50' width='50' />");
                      //$('#loading_scroll').show();
                     
                  },
                  success: function(data)
                  {
                    
                    //contador_de_alumnos
                    //console.log(data);

                 //   $("#contador_de_alumnos").val(data['contador_alumnos']);
                   // document.getElementById("contador_de_alumnos").innerHTML=data['contador_alumnos'];
                   $('#grafico_promedios_trimestres').append(data);
                  // reload_datatable();

                    $('#loading_scroll1').remove();
                  },
                  error: function (jqXHR, textStatus, errorThrown,data)
                  {
                     //alert('error');
                      console.log(jqXHR);
                      console.log(textStatus);
                      console.log(errorThrown);
                  } 
          });
          function reload_datatable() 
          {
              if ($("#datatable-buttons").length) 
              {
                $("#datatable-buttons").DataTable({
                  responsive: true

                });
            }
          }
      
                  
    });
  })
               
</script>
<!-- Datatables -->
<script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
          keys: true
        });

        $('#datatable-responsive').DataTable();

        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });

        var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });

        TableManageButtons.init();
      });
</script>
    <!-- /Datatables -->
<script>
  let total_alumnos_divisiones;
  let contador_de_varones = 0;
  let contador_de_mujeres =0;
  total_alumnos_divisiones = 0;
  function traer_reprobados_x_materia_x_division(posicion,division,materia, asincrono)
  {
    //console.log(posicion);
    $.ajax
    ({
      url: '<?php echo site_url("gestion/gestion/traer_reprobados_x_materia_x_division"); ?>',
                  
      type: 'POST',
      dataType: "JSON",
      async: asincrono,
      data: {division: division, materia:materia},
      beforeSend: function() {
      $('#loading_'+posicion).append("<img  id='loading_scroll' src='<?=base_url('vendors/loading.gif');?>' height='20' width='20' />");
                      //$('#loading_scroll').show();
      },
      success: function(data)
      {
        document.getElementById("reprobados_"+posicion).innerHTML=data['reprobados'];
        document.getElementById("cant_alumnos_"+posicion).innerHTML=data['cant_alumnos'];
        $('#loading_'+posicion).remove();
        
      },
      error: function (jqXHR, textStatus, errorThrown,data)
      {
                     //alert('error');
                      console.log(jqXHR);
                      console.log(textStatus);
                      console.log(errorThrown);
      } 
    });
  }
  function traer_porcentaje_reprobados_x_materia_x_division(posicion,division,materia,asincrono)
  {

 
  //console.log(posicion);
    $.ajax
    ({
      url: '<?php echo site_url("gestion/gestion/traer_porcentaje_reprobados_x_materia_x_division"); ?>',
                  
      type: 'POST',
      dataType: "JSON",
      async: asincrono,
      data: {division: division, materia:materia},
      beforeSend: function() {
      $('#por_loading_'+posicion).append("<img  id='loading_scroll' src='<?=base_url('vendors/loading.gif');?>' height='20' width='20' />");
                      //$('#loading_scroll').show();
      },
      success: function(data)
      {
        //porcentaje_total_reprobados = porcentaje_total_reprobados + data['reprobados'];
        document.getElementById("por_reprobados_"+posicion).innerHTML=data['reprobados']+'%';
        if(data['reprobados']>= 25){
          $('#por_reprobados_'+posicion).css("background", "#FF0000");
          $('#por_reprobados_'+posicion).css("font-weight", "bold");
          $('#por_reprobados_'+posicion).css("color", "#FFFFFF");
        }
        else if (data['reprobados'] <= 25 && data['reprobados'] >= 15) {
          $('#por_reprobados_'+posicion).css("background", "#FFFF00");
          $('#por_reprobados_'+posicion).css("font-weight", "bold");
          //$('#por_reprobados_'+posicion).css("color", "#FFFFFF");
        }
        
      //  document.getElementById("por_cant_alumnos_"+posicion).innerHTML=data['cant_alumnos'];
        $('#por_loading_'+posicion).remove();
        
      },
      error: function (jqXHR, textStatus, errorThrown,data)
      {
                     //alert('error');
                      console.log(jqXHR);
                      console.log(textStatus);
                      console.log(errorThrown);
      } 
    });
  }
  function completar_total_reprobados_div(posicion)
  {
    //document.getElementById("total_rep_"+posicion).innerHTML=porcentaje_total_reprobados+'%';
  }
  function traer_porcentaje_reprobados_x_division(posicion,division)
  {
   // alert(division);
    console.log(division);
    $.ajax
    ({
      url: '<?php echo site_url("gestion/gestion/traer_porcentaje_reprobados_x_division"); ?>',
                  
      type: 'POST',
      dataType: "JSON",
       //async: false,
      data: {division: division},
      beforeSend: function() {
      $('#por_loading_materia_'+posicion).append("<img  id='loading_scroll' src='<?=base_url('vendors/loading.gif');?>' height='20' width='20' />");
                      //$('#loading_scroll').show();
      },
      success: function(data)
      {
        
        console.log(data);
        document.getElementById("total_rep_"+posicion).innerHTML=data['reprobados']+'%';
        if(data['reprobados']>= 25){
          $('#total_rep_'+posicion).css("background", "#FF0000");
          $('#total_rep_'+posicion).css("font-weight", "bold");
          $('#total_rep_'+posicion).css("color", "#FFFFFF");
        }
        else if (data['reprobados'] <= 25 && data['reprobados'] >= 15) {
          $('#total_rep_'+posicion).css("background", "#FFFF00");
          $('#total_rep_'+posicion).css("font-weight", "bold");
         
        }
        $('#por_loading_materia_'+posicion).remove();
        total_alumnos_divisiones = total_alumnos_divisiones + data['contador_alumnos_division'];
        contador_de_varones = contador_de_varones + data['contador_varones_division'];
        contador_de_mujeres = contador_de_mujeres + data['contador_mujeres_division'];
        document.getElementById("contador_de_alumnos").innerHTML=total_alumnos_divisiones;
        document.getElementById("contador_de_mujeres").innerHTML=contador_de_mujeres;
        document.getElementById("contador_de_alumnos_anio").innerHTML=data['anio_nombre'];
        document.getElementById("contador_de_varones_anio").innerHTML=data['anio_nombre'];
        document.getElementById("contador_de_mujeres_anio").innerHTML=data['anio_nombre'];

        document.getElementById("contador_de_varones").innerHTML=contador_de_varones;

        $('#loading_scroll_contador_de_alumnos').remove();
        $('#loading_scroll_contador_de_varones').remove();
        $('#loading_scroll_contador_de_mujeres').remove();
        console.log(total_alumnos_divisiones);
        
      },
      error: function (jqXHR, textStatus, errorThrown,data)
      {
                     //alert('error');
                      console.log(jqXHR);
                      console.log(textStatus);
                      console.log(errorThrown);
      } 
    });
  }
  function traer_porcentaje_total_reprobados_escuela()
  {
    let division = 1
    $.ajax
    ({
      url: '<?php echo site_url("gestion/gestion/traer_porcentaje_total_reprobados_escuela"); ?>',
                  
      type: 'POST',
      dataType: "JSON",
      data: {division: division},
      beforeSend: function() {
      //$('#por_loading_materia_'+posicion).append("<img  id='loading_scroll' src='<?=base_url('vendors/loading.gif');?>' height='20' width='20' />");
                      //$('#loading_scroll').show();
      },
      success: function(data)
      {
       
        console.log(data);
       
        document.getElementById("porcentaje_total_escuela").innerHTML=data['reprobados_escuela']+'%';
        document.getElementById("porcentaje_total_basico").innerHTML=data['reprobados_basico']+'%';
        document.getElementById("porcentaje_total_orientado").innerHTML=data['reprobados_orientado']+'%';

        $('#loading_scroll_porcentaje_total_escuela').remove();
        $('#loading_scroll_porcentaje_total_basico').remove();
        $('#loading_scroll_porcentaje_total_orientado').remove();
       
        
      },
      error: function (jqXHR, textStatus, errorThrown,data)
      {
                     //alert('error');
                      console.log(jqXHR);
                      console.log(textStatus);
                      console.log(errorThrown);
      } 
    });
  }
</script>

    


		
