﻿
<script src="<?=base_url('assets/js/bootstrap.min.js');?>"></script>

<script type="text/javascript">

var idUser =  '<?php echo $idusuario; ?>';

$(document).ready(function(){

      $('.btn_materias').attr('disabled', true);

      $('.select2').select2({
           theme: "bootstrap",
           placeholder: "Seleccione un A&ntilde;0",
          allowClear: true
      });


      $('.selectareas').change(function(){

          var value= $(this).val();

          var arre = Array();
          arre = value.split('_');
          var idarea = arre[0];
          var idcole = arre[1];

          if(idarea != -1){

              get_materias_x_area(idcole, idarea);

              $('#btn_materias_'+idcole).attr('disabled', false);
          }
          else
              $('#btn_materias_'+idcole).attr('disabled', true);

      });

  });



  function get_materias_x_area(idcole, idarea){

        var rol = $('#formdir_'+idcole).find('[name="rol"]').val();
        var idrol = $('#formdir_'+idcole).find('[name="idrol"]').val();


        $("#tabladatos_"+idcole).dataTable().fnDestroy();
        $("#tabladatos_body_"+idcole).empty();

        var url = '<?=base_url("/index.php/materias_area/get_materias_x_area_ajax")?>';

        var arre = Array(idarea);
        var idarea2 = JSON.stringify(arre);
        //console.log(idarea);
        //console.log(idarea2);

        $.ajax({
          url: url,
          type: 'POST',

          data:{idcole:idcole, idrol:idrol, rol:rol, idarea:idarea2},
          beforeSend: function() {
              $('#selectareas_'+idcole).after("<img id='loadingMat' src='<?=base_url('assets/images/loading.gif');?>' height='25' width='25' />");
          },
          success: function(data)
          {
            //console.log(data);
            var data = jQuery.parseJSON( data );
            //console.log(data);

            if( data['status']===0){
              var string = "<p class='text-danger'>No hay materias asignadas</p>";
              //console.log(string);
              $("#tabladatos_body_"+idcole).html(string);

              tableMatAsig=null;
            }
            else
            {
              //var data = jQuery.parseJSON( data );
              /*[{"id":"4","aniomateria_id":"952","idmateria":"60","nombremat":"CIENCIAS BIOL\u00d3GICAS","nodoanioid":"139","nombreanio":"2\u00ba","areaname":"Ciencias Naturales"},*/

              var cont = 1 ;

              data.result.forEach(function(currentValue,index){
                //console.log(currentValue);
                var id = currentValue.id;
                var aniomateria_id = currentValue.aniomateria_id;
                var idmateria = currentValue.idmateria;

                var nombremat = currentValue.nombremat;
                var nombreanio = currentValue.nombreanio;
                var nombrenivel = currentValue.nombrenivel;
                if(currentValue.nombreespecialidad)
                  var nombreespecialidad = currentValue.nombreespecialidad;
                else var nombreespecialidad = ""



                var fila = '<tr id="materia_'+id+'_'+aniomateria_id+'_'+idmateria+'">'+
                              '<td>'+cont+'</td>'+
                              '<td>'+nombremat+'</td>'+
                              '<td>'+nombreanio+'</td>'+

                              '<td>'+nombrenivel+'</td>'+
                              '<td>'+nombreespecialidad+'</td>'+

                              '<td><div align="center"><button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="" title="borrar materia" onclick="delete_areamateria(this,'+id+','+idcole+','+idarea+')" type="button"><i class="glyphicon glyphicon-trash"></i></button></div></td>'+

                          '</tr>';
                cont++;

                $("#tabladatos_body_"+idcole).append(fila);

              });
              generar_datatable(idcole);
            }

            $('#loadingMat').remove();
          },
          error: function(response){
              console.log(response);

              $('#loadingMat').remove();
          }
        });
  }


  var tableMat = Array();
  var tableMatAsig = null;
  function generar_datatable(id_colegio)
  {
            var handleDataTableButtons2 = function() {
              if ($("#tabladatos_"+id_colegio).length) {
                tableMatAsig=$("#tabladatos_"+id_colegio).DataTable({

            //"scrollX": true,
            //"scrollY": true,

            "scroller": {
            "displayBuffer": 2
            },


                  responsive: true,
                  keys: true,
                  deferRender: true,
                  scrollCollapse: true,

                  "language": {
                                "sProcessing":     "Procesando...",
                                "sLengthMenu":     "Mostrar _MENU_ registros",
                                "sZeroRecords":    "No se encontraron resultados",
                                "sEmptyTable":     "NingÃºn dato disponible en esta tabla",
                                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Buscar:",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":     "Ãšltimo",
                                    "sNext":     "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                              }
                });
              }
            };
            TableManageButtons2 = function() {
              "use strict";
              return {
                init: function() {

                  handleDataTableButtons2();
                }
              };
            }();
            TableManageButtons2.init();
            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true,

            });
    }

  function generar_datatable_mat(id_colegio)
  {

    if(!tableMat[id_colegio]) {
        var handleDataTableButtons2 = function() {
        if ($("#materiastabladatos_"+id_colegio).length) {
            tableMat[id_colegio] = $("#materiastabladatos_"+id_colegio).DataTable({

                //"scrollX": true,
                //"scrollY": true,

                "scroller": {
                "displayBuffer": 2
                },


                  responsive: true,
                  keys: true,
                  deferRender: true,
                  scrollCollapse: true,

                  "ordering": true,

                  "language": {
                                "sProcessing":     "Procesando...",
                                "sLengthMenu":     "Mostrar _MENU_ registros",
                                "sZeroRecords":    "No se encontraron resultados",
                                "sEmptyTable":     "NingÃºn dato disponible en esta tabla",
                                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Buscar:",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":     "Ãšltimo",
                                    "sNext":     "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                              }
                });
              }
            };

        TableManageButtons2 = function() {
          "use strict";
          return {
            init: function() {

              handleDataTableButtons2();
            }
          };
        }();

        TableManageButtons2.init();
        /*var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true,
        });     */
    }
  }



  function delete_areamateria(este, idareamat, colegioId, areaId)
  {

    bootbox.confirm("Eliminar Materia?", function(result){
      if(result){

        var urlRequest = '<?=base_url("/index.php/materias_area/delete_aniomat_ajax")?>';

        $.ajax({
              url: urlRequest,
              type: 'POST',
              headers: {

              },
              data: {idareamat: idareamat},
              beforeSend: function() {
                  $(este).after("<img id='loadingdelete' src='<?=base_url('assets/images/loading.gif');?>' height='15' width='15' />");
              },
              success: function(data){
                    var data = jQuery.parseJSON( data );
                    console.log(data);
                    if(data['status']==false)
                    {
                        if(data['message'] == 'error')
                        {
                           $.notify('error: elimine todos los horarios', "warning");
                        }
                    }
                    else{   //alert('OK');
                          $.notify('Materia eliminada', "success");

                          get_materias_x_area(colegioId, ''+areaId+'');
                    }
                    $('#loadingdelete').remove();

              },
              error: function(response){
                  //console.log(response);
              }
          });

      }

    });

  }

  function add_materias(colegioId)
  {

      var aux = $('#selectareas_'+colegioId).val();
      var arre = Array();
      arre=aux.split('_');
      var areaId = arre[0];


      var selected = [];

      if(tableMat[colegioId])
        tableMat[colegioId].rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            //var data = this.data();
            var node = this.node();
            var check = $(node).find('.checkmat');
            if($(check).is(':checked'))
              selected.push($(check).val());
        } );

      var materias = JSON.stringify(selected);

      //console.log(materias);
      //console.log(colegioId);
      //console.log(areaId);

      var urlRequest = '<?=base_url("/index.php/materias_area/add_materias_x_area_ajax")?>';

      $.ajax({
            url: urlRequest,
            type: 'POST',
            headers: {

            },
            data: {materias: materias, colegio: colegioId, area:areaId},
            beforeSend: function() {
                $('#guardar_mat_'+colegioId).after("<img id='loadingAdd' src='<?=base_url('assets/images/loading.gif');?>' height='15' width='15' />");
            },
            success: function(data){
                  //console.log(data);
                  var data = jQuery.parseJSON( data );


                  if(data['status']==false)
                  {
                      if(data['message'] == 'no_materias')
                      {
                        $.notify('No se seleccionaron materias', "warning");
                      }
                   }
                  else{
                        $.notify('Materias Asignadas', "success");
                        get_materias_x_area(colegioId, areaId);
                  }

                  $('#loadingAdd').remove();
                  $('#modalmaterias_'+colegioId).modal('hide');
            },
            error: function(response){
                //console.log(response);
            }
        });

  }

  function abrir_modal_materias(id_colegio)
  {
    generar_datatable_mat(id_colegio);

    //$("#modal_materias_idcolegio").val(id_colegio);


    if(tableMat[id_colegio])
      tableMat[id_colegio].rows().every( function ( rowIdx, tableLoop, rowLoop ) {
          //var data = this.data();
          var node = this.node();
          $(node).find('.checkmat').prop('checked',false);
      } );


    if(tableMatAsig!=null)
      tableMatAsig.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
          //var data = this.data();
          var node = this.node();
          var idtr = $(node).attr('id');
          //console.log(idtr);

          var arre = Array();
          arre=idtr.split('_');
          var idaniomat = arre[2];
          //var idmat = arre[3];

          var row = tableMat[id_colegio].row('#rowmat_'+id_colegio+'_'+idaniomat).node();
          $(row).find('.checkmat').prop('checked',true);

      } );


    $('#modalmaterias_'+id_colegio).modal('show');
  }


</script>







<div class="wrapper wrapper-content animated fadeInRight" id="">
    <div class="row">
          <div class="col-lg-12 " id="usuarios_borrar">
              <div class="ibox collapsed2 float-e-margins">
                  <div class="ibox-title" id="tituloaltaevento">
                      <h5>Materias por Area </h5>
                      <div class="ibox-tools">
                          <a class="">
                              <!--<i class="fa fa-chevron-up"></i>-->
                          </a>
                      </div>
                  </div>
                  <div class="ibox-content">
                      <div class="row row2">
                          <div class="col-sm-12 b-r" id="">
                              <div class="tabs-container">
                                <ul class="nav nav-tabs" id="colegios_dir_prec">

                                  <?
                                  $cont=0;
                                  if($colegios)
                                    foreach ($colegios as $cole)
                                    {
                                      $idcole = $cole['idcole'];
                                      $url = base_url()."Horarios/index#tab_dir-".$idcole;
                                      $name = $cole['namecole'];
                                      if($cont==0)
                                        echo "<li class='active'><a data-toggle='tab' href='$url'>$name</a></li>";
                                      else
                                        echo "<li class=''><a data-toggle='tab' href='$url'>$name</a></li>";

                                      $cont++;
                                    }


                                  ?>
                                </ul>

                                <div class="tab-content " id="contenido_tab_dir_prec">

                                  <?
                                  $cont=0;
                                  if($colegios)
                                    foreach ($colegios as $cole)
                                    {
                                      $idcole = $cole['idcole'];
                                      $id_grupo = $cole['idrol'];
                                      $rol = $cole['namerol'];

                                      if($cont==0)
                                        $clase = 'active';
                                      else
                                        $clase = ''
                                      ?>

                                        <div id="tab_dir-<?=$idcole?>" class="tab-pane <?=$clase?>">
                                          <div class="panel-body table-responsive">
                                              <div class="row">
                                                  <div class="col-sm-6">
                                                    <form id="formdir_<?=$idcole?>" autocomplete="on" method="post" class="form-horizontal form-label-left">
                                                        <input type="hidden" name="rol" value="<?=$rol?>">
                                                        <input type="hidden" name="idrol" value="<?=$id_grupo?>">
                                                        <div class="form-group ">
                                                              <label>Seleccione una Area: </label>
                                                        </div>
                                                        <div class="form-group ">
                                                          <select name="selectareas" id="selectareas_<?=$idcole?>" class="select2 form-control selectareas">
                                                              <option value='-1_<?=$idcole?>'>Seleccione area...</option>
                                                              <?
                                                              if($areas[$idcole])
                                                                foreach ($areas[$idcole] as $row)
                                                                {
                                                                  $areaId = $row->id;

                                                                  $nombre = $row->nombre;

                                                                  $value = $areaId."_".$idcole;
                                                                  echo "<option value='$value'>$nombre</option>" ;
                                                                }


                                                              ?>

                                                          </select>
                                                        </div>
                                                    </form>
                                                  </div>
                                                  <div class="col-sm-6" style="margin-top:35px">
                                                      <!--<button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="btn_alumnos" onclick="get_materias_x_anio(<?=$idcole?>)" type="button"><strong>Listar</strong></button>-->

                                                      <button class="btn btn-sm btn-info pull-right m-t-n-xs btn_materias" id="btn_materias_<?=$idcole?>" onclick="abrir_modal_materias(<?=$idcole?> )" type="button"><i class="glyphicon glyphicon-plus "></i> Asignar Materias<strong></strong></button>

                                                      <div type="hidden" id="loading_horarios_dir_esc_<?=$idcole?>"></div>
                                                  </div>
                                              </div>

                                              <table class="table table-striped table-bordered dt-responsive nowrap" id="tabladatos_<?=$idcole?>">
                                                  <thead>
                                                    <th>#</th>
                                                    <th>Nombre Materia</th>
                                                    <th>A&ntilde;o</th>
                                                    <th>Nivel</th>
                                                    <th>Especialidad</th>
                                                    <th></th>
                                                  </thead>
                                                  <tbody id="tabladatos_body_<?=$idcole?>"></tbody>
                                              </table>
                                          </div>
                                        </div>





                                      <!-- modal agregar materias-->
                                      <div class="modal fade" id="modalmaterias_<?=$idcole?>" role="dialog">
                                        <div class="modal-dialog">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Materias</h4>
                                            </div>

                                            <div class="modal-body">
                                              <form id="asignar_materias" autocomplete="on" method="post" class="form-horizontal form-label-left">

                                                  <div class="panel-body table-responsive">
                                                      <table class="table table-striped table-bordered dt-responsive nowrap" id="materiastabladatos_<?=$idcole?>">
                                                          <thead>
                                                            <th></th>
                                                            <th>Nombre Materia</th>
                                                            <th>A&ntilde;o</th>
                                                            <th>Nivel</th>
                                                            <th>Especialidad</th>
                                                          </thead>
                                                          <tbody id="materias_tabladatos_body_<?=$idcole?>">
                                                            <?
                                                            $cont=0;
                                                            if($materias[$idcole]){
                                                                  foreach ($materias[$idcole] as $mat)
                                                                  {
                                                                    //print_r($mat); die();
                                                                    $idaniomat = $mat->id;
                                                                    $nombremat = $mat->nombre;
                                                                    $idmat = $mat->idmateria;
                                                                    $nombreanio = $mat->nombreanio;
                                                                    $nombrenivel = $mat->nombrenivel;
                                                                    $nombreespecialidad = $mat->nombreespecialidad;


                                                                    ?>

                                                                    <tr id='rowmat_<?echo $idcole."_".$idaniomat?>'>
                                                                        <td>
                                                                          <div align="center" class="custom-control custom-checkbox">
                                                                              <input type="checkbox" class="custom-control-input checkmat" name="checkmat" value="<?=$idaniomat?>" id="idmat_<?=$idaniomat?>">
                                                                          </div>
                                                                        </td>
                                                                        <td name="mat_<?=$idaniomat?>"><?=$nombremat?></td>
                                                                        <td><?=$nombreanio?></td>
                                                                        <td><?=$nombrenivel?></td>
                                                                        <td><?=$nombreespecialidad?></td>
                                                                    </tr>


                                                                    <?
                                                                    $cont++;
                                                                  }
                                                            }
                                                            ?>
                                                          </tbody>
                                                      </table>

                                                      <?
                                                      echo "<script>tableMat[$idcole]=false;</script>";

                                                        /*$clave = in_array('add_materia', $acciones);
                                                        if($clave)  { ?>
                                                            <button class="btn btn-sm btn-info pull-left m-t-n-xs" id="btn_alumnos" onclick="abrir_modal_addmateria()" type="button"><i class="glyphicon glyphicon-plus"></i> Agregar Materia<strong></strong></button>
                                                      <?}*/?>


                                                  </div>

                                              </form>

                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                <button class="btn btn-primary" id ="guardar_mat_<?=$idcole?>" onclick="add_materias(<?=$idcole?>)">Agregar</button>
                                            </div>

                                          </div>
                                        </div>
                                      </div>


                                    <?$cont++;
                                    }?>

                                </div>
                             </div>
                          </div>
                      </div>

                  </div>
              </div>

          </div>

      </div>
</div>

