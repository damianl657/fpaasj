-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-07-2016 a las 21:22:59
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `nodos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reglas_comunicacion`
--

CREATE TABLE IF NOT EXISTS `reglas_comunicacion` (
`id` int(11) NOT NULL,
  `colegio_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `roles_id` varchar(100) NOT NULL,
  `id_menus_acciones` text
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `reglas_comunicacion`
--

INSERT INTO `reglas_comunicacion` (`id`, `colegio_id`, `group_id`, `roles_id`, `id_menus_acciones`) VALUES
(1, 1, 4, '3,6,8,7', '1,2,4'),
(2, 1, 9, '1,7,8,9,7,3,4', '1,2,4'),
(3, 1, 7, '3,4,8,7', '1,2,4'),
(4, 1, 3, '4,5,6,8,7', '1,2,4'),
(5, 1, 8, '3,4,5,6,7,8', '1,2,4'),
(7, 1, 1, '3,4,5,6,8,9,7', '1,2,4'),
(8, 1, 5, '', '1,2,4');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `reglas_comunicacion`
--
ALTER TABLE `reglas_comunicacion`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `reglas_comunicacion`
--
ALTER TABLE `reglas_comunicacion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
