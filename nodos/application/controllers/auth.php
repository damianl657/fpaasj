<?php
class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("usuarios/model_usuarios");     
        //$this->output->enable_profiler(TRUE);
    }

    public function index()
    {
        $data['action'] = base_url('auth/validar');
        $this->load->view('auth/login', $data);
    }

    public function validar()
    {
        $this->load->model('auth/user');

        //$auth = $this->user->validar($_POST['username'], $_POST['password']);
        
        if ( isset($_POST['username']) ) {
            // Existe el usuario

            $respuesta = $this->user->comprueba_datos($_POST['username'], $_POST['password']);

            if (!isset($respuesta['error'])) {
                //$respuesta["usuario"] = $_POST['username'];
                //$condiciones = array("id" => $respuesta['id_usuario']);
                //$datos_usuario = $this->model_usuarios->get_datos_usuario($respuesta['id_usuario'], $condiciones);
                //$respuesta["documento"] = $datos_usuario->row()->documento;
                $this->redireccion_x_rol_usuario($respuesta);
            } else {
                $data['error']  = $respuesta['error'];
                $data['action'] = 'validar';
                $this->load->view('auth/login', $data);
                //redirect('auth');
            }
       
        }
        else
        {
            $data['error']  = 'Ingrese Username';
            $data['action'] = 'validar';
            $this->load->view('auth/login', $data);
        }
    }

    public function cambiar_rol_usuario($role_id)
    {
        //print_r($this->session->userdata());
        //exit();
        $documento=$this->session->userdata("documento");
        $idtutor=$this->session->userdata("id_tutor");
        
        if($documento)
            $condiciones = array("role_id" => $role_id,"documento" => $documento);
        else
            $condiciones = array("role_id" => $role_id,"tutor_id" =>  $idtutor);

        $datos_usuario = $this->model_usuarios->get_datos_usuario($this->session->userdata("id_usuario"), $condiciones);
        $rol = "";
        if(isset($this->session->userdata("roles")[$role_id])){
            $rol = $this->session->userdata("roles")[$role_id];
        }
        else{
            $rol = $this->usuarios->mostar_role_id($role_id);
        }

        $respuesta["rol"]        = $rol;
        $respuesta["status"]     = 1;
        $respuesta['id_usuario'] = $datos_usuario->row()->id;
        //$respuesta['logged_in']  = $this->session->userdata("logged_in");
        $respuesta['usuario']    = $datos_usuario->row()->username;
        $respuesta['id_rol']     = $role_id;
        $respuesta['documento']  = $datos_usuario->row()->documento;
        $respuesta['id_tutor']  = $datos_usuario->row()->tutor_id;

        $this->redireccion_x_rol_usuario($respuesta);
    }

    public function redireccion_x_rol_usuario($respuesta)
    {
        $this->load->model("mensajes/model_mensajes");
        $datos='';
        /** almaceno los datos de session **/
        $this->session->set_userdata(array('logged_in' => true, 'id_usuario' => $respuesta['id_usuario'], 'usuario' => $respuesta['usuario'], 'rol' => $respuesta['rol'], 'estado' => $respuesta['status'], 'documento' => $respuesta['documento'], 'id_rol' => $respuesta['id_rol'], 'id_tutor' => $respuesta['id_tutor']));        

        if ($respuesta['rol'] == "Preceptores" && $respuesta['status'] == 1) 
        {
            // Si es administrador
            $view = 'auth/welcome_preceptor';
        } else if ($respuesta['rol'] == "Admin" && $respuesta['status'] == 1) {
            $view = 'auth/welcome_administrador';
        } else if ($respuesta['rol'] == "Docentes Secundaria" && $respuesta['status'] == 1) {
            $view = 'auth/welcome_docente';
        } else if ($respuesta['rol'] == "Directivos" && $respuesta['status'] == 1) {
            $view = 'auth/welcome_directivo';
        } else if ($respuesta['rol'] == "Padres" && $respuesta['status'] == 1) {
            //$view = 'auth/welcome_tutor';
            $datos_usuario = $this->alumno->get_datos_tutor_p_dni($respuesta["documento"]);
            if(isset($datos_usuario)){
                $respuesta['id_tutor']  = $datos_usuario->codigo; 
            }
            else{
                $respuesta['id_tutor']  = 0; 
            }
            $this->session->set_userdata(array("id_tutor" => $respuesta['id_tutor']));           
            $roles = array(5);
            $mensajes = $this->model_mensajes->get_mensaje_p_rol($roles);
            $mensaje_user = null;
            $datos["mensaje"] = null;
            $ban = false;
            //var_dump($this->db->last_query());
            if(!is_null($mensajes)){
                $row = null;
                foreach ($mensajes->result() as $key => $row) {
                    $mensaje_user = $this->model_mensajes->get_mensaje_user($row->id);
                    //print_r($mensaje_user->row());
                    if(is_null($mensaje_user) && $ban === false){
                        print_r($mensaje_user);
                        $ban = true;
                        $datos["mensaje"] = $row;
                    }
                }
            }
            //var_dump($datos["mensaje"]);
            //exit();
            if($ban && !is_null($mensajes) /* && is_null($aspirante)*/){
                $this->session->set_userdata(array("mensaje_id" => $datos["mensaje"]->id));
                if($datos["mensaje"]->recurso == 'view'){
                    //redirect("mensajes/mensajes/procesar_mensaje");
                    //var_dump($this->db->last_query());
                    $view = $mensajes->row()->destino;                        
                }
                elseif($datos["mensaje"]->recurso == 'controller'){
                    redirect($datos["mensaje"]->destino);
                }
                elseif($datos["mensaje"]->recurso == 'library'){

                }
            }
            else{
                $datos['titulo_pagina'] = 'Notas Formativas por alumno';
                $this->load->model('tutores/model_tutores');
                $id_user             = $respuesta['id_usuario'];
                $where = array("tipo_upload" => 'I');
                $this->db->where($where);
                $datos['files'] = $this->db->get("uploads");
                $datos['list_hijos'] = $this->model_tutores->get_hijos_x_userid($id_user);
                $datos['titulo_pagina'] = 'Comunicados';
                $view = 'docentes/deberes/vista_lista_hijos_comunicados';
            }

        } else if($respuesta['rol'] == "Alumno" && $respuesta['status'] == 1){
            $view = 'auth/welcome_alumno';
        } else if ($respuesta['rol'] == "Directivos Primaria" && $respuesta['status'] == 1) {
            $view = 'auth/welcome_directivo_primaria';
        } else if ($respuesta['rol'] == "Secretarias" && $respuesta['status'] == 1) {
            $view = 'auth/welcome_secretaria';
        } 
        else if ($respuesta['rol'] == "Docentes Primaria" && $respuesta['status'] == 1) {
            $view = 'auth/welcome_docente_primaria';
        } 
        else if ($respuesta['rol'] == "Alumno" && $respuesta['status'] == 1) 
        {
            $view = 'notas/padres/vista_lista_hijos_prome_nota_formativa';
        }
        else if ($respuesta['rol'] == "Secretaria Nivel Inicial" && $respuesta['status'] == 1) 
        {
            $view = 'auth/welcome_secretaria_nivel_inicial';
        }
        else if ($respuesta['rol'] == "Secretaria Secundario" && $respuesta['status'] == 1) 
        {
            $view = 'auth/welcome_secretaria_secundaria';
        }
        else if ($respuesta['rol'] == "Administrativo" && $respuesta['status'] == 1) 
        {
            $view = 'auth/welcome_administrativo';
        }
        else if ($respuesta['rol'] == "Docente Nivel Inicial" && $respuesta['status'] == 1) 
        {
            $view = 'auth/welcome_administrativo';
        }
        else
        {
           redirect('auth');
        }

        /*NOTA: ACA SE DEBEN AGREGAR LOS DEMAS USSER */

        //print_r($this->session->userdata());

        //Voy a buscar todos los roles que tiene el usuario
        $this->load->model("usuarios/model_usuarios");
        $roles_query = $this->model_usuarios->get_roles_usuario();
        $roles       = array();
        foreach ($roles_query->result() as $key => $value) {
            $roles[$value->id] = $value->name;
        }

        $this->session->set_userdata(array("roles" => $roles));
        $this->load->view($view, $datos);
    }

    function logout(){
        $this->session->sess_destroy();
        redirect('auth');
    }

    function change_pass(){//$data['action2'] = 'auth/lolo';
       $data['titulo'] = "Modificar Clave de Acceso";
       $this->load->view('auth/cambio_pass',$data); 
    }  

    function check_change_pass(){
            
        $this->load->model('auth/user');
        $respuesta = $this->user->check_change_pass($_POST);
        
         if( $respuesta['error']== " "){ 
          //  $this->session->sess_destroy(); 
               $data['titulo_pagina'] = "Modificar Clave de Acceso";
               $data['accion_realizada'] = "Modificados";
               $this->load->view('auth/vista_cambia_pass_sucefull',$data);  
    
         }else{ // muestra el error.
            $data['error'] = $respuesta['error'];
            $data['titulo'] = "Modificar datos de Acceso";
            $this->load->view('auth/cambio_pass',$data);    
         }                        
    }
}
