<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_doc_div_mat_alu extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }

        public function obtener_docentes_x_div_x_colegio($id_division,$id_colegio, $id_materia)
        {
          $sql = "SELECT nodo_doc_div_mat_alu.*, users.id as user_id, users.first_name, users.last_name, users.documento FROM nodo_doc_div_mat_alu JOIN users_groups on users_groups.id = nodo_doc_div_mat_alu.id_docente JOIN users on users.id = users_groups.user_id join nodocolegio on nodocolegio.id = nodo_doc_div_mat_alu.id_colegio where nodo_doc_div_mat_alu.id_division = $id_division and nodo_doc_div_mat_alu.id_colegio = $id_colegio and nodocolegio.cicloa = nodo_doc_div_mat_alu.ciclo_lectivo and nodo_doc_div_mat_alu.id_alumno = 0 and nodo_doc_div_mat_alu.id_materia = $id_materia";
          $query = $this->db->query($sql); 
          return $query; 
        }
        public function existe_doc_div_mat_alu($id_colegio,$id_division,$id_materia,$id_docente,$id_alumno, $cicloa)
        {
            $sql = "SELECT nodo_doc_div_mat_alu.* FROM nodo_doc_div_mat_alu  where nodo_doc_div_mat_alu.id_division = $id_division and nodo_doc_div_mat_alu.id_colegio = $id_colegio  and nodo_doc_div_mat_alu.id_alumno = $id_alumno and nodo_doc_div_mat_alu.id_materia = $id_materia and id_docente = $id_docente and ciclo_lectivo = $cicloa";
          $query = $this->db->query($sql);
          if($query->num_rows()>0){
              return 1;
          }
          else{
              return 0;
          }
        }
        public function insertar($data)
        {
            $query = $this->db->insert("nodo_doc_div_mat_alu",$data);
            return $query;
        }
        public function borrar_doc_div_mat_alu($id_colegio,$id_division,$id_materia,$id_docente, $cicloa)//borra las del ciclo seleccionado
        {
            $sql = "DELETE  FROM nodo_doc_div_mat_alu  where id_division = $id_division and id_colegio = $id_colegio   and id_materia = $id_materia and id_docente = $id_docente and ciclo_lectivo = $cicloa";
          $query = $this->db->query($sql);
         
        }
        public function obtener_alumnos_x_doc_div_mat($id_division,$id_colegio,$id_materia,$id_docente)
        {

            $sql = "SELECT users.last_name, users.first_name, users.documento, users.id as user_id, users_groups.id as id_user_group  , nodo_doc_div_mat_alu.id_docente, nodo_doc_div_mat_alu.id_division, nodo_doc_div_mat_alu.id_materia, nodo_doc_div_mat_alu.id_colegio, nodo_doc_div_mat_alu.ciclo_lectivo
                FROM `users`
                join users_groups on users_groups.user_id = users.id
                join nodo_doc_div_mat_alu on nodo_doc_div_mat_alu.id_alumno = users_groups.id where nodo_doc_div_mat_alu.id_division = $id_division and nodo_doc_div_mat_alu.id_materia = $id_materia and nodo_doc_div_mat_alu.id_docente = $id_docente";
            $query = $this->db->query($sql); 
          return $query;
        }
        public function delete_doc_div_mat_alu($id_colegio,$id_division,$id_materia,$id_docente, $id_alumno ,$cicloa)//borra las del ciclo seleccionado
        {
            $sql = "DELETE  FROM nodo_doc_div_mat_alu  where id_division = $id_division and id_colegio = $id_colegio   and id_materia = $id_materia and id_docente = $id_docente and ciclo_lectivo = $cicloa and nodo_doc_div_mat_alu.id_alumno = $id_alumno";
          $query = $this->db->query($sql);
         
        }
        
        
              

	}