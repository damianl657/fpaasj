<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';


    class Materia extends REST_Controller {

    function __construct()
    {
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        
        parent::__construct();
        $this->load->model("modelo_materias");
        $this->load->model("modelo_doc_div_mat_alu");
        $this->load->model("modelo_usuario");
        $this->load->model("modelo_cicloa");
        $this->load->model("modelo_alumno");
        // Configurar limites para cada uno de los metodos, no solo controlador
        // La tabla limits tiene que estar creada y la opcion limits TRUE en application/config/rest.php
        
        //$this->methods['tutores_get']['limit'] = 1; // 500 peticiones por hora por usuario/key
        //$this->methods['user_post']['limit'] = 100; // 100 peticiones por hora por usuario/key
        //$this->methods['user_delete']['limit'] = 50; // 50 peticiones por hora por usuario/key
    }

    public function nombres_materia_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $result = $this->modelo_materias->obtener_materias();
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
                $data = json_encode($result);
                $this->response($data, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron materias'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }

    public function anios_materia_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            //$idcolegio = $_GET['idcolegio'];
            $idcolegio = $this->get('idcolegio'); 
            //$nodoanioid = $_GET['nodoanioid'];
            $nodoanioid = $this->get('nodoanioid'); 

            $result = $this->modelo_materias->obtener_aniosmaterias($idcolegio, $nodoanioid);
           
            if ($result)
            {
                $data = json_encode($result);
                $this->response($data, REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron materias'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }

    public function anios_materia_bycolegio_get() //by colegio
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $idcolegio = $this->get('idcolegio');    

            $result = $this->modelo_materias->obtener_aniosmaterias_bycolegio($idcolegio);
           
            if ($result)
            { 
                $arre = array(
                    'status' => 1,
                    'result' => $result,
                );
            }
            else
            {
                $arre = array(
                    'status' => 0,
                    'message' => 'No se encontraron materias'
                );
            }

            $data = json_encode($arre);
            $this->response($data, REST_Controller::HTTP_OK);
        }
    }

    public function materia_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
                //$data = json_decode($_POST['data']) ;
                $materiaName = $this->post('materiaName');
                if($materiaName){

                    $result0 = $this->modelo_materias->obtener_materia($materiaName);
                    if (!$result0) // para NO insertar filas repetidas
                    {
                        $data = array(
                            'nombre'=>$materiaName,
                        );
                        $result = $this->modelo_materias->insertar_materia($data);          
                        if ($result)
                        {
                            $ultimoId=$this->db->insert_id(); 
                            $this->response([
                                    'status' => TRUE,
                                    'message' => 'alta_ok',
                                    'id' => $ultimoId 
                                ], REST_Controller::HTTP_OK);  
                        }
                        else
                            {
                                $this->response([
                                    'status' => FALSE,
                                    'message' => 'error_alta'
                                ]); // NOT_FOUND (404) being the HTTP response code
                            }
                    }
                    else
                        {
                            $this->response([
                                'status' => FALSE,
                                'message' => 'existe'
                            ]); // NOT_FOUND (404) being the HTTP response code
                        }

                }
                else
                    {
                        $this->response([
                            'status' => FALSE,
                            'message' => 'vacio'
                        ]); // NOT_FOUND (404) being the HTTP response code
                    }

        }
    }


    public function materias_anio_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
                $colegioId = $this->post('colegioId');
                $nodoanioId = $this->post('nodoanioId');

                $materiasId = json_decode($_POST['materiasId']) ;


               
                if (count($materiasId)>0) 
                {   
                    //PASO 1
                    //materias que ya tiene asignadas el anio
                    $resultmaterias = $this->modelo_materias->obtener_aniosmaterias($colegioId, $nodoanioId);
                    $materiasAct = array();
                    //print_r($resultmaterias);
                    if($resultmaterias)
                        foreach ($resultmaterias as $key)
                            $materiasAct[] = $key->idmateria;

                   
                    $planest= $this->utilidades->get_planestudio( date('Y') );  
                    $plan_est =$this->modelo_cicloa->obtener_planestudio_actua_x_colegio($colegioId);       

                    //PASO 2; inserto las materias nuevas
                    for ($i=0; $i < count($materiasId); $i++) { 
                        $mateId = $materiasId[$i];

                        $clave = array_search($mateId, $materiasAct);

                        if(!$clave) {//solo inserta si no encuentra

                            $data = array(
                                'nodoanios_id'=>$nodoanioId,
                                'nombresmateria_id'=>$mateId,
                                'colegio_id'=>$colegioId,
                               // 'cicloa'=>$planest,
                                'cicloa'=>$plan_est,
                            );

                            $data2 = array(
                                'nodoanios_id'=>$nodoanioId,
                                'nombresmateria_id'=>$mateId,
                                'colegio_id'=>$colegioId,
                               // 'cicloa'=>$planest,
                                'cicloa'=>$plan_est,
                                'anio_materia'=> 0,
                            );

                            $result = $this->modelo_materias->insertar_aniosmaterias2($data, $data2); 
                        }
                    }
                            

                    //PASO 3: borrar (en cascada) los registros que ya no corresponden
                    /*$materiasBorrar = array_diff($materiasAct, $materiasId);
                    foreach ($materiasBorrar as $value) {
                        $data = array(
                            'nodoanios_id'=>$nodoanioId,
                            'nombresmateria_id'=>$value,
                            'colegio_id'=>$colegioId,
                            'cicloa'=>$planest,
                        );
                        
                        $aniomat = $this->modelo_materias->get_aniomateria($data); 
                        if($aniomat)
                        {   
                            $data2 = array(
                                'materias_id'=>$aniomat->id,
                            );                          
                            $horariomat = $this->modelo_materias->get_horariosmateria($data2); 
                            if(!$horariomat){ //solo elimina si no tiene horarios cargados
                                $data3 = array(
                                    'id'=>$aniomat->id,
                                );
                                $this->modelo_materias->delete_aniosmaterias($data3);
                            }
                        }
                    }   */
                    
                    $data = array( 
                        'status' => TRUE,
                        'message' => 'Materias asignadas'
                    );

                    $data = json_encode($data);

                    $this->response($data, REST_Controller::HTTP_OK);  
                   
                }
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'no_materias'
                    ]); // NOT_FOUND (404) being the HTTP response code
                }
        }
    }


     public function materias_area_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $colegioId = $this->post('colegioId');
            $areaId = $this->post('areaId');
            $aniosmateriasId = json_decode($_POST['materiasId']) ;

           
            if (count($aniosmateriasId)>0) 
            {   
                //PASO 1
                //materias que ya tiene asignadas el anio
               /* $resultmaterias = $this->modelo_materias->obtener_aniosmaterias($colegioId, $nodoanioId);
                $materiasAct = array();
                //print_r($resultmaterias);
                if($resultmaterias)
                    foreach ($resultmaterias as $key)
                        $materiasAct[] = $key->idmateria;*/

               
                $planestAnio = $this->utilidades->obtener_planestudioaXcolegio( $colegioId );  
                $planestId = $this->utilidades->get_planestudio($planestAnio);
                

                //PASO 2; inserto las materias nuevas
                for ($i=0; $i < count($aniosmateriasId); $i++) { 
                    $aniomateId = $aniosmateriasId[$i];

                    //$clave = array_search($mateId, $materiasAct);
                   // if(!$clave) {//solo inserta si no encuentra

                        $data = array(
                            'area_id'=>$areaId,
                            'aniomateria_id'=>$aniomateId,
                            'colegio_id'=>$colegioId,
                            'planestudio'=>$planestId,
                        );
                    
                        $result = $this->modelo_materias->insertar_areasmaterias($data, $data); 
                    //}
                }
                        
                
                $data = array( 
                    'status' => 1,
                    'message' => 'Materias asignadas'
                );
                $data = json_encode($data);
                $this->response($data, REST_Controller::HTTP_OK);  
               
            }
            else
            {
                $arre = array(
                    'status' => 0,
                    'message' => 'no_materias'
                );
                $data = json_encode($arre);
                $this->response($data, REST_Controller::HTTP_OK);
            }
        }
    }


    public function delete_aniomateria_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $idaniomat = $this->post('idaniomat');

            $data2 = array(
                'materias_id'=>$idaniomat,
            );       
            $horariomat = $this->modelo_materias->get_horariosmateria($data2); 

            $data3 = array(
                'aniomateria_id'=>$idaniomat,
            );
            $areamat = $this->modelo_materias->get_areasmateria($data3); 


            if( (!$horariomat) && (!$areamat) ){ //solo elimina si no tiene horarios cargados, ni areas.
                $data3 = array(
                    'id'=>$idaniomat,
                );
                $this->modelo_materias->delete_aniosmaterias($data3);

                $data = array( 
                    'status' => TRUE,
                    'message' => 'Materia Eliminada'
                );
            }
            else
                if($horariomat)
                {
                    $data = array( 
                        'status' => FALSE,
                        'message' => 'tiene_horarios'
                    );
                    
                }
                else
                    {
                         $data = array( 
                            'status' => FALSE,
                            'message' => 'tiene_areas'
                        );
                    }

            $data = json_encode($data);
            $this->response($data, REST_Controller::HTTP_OK); 

        }
    }


    public function delete_areamateria_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $idareamat = $this->post('idareamat');

            $data3 = array(
                'id'=>$idareamat,
            );
            $result = $this->modelo_materias->delete_areamateria($data3);

            if($result){
                $data = array(
                    'status' => 1,
                    'message' => 'Materia Eliminada'
                );
            }
            else {
                $data = array( 
                    'status' => 0,
                    'message' => 'error'
                );
            }

            $data = json_encode($data);
            $this->response($data, REST_Controller::HTTP_OK); 
        }
    }

    public function asignar_doc_div_mat_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $id_colegio = $_POST["id_colegio"];
            $id_materia = $_POST["id_materia"];
            $id_division = $_POST["id_division"];
            $id_docente = $_POST["id_docente"];
            $cicloa = $this->utilidades->obtener_cicloaXcolegio($id_colegio);
            $existe = $this->modelo_doc_div_mat_alu->existe_doc_div_mat_alu($id_colegio,$id_division,$id_materia,$id_docente,0,$cicloa);
            $datos_docente = $this->modelo_usuario->get_user_x_id_user_group($id_docente);
            if($existe == 1)//existe
            {
                $data = array
                    ( 
                        'status' => 0,
                        'message' => 'Ya Esta Asignado',
                        'existe' => 1
                    );
            }
            else
            {
                $dato = array(
                    'id_colegio' =>  $id_colegio,
                    'id_materia' =>  $id_materia,
                    'id_division' =>  $id_division,
                    'id_docente' =>  $id_docente,
                    'ciclo_lectivo' =>  $cicloa,
                    'id_alumno' =>  0,
                    );
                $this->modelo_doc_div_mat_alu->insertar($dato);
                $existe = $this->modelo_doc_div_mat_alu->existe_doc_div_mat_alu($id_colegio,$id_division,$id_materia,$id_docente,0,$cicloa);
                if($existe == 1)//insertado
                {
                    $docentes_asignados = $this->modelo_doc_div_mat_alu->obtener_docentes_x_div_x_colegio($id_division,$id_colegio,$id_materia);
                    $string_docentes = 'Sin Docentes Asignados';
                    if ($docentes_asignados->num_rows() > 0)
                    {
                        $string_docentes = '';
                        $i = 0;
                        foreach ($docentes_asignados->result() as $key) 
                        {
                            if($i == 0)
                            {
                                $string_docentes = $key->last_name.' '.$key->first_name;
                                $i++;
                            }
                            else
                            {
                                $string_docentes = $key->last_name.' '.$key->first_name.', '.$string_docentes;
                            }
                        }
                    }
                    $data = array
                    ( 
                        'status' => 1,
                        'message' => 'Docente Asignado',
                         'existe' => 0,
                         'string_docentes' => $string_docentes,
                         'last_name' => $datos_docente->last_name,
                        'first_name'=> $datos_docente->first_name,
                        'documento'=> $datos_docente->documento,
                        'user_id' => $datos_docente->id,
                        'id_user_group' => $id_docente,
                        'cant_docentes_materia' => $docentes_asignados->num_rows(),
                    );
                }
                else
                {
                    $docentes_asignados = $this->modelo_doc_div_mat_alu->obtener_docentes_x_div_x_colegio($id_division,$id_colegio,$id_materia);
                    $string_docentes = 'Sin Docentes Asignados';
                    if ($docentes_asignados->num_rows() > 0)
                    {
                        $string_docentes = '';
                        $i = 0;
                        foreach ($docentes_asignados->result() as $key) 
                        {
                            if($i == 0)
                            {
                                $string_docentes = $key->last_name.' '.$key->first_name;
                                $i++;
                            }
                            else
                            {
                                $string_docentes = $key->last_name.' '.$key->first_name.', '.$string_docentes;
                            }
                        }
                    }
                    $data = array
                    ( 
                        'status' => 0,
                        'message' => 'Error',
                         'existe' => 0,
                         'string_docentes' => $string_docentes,
                         'last_name' => $datos_docente->last_name,
                        'first_name'=> $datos_docente->first_name,
                        'documento'=> $datos_docente->documento,
                        'user_id' => $datos_docente->id,
                        'id_user_group' => $id_docente,
                        'cant_docentes_materia' => $docentes_asignados->num_rows(),
                    );
                }
            }
            $data = json_encode($data);
            $this->response($data, REST_Controller::HTTP_OK); 

        }
    }
    public function borrar_doc_div_mat_post()//ingluye borrar alumnos asignados por si los hay ya q la eliminacion va ser directa, y se va ser delete en la tabla para no generar inconsitencia o bug al enviar eventos a destinos eliminados logicamente
    {
        
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $id_colegio = $_POST["id_colegio"];
            $id_materia = $_POST["id_materia"];
            $id_division = $_POST["id_division"];
            $id_docente = $_POST["id_docente"];
            $cicloa = $this->utilidades->obtener_cicloaXcolegio($id_colegio);
            $this->modelo_doc_div_mat_alu->borrar_doc_div_mat_alu($id_colegio,$id_division,$id_materia,$id_docente,$cicloa);//queda claro q se elimina la asignacion del cicloa y no la de los años atenriores
            $datos_docente = $this->modelo_usuario->get_user_x_id_user_group($id_docente);
            $docentes_asignados = $this->modelo_doc_div_mat_alu->obtener_docentes_x_div_x_colegio($id_division,$id_colegio,$id_materia);
            $string_docentes = 'Sin Docentes Asignados';
            if ($docentes_asignados->num_rows() > 0)
            {
                $string_docentes = '';
                $i = 0;
                foreach ($docentes_asignados->result() as $key) 
                {
                    if($i == 0)
                    {
                        $string_docentes = $key->last_name.' '.$key->first_name;
                        $i++;
                    }
                    else
                    {
                        $string_docentes = $key->last_name.' '.$key->first_name.', '.$string_docentes;
                    }
                }
            }
            $data = array
                    ( 
                        'status' => 1,
                        'message' => 'Docente Eliminado de la Materia',
                        'string_docentes' => $string_docentes,
                         'last_name' => $datos_docente->last_name,
                        'first_name'=> $datos_docente->first_name,
                        'documento'=> $datos_docente->documento,
                        'user_id' => $datos_docente->id,
                        'id_user_group' => $id_docente,
                        'cant_docentes_materia' => $docentes_asignados->num_rows(),
                    );
            $data = json_encode($data);
            $this->response($data, REST_Controller::HTTP_OK); 

        }
    }
    public function get_docentes_x_div_mat_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $id_colegio = $_POST["id_colegio"];
            $id_materia = $_POST["id_materia"];
            $id_division = $_POST["id_division"];
           
            $cicloa = $this->utilidades->obtener_cicloaXcolegio($id_colegio);
            $docentes = $this->modelo_doc_div_mat_alu->obtener_docentes_x_div_x_colegio($id_division,$id_colegio,$id_materia);
            $data = array
                    ( 
                        'status' => 1,
                        'message' => 'docentes',
                        'cant' => $docentes->num_rows(),
                        'docentes' => $docentes->result(),
                    );
            
            $data = json_encode($data);
            $this->response($data, REST_Controller::HTTP_OK); 

        }
    }
    public function get_alumnos_x_div_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $id_colegio = $_POST["id_colegio"];
            $id_division = $_POST["id_division"];
            $cicloA = $this->utilidades->obtener_cicloaXcolegio( $id_colegio );
            $result_alumnos = $this->modelo_alumno->get_alumnos_x_division($id_division, $id_colegio, $cicloA);
            $data = array
                    ( 
                        'status' => 1,
                        'message' => 'alumnos',
                        'alumnos' => $result_alumnos,
                    );
            
            $data = json_encode($data);
            $this->response($data, REST_Controller::HTTP_OK); 
        }
    }
    public function get_alumnos_x_doc_div_mat_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $id_colegio = $_POST["id_colegio"];
            $id_materia = $_POST["id_materia"];
            $id_division = $_POST["id_division"];
            $id_docente = $_POST["id_docente"];
           
            $cicloa = $this->utilidades->obtener_cicloaXcolegio($id_colegio);
            $alumnos = $this->modelo_doc_div_mat_alu->obtener_alumnos_x_doc_div_mat($id_division,$id_colegio,$id_materia,$id_docente);
            $data = array
                    ( 
                        'status' => 1,
                        'message' => 'alumnos',
                        'cant' => $alumnos->num_rows(),
                        'alumnos' => $alumnos->result(),
                    );
            
            $data = json_encode($data);
            $this->response($data, REST_Controller::HTTP_OK); 

        }
    }
    public function save_doc_div_mat_alu_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            
            $id_materia = $_POST["modal_add_alumno_id_materia"];
            $id_colegio = $_POST["modal_add_alumno_id_colegio"];
            $cicloa = $this->utilidades->obtener_cicloaXcolegio($id_colegio);
            $id_division = $_POST["modal_add_alumno_id_division"];
            $id_docente = $_POST["select_docentes_div_mat"];
            $cicloA = $this->utilidades->obtener_cicloaXcolegio( $id_colegio );
            $result_alumnos = $this->modelo_alumno->get_alumnos_x_division($id_division, $id_colegio, $cicloA);
           // print_r($_POST);
           // print_r($result_alumnos);
            foreach ($result_alumnos as $alu_inscrip)
            {
                $text_check = 'checkbox_alumno_'.$alu_inscrip->alumno_id.'_'.$id_colegio.'_'.$id_division;
                
                //if($_POST[$text_check])
                $existe = $this->modelo_doc_div_mat_alu->existe_doc_div_mat_alu($id_colegio,$id_division,$id_materia,$id_docente,$alu_inscrip->alumno_id,$cicloa);
                if(isset($_POST[$text_check]))//existe en el form
                {
                   // print_r($_POST[$text_check]);
                    
                    if($existe == 1)//existe
                    {
                        //no hacer nada
                    }
                    else
                    {
                        $dato = array(
                            'id_colegio' =>  $id_colegio,
                            'id_materia' =>  $id_materia,
                            'id_division' =>  $id_division,
                            'id_docente' =>  $id_docente,
                            'ciclo_lectivo' =>  $cicloa,
                            'id_alumno' =>  $alu_inscrip->alumno_id,
                            );
                        $this->modelo_doc_div_mat_alu->insertar($dato);
                    }
                }
                else
                {
                    ///echo "no existe";
                    //hay q eliminar de doc div mat alu
                    $this->modelo_doc_div_mat_alu->delete_doc_div_mat_alu($id_colegio,$id_division,$id_materia,$id_docente, $alu_inscrip->alumno_id ,$cicloa);

                }
            }
            $data = array
                    ( 
                        'status' => 1,
                        'message' => 'La Asignaci&oacute;n fue Realizada',
                        
                    );
            
            $data = json_encode($data);
            $this->response($data, REST_Controller::HTTP_OK); 
        }
    }

}
