<?php 
 
class Migracion_mudap extends CI_Controller 
{	
      
	function __construct(){
		parent::__construct();
		
			//$this->output->enable_profiler(TRUE);
        if(!$this->session->userdata("logged_in")){
            //echo "pierde sesion"; die();
               redirect("login");
         }

	}

	public function crear_pass($pass)
    {
        $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
                    $passApiKey = $this->variables->get_apikey();
                    //$correo = "nodos";
                    $url = $urlApi."/login/prueba_encriptar";
                    $ch = curl_init();
                    $options = array(
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_URL => $url,
                                CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey"),
                                CURLOPT_POST => true,
                                //CURLOPT_POSTFIELDS => json_encode($fields),
                                //CURLOPT_POSTFIELDS => $fields,
                                CURLOPT_POSTFIELDS => array(
                                                            'data' => $pass
                                                    ),  
                                CURLOPT_SSL_VERIFYPEER=> false,
                            );
                      curl_setopt_array( $ch, $options);
                      $response = curl_exec($ch);
                      curl_close($ch);
                     $response = json_decode($response, True);
                    
                     $arre = (array) $response; //convierto el obj en un arreglo.
                     $pass_encriptado = $arre['pass'];
                     return $pass_encriptado;
    }
    //damian
    public function importar_docentes()
    {
        //VARIABLES
        $tabla = "mudap_docentes"; //--modificar por la division
        
        $group_id = 69;
        $colegio_id = 6;
        //$plan_estudio_id = 1;

        //SCRIPT        
        $sql = "SELECT * FROM ".$tabla;
        $cont = 0;
        $query = $this->db->query($sql);
        if($query->num_rows() > 0)
        {
            foreach ($query->result() as $key => $value) 
            {
                $sql_check= "SELECT * FROM users WHERE documento='".$value->dni."'";
                $query_ch = $this->db->query($sql_check);
                if($query_ch->num_rows() == 0)
                {
                    $pass_alu = $this->crear_pass($value->dni);
                    if($value->correo == null)
                            {
                                $email = "";
                            }
                            else
                            {
                                $email = $value->correo;
                            }

                    $insert_docente = "INSERT INTO users (first_name, last_name, active, documento, password,email)
                            VALUES ('$value->nombre', '$value->apellido',1, '$value->dni','$pass_alu','$email')";
                                
                   if($this->db->query($insert_docente))
                   {
                        $id_user_docente = $this->db->insert_id();                    
                        //insertar en users_groups
                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id)
                                VALUES ($id_user_docente, $group_id, $colegio_id)";
                        $this->db->query($insert_users_groups);
                         $cont = $cont + 1;
                           
                    }
                } // fin if el alumno ya existe.
                else{
                    $docente = $query_ch->row();
                    $id_doc = $docente->id;

                    echo "EL Docente".$value->dni." ".$value->apellido." ".$value->nombre." Existe en USERS <br>";
                    $sql_check_user_group= "SELECT * FROM users_groups WHERE user_id='".$id_doc."'and group_id = $group_id";
                    $query_ch_user_group = $this->db->query($sql_check_user_group);
                    if($query_ch_user_group->num_rows() > 0)
                    {
                        echo "EL Docente YA ES DOCENTE EN LA ESCUELA SELECCIONADA<br>";
                    }
                    else
                    {
                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id)
                                VALUES ($id_doc, $group_id, $colegio_id)";
                        $this->db->query($insert_users_groups);
                         //$cont = $cont + 1;
                    }
                    
                }
                
            } // fin for recorrido de los alumnos.
            echo "se insertaron: ".$cont." Docentes<br>";
        }
    } 

    public function importar_alumnos()
    {
        //VARIABLES
        $tabla = "mudap_alumnos"; //--modificar por la division
       // $division_id = 63;
        $group_id = 71;
        $colegio_id = 6;
        $plan_estudio_id = 2;

        $sql = "SELECT * FROM ".$tabla;
        $cont = 0;
        $query = $this->db->query($sql);
        if($query->num_rows() > 0)
            foreach ($query->result() as $key => $value) 
            {
                $sql_check= "SELECT * FROM users WHERE documento='".$value->dni."'";
                $query_ch = $this->db->query($sql_check);
                if($query_ch->num_rows() == 0)
                {
                    $pass_alu = $this->crear_pass($value->dni);
                    $insert_alumno = "INSERT INTO users (first_name, last_name, active, documento, sexo, password, recibir_notificacion)
                            VALUES ('$value->nombre', '$value->apellido',1, '$value->dni', '$value->sexo','$pass_alu',1)";
                    if($this->db->query($insert_alumno))
                    {
                        $id_user_alumno = $this->db->insert_id();                    
                        //insertar en users_groups
                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)
                                VALUES ($id_user_alumno, $group_id, $colegio_id,1)";
                        if($this->db->query($insert_users_groups))
                        {
                            //insertar en inscripcion
                            $id_alumno = $this->db->insert_id();
                            $insert_inscripcion = "INSERT INTO inscripciones (alumno_id,activo, planesestudio_id,divisiones_id)
                                    VALUES ($id_alumno,1,$plan_estudio_id,$value->id_division)";
                                   //echo $insert_inscripcion."<br>";        

                                    if($this->db->query($insert_inscripcion))
                                    {
                                        echo "OK -".$value->dni." ".$value->apellido." ".$value->nombre."<br>";
                                        $cont = $cont + 1;
                                    }else
                                    {
                                      echo "NoInsertoAlumno: ".$value->dni." ".$value->apellido." ".$value->nombre."<br>";
                                      //echo "NoInsertoAlumno: <br>";
                                    }
                        }    
                    }


                }
                else{
                    echo "ELAlumnoExiste: ".$value->dni." ".$value->apellido." ".$value->nombre." <br>";
                }
            echo "se insertaron: ".$cont." alumnos";
            }
    }
    public function importar_tutor()
    {
        $colegio_id = 6;
        $group_id = 70;
        /*$tabla_alumnos = "st_alu_6_anio_b";
        $sql = "SELECT * FROM ".$tabla_alumnos;
        $query_alumnos = $this->db->query($sql);*/

        $tabla_tutores = "tutores_varios";
        $sql2 = "SELECT * FROM ".$tabla_tutores;
        $query_tutores = $this->db->query($sql2);

        /*if($query_tutores->num_rows() > 0)
        {
            //print_r($query_tutores->num_rows())."<br>";
            foreach ($query_tutor->result() as $Tutor)
            {

            }
        }*/
        if($query_tutores->num_rows() > 0)
        {
            //print_r($query_alumnos->num_rows())."<br>";
            foreach ($query_tutores->result() as $tutor_t)
            {
                $email = $tutor_t->username.'@'.$tutor_t->aux;

                $sql_tutor_aux = "SELECT * FROM users WHERE email = '$email'";
                $sql_tutor_aux = $this->db->query($sql_tutor_aux);
                if($sql_tutor_aux->num_rows() > 0)
                {
                    echo "YaExiste TUTOR:".$tutor_t->apellido.", ".$tutor_t->nombre." <br>";
                }
                else
                {
                    $pass = $this->crear_pass($tutor_t->username);
                    
                    $insert_tutor = "INSERT INTO users (first_name, last_name, active, email, password, pago)
                                                VALUES ('$tutor_t->nombre', '$tutor_t->apellido',1,'$email', '$pass',1)";

                            if($this->db->query($insert_tutor))
                            {
                               $id_tutor = $this->db->insert_id();
                               echo "ok Insertado <br>";
                               //-- USERS_GRUPS Insert ---
                                $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)
                                                        VALUES ($id_tutor, $group_id, $colegio_id, 1)";
                                if($this->db->query($insert_users_groups))
                                {

                                }
                            }
                            else
                            {
                                echo "NO INSERTADO :".$tutor_t->apellido.", ".$tutor_t->nombre." <br>";
                            }


                }
                

                    
                
            }

        }
    }
    //

}