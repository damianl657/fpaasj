<script type="text/javascript">
  	$(document).ready(function()
  	{

      $('.select2').select2({
           theme: "bootstrap",
           placeholder: "Seleccione division",
          allowClear: true
      }); 
  	}); 
  	function traer_divisiones(img)
  	{
	  	var idcolegio = $('#select_colegios').val();
	  	$('#divisiones_colegio_anio').empty();
	  	$('#button_add_division').empty();

	  	var button_add_division = '<div class="col-sm-12">'+
			                		'<button class="btn btn-sm btn-warning pull-right m-t-n-xs" id="btn_alumnos" onclick="abrir_modal_agregar_divisiones(this,'+idcolegio+')" type="button"><strong>Agregar Curso</strong></button>'+
			                	'</div>';
		$('#button_add_division').append(button_add_division);
	  	//alert(idcolegio);
	  	var url = '<?=base_url("/index.php/materias/docentes_division_traer_divisiones_colegio_ajax")?>';   
	  	$.ajax({
	          	url: url,
	          	type: 'GET',
	          
	          	data:{idcolegio: idcolegio },
	          	beforeSend: function() 
	          	{
	          			$('#btn_colegios').hide();
	                  $(img).after("<img id='loadingtraer_div' src='<?=base_url('assets/images/loading.gif');?>' height='25' width='25' />");
	            },
	          	success: function(data)
	          	{ 
		          	$('#btn_colegios').show();  
		            $('#loadingtraer_div').remove();
	            	var data = jQuery.parseJSON( data );
	            //console.log(data['divisiones']);
	            	divisiones = jQuery.parseJSON( data['divisiones'] );
	            	if(divisiones.length > 0)
	            	{
		            	var option = '';
		            	var table = '<table id="table_divisiones" class="table table-striped table-bordered dt-responsive nowrap " width="100%">'+
		                            '<thead>'+
		                                '<tr>'+
		                                  '<th>Nombre</th>'+
		                                  '<th>Acciones</th>'+
		                                '</tr>'+
		                              '</thead>'+
		                              '<tbody id="tbody_divisiones">';
	            	for(var i=0; i < divisiones.length ; i++)
		            {
		            	//console.log(divisiones[i]);   
		            	var tr = '<tr>';
		            	var nombre_nivel = divisiones[i]['nombre_nivel']+' '+divisiones[i]['nombre_anio']+' '+divisiones[i]['nombre_division'];
		            	option = option+'<option value="'+divisiones[i]['nodoanios_id']+'_'+idcolegio+'_'+divisiones[i]['division_id']+'">'+nombre_nivel+'</option>';
		            	tr = tr + '<td id="'+divisiones[i]['nodoanios_id']+'_'+idcolegio+'_'+divisiones[i]['division_id']+'">' + nombre_nivel +'</td>'+'<td>'+'</td>';
		            	tr = tr + '</tr>';
		            	table = table + tr;
		            }
		            table = table +'</tbody> </table';
		           


		          //  console.log(option);  
		            var select = '<select name="select_divisiones" id="select_divisiones" class="select2 form-control">'+option+'</select>';
		           	$('#divisiones_colegio_anio').append(table);
		           	$('#table_divisiones').dataTable();
			        

			    }
		        

	          },
	          error: function(response){
	            console.log(response);
	             $('#btn_colegios').show();  
	          }
	        });
  	}
  	function abrir_modal_agregar_divisiones(img, idcolegio)
  	{
  		$('#div_select_anios').empty();
  		$('#div_select_niveles').empty();
  		$('#div_select_especialidades').empty();
  		$("#modal_add_division").modal('show');
  		$(img).after("<img id='loadingtraer_anios_esp_niv' src='<?=base_url('assets/images/loading.gif');?>' height='25' width='25' />");
  		var url = '<?=base_url("/index.php/colegios/get_all_anios_ajax")?>';  
  		$.ajax({
	          	url: url,
	          	type: 'GET',
	          
	          	data:{idcolegio: idcolegio },
	          	beforeSend: function() 
	          	{
	          			
	                  //$(img).after("<img id='loadingtraer_div' src='<?=base_url('assets/images/loading.gif');?>' height='25' width='25' />");
	            },
	          	success: function(data)
	          	{ 
	          		var data = jQuery.parseJSON( data );
		          	//console.log(data.cant_anios);
		          	console.log(data);
		          	var option_d = '';
		          	if(data.cant_anios > 0)
		          	{	
		          		for(var i=0; i < data.anios.length ; i++)
			            {
			            	console.log(data.anios[i]);   
			            	var nombre = data.anios[i]['nombre'];
			            	option_d = option_d+'<option value="'+data.anios[i]['id']+'">'+data.anios[i]['nombre']+'</option>';
			            }
		            	var select_d = '<select name="select_anios" id="select_anios" class="select2 form-control">'+option_d+'</select>';
		            	$('#div_select_anios').append(select_d);
						$('#select_anios').select2({
			           		theme: "bootstrap",
			           		placeholder: "Seleccione el Año",
			          		allowClear: true
			        	}); 
		          	}
			        

			    
		        

	          	},
	          	error: function(response)
	          	{
	            	console.log(response);
	             	//$('#btn_colegios').show();  
	          	}
	        }); 
  		var url = '<?=base_url("/index.php/colegios/get_all_niveles_ajax")?>';  
  		$.ajax({
	          	url: url,
	          	type: 'GET',
	          
	          	data:{idcolegio: idcolegio },
	          	beforeSend: function() 
	          	{
	          			
	                  //$(img).after("<img id='loadingtraer_div' src='<?=base_url('assets/images/loading.gif');?>' height='25' width='25' />");
	            },
	          	success: function(data)
	          	{ 
	          		var data = jQuery.parseJSON( data );
		          	//console.log(data.cant_anios);
		          	console.log(data);
		          	var option_d = '';
		          	if(data.cant_niveles > 0)
		          	{	
		          		for(var i=0; i < data.niveles.length ; i++)
			            {
			            	console.log(data.niveles[i]);   
			            	var nombre = data.niveles[i]['nombre'];
			            	option_d = option_d+'<option value="'+data.niveles[i]['id']+'">'+data.niveles[i]['nombre']+'</option>';
			            }
		            	var select_d = '<select name="select_niveles" id="select_niveles" class="select2 form-control">'+option_d+'</select>';
		            	$('#div_select_niveles').append(select_d);
						$('#select_niveles').select2({
			           		theme: "bootstrap",
			           		placeholder: "Seleccione el Año",
			          		allowClear: true
			        	}); 
		          	}
			        

			    
		        

	          	},
	          	error: function(response)
	          	{
	            	console.log(response);
	             	//$('#btn_colegios').show();  
	          	}
	        });
  		var url = '<?=base_url("/index.php/colegios/get_all_especialidades_ajax")?>';  
  		$.ajax({
	          	url: url,
	          	type: 'GET',
	          
	          	data:{idcolegio: idcolegio },
	          	beforeSend: function() 
	          	{
	          			
	                  //$(img).after("<img id='loadingtraer_div' src='<?=base_url('assets/images/loading.gif');?>' height='25' width='25' />");
	            },
	          	success: function(data)
	          	{ 
	          		var data = jQuery.parseJSON( data );
		          	//console.log(data.cant_anios);
		          	console.log(data);
		          	var option_d = '';
		          	if(data.cant_especialidades > 0)
		          	{	option_d = option_d+'<option value="0">No Corresponde</option>';
		          		for(var i=0; i < data.especialidades.length ; i++)
			            {
			            	console.log(data.especialidades[i]);   
			            	var nombre = data.especialidades[i]['nombre'];
			            	option_d = option_d+'<option value="'+data.especialidades[i]['id']+'">'+data.especialidades[i]['nombre']+'</option>';
			            }
		            	var select_d = '<select name="select_especialidades" id="select_especialidades" class="select2 form-control">'+option_d+'</select>';
		            	$('#div_select_especialidades').append(select_d);
						$('#select_especialidades').select2({
			           		theme: "bootstrap",
			           		placeholder: "Seleccione el Año",
			          		allowClear: true
			        	}); 
		          	}
			        

			    
		        

	          	},
	          	error: function(response)
	          	{
	            	console.log(response);
	             	//$('#btn_colegios').show();  
	          	}
	        });
  		$('#loadingtraer_anios_esp_niv').hide();
  		//traer anios
  	}
 
</script>
<div class="wrapper wrapper-content animated fadeInRight" id="">
    <div class="row">
        <div class="col-lg-12 " id="usuarios_borrar">
            <div class="ibox collapsed float-e-margins">
                <div class="ibox-title collapse-link" id="tituloaltaevento">
                    <h5>Divisiones </h5>
                        <div class="ibox-tools">
                            <a class="">
                                  
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display:block">
                        
                        <div class="form-group ">
	                    	<LABEL>Seleccione Colegio:</LABEL>
	                    </div>
	                    <div class="row">

	                    	<div class="col-sm-4">
	                        	
		                        <div class="form-group " id="div_select_colegios">
		                           	<select name="select_colegios" id="select_colegios" class="select2 form-control">
		                                    <?php 
		                                        foreach ($acciones as $accion) 
		                                        {
		                                            $value = $accion['idcole'];
		                                            ?>
		                                                <option value="<?php echo $value; ?>"><?php echo $accion['namecole'] ?></option>
		                                            <?
		                                        }
		                                    ?>
		                                                                          
		                            </select>
		                        </div>
		                    </div>
		                    <div class="col-sm-6">
		                        <div class="form-group ">
		                           		<button class="btn btn-sm btn-success " id="btn_colegios" onclick="traer_divisiones(this)" type="button"><strong>Listar Divisiones</strong></button>
		                                                          <div type="hidden" id="loading_horarios_dir_esc_" style="margin-top:-5px">
		                                                          </div>
		                        </div>
		                    </div>

		                </div>
		                
		                <div class="row" id="button_add_division">
		                	
                      
		                </div>
				                
		                <div id="divisiones_colegio_anio">
		                    	
		                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal crear divisiones-->
<div class="modal fade" id="modal_add_division" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Agregar Curso</h4>
          </div>
          <center>
          <form id="form_division" autocomplete="on" method="post" class="form-horizontal form-label-left">
            <input type="hidden" id="user_id_edit" name="user_id_edit" value="0" />
            <div class="modal-body">
        
                <input type="hidden" id="permiso_add_divisiones_modal" name="permiso_add_divisiones_modal" value="0" />
                <input type="hidden" id="id_colegio" name="id_colegio" value="0" />
             	<div class="form-group">
                  	<label class="col-sm-2 control-label">Nombre</label>
                  	<div class="col-sm-8"><input type="text" class="form-control" name="nombre" id="nombre">
                 	</div>
              	</div>
              	<div class="form-group">
                  	<label class="col-sm-2 control-label">Nivel</label>
                  	<div class="col-sm-8" id="div_select_niveles">
                 	</div>
              	</div>
              	<div class="form-group">
                  	<label class="col-sm-2 control-label">Año</label>
                  	<div class="col-sm-8" id="div_select_anios">
                 	</div>
              	</div>
              	<div class="form-group">
                  	<label class="col-sm-2 control-label">Especialidad</label>
                  	<div class="col-sm-8" id="div_select_especialidades">
                 	</div>
              	</div>
          
            </div>
            <div class="modal-footer">

                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                
                
          </form>
          </center>
        </div>
    </div>
</div>