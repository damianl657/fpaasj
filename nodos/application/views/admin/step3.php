 <script type="text/javascript">

 	function vaciar_tabla_P3(){
 		var rowCount = $('#tablaPaso3 tr').length;

		if(rowCount > 1)
			for(var i=0; i<(rowCount - 1) ; i++) //-1 porque la cabecera es un tr
				$('#tablaPaso3 tr:last').remove();
 	}

	//-------------------------------------------------------------
	function listar_nivelesEspecialidadesAnios(){

		vaciar_tabla_P3();

		var url = urlApi+"/especialidad/anios_especialidades_cargados";
        
		$.ajax({
	            url: url,
	            type: 'GET',
	            headers: {				      
				       'APIKEY' : passApiKey,
				       'userid' : idUser
				},
				data: {idcolegio: idcolegio},
	            success: function(data)
	            { 
				    	//console.log(data);
						if(data['status']==false)
                   		{ 
                   		  if(data['message']=='NO LOGUIN')
                   		 		location.href = "<?php echo base_url('login'); ?>"; //no està logueado
                   		}
                   		else
                   		{
						 var datos = jQuery.parseJSON( data );
						 //console.log(datos);											
						 $("#tablaPaso3").append(datos); 
						 $('#cuerpo img:last-child').remove();
						 $('#paso3').css('display','block');
						 $('#paso3').slideDown(); 
						}
				} //--fun success
					 	
	    });
	}// fin function listar_nivelesEspecialidadesAnios
	
   //-----------------------------------------------------------
	function guardar_step3(parametros)
	{           //--- Guarda in table nodoAnios ---//
        //console.log(parametros);
        var urlRequest = urlApi+"/anio/insert_nodoanio";

        var stringJson = JSON.stringify(parametros);
		
		//alert('idtabla: '+idusergrupo);
        $.ajax({
            url: urlRequest,
            type: 'POST',
            headers: {      
                    'APIKEY' : passApiKey,
					'userid' : idUser
            },
            data: {data: stringJson, idusergrupo:idusergrupo },
            success: function(data){
                    //console.log(data);
                    if(data['status']==true)
                    {     //console.log(data['message']);
                    	$('.top-right').notify({
	                        message: { text: data['message'] },
	                        fadeOut: { enabled: true, delay: 5000 },
	                        type: 'success2'
	                    }).show();
                    }
                    else
                    {                                      		
                   		if(data['message']=='NO LOGUIN')
                   		 	location.href = "<?php echo base_url('login'); ?>"; //no està logueado
                   		                                
                    }
            },
            error: function(response){
                    console.log(response);    
            }
        });
	} // fin function Guardar_step3
	
    //---------------------------------------------------------------
	
	$(document).ready(function(){
		
		$('#next3').click(function(e){ 
			e.preventDefault();

			var parametros = Array(); 
            
			$("#tablaPaso3 tbody tr").each(function (index) { // Recorre la Tabla
	            if(index>0){ 
	                 var tr_nivel = $(this);   // tr
	                 var id_nodonivel = tr_nivel.attr('id');
					
					//alert(num_nivel);  // cada 
				    $(this).children("td").each(function (index2) { 
						switch (index2) {
							case 0:
						          break;
						    case 1:
						          break;
						    case 2:  
						      	var arre_anios = Array();  // arreglo de especialidades		
								var isChecked = $(this).find('input:checkbox');
								//console.log(isChecked.length);
								for (var i = 0; i < isChecked.length; i++) 
								{
									var id_anio = $(isChecked[i]).val();  // value del checkbox
									var id_nodoespecialidad = $(isChecked[i]).attr('name'); // name del checkbox
									
									if($(isChecked[i]).is(':checked')){ // tratamiento solo para los elementos seleccionados
		                           	  //  var id_anio = isChecked.val();  // value del checkbox
									  //	var id_nodoespecialidad = isChecked.attr('name'); // name del checkbox	
										var fila = {
		                                 //'nodocolegio_id': colegio,
		                                    'anios_id': id_anio,
											'nodoesp_id': id_nodoespecialidad,
											'nodoniv_id':id_nodonivel,
											'checked': '1'
									    };                           
									}else{ // elementos que no estan seleccionados.
										var fila = {
		                                 //'nodocolegio_id': colegio,
		                                    'anios_id': id_anio,
											'nodoesp_id': id_nodoespecialidad,
											'nodoniv_id':id_nodonivel,
											'checked': '0' 
									    };               
									}   
		                             parametros.push( fila ); // guardo en un array los años seleccionados para c/ Nivel-Especialidad.
		                        }
	                            break;

						} // fin switch
					});
				}
			}); // fin each Recorrido de la tabla 
	        
			//console.log(parametros);
			guardar_step3(parametros);

			listar_nivelesEspecialidades(); 
                                   
            $('#paso3').slideUp(); 
            //$('#cuerpo').append("<img src='<?=base_url('assets/images/loading.gif');?>' height='60' width='60' />");               
            //$('#paso3').css('display','none');  
            
		}); //--fin function next_3
		
		//---------------------------------------------------------------------------
		
			
		 $('#atras3').click(function(e){ 
		 	e.preventDefault(); alert('atras');

            mostrar_especialidad_step2();

			listar_especialidades_cargadas();
	        						           
	        $('#paso3').slideUp();
            $('#paso2').slideDown(); 
            //$('#paso3').css('display','none');               
        });
	});         			 				   
			
</script>
  <!------------------------------------------------------------------>
  <!------------- COMIENZA EL HTML del Paso #3  ---------->
  <!------------------------------------------------------------------>
	
	<div class = "paso3" id="paso3" style='display:none; ' >
		
			<!-- Indicadores de Pasos -->
		<div class="container-fluid nopadding">
			<div class="row nopadding center indicadores-pasos">
				<div id="paso-nivel" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding ok">
					Paso 1: Niveles <span><i class="fa fa-check-circle-o"></i></span>
				</div>
				<div id="paso-especialidad" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding ok">
					Paso 2: Especialidades <span><i class="fa fa-check-circle-o"></i></span>
				</div>
				<div id="paso-ano" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding">
					Paso 3: A&ntilde;os <span><i class="fa fa-check-circle-o"></i></span>
				</div>
				<div id="paso-division" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding">
					Paso 4: Divisiones <span><i class="fa fa-check-circle-o"></i></span>
				</div>
			</div>
		</div>
		
		<div class="container-fluid">
			<!-- Descripción Paso -->
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center vspace2">
					<h3>Paso 3</h3>
					<h6>Asigne los a&ntilde;os seg&uacute;n nivel y especialidad</h6>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center vspace">
					<p>Asigne los a&ntilde;os donde se cursa cada especialidad.<br>
						Debe tildar por cada espcialidad los a&ntilde;os que corresponda.</p>
				</div>
			</div>


			<!-- Funcionalidad -->
			<div class="row" style="overflow: auto; height:auto">
				
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					
					<table class="pasos" id="tablaPaso3">
						<tr>
							<th>Nivel</th>
							<th>Especialidad</th>
							<th>Años</th>
						</tr>
						<!--<tr>
							<td>Educación Secundaria Ciclo Básico</td>
							<td>Economía</td> 
							<td>
								<form>
									<div>
										1º
										<input type="checkbox" name="anios" value="1" checked>
									</div>
									<div>
										2º
										<input type="checkbox" name="anios" value="2">
									</div>
									<div>
										3º
										<input type="checkbox" name="anios" value="3">
									</div>
									<div>
										4º
										<input type="checkbox" name="anios" value="4">
									</div>
									<div>
										5º
										<input type="checkbox" name="anios" value="5">
									</div>
									<div>
										6º
										<input type="checkbox" name="anios" value="6">
									</div>
									<div>
										7º
										<input type="checkbox" name="anios" value="7">
									</div>
								</form>
							</td>
						</tr>-->						
												
					</table>
					
				</div>
				
			</div>



			<!-- Anterior/Siguiente -->
			<div class="row vspace4">
				<div id="atras3" class="col-lg-offset-3 col-lg-3 col-md-offset-3 col-md-3 col-sm-offset-3 col-sm-3 col-xs-6">
					<a class="btn-principal-azul" href="">Anterior Paso</a>
				</div>
				<div id="next3" class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
					<a class="btn-principal-azul" href="">Siguiente Paso</a>
				</div>
				
			</div>
			
		</div>		
	   	 		    
    <br>
	 
 
 </div>
  