<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_anio extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }



        /*public function get_anios_BynodonivANDnodoesp($idnodonivel,  $idesp)
        {
            $sql = "SELECT nodoanios.id, anios.nombre, nodoanios.anio_id
                    FROM nodoanios
                    JOIN anios ON nodoanios.anio_id = anios.id
                    JOIN nodonivel ON nodoanios.nodoniv_id = nodonivel.id 
                    WHERE nodonivel.id = $idnodonivel 
                    AND nodonivel.esp_id = $idesp 
                    AND nodonivel.estado=1 ";
            $query = $this->db->query($sql);
            return $query->result();
        }*/
        
        /*public function get_anios_BynodonivANDnodoespNull($idnodonivel)
        {
            $sql = "SELECT nodoanios.id, anios.nombre, nodoanios.anio_id
                    FROM nodoanios 
                    JOIN anios ON nodoanios.anio_id = anios.id
                    AND nodoniv_id = $idnodonivel
                    AND nodoesp_id IS NULL
                     AND estado=1   
                     ";
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }    
        }*/

        public function get_nodoanios($idnodonivel)
        {
            $sql = "SELECT nodoanios.id, anios.nombre, nodoanios.anio_id, ultimo
                    FROM nodoanios 
                    JOIN anios ON nodoanios.anio_id = anios.id
                    AND nodoniv_id = $idnodonivel
                    AND estado=1   
                    ";
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }    
        }

        public function aniosXdivisiones($divisiones)
        {
            $sql="SELECT anios_id
                  FROM divisiones
                  WHERE id IN ($divisiones)
                  GROUP BY anios_id
                 ";
            $query = $this->db->query($sql);
            //return $query->result();
            return $query->result();
        }

        //---------------------------------------------------------
		public function get_all_anios(){
            $this->db->select('anios.*');
            $this->db->from('anios');
            $this->db->order_by('anios.nombre');
            return $this->db->get();
        }
        
        public function insertar_nodoanio($data)
        { 
            /*if($data['nodoesp_id'] == 0){
                //$sql= "SELECT * FROM nodoanios WHERE anio_id=".$data['anio_id']." AND nodoniv_id=".$data['nodoniv_id']." AND nodoesp_id IS NULL";     
                $sql= "SELECT * FROM nodoanios WHERE anio_id=".$data['anio_id']." AND nodoniv_id=".$data['nodoniv_id'];     
                                
            }else{
               $sql= "SELECT * FROM nodoanios WHERE anio_id=".$data['anio_id']." AND nodoesp_id=".$data['nodoesp_id']." AND nodoniv_id=".$data['nodoniv_id'];               
            }*/

            $sql=  "SELECT * 
                    FROM nodoanios 
                    WHERE anio_id=".$data['anio_id']." 
                    AND nodoniv_id=".$data['nodoniv_id']."
                    AND cicloa_id=".$data['cicloa_id']; 

                        
            $query_anio = $this->db->query($sql); 
            
            if($query_anio->num_rows() > 0){ // si el par ya exite que NO inserte dos veces. pero si ya existe y está en baja vuelvo a dar de alta.--//                
                $query= "";
                if($query_anio->row()->estado==0) //si existe pero fue dada de baja, vuelvo a habilitar
                   {$id = $query_anio->row()->id; 
                    $this->db->query("UPDATE nodoanios SET estado=1 WHERE id=$id"); 
                    }
            }else{
                //-- insertar el par (Nivel - Especialidad) --//
                /*if($data['nodoesp_id'] == 0){
                    $data['nodoesp_id'] = NULL;
                }*/
                // var_dump($data);
                $query = $this->db->insert("nodoanios",$data);      
            }
            return $query;
        }

        public function eliminar_nodoanio($data)
        { 
            /*if($data['nodoesp_id'] == 0){
                //$sql= "SELECT * FROM nodoanios WHERE anio_id=".$data['anio_id']." AND nodoniv_id=".$data['nodoniv_id']." AND nodoesp_id IS NULL";     
                $sql= "SELECT * FROM nodoanios WHERE anio_id=".$data['anio_id']." AND nodoniv_id=".$data['nodoniv_id'];     
                                
            }else{
               $sql= "SELECT * FROM nodoanios WHERE anio_id=".$data['anio_id']." AND nodoesp_id=".$data['nodoesp_id']." AND nodoniv_id=".$data['nodoniv_id'];               
            }*/

            $sql= " SELECT * 
                    FROM nodoanios 
                    WHERE anio_id=".$data['anio_id']."
                    AND nodoniv_id=".$data['nodoniv_id']."
                    AND cicloa_id=".$data['cicloa_id'];   

            $query_anio = $this->db->query($sql); 
            
            if($query_anio->num_rows() > 0){ // si el par ya exite lo elimino de la base de datos Por destildarlo.--//
                /*if($data['nodoesp_id'] == 0){
                    $data['nodoesp_id'] = NULL;
                }*/
                //$data=array('estado'=>0);
                $this->db->where('id', $query_anio->row()->id);
                return $this->db->update('nodoanios',$data);
                
            }else{
               return "";             
            }
        }

        

        public function get_anio_in_NodoAnio($idanio, $idnodonivel,  $idnodoesp)
        {
            $sql = "SELECT nodoanios.*, anios.nombre
                    FROM nodoanios
                    JOIN anios ON nodoanios.anio_id = anios.id
                    AND nodoniv_id = $idnodonivel 
                    AND nodoesp_id = $idnodoesp 
                    AND anio_id = $idanio 
                    AND estado=1
                    ";

            $query = $this->db->query($sql);
            if($query->num_rows() > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }
        
        public function get_anio_in_NodoAnio_con_NodoEspNull($idanio,$idnodonivel)
        {
            $sql = "SELECT nodoanios.*, anios.nombre
                    FROM nodoanios 
                    JOIN anios ON nodoanios.anio_id = anios.id
                    AND nodoniv_id = $idnodonivel
                    AND nodoesp_id IS NULL   
                    AND anio_id = $idanio
                     AND estado=1
                   ";
            

            $query = $this->db->query($sql);
            if($query->num_rows() > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }



        public function get_nodoanio_new($id)
        {
            $sql = "SELECT *
                    FROM nodoanios 
                    WHERE nodoanio = $id
                    AND estado=1   
                    ";
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }    
        }


        public function get_nodoanio_datos($datos)
        {
            $query = $this->db->get_where('nodoanios', $datos);
            
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }    
        }


         public function get_nodoanio($id)
        {
            $sql = "SELECT *
                    FROM nodoanios 
                    WHERE id = $id
                    AND estado=1   
                    ";
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }    
        }


        
        public function get_anio_sig($idanio)
        {
            //dado un id traigo el proximo. por seguridad
            $sql = "SELECT * FROM anios 
                    WHERE orden > (SELECT orden FROM anios WHERE id=$idanio) 
                    ORDER BY orden LIMIT 1  
                    ";
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->row()->id;
            }
            else{
                return false;
            }    
        }

        public function get_nodoanio_sig($nivel, $ciclo)
        { 
            $sql = "SELECT * FROM nodoanios
                    JOIN anios ON anios.id = nodoanios.anio_id
                    WHERE nodoniv_id = $nivel  
                    AND cicloa_id = $ciclo
                    ORDER BY anios.orden LIMIT 1  
                    ";
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->row();
            }
            else{
                return false;
            }    
        }


        //-------------------------------------------------------------
        public function get_anios_ConEspecialidad($idcolegio, $anioA) //$anioA = id de plan de estudio
      {           
          $sql ="SELECT nodoanios.id as nodoanios_id,niveles.nombre as nombre_nivel,especialidades.nombre as    nombre_especialidad,anios.nombre as nombre_anio
                FROM nodoanios
                JOIN anios ON anios.id= nodoanios.anio_id 
                JOIN nodonivel ON nodonivel.id= nodoanios.nodoniv_id 
                JOIN niveles ON niveles.id = nodonivel.niveles_id 
                JOIN especialidades ON especialidades.id = nodonivel.esp_id
                WHERE nodonivel.nodocolegio_id= $idcolegio 
                AND nodoanios.cicloa_id = $anioA 
                AND  nodonivel.estado = 1 
          "; 
            
          $query = $this->db->query($sql);           
          if($query->num_rows()>0){
              return $query->result();
          }
          else{
              return false;
          }
      }
  
        public function get_anios_SinEspecialidad($idcolegio, $anioA) //$anioA = id de plan de estudio
        {
            $sql ="SELECT nodoanios.id as nodoanios_id,niveles.nombre as nombre_nivel,anios.nombre as nombre_anio 
                    FROM nodoanios
                    JOIN anios ON anios.id= nodoanios.anio_id 
                    JOIN nodonivel ON nodonivel.id= nodoanios.nodoniv_id 
                    JOIN niveles ON niveles.id = nodonivel.niveles_id 
                    WHERE nodonivel.nodocolegio_id= $idcolegio 
                    AND nodoanios.cicloa_id = $anioA 
                    AND  nodonivel.estado = 1 
                    AND nodonivel.esp_id IS null
                    ";
            
            $query = $this->db->query($sql);           
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }
        public function get_all_anios2()
        {
            $sql ="SELECT * FROM `anios`";
            $query = $this->db->query($sql);    
            return $query;
        }
           
          

       

	}