<style>
  
</style>
<script type="text/javascript">


  passApiKey = '<?php echo $passApiKey; ?>';
   urlApi = '<?php echo $urlApi; ?>';

   idUser = '<?php echo $idusuario; ?>';
   //idcolegio = '<?php //echo $idcolegio; ?>';
   //idgrupo = '<?php //echo $idgrupo; ?>';
   //nombregrupo = '<?php //echo $nombregrupo; ?>';
   //ordengrupo = '<?php //echo $ordengrupo; ?>';
   var colegios = JSON.parse('<?php echo json_encode($this->session->userdata("colegios")); ?>');

   console.log(colegios);
   var arre_activar_user = Array();
   var arre_alta_user = Array();
   var arre_admin_roles = Array();
   var arre_edita_user = Array();
   var arre_listar_user = Array();
   var arre_asigna_tutor = Array();




  if(colegios)
  {
      for(var i=0; i < colegios.length ; i++)
      {
        menu = colegios[i]["menu"];
        if(menu)
          for(var j=0; j < menu.length ; j++)
          {
            //console.log(menu[i]["nombre"]);
            if(menu[j]["nombre"] == "activar_user")
            {
                //colegios a los que puedo activar usuarios
                arre_activar_user.push(colegios[i]["id_colegio"]);
            }
            else
            if(menu[j]["nombre"] == "alta_usuarios")
            {
                //colegios a los que puedo cargar usuarios
                arre_alta_user.push(colegios[i]["id_colegio"]);
            }
            else
            if(menu[j]["nombre"] == "admin_roles")
            {
                //colegios a los que puedo administrar (alta y baja) roles
                arre_admin_roles.push(colegios[i]["id_colegio"]);
            }
            else
            if(menu[j]["nombre"] == "editar_user")
            {
                //colegios a los que puedo administrar (alta y baja) roles
                arre_edita_user.push(colegios[i]["id_colegio"]);
            }
            else
            if(menu[j]["nombre"] == "listado_usuarios")
            {
                //colegios a los que puedo administrar (alta y baja) roles
                arre_listar_user.push(colegios[i]["id_colegio"]);
            }
            else
            if(menu[j]["nombre"] == "agregar_tutor")
            {
                arre_asigna_tutor.push(colegios[i]["id_colegio"]);
            }
           
          }
       
      }

      var arre_alta_user_join = arre_alta_user.join();
      var arre_listar_user_join = arre_listar_user.join();

  }


</script>

  
<div class="wrapper wrapper-content animated fadeInRight" id="general2">
  <div class="row">
    <div class="col-lg-12 ">
      <div class="ibox collapsed float-e-margins">
        <div class="ibox-title collapse-link2" id="tituloaltaevento">
          <h5>Deportista <small>Inscripciones al Torneo</small></h5>
          <div class="ibox-tools">
            <a class="">
              <i class="fa fa-chevron-down"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content" style="display: block">
          <div class="row row2">
            <div class="col-sm-12 b-r " id="contenido2">                                     
              <form id="altatorneo" class="form-horizontal form-label-left" action="<?=site_url('torneos/crear_torneo')?>" method="post">
                <div id="form_roles_edit">        
                  <div class="form-group" id ="colegios_edit"></div>
                  <div class="form-group" id="grupoTorneos">
                    <label for="campo_torneos">Torneos</label>
                    <select class="form-control select2" id="campo_torneos" name="campo_torneos" placeholder="Seleccione Torneo" required>
                      <div id="opcion"></div> 
                    </select>
                  </div>
                   <div id="fechasincrip" class="alert alert-info" role="alert" align="center"></div>
                </div>
             

                <div class="ibox-content" id="idbuscar" >
                  <label>BUSCAR PARTICIPANTE POR <small>Documento, Nombre o Apellido</small></label><br><br>
                  <input id="txt_buscar" class="form-control form-control-sm" type="text" placeholder="Ingrese dni/nombre/apellido">
                  <table class="table table-hover table-responsive col-sm-12 col-md-12 col-lg-12 display " id="tablapart" cellspacing="0" cellpadding="3" width="100%" >
                    <thead>
                      <tr>
                        <th scope="col">Documento</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">Nombre</th>                    
                        <th scope="col">Club</th>
                        <th scope="col">Edad</th>
                        <th scope="col">Inscripto</th>
                      </tr>
                    </thead>
                    <tbody id="tinsc">

                    </tbody>

                  </table>
                </div>
                <!--  <button>Guardar</button> -->
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight" id="general2">
  <div class="row">
    <div class="col-lg-12 ">
      <div class="ibox collapsed float-e-margins">
        <div class="ibox-title collapse-link2" id="tituloaltaevento">
          <h5>Lista <small>de inscriptos al torneo</small></h5>
         
          <div class="ibox-tools">
            <a class="">
              <i class="fa fa-chevron-down"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content" >
         
         
          <table class="table table-hover table-responsive display tabla"  cellspacing="0" 
cellpadding="3" width="100%">
            <thead>
              <tr>
                <th scope="col">Documento</th>
                <th scope="col">Apellido</th>
                <th scope="col">Nombre</th>
                <th scope="col">Especialidad</th>
                <th scope="col">Divisi&oacute;n</th>
                <th scope="col">Eficiencia</th>
                <th scope="col">Categoria</th>
                <th scope="col">Club</th>
                <th scope="col">Torneo</th>
                <th scope="col">Eliminar</th>
              </tr>
            </thead>
            <tbody id="listainscriptostorneo">
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- modal de notificacion de inscripto -->
<div class="modal" id="isncriptosnoty" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Informaci&oacute;n</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <strong id="notificacion"></strong>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-success" data-dismiss="modal">Aceptar</button>
        
      </div>
    </div>
  </div>
</div>
<div class="modal" id="modalinscripcion" tabindex="1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header alert alert-success">
        
                  <h3> Inscripci&oacute;n</h3> 
                   
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
         <form id="altainscripcion" class="form-horizontal form-label-left" method="post">
                <div id="form_roles_edit" class="col-sm-12 b-r">        
                        
                  
                   <!--  <label for="edad">Edad</label> -->
                    <input id="edad" class="form-control" type="hidden">                     
                   
                  
                    <!-- <label for="idtorneo">torneo</label> -->
                    <input id="idtorneo" class="form-control" type="hidden">                     
                  
                 
                   <!--  <label for="iddepo">id_dep</label> -->
                    <input id="iddepo" class="form-control" type="hidden">                     
                  
                 
                   <!--  <label for="idclub">id_club</label> -->
                    <input id="idclub" class="form-control" type="hidden">                     
                 
                  <div class="form-group" id="datosdepo">
                    
                  </div>
                 
                  <div class="form-group" id="grupoEspecialidad">
                    <label for="campo_especialidad">Especialidad</label>
                    <select class="form-control select2" id="select_especialidad" name="select_especialidad" placeholder="Seleccione Especialidad" required>                                            
                      <div id="opcion"></div>                                                  
                    </select>
                  </div>
                  <div class="form-group" id="grupoDivision">
                    <label for="select_division">Divisi&oacute;n</label>
                    <select class="form-control select2" id="select_division" name="select_division" placeholder="Seleccione Divisi&oacute;n" required>                                            
                      <div id="opcion"></div>                                                  
                    </select>
                  </div>
                  <div class="form-group" id="grupoEficiencia">
                    <label for="select_eficiencia">Eficiencia</label>
                    <select class="form-control select2" id="select_eficiencia" name="select_eficiencia" placeholder="Seleccione Eficiencia" required>
                                                       
                    </select>
                  </div>
                  <div class="form-group" id="grupoCategoria">
                    <label for="select_categoria">categoria</label>
                    <select class="form-control select2" id="select_categoria" name="select_categoria" placeholder="Seleccione Categoria" required>                                            
                                                                        
                    </select>
                  </div>
                   
                                    
                </div>
                 
          </form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-success" data-dismiss="modal">Cerrar</button>
        <!-- <input id="btn_guardar" type="button" class="btn btn-outline-success" value="Guardar"> -->
        <button type="button" id="btn_guardar" type="button" class="btn btn-outline-success" data-dismiss="modal">Guardar</button>
      </div>
    </div>
  </div>
</div>
<div class="modal" id="modaleliminar" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Eliminar torneo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <input type="hidden" class="form-control" id="ideliminar_inscripcion" name="ideliminar_inscripcion">
         <input type="hidden" class="form-control" id="clubeliminar_inscripcion" name="clubeliminar_inscripcion">
         <input type="hidden" class="form-control" id="torneoeliminar_inscripcion" name="torneoeliminar_inscripcion">
        <div class="alert alert-danger" role="alert" align="center">
         <h3> SEGURO QUIERE ELIMINAR EL <b>INSCRIPTO</b></h3>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btn_eliminar" class="btn btn-success" data-dismiss="modal">Eliminar</button>
      
      </div>
  </div>
</div>
</div>
<script>
 function Edad(FechaNacimiento) {

    var fechaNace = new Date(FechaNacimiento);
    var fechaActual = new Date()

    var mes = fechaActual.getMonth();
    var dia = fechaActual.getDate();
    var año = fechaActual.getFullYear();

    fechaActual.setDate(dia);
    fechaActual.setMonth(mes);
    fechaActual.setFullYear(año);

    edad = Math.floor(((fechaActual - fechaNace) / (1000 * 60 * 60 * 24) / 365));
    
    return edad;
}
function convertDateFormat(string) {
  var info = string.split('-');
  var mes= "";
  console.log(info);
  switch (info[1]) {
                case "01":
                    mes="Enero";
                    break;
                case "02":
                    mes="Febrero";
                    break;
                case "03":
                    mes="Marzo";
                    break;
                case "04":
                    mes="Abril";
                    break;
                case "05":
                    mes="Mayo";
                    break;
                case "06":
                    mes="Junio";
                    break;
                case "07":
                    mes="Julio";
                    break;
                case "08":
                    mes="Agosto";
                    break;
                case "09":
                    mes="Septiembre";
                    break;
                case "10":
                    mes="Octubre";
                    break;
                case "11":
                    mes="Noviembre";
                    break;
                case "12":
                    mes="Diciembre";
                    break;
                
                default:
                    
                    break;
            }
  return info[2] + ' de ' + mes + ' del ' + info[0];
}
function convertDatenumero(string) {
  var info = string.split('-');
  var fecha= info[2] + '-' + info[1] + '-' + info[0];
  
  return fecha;
}

 function crear_select_colegios(accion)
{   
      //verificar si puede editar datos de usuarios. LISTADO DE USUARIOS!!
      var cont_col = 0;
      var edit_users = [];
      //var colegios = JSON.parse('<?php echo json_encode($this->session->userdata("colegios")); ?>');
      for(var i=0; i < colegios.length ; i++)
      {
        var bandera = false;                 
        menu = colegios[i]["menu"];
        //console.log('menu');
        //console.log(colegios[i]);
        for(var j=0; j < menu.length ; j++)
        {
                            //console.log('menu: '+menu[j]["nombre"]);
          if((menu[j]["nombre"] == accion))
          {
            cont_col = cont_col + 1;
            bandera = true;
            //alert('entro');
          }
          
        }
        if(bandera == true)
        {
          edit_users[i] = 1; //1 SI PUEDE
          //alert('entro');
        } 
        else
        {
          edit_users[i] = 0; //1 SI PUEDE
        }
                         
      }
      //console.log('el arreglo de colegios quedo asi:');
     

      if( cont_col > 0)
      {
        options = '<option value="0" >Seleccione Club.....</option>';
        for(var i=0; i < colegios.length ; i++)
        {
          if(edit_users[i] == 1)
          {
             options = options + '<option value="'+colegios[i]["id_colegio"]+'"  >'+colegios[i]["nombre_colegio"]+'</option>';
          }
        }
        select_edit = '<label class="col-sm-2 control-label">Club:</label><div class="col-sm-8">'+
        '<select class="form-control input-md" id="select_colegio_edit" name="select_colegio_edit">'+options+      
        '</select></div>';
        $("#colegios_edit").append(select_edit);
        $('#select_colegio_edit').select2({
           theme: "bootstrap",
           placeholder: "Seleccione..",
          allowClear: true
        }); 
         //alert('entro');
       
      }
     
    }

    
    
    function listar_torneo(torneo){
      var url ='<?=base_url("/torneos/obtener_torneo_p_club") ?>';    

      $.ajax({
        url: url,
        type: 'POST',
        data: {idclub: torneo},
        success: function(data){
          //alert(data);
          data= JSON.parse(data);
          console.log(data); 
                  
          $('#campo_torneos').html("");
          $.each(data, function(index, val) {                       
                          
                          $('#campo_torneos').append('<option value="'+val.id_torneo+'">'+val.nombre+'</option>');
                       });  
        }
      }) 
      $('#campo_torneos').select2();  
      
  }
   
  function buscarparticipante(participante,club){
    var url ='<?=base_url("/alumno/buscar_participante") ?>';    
    var torneo= $('#campo_torneos').val();
      $.ajax({
        url: url,
        type: 'POST',
        data: {particip: participante,
               club: club},
        success: function(data){
       
          data= JSON.parse(data);
          console.log(data); 
           $('#tinsc').html("");
           if (data.status==false) {
               $('#tinsc').html("");
           }else{
            
          $.each(data, function(index, val) {
            traer_categoria(val.fechanac,val.divisiones_id,i);
            //alert(obtener_inscriptos_a_torneo(torneo,val.id,val.especializacion_id,val.divisiones_id,val.colegio_id));
            //alert(torneo+','+val.id+','+val.especializacion_id+','+val.divisiones_id+','+val.colegio_id);
            //obtener_inscriptos_a_torneo(torneo,val.id,val.especializacion_id,val.divisiones_id,val.colegio_id,i);
           
            //estado=1;
        
            var edad=Edad(val.fechanac)
             $("#tinsc").append(
              '<tr>'
                +'<th scope="row">'+val.documento+'</th><td style="text-transform: uppercase">'+
                val.last_name+
                '</td><td style="text-transform: capitalize">'+val.first_name+'</td><td>'+val.nombreclub+'</td><td>'+edad+'</td>'
                  +'<td>'
                  +'<button id="btn_inscripcion" type="button" class="btn btn-warning" data-toggle="modal" data-target="#modalinscripcion" title="Presione, para inscribir" onclick="datos_para_inscripcion('+edad+','+torneo+','+val.id_usergroup+','+val.colegio_id+')">Inscribir</button>' 
                  +'</td>'     
              +'</tr>'
              );
            
             i++;
          });
           }
          $("#tablapart").dataTable({
             responsive: "true",
             paging: "false",
        dom: 'Bfrtilp',       

        buttons:[ 

      {

        extend:    'excelHtml5',

        text:      'Excel',

        titleAttr: 'Exportar a Excel',

        className: 'btn btn-success'

      },

      {

        extend:    'pdfHtml5',

        text:      'PDF',

        titleAttr: 'Exportar a PDF',

        className: 'btn btn-danger'

      },

      {

        extend:    'print',

        text:      '<i class="fa fa-print"></i> ',

        titleAttr: 'Imprimir',

        className: 'btn btn-info'

      },

    ]       
          });
          
        }
      }) 
  }
function inscribir_a_torneo(torneo,deportista,especialidad,division,eficiencia,categoria,club){
   var url ='<?=base_url("/torneos/inscribir_a_torneo")?>';    
   var i= 0;
      $.ajax({
        url: url,
        type: 'POST',
        data: {tor:torneo,
               dep:deportista,
               esp:especialidad,
               div:division,
               efi:eficiencia,
               cat:categoria,               
               cl:club },
               beforeSend: function() 
          {
            
           $.notify("Prosesando...","info");
            $("#tinsc").html("");
           
          },
        success: function(data){
            $("#notificacion").html("");
           data= JSON.parse(data);
           if (data.estado==1) {
             $("#notificacion").append('<div class="alert alert-success" role="alert" align="center">'
                  +data.message    
                  +'</div>');
             $("#isncriptosnoty").modal("show");
             
             
          }else if(data.estado==2){
             $("#notificacion").append('<div class="alert alert-warning" role="alert" align="center">'
                  +data.message+', se abrira la inscripción el '+data.fecha+'</div>'
                  );
             $("#isncriptosnoty").modal("show");
              
            
          }else{
             $("#notificacion").append('<div class="alert alert-danger" role="alert" align="center">'
                  +data.message+'.<br> Se cerro el '+data.fecha+'</div>'
                  );
             $("#isncriptosnoty").modal("show");
              
          } 
            $("#select_especialidad").empty();
            $("#select_division").empty();
            $("#select_eficiencia").empty();
            $("#select_categoria").empty();
         
        
          
        }
      });

}
/*function obtener_inscriptos_a_torneo(torneo,deportista,especialidad,division,club,indice){
  var url ='<?=base_url("/torneos/obtener_inscripcion_a_torneo") ?>';
  var estado=0;
   $.ajax({
        url: url,
        type: 'POST',
        data: {tor:torneo,
               dep:deportista,
               esp:especialidad,
               div:division,               
               cl:club},
        
        success: function(data){
          data=JSON.parse(data);

          if (data.estado==0) {
          $("#btn_inscrip_"+indice).append('<div class="btn-group" role="group" aria-label="Basic example">'
                      +'<button id="btn_inscripcion" type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Presione, para inscribir" onclick="inscribir_a_torneo('+torneo+','+deportista+','+especialidad+','+division+','+club+')">Inscribir</button>'

                  +'</div>');

          } else {
            $("#btn_inscrip_"+indice).append("<img  id='loading_alta_user' src='<?=base_url('assets/images/tildeve2.png');?>' height='20' width='20' />");
          }
        }
      });

   
}*/
function listar_inscriptos_torneo(idclub,idtorneo){
      var url ='<?=base_url("/Torneos/listar_inscriptos_torneo")?>';    
       var categoria="";
        
      $.ajax({
        url: url,
        type: 'POST',
        data: {cl:idclub,
               tor:idtorneo},
        success: function(data){
          //alert(data);
          data= JSON.parse(data);
          //console.log(data); 
             i=0;
           $("#listainscriptostorneo").html("");
          $.each(data, function(index, val) {
          traer_categoria(val.fechanac,val.iddivisiones,i);
             
           
             $("#listainscriptostorneo").append(
              '<tr>'
                +'<th scope="row">'+val.documento+'</th><td style="text-transform: uppercase">'+val.last_name
                  +'</td><td style="text-transform: capitalize">'+val.first_name+
                  '</td><td>'+val.nespecialidad+
                  '</td><td>'+val.ndivision+'</td>'+
                  '<td>'+val.eficiencia+
                  '</td><td><div id="cat_'+i+'">'+traer_categoria_inscripcion(val.categoria,i)+'</div>'+
                  '</td><td>'+val.nclub+'</td><td>'+val.ntorneo+
                  '</td><td><button type="button" class="btn btn-outline-danger" data-toggle="modal" onclick="id_inscripcion_eliminar('+val.id+','+val.idclub+','+val.idtorneo+')" data-target="#modaleliminar">Eliminar</button></td>'                  
              +'</tr>'

              );
            i++;
          });
           $(".tabla").dataTable({
              responsive: "true",
              paging: "false",

        dom: 'Bfrtilp',       

        buttons:[ 

      {

        extend:    'excelHtml5',

        text:      'Excel',

        titleAttr: 'Exportar a Excel',

        className: 'btn btn-info'

      },

      {

        extend:    'pdfHtml5',

        text:      'PDF',

        titleAttr: 'Exportar a PDF',

        className: 'btn btn-danger'

      },

      {

        extend:    'print',

        text:      '<i class="fa fa-print"></i> ',

        titleAttr: 'Imprimir',

        className: 'btn btn-warning'

      },

    ]       
           });
        }
        
      }) 
    }
  function obtener_torneo_x_id(torneo){
   var url ='<?=base_url("/torneos/obtener_torneo_x_id") ?>';    
       $.ajax({
        url: url,
        type: 'POST',
        data: {tor:torneo},
        beforeSend: function(){
           $("#fechasincrip").html('<strong>Fecha de inicio de inscripci&oacute;n <span class="badge badge-primary">CARGANDO FECHAS......</span></strong>');
        },
         success: function(data){
          data= JSON.parse(data);
          $.each(data, function(index, val) {

           $("#fechasincrip").html('<strong>Fecha de inicio de inscripci&oacute;n <span class="badge badge-primary">'+convertDateFormat(val.inicio)+'</span></strong><br><strong>Fecha de finalizaci&oacute;n de inscripcion <span class="badge badge-primary">'+convertDateFormat(val.fin)+'</span></strong>');
          
           });
         
          
        }
      });
      
  }
  function eliminar_inscripcion(id){
    var url ='<?=base_url("/torneos/eliminar_inscripto") ?>';    

    $.ajax({
      url: url,
      type: 'POST',
      data: {insc:id},
      beforeSend: function () {
        
        $.notify(" Procesando....","info");
      },
      success: function(data){
        $.notify("Inscripto Eliminando","info");
         $('#listainscriptostorneo').empty();
         $.each(data, function(index, val) {
          listar_inscriptos_torneo(val.idclub,val.idtorneo);
          $(".tabla").dataTable();
         }); 
      }
    }) ;

  }
  function datos_para_inscripcion(edad,torneo,depo,club){
    $("#edad").val(edad);
    $("#idtorneo").val(torneo);
    $("#iddepo").val(depo);
    $("#idclub").val(club);
   
  }
  function id_inscripcion_eliminar(id,club,torneo){
  $("#ideliminar_inscripcion").val(id);
  $("#clubeliminar_inscripcion").val(club);
  $("#torneoeliminar_inscripcion").val(torneo);

  }

 
  /* extraido de deportista*/
    function listar_especialidad(){
      var url ='<?=base_url("/alumno/obtener_especialidad") ?>';    

      $.ajax({
        url: url,
        type: 'POST',
        data: {},
       
        success: function(data){
         
          opt="<option value='0'>Selecione una Especialidad........</option>";
          data= JSON.parse(data);
          console.log(data);  
          $('#select_especialidad').html("");
         
          $.each(data, function(index, val) {                       
            opt= opt +'<option value="'+val.id+'">'+val.nombre+'</option>';              
                       });  
             $('#select_especialidad').append(opt);
        }
      }) 
      $('#select_especialidad').select2({        
        placeholder: "Select a state",
        allowClear: true
  
      });

  }
  function listar_division(esp,obj){
      var url ='<?=base_url("/alumno/obtener_division") ?>';    
  
      $.ajax({
        url: url,
        type: 'POST',
        data: {id_esp: esp},
        beforeSend: function() 
              {
                $(obj).after("<img  id='loading_division' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");
              },
        success: function(data){
          //alert(data);
          opt = '<option value="0"> seleccione divisi&oacute;n</option>';
          data= JSON.parse(data);       
                  
          $('#select_division').html("");
          $.each(data, function(index, val) {
                          opt = opt + '<option value="'+val.id+'">'+val.nombre+'</option>';
                       });  
                          $('#select_division').append(opt);
                           $("#loading_division").remove();
        }
      }) 
      $('#select_division').select2();  
      
  }
  function traer_categoria_inscripcion(id,i){
    var url ='<?=base_url("/torneos/traer_categoria_inscripcion") ?>';    
       $.ajax({
        url: url,
        type: 'POST',
        data: {ins:id},
         success: function(data){
          data= JSON.parse(data);
          
          $("#cat_"+i).html("");
          $.each(data, function(index, val) {
          $("#cat_"+i).html(val.descripcion);
            
          });
         
          
        }
      });
  }
  function listar_eficiencia(div,obj){
      var url ='<?=base_url("/alumno/obtener_eficiencia") ?>';    
  
      $.ajax({
        url: url,
        type: 'POST',
        data: {id_div: div},
         beforeSend: function() 
              {
                $(obj).after("<img  id='loading_eficiencia' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");
              },
        success: function(data){
          opt = '<option  value="0"> seleccione eficiencia</option>';
          data= JSON.parse(data);       
          
                  
          $('#select_eficiencia').html("");
          $.each(data, function(index, val) {
                          opt = opt + '<option value="'+val.id+'">'+val.nombre+'</option>';
                       });  
                          $('#select_eficiencia').append(opt);
                          $('#loading_eficiencia').remove();
        }
      }) 
      $('#select_eficiencia').select2();  
      
  }
function traer_categoria(edad,divis){
    var url ='<?=base_url("/alumno/obtener_categoria") ?>';
   
    
    $.ajax({
        url: url,
        type: 'POST',
        data: {edad:edad,
               div:divis},
      
      success: function(data){
        //alert(data);
        data= JSON.parse(data);
        //$("#categoria").html("");
        $('#select_categoria').html("");
          $.each(data, function(index, val) {
                          edad1=val.edad; 
                          arre_edad =edad1.split(",");                      
                          for (var i = 0; i < arre_edad.length; i++) {
                            if (arre_edad[i]==edad) {

                              $('#select_categoria').append('<option value="'+val.id+'">'+val.descripcion+'</option>');
                            }else if (arre_edad[i]==0) {
                              $('#select_categoria').append('<option value="'+val.id+'">'+val.descripcion+'</option>');
                            }
                          }
                       });
                      $('#select_categoria').select2();
              
      }
    });
 
   
  }
$(document).ready(function() {
    
    $("#grupoTorneos").hide();
    $("#selectdivision").hide();
    $("#grupoCategoria").hide(); 
    $("#fechasincrip").hide();
    $("#grupoDivision").hide();
    $("#grupoEficiencia").hide();
    
   

    $("#select_colegio_edit").change(function(){  
      var club = $('#select_colegio_edit').val();
      console.log(club);
      listar_especialidad();
      

    });
     $("#select_especialidad").change(function(){   
      //alert('entro');
      $("#grupoDivision").show();   
      var especialidad = $('#select_especialidad').val();
      listar_division(especialidad,this);
    });   
      $("#select_division").change(function(){   
      //alert('entro');
      $("#grupoEficiencia").show();   
      var eficiencia = $('#select_division').val();
      listar_eficiencia(eficiencia,this);
      var edad= $("#edad").val();
      traer_categoria(edad,eficiencia);
      $("#grupoCategoria").show();   
    });
     

    crear_select_colegios('listado_usuarios');
    $("#select_colegio_edit").change(function(){   
      $("#grupoTorneos").show();   
      var torneo = $('#select_colegio_edit').val();
      listar_torneo(torneo);
    });
    $("#campo_torneos").change(function() {
      $("#fechasincrip").show();
      var torneo= $("#campo_torneos").val();
      var club= $('#select_colegio_edit').val();
      $("#idbuscar").show();      
      obtener_torneo_x_id(torneo);
      listar_inscriptos_torneo(club,torneo);
      //$("#tinsc").empty();


    });
    $("#btn_guardar").click(function(){
      /*alert("hola");*/
     var torneo = $("#idtorneo").val();
     var deportista = $("#iddepo").val();
     var especialidad = $("#select_especialidad").val();
     var division = $("#select_division").val();
     var eficiencia = $("#select_eficiencia").val();
     var categoria = $("#select_categoria").val();
     var club = $("#idclub").val();
     var torneo2= $("#campo_torneos").val();
     var club2= $('#select_colegio_edit').val();
      //alert(categoria);
      inscribir_a_torneo(torneo,deportista,especialidad,division,eficiencia,categoria,club);
      listar_inscriptos_torneo(club2,torneo2);
    });
    $("#txt_buscar").keyup(function(){
      var part= $("#txt_buscar").val();
      var club= $('#select_colegio_edit').val();
      //console.log("Club:"+club);
      buscarparticipante(part,club);
      listar_especialidad();
    });
    $("#btn_eliminar").click(function()
    { 
       var id =$("#ideliminar_inscripcion").val();  
      
       eliminar_inscripcion(id);
       
    });   
    $("#selectespecialidad").change(function(){   
      alert('entro');

      $("#selectdivision").show();   
      var especialidad = $('#selectespecialidad').val();
      listar_division(especialidad);
    });


});
    
    
     


    
</script>