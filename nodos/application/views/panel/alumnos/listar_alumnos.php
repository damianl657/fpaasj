
   <style>
	    .columna-categoria {
			display: block;
			width: 270px;
			margin-right: auto;
			margin-left: auto;
			margin-top: 4px;
			white-space: nowrap;
			height: 250px;
			vertical-align: top;
			padding: 10px;
			border-radius: 4px;
        }
		
		#btn-listar{
			  margin-top: 12px;
			  
		}
  </style>
<script type="text/javascript">
  
    //--- Areas Variables ----------------------------
	  var passApiKey = '<?php echo $passApiKey; ?>';
	  var urlApi = '<?php echo $urlApi; ?>';
	  var idUser = '<?php echo $idusuario; ?>';
	  var idcolegio = '<?php echo $idcolegio; ?>';
	  //var idusergrupo = '<?php echo $idgrupo; ?>';
	  var nombrecolegio = '<?php echo $nombrecolegio; ?>';
	  
	//----Areas Function ------------------------------
    function get_alumnos_x_division(id_division){
		$(".alumnos_tutores").html("");		 
	    var url = urlApi+"/alumno/obtener_alumnos_padres_x_division";   
  
		$.ajax({
	    	url: url,
			type: 'GET',
			headers: {              
				'APIKEY' : passApiKey,
				'userid' : idUser,
			},
			data:{idcolegio:idcolegio, division_id: id_division },
			success: function(data){
				console.log(data);
				var data = jQuery.parseJSON( data );
			  	if(data['status']==false){
			  		var string = "<p class='text-danger'>No hay alumnos asignados</p>";
			  		$(".alumnos_tutores").html(string);
			  	}
			  	else{
			  		data.forEach(function(currentValue,index){
			  			console.log(currentValue);
			  			var tutores = currentValue.tutores;
			  			var nombre = currentValue.nombre_alumno;
			  			var apellido = currentValue.apellido_alumno;
			  			var documento = currentValue.documento_alumno;
			  			if(typeof documento == 'undefined'){
			  				documento = "";
			  			}
			  			var email = currentValue.email_alumno;
			  			if(typeof email == 'undefined'){
			  				email = "";
			  			}
			  			var id = currentValue.id
			  			var id = currentValue.id;
			  			var string = "<br><div class= 'success'>";
			  			string += "<div id='alumno"+id+"'>"+
			  							apellido+" "+nombre+
			  						 "<br>"+documento+"<br>"+email+"<br>"+id+
			  						 "</div>";

			  			if(typeof tutores != 'undefined'){
				  			string += "<table class='table'>";
				  			string += "<head>"+
				  						"<th>Id #</th>"+
				  						"<th>Nombre</th>"+
				  						"<th>Email</th>"+
				  					  "</head>"+
				  					  "<body>";
				  			//Recorro los tutores			  				
				  			tutores.forEach(function(currentValue, index){				  				
					  			var nombre = currentValue.nombre_tutor;
					  			var apellido = currentValue.apellido_tutor;
					  			var email_tutor = currentValue.email_tutor;
					  			var id = currentValue.id;
					  			string += 	"<tr id='tutor"+id+"'>"+
					  						"<td id='tutor"+id+"'>"+
					  							id+
					  						 "</td>"+
					  						"<td id='tutor"+id+"'>"+
					  							apellido+" "+nombre+
					  						 "</td>"+
					  						"<td id='tutor"+id+"'>"+
					  							email_tutor+
					  						 "</td>"+
					  						 "</tr>";
				  			});	
				  			string += "</body>"+
				  					  "</table>";					  			
			  			}
			  			else{
			  				console.log("Tutores no definidos");
			  				string += "<br><p class='text-danger'>No hay padre/madre o tutor asignados</p>";
			  			}
			  			string += "</div><br>";
			  			$(".alumnos_tutores").append(string);
			  			string = "";
			  		});
			  	}
			},
			error: function(response){
				console.log(response);
		  		var string = "No hay alumnos asignados";
		  		$(".alumnos_tutores").html(string);					
			}
		});
    }      
	 
    //----------------------------------------------------
    $(document).ready(function() {
          $('#division').select2({
               theme: "bootstrap",
               placeholder: "Seleccione division",
              allowClear: true
          }); 

          $('.select2').select2({
               theme: "bootstrap",
               placeholder: "Seleccione division",
              allowClear: true
          }); 
		  
		//--------------------------------------------  
		$('#btn_alumnos').on('click', function() {
			var id_division = $('#division_id').val();
			get_alumnos_x_division(id_division);
			return false;
		});
		
		/*$('select#division_id').on('change',function(){
			 var id_division = $('#division_id').val();
			 
			 //get_alumnos_x_division(id_division);
			return false;
        });*/
		
		//--------------------------------------------  
		  
    }); // fin document Ready.
   
</script>
  <!-- Comienzo HTML -->     
 <div class="wrapper wrapper-content animated fadeInRight">        
    <!-- Funcionalidad -->     
        <div class="row">
            <div class="col-lg-12">
			    <div class="ibox collapsed float-e-margins">
                    <div class="ibox-title">
                        <h5><?php echo $titulo?></h5>
                        <div class="ibox-tools">                            
							<a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                      <div class="row">
                          <div class="col-sm-6">
                              <?php $attributes = array('id' => 'formulario', 'class' => 'form');
                                              echo form_open('alumno/alumnos_detaill',$attributes); ?>
                            <div class="form-group ">
                                  <label>Seleccione una Divisi&oacute;n: </label>
                            </div>
                            <div class="form-group ">
                              <?php  //print_r($divisiones); ?>
                                <!--<table cellspacing="10"><tr><td>-->
                                <select name="divisiones" id="division_id" class="select2 form-control">
                                 <?php 
                                foreach($divisiones as $key)
                                {
                                  if(isset($key->nombre_especialidad)){ ?>
                                    <option value="<?php echo $key->division_id ?>"><?php echo $key->nombre_nivel." ".$key->nombre_especialidad."  ".$key->nombre_anio ?></option>
                                  <?php
                                 }else{ ?>
                                  <option value="<?php echo $key->division_id?>"><?php echo $key->nombre_nivel." ".$key->nombre_anio." ".$key->nombre_division ?></option><?php 
                                 }
                                } ?> 
                                </select>
                                </td><td>
                               
                                <!--</td></tr></table>-->
                            </div> 

                            <?php echo form_close( ); ?>
                            
                          </div>

                          <div class="col-sm-6" style="margin-top:35px">
                            <button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="btn_alumnos" type="button"><strong>Listar</strong></button>
                          </div>
                        </div>
                    </div>
                    <div class="ibox-content">
                    	<div class="row">
                      		<div class="col-sm-6 alumnos_tutores">
                      		</div>
                      	</div>
                    </div>
                </div>
			    <!-- -->               
     			<!-- Funcionalidad --> 
                <br>
            </div>
         </div>
    </div>
    