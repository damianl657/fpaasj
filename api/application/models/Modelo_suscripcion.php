<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_suscripcion extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }

        public function guardar_email($email)
        {
            $data = array('email' => $email);
            $this->db->select('email');
            $this->db->where($data);
            $resultado = $this->db->get('suscripciones');
            if($resultado->num_rows() > 0){//repetido
                return 2;
            }
            else{
                if($this->db->insert('suscripciones', $data)){//se guardo correctamente
                    return 0;
                }
                else{//ocurrio un error
                    return 1;
                }
            }            
        }

	}