<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';


    class Upload extends REST_Controller {


    private $extPermitidas = array('jpg','jpeg','png','gif','bmp', 'doc','dot','rtf','docx','xlsx','xls','csv','pdf','rar','zip', '7z','gz','tgz','txt','ppt','pps','pptx','potx','ppsx','odt','ods','mp3','wma','JPG','JPEG','PNG','BMP','mp4','MP4');

     private $extPermitidasFotos = array('jpg','jpeg','png','bmp','JPG','JPEG','PNG','BMP');


    function __construct()
    {
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        parent::__construct();
        $this->load->model("modelo_upload");
        //$this->load->library("class.uploader");
       
        // Configurar limites para cada uno de los metodos, no solo controlador
        // La tabla limits tiene que estar creada y la opcion limits TRUE en application/config/rest.php
        
        //$this->methods['tutores_get']['limit'] = 1; // 500 peticiones por hora por usuario/key
        //$this->methods['user_post']['limit'] = 100; // 100 peticiones por hora por usuario/key
        //$this->methods['user_delete']['limit'] = 50; // 50 peticiones por hora por usuario/key
    }

    /*public function get_extPermitidas(){
        return $this->extPermitidas;
    }*/

    public function upload_file_post()
    {
        $userid = $this->utilidades->verifica_userid();
        
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        { 
            $uploaddir = $this->utilidades->get_url_adjuntos();
            //print_r($uploaddir);

            $sizeArchivo=20; //MB
            
            $ecxedesize=0;
            
            $anioA=date('Y');
            $mesA=date('m');

            if(!is_dir($uploaddir)) 
                mkdir($uploaddir, 0777);
          
            $carpeta = $anioA.'-'.$mesA;
            $uploaddir = $uploaddir.$carpeta.'/';
            if(!is_dir($uploaddir)) 
                mkdir($uploaddir, 0777);

            $data = array();

            
            $error = false;
            $files = array();
           
            foreach($_FILES as $file)
            {
                $ext = pathinfo($file['name'], PATHINFO_EXTENSION);  //obtengo la extension
                $extperm = $this->extPermitidas;
                if (in_array($ext, $extperm))  //comprubo que sea una extension permitida
                {
                
                    $datos = array('nombre'=>''); // vacio
                    $idfile = $this->modelo_upload->insert_file($datos); //retorna id insertado

                    //$ultimoId = $this->modelo_upload->obtener_ultimoid() + 1;                

                    $nombre= $idfile."_".$file['name'];
                    $nombre = str_replace(" ", "_", $nombre);

                    $nombre2 = $file['name'];
                    $nombre2 = str_replace(" ", "_", $nombre2);

                    $ruta = $uploaddir.$nombre;


                    $size = $file['size'] ;
                    if($size > 1024)
                    {
                        $temp = $size/1024;
                        //$temp = number_format($temp, 2, '.', '');
                        if ($temp > 1024)
                        {
                            $size = number_format($temp/1024, 2, '.', '')." MB";
                            if( ($temp/1024) > $sizeArchivo )
                                $ecxedesize=1;
                        }
                        else
                            $size = number_format($temp, 2, '.', '')." KB";
                    }
                    else 
                        $size = number_format($size, 2, '.', '')." Bytes";


                    if($ecxedesize==0)
                    {
                        $tipo = $file['type']; //mime

                        //echo "LLEGA--";
                        //echo $ruta."--";
                        //echo $file['name']."--";

    
                        //$move = copy($file['tmp_name'], $ruta);

                        //var_dump($move);

                        if($file &&  move_uploaded_file($file['tmp_name'], $ruta) )
                        //if(0)
                        {
                            $datos = array('nombre'=>$nombre2, 'carpeta'=>$carpeta, 'size'=>$size, 'tipo'=>$tipo); 
                            $result = $this->modelo_upload->update_file($idfile,$datos);
                            if($result)
                            {
                                $arre = array('id'=>$idfile, 'nombre'=>$nombre2, 'size'=>$size);
                                $files[] = $arre; //id file
                            }
                        }
                        else
                        {
                            $error = true;
                            $mensaje="error al subir el archivo"; 
                        }
                    }
                    else
                        {
                            $error = true;
                            $mensaje="tamaño eccedido"; 
                        }
                }
                else 
                {
                    $error = true;
                    $mensaje="Extensión no permitida";
                }
                    
            }
            //$idsfiles = join(',',$files);
            $data = ($error) ? array('error' => $mensaje, 'nombre'=>$file['name']) : array('files' => $files);
            
            print_r( json_encode($data) );
        }
                    
    }


    public function delete_file_post()
    {
        $userid = $this->utilidades->verifica_userid();
        
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        { 
            $uploaddir = $this->utilidades->get_url_adjuntos();
           
            $idfile = $_POST['idfile'];   
            $idevento = $_POST['idevento'];   

            //asumo que idevento = 0, cuando está subiendo el archivo
            //idevento > 0, cuando el archivo fue subido antes y ahora está editando
            // es decir el evento ya fue creado anteriormente.

            if($idevento == 0){
                $result = $this->modelo_upload->get_file($idfile); 
                if($result)
                {                
                    $result2 = $this->modelo_upload->delete_file($idfile); 
                    if($result2)
                    {
                        $name = $result->id .'_'.$result->nombre ;
                        $urlfile =  $uploaddir.$result->carpeta.'/'.$name;
                        //print_r($urlfile);
                        if (file_exists($urlfile))
                            unlink($urlfile);
                    }
                } 
            }
            else
            {
                $this->modelo_upload->delete_event_file($idevento, $idfile); 

                $files = $this->modelo_upload->get_eventFiles_ByFileId($idfile);
                                
                if($files==0){
                    $result = $this->modelo_upload->get_file($idfile); 
                    if($result)
                    {                
                        $result2 = $this->modelo_upload->delete_file($idfile); 
                        if($result2)
                        {
                            $name = $result->id .'_'.$result->nombre ;
                            $urlfile =  $uploaddir.$result->carpeta.'/'.$name;
                            //print_r($urlfile);
                            if (file_exists($urlfile))
                                unlink($urlfile);
                        }
                    } 
                }
            }

            $this->response([
                'status' => TRUE,
            ], REST_Controller::HTTP_OK); 
               
        }
                    
    }


    public function get_files_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $idevento = $this->get('idevento');

            $uploaddir = $this->utilidades->get_url_adjuntos2();
            
            $result = $this->modelo_upload->get_files_evento($idevento);
            
           
            if ($result)
            {

                $arre = array();
                
                foreach ($result as $file) {

                    $nombre2 = $file->id.'_'.$file->nombre;
                    
                    $url = $uploaddir.$file->carpeta.'/'.$nombre2;

                    $aux = array(
                        'id' => $file->id,
                        'url' => $url,  
                        'nombre' => $file->nombre,  
                        'tipo' => $file->tipo,   
                        'size' => $file->size
                    );

                    $arre[] = $aux;
                }

                $this->response([
                    'status' => TRUE,
                    'files' => $arre,
                ], REST_Controller::HTTP_OK); 
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron archivos',
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }




    public function subir_foto_post()
    {
        $userid = $this->utilidades->verifica_userid();
        
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        { 
            $uploaddir = $this->utilidades->get_url_img2();
            //print_r($uploaddir);

            $sizeArchivo=10; //MB
            $ecxedesize=0;
            
            $anioA=date('Y');
            $mesA=date('m');

            if(!is_dir($uploaddir)) 
                mkdir($uploaddir, 0777);

            $data = array();
            $error = false;
            $files = array();
           
            foreach($_FILES as $file)
            {
                $ext = pathinfo($file['name'], PATHINFO_EXTENSION);  //obtengo la extension
                $extperm = $this->extPermitidasFotos;
                if (in_array($ext, $extperm))  //compruebo que sea una extension permitida
                {     
                    $nombre0 = 'foto_id'.$userid;
                    $nombre = $nombre0.'.'.$ext;
                    $ruta = $uploaddir.$nombre;
                    $ruta0 = $uploaddir.$nombre0; //sin extension


                    //print_r($ruta);
                    $kb = 0;
                    $size = $file['size'] ;
                    if($size > 1024)
                    {
                        $temp = $size/1024;
                        //$temp = number_format($temp, 2, '.', '');
                        if ($temp > 1024)
                        {
                            $size = number_format($temp/1024, 2, '.', '')." MB";
                            if( ($temp/1024) > $sizeArchivo )
                                $ecxedesize=1;
                        }
                        else
                        {
                            $kb = 1;
                            $size0 = number_format($temp, 2, '.', '');
                            $size = number_format($temp, 2, '.', '')." KB";
                        }
                    }
                    else 
                        $size = number_format($size, 2, '.', '')." Bytes";


                    if($ecxedesize==0)
                    {   
                        if (file_exists($ruta)) {
                            unlink($ruta);
                        }

                        $noAchicar=0;
                        if( ($kb==1) && ($size0 < 500) ) //no comprimir las menores a 500 kb
                            $noAchicar=1;

                        if($file && move_uploaded_file( $file['tmp_name'], $ruta) )
                        //if(0)
                        {
                            if($noAchicar==0)
                            {
                                //creo la imagen gd
                                $ext = pathinfo($ruta, PATHINFO_EXTENSION);
                                $image = $this->crearImagen_gd($ext, $ruta);

                                //obtengo el tamanio original
                                $sizeO = getimagesize($ruta);
                                
                                //comprimo la imagen a la mitad, en forma proporcional al ancho
                                $imageScaled = imagescale($image, $sizeO[0]*0.6); 

                               
                                if (file_exists($ruta)) {
                                    unlink($ruta);
                                }

                                // Save the image as 'simpletext.jpg'
                                //imagejpeg($imageScaled, $ruta.'simpletext.jpg',80); //80 = calidad (del 0 al 100)
                                imagejpeg($imageScaled, $ruta0.'.jpg', 80); //80 = calidad (del 0 al 100)
                                
                                // Free up memory
                                imagedestroy($image);

                                $nombreNew = $nombre0.'.jpg';
                            }
                            else $nombreNew = $nombre;

                            //print_r($nombreNew);

                            $data = array(
                                'foto' => $nombreNew,
                            );

                            $this->modelo_upload->update_foto_usu($data,$userid);
                            
                            $mensaje = "ok"; 
                        }
                        else
                        {
                            $error = true;
                            $mensaje="error al subir el archivo"; 
                        }
                    }
                    else
                        {
                            $error = true;
                            $mensaje="tamaño eccedido"; 
                        }
                }
                else 
                {
                    $error = true;
                    $mensaje="Extensión no permitida";
                }
                    
            }
            //$idsfiles = join(',',$files);
            $data = ($error) ? array('error' => $mensaje) : array('result' => $mensaje,'name' => $nombreNew);
            
            print_r( json_encode($data) );
        }
                    
    }

    public function crearImagen_gd($ext, $src)
    {
        switch ($ext) {
           case 'jpg':
               $img_r = imagecreatefromjpeg($src);
               break;

            case 'jpeg':
               $img_r = imagecreatefromjpeg($src);
               break;

             case 'png':
               $img_r = imagecreatefrompng($src);
               break;

             case 'bmp':
               $img_r = imagecreatefromwbmp($src);
               break;
           
           default:
               exit;
               break;
       }
       return $img_r;
    }

    public function recortar_foto_post()
    {
        $userid = $this->utilidades->verifica_userid();
        
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {  
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                $varX=$_POST['x'];
                $varY=$_POST['y'];
                $varW=$_POST['w'];
                $varH=$_POST['h'];

                $wtotal = $_POST['wtotal'];
                $htotal = $_POST['htotal'];


                //print_r($_POST);

                $this->load->model("modelo_usuario");
                $userDatos = $this->modelo_usuario->get_users_id($userid);


                $uploaddir = $this->utilidades->get_url_img2();

           
               //$targ_w = 200;
               //$targ_h = 200;
               $jpeg_quality = 90;
               $src = $uploaddir.$userDatos->foto;

               $nombre = $userDatos->foto;
               $nombre = explode(".", $nombre);

               $src0 = $uploaddir.$nombre[0]; //ruta mas nombre sin extension

               //print_r($src0);

               $sizeO = getimagesize($src);

               $auxW = $wtotal*100/$sizeO[0]; //relacion entre la imagen real y el tamanio del div img
               $auxH = $htotal*100/$sizeO[1]; //relacion entre la imagen real y el tamanio del div img

               $razonW = 100 / $auxW;
               $razonH = 100 / $auxH;


               //print_r($sizeO);

               $ext = pathinfo($src, PATHINFO_EXTENSION);
               
               $img_r = $this->crearImagen_gd($ext, $src);
               

               $varW = $varW*$razonW;
               $varH = $varH*$razonH;

               $varX= $varX*$razonW;
               $varY = $varY*$razonH;

               $targ_w = $varW;
               $targ_h = $varH;

               /*print_r($varW .'x'.$varH);
               print_r('<br>');
               print_r($varX .'x'.$varY);
               print_r('<br>');
               print_r($targ_w .'x'.$targ_h);*/

               
               $dst_r = ImageCreateTrueColor($targ_w, $targ_h);
               //$dst_r = ImageCreateTrueColor($sizeO[0], $sizeO[1]);
               //$dst_r = ImageCreateTrueColor(, $varH*$razonH );
               
               imagecopyresampled($dst_r, $img_r, 0, 0, $varX, $varY, $targ_w, $targ_h, $varW, $varH);
               header('Content-type: image/jpeg');

                if (file_exists($src)) {
                    unlink($src);
                }

               imagejpeg($dst_r, $src0.'.jpg', $jpeg_quality);

               $nombreNew = $nombre[0].'.jpg' ;
               $data = array(
                    'foto' => $nombreNew,
                );

                $this->modelo_upload->update_foto_usu($data,$userid);

               $data = array('name' => $nombreNew);
            
                print_r( json_encode($data) );
            }
        }
                    
    }


}
