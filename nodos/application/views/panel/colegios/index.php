<script type="text/javascript">
  $(document).ready(function()
  {

      $('.select2').select2({
           theme: "bootstrap",
           placeholder: "Seleccione division",
          allowClear: true
      }); 
      
    if ($("#datatable-nodos2").length) 
    {
      $("#datatable-nodos2").DataTable({
      //"scrollX": true,scrollY:        "300px",
        scrollCollapse: true,
      
      });//fin datatable
    }
  
  }); 
</script>
<script>
  function form_alta_colegio()
  {
    $("#modal_alta_colegio").modal('show');
  }
  function reload_table()
  {
    $("#datatable-nodos2").dataTable().fnDestroy();
    if ($("#datatable-nodos2").length) 
    {
      $("#datatable-nodos2").DataTable({
      //"scrollX": true,scrollY:        "300px",
        scrollCollapse: true,
      
      });//fin datatable
    }
  }
  function generar_datatable_menuacciones()
  {
    //$("#tabladatos_"+id_colegio).DataTable();

            var handleDataTableButtons3 = function() {
              if ($("#datatable-nodos2").length) {
                tableMenuacciones=$("#datatable-nodos2").DataTable({

            //"scrollX": true,
            //"scrollY": true,

            "scroller": {
            "displayBuffer": 2
            },


                  responsive: true,
                  keys: true,
                  deferRender: true,
                  scrollCollapse: true,

                  "language": {
                                "sProcessing":     "Procesando...",
                                "sLengthMenu":     "Mostrar _MENU_ registros",
                                "sZeroRecords":    "No se encontraron resultados",
                                "sEmptyTable":     "NingÃºn dato disponible en esta tabla",
                                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Buscar:",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":     "Ãšltimo",
                                    "sNext":     "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                              }
                });
              }
            };
            TableManageButtons3= function() {
              "use strict";
              return {
                init: function() {

                  handleDataTableButtons3();
                }
              };
            }();
            TableManageButtons3.init();
            /*var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true,

            });  */
  }
  function abrir_modal_add_division(img,id_colegio)
  {
    
  }
</script>

<div class="wrapper wrapper-content animated fadeInRight" id="" >
    <div class="row">
        <div class="col-lg-12 " id="usuarios_borrar">
            <div class="ibox collapsed float-e-margins">
                <div class="ibox-title collapse-link" id="tituloaltaevento">
                    <h5>Clubes</h5>
                        <div class="row">  
                              <button class="btn btn-sm btn-primary pull-right m-t-n-xs" id="altaeventohome" onclick="form_alta_colegio()" type="button" style="margin-right: 20px"><i class="glyphicon glyphicon-plus "></i>Crear Club<strong></strong></button>
                      </div>
                    </div>

                    <div class="ibox-content" style="display:block">
                      <table id="datatable-nodos2" class="table table-striped table-bordered dt-responsive nowrap " width="100%">
                            <thead>
                                <tr>
                                  <th colspan="2">Nombre</th>
                                 <!--  <th>Ciclo Lectivo</th>
                                  <th>Plan Estudio</th> -->
                                  
                                 
                                  <th colspan="1" style="">Acciones</th>
                                </tr>
                              </thead>
                              <tbody id="tbody_colegios">
                                <? 
                                  //print_r($colegios_all);
                                  foreach ($colegios_all as $colegio ) 
                                  {
                                    //echo $colegio->id.'<br>';
                                    ?>
                                      <tr colspan="4" id="<? echo $colegio->id;?>">
                                        <th><? echo $colegio->nombre;?></th>
                                       <!--  <th><? echo $colegio->cicloa;?></th>
                                        <th><? echo $colegio->planestudio;?></th> -->
                                        <th colspan="2"><button  class="btn btn-sm btn-info  m-t-n-xs" id="button_add_docente_<? echo $colegio->id;?>" title="Agregar division" onclick="abrir_modal_add_division(this, <? echo $colegio->id;?>)" type="button"><i class="glyphicon glyphicon-plus"></i></button></th>
                                      </tr>
                                    <?
                                  }
                                  ?>
                              </tbody>



                          </table>

                        <div class="form-group ">
	                    	
	                    </div>
	                     
		                 
		                
				                
		                <div id="materias_colegio_anio">
		                    	
		                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal alta colegio-->
<div class="modal" id="modal_alta_colegio" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Agregar Colegio</h4>
      </div>
      <form enctype="multipart/form-data"  id="alta_agregar_colegio" autocomplete="on" method="post" class="form-horizontal form-label-left">
      <div class="modal-body">
        
          <input type="hidden" id="id_colegio" name="id_colegio" value="" />
          <div class="form-group">
                  <label class="col-sm-2 control-label">Nombre</label>
                  <div class="col-sm-8"><input type="text" class="form-control" name="nombre" id="nombre">
                  </div>
              </div>
                         
              
              <div class="form-group">
                <label class="col-sm-2 control-label">Ciclo Lectivo</label>

                <div class="col-sm-8"><select name="ciclo_lectivo">
                            <?
                            foreach ($ciclos_lectivos as $ciclo) 
                            {
                              ?>
                                <option><? echo $ciclo->nombre ?></option>
                              <?
                            }
                            ?>
                                
                          </select>
                        </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Plan de Estudio</label>

                <div class="col-sm-8"><select name="plan_estudio">
                            <?
                            foreach ($planes_estudio as $plan) 
                            {
                              ?>
                                <option><? echo $plan->nombre ?></option>
                              <?
                            }
                            ?>
                                
                          </select>
                        </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Tipo</label>

                <div class="col-sm-8"><select name="tipo">
                            <?
                            foreach ($tipos_colegios as $tipo) 
                            {
                              ?>
                                <option><? echo $tipo->nombre ?></option>
                              <?
                            }
                            ?>
                                
                          </select>
                        </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Foto</label>

                <div class="col-sm-8"><input type="file" id="foto" name="foto"class="form-control"></div>
              </div>
              <div class="form-group">
                                            <label class="col-sm-2 control-label">Aplica Areas <br/><small class="text-navy"></small></label>

                                            <div class="col-sm-8">
                                                  <div class="i-checks"><label> <input type="radio" value="1" name="area" id="radio_masculino">  <i></i> Si </label></div>
                                                  <div class="i-checks"><label> <input type="radio" checked="" value="0" name="area" id="radio_femenino"> <i></i> No </label></div>
                                            </div>
              </div>
              
        
        </div>
        <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                
                 <button class="btn btn-primary" id ="guardar" >Agregar</button>
      </form>
      </div>
    </div>
  </div>
</div>
<!-- modal crear divisiones-->
<div class="modal" id="modal_add_docente_materia" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Divisiones</h4>
          </div>
          <center>
          <form id="form_hijos" autocomplete="on" method="post" class="form-horizontal form-label-left">
            <input type="hidden" id="user_id_edit" name="user_id_edit" value="0" />
            <div class="modal-body">
        
                <input type="hidden" id="permiso_eliminar_docentens_modal" name="permiso_eliminar_docentens_modal" value="0" />
                <input type="hidden" id="id_colegio_modal_hijos" name="id_colegio_modal_hijos" value="0" />
              <div id="div_select_docentes" >  

                  <div class="form-group" id="">
                      <label class="col-sm-2 control-label">Seleccione el Docente</label>
                      <div class="col-sm-8" id="div_col_select_docentes">
                        
                      </div>
                  </div>
                  <div class="form-group" id="div_button_asigna_docente">
                   
                  </div>

              </div>
          <table class="table table-striped table-bordered dt-responsive nowrap" id="">
            <thead>
              <th>Nombre</th>
              <th>Nombre</th>
              <th>DNI</th>
              
            </thead>
            <tbody id="tbody_docentes_cargados">
                                                          
            </tbody>
          </table>
            </div>
            <div class="modal-footer">

                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                
                
          </form>
          </center>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function()  
{
    $("#guardar").click(function()
    { 
                          $('#alta_agregar_colegio').validate
                          ({
                              rules: {
                                  nombre: { required: true},
                                  
                                    
                                  
                              },
                              messages: {
                                  nombre:  {required: "Debe introducir el Nombre."}, 
                                 
                                  
                              },
                              submitHandler: function(form)
                              {
                                //alert('hola2');
                                var formData = new FormData($("#alta_agregar_colegio")[0]);
                                var url = '<?=base_url("/index.php/colegios/registrar_colegio")?>';   
                                 
                                  $.ajax
                                  ({
                                          url: url,
                                          type: 'POST',
                                          cache: false,
                                          contentType: false,
                                          processData: false,
                                        //  data: $('#alta_agregar_colegio').serialize(),
                                          data: formData,
                                          success: function(data)
                                          {
                                              //alert('entro');
                                                //console.log(data);

                                                var data = jQuery.parseJSON( data );
                                              $("#modal_alta_colegio").modal('hide');
                                              if(data.status == 1)
                                              {
                                                bootbox.alert({
                                                title: "Colegio Registrado ",
                                                          message: '<h3><center>'+data.message+'</center></h3>',
                                                          size: 'small'
                                                         });

                                               // console.log(data.colegio);
                                                var colegio = data.colegio;
                                                console.log (colegio.ael);
                                                $id_colegio = colegio.id;
                                                filas =  '<tr id="'+id_colegio+'"><td>'+colegio.nombre+'</td>'+'<td>'+colegio.cicloa+'</td>'+'<td>'+colegio.planestudio+'</td>'+'<td></td>'+'</tr>';

                                  
                                               // $('#tbody_colegios').append(filas);
                                               $('#datatable-nodos2').dataTable().fnAddData( [
                                                  colegio.nombre,
                                                  colegio.cicloa,
                                                  colegio.planestudio,
                                                  " " ] );
                                                 //reload_table();
                                                 //generar_datatable_menuacciones();
                                              }
                                                
                                                  
                                              
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                            // document.getElementById("myModalLabel").innerHTML = 'Registro de Usuarios';
                                                  //document.getElementById("mensaje").innerHTML = 'ERROR';
                                                  //$("#myModal").modal('show');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }
                                  });
                              }
                          });
    })
});
</script>