﻿<?php 
 
class Materias extends CI_Controller {	
      
	function __construct(){
		parent::__construct();
			
		if(!$this->session->userdata("logged_in")){
               redirect("login");
         }
	}		

    //-------------------------------------------------------

    public function index(){ 

	    $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		//$data['urlApi'] = $urlApi;
		//$data['passApiKey'] = $passApiKey;
		$data['idusuario'] = $this->session->userdata('idusuario');
		
		

		$band = 0;
		$controlador = "Materias";
		$metodo = "index";
		$accion1 = "add_materia";

        $colegios = $this->session->userdata("colegios"); 
        $colegiosPermi = array();
        $acciones = array();
        $banAddMat = 0;
		foreach ($colegios as $cole)
		{
			$idcolegio = $cole["id_colegio"];
			$menus = $cole["menu"];
		 	foreach ($menus as $menu)
			{	
				if( ($menu->controlador === $controlador) && ($menu->metodo === $metodo))
				{
				    $band = 1;

				    $key = array_search($idcolegio, array_column($colegiosPermi, 'idcole'));

				    if(!$key){
					    $arre = array(
					    	'idcole'=>$idcolegio,
					    	'namecole'=>$cole["nombre_colegio"],
					    	'idrol'=>$cole["id_grupo"],
					    	'namerol'=>$cole["rol"],
					    );
					    $colegiosPermi[]=$arre;
					}
				}
			}

			
			foreach ($menus as $menu)
			{	
				if( $menu->nombre === $accion1)
				    $banAddMat = 1;
			}

			$data['anios'][$idcolegio]=$this->get_anios_x_colegio_curl($urlApi, $passApiKey, $data['idusuario'], $idcolegio, $cole["rol"]);
			
		}

		if($banAddMat == 1){
			/*$arre2 = array(
		    	'accion'=>$accion1,
		    );*/
		    $acciones[]=$accion1;
		}


		
        if($band == 1)
        {
    		$data['colegios'] = $colegiosPermi;
    		$data['acciones'] = $acciones;
    		
			$data['nombresmaterias']=$this->get_nombres_materias_curl($urlApi, $passApiKey, $data['idusuario']);
		    $data['contenido'] = 'admin/confMaterias';
			$this->load->view('include/template_colegio',$data);
				
		}
		else redirect(404);
		
	}	
		
   
	//---------------------------------------- CURL request to API -------------------------------------------

    public function get_anios_x_colegio_curl($urlApi, $passApiKey, $idUser, $idcolegio, $nombregrupo)  
    {
    	$url = $urlApi."/anio/obtener_anios_x_colegio/idcolegio/".$idcolegio."/nombregrupo/".$nombregrupo;

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			//CURLOPT_POST => true,
			//CURLOPT_POSTFIELDS => array('rol' => $rol,'id_colegio' =>$idcolegio),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);
		$response = json_decode($response);

		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}


	public function get_nombres_materias_curl($urlApi, $passApiKey, $idUser)  
    {
    	$url = $urlApi."/materia/nombres_materia";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			//CURLOPT_POST => true,
			//CURLOPT_POSTFIELDS => array('rol' => $rol,'id_colegio' =>$idcolegio),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);
		$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}


	public function get_materias_x_anio_curl($urlApi, $passApiKey, $idUser, $idcolegio, $nodoanioid)  
    {
    	$url = $urlApi."/materia/anios_materia/idcolegio/".$idcolegio."/nodoanioid/".$nodoanioid;

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			//CURLOPT_POST => true,
			//CURLOPT_POSTFIELDS => array('rol' => $rol,'id_colegio' =>$idcolegio),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);
		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}


	public function add_materias_x_anio_curl($urlApi, $passApiKey, $idUser, $colegioId, $nodoanioId, $materiasId)  
    {
    	$url = $urlApi."/materia/materias_anio/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('colegioId'=>$colegioId, 'nodoanioId'=>$nodoanioId, 'materiasId'=>$materiasId),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);
		//$response = json_decode($response);
		//print_r($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}


	public function delete_aniomat_curl($urlApi, $passApiKey, $idUser, $idaniomat)  
    {
    	$url = $urlApi."/materia/delete_aniomateria/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('idaniomat'=>$idaniomat),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);
		//$response = json_decode($response);
		//print_r($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}


	public function alta_materia_curl($urlApi, $passApiKey, $idUser, $materiaName)  
    {
    	$url = $urlApi."/materia/materia/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('materiaName'=>$materiaName),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);
		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	public function rol_docente_colegio_curl($urlApi, $passApiKey, $idUser,$idcolegio)
	{
		$url = $urlApi."/group/get_id_rol_docente/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('idcolegio'=>$idcolegio),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);
		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	public function get_docentes_colegio_curl($urlApi, $passApiKey, $idUser,$idcolegio, $id_group)
	{
		$url = $urlApi."/usuario/obtener_usuarios_rol/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('id_rol'=>$id_group),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		//print_r($response);
		//die();
		$response = substr($response, 3);
		//$response = json_decode($response);
		//return json_decode($response);
		return $response;
	}
	public function get_dvisiones_colegio_rol($urlApi, $passApiKey, $idUser, $idcolegio, $namerol)
	{
		$url = $urlApi."/division/obtener_divisiones_x_colegio/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('idcolegio'=>$idcolegio,'nombregrupo'=>$namerol),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		//print_r($response);
		//die();
		//$response = substr($response, 3);
		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	public function get_docentes_x_division_curl($urlApi, $passApiKey, $idusuario, $idcolegio, $iddivision, $idmateria)
	{
		$url = $urlApi."/division/obtener_docentes_x_div_colegio/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('id_colegio'=>$idcolegio,'id_division'=>$iddivision,'id_materia'=>$idmateria),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);
		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	public function asignar_docente_materia_curl($urlApi, $passApiKey, $idusuario, $idcolegio, $iddivision, $idmateria, $id_docente)
	{
		$url = $urlApi."/materia/asignar_doc_div_mat/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('id_colegio'=>$idcolegio,'id_division'=>$iddivision,'id_materia'=>$idmateria, 'id_docente' => $id_docente),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		//print_r($response);
		//$response = substr($response, 3);
		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	public function borrar_doc_div_mat_curl($urlApi, $passApiKey, $idusuario, $idcolegio, $iddivision, $idmateria, $id_docente)
	{
		$url = $urlApi."/materia/borrar_doc_div_mat/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('id_colegio'=>$idcolegio,'id_division'=>$iddivision,'id_materia'=>$idmateria, 'id_docente' => $id_docente),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		//print_r($response);
		//$response = substr($response, 3);
		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	public function get_docentes_x_div_mat_curl($urlApi, $passApiKey, $idusuario, $idcolegio, $iddivision, $idmateria)
	{
		$url = $urlApi."/materia/get_docentes_x_div_mat/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('id_colegio'=>$idcolegio,'id_division'=>$iddivision,'id_materia'=>$idmateria),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		//print_r($response);
		//$response = substr($response, 3);
		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	public function get_alumnos_x_div_curl($urlApi, $passApiKey, $idusuario, $idcolegio, $iddivision)
	{
		$url = $urlApi."/materia/get_alumnos_x_div/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('id_colegio'=>$idcolegio,'id_division'=>$iddivision),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		//print_r($response);
		//$response = substr($response, 3);
		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	public function get_alumnos_x_doc_div_mat_curl($urlApi, $passApiKey, $idusuario, $idcolegio, $iddivision,$id_materia, $id_docente)
	{
		$url = $urlApi."/materia/get_alumnos_x_doc_div_mat/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('id_colegio'=>$idcolegio,'id_division'=>$iddivision, 'id_materia' => $id_materia, 'id_docente' => $id_docente ),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		//print_r($response);
		//$response = substr($response, 3);
		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}




	//---------------------------------------- AJAX request from WEB -------------------------------------------

	public function get_materias_x_anio_ajax(){ 

	    $urlApi = $this->variables->get_urlapi(); 
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');
		
		$idcolegio = $_GET['idcolegio'];
        $nodoanioid = $_GET['nodoanioid'];
		

		$result=$this->get_materias_x_anio_curl($urlApi, $passApiKey, $idusuario, $idcolegio, $nodoanioid);
		
		if( (isset($result->status)) && ($result->status=='') ){
			$arre = array(
				'status' => 0,
             	'message' => 'No se encontraron materias'
			);
			$arre=json_encode($arre);
			print_r($arre);
		}
		else 
			print_r($result);
	}	
	

	public function add_materias_x_anio_ajax(){ 

	    $urlApi = $this->variables->get_urlapi(); 
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');
		
		$idcolegio = $_POST['colegio'];
        $idanio = $_POST['anio'];
        $materias = $_POST['materias'];

       
		$result=$this->add_materias_x_anio_curl($urlApi, $passApiKey, $idusuario, $idcolegio, $idanio, $materias);
		//print_r($result);

		if( (isset($result->status)) && ($result->status=='') ){
			$arre = array(
				'status' => 0,
             	'message' => 'No se encontraron materias'
			);
			$arre=json_encode($arre);
			print_r($arre);
		}
		else 
			print_r($result);
	}	


	public function delete_aniomat_ajax(){ 

	    $urlApi = $this->variables->get_urlapi(); 
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');
		
		$idaniomat = $_POST['idaniomat'];
        
       
		$result=$this->delete_aniomat_curl($urlApi, $passApiKey, $idusuario, $idaniomat);
		//print_r($result);

		if( (isset($result->status)) && ($result->status=='') ){
			$arre = array(
				'status' => 0,
             	'message' => $result->message
			);
			$arre=json_encode($arre);
			print_r($arre);
		}
		else 
			print_r($result);
	}	

	public function alta_materia_ajax(){ 

	    $urlApi = $this->variables->get_urlapi(); 
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');
		
		$materia = $_POST['nombremat'];
        

       
		$result=$this->alta_materia_curl($urlApi, $passApiKey, $idusuario, $materia);


		if( (isset($result->status)) && ($result->status=='') ){

			if($result->message === 'existe')                                               
                $mensaje = "La materia ya se encuentra registrada";
            else 
              if($result->message === 'vacio')  
                  $mensaje = "Ingrese un nombre de materia";
              else 
                if($result->message === 'error_alta') 
                    $mensaje = "ERROR al dar de alta: intente nuevamente";
                else 
                    $mensaje = "nada";

			$arre = array(
				'status' => 0,
             	'message' => $mensaje
			);
			$arre=json_encode($arre);
			print_r($arre);
		}
		else {
			$arre = array(
				'status' => 1,
				'id' => $result->id,
             	'message' => "Materia Registrada"
			);
			$arre=json_encode($arre);
			print_r($arre);
		}
	}
	public function get_materias_x_anio_2_ajax(){ 

	    $urlApi = $this->variables->get_urlapi(); 
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');//
		
		$idcolegio = $_GET['idcolegio'];
        $nodoanioid = $_GET['nodoanioid'];
		$iddivision = $_GET['id_division'];

		$result=$this->get_materias_x_anio_curl($urlApi, $passApiKey, $idusuario, $idcolegio, $nodoanioid);
		
		if( (isset($result->status)) && ($result->status=='') ){
			$arre = array(
				'status' => 0,
             	'message' => 'No se encontraron materias'
			);
			$arre=json_encode($arre);
			print_r($arre);
		}
		else 
		{
			$array_resul = array();		
			foreach (json_decode($result) as $key ) 
			{

				$get_docentes = $this->get_docentes_x_division_curl($urlApi, $passApiKey, $idusuario, $idcolegio, $iddivision,$key->id);
				$get_docentes = json_decode($get_docentes);

				


				//$get_docentes_escuela = $this->get_docentes_x_escuela_curl($urlApi, $passApiKey, $idusuario, $idcolegio, $iddivision,$key->id);
			//	$get_docentes_escuela = json_decode($get_docentes_escuela);
				//print_r($get_docentes->cant_docentes);
				$string_docentes = $get_docentes->string_docentes;
				if($get_docentes->cant_docentes > 0)
				{
					$bandera_docentes = 1;
					$bandera_alumnos = 0;
				}
				else
				{
					$bandera_docentes = 0;
					$bandera_alumnos = 0;
				}
				$arrayName = array(
					'id' => $key->id, //id_materia_con todas las relaciones
					'nombre' =>$key->nombre,
					'idmateria' =>$key->idmateria,//id nombre de materia
					'bandera_docentes' =>$bandera_docentes,
					'bandera_alumnos' =>$bandera_alumnos,
					'sql' => $get_docentes->sql,
					'iddivision' => $iddivision,
					'string_docentes' =>$string_docentes,
					'cant_docentes_materia' =>$get_docentes->cant_docentes
					

					//'docentes' => json_encode($get_docentes) ,
					//'idusuario' => $idusuario
				);
				$array_resul[] =$arrayName;
				
			}
			
			$array_resul=json_encode($array_resul);
			print_r($array_resul);
			//print_r($result);
		}
	}	
	public function docentes_division(){ 

	    $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		//$data['urlApi'] = $urlApi;
		//$data['passApiKey'] = $passApiKey;
		$data['idusuario'] = $this->session->userdata('idusuario');
		
		$idusuario = $this->session->userdata('idusuario');
		
		//$idcolegio = $_POST['colegio'];
       // $idanio = $_POST['anio'];
      //  $materias = $_POST['materias'];

       
		
		

		$band = 0;
		$controlador = "Materias";
		$metodo = "docentes_division";
		$accion1 = "add_docente_division";

        $colegios = $this->session->userdata("colegios"); 
        $colegiosPermi = array();
        $acciones = array();
        $banAddMat = 0;
		foreach ($colegios as $cole)
		{
			$idcolegio = $cole["id_colegio"];
			$menus = $cole["menu"];
			 $menu_encontrado = false;
		 	foreach ($menus as $menu)
			{	
				if( ($menu->controlador === $controlador) && ($menu->metodo === $metodo))
				{
				    $band = 1;
				    $menu_encontrado = true;
					    //echo "permiso de ver <br>";
				    $key = array_search($idcolegio, array_column($colegiosPermi, 'idcole'));

				    if(!$key)
				    {
					    $arre = array(
					    	'idcole'=>$idcolegio,
					    	'namecole'=>$cole["nombre_colegio"],
					    	'idrol'=>$cole["id_grupo"],
					    	'namerol'=>$cole["rol"],
					    );
					    $colegiosPermi[]=$arre;

					}
				}
			}
			if($menu_encontrado == true)
			{
			//	$divisiones = $this->get_dvisiones_colegio_rol($urlApi, $passApiKey, $idusuario, $idcolegio, $cole["rol"]);
				$puede_editar = false;
				foreach ($menus as $menu)
				{	
					if( $menu->nombre === $accion1)
					{
						//echo "permiso de editar <br>";
						$banAddMat = 1;
						$puede_editar = true;
					}
					    
				}
				if($puede_editar == true)
				{
					
					$accion_cole  = array(
								'idcole'=>$idcolegio,
						    	'namecole'=>$cole["nombre_colegio"],
						    	'idrol'=>$cole["id_grupo"],
						    	'namerol'=>$cole["rol"], 
						    	'editar' => 1,
						    //	'divisiones' => $divisiones
						    	
						    	);
					$acciones[]=$accion_cole;
				}
				else
				{
					$accion_cole  = array(
								'idcole'=>$idcolegio,
						    	'namecole'=>$cole["nombre_colegio"],
						    	'idrol'=>$cole["id_grupo"],
						    	'namerol'=>$cole["rol"], 
						    	'editar' => 0,
						    //	'divisiones' => $divisiones
						    	);
					$acciones[]=$accion_cole;
				}
			}
			//print_r($acciones);
			//die();
			//$data['anios'][$idcolegio]=$this->get_anios_x_colegio_curl($urlApi, $passApiKey, $data['idusuario'], $idcolegio, $cole["rol"]);
			
		}
		//print_r($acciones);
		if($banAddMat == 1){
			/*$arre2 = array(
		    	'accion'=>$accion1,
		    );*/
		    
		}


		
        if($band == 1)
        {
    		$data['colegios'] = $colegiosPermi;
    		$data['acciones'] = $acciones;
    		
			$data['nombresmaterias']=$this->get_nombres_materias_curl($urlApi, $passApiKey, $data['idusuario']);
		    $data['contenido'] = 'panel/materias/docentes_division';
			$this->load->view('include/template_colegio',$data);
				
		}
		else redirect(404);
		
	}
	public function docentes_division_traer_divisiones_colegio_ajax(){ 

	    $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		$data['idusuario'] = $this->session->userdata('idusuario');
		$idusuario = $this->session->userdata('idusuario');
		$accion1 = "add_docente_division";
        $colegios = $this->session->userdata("colegios"); 
		foreach ($colegios as $cole)
		{
			$idcolegio = $cole["id_colegio"];
			if($idcolegio == $_GET['idcolegio'])
			{
				$menus = $cole["menu"];
				$menu_encontrado = false;
				$divisiones = $this->get_dvisiones_colegio_rol($urlApi, $passApiKey, $idusuario, $idcolegio, $cole["rol"]);
				$puede_editar = false;
				foreach ($menus as $menu)
				{	
					if( $menu->nombre === $accion1)
					{
					
						$puede_editar = true;
					}
					    
				}
				if($puede_editar == true)
				{
					$get_id_rol_docente = $this->rol_docente_colegio_curl($urlApi, $passApiKey, $idusuario,$idcolegio);
					$get_id_rol_docente = json_decode($get_id_rol_docente);

					$get_docentes_colegio = $this->get_docentes_colegio_curl($urlApi, $passApiKey, $idusuario,$idcolegio, $get_id_rol_docente->id_rol );
					$get_docentes_colegio = json_decode($get_docentes_colegio);
					
					$accion_cole  = array(
								'idcole'=>$idcolegio,
						    	'namecole'=>$cole["nombre_colegio"],
						    	'idrol'=>$cole["id_grupo"],
						    	'namerol'=>$cole["rol"], 
						    	'editar' => 1,
						    	'divisiones' => $divisiones,
						    	'id_group_docente' => $get_id_rol_docente->id_rol,
						    	'docentes' =>  $get_docentes_colegio
						    	);
					
				}
				else
				{
					$accion_cole  = array(
								'idcole'=>$idcolegio,
						    	'namecole'=>$cole["nombre_colegio"],
						    	'idrol'=>$cole["id_grupo"],
						    	'namerol'=>$cole["rol"], 
						    	'editar' => 0,
						    	'divisiones' => $divisiones
						    	);
					
				}
				echo json_encode($accion_cole);
				break;
			}
			
		}
		


		
       
		
	}
	public function get_docentes_x_mat_x_div_ajax()
	{
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		
		$data['idusuario'] = $this->session->userdata('idusuario');
		
		$idusuario = $this->session->userdata('idusuario');
		$id_colegio = $_GET["id_colegio"];
		$id_materia = $_GET["id_materia"];
		$id_division = $_GET["id_division"];

		$get_docentes = $this->get_docentes_x_division_curl($urlApi, $passApiKey, $idusuario, $id_colegio, $id_division,$id_materia);
				$get_docentes = json_decode($get_docentes);
		//print_r($get_docentes);
		echo json_encode($get_docentes);



	}
	public function asignar_docente_materia_ajax()
	{
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		
		$data['idusuario'] = $this->session->userdata('idusuario');
		
		$idusuario = $this->session->userdata('idusuario');
		$id_colegio = $_GET["id_colegio"];
		$id_materia = $_GET["id_materia"];
		$id_division = $_GET["id_division"];
		$id_docente = $_GET["id_docente"];
		$request = $this->asignar_docente_materia_curl($urlApi, $passApiKey, $idusuario, $id_colegio, $id_division,$id_materia,$id_docente);
		print_r($request);
		//echo json_encode($request);
	}
	public function borrar_doc_div_mat_ajax()
	{
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		
		$data['idusuario'] = $this->session->userdata('idusuario');
		
		$idusuario = $this->session->userdata('idusuario');
		$id_colegio = $_GET["id_colegio"];
		$id_materia = $_GET["id_materia"];
		$id_division = $_GET["id_division"];
		$id_docente = $_GET["id_docente"];
		$request = $this->borrar_doc_div_mat_curl($urlApi, $passApiKey, $idusuario, $id_colegio, $id_division,$id_materia,$id_docente);
		print_r($request);
	}
	public function get_docentes_divsion_materia_ajax()
	{
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		
		$data['idusuario'] = $this->session->userdata('idusuario');
		
		$idusuario = $this->session->userdata('idusuario');
		$id_colegio = $_GET["id_colegio"];
		$id_materia = $_GET["id_materia"];
		$id_division = $_GET["id_division"];
		
		$request = $this->get_docentes_x_div_mat_curl($urlApi, $passApiKey, $idusuario, $id_colegio, $id_division,$id_materia);
		print_r($request);
	}
	public function get_alumnos_x_div_ajax()
	{
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		
		$data['idusuario'] = $this->session->userdata('idusuario');
		
		$idusuario = $this->session->userdata('idusuario');
		$id_colegio = $_GET["id_colegio"];
		
		$id_division = $_GET["id_division"];
		
		$request = $this->get_alumnos_x_div_curl($urlApi, $passApiKey, $idusuario, $id_colegio, $id_division);
		print_r($request);
	}
	public function get_alumnos_x_doc_div_mat_ajax()
	{
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		
		$data['idusuario'] = $this->session->userdata('idusuario');
		
		$idusuario = $this->session->userdata('idusuario');
		$id_colegio = $_GET["id_colegio"];
		
		$id_division = $_GET["id_division"];
		$id_materia = $_GET["id_materia"];
		$id_docente = $_GET["id_docente"];
		$request = $this->get_alumnos_x_doc_div_mat_curl($urlApi, $passApiKey, $idusuario, $id_colegio, $id_division,$id_materia, $id_docente);
		print_r($request);


	}
	public function save_doc_div_mat_alu_ajax()
	{
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		
		$data['idusuario'] = $this->session->userdata('idusuario');
		
		$idusuario = $this->session->userdata('idusuario');
		//print_r($_POST);
		$id_colegio = $_POST["modal_add_alumno_id_colegio"];
		
		$id_division = $_POST["modal_add_alumno_id_division"];
		$id_materia = $_POST["modal_add_alumno_id_materia"];
		$id_docente = $_POST["select_docentes_div_mat"];

		//print_r($_POST["checkbox_alumno_14745_1_129"]);
		//hacer curl
		$url = $urlApi."/materia/save_doc_div_mat_alu/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
			CURLOPT_POST => true,
			//CURLOPT_POSTFIELDS => array('datos'=>$_POST),
			CURLOPT_POSTFIELDS => $_POST,
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		//print_r($response);
		//$response = substr($response, 3);
		//$response = json_decode($response);
		$response = json_decode($response);
		print_r($response);
		//fin curl
		
	}
	public function migrar_horarios_a_doc_div_mat()
	{
		//die();
		$sql = "SELECT * FROM `horariosmaterias` where planesestudio_id = 3";
        $query = $this->db->query($sql);
        foreach ($query->result() as $key) 
        {
        	//print_r($key);
        	$sql_verificar = "SELECT * FROM nodo_doc_div_mat_alu 
        	where id_docente = ".$key->docente_id." and id_division	 = ".$key->divisiones_id." and id_materia = ".$key->materias_id;
        	//print_r($sql_verificar);
        	//break;
        	$sql_verificar = $this->db->query($sql_verificar);
        	if($sql_verificar->num_rows() > 0)
        	{

        	}
        	else
        	{
        		$arrayName = array(
        			'id_docente' => $key->docente_id, 
        			'id_division' => $key->divisiones_id, 
        			'id_materia' => $key->materias_id, 
        			'id_alumno' => 0, 
        			'id_colegio' => $key->colegio_id, 
        			'estado' => 1, 
        			'ciclo_lectivo' => 2019, 
        		);
        		$query2 = $this->db->insert("nodo_doc_div_mat_alu",$arrayName);
        		//cargar alumnos
        		/*$sql_inscripciones = "SELECT * FROM `inscripciones` where divisiones_id = ".$key->divisiones_id." and cicloa = 2019";
        		$inscripciones = $this->db->query($sql_inscripciones);
        		foreach ($inscripciones->result() as $key2 ) 
        		{
        			$arrayName = array(
	        			'id_docente' => $key->docente_id, 
	        			'id_division' => $key->divisiones_id, 
	        			'id_materia' => $key->materias_id, 
	        			'id_alumno' => $key2->alumno_id, 
	        			'id_colegio' => $key->colegio_id, 
	        			'estado' => 1, 
	        			'ciclo_lectivo' => 2019, 
	        		);
	        		$query = $this->db->insert("nodo_doc_div_mat_alu",$arrayName);
        		}*/
	        		

        	}
        }
        foreach ($query->result() as $key) 
        {
        	$sql_inscripciones = "SELECT * FROM `inscripciones` where divisiones_id = ".$key->divisiones_id." and cicloa = 2019";
        		$inscripciones = $this->db->query($sql_inscripciones);
        		foreach ($inscripciones->result() as $key2 ) 
        		{
        			$sql_verificar = "SELECT * FROM nodo_doc_div_mat_alu 
		        	where id_docente = ".$key->docente_id." and id_division	 = ".$key->divisiones_id." and id_materia = ".$key->materias_id." and id_alumno = ".$key2->alumno_id." and ciclo_lectivo = 2019";
		        	//print_r($sql_verificar);
		        	//break;
		        	$sql_verificar = $this->db->query($sql_verificar);
		        	if($sql_verificar->num_rows() > 0)
		        	{

		        	}
		        	else
		        	{



	        			$arrayName = array(
		        			'id_docente' => $key->docente_id, 
		        			'id_division' => $key->divisiones_id, 
		        			'id_materia' => $key->materias_id, 
		        			'id_alumno' => $key2->alumno_id, 
		        			'id_colegio' => $key->colegio_id, 
		        			'estado' => 1, 
		        			'ciclo_lectivo' => 2019, 
		        		);
		        		$query = $this->db->insert("nodo_doc_div_mat_alu",$arrayName);
		        	}
        		}
        }

        
	}

	 
} // fin Controller Alumno.