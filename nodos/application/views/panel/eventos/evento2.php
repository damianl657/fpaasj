<script type="text/javascript">
	
	 $('[data-toggle="tooltip"]').tooltip();
	 
</script>

<!-- 2 Posteo Con Permiso PARA DARLE ACPTAR O DECLINAR-->
			<div class="bloque-info posteo_<?php echo $eventoid;?>" >
				
				<div class="bloque-info-header">
					<div class="row">
						<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 text-left">
							<div class="avatar">
								<img src="<?php echo $foto;?>" alt=""/>
							</div>
							<div class="hijo">
								<h4><?php echo rawurldecode($apellido).' '.rawurldecode($nombre);?></h4>
								<div class="contfecha">
									<p class="fechaI">
										<span class="hora">Fecha de Inicio: <?php echo substr($fechaInicio,11,5).' hs';?></span>
										<span class="fecha"><?php echo substr($fechaInicio,8,2).'/'.substr($fechaInicio,5,2).'/'.substr($fechaInicio,0,4);;?></span>
									</p>
								</div>
								<p class="enviado-por">
									<i class="fa fa-share"></i>
									<span><?php echo rawurldecode($apellido_creador).' '.rawurldecode($nombre_creador);?></span> - <span><?php echo rawurldecode($nombre_grupo);?></span>
								</p>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-right">
							<img src="<?php echo $logo;?>" alt=""/>
						</div>
					</div>
				</div>
				<hr>
				<div class="bloque-info-main">
					<div class="contenido"><div class="contenido2">
						<h4 class="titulo"> <?php echo rawurldecode($titulo);?></h4>
						<!--Carusel Fotos  -->
				<?php 
				//print_r($urls_fotos);
				if(isset($urls_fotos))
				{

				?>
				<div class="carousel slide" id="carousel<?php echo $id;?>">
                                <ol class="carousel-indicators">
                                    
                                    <?php 
                                    $cant_img = 0;
                                    $active = 'active';
                                	foreach ($urls_fotos as $url_img) 
                                	{
                                		
	                                	?>
		                                    <li data-slide-to="<?php echo $cant_img;?>" data-target="#carousel<?php echo $id;?>" class="<?php echo $active;?>"></li>
	                                    <?php
	                                    $cant_img++;
	                                    $active = '';
                                    }
                                	?>
                                </ol>
                                <div class="carousel-inner">
                                	<?php 
                                	$active = 'active';
                                	foreach ($urls_fotos as $url_img) 
                                	{
                                		//$urlApi = $this->variables->get_urlapi();
                                		//print_r($url_img['carpeta']);
     $url_f = $this->variables->get_url_storage().$url_img['carpeta'].'/'.$url_img['id'].'_'.$url_img['nombre'];
                                		//echo $url_f;
	                                	?>
		                                    <div class="item <?php echo $active;?>">
		                                        <img alt="image" class="img-responsive" src="<?php echo $url_f?>">
		                                       
		                                    </div>
	                                    <?php
	                                    $active = '';
                                    }
                                	?>
                                    
                                </div>
                                <a data-slide="prev" href="#carousel<?php echo $id;?>" class="left carousel-control">
                                    <span class="icon-prev"></span>
                                </a>
                                <a data-slide="next" href="#carousel<?php echo $id;?>" class="right carousel-control">
                                    <span class="icon-next"></span>
                                </a>
                            </div>
                            <br>
                <?php }?>
				<!-- -->
						

						<div class="forzar_ul forzar_ol" ><?php echo $descripcion;
					//rawurldecode($descripcion);?></div>
					</div></div>
					<br>


					
					<!-- Row de Botones de permiso -->
					<div class="row" id="visto<?php echo $id;?>">
						<form action="#" id="form_evento<?php echo $id;?>" method="post" class="form-horizontal">
                  				<input type="hidden" value='["[<?php echo $id;?>]"]' name="eventosid"/>
                  				<input type="hidden" value="<?php echo $id;?>" name="eventousers_id"/>
                  				<input id="id_evento_megusta<?php echo $id;?>" type="hidden" value="<?php if($me_gusta == 1){echo 0;}else{echo 1;}?>" name="me_gusta"/>
						<input type="hidden" value="<?php echo $visto;?>" id="valorvisto<?php echo $id;?>" name="id"/> 
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<button type="button" id="declinar<?php echo $id;?>"class="btn2 btn-danger" ><i class="fa fa-close"></i> Declinar</button>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<button type="button" id="aceptar<?php echo $id;?>" class="btn2 btn-success" ><i class="fa fa-check"></i> Aceptar</button>
						</div>
						</form>
					</div>

					
				</div>

				<!--<div id='loading_vista_evento_<?php echo $eventoid;?>_<?php echo $id;?>'>
					<a href="#" class="ver_mas" id='vermas_<?php echo $eventoid;?>_<?php echo $id;?>' tittle="ver detalles"> <i class="fa fa-ellipsis-h"/> </a>
				</div>-->
				
				<!-- cantidad de adjuntos -->
				<?php if((int)$cantfiles > 0){?>
					<a href="#" class="adjuntos_ " id='adjuntos_<?php echo $eventoid;?>_<?php echo $id;?>' data-toggle="tooltip" title="ver archivos adjuntos"> <i class="fa fa-paperclip" /> (<?php echo $cantfiles; ?>&nbsp; archivos adjuntos) </a>
				<?php } else {?>
						<a href="#" class="ver_mas" id='adjuntos_<?php echo $eventoid;?>_<?php echo $id;?>' data-toggle="tooltip" title="ver detalles"> ver m&aacute;s </a>
				<?php } ?>	

				<hr>

				<div class="bloque-info-footer">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-left">
							<?php if($me_gusta == 1)
							{?>
								<a class="like" data-toggle="tooltip" title="me gusta" id="megusta<?php echo $id;?>"><i class="fa fa-heart"></i><i style="font-size: 10px;font-weight: bold;"><?php if($cant_me_gusta > 0){echo '&nbsp&nbsp '.$cant_me_gusta.' Me Gusta';}?></i> </a>
								
							<?php	
							}
							else
							{
							?>
								<a class="" data-toggle="tooltip" title="me gusta" id="megusta<?php echo $id;?>"><i class="fa fa-heart"></i><i style="font-size: 10px;font-weight: bold;"><?php if($cant_me_gusta > 0){echo '&nbsp&nbsp '.$cant_me_gusta.' Me Gusta';}?></i> </a>
								

							<?php
							} 
							?>
							<?php ?>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right" id='loading_vista_evento_<?php echo $eventoid;?>_<?php echo $id;?>'>
							<a href="#" class="ver_mas" id='vermas_<?php echo $eventoid;?>_<?php echo $id;?>' data-toggle="tooltip" title="ver detalles"> <i class="fa fa-ellipsis-h" /> </a>
						</div>
					</div>
					
				</div>

				<!-- <label>HOLAAAA2</label> -->

			</div>
			<!-- FIN Posteo Con Permiso -->
			<style type="text/css">
					.forzar_ul ul li {
  						list-style-type: disc;
						margin: 20px;
  						/*line-height: 1;*/
					}
					.forzar_ol ol li {
  						list-style-type: decimal;
  						margin: 20px;
  					/*	line-height: 1;*/
  						
					}
				</style>
  


