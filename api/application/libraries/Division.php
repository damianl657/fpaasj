<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    /**
     * Clase para almacenar funciones varias que tengan
     * que ver con operaciones con Division
     */
    class Division
    {
        private $CI; // Por si hace falta acceder desde algun metodo a la instancia global de CodeIgniter

        public function __construct()
        {
            $this->CI = get_instance();
        }

        /**
         * Busca la division en el ciclo actual de un alumno
         * @param  [type] $idalumno [idalumno encriptado]
         * @return [type]         [division del alumno]
         */
        function obtenerDivisionPorAlumno($idalumno)
        {
            $sql = "SELECT divisiones_id
                    FROM inscripciones
                    JOIN users_groups ON inscripciones.alumno_id = users_groups.id
                    JOIN nodocolegio ON nodocolegio.id = users_groups.colegio_id
                    
                    WHERE users_groups.user_id = $idalumno 
                    AND nodocolegio.cicloa = inscripciones.cicloa";  

                    //JOIN planesestudio ON planesestudio.id = inscripciones.cicloa
            $query = $this->CI->db->query($sql);

            if($query->num_rows()>0)return $query->row()->divisiones_id;
            else return 0;
        }
        
    }