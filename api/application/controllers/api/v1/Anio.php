<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';


    class Anio extends REST_Controller {

    function __construct()
    {
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        parent::__construct();
        $this->load->model("modelo_anio");
        $this->load->model("modelo_division");
        $this->load->model("Modelo_permisos");
       
        // Configurar limites para cada uno de los metodos, no solo controlador
        // La tabla limits tiene que estar creada y la opcion limits TRUE en application/config/rest.php
        
        //$this->methods['tutores_get']['limit'] = 1; // 500 peticiones por hora por usuario/key
        //$this->methods['user_post']['limit'] = 100; // 100 peticiones por hora por usuario/key
        //$this->methods['user_delete']['limit'] = 50; // 50 peticiones por hora por usuario/key
    }

    public function obtener_aniosBynodoniv_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $parametros = $_GET['parametros'] ;
            $todos = $_GET['todos'] ;
            $nombregrupo = $_GET['nombregrupo']; 
            $idcolegio = $_GET['idcolegio']; 
            
            //$anioA = date('Y');
            $anioA = $this->utilidades->obtener_planestudioaXcolegio( $idcolegio );  
            //$anioA = '2016';
            $planest_Id = $this->utilidades->get_planestudio( $anioA ); 

            $divis=array();

            $divisTo=array();

            if($todos == 1) //traer todos los años
            { 
                $nada="";  //POR AHORA NO SE USA
            }
            else
            {   
                //print_r($parametros);
                //die();
                $parametros = json_decode($parametros);

                foreach ($parametros as $row)
                { 
                    /*if($row[0]->idnodoesp != '-1')
                        $anio = $this->modelo_anio->get_anios_BynodonivANDnodoesp($row[0]->idnodonivel, $row[0]->idnodoesp);
                    else 
                        $anio = $this->modelo_anio->get_anios_BynodonivANDnodoespNull($row[0]->idnodonivel);*/

                    $anio = $this->modelo_anio->get_nodoanios($row[0]->idnodonivel);

                    $arre = array ( 'nombrenodonivel' => $row[0]->nombrenodonivel,
                                        'anios' => $anio
                                        );
                    $result[]=$arre;

                    foreach ($anio as $xx)
                    {
                        $divisTo[]=$xx->id; //con estos id traigo las divisiones
                    }
                }

                $idsAnios = join(',',$divisTo);
                $resultdivi = $this->modelo_division->get_divisionesXanios($idsAnios, $planest_Id);
                if($resultdivi){
                    foreach ($resultdivi as $key)
                    {
                        $divisionesTo[] = $key->id;
                    }                 
                    $idsDivi = join(',',$divisionesTo);
                    $resfinal['idsDiviTo']=$idsDivi;                    
                }


                //print_r($result);

                /*if( ($nombregrupo=='Institucion') || ($nombregrupo=='Directivo') || ($nombregrupo=='Nodos')|| ($nombregrupo=='Administrativo') || ($nombregrupo=='Gabinete') || ($nombregrupo=='Administrativo Gral') || ($nombregrupo == 'Secretaria Inicial') || ($nombregrupo == 'Secretaria Primaria'))              
                {   //todos los niveles del colegio
                    $resfinal['resul'] = $result;
                }*/
                $div_todas = $this->Modelo_permisos->get_permiso_div_todas($idcolegio,$nombregrupo);
                if($div_todas == true)
                {
                    $resfinal['resul'] = $result;
                }
                else if($nombregrupo=='Docente')
                     {
                        $this->load->model("modelo_division");
                        
                        $diviDoce = $this->modelo_division->divisionesXdocentes($idcolegio, $planest_Id, $userid);
                        if( count($diviDoce)>0)
                        {
                            foreach ($diviDoce as $key) {
                                $divis[] = $key->divisiones_id;
                            }
                            $divisiones = join(',',$divis);
                            $intersecDivis = array_intersect($divisionesTo, $divis);
                            $intersecDivis = join(',',$intersecDivis);
                            $resfinal['idsDiviTo']=$intersecDivis;
                            
                            $this->load->model("modelo_anio");
                            $anioDivi = $this->modelo_anio->aniosXdivisiones($divisiones);
                            //print_r($anioDivi);
                            
                            foreach ($result as $clave) 
                            {
                                foreach ($clave['anios'] as $arreanio) 
                                {
                                    foreach ($anioDivi as $key)
                                    {
                                        $aniopermitido = $key->anios_id;
                                   
                                        if( $arreanio->id == $aniopermitido )
                                        {
                                            $aniosfinal[]= $arreanio;
                                        }
                                    }
                                }

                                if( $aniosfinal != null)
                                {
                                    $arreaux = array ( 'nombrenodonivel' => $clave['nombrenodonivel'],
                                        'anios' => $aniosfinal
                                        );
                                    $resfinal['resul'][]= $arreaux;
                                    $aniosfinal = null;
                                }

                                $aniosfinal=null;
                            }
                        }
                     }
                    else if($nombregrupo=='Preceptor')
                         {
                            $this->load->model("modelo_division");
                            $diviPrece = $this->modelo_division->divisionesXpreceptores($idcolegio,  $planest_Id, $userid);
                            if( count($diviPrece)>0)
                            {
                                foreach ($diviPrece as $key) {
                                    $divis[] = $key->division_id;
                                }
                                $divisiones = join(',', $divis);
                                $intersecDivis = array_intersect($divisionesTo, $divis);
                                $intersecDivis = join(',',$intersecDivis);
                                $resfinal['idsDiviTo']=$intersecDivis;
                            
                                
                                $this->load->model("modelo_anio");
                                $anioDivi = $this->modelo_anio->aniosXdivisiones($divisiones);
                                //print_r($anioDivi);
                                
                                foreach ($result as $clave) 
                                {
                                    foreach ($clave['anios'] as $arreanio) 
                                    {
                                        foreach ($anioDivi as $key)
                                        {
                                            $aniopermitido = $key->anios_id;
                                       
                                            if( $arreanio->id == $aniopermitido )
                                            {
                                                $aniosfinal[]= $arreanio;
                                            }
                                        }
                                    }

                                    if( $aniosfinal  != null)
                                    {
                                        $arreaux = array ( 'nombrenodonivel' => $clave['nombrenodonivel'],
                                            'anios' => $aniosfinal 
                                            );
                                        $resfinal['resul'] []= $arreaux;
                                        $aniosfinal = null;
                                    }

                                    $aniosfinal=null;
                                }
                            }
                         }
                    //$resfinal = $result; //quitar esta linea
            }

            //print_r($resfinal);

            // Si existe mas de un resultado, lo mando
            if ($resfinal)
            {
                $data = json_encode($resfinal);
                //$data.= "**$nombrenodonivel";
                $this->response($data, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron anios'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }
   
    //--------------------------------------------------------------

    public function insert_nodoanio_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {     
            if($_POST['data']){
                //var_dump($_POST['data']);
                
                $idusergrupo = $_POST['idusergrupo']; 
                $data = json_decode($_POST['data']);  

                //$anioA = date('Y');
                $anioA = $this->utilidades->obtener_planestudioaXcolegio( $idcolegio );  
                //$anioA = '2016';
                $planest_Id = $this->utilidades->get_planestudio( $anioA ); 

                
                foreach ($data as $key ){ // recorrer los datos del  formulario $_POST
                    
                    if($key->checked == '1'){
                        $campos = array(
                                    'anio_id' => $key->anios_id,
                                    'nodoniv_id' => $key->nodoniv_id,
                                    'cicloa_id'=>$planest_Id,
                                    'estado' => '1'
                                );


                        $result = $this->modelo_anio->insertar_nodoanio($campos);
                    }else{
                        $campos = array(
                                    'anio_id' => $key->anios_id,
                                    'nodoniv_id' => $key->nodoniv_id,
                                    'cicloa_id'=>$planest_Id,
                                    'estado' => '0'
                                );
                        $result = $this->modelo_anio->eliminar_nodoanio($campos);                       
                    }
                    
                }// fin foreach*/
    
                //-- Actualizar al paso 3
                $this->load->model("modelo_usuario");
                $result3 = $this->modelo_usuario->update_pasos($idusergrupo,3); 

                if($result3){
                    $this->response([
                            'status' => TRUE,
                            'message' => 'Paso 3 Guardado', 
                        ], REST_Controller::HTTP_OK
                    );
                }else{
                    $this->response([
                     'status' => FALSE,
                     'message' => 'Error al Guardar Paso 3'
                    ]); // NOT_FOUND (404) being the HTTP response code
                }
                    
            }else{
                 $this->response([
                    'status' => FALSE,
                    'message' => 'Error al Guardar Paso 3'
                ]); // NOT_FOUND (404) being the HTTP response code 
            }
        } 
             
    } // fin function insert_nodoanio_post


    public function obtener_anios_x_colegio_get(){
        
        $userid = $this->utilidades->verifica_userid();
        $idcolegio = $this->get('idcolegio'); 
        $nombregrupo = $this->get('nombregrupo'); 
        $arre;


        $anioA = $this->utilidades->obtener_planestudioaXcolegio( $idcolegio );
      
        $planest_Id = $this->utilidades->get_planestudio( $anioA ); 
        
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {

            if( ($nombregrupo=='Institucion') || ($nombregrupo=='Directivo') || ($nombregrupo=='Nodos')|| ($nombregrupo=='Administrativo') || ($nombregrupo=='Gabinete') || ($nombregrupo=='Secretaria Inicial') || ($nombregrupo=='Secretaria Primaria'))              
            {   //todas las divisiones del colegio
                $result2 = $this->modelo_anio->get_anios_SinEspecialidad($idcolegio,$planest_Id);
                $result1 = $this->modelo_anio->get_anios_ConEspecialidad($idcolegio,$planest_Id);
                //$result = $this->modelo_division->get_divisiones_SinEspecialidad($idcolegio);
                //$result = array_merge($result2, $result1);

                if( ($result1)&&($result2) )
                    $result = array_merge($result2, $result1);
                else  if($result1)
                        $result = $result1;
                      else 
                        if($result2)
                            $result = $result2;

            }
            else $result=0;
            /*else if($nombregrupo=='Docente')
                {  
                    $this->load->model("modelo_division");
                    $diviDoce = $this->modelo_division->divisionesXdocentes($idcolegio,  $planest_Id, $userid);
                    foreach ($diviDoce as $key) {
                        $arre[] = $key->divisiones_id;
                    }
                    $divisiones = join(',',$arre);
                    
                    $result2 = $this->modelo_division->get_divisiones_SinEspecialidad_Xdivision($idcolegio, $divisiones,$planest_Id);
                    $result1 = $this->modelo_division->get_divisiones_ConEspecialidad_Xdivision($idcolegio, $divisiones,$planest_Id);
                    
                    if( ($result1)&&($result2) )
                            $result = array_merge($result2, $result1);
                        else  if($result1)
                                $result = $result1;
                              else 
                                if($result2)
                                    $result = $result2;
                 }
                 else if($nombregrupo=='Preceptor')
                     {
                        $this->load->model("modelo_division");
                        $diviPrece = $this->modelo_division->divisionesXpreceptores($idcolegio,  $planest_Id, $userid);
                        foreach ($diviPrece as $key) {
                            $arre[] = $key->division_id;
                        }
                        if($arre != null)
                        {
                            $divisiones = join(',',$arre);


                            $result2 = $this->modelo_division->get_divisiones_SinEspecialidad_Xdivision($idcolegio, $divisiones,$planest_Id);
                            $result1 = $this->modelo_division->get_divisiones_ConEspecialidad_Xdivision($idcolegio, $divisiones,$planest_Id);
                            
                            if( ($result1)&&($result2) )
                                $result = array_merge($result2, $result1);
                            else  if($result1)
                                    $result = $result1;
                                  else 
                                    if($result2)
                                        $result = $result2;
                        }
                            

                     }
                     //else echo "no entra";
                    */
                       
        
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
                $result = json_encode($result);
                //$result = utf8_decode($result);
                //print_r($result);
                //echo "<br><br>";
                $this->response($result, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No existen divisiones cargadas'
                ], REST_Controller::HTTP_OK); 
            }
        }
        
    }
    public function get_all_anios_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
           // print_r("aaa");
            $result = $this->modelo_anio->get_all_anios2();
            if($result->num_rows() > 0)
            {
               
                //$result = utf8_decode($result);
                //print_r($result);
                //echo "<br><br>";
               
                $data = array
                    ( 
                        'status' => 1,
                        'message' => 'ok',
                        'cant_anios' => $result->num_rows(),
                        'anios' => $result->result(),
                    );
                $data = json_encode($data);
                $this->response($data, REST_Controller::HTTP_OK); 
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => 1,
                    'message' => 'ok',
                    'cant_anios' => $result->num_rows(),
                    'anios' => $result->result(),
                ], REST_Controller::HTTP_OK); 
            }
        }
    }


} // fin Class Anio
