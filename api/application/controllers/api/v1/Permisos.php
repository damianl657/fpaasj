<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';


    class Permisos extends REST_Controller {

    function __construct()
    {
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        
        parent::__construct();
        $this->load->model("modelo_permisos");
       
    }


    public function obtener_permisos_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $idcolegio = $this->post('idcolegio'); 
            $idrol = $this->post('idrol'); 
            $campo = $this->post('campo'); 

            $reglas = $this->modelo_permisos->get_reglas($idcolegio, $idrol);
           
            if ($reglas)
            {
                switch ($campo) {
                    case 'reglas':
                        $result = $reglas->roles_id;
                        break;
                    case 'eventos':
                        $result = $reglas->roles_eventos;
                        break;
                    case 'supervisa':
                        //$result = $reglas->supervisado;
                        $reglascole = $this->modelo_permisos->get_reglas3($idcolegio); 
                        $grupos = array();
                        $result = array();
                        if($reglascole){
                            foreach ($reglascole as $regla) {
                                //$arreSup = array();
                                if($regla->supervisado!= null){
                                    $arreSup = explode(",", $regla->supervisado);
                                    if( in_array($idrol, $arreSup) ){
                                        $result[]=$regla->group_id;
                                    }
                                }
                            }

                            if(count($result)>0)
                                $result = join(',',$result);
                        }
                        break;
                    case 'menu':
                        $result = $reglas->id_menus_acciones;
                        break;
                    default:
                        # code...
                        break;
                }

                if(count($result)>0){
                    $arre = array(
                        'status' => 1,
                        'result' => explode(',',$result),
                    );
                }
                else{
                    $arre = array(
                        'status' => 0,
                        'message' => 'no_reglas'
                    );
                } 

                $data = json_encode($arre);
                $this->response($data, REST_Controller::HTTP_OK);
            }
            else
            {
                $arre = array(
                    'status' => 0,
                    'message' => 'no_reglas'
                );
                $data = json_encode($arre);
                $this->response($data, REST_Controller::HTTP_OK);
            }
        }
    }

    public function set_permisos_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $data = $this->post('data'); 
            //print_r($data); die();

            //
           
            if ($data)
            {  
                $result = $this->modelo_permisos->set_reglas(json_decode($data));

                if($result){
                    $arre = array(
                        'status' => 1,
                    );
                    $data = json_encode($arre);
                    $this->response($data, REST_Controller::HTTP_OK);
                }
                else
                {
                    $arre = array(
                        'status' => 0,
                        'message' => 'erro'
                    );
                    $data = json_encode($arre);
                    $this->response($data, REST_Controller::HTTP_OK);
                }
            }
            else
            {
                $arre = array(
                    'status' => 0,
                    'message' => 'no_data'
                );
                $data = json_encode($arre);
                $this->response($data, REST_Controller::HTTP_OK);
            }
        }
    }


    public function menu_acciones_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            //$idcolegio = $this->post('idcolegio'); 
            
            $reglas = $this->modelo_permisos->get_menuacciones();
           
            if ($reglas)
            {
                $arre = array(
                    'status' => 1,
                    'data' => $reglas,
                );
                $data = json_encode($arre);
                $this->response($data, REST_Controller::HTTP_OK);
            }
            else
            {
                $arre = array(
                    'status' => 0,
                    'message' => 'no_acciones'
                );
                $data = json_encode($arre);
                $this->response($data, REST_Controller::HTTP_OK);
            }
        }
    }


}
