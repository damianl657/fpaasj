<script>
$('#div_contador_de_alumnos').append("<img  id='loading_scroll_contador_de_alumnos' src='<?=base_url('vendors/loading.gif');?>' height='50' width='50' />");
$('#div_contador_de_varones').append("<img  id='loading_scroll_contador_de_varones' src='<?=base_url('vendors/loading.gif');?>' height='50' width='50' />");
$('#div_contador_de_mujeres').append("<img  id='loading_scroll_contador_de_mujeres' src='<?=base_url('vendors/loading.gif');?>' height='50' width='50' />");
$('#div_porcentaje_total_escuela').append("<img  id='loading_scroll_porcentaje_total_escuela' src='<?=base_url('vendors/loading.gif');?>' height='50' width='50' />");
$('#div_porcentaje_total_basico').append("<img  id='loading_scroll_porcentaje_total_basico' src='<?=base_url('vendors/loading.gif');?>' height='50' width='50' />");
$('#div_porcentaje_total_orientado').append("<img  id='loading_scroll_porcentaje_total_orientado' src='<?=base_url('vendors/loading.gif');?>' height='50' width='50' />");
var asincrono = 0;
var asincrono_a = true;

var asincrono2 = 0;
var asincrono_a2 = true;
</script>
<?php
$cant_div = 0;
//print_r($divisiones);
foreach($divisiones as $division)
{
    $cant_div++;
    $texto_anio = substr($division->descripcion,0,2).' a&ntilde;o';
}
?>
<script>total_alumnos_divisiones=0; contador_de_varones=0; contador_de_mujeres=0;</script>
<table id="datatable-buttons" class="table table-striped table-bordered jambo_table bulk_action">
    <thead>

       <tr>
            <th rowspan="2"><center> Materias / A&ntilde;os</center></th>
            <th colspan="<? echo ($cant_div*2)?>" ><center><? echo $texto_anio;?></center></th>
            <th colspan="<? echo $cant_div?>" ><center><? echo $texto_anio;?></center></th>
                          
        </tr>
        <tr>
            <?
                foreach($divisiones as $division)
                {
                    echo '<th colspan="2">'.$division->descripcion.'</th>';
                    
                }
            ?>
            <?
                foreach($divisiones as $division)
                {
                    echo '<th colspan="1">'.$division->descripcion.'</th>';
                    //de paso genero la variable global de porcentajes
                    echo '<script> var reprobados_total_div_'.$division->codigo.' = 0; </script>';
                }
            ?>
        </tr>
    </thead>
    <tbody>
    <?php
        foreach($materias as $materia)
        {
        ?>
        <tr>
            <td><? echo  $materia->descripcion?></td>
            <?
                foreach($divisiones as $division)
                {
                    $posicion = $id_anio.'_'.$division->codigo.'_'.$materia->codigo;
                    echo '<td><center>
                                <div id="loading_'.$posicion.'"></div></center>
                                <p id="reprobados_'.$posicion.'"></p>
                        </td>
                        <td>
                                <p id="cant_alumnos_'.$posicion.'"></p>
                        </td>
                        <script>
                                    if(asincrono_a < 0)
                                    {
                                        asincrono_a++;
                                        asincrono_a = false;
                                    }
                                    else
                                    {
                                        asincrono_a = 0;
                                        asincrono_a = true;

                                    }
                                    var division = '.$division->codigo.';
                                    var materia = '.$materia->codigo.';
                                    var posicion = "'.$posicion.'";
                                    traer_reprobados_x_materia_x_division(posicion,division,materia,asincrono_a);
                        </script>';
                }
                foreach($divisiones as $division)
                {
                    $posicion = $id_anio.'_'.$division->codigo.'_'.$materia->codigo;
                    echo '<td><center>
                                <div id="por_loading_'.$posicion.'"></div>
                                <p id="por_reprobados_'.$posicion.'"></p></center>
                        </td>
                       
                        <script>
                        if(asincrono_a2 < 2)
                        {
                            asincrono_a2++;
                            asincrono_a2 = false;
                        }
                        else
                        {
                            asincrono_a2 = 0;
                            asincrono_a2 = true;

                        }
                                    var division = '.$division->codigo.';
                                    var materia = '.$materia->codigo.';
                                    var posicion = "'.$posicion.'";
                                    traer_porcentaje_reprobados_x_materia_x_division(posicion,division,materia,asincrono_a2);
                        </script>';
                }
            ?>
                          
        </tr>
        <?
        } 
    
        ?>
        <tr>
            <td><strong>
                S&iacute;ntesis de Porcentajes de desaprobados</strong>
            </td>
            <?
            foreach($divisiones as $division)
            {
                ?>
                <td colspan="2"></td>
                <?
            }
            foreach($divisiones as $division)
            {
                $posicion = $id_anio.'_'.$division->codigo;
                ?>
                <td>
                    <center><div id="por_loading_materia_<? echo $posicion;?>"></div>
                    <strong><p id="total_rep_<? echo $posicion;?>" ></p></center></strong>
                    <script>
                         var posicion = "<? echo $posicion ?>";
                         var division = "<? echo $division->codigo?>";
                         traer_porcentaje_reprobados_x_division(posicion,division);
                    </script>
                </td>
                <?
            }
            ?>
        </tr>
                        
    </tbody>
</table>
<script>
    traer_porcentaje_total_reprobados_escuela();
</script>