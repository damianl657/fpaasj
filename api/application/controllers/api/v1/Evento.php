<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';

    class Evento extends REST_Controller
    {

    function __construct()
    {
        //inserto cabeceras para que no me de error cors
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        parent::__construct();
        $this->load->model("modelo_evento");
        $this->load->model("modelo_usuario");
        $this->load->model("modelo_alumno");
        $this->load->model("modelo_colegio");
        $this->load->model("modelo_anio");
        $this->load->model("modelo_nivel");
        $this->load->model("modelo_division");
        $this->load->model("modelo_upload");
        $this->load->model("modelo_permisos");
        $this->load->model("modelo_tutor");
        
    }

    public function nuevos_recientes_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            $eventos = $this->modelo_evento->obtener_eventos_recientes($userid);

            if ($eventos)
            {
                foreach ($eventos as $key  )
                {
                   $this->modelo_evento->registrar_noficacion($key->id);
                }
                //print_r($eventos);
                //$eventos["query"] = $this->db->last_query();
              //  $eventos_q = $this->db->last_query();
                // print_r($eventos_q); die();
                $result = array();
                foreach ($eventos as $key )
                {

                    $puede_ver = 0;
                    if($this->modelo_alumno->is_alumno($key->destinatario) == 1)
                    {
                        if($this->modelo_alumno->pago($key->destinatario) == 1)
                        {
                            $puede_ver = 1;

                        }
                    }
                    else
                    {
                        $puede_ver = 1;

                    }

                    if($puede_ver == 1)
                    {

                        $tipo = 'image';
                            $fotos = $this->modelo_upload->obtener_files_x_tipo($key->evento_id,$tipo);

                            $arrefotos = array();

                            foreach ($fotos as $url )
                            {
                              $arrefotos[]= $url;
                            }

                            $cont_me_gusta = $this->modelo_evento->obtener_cant_me_gusta_x_evento($key->evento_id,$tipo);
                            $key->cant_me_gusta = $cont_me_gusta;
                            $key->urls_fotos = $arrefotos;

                            //parte que agregue
                           //obenter_cant_files
                            $cant_files2 = "SELECT eventos.*,COUNT(files.id) as cantfiles FROM eventos LEFT JOIN files ON eventos.id = files.evento_id WHERE  eventos.id = $key->evento_id";
                           // $cant_files2 = "CALL obtener_cant_files_x_evento(".$key->evento_id.")";
                            $cant_files2 = $this->db->query($cant_files2);
                            $cant_files2 = $cant_files2->row();
                            $key->cantfiles = $cant_files2->cantfiles;
                            //mysqli_next_result( $this->db->conn_id );
                            //fin

                            //grupo
                           $groups = "SELECT groups.name nombre_grupo, groups.aliasM nombre_grupo_aliasM, groups.aliasF nombre_grupo_aliasF FROM groups  WHERE  id = $key->group_id";
                            //$groups = "CALL obtener_eventos_grupo_creador(".$key->group_id.")";
                            $groups = $this->db->query($groups);
                            $groups = $groups->row();
                            $key->nombre_grupo = $groups->nombre_grupo;
                            //$key->nombre_grupo_aliasM = $groups->nombre_grupo_aliasM;
                            //$key->nombre_grupo_aliasF = $groups->nombre_grupo_aliasF;
                            mysqli_next_result( $this->db->conn_id );
                            //fin

                            //datos del creador
                            $sql_creador = "SELECT users.first_name nombre_creador,users.last_name apellido_creador,users.sexo sexo_creador,users_groups.group_alias nombre_grupo_alias FROM users
                            JOIN users_groups ON users_groups.user_id = users.id
                            where users.id = $key->users_id
                            AND users_groups.estado = 1";
                            //$sql_creador = "CALL obtener_creador(".$key->users_id.")";
                            $sql_creador = $this->db->query($sql_creador);
                            $sql_creador = $sql_creador->row();
                            $key->nombre_creador = $sql_creador->nombre_creador;
                            $key->apellido_creador = $sql_creador->apellido_creador;
                            //$key->sexo_creador = $sql_creador->sexo_creador;
                            //$key->nombre_grupo_alias = $sql_creador->nombre_grupo_alias;
                            //mysqli_next_result( $this->db->conn_id );
                            //fin


                            //ALIAS ROL
                            if($sql_creador->nombre_grupo_alias!=null){
                                //echo "entra1";
                                $key->nombre_grupo = $sql_creador->nombre_grupo_alias;
                            }
                            else
                                if( $groups->nombre_grupo_aliasM!=null) {
                                    //echo "entra2";
                                    if($sql_creador->sexo_creador=='F'){
                                        //echo "entra3";
                                        $key->nombre_grupo = $groups->nombre_grupo_aliasF;
                                    }
                                    else{
                                        //echo "entra4";
                                        $key->nombre_grupo = $groups->nombre_grupo_aliasM;
                                    }
                                }
                            //$key->notificado;
                         //   $key->medio;

                            //destinatario
                            //$sql_destinatario = "CALL obtener_eventos_datos_destinatario(".$key->destinatario.")";
                            $sql_destinatario = "SELECT users.first_name nombre, users.last_name apellido, users.foto, users.id user_id_destinatario, users.sexo from users where users.id = $key->destinatario";
                            $sql_destinatario = $this->db->query($sql_destinatario);
                            $sql_destinatario = $sql_destinatario->row();
                            $key->nombre = $sql_destinatario->nombre;
                            $key->apellido = $sql_destinatario->apellido;
                            $key->foto = $sql_destinatario->foto;
                            if($key->foto != NULL)
                            {
                                $key->foto = $this->utilidades->get_url_img().'fotos_usuarios/'.$key->foto;
                            }
                            else
                            {
                                $key->foto = $this->utilidades->get_url_img().'fotos_usuarios/default-avatar.png';
                            }
                            mysqli_next_result( $this->db->conn_id );

                            //agregar foto colegio
                            if($key->logo != NULL)
                            {
                                $key->logo = $this->utilidades->get_url_img().'fotos_colegios/'.$key->logo;
                            }
                            else
                            {
                                $key->logo = $this->utilidades->get_url_img().'fotos_colegios/default_esc.png';
                            }
                            //fin
                         //   $key->notificado;
                         //   $key->medio;
                            $result[]=$key;
                           // print_r($result); die();
                    }

                }
               // print_r($result);
               //  die();
              $this->response($result, REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron eventos',
                    //'query' => $this->db->last_query()
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }
    public function eventos_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            $limit = $this->get("limit");
            if(isset($limit)){
                $limit = str_replace("-", ",", $limit);
            }
            else{
                $limit = '0,5';
            }
            // Obtengo todos los eventos
            $eventos = $this->modelo_evento->obtener_eventos($userid, $limit); // obtiene los eventos de el usuario X
            // Si existe mas de un resultado, lo mando
            if ($eventos)
            {
               //print_r($eventos);
               /* $eventos["query"] = $this->db->last_query();
                print_r($eventos["query"]);*/
                //$eventos = $this->db->last_query();

                $result = array();
                foreach ( $eventos as $key )
                {
                    //print_r($key); die();
                    $puede_ver = 0;
                    if($this->modelo_alumno->is_alumno($key->destinatario) == 1)
                    {
                        if($this->modelo_alumno->pago($key->destinatario) == 1)
                        {
                            $puede_ver = 1;

                        }
                    }
                    else
                    {
                        $puede_ver = 1;

                    }

                    if($puede_ver == 1)
                    {
                        $tipo = 'image';
                            $fotos = $this->modelo_upload->obtener_files_x_tipo($key->evento_id,$tipo);

                            $arrefotos = array();

                            foreach ($fotos as $url )
                            {
                              $arrefotos[]= $url;
                            }

                            $cont_me_gusta = $this->modelo_evento->obtener_cant_me_gusta_x_evento($key->evento_id,$tipo);
                            $key->cant_me_gusta = $cont_me_gusta;
                            $key->urls_fotos = $arrefotos;

                            //parte que agregue
                           //obenter_cant_files
                            $cant_files2 = "SELECT eventos.*,COUNT(files.id) as cantfiles FROM eventos LEFT JOIN files ON eventos.id = files.evento_id WHERE  eventos.id = $key->evento_id";
                            //$cant_files2 = "CALL obtener_cant_files_x_evento(".$key->evento_id.")";
                            $cant_files2 = $this->db->query($cant_files2);
                            $cant_files2 = $cant_files2->row();
                            $key->cantfiles = $cant_files2->cantfiles;
                            //mysqli_next_result( $this->db->conn_id );
                            //fin

                            //grupo
                           $groups = "SELECT groups.name nombre_grupo, groups.aliasM nombre_grupo_aliasM, groups.aliasF nombre_grupo_aliasF FROM groups  WHERE  id = $key->group_id";
                            //$groups = "CALL obtener_eventos_grupo_creador(".$key->group_id.")";
                            $groups = $this->db->query($groups);
                            $groups = $groups->row();
                            $key->nombre_grupo = $groups->nombre_grupo;
                            //$key->nombre_grupo_aliasM = $groups->nombre_grupo_aliasM;
                            //$key->nombre_grupo_aliasF = $groups->nombre_grupo_aliasF;

                           // mysqli_next_result( $this->db->conn_id );
                            //fin

                            //datos del creador
                            $sql_creador = "SELECT users.first_name nombre_creador,users.last_name apellido_creador,users.sexo sexo_creador,users_groups.group_alias nombre_grupo_alias FROM users
                            JOIN users_groups ON users_groups.user_id = users.id
                            where users.id = $key->users_id
                            AND users_groups.estado = 1";
                            //$sql_creador = "CALL obtener_creador(".$key->users_id.")";
                            $sql_creador = $this->db->query($sql_creador);
                            $sql_creador_ = $sql_creador;
                            $creador_existe = true;
                            if($sql_creador_->num_rows() > 0)
                            {
                                $creador_existe = true;
                                $sql_creador = $sql_creador->row();
                                $key->nombre_creador = $sql_creador->nombre_creador;
                                $key->apellido_creador = $sql_creador->apellido_creador;
                                //$key->sexo_creador = $sql_creador->sexo_creador;
                                //$key->nombre_grupo_alias = $sql_creador->nombre_grupo_alias;
                            }
                            else
                            {
                                $creador_existe = false;
                            }
                            //mysqli_next_result( $this->db->conn_id );
                            //fin


                            //ALIAS ROL
                            if($sql_creador_->num_rows() > 0)
                            {
                                if($sql_creador->nombre_grupo_alias!=null){
                                    //echo "entra1";
                                    $key->nombre_grupo = $sql_creador->nombre_grupo_alias;
                                }
                                else if( $groups->nombre_grupo_aliasM!=null)
                                {
                                    //echo "entra2";
                                    if($sql_creador->sexo_creador=='F')
                                    {
                                        //echo "entra3";
                                        $key->nombre_grupo = $groups->nombre_grupo_aliasF;
                                    }
                                    else{
                                        //echo "entra4";
                                        $key->nombre_grupo = $groups->nombre_grupo_aliasM;
                                    }
                                }
                            }
                            /*unset($key->nombre_grupo_aliasM);
                            unset($key->nombre_grupo_aliasF);
                            unset($key->nombre_grupo_alias);
                            unset($key->sexo_creador);*/


                            //destinatario
                            //$sql_destinatario = "CALL obtener_eventos_datos_destinatario(".$key->destinatario.")";
                            $sql_destinatario = "SELECT users.first_name nombre, users.last_name apellido, users.foto, users.id user_id_destinatario, users.sexo from users where users.id = $key->destinatario";
                            $sql_destinatario = $this->db->query($sql_destinatario);
                            $sql_destinatario = $sql_destinatario->row();
                            $key->nombre = $sql_destinatario->nombre;
                            $key->apellido = $sql_destinatario->apellido;
                            $key->foto = $sql_destinatario->foto;
                            if($key->foto != NULL)
                            {
                                $key->foto = $this->utilidades->get_url_img().'fotos_usuarios/'.$key->foto;
                            }
                            else
                            {
                                 $key->foto = $this->utilidades->get_url_img().'fotos_usuarios/default-avatar.png';
                            }
                            //mysqli_next_result( $this->db->conn_id );

                            //agregar foto colegio
                            if($key->logo != NULL)
                            {
                                $key->logo = $this->utilidades->get_url_img().'fotos_colegios/'.$key->logo;
                            }
                            else
                            {
                                $key->logo = $this->utilidades->get_url_img().'fotos_colegios/default_esc.png';
                            }
                            //fin

                            if($creador_existe == true)
                            {
                                $result[]=$key;
                            }
                    }


                }
               // print_r($result);
                // die();
              $this->response($result, REST_Controller::HTTP_OK);


            }
            // Sino, envio respuesta con 404
            else
            {
                $fecha_creacion = date('Y-m-d');
                $usuario_aux = $this->modelo_usuario->get_users_id($userid);
                $eventos = array (['evento_id'=> 0,
                    'titulo'=>'Bienvenido a Nodos',
                     'descripcion'=>'Estimada Familia, a partir de ahora por este medio, podrán recibir todos los comunicados enviados por la Institución, junto con los documentos, fotos y videos, de forma instantánea y en tiempo real',
                     'nombre'=>$usuario_aux->first_name,
                     'apellido'=>$usuario_aux->last_name,
                     'fechaInicio' => $fecha_creacion,
                     'confirm' => '0',
                     'fechaCreacion' => $fecha_creacion,
                     'aceptado' => '0',
                     'visto' => '0',
                     'id' => '0',
                     'me_gusta' => '0',
                     'destinatario' => '0',
                     'cantfiles' => '0',
                     'foto' => $this->utilidades->get_url_img()."/icon-isologo-nodos.png",
                     'nombre_creador' => 'Team',
                     'apellido_creador' => 'Nodos',
                     'logo' => $this->utilidades->get_url_img()."/icon-user.png",
                     'nombre_grupo' => 'NODOS',
                     'cant_me_gusta' => 0,

                     ]
                     );

                $this->response($eventos, REST_Controller::HTTP_OK);

            }
        }
    }

    public function mas_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            //$limit = $this->get("limit");
            $limit = '0,5';
            if(isset($limit)){
            }
            else{
                $limit = false;
            }
            $eventos = 0;
            $lastId = $this->get("lastId");
            if(isset($lastId)){
                $eventos = $this->modelo_evento->cargar_mas_eventos((int)$this->get("lastId"),$userid);
            }
            else{
                $firstId = $this->get("firstId");
                if(isset($firstId))
                    $eventos = $this->modelo_evento->cargar_mas_eventos_nuevos((int)$firstId,$userid);
            }
            // Si existe mas de un resultado, lo mando
            if ($eventos != 0 && $eventos)
            {
                //$eventos["query"] = $this->db->last_query();
                $result = array();
                foreach ($eventos as $key)
                {


                    $puede_ver = 0;
                    if($this->modelo_alumno->is_alumno($key->destinatario) == 1)
                    {
                        if($this->modelo_alumno->pago($key->destinatario) == 1)
                        {
                            $puede_ver = 1;

                        }
                    }
                    else
                    {
                        $puede_ver = 1;

                    }

                    if($puede_ver == 1)
                    {
                        $tipo = 'image';
                            $fotos = $this->modelo_upload->obtener_files_x_tipo($key->evento_id,$tipo);

                            $arrefotos = array();

                            foreach ($fotos as $url )
                            {
                              $arrefotos[]= $url;
                            }

                            $cont_me_gusta = $this->modelo_evento->obtener_cant_me_gusta_x_evento($key->evento_id,$tipo);
                            $key->cant_me_gusta = $cont_me_gusta;
                            $key->urls_fotos = $arrefotos;

                            //parte que agregue
                           //obenter_cant_files
                            $cant_files2 = "SELECT eventos.*,COUNT(files.id) as cantfiles FROM eventos LEFT JOIN files ON eventos.id = files.evento_id WHERE  eventos.id = $key->evento_id";
                            //$cant_files2 = "CALL obtener_cant_files_x_evento(".$key->evento_id.")";
                            $cant_files2 = $this->db->query($cant_files2);
                            $cant_files2 = $cant_files2->row();
                            $key->cantfiles = $cant_files2->cantfiles;
                           // mysqli_next_result( $this->db->conn_id );
                            //fin

                            //grupo
                           $groups = "SELECT groups.name nombre_grupo, groups.aliasM nombre_grupo_aliasM, groups.aliasF nombre_grupo_aliasF FROM groups  WHERE  id = $key->group_id";
                            //$groups = "CALL obtener_eventos_grupo_creador(".$key->group_id.")";
                            $groups = $this->db->query($groups);
                            $groups = $groups->row();
                            $key->nombre_grupo = $groups->nombre_grupo;
                            //$key->nombre_grupo_aliasM = $groups->nombre_grupo_aliasM;
                            //$key->nombre_grupo_aliasF = $groups->nombre_grupo_aliasF;
                            mysqli_next_result( $this->db->conn_id );
                            //fin

                            //datos del creador
                            $sql_creador = "SELECT users.first_name nombre_creador,users.last_name apellido_creador,users.sexo sexo_creador,users_groups.group_alias nombre_grupo_alias FROM users
                            JOIN users_groups ON users_groups.user_id = users.id
                            where users.id = $key->users_id
                            AND users_groups.estado = 1";
                            //$sql_creador = "CALL obtener_creador(".$key->users_id.")";
                            $sql_creador = $this->db->query($sql_creador);
                            $sql_creador_ = $sql_creador;
                            $creador_existe = true;
                            if($sql_creador_->num_rows() > 0)
                            {
                                $creador_existe = true;
                                $sql_creador = $sql_creador->row();
                                $key->nombre_creador = $sql_creador->nombre_creador;
                                $key->apellido_creador = $sql_creador->apellido_creador;
                                //$key->sexo_creador = $sql_creador->sexo_creador;
                                //$key->nombre_grupo_alias = $sql_creador->nombre_grupo_alias;
                            }

                            //mysqli_next_result( $this->db->conn_id );
                            //fin


                            //ALIAS ROL
                            if($sql_creador_->num_rows() > 0)
                            {
                                if($sql_creador->nombre_grupo_alias!=null){
                                    //echo "entra1";
                                    $key->nombre_grupo = $sql_creador->nombre_grupo_alias;
                                }
                                else
                                    if( $groups->nombre_grupo_aliasM!=null) {
                                        //echo "entra2";
                                        if($sql_creador->sexo_creador=='F'){
                                            //echo "entra3";
                                            $key->nombre_grupo = $groups->nombre_grupo_aliasF;
                                        }
                                        else{
                                            //echo "entra4";
                                            $key->nombre_grupo = $groups->nombre_grupo_aliasM;
                                        }
                                    }
                            }

                            //destinatario
                            //$sql_destinatario = "CALL obtener_eventos_datos_destinatario(".$key->destinatario.")";
                            $sql_destinatario = "SELECT users.first_name nombre, users.last_name apellido, users.foto, users.id user_id_destinatario, users.sexo from users where users.id = $key->destinatario";
                            $sql_destinatario = $this->db->query($sql_destinatario);
                            $sql_destinatario = $sql_destinatario->row();
                            $key->nombre = $sql_destinatario->nombre;
                            $key->apellido = $sql_destinatario->apellido;
                            $key->foto = $sql_destinatario->foto;
                            if($key->foto != NULL)
                            {
                                $key->foto = $this->utilidades->get_url_img().'fotos_usuarios/'.$key->foto;
                            }
                            else
                            {
                                $key->foto = $this->utilidades->get_url_img().'fotos_usuarios/default-avatar.png';
                            }
                            //mysqli_next_result( $this->db->conn_id );

                            //agregar foto colegio
                            if($key->logo != NULL)
                            {
                                $key->logo = $this->utilidades->get_url_img().'fotos_colegios/'.$key->logo;
                            }
                            else
                            {
                                $key->logo = $this->utilidades->get_url_img().'fotos_colegios/default_esc.png';
                            }
                            //fin

                            if($creador_existe == true)
                            {
                                $result[]=$key;
                            }
                    }
                    //rol alias

                }
                $this->response($result, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {   //$eventos["query"] = $this->db->last_query();
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron eventos',
                 //   'query' => $this->db->last_query()
                 //   'lastId'=>$lastId,
                 //    'firstId'=>$firstId,
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code*/
            }
        }
    }

    public function eventos_hijos_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
           
            $userd_id_hijos = $this->post("userd_id_hijos");
            if($userd_id_hijos == -1)
            {
                $userd_id_hijos = 0;
            }
            $userd_id_tutor = $this->post("userd_id_tutor");
            /*if($userd_id_tutor == 0)
            {
                $userd_id_tutor = $userid;
            }*/
           
            // Obtengo todos los eventos
            if(($this->post("userd_id_tutor") == 0) and ($this->post("userd_id_hijos") == -1))
            {
                $limit = '0,5';
                $eventos = $this->modelo_evento->obtener_eventos($userid, $limit);
            }
            else
            {
                $eventos = $this->modelo_evento->obtener_eventos_hijos($userd_id_hijos,$userd_id_tutor); // obtiene los eventos de el usuario X
            }
                
            $hay_eventos = false;
            // Si existe mas de un resultado, lo mando
            $result = array();
            if ($eventos)
            {
                foreach ( $eventos as $key )
                {
                    $key->last_query = $this->db->last_query();
                    $hay_eventos = true;
                    //print_r($key); die();
                    $puede_ver = 0;
                    if($this->modelo_alumno->is_alumno($key->destinatario) == 1)
                    {
                        if($this->modelo_alumno->pago($key->destinatario) == 1)
                        {
                            $puede_ver = 1;

                        }
                    }
                    else
                    {
                        $puede_ver = 1;

                    }

                    if($puede_ver == 1)
                    {
                        $tipo = 'image';
                            $fotos = $this->modelo_upload->obtener_files_x_tipo($key->evento_id,$tipo);

                            $arrefotos = array();

                            foreach ($fotos as $url )
                            {
                              $arrefotos[]= $url;
                            }

                            $cont_me_gusta = $this->modelo_evento->obtener_cant_me_gusta_x_evento($key->evento_id,$tipo);
                            $key->cant_me_gusta = $cont_me_gusta;
                            $key->urls_fotos = $arrefotos;

                            //parte que agregue
                           //obenter_cant_files
                            $cant_files2 = "SELECT eventos.*,COUNT(files.id) as cantfiles FROM eventos LEFT JOIN files ON eventos.id = files.evento_id WHERE  eventos.id = $key->evento_id";
                            //$cant_files2 = "CALL obtener_cant_files_x_evento(".$key->evento_id.")";
                            $cant_files2 = $this->db->query($cant_files2);
                            $cant_files2 = $cant_files2->row();
                            $key->cantfiles = $cant_files2->cantfiles;
                            //mysqli_next_result( $this->db->conn_id );
                            //fin

                            //grupo
                           $groups = "SELECT groups.name nombre_grupo, groups.aliasM nombre_grupo_aliasM, groups.aliasF nombre_grupo_aliasF FROM groups  WHERE  id = $key->group_id";
                            //$groups = "CALL obtener_eventos_grupo_creador(".$key->group_id.")";
                            $groups = $this->db->query($groups);
                            $groups = $groups->row();
                            $key->nombre_grupo = $groups->nombre_grupo;
                            //$key->nombre_grupo_aliasM = $groups->nombre_grupo_aliasM;
                            //$key->nombre_grupo_aliasF = $groups->nombre_grupo_aliasF;

                            //mysqli_next_result( $this->db->conn_id );
                            //fin

                            //datos del creador
                            $sql_creador = "SELECT users.first_name nombre_creador,users.last_name apellido_creador,users.sexo sexo_creador,users_groups.group_alias nombre_grupo_alias FROM users
                            JOIN users_groups ON users_groups.user_id = users.id
                            where users.id = $key->users_id
                            AND users_groups.estado = 1";
                            //$sql_creador = "CALL obtener_creador(".$key->users_id.")";
                            $sql_creador = $this->db->query($sql_creador);
                            $sql_creador_ = $sql_creador;
                            $creador_existe = true;
                            if($sql_creador_->num_rows() > 0)
                            {
                                $creador_existe = true;
                                $sql_creador = $sql_creador->row();
                                $key->nombre_creador = $sql_creador->nombre_creador;
                                $key->apellido_creador = $sql_creador->apellido_creador;
                                //$key->sexo_creador = $sql_creador->sexo_creador;
                                //$key->nombre_grupo_alias = $sql_creador->nombre_grupo_alias;
                            }
                            else
                            {
                                $creador_existe = false;
                            }
                            //mysqli_next_result( $this->db->conn_id );
                            //fin


                            //ALIAS ROL
                            if($sql_creador_->num_rows() > 0)
                            {
                                if($sql_creador->nombre_grupo_alias!=null){
                                    //echo "entra1";
                                    $key->nombre_grupo = $sql_creador->nombre_grupo_alias;
                                }
                                else if( $groups->nombre_grupo_aliasM!=null)
                                {
                                    //echo "entra2";
                                    if($sql_creador->sexo_creador=='F')
                                    {
                                        //echo "entra3";
                                        $key->nombre_grupo = $groups->nombre_grupo_aliasF;
                                    }
                                    else{
                                        //echo "entra4";
                                        $key->nombre_grupo = $groups->nombre_grupo_aliasM;
                                    }
                                }
                            }
                            /*unset($key->nombre_grupo_aliasM);
                            unset($key->nombre_grupo_aliasF);
                            unset($key->nombre_grupo_alias);
                            unset($key->sexo_creador);*/


                            //destinatario
                            //$sql_destinatario = "CALL obtener_eventos_datos_destinatario(".$key->destinatario.")";
                            $sql_destinatario = "SELECT users.first_name nombre, users.last_name apellido, users.foto, users.id user_id_destinatario, users.sexo from users where users.id = $key->destinatario";
                            $sql_destinatario = $this->db->query($sql_destinatario);
                            $sql_destinatario = $sql_destinatario->row();
                            $key->nombre = $sql_destinatario->nombre;
                            $key->apellido = $sql_destinatario->apellido;
                            $key->foto = $sql_destinatario->foto;
                            if($key->foto != NULL)
                            {
                                $key->foto = $this->utilidades->get_url_img().'fotos_usuarios/'.$key->foto;
                            }
                            else
                            {
                                 $key->foto = $this->utilidades->get_url_img().'fotos_usuarios/default-avatar.png';
                            }
                            //mysqli_next_result( $this->db->conn_id );

                            //agregar foto colegio
                            if($key->logo != NULL)
                            {
                                $key->logo = $this->utilidades->get_url_img().'fotos_colegios/'.$key->logo;
                            }
                            else
                            {
                                $key->logo = $this->utilidades->get_url_img().'fotos_colegios/default_esc.png';
                            }
                            //fin

                            if($creador_existe == true)
                            {
                                $result[]=$key;
                            }
                    }


                }
                $this->response($result, REST_Controller::HTTP_OK);
            }
            else
            {
                $fecha_creacion = date('Y-m-d');
                $usuario_aux = $this->modelo_usuario->get_users_id($userid);
                $eventos = array (['evento_id'=> 0,
                    'titulo'=>'Bienvenido a Nodos',
                     'descripcion'=>'Estimada Familia, a partir de ahora por este medio, podrán recibir todos los comunicados enviados por la Institución, junto con los documentos, fotos y videos, de forma instantánea y en tiempo real',
                     'nombre'=>$usuario_aux->first_name,
                     'apellido'=>$usuario_aux->last_name,
                     'fechaInicio' => $fecha_creacion,
                     'confirm' => '0',
                     'fechaCreacion' => $fecha_creacion,
                     'aceptado' => '0',
                     'visto' => '0',
                     'id' => '0',
                     'me_gusta' => '0',
                     'destinatario' => '0',
                     'cantfiles' => '0',
                     'foto' => $this->utilidades->get_url_img()."/icon-isologo-nodos.png",
                     'nombre_creador' => 'Team',
                     'apellido_creador' => 'Nodos',
                     'logo' => $this->utilidades->get_url_img()."/icon-user.png",
                     'nombre_grupo' => 'NODOS',
                     'cant_me_gusta' => 0,

                     ]
                     );

                $this->response($eventos, REST_Controller::HTTP_OK);

            }
            // Sino, envio respuesta con 404
            
        }
    }
    public function mas_eventos_hijos_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
           
            $userd_id_hijos = $this->post("userd_id_hijos");
            $userd_id_tutor = $this->post("userd_id_tutor");
            if($userd_id_hijos == -1)
            {
                $userd_id_hijos = 0;
            }
            /*if($userd_id_tutor == 0)
            {
                $userd_id_tutor = $userid;
            }*/
            $limit = '0,5';
            if(isset($limit)){
            }
            else{
                $limit = false;
            }
            $eventos = 0;
            $lastId = $this->post("lastId");
            if(isset($lastId))
            {
                
                if(($this->post("userd_id_tutor") == 0) and ($this->post("userd_id_hijos") == -1))
                {
                   
                    $eventos = $this->modelo_evento->cargar_mas_eventos((int)$this->post("lastId"),$userid);
                }
                else
                {
                    $eventos = $this->modelo_evento->cargar_mas_eventos_hijos((int)$this->post("lastId"),$userd_id_hijos,$userd_id_tutor);
                }
            }
            

           
           
           
            // Si existe mas de un resultado, lo mando
            $result = array();
            if ($eventos)
            {
                foreach ( $eventos as $key )
                {
                    $hay_eventos = true;
                    //print_r($key); die();
                    $puede_ver = 0;
                    if($this->modelo_alumno->is_alumno($key->destinatario) == 1)
                    {
                        if($this->modelo_alumno->pago($key->destinatario) == 1)
                        {
                            $puede_ver = 1;

                        }
                    }
                    else
                    {
                        $puede_ver = 1;

                    }

                    if($puede_ver == 1)
                    {
                        $tipo = 'image';
                            $fotos = $this->modelo_upload->obtener_files_x_tipo($key->evento_id,$tipo);

                            $arrefotos = array();

                            foreach ($fotos as $url )
                            {
                              $arrefotos[]= $url;
                            }

                            $cont_me_gusta = $this->modelo_evento->obtener_cant_me_gusta_x_evento($key->evento_id,$tipo);
                            $key->cant_me_gusta = $cont_me_gusta;
                            $key->urls_fotos = $arrefotos;

                            //parte que agregue
                           //obenter_cant_files
                            $cant_files2 = "SELECT eventos.*,COUNT(files.id) as cantfiles FROM eventos LEFT JOIN files ON eventos.id = files.evento_id WHERE  eventos.id = $key->evento_id";
                            //$cant_files2 = "CALL obtener_cant_files_x_evento(".$key->evento_id.")";
                            $cant_files2 = $this->db->query($cant_files2);
                            $cant_files2 = $cant_files2->row();
                            $key->cantfiles = $cant_files2->cantfiles;
                            //mysqli_next_result( $this->db->conn_id );
                            //fin

                            //grupo
                           $groups = "SELECT groups.name nombre_grupo, groups.aliasM nombre_grupo_aliasM, groups.aliasF nombre_grupo_aliasF FROM groups  WHERE  id = $key->group_id";
                            //$groups = "CALL obtener_eventos_grupo_creador(".$key->group_id.")";
                            $groups = $this->db->query($groups);
                            $groups = $groups->row();
                            $key->nombre_grupo = $groups->nombre_grupo;
                            //$key->nombre_grupo_aliasM = $groups->nombre_grupo_aliasM;
                            //$key->nombre_grupo_aliasF = $groups->nombre_grupo_aliasF;

                            mysqli_next_result( $this->db->conn_id );
                            //fin

                            //datos del creador
                            $sql_creador = "SELECT users.first_name nombre_creador,users.last_name apellido_creador,users.sexo sexo_creador,users_groups.group_alias nombre_grupo_alias FROM users
                            JOIN users_groups ON users_groups.user_id = users.id
                            where users.id = $key->users_id
                            AND users_groups.estado = 1";
                            //$sql_creador = "CALL obtener_creador(".$key->users_id.")";
                            $sql_creador = $this->db->query($sql_creador);
                            $sql_creador_ = $sql_creador;
                            $creador_existe = true;
                            if($sql_creador_->num_rows() > 0)
                            {
                                $creador_existe = true;
                                $sql_creador = $sql_creador->row();
                                $key->nombre_creador = $sql_creador->nombre_creador;
                                $key->apellido_creador = $sql_creador->apellido_creador;
                                //$key->sexo_creador = $sql_creador->sexo_creador;
                                //$key->nombre_grupo_alias = $sql_creador->nombre_grupo_alias;
                            }
                            else
                            {
                                $creador_existe = false;
                            }
                            //mysqli_next_result( $this->db->conn_id );
                            //fin


                            //ALIAS ROL
                            if($sql_creador_->num_rows() > 0)
                            {
                                if($sql_creador->nombre_grupo_alias!=null){
                                    //echo "entra1";
                                    $key->nombre_grupo = $sql_creador->nombre_grupo_alias;
                                }
                                else if( $groups->nombre_grupo_aliasM!=null)
                                {
                                    //echo "entra2";
                                    if($sql_creador->sexo_creador=='F')
                                    {
                                        //echo "entra3";
                                        $key->nombre_grupo = $groups->nombre_grupo_aliasF;
                                    }
                                    else{
                                        //echo "entra4";
                                        $key->nombre_grupo = $groups->nombre_grupo_aliasM;
                                    }
                                }
                            }
                            /*unset($key->nombre_grupo_aliasM);
                            unset($key->nombre_grupo_aliasF);
                            unset($key->nombre_grupo_alias);
                            unset($key->sexo_creador);*/


                            //destinatario
                            //$sql_destinatario = "CALL obtener_eventos_datos_destinatario(".$key->destinatario.")";
                            $sql_destinatario = "SELECT users.first_name nombre, users.last_name apellido, users.foto, users.id user_id_destinatario, users.sexo from users where users.id = $key->destinatario";
                            $sql_destinatario = $this->db->query($sql_destinatario);
                            $sql_destinatario = $sql_destinatario->row();
                            $key->nombre = $sql_destinatario->nombre;
                            $key->apellido = $sql_destinatario->apellido;
                            $key->foto = $sql_destinatario->foto;
                            if($key->foto != NULL)
                            {
                                $key->foto = $this->utilidades->get_url_img().'fotos_usuarios/'.$key->foto;
                            }
                            else
                            {
                                 $key->foto = $this->utilidades->get_url_img().'fotos_usuarios/default-avatar.png';
                            }
                            //mysqli_next_result( $this->db->conn_id );

                            //agregar foto colegio
                            if($key->logo != NULL)
                            {
                                $key->logo = $this->utilidades->get_url_img().'fotos_colegios/'.$key->logo;
                            }
                            else
                            {
                                $key->logo = $this->utilidades->get_url_img().'fotos_colegios/default_esc.png';
                            }
                            //fin

                            if($creador_existe == true)
                            {
                                $result[]=$key;
                            }
                    }


                }
                $this->response($result, REST_Controller::HTTP_OK);
            }
            else
            {
                $fecha_creacion = date('Y-m-d');
                $usuario_aux = $this->modelo_usuario->get_users_id($userid);
                $eventos = array (['evento_id'=> 0,
                    'titulo'=>'Bienvenido a Nodos',
                     'descripcion'=>'Estimada Familia, a partir de ahora por este medio, podrán recibir todos los comunicados enviados por la Institución, junto con los documentos, fotos y videos, de forma instantánea y en tiempo real',
                     'nombre'=>$usuario_aux->first_name,
                     'apellido'=>$usuario_aux->last_name,
                     'fechaInicio' => $fecha_creacion,
                     'confirm' => '0',
                     'fechaCreacion' => $fecha_creacion,
                     'aceptado' => '0',
                     'visto' => '0',
                     'id' => '0',
                     'me_gusta' => '0',
                     'destinatario' => '0',
                     'cantfiles' => '0',
                     'foto' => $this->utilidades->get_url_img()."/icon-isologo-nodos.png",
                     'nombre_creador' => 'Team',
                     'apellido_creador' => 'Nodos',
                     'logo' => $this->utilidades->get_url_img()."/icon-user.png",
                     'nombre_grupo' => 'NODOS',
                     'cant_me_gusta' => 0,

                     ]
                     );

                $this->response($eventos, REST_Controller::HTTP_OK);

            }
            // Sino, envio respuesta con 404
            
        }
    }

    public function eventosmesanio_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            $mes = $this->get("mes");
            $anio = $this->get("anio");
            if  (
                    (isset($mes)) && (!empty($mes)) && ($mes >= 1) && ($mes <= 12) &&
                    (isset($anio)) && (!empty($anio)) && ($anio >= 2015) && ($anio <= 2100)
                )
            {
                $eventos = $this->modelo_evento->obtener_eventos_mesanio($mes, $anio, $userid); // obtiene los eventos de el usuario X
                // Si existe mas de un resultado, lo mando
                if ($eventos)
                {
                    $this->response($eventos, REST_Controller::HTTP_OK);
                }
                // Sino, envio respuesta con 404
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'No se encontraron eventos'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                }
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Debe proveer los parametros de mes y anio'
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    function eventofechaseleccionada_get(){
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            $fecha = $this->get('fecha');
            if  ($fecha === NULL)
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Debe proveer una fecha'
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
            else
            {
                $eventos = $this->modelo_evento->obtener_eventos_fecha($fecha, $userid); // obtiene los eventos de el usuario X
                // Si existe mas de un resultado, lo mando
                if ($eventos)
                {
                    $this->response($eventos, REST_Controller::HTTP_OK);
                }
                // Sino, envio respuesta con 404
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'No se encontraron eventos'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                }
            }
        }
    }

    public function evento_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            $idevento = $this->get('id'); // obtengo el id del evento, si es que vino en la url
            // Obtengo un evento especifico
            $evento = $this->modelo_evento->obtener_evento($idevento); // obtiene el evento especifico
            // Si existe mas de un resultado, lo mando


            if ($evento)
            {
                //agregar campo si es propietario o no. 1 o 0
                if($evento->userid==$userid)
                    $evento->owner = 1;
                else
                    $evento->owner = 0;

                //rol alias
                if($evento->group_alias!=null){
                    //echo "entra1";
                    $evento->name = $evento->group_alias;
                }
                else
                    if( $evento->aliasM!=null) {
                        //echo "entra2";
                        if($evento->sexo=='F'){
                            //echo "entra3";
                            $evento->name = $evento->aliasF;
                        }
                        else{
                            //echo "entra4";
                            $evento->name = $evento->aliasM;
                        }
                    }
                unset($evento->aliasM);
                unset($evento->aliasF);
                unset($evento->group_alias);
                unset($evento->sexo);

                //$this->response($evento, REST_Controller::HTTP_OK);
                if($evento->standby == 1)
                {   $grupos = '';
                    $gru_c = 0;
                    $roles = $this->modelo_permisos->get_quien_autoriza_a_rol($evento->id_groups);
                    //$roles = false;
                    if($roles != false)
                    {
                        foreach ($roles->result() as $key )
                        {
                            if($gru_c == 0)
                            {
                                $grupos = $key->name;
                                $gru_c = 1;
                            }
                            else
                            {
                                $grupos = $grupos.', '.$key->name;
                            }

                        }
                    }

                    //$evento->sql_grp = $roles->result();
                    $evento->estado_publicacion= "En Espera de Autorizacion por: ".$grupos;
                    $this->response($evento, REST_Controller::HTTP_OK);
                }
                else
                {
                        $evento->aa= "Publicado";
                        $this->response($evento, REST_Controller::HTTP_OK);
                }
            }
            // Sino, envio respuesta con 404
            else
            {   //echo $idevento;
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontro el evento con ese ID',
                    'id' => $idevento
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
		}
    }
    public function evento_users_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            $id_evento = $this->get('id_evento'); // obtengo el id del evento, si es que vino en la url
            // Obtengo un evento especifico
            $evento = $this->modelo_evento->obtener_evento_users($id_evento, $userid); // obtiene el evento especifico
            // Si existe mas de un resultado, lo mando
            if ($eventos)
            {
                foreach ($eventos as $key  )
                {
                   $this->modelo_evento->registrar_noficacion($key->id);
                }
                //print_r($eventos);
                //$eventos["query"] = $this->db->last_query();
                //$eventos = $this->db->last_query();

                $result = array();
                foreach ($eventos as $key)
                {


                    $puede_ver = 0;
                    if($this->modelo_alumno->is_alumno($key->destinatario) == 1)
                    {
                        if($this->modelo_alumno->pago($key->destinatario) == 1)
                        {
                            $puede_ver = 1;

                        }
                    }
                    else
                    {
                        $puede_ver = 1;

                    }

                    if($puede_ver == 1)
                    {
                        $tipo = 'image';
                            $fotos = $this->modelo_upload->obtener_files_x_tipo($key->evento_id,$tipo);

                            $arrefotos = array();

                            foreach ($fotos as $url )
                            {
                              $arrefotos[]= $url;
                            }

                            $cont_me_gusta = $this->modelo_evento->obtener_cant_me_gusta_x_evento($key->evento_id,$tipo);
                            $key->cant_me_gusta = $cont_me_gusta;
                            $key->urls_fotos = $arrefotos;

                            //parte que agregue
                           //obenter_cant_files
                            $cant_files2 = "SELECT eventos.*,COUNT(files.id) as cantfiles FROM eventos LEFT JOIN files ON eventos.id = files.evento_id WHERE  eventos.id = $key->evento_id";
                            //$cant_files2 = "CALL obtener_cant_files_x_evento(".$key->evento_id.")";
                            $cant_files2 = $this->db->query($cant_files2);
                            $cant_files2 = $cant_files2->row();
                            $key->cantfiles = $cant_files2->cantfiles;
                            //mysqli_next_result( $this->db->conn_id );
                            //fin

                            //grupo
                           $groups = "SELECT groups.name nombre_grupo, groups.aliasM nombre_grupo_aliasM, groups.aliasF nombre_grupo_aliasF FROM groups  WHERE  id = $key->group_id";
                            //$groups = "CALL obtener_eventos_grupo_creador(".$key->group_id.")";
                            $groups = $this->db->query($groups);
                            $groups = $groups->row();
                            $key->nombre_grupo = $groups->nombre_grupo;
                            $key->nombre_grupo_aliasM = $groups->nombre_grupo_aliasM;
                            $key->nombre_grupo_aliasF = $groups->nombre_grupo_aliasF;
                            //mysqli_next_result( $this->db->conn_id );
                            //fin

                            //datos del creador
                            $sql_creador = "SELECT users.first_name nombre_creador,users.last_name apellido_creador,users.sexo sexo_creador,users_groups.group_alias nombre_grupo_alias FROM users
                            JOIN users_groups ON users_groups.user_id = users.id
                            where users.id = $key->users_id
                            AND users_groups.estado = 1";
                            // $sql_creador = "CALL obtener_creador(".$key->users_id.")";
                            $sql_creador = $this->db->query($sql_creador);
                            $sql_creador = $sql_creador->row();
                            $key->nombre_creador = $sql_creador->nombre_creador;
                            $key->apellido_creador = $sql_creador->apellido_creador;
                            $key->sexo_creador = $sql_creador->sexo_creador;
                            $key->nombre_grupo_alias = $sql_creador->nombre_grupo_alias;
                            // mysqli_next_result( $this->db->conn_id );
                            //fin

                            //destinatario
                            $sql_destinatario = "SELECT users.first_name nombre, users.last_name apellido, users.foto, users.id user_id_destinatario, users.sexo from users where users.id = $key->destinatario";
                           // $sql_destinatario = "CALL obtener_eventos_datos_destinatario(".$key->destinatario.")";
                            $sql_destinatario = $this->db->query($sql_destinatario);
                            $sql_destinatario = $sql_destinatario->row();
                            $key->nombre = $sql_destinatario->nombre;
                            $key->apellido = $sql_destinatario->apellido;
                            $key->foto = $sql_destinatario->foto;
                            if($key->foto != NULL)
                            {
                                $key->foto = $this->utilidades->get_url_img().'fotos_usuarios/'.$key->foto;
                            }
                            else
                            {
                                $key->foto = $this->utilidades->get_url_img().'fotos_usuarios/default-avatar.png';
                            }
                            mysqli_next_result( $this->db->conn_id );

                            //agregar foto colegio
                            if($key->logo != NULL)
                            {
                                $key->logo = $this->utilidades->get_url_img().'fotos_colegios/'.$key->logo;
                            }
                            else
                            {
                                $key->logo = $this->utilidades->get_url_img().'fotos_colegios/default_esc.png';
                            }
                            //fin

                            $result[]=$key;
                    }
                    //rol alias
                    if($key->nombre_grupo_alias!=null)
                        $key->nombre_grupo = $key->nombre_grupo_alias;
                    else
                        if( $key->nombre_grupo_aliasM!=null) {
                            if($key->sexo_creador=='F')
                                $key->nombre_grupo = $key->nombre_grupo_aliasF;
                            else
                                $key->nombre_grupo = $key->nombre_grupo_aliasM;
                        }
                    unset($key->nombre_grupo_aliasM);
                    unset($key->nombre_grupo_aliasF);
                    unset($key->nombre_grupo_alias);
                    unset($key->sexo_creador);


                }
                $this->response($result, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'no era para mi el evento',
                    'consulta' =>$this->db->last_query()
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }

    public function obtener_destinatario_filtro_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            $respuesta="Perzonalizado\Varios";
            $idevento = $this->get('id');
            $lista_filtro = $this->modelo_evento->obtener_filtro($idevento);
            $ciclo = 0;
            foreach ($lista_filtro as $dato)
            {
                $ciclo = $ciclo + 1;
            }
            if($ciclo == 1)
            {
                $arreglo_id = explode(",", $lista_filtro[0]->valores);
                $ciclo_valor = 0;
                foreach ($arreglo_id as $valor)
                {
                    $ciclo_valor = $ciclo_valor + 1;
                }
                if($ciclo_valor == 1)
                {
                    if($lista_filtro[0]->filtro == "nivel")
                    {
                        $aux=$this->modelo_nivel->obtener_nivel_id($arreglo_id[0]);
                        $respuesta = $aux->nombre;
                    }
                    if($lista_filtro[0]->filtro == "division")
                    {
                        $aux=$this->modelo_division->obtener_division_id($arreglo_id[0]);
                        $respuesta= $aux->nombanio.' '.$aux->nombdiv;
                    }
                    if($lista_filtro[0]->filtro == "rol")
                    {
                        $aux=$this->modelo_usuario->get_rol_id($arreglo_id[0]);
                        $respuesta= $aux->name;
                        if($aux->id == 3)
                        {
                            $respuesta = "Preceptores";
                        }
                        if($aux->name == 4)
                        {
                            $respuesta = "Docentes";
                        }
                        if($aux->name == 5)
                        {
                            $respuesta = "Tutores";
                        }
                        if($aux->name == 6)
                        {
                            $respuesta = "Alumnos";
                        }
                        if($aux->name == 7)
                        {
                            $respuesta = "Administrativos";
                        }
                        if($aux->name == 8)
                        {
                            $respuesta = "Directivos";
                        }
                    }
                    if($lista_filtro[0]->filtro == "usuario")
                    {
                        $aux=$this->modelo_usuario->get_users_id($arreglo_id[0]);
                        $respuesta= $aux->last_name.' '.$aux->first_name;
                    }
                }
                else
                {
                    $respuesta="Perzonalizado\Varios";
                }
            }
            else
            {
                $respuesta="Perzonalizado\Varios";
            }
            $this->response($respuesta, REST_Controller::HTTP_OK);

        }
    }
    /**
     * Recibe el id de un alumno y devuelve los tutores
    */
    public function traer_tutores($alumnosId)
    {
        if( ($alumnosId!='vacio')&& (count($alumnosId)>0) )
        {
            $idsalumnos = join(',',$alumnosId);
            $tutores = $this->modelo_alumno->get_tutoresXalumnos($idsalumnos);

            $arreTutores = array();

            if($tutores)
                foreach ($tutores as $key) {
                    $arreTutores[] = $key->tutor_id;
                    //$tutoresAlumnos[] = array($key->tutor_id, $key->alumno_id);
                }

            $data['tutores']=$arreTutores;
            //$data['tutoresalumnos']=$tutoresAlumnos;
            return $data;
        }
        else
            return "nada";
    }


    public function traer_usuariosXrolesIds($idcolegio, $nombregrupo, $ids_roles)
    {
        $anioA = $this->utilidades->obtener_planestudioaXcolegio( $idcolegio );
        $planest_Id = $this->utilidades->get_planestudio( $anioA );


        $roles = $this->modelo_usuario->get_rolesXrolesid($ids_roles, $idcolegio);


        $demasRoles = array(); $idsalu = 0; $arreDoce = 0; $arrePrese = 0;
        $divisiones = null;

        if($nombregrupo=='Docente')
        {
            $this->load->model("modelo_division");
            $this->load->model("modelo_alumno");

            $diviDoce = $this->modelo_division->divisionesXdocentes($idcolegio,$planest_Id, $userid);

            if( count($diviDoce)>0)
            {
                $arre=array();
                foreach ($diviDoce as $key) {
                    $arre[] = $key->divisiones_id;
                }
                $divisiones = join(',',$arre); //las divisiones del preceptor
            }

            //a partir  de aqui el codigo es exacamente igual que para preceptor

            foreach ($roles as $rol)
            {
                switch ($rol->name)
                {
                    case 'Alumno':
                        if(count($divisiones))
                        {
                            $cicloA = $this->utilidades->obtener_cicloaXcolegio( $colegio_id );
                            $alumnos = $this->modelo_alumno->get_alumnosXdivisiones($divisiones,$cicloA);
                            if( count($alumnos)>0 )
                            {
                                $arreAlum=array();
                                foreach ($alumnos as $key) {
                                    $arreAlum[] = $key->user_id;
                                }
                                $idsalu = join(',',$arreAlum); //los alumnos del preceptor
                                //print_r('IDSALUMNOS: '.$idsalu.'$$');
                            }
                            else $idsalu = 0;
                        }
                        else $idsalu = 0;

                        break;

                    case 'Docente':
                        if(count($divisiones))
                        {
                            $docentes = $this->modelo_alumno->get_docentesXdivisiones($divisiones);
                        }
                        else {  //si no tiene divisiones asignadas, traigo todos los docentes del colegio
                                $docentes = $this->modelo_usuario->get_usuariosXrol($idcolegio,"Docente");
                            }


                        //if( count($docentes)>0 )
                        if( $docentes )
                        {
                            $arreDoce=array();
                            foreach ($docentes as $key) {
                                $arreDoce[] = $key->user_id;
                            }
                            $arreDoce = join(',',$arreDoce);
                        }
                        else $arreDoce = 0;

                        break;

                    case 'Preceptor':
                        if(count($divisiones))
                        {
                            $preceptores = $this->modelo_alumno->get_preceptoresXdivisiones($divisiones);

                            if( ! $preceptores )
                            {  //si no tiene divisiones asignadas, traigo todos los preceptores del colegio
                                $preceptores = $this->modelo_usuario->get_usuariosXrol($idcolegio,"Preceptor");
                            }
                        }
                        else {  //si no hay divisiones cargadas, traigo todos los preceptores del colegio
                                $preceptores = $this->modelo_usuario->get_usuariosXrol($idcolegio,"Preceptor");
                            }

                        //print_r($preceptores);
                        if( $preceptores)
                        //if( count($preceptores)>0 )
                        {
                            $arrePrese=array();
                            foreach ($preceptores as $key) {
                                $arrePrese[] = $key->user_id;
                            }
                            $arrePrese = join(',',$arrePrese);
                        }
                        else $arrePrese = 0;

                        break;

                    default:
                        $demasRoles[]=$rol->id;
                        break;
                }
            }

            $idsaux=$idsalu.",".$arrePrese.",".$arreDoce;

            $resultAlgo = $this->modelo_usuario->get_usuariosEspecificos2($idcolegio, $idsaux, $ids_roles);
            //print_r("A VER: ". $resultAlgo);
            if( $resultAlgo )
            {
                if( count($demasRoles)>0 )
                {
                    $demasRolesId = join(',',$demasRoles);
                    $resultOtro = $this->modelo_usuario->get_usuariosColegio2($idcolegio, $demasRolesId);
                    if( $resultOtro )
                        $Todos = array_merge($resultAlgo, $resultOtro);
                    else
                        $Todos = $resultAlgo;

                    $result=$Todos;
                }
                else $result=$resultAlgo;
            }
            else
                if( count($demasRoles)>0 )
                {
                    $demasRolesId = join(',',$demasRoles);
                    $resultOtro = $this->modelo_usuario->get_usuariosColegio2($idcolegio, $demasRolesId);
                    $result=$resultOtro;
                }

        }
        else
            if($nombregrupo=='Preceptor')
            {//if preceptor
                $this->load->model("modelo_division");
                $this->load->model("modelo_alumno");
                $diviPrece = $this->modelo_division->divisionesXpreceptores($idcolegio,$planest_Id,$userid);

                if( count($diviPrece)>0)
                {
                    $arre=array();
                    foreach ($diviPrece as $key) {
                        $arre[] = $key->division_id;
                    }
                    $divisiones = join(',',$arre); //las divisiones del preceptor
                }


                foreach ($roles as $rol)
                {
                    switch ($rol->name)
                    {
                        case 'Alumno':
                            if(count($divisiones))
                            {
                                $cicloA = $this->utilidades->obtener_cicloaXcolegio( $colegio_id );
                                $alumnos = $this->modelo_alumno->get_alumnosXdivisiones($divisiones,$cicloA);
                                if( count($alumnos)>0 )
                                {
                                    $arreAlum=array();
                                    foreach ($alumnos as $key) {
                                        $arreAlum[] = $key->user_id;
                                    }
                                    $idsalu = join(',',$arreAlum); //los alumnos del preceptor
                                    //print_r('IDSALUMNOS: '.$idsalu.'$$');
                                }
                                else $idsalu = 0;
                            }
                            else $idsalu = 0;

                            break;

                        case 'Docente':
                            /*if(count($divisiones))
                            {
                                $docentes = $this->modelo_alumno->get_docentesXdivisiones($divisiones);
                                if( count($docentes)>0 )
                                {
                                    $arreDoce=array();
                                    foreach ($docentes as $key) {
                                        $arreDoce[] = $key->user_id;
                                    }
                                    $arreDoce = join(',',$arreDoce);
                                }
                                else $arreDoce = 0;
                            }
                            else $arreDoce = 0;*/

                            if(count($divisiones))
                            {
                                $docentes = $this->modelo_alumno->get_docentesXdivisiones($divisiones);

                                if(! $docentes)
                                {  //si no tiene divisiones asignadas, traigo todos los docentes del colegio
                                    $docentes = $this->modelo_usuario->get_usuariosXrol($idcolegio,"Docente");
                                }
                            }
                            else {  //si no hay divisiones cargadas, traigo todos los docentes del colegio
                                    $docentes = $this->modelo_usuario->get_usuariosXrol($idcolegio,"Docente");
                                }

                            //if( count($docentes)>0 )
                            if( $docentes )
                            {
                                $arreDoce=array();
                                foreach ($docentes as $key) {
                                    $arreDoce[] = $key->user_id;
                                }
                                $arreDoce = join(',',$arreDoce);
                            }
                            else $arreDoce = 0;

                            break;

                        case 'Preceptor':
                            /*if(count($divisiones))
                            {
                                $preceptores = $this->modelo_alumno->get_preceptoresXdivisiones($divisiones);

                                $arrePrese=array();
                                if( $preceptores)
                                {
                                    foreach ($preceptores as $key) {
                                        $arrePrese[] = $key->user_id;
                                    }
                                    $arrePrese = join(',',$arrePrese);
                                }
                                else $arrePrese = 0;
                            }
                            else $arrePrese = 0;*/

                            if(count($divisiones))
                            {
                                $preceptores = $this->modelo_alumno->get_preceptoresXdivisiones($divisiones);
                            }
                            else {  //si no tiene divisiones asignadas, traigo todos los preceptores del colegio
                                    $preceptores = $this->modelo_usuario->get_usuariosXrol($idcolegio,"Preceptor");
                                }

                            if( $preceptores)
                            //if( count($preceptores)>0 )
                            {
                                $arrePrese=array();
                                foreach ($preceptores as $key) {
                                    $arrePrese[] = $key->user_id;
                                }
                                $arrePrese = join(',',$arrePrese);
                            }
                            else $arrePrese = 0;

                            break;

                        default:
                            $demasRoles[]=$rol->id;
                            break;
                    }
                }


                $idsaux=$idsalu.",".$arrePrese.",".$arreDoce;
                //print_r($idsaux);
                $resultAlgo = $this->modelo_usuario->get_usuariosEspecificos2($idcolegio, $idsaux, $ids_roles);

                if( $resultAlgo )
                {
                    if( count($demasRoles)>0 )
                    {
                        $demasRolesId = join(',',$demasRoles);
                        $resultOtro = $this->modelo_usuario->get_usuariosColegio2($idcolegio, $demasRolesId);
                        if( $resultOtro )
                            $Todos = array_merge($resultAlgo, $resultOtro);
                        else
                            $Todos = $resultAlgo;

                        $result=$Todos;
                    }
                    else $result=$resultAlgo;
                }
                else
                    if( count($demasRoles)>0 )
                    {
                        $demasRolesId = join(',',$demasRoles);
                        $resultOtro = $this->modelo_usuario->get_usuariosColegio2($idcolegio, $demasRolesId);
                        $result=$resultOtro;
                    }


            }//fin if preceptor
            else
                $result = $this->modelo_usuario->get_usuariosColegio2($idcolegio, $ids_roles);


        if (isset($result))
            return $result;
        else return false;
    }

    public function traer_destinos($parametros, $idcolegio)
    {
        if( count($parametros->divisiones) > 0 )  //hay divisiones
        {
            $divisiones = $parametros->divisiones;
            $idsDivi = join(',',$divisiones);
        }
        else
            $idsDivi = $parametros->divisionestodas;  //si no selecciona division, entonces traigo todos los del select

         //print_r($parametros);
        //print_r($idsDivi);

        $idsalu=0; $arrePrese=0; $arreDoce=0;
        if (count($parametros->roles) > 0)  //hay roles, se supone q siempre tiene que haber al menos un rol (alumno por defecto). pero por las dudas si no viene ninguno, se va al else
        {
            //print_r('hay roles-');

            //$resultroles = $this->modelo_usuario->get_rolesXrolesname($namesroles, $idcolegio);


            $alumnosId = 0;
            foreach ($parametros->roles as $rol)
            {
                $rol = trim($rol); //elimino espacios por seguridad
                switch($rol)
                {
                    case 'Alumno':
                            $cicloA = $this->utilidades->obtener_cicloaXcolegio( $idcolegio );
                            $alumnos = $this->modelo_alumno->get_alumnosXdivisiones($idsDivi,$cicloA);

                            $arreAlum=array();
                            if( ($alumnos)&& (isset($parametros->ambos)) )
                            {
                                foreach ($alumnos as $key) {
                                    $arreAlum[] = $key->user_id;
                                }

                                switch ($parametros->ambos) {
                                    case 'ambos':
                                        $data = $this->traer_tutores($arreAlum);
                                        //$idsalu = array_merge($arreAlum, $data['tutores']);
                                        $idsalu = $arreAlum;
                                        $alumnosId = $arreAlum;
                                        $tutoresId = $data['tutores'];
                                        break;
                                    case 'soloalumno':
                                        $idsalu = $arreAlum;
                                        $tutoresId = 0;
                                        $alumnosId = 0;
                                        break;
                                    case 'solotutor':
                                        $data = $this->traer_tutores($arreAlum);
                                        $idsalu = 0;
                                        $tutoresId = $data['tutores'];
                                        $alumnosId = $arreAlum;

                                        break;
                                    default:
                                        # code... nunca deberia entrar aqui'
                                        $idsalu = 0;
                                        $tutoresId = 0;
                                        $alumnosId = 0;
                                        break;
                                }

                                 //tutoresalumnos
                                if($alumnosId)
                                    $alumnosId = join(',',$alumnosId);
                                else $alumnosId = 0;

                                if($idsalu)
                                    $idsalu = join(',',$idsalu); //solo alumnos
                                else $idsalu = 0;

                                if($tutoresId)
                                    $tutoresId = join(',',$tutoresId); //solo tutores
                                else  $tutoresId = 0;
                                //$alumnosId = join(',',$arreAlum); //solo alumnos
                            }
                            else {
                                $idsalu = 0;
                                $tutoresId = 0;
                                $alumnosId = 0;
                            }
                            break;
                    case 'Preceptor':  $preceptores = $this->modelo_alumno->get_preceptoresXdivisiones($idsDivi);

                                $arrePrese=array();

                                if( $preceptores)
                                {
                                    foreach ($preceptores as $key) {
                                        $arrePrese[] = $key->user_id;
                                    }
                                    //$idspre[] = $arrePrese;
                                    $arrePrese = join(',',$arrePrese);
                                }
                                else $arrePrese = 0;
                                break;
                    case 'Docente':  $docentes = $this->modelo_alumno->get_docentesXdivisiones($idsDivi);

                                if( $docentes)
                                {
                                    $arreDoce=array();
                                    foreach ($docentes as $key) {
                                        $arreDoce[] = $key->user_id;
                                    }
                                    //$idsdoce[] = $arreDoce;
                                    $arreDoce = join(',',$arreDoce);
                                }
                                // else print_r("no entró");
                                else $arreDoce = 0;
                                break;
                }
            }
        }
        else
            {
                $cicloA = $this->utilidades->obtener_cicloaXcolegio( $idcolegio );
                $alumnos = $this->modelo_alumno->get_alumnosXdivisiones($idsDivi,$cicloA);

                $arreAlum=array();
                if($alumnos)
                {
                    foreach ($alumnos as $key) {
                        $arreAlum[] = $key->user_id;
                    }

                    $data = $this->traer_tutores($arreAlum);

                    $idsalu = array_merge($arreAlum, $data['tutores']); //tutoresalumnos
                    $idsalu = join(',',$idsalu);
                }
                else $idsalu = 0;
            }


       // print_r($idsalu."--".$arrePrese."--".$arreDoce);

        $aux['ids']=$idsalu.",".$arrePrese.",".$arreDoce;

        //print_r($aux['ids']."@@".$alumnosId);

        $data['todos']=$aux['ids']; //ecepto tutores
        if(isset($tutoresId) == null)
        {
            $tutoresId = 0;
        }
        if(isset($alumnosId) == null)
        {
            $alumnosId = 0;
        }
        $data['alumnos']=$alumnosId;
        $data['tutores']=$tutoresId;

        return $data;
    }

    public function traer_destinosDiri($parametros, $idcolegio)
    {
        $tutores_new = 0;
        if(count($parametros->usuarios) > 0)  //hay usuarios
        {
            $usuarios = $parametros->usuarios;
            //print_r($usuarios);
            //damian parte de seleccion de tutores
            $aux_idsusu = $usuarios;//lo guardo aca antes de romper la variable original
            $arre_tutores= array();
            foreach ($aux_idsusu as $id_usu_select)  //ahora debo buscar los  posibles tutores
            {
                //echo $id_usu_select."<br>";
                if($this->modelo_tutor->is_tutor_en_colegio($id_usu_select, $idcolegio) == 1)
                {
                    $arre_tutores[] = $id_usu_select;
                }

            }
           // echo "arreglo: <br>";
           // print_r($arre_tutores);
           // echo "total: ".count($arre_tutores);
            if(count($arre_tutores) > 0)
            {
                $tutores_new = join(',',$arre_tutores);

            }
          //  echo "<br> tutores_new: ".$tutores_new;
            //fin parte seleccion tutores
            $idsusu = join(',',$usuarios);
            
            
        }
        else
        {
                $usuarios=$parametros->usuariostodos; //si no selecciona usuarios, trae todos los del select usuario, o sea solo los permitidos.
                //damian parte de seleccion de tutores
                $aux_idsusu = $usuarios;//lo guardo aca antes de romper la variable original
                $arre_tutores= array();
                foreach ($aux_idsusu as $id_usu_select)  //ahora debo buscar los  posibles tutores
                {
                    //echo $id_usu_select."<br>";
                    if($this->modelo_tutor->is_tutor_en_colegio($id_usu_select, $idcolegio) == 1)
                    {
                        $arre_tutores[] = $id_usu_select;
                    }

                }
                if(count($arre_tutores) > 0)
                {
                    $tutores_new = join(',',$arre_tutores);

                }
                //fin parte seleccion tutores
                $idsusu = join(',',$usuarios);
        }



        $resultAlumnos = $this->modelo_usuario->get_usuariosXrol2($idcolegio, "Alumno", $idsusu);
        //print_r($parametros);
       // die();
        $alumnos = array();
        if($resultAlumnos)  //hay alumnos
        {
            // print_r('hay_alumnos:');
            //print_r($parametros[2]);

            foreach ($resultAlumnos as $key)
            {
                $alumnos[] = $key->id;
            }

            switch ($parametros->ambos)
            {
                case 'ambos':
                    $data = $this->traer_tutores($alumnos);
                    //print_r($data);
                    $idsusu = $idsusu;
                    $alumnosId = $alumnos;
                    $tutoresId = $data['tutores'];
                    break;
                case 'soloalumno':
                    //$idsusu = join(',',$usuarios);
                    $idsusu=$idsusu; //mp hacer nada
                    $tutoresId = 0;
                    $alumnosId = 0;
                    break;
                case 'solotutor':
                    $data = $this->traer_tutores($alumnos);

                    $diferencia = array_diff($usuarios, $alumnos); //todos menos los alumnos

                    /*if( ($diferencia) && ($data['tutores']) )
                        $usuarios = array_merge($diferencia, $data['tutores']);
                    else  if($diferencia)
                                $usuarios = $diferencia;
                            else $usuarios = $data['tutores'];*/

                    if($diferencia)
                        $usuarios = $diferencia;
                    else $usuarios = 0;



                    if($usuarios)
                        $idsusu = join(',',$usuarios);
                    else $idsusu = 0;

                    if($data['tutores'])
                        $tutoresId = $data['tutores'];
                    else $tutoresId = 0;

                    if($alumnos)
                        $alumnosId = $alumnos;
                    else $alumnosId = 0;

                    break;
                default:
                    # code... nunca deberia entrar aqui'
                    $tutoresId = 0;
                    $alumnosId = 0;
                    break;
            }

            if($alumnosId)
                $alumnosId = join(',',$alumnosId);
            else $alumnosId = 0;

            if($tutoresId)
                $tutoresId = join(',',$tutoresId); //solo tutores
            else  $tutoresId = 0;

            //print_r($idsusu); echo "---";
            //print_r($alumnosId);echo "---";
            //print_r($tutoresId);


            //$alumnosId=join(',',$alumnos);
        }
        else { //print_r('Nohay_alumnos');

            $idsusu = $idsusu;
            $tutoresId = 0;
            $alumnosId = 0;
        }


        //print_r($idsusu."@@".$alumnosId);

       // print_r($idsusu."@@".$alumnosId);

        if($tutores_new != 0)
        {
            if($tutoresId != 0)
            {
                $tutoresId = $tutoresId.','.$tutores_new;
            }
            else
            {
                $tutoresId =$tutores_new;
            }
        }
        $data['todos']=$idsusu;
        $data['alumnos']=$alumnosId;
        $data['tutores']=$tutoresId;

      //  print_r($data);
        //die();

        return $data;
    }



    //inserta pero no envia: supervisa, paso 1
    public function noenviar_eventosusers($usuarios, $ultimoId, $data, $idcolegio, $userid, $motivo){
        $usuariosEmail = array();

        $eu_movil = array();

        if($usuarios){
            foreach ($usuarios as $row )
            {
                $enviar=0;
                if($row->name == 'Tutor')
                {
                    if($row->alumno_id != null)
                        $enviar = $row->alumno_id; //guardo el hijo
                    else
                        $enviar = $row->user_id;
                }
                else //preceptor, docente, alumno
                {
                    $enviar = $row->user_id; //guardo el mismo usuario
                }


                $datos2 = array( 'eventos_id'=>$ultimoId, 'users_id'=>$row->user_id, 'destinatario'=>$enviar, 'enviado'=>0,'motivo'=>$motivo, 'medio'=>'n', 'alta'=>'a' , 'group_id'=>$row->group_id);
                //$this->modelo_evento->insertar_eventousers($datos2); //inserto luego actualizo campo 'enviado'.
                $eu_movil[] = $datos2;
            }

            if(count($eu_movil)>0)
                $this->modelo_evento->insertar_eventousers2($eu_movil);
        }

    }

    //no inserta, pero envia y hace update: supervisa, paso 2
    public function soloenviar_eventosusers($eventosusers, $data, $fichero, $fichero2){ //data = evento datos
        $usuariosEmail = array();

        $idevento = $data->id;
        $idcolegio = $data->colegio_id;

        $datacolegio=$this->modelo_colegio->get_colegio($idcolegio); //colegio


        $eu_email = array();
        $eu_movil = array();
        $detallesPush = array();

        if($eventosusers)
            foreach ($eventosusers as $row )
            {
                $row->user_id = $row->users_id; //Correctivo

                $eventosusers_id = $row->id;


                //enviar notificacion o mail
                $datos_push = array("message" => $data->titulo,
                    "userid" => $this->utilidades->encriptar($row->user_id), "nombrecolegio"=>$datacolegio->row()->nombre);


                $result = $this->enviar_notificacion_usuarios3($row,$datos_push,$data); //envia y update.

                if($result['result'] == 0) //si es 0 no tiene movil o fallo el envio, enviar por email
                {
                    if ( valid_email($row->email) )
                    {
                            $usuariosEmail[] = $row->email;
                            $motivo=null;
                    }
                    else
                    {
                            $motivo='email_invalid';
                    }

                    $datos = array();
                    $datos['enviado'] = 0;
                    $datos['medio'] = 'e';
                    $datos['motivo'] = $motivo;
                    $datos['id'] = $eventosusers_id;
                    //$this->modelo_evento->update_eventosusers($datos, $eventosusers_id);
                    $eu_email[] = $datos;
                }
                else {
                        $result['data']['id'] = $eventosusers_id;
                        $eu_movil[] = $result['data'];
                    }

                if( isset($result['detallesPush']) && (count($result['detallesPush'])>0) ){
                    if(count($detallesPush)==0)
                        $detallesPush = $result['detallesPush'];
                    else
                        $detallesPush = array_merge($detallesPush,$result['detallesPush']);
                }
            }


        /*print_r($eu_movil);
        echo "<br><br>";
        print_r($detallesPush);
        echo "<br><br>";
        print_r($eu_email); */
        //die();


        if(count($eu_movil)>0)
            $this->modelo_evento->update_eventosusers2($eu_movil);

        if(count($detallesPush)>0)
            $this->modelo_evento->insertar_eventousers_detalle2($detallesPush);

        if(count($eu_email)>0)
                $this->modelo_evento->update_eventosusers2($eu_email);


        if( (isset($usuariosEmail)) && (count($usuariosEmail)>0) ) //enviar por GMAIL google
        {

            //$destinos = array_column($usuariosEmail, 1);
            $destinos = $usuariosEmail;

            $from1 = "contacto@smtp.e-nodos.com";
            $from2 = $datacolegio->row()->nombre;


            $asunto="Nuevo Evento: ".$data->titulo;
            $datos['remitente'] = $this->modelo_usuario->get_usuariosEspecificos($data->users_id);
            $datos['titulo']= $data->titulo;
            $datos['fechaInicio']= $data->fechaInicio;
            $datos['descripcion']= $data->descripcion;
            $datos['lugar']= $data->lugar;
            $datos['colegio_nombre']= $datacolegio->row()->nombre;
            $datos['data']= $data;


            $mensaje = $this->load->view('evento/mail_evento', $datos,true);
            $enviado = $this->usuarios->enviar_email($destinos, $mensaje, $from1, $from2, $asunto);
            //print_r($enviado);
            if($enviado['enviado'])
            {
                $valores=array('enviado'=>1);
                //actualiza todos menos los que tiene email invalido
                $this->modelo_evento->update_enviado_eventousers($idevento,'e','a',$valores);
            }
            else {

                    $contenido2 = "\n\n\n INSERT IdColegio:".$idcolegio."  IdEvento:".$idevento."\n";
                    $contenido2 .= "Error:".$enviado['error']."\n";
                    file_put_contents($fichero2, $contenido2, FILE_APPEND | LOCK_EX);

                }

            $contenido = "Emails:".implode(';', $destinos);
            file_put_contents($fichero, $contenido, FILE_APPEND | LOCK_EX);

        }
    }

    //inserta y envia
    public function enviar_eventosusers($usuarios, $ultimoId, $data, $idcolegio, $userid, $fichero, $fichero2){
        $usuariosEmail = array();

        $datacolegio=$this->modelo_colegio->get_colegio($idcolegio); //colegio

        $eu_email = array();
        $eu_movil = array();
        $detallesPush = array();
        if($usuarios)
            foreach ($usuarios as $row )
            {
                $enviar=0;
                if($row->name == 'Tutor')
                {
                    if($row->alumno_id != null)
                        $enviar = $row->alumno_id; //guardo el hijo
                    else
                        $enviar = $row->user_id;
                }
                else //preceptor, docente, alumno
                {
                    $enviar = $row->user_id; //guardo el mismo usuario
                }

                //enviar notificacion o mail
                $datos_push = array("message" => $data->titulo,
                    "userid" => $this->utilidades->encriptar($row->user_id), "nombrecolegio"=>$datacolegio->row()->nombre);

                $datos = array( 'eventos_id'=>$ultimoId, 'users_id'=>$row->user_id, 'destinatario'=>$enviar, 'medio'=>'m', 'alta'=>'a');

                $result = $this->enviar_notificacion_usuarios($row,$datos_push,$datos); //envia e inserta.

                //$result['data']

                if($result['result'] == 0) //si es 0 no tiene movil o fallo el envio, enviar por email
                {
                    //no se inserta en eventosusers

                    if ( valid_email($row->email) )
                    {
                            $usuariosEmail[] = $row->email;
                            $motivo=null;
                    }
                    else
                    {
                            $motivo='email_invalid';
                    }

                    $eu_email[]= array( 'eventos_id'=>$ultimoId, 'users_id'=>$row->user_id, 'destinatario'=>$enviar, 'enviado'=>0,'motivo'=>$motivo, 'medio'=>'e', 'alta'=>'a');
                    //$this->modelo_evento->insertar_eventousers($datos2); //inserto luego actualizo campo 'enviado'.
                }
                else {
                        //$this->modelo_evento->insertar_eventousers($result['data']); //insertar
                        //$eventosusers_id = $this->db->insert_id();
                        $eu_movil[] = $result['data'];
                    }

                if( isset($result['detallesPush']) && (count($result['detallesPush'])>0) ){
                    if(count($detallesPush)==0)
                        $detallesPush = $result['detallesPush'];
                    else
                        $detallesPush = array_merge($detallesPush,$result['detallesPush']);
                }
            }

        /*print_r($eu_movil);
        echo "<br><br>";
        print_r($detallesPush);
        echo "<br><br>";
        print_r($eu_email);
        //die();*/
        if(count($eu_movil)>0)
            $this->modelo_evento->insertar_eventousers2($eu_movil);

        if(count($detallesPush)>0)
            $this->modelo_evento->insertar_eventousers_detalle2($detallesPush);

        if(count($eu_email)>0)
            $this->modelo_evento->insertar_eventousers2($eu_email); //inserto luego actualizo campo 'enviado'.


        if( (isset($usuariosEmail)) && (count($usuariosEmail)>0) ) //enviar por GMAIL google
        {

            //$destinos = array_column($usuariosEmail, 1);
            $destinos = $usuariosEmail;

            $from1 = "contacto@smtp.e-nodos.com";
            $from2 = $datacolegio->row()->nombre;


            $asunto="Nuevo Evento: ".$data->titulo;
            $datos['remitente'] = $this->modelo_usuario->get_usuariosEspecificos($userid);
            $datos['titulo']= $data->titulo;
            $datos['fechaInicio']= $data->fechaInicio;
            $datos['descripcion']= $data->descripcion;
            $datos['lugar']= $data->lugar;
            $datos['colegio_nombre']= $datacolegio->row()->nombre;
            $datos['data']= $data;


            $mensaje = $this->load->view('evento/mail_evento', $datos,true);
            $enviado = $this->usuarios->enviar_email($destinos, $mensaje, $from1, $from2, $asunto);
            //print_r($enviado);
            if($enviado['enviado'])
            {
                $valores=array('enviado'=>1);
                //actualiza todos menos los que tiene email invalido
                $this->modelo_evento->update_enviado_eventousers($ultimoId,'e','a',$valores);
            }
            else {
                    $contenido2 = "\n\n\n INSERT IdColegio:".$idcolegio."  IdEvento:".$ultimoId."\n";
                    $contenido2 .= "Error:".$enviado['error']."\n";
                    file_put_contents($fichero2, $contenido2, FILE_APPEND | LOCK_EX);
                }

            $contenido = "Emeils:".implode(';', $destinos);
           // file_put_contents($fichero, $contenido, FILE_APPEND | LOCK_EX);

        }
    }

    //inserta y envia solo los que no se insertaron
    /*public function enviar_eventosusers_faltantes($usuarios, $ultimoId, $data, $idcolegio, $userid, $fichero, $fichero2){
        $usuariosEmail = array();

        $datacolegio=$this->modelo_colegio->get_colegio($idcolegio); //colegio

        $eu_email = array();
        $eu_movil = array();
        $detallesPush = array();
        if($usuarios)
            foreach ($usuarios as $row )
            {
                    $enviar=0;
                    if($row->name == 'Tutor')
                    {
                        if($row->alumno_id != null)
                            $enviar = $row->alumno_id; //guardo el hijo
                        else
                            $enviar = $row->user_id;
                    }
                    else //preceptor, docente, alumno
                    {
                        $enviar = $row->user_id; //guardo el mismo usuario
                    }

                    //enviar notificacion o mail
                    $datos_push = array("message" => $data->titulo,
                        "userid" => $this->utilidades->encriptar($row->user_id), "nombrecolegio"=>$datacolegio->row()->nombre);

                    $datos = array( 'eventos_id'=>$ultimoId, 'users_id'=>$row->user_id, 'destinatario'=>$enviar, 'medio'=>'m', 'alta'=>'a');


                    $result = $this->enviar_notificacion_usuarios($row,$datos_push,$datos); //envia e inserta.


                    if($result['result'] == 0) //si es 0 no tiene movil o fallo el envio, enviar por email
                    {
                        //$usuariosEmail[] = array($row->user_id, $row->email);

                        if ( valid_email($row->email) )
                        {
                            $usuariosEmail[] = $row->email;
                            $motivo=null;
                        }
                        else
                        {
                            $motivo='email_invalid';
                        }

                        $eu_email[]= array( 'eventos_id'=>$ultimoId, 'users_id'=>$row->user_id, 'destinatario'=>$enviar, 'enviado'=>0,'motivo'=>$motivo, 'medio'=>'e', 'alta'=>'a');
                        //$this->modelo_evento->insertar_eventousers($datos2); //inserto luego actualizo campo 'enviado'.
                    }
                    else {
                            //$this->modelo_evento->insertar_eventousers($result['data']); //insertar
                            //$eventosusers_id = $this->db->insert_id();
                            $eu_movil[] = $result['data'];
                        }

                    if( isset($result['detallesPush']) && (count($result['detallesPush'])>0) ){

                        if(count($detallesPush)==0)
                            $detallesPush = $result['detallesPush'];
                        else
                            $detallesPush = array_merge($detallesPush,$result['detallesPush']);

                    }


                //}
            }

        if( (isset($usuariosEmail)) && (count($usuariosEmail)>0) ) //enviar por GMAIL google
        {
            //$destinos = array_column($usuariosEmail, 1);
            $destinos = $usuariosEmail;

            $from1 = "contacto@smtp.e-nodos.com";
            $from2 = $datacolegio->row()->nombre;


            $asunto="Nuevo Evento: ".$data->titulo;
            $datos['remitente'] = $this->modelo_usuario->get_usuariosEspecificos($userid);
            $datos['titulo']= $data->titulo;
            $datos['fechaInicio']= $data->fechaInicio;
            $datos['descripcion']= $data->descripcion;
            $datos['lugar']= $data->lugar;
            $datos['colegio_nombre']= $datacolegio->row()->nombre;
            $datos['data']= $data;


            $mensaje = $this->load->view('evento/mail_evento', $datos,true);
            $enviado = $this->usuarios->enviar_email($destinos, $mensaje, $from1, $from2, $asunto);
            //print_r($enviado);
            if($enviado['enviado'])
            {
                $valores=array('enviado'=>1);
                //actualiza todos menos los que tiene email invalido
                $this->modelo_evento->update_enviado_eventousers($ultimoId,'e','a',$valores);
            }
            else {

                    $contenido2 = "\n\n\n INSERT IdColegio:".$idcolegio."  IdEvento:".$ultimoId."\n";
                    $contenido2 .= "Error:".$enviado['error']."\n";
                    file_put_contents($fichero2, $contenido2, FILE_APPEND | LOCK_EX);

                }

            $contenido = "Emeils:".implode(';', $destinos);
            file_put_contents($fichero, $contenido, FILE_APPEND | LOCK_EX);

        }
    }*/

    /*
    public function evento_post()  //ANDRES -> Original
        {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
        $pin = $_POST['pin'];
        $existe = $this->utilidades->verifica_pin($pin);
        if ($existe == 1)
        {
            $data = json_decode($_POST['data']) ; //datos del evento
            $filtro = $_POST['filtro']; //datos del los destinos
            $idcolegio = $_POST['idcolegio'];
            $idgrupo = $_POST['idgrupo'];
            $idsfiles = $_POST['idsfiles'];

            $parametros = json_decode($_POST['parametros']);
            $parametros2 = json_decode($_POST['parametros']);

            if(isset($parametros->usuariostodos)){
                unset($parametros2->usuariostodos);
                $parametrosJson = json_encode($parametros2);
            }
            else
                $parametrosJson = $_POST['parametros'];



            $data->users_id = $userid;
            $data->group_id = $idgrupo;
            $data->colegio_id = $idcolegio;


            $reglas = $this->modelo_permisos->get_reglas($idcolegio,$idgrupo);
            $standby = 0;
            if($reglas)
                if($reglas->supervisado != null){
                    $standby = 1;
                    $data->standby = 1;
                }


           // print_r($parametros); die();

            if($data->fechaInicio == '')
                $data->fechaInicio=null;
            if($data->fechafin == '')
                $data->fechafin=null;

            $result = $this->modelo_evento->insertar_evento($data);

            if ($result)
            {
                $ultimoId=$this->db->insert_id();

                if($idsfiles!='')
                {
                    $this->load->model("modelo_upload");
                    $this->modelo_upload->set_eventoId($ultimoId, $idsfiles);
                }

                $this->load->helper('email');
                $urlapi = $this->utilidades->get_url_api();
                //$fichero = $urlapi."application/controllers/api/v1/log_eventos.txt";
                //$fichero2 = $urlapi."application/controllers/api/v1/log_emails.txt";


                //inserta filtro.............................................................
                switch($filtro)
                {
                    case 't'://todos

                        $datafiltro = array ('evento_id'=>$ultimoId, 'tipo'=>'t', 'colegio'=>$idcolegio);
                        $this->modelo_evento->insertar_filtroevento($datafiltro);

                       // $usuarios = $this->modelo_usuario->get_usuariosColegio_2($idcolegio);
                       // $arre=array();
                       // foreach ($usuarios as $row )
                       // {
                       //    $arre[]=$row->user_id;
                       // }
                       
                        $destinos['alumnos'] = 0;
                        $destinos['tutores'] = 0;

                        $usuarios = $this->modelo_usuario->get_usuariosParaEnviar3(0, $idcolegio);

                        $arre=array();
                        foreach ($usuarios as $row )
                        {
                           $arre[]=$row->user_id;
                        }

                        $destinos['todos'] = join(',',$arre);

                        break;

                    case 'd'://dirigido a un/s usuario/s especifico/s

                        if ($parametrosJson) {
                            $datafiltro = array ('evento_id'=>$ultimoId, 'tipo'=>"$filtro", 'filtro'=>$parametrosJson, 'alumno_tutor'=>$parametros->ambos, 'colegio'=>$idcolegio);
                            $this->modelo_evento->insertar_filtroevento($datafiltro);
                        }

                       // print_r($parametros);
                        $destinos = $this->traer_destinosDiri($parametros, $idcolegio);

                        if( (isset($destinos)) && (count($destinos['todos']) > 0)  ) //me aseguro que no venga vacio
                        {
                             $usuarios1 = $this->modelo_usuario->get_usuariosParaEnviar($destinos['todos'], 0, $idcolegio);

                             $usuarios2 = $this->modelo_usuario->get_usuariosParaEnviar2($destinos['tutores'], $destinos['alumnos'], $idcolegio);

                            if( ($usuarios1) && ($usuarios2) )
                                $usuarios = array_merge($usuarios1, $usuarios2);
                            else  if($usuarios1)
                                        $usuarios = $usuarios1;
                                    else $usuarios = $usuarios2;
                        }

                        break;

                    case 'p'://personalizado (nivel, año, division)

                        if ($parametrosJson) {
                            $datafiltro = array ('evento_id'=>$ultimoId, 'tipo'=>"$filtro", 'filtro'=>$parametrosJson, 'alumno_tutor'=>$parametros->ambos, 'colegio'=>$idcolegio);
                            $this->modelo_evento->insertar_filtroevento($datafiltro);
                        }


                        $destinos = $this->traer_destinos($parametros, $idcolegio);
                        //estos ids son de users (no user_group)

                        if( (isset($destinos)) && (count($destinos['todos']) > 0)  ) //me aseguro que no venga vacio
                        {
                             $usuarios1 = $this->modelo_usuario->get_usuariosParaEnviar($destinos['todos'], 0, $idcolegio);

                             $usuarios2 = $this->modelo_usuario->get_usuariosParaEnviar2($destinos['tutores'], $destinos['alumnos'], $idcolegio);

                            if( ($usuarios1) && ($usuarios2) )
                                $usuarios = array_merge($usuarios1, $usuarios2);
                            else  if($usuarios1)
                                        $usuarios = $usuarios1;
                                    else $usuarios = $usuarios2;
                        }


                        break;

                    case 'a'://dirigido a usuarios relacionados con areas

                        if ($parametrosJson) {
                            $datafiltro = array ('evento_id'=>$ultimoId, 'tipo'=>"$filtro", 'filtro'=>$parametrosJson, 'alumno_tutor'=>$parametros->ambos, 'colegio'=>$idcolegio);
                            $this->modelo_evento->insertar_filtroevento($datafiltro);
                        }


                        $destinos = $this->traer_destinosDiri($parametros, $idcolegio);

                        if( (isset($destinos)) && (count($destinos['todos']) > 0)  ) //me aseguro que no venga vacio
                        {
                             $usuarios1 = $this->modelo_usuario->get_usuariosParaEnviar($destinos['todos'], 0, $idcolegio);

                             $usuarios2 = $this->modelo_usuario->get_usuariosParaEnviar2($destinos['tutores'], $destinos['alumnos'], $idcolegio);

                            if( ($usuarios1) && ($usuarios2) )
                                $usuarios = array_merge($usuarios1, $usuarios2);
                            else  if($usuarios1)
                                        $usuarios = $usuarios1;
                                    else $usuarios = $usuarios2;
                        }

                        break;

                }//fin swich

                $contenido = "\n\n\nIdColegio:".$idcolegio."  IdEvento:".$ultimoId."\n";
                $contenido .= "destinosTodos:".$destinos['todos']."\n";
                $contenido .= "destinosAlumnos:".$destinos['alumnos']."\n";
                $contenido .= "destinosTutores:".$destinos['tutores']."\n";
                // usando la bandera FILE_APPEND para añadir el contenido al final del fichero
                // y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
                file_put_contents($fichero, $contenido, FILE_APPEND | LOCK_EX);


                if($standby == 0)
                    $this->enviar_eventosusers($usuarios, $ultimoId, $data, $idcolegio, $userid, $fichero, $fichero2); //inserta y envia
                else
                    $this->noenviar_eventosusers($usuarios, $ultimoId, $data, $idcolegio, $userid, "no_auto"); //inserta y el envio queda en espera. no autorizado.

                //generar contador de eventos enviados
                $sql_eventosusers = "SELECT COUNT(id)  as total_eventos_user FROM `eventosusers` WHERE eventos_id = ".$ultimoId;
                $sql_eventosusers = $this->db->query($sql_eventosusers);
                $sql_eventosusers = $sql_eventosusers->row();
                //print_r($sql_eventosusers);
                $total_eventos_user = $sql_eventosusers->total_eventos_user;

                $sql_update = "UPDATE eventos SET cont_eventosusers= $total_eventos_user WHERE id = ".$ultimoId;
                $sql_update = $this->db->query($sql_update);


                $creador = $this->modelo_usuario->get_usuariosEspecificos($userid);
                foreach ($creador as $row )
                {
                    $fechaA=date('Y-m-d H:i:s');
                    $this->response([
                            'status' => TRUE,
                            'message' => 'Evento guardado',
                            'id' => $ultimoId,
                            'fechaalta'=>$fechaA,
                            'creadorname'=> $row->first_name.' '.$row->last_name,
                        ], REST_Controller::HTTP_OK);
                }
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Error al guardar'
                ]); // NOT_FOUND (404) being the HTTP response code
            }

        }else  $this->response([
                'status' => FALSE,
                'message' => 'pininvalido'
                ], REST_Controller::HTTP_OK);

        }
    }
    */
    
   //Para el envio de comunicados masivos
   public function evento_post()
   {
       /*Si viene la variable batch con el valor 1
       el envio de mensaje toma la ruta "batch" -> envio masivo de eventos 
       sino, sigue la ruta de envio usada originalmente */
        $esBatch = 0;
        if(isset($_POST['batch']))
        {
            $esBatch = $_POST['batch'];
            if( $esBatch == 1)
            {
                $this->batch_evento_post();
            }
        }
        else
        {
            $userid = $this->utilidades->verifica_userid();
            if ($userid == -1)
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'NO LOGUIN'
                ], REST_Controller::HTTP_OK);
            }
            else
            {
             $pin = $_POST['pin'];
             $existe = $this->utilidades->verifica_pin($pin);
            if ($existe == 1)
             {
                $data = json_decode($_POST['data']) ; //datos del evento
                $filtro = $_POST['filtro']; //datos del los destinos
                $idcolegio = $_POST['idcolegio'];
                $idgrupo = $_POST['idgrupo'];
                $idsfiles = $_POST['idsfiles'];
     
                $parametros = json_decode($_POST['parametros']);
                $parametros2 = json_decode($_POST['parametros']);
     
                if(isset($parametros->usuariostodos)){
                    unset($parametros2->usuariostodos);
                    $parametrosJson = json_encode($parametros2);
                }
                else
                    $parametrosJson = $_POST['parametros'];
     
     
     
                $data->users_id = $userid;
                $data->group_id = $idgrupo;
                $data->colegio_id = $idcolegio;
     
     
                $reglas = $this->modelo_permisos->get_reglas($idcolegio,$idgrupo);
                $standby = 0;
                if($reglas)
                    if($reglas->supervisado != null){
                        $standby = 1;
                        $data->standby = 1;
                    }
     
     
                // print_r($parametros); die();
     
                 if($data->fechaInicio == '')
                    $data->fechaInicio='0000-00-00 00:00:00';
                 if($data->fechafin == '')
                    $data->fechafin='0000-00-00 00:00:00';
     
                 $result = $this->modelo_evento->insertar_evento($data);
     
                 if ($result)
                 {
                    $ultimoId=$this->db->insert_id();
     
                    if($idsfiles!='')
                    {
                        $this->load->model("modelo_upload");
                        $this->modelo_upload->set_eventoId($ultimoId, $idsfiles);
                    }
     
                    $this->load->helper('email');
                    $urlapi = $this->utilidades->get_url_api();
                   // $fichero = $urlapi."application/controllers/api/v1/log_eventos.txt";
                   // $fichero2 = $urlapi."application/controllers/api/v1/log_emails.txt";
     
     
                    //inserta filtro.............................................................
                    switch($filtro)
                    {
                        case 't'://todos
     
                            $datafiltro = array ('evento_id'=>$ultimoId, 'tipo'=>'t', 'colegio'=>$idcolegio);
                            $this->modelo_evento->insertar_filtroevento($datafiltro);
     
                            /*$usuarios = $this->modelo_usuario->get_usuariosColegio_2($idcolegio);
                            $arre=array();
                            foreach ($usuarios as $row )
                            {
                               $arre[]=$row->user_id;
                            }*/
     
                            $destinos['alumnos'] = 0;
                            $destinos['tutores'] = 0;
     
                            $usuarios = $this->modelo_usuario->get_usuariosParaEnviar3(0, $idcolegio);
     
                            $arre=array();
                            foreach ($usuarios as $row )
                            {
                               $arre[]=$row->user_id;
                            }
     
                            $destinos['todos'] = join(',',$arre);
     
                            break;
     
                        case 'd'://dirigido a un/s usuario/s especifico/s
     
                            if ($parametrosJson) {
                                $datafiltro = array ('evento_id'=>$ultimoId, 'tipo'=>"$filtro", 'filtro'=>$parametrosJson, 'alumno_tutor'=>$parametros->ambos, 'colegio'=>$idcolegio);
                                $this->modelo_evento->insertar_filtroevento($datafiltro);
                            }
     
                           // print_r($parametros);
                            $destinos = $this->traer_destinosDiri($parametros, $idcolegio);
     
                            if( (isset($destinos)) && (count($destinos['todos']) > 0)  ) //me aseguro que no venga vacio
                            {
                                 $usuarios1 = $this->modelo_usuario->get_usuariosParaEnviar($destinos['todos'], 0, $idcolegio);
     
                                 $usuarios2 = $this->modelo_usuario->get_usuariosParaEnviar2($destinos['tutores'], $destinos['alumnos'], $idcolegio);
     
                                if( ($usuarios1) && ($usuarios2) )
                                    $usuarios = array_merge($usuarios1, $usuarios2);
                                else  if($usuarios1)
                                            $usuarios = $usuarios1;
                                        else $usuarios = $usuarios2;
                            }
     
                            break;
     
                        case 'p'://personalizado (nivel, año, division)
     
                            if ($parametrosJson) {
                                $datafiltro = array ('evento_id'=>$ultimoId, 'tipo'=>"$filtro", 'filtro'=>$parametrosJson, 'alumno_tutor'=>$parametros->ambos, 'colegio'=>$idcolegio);
                                $this->modelo_evento->insertar_filtroevento($datafiltro);
                            }
     
     
                            $destinos = $this->traer_destinos($parametros, $idcolegio);
                            //estos ids son de users (no user_group)
     
                            if( (isset($destinos)) && (count($destinos['todos']) > 0)  ) //me aseguro que no venga vacio
                            {
                                 $usuarios1 = $this->modelo_usuario->get_usuariosParaEnviar($destinos['todos'], 0, $idcolegio);
     
                                 $usuarios2 = $this->modelo_usuario->get_usuariosParaEnviar2($destinos['tutores'], $destinos['alumnos'], $idcolegio);
     
                                if( ($usuarios1) && ($usuarios2) )
                                    $usuarios = array_merge($usuarios1, $usuarios2);
                                else  if($usuarios1)
                                            $usuarios = $usuarios1;
                                        else $usuarios = $usuarios2;
                            }
     
     
                            break;
     
                        case 'a'://dirigido a usuarios relacionados con areas
     
                            if ($parametrosJson) {
                                $datafiltro = array ('evento_id'=>$ultimoId, 'tipo'=>"$filtro", 'filtro'=>$parametrosJson, 'alumno_tutor'=>$parametros->ambos, 'colegio'=>$idcolegio);
                                $this->modelo_evento->insertar_filtroevento($datafiltro);
                            }
     
     
                            $destinos = $this->traer_destinosDiri($parametros, $idcolegio);
     
                            if( (isset($destinos)) && (count($destinos['todos']) > 0)  ) //me aseguro que no venga vacio
                            {
                                 $usuarios1 = $this->modelo_usuario->get_usuariosParaEnviar($destinos['todos'], 0, $idcolegio);
     
                                 $usuarios2 = $this->modelo_usuario->get_usuariosParaEnviar2($destinos['tutores'], $destinos['alumnos'], $idcolegio);
     
                                if( ($usuarios1) && ($usuarios2) )
                                    $usuarios = array_merge($usuarios1, $usuarios2);
                                else  if($usuarios1)
                                            $usuarios = $usuarios1;
                                        else $usuarios = $usuarios2;
                            }
     
                            break;
     
                    }//fin swich
     
                    $contenido = "\n\n\nIdColegio:".$idcolegio."  IdEvento:".$ultimoId."\n";
                    $contenido .= "destinosTodos:".$destinos['todos']."\n";
                    $contenido .= "destinosAlumnos:".$destinos['alumnos']."\n";
                    $contenido .= "destinosTutores:".$destinos['tutores']."\n";
                    // usando la bandera FILE_APPEND para añadir el contenido al final del fichero
                    // y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
                    file_put_contents($fichero, $contenido, FILE_APPEND | LOCK_EX);
     
     
                    if($standby == 0)
                        $this->enviar_eventosusers($usuarios, $ultimoId, $data, $idcolegio, $userid, $fichero, $fichero2); //inserta y envia
                    else
                        $this->noenviar_eventosusers($usuarios, $ultimoId, $data, $idcolegio, $userid, "no_auto"); //inserta y el envio queda en espera. no autorizado.
     
                    //generar contador de eventos enviados
                    $sql_eventosusers = "SELECT COUNT(id)  as total_eventos_user FROM `eventosusers` WHERE eventos_id = ".$ultimoId;
                    $sql_eventosusers = $this->db->query($sql_eventosusers);
                    $sql_eventosusers = $sql_eventosusers->row();
                    //print_r($sql_eventosusers);
                    $total_eventos_user = $sql_eventosusers->total_eventos_user;
     
                    $sql_update = "UPDATE eventos SET cont_eventosusers= $total_eventos_user WHERE id = ".$ultimoId;
                    $sql_update = $this->db->query($sql_update);
     
     
                    $creador = $this->modelo_usuario->get_usuariosEspecificos($userid);
                    foreach ($creador as $row )
                    {
                        $fechaA=date('Y-m-d H:i:s');
                        $this->response([
                                'status' => TRUE,
                                'message' => 'Evento guardado',
                                'id' => $ultimoId,
                                'fechaalta'=>$fechaA,
                                'creadorname'=> $row->first_name.' '.$row->last_name,
                            ], REST_Controller::HTTP_OK);
                    }
                }
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Error al guardar'
                    ]); // NOT_FOUND (404) being the HTTP response code
                }
             }
             else{
                     $this->response([
                     'status' => FALSE,
                     'message' => 'pininvalido'
                     ], REST_Controller::HTTP_OK);
                 } 
            }
        }
   }

    /*public function reintentar_evento_post()  //ANDRES: este inserta y envia solo cuando NO hay eventos_users. pero si hay filtro
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
        $pin = $_POST['pin'];
        $existe = $this->utilidades->verifica_pin($pin);
        if ( ($existe == 1) && ($pin==105) )
        {

            $idevento = $_POST['idevento'];

            $data = $this->modelo_evento->obtener_datosEvento($idevento);
            $idcolegio = $data->colegio_id;
            $idgrupo = $data->group_id;


            $evenusers = $this->modelo_evento->obtener_eventosuser_ByIdevento2($idevento);



            $filtro = $this->modelo_evento->obtener_filtro($idevento);

            //$parametros = json_decode($filtro->filtro, true); //true lo convierte a array, de lo contrario stdclass
            $parametros = json_decode($filtro->filtro);



            $standby = 0;
            if($data->standby == 1)
                $standby = 1;


            $files = $this->modelo_upload->get_files_evento($idevento);


           // print_r($parametros); die();

            if ( ($data) && ($filtro) && (!$evenusers))
            {
                $ultimoId=$data->id;

                if($files!='')
                {
                    $arrefiles = array();
                    foreach ($files as $file ) {
                        $arrefiles[]=$file->id;
                    }
                    $idsfiles = join(',',$arrefiles);
                    $this->modelo_upload->set_eventoId($ultimoId, $idsfiles);
                }

                $this->load->helper('email');
                $urlapi = $this->utilidades->get_url_api();
               //$fichero = $urlapi."application/controllers/api/v1/log_eventos.txt";
               // $fichero2 = $urlapi."application/controllers/api/v1/log_emails.txt";


                //inserta filtro.............................................................
                switch($filtro->tipo)
                {
                    case 't'://todos


                        $usuarios = $this->modelo_usuario->get_usuariosParaEnviar3(0, $idcolegio);


                        break;

                    case 'd'://dirigido a un/s usuario/s especifico/s


                        break;

                    case 'p'://personalizado (nivel, año, division)



                        $destinos = $this->traer_destinos($parametros, $idcolegio);

                        //estos ids son de users (no user_group)

                        if( (isset($destinos)) && (count($destinos['todos']) > 0)  ) //me aseguro que no venga vacio
                        {
                             $usuarios1 = $this->modelo_usuario->get_usuariosParaEnviar($destinos['todos'], 0, $idcolegio);

                             $usuarios2 = $this->modelo_usuario->get_usuariosParaEnviar2($destinos['tutores'], $destinos['alumnos'], $idcolegio);

                            if( ($usuarios1) && ($usuarios2) )
                                $usuarios = array_merge($usuarios1, $usuarios2);
                            else  if($usuarios1)
                                        $usuarios = $usuarios1;
                                    else $usuarios = $usuarios2;
                        }


                        break;

                    case 'a'://dirigido a usuarios relacionados con areas




                        break;

                }//fin swich

                $contenido = "\n\n\nIdColegio:".$idcolegio."  IdEvento:".$ultimoId."\n";
                $contenido .= "destinosTodos:".$destinos['todos']."\n";
                $contenido .= "destinosAlumnos:".$destinos['alumnos']."\n";
                $contenido .= "destinosTutores:".$destinos['tutores']."\n";
                // usando la bandera FILE_APPEND para añadir el contenido al final del fichero
                // y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
                file_put_contents($fichero, $contenido, FILE_APPEND | LOCK_EX);


                if($standby == 0)
                    $this->enviar_eventosusers($usuarios, $ultimoId, $data, $idcolegio, $data->users_id, $fichero, $fichero2); //inserta y envia
                else
                    $this->noenviar_eventosusers($usuarios, $ultimoId, $data, $idcolegio, $data->users_id, "no_auto"); //inserta y el envio queda en espera. no autorizado.



                $creador = $this->modelo_usuario->get_usuariosEspecificos($data->users_id);
                foreach ($creador as $row )
                {
                    $fechaA=date('Y-m-d H:i:s');
                    $this->response([
                            'status' => TRUE,
                            'message' => 'Evento reenviado',
                            'id' => $ultimoId,
                            'fechaalta'=>$fechaA,
                            'creadorname'=> $row->first_name.' '.$row->last_name,
                        ], REST_Controller::HTTP_OK);
                }
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Error al guardar'
                ]); // NOT_FOUND (404) being the HTTP response code
            }

        }else  $this->response([
                'status' => FALSE,
                'message' => 'pininvalido'
                ], REST_Controller::HTTP_OK);

        }
    } */

    public function reintentar_evento2_post()  //ANDRES: este inserta y envia solo aquellos eventos_users que no se insertaron. pero si hay filtro// CAMBIAR NOMBRE A REINTENTAR
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
        $pin = $_POST['pin'];
        $existe = $this->utilidades->verifica_pin($pin);
        if ( ($existe == 1) && ($pin==105) )
        {


            $idevento = $_POST['idevento'];

            $data = $this->modelo_evento->obtener_datosEvento($idevento);
            $idcolegio = $data->colegio_id;
            $idgrupo = $data->group_id;


            $filtro = $this->modelo_evento->obtener_filtro($idevento);

            //$parametros = json_decode($filtro->filtro, true); //true lo convierte a array, de lo contrario stdclass
            $parametros = json_decode($filtro->filtro);


            $standby = 0;
            if($data->standby == 1)
                $standby = 1;


            //$files = $this->modelo_upload->get_files_evento($idevento);

           //print_r($parametros); die();

            if ( ($data) && ($filtro) )
            {
                $ultimoId=$data->id;

                /*if($files!='')
                {
                    $arrefiles = array();
                    foreach ($files as $file ) {
                        $arrefiles[]=$file->id;
                    }
                    $idsfiles = join(',',$arrefiles);
                    $this->modelo_upload->set_eventoId($ultimoId, $idsfiles);
                }*/

                $this->load->helper('email');
                $urlapi = $this->utilidades->get_url_api();
                //$fichero = $urlapi."application/controllers/api/v1/log_eventos.txt";
               // $fichero2 = $urlapi."application/controllers/api/v1/log_emails.txt";


                //inserta filtro.............................................................
                switch($filtro->tipo)
                {
                    case 't'://todos

                        /*$usuarios = $this->modelo_usuario->get_usuariosColegio($idcolegio);
                        $arre=array();
                        foreach ($usuarios as $row )
                        {
                           $arre[]=$row->user_id;
                        }*/

                        $getdata = array(
                            'eventos_id'=>$ultimoId,
                        );
                        $existen = $this->modelo_evento->get_eventosusers($getdata);
                        $arre2=array();
                        foreach ($existen as $row2 )
                        {
                           $arre2[]=$row2->users_id;
                        }


                        //$diferencia = array_diff($arre, $arre2); //todos menos los que se habian insertado

                        $quitar = join(',',$arre2);
                        //$destinos['todos'] = join(',',$diferencia);
                        //$destinos['alumnos'] = 0;

                        $usuarios = $this->modelo_usuario->get_usuariosParaEnviar3_1($quitar,0, $idcolegio);

                        break;

                    case 'd'://dirigido a un/s usuario/s especifico/s

                        //usuariostodos
                        if(count($parametros->usuarios) == 0)  //si no hay usuarios, debe traer todos los users segun los roles seleccionados. copia de usuario/obtener_usuarios
                        {
                            $ids_roles = join(',',$parametros->roles);

                            $this->load->model("modelo_groups");
                            $myrol = $this->modelo_groups->get_name_rol($idgrupo);
                            $nombregrupo = $myrol->name;

                            $result = $this->traer_usuariosXrolesIds($idcolegio, $nombregrupo, $ids_roles);
                            $array = array();
                            foreach ($result as $key) {
                                $array[] = $key->user_id;
                            }
                            //print_r($array);  die();
                            $parametros->usuariostodos = $array;
                        }


                        $destinos = $this->traer_destinosDiri($parametros, $idcolegio);


                        if( (isset($destinos)) && (count($destinos['todos']) > 0)  ) //me aseguro que no venga vacio
                        {

                             $usuarios1 = $this->modelo_usuario->get_usuariosParaEnviar($destinos['todos'], 0, $idcolegio);

                             $usuarios2 = $this->modelo_usuario->get_usuariosParaEnviar2($destinos['tutores'], $destinos['alumnos'], $idcolegio);

                            if( ($usuarios1) && ($usuarios2) )
                                $usuarios = array_merge($usuarios1, $usuarios2);
                            else  if($usuarios1)
                                        $usuarios = $usuarios1;
                                    else $usuarios = $usuarios2;
                        }

                        //print_r($usuarios);  die();

                        break;

                    case 'p'://personalizado (nivel, año, division)

                        /*if (count($parametros['divisiones']) == 0)  //NO hay divisiones? tons traer a la fuerza!
                        {
                            if (count($parametros['anios']) > 0){
                                $aniosIds = join(',',$parametros['anios']);
                                $divisiones = $this->modelo_division->get_divisionesXanios($aniosIds, 3 ); //3 plan estudio
                            }
                            else {
                                $nivelesIds = join(',',$parametros['niveles']);
                                $divisiones = $this->modelo_division->get_divisionesXNiveles($nivelesIds, 3 );
                            }

                            if($divisiones)
                                foreach ($divisiones as $div)
                                    $parametros['divisiones'][] = $div->id;

                        }*/

                        $destinos = $this->traer_destinos($parametros, $idcolegio);

                        //estos ids son de users (no user_group)

                        if( (isset($destinos)) && (count($destinos['todos']) > 0)  ) //me aseguro que no venga vacio
                        {
                             $usuarios1 = $this->modelo_usuario->get_usuariosParaEnviar($destinos['todos'], 0, $idcolegio);

                             $usuarios2 = $this->modelo_usuario->get_usuariosParaEnviar2($destinos['tutores'], $destinos['alumnos'], $idcolegio);

                            if( ($usuarios1) && ($usuarios2) )
                                $usuarios = array_merge($usuarios1, $usuarios2);
                            else  if($usuarios1)
                                        $usuarios = $usuarios1;
                                    else $usuarios = $usuarios2;
                        }


                        break;

                    case 'a'://dirigido a usuarios relacionados con areas


                        /*$destinos = $this->traer_destinosDiri($parametros, $idcolegio);

                        if( (isset($destinos)) && (count($destinos['todos']) > 0)  ) //me aseguro que no venga vacio
                        {
                             $usuarios1 = $this->modelo_usuario->get_usuariosParaEnviar($destinos['todos'], 0, $idcolegio);

                             $usuarios2 = $this->modelo_usuario->get_usuariosParaEnviar2($destinos['tutores'], $destinos['alumnos'], $idcolegio);

                            if( ($usuarios1) && ($usuarios2) )
                                $usuarios = array_merge($usuarios1, $usuarios2);
                            else  if($usuarios1)
                                        $usuarios = $usuarios1;
                                    else $usuarios = $usuarios2;
                        }*/

                        break;

                }//fin swich

                $contenido = "\n\n\nIdColegio:".$idcolegio."  IdEvento:".$ultimoId."\n";
                $contenido .= "destinosTodos:".$destinos['todos']."\n";
                $contenido .= "destinosAlumnos:".$destinos['alumnos']."\n";
                $contenido .= "destinosTutores:".$destinos['tutores']."\n";
                // usando la bandera FILE_APPEND para añadir el contenido al final del fichero
                // y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
                file_put_contents($fichero, $contenido, FILE_APPEND | LOCK_EX);

                //print_r($usuarios); die();

                if($standby == 0)
                    $this->enviar_eventosusers($usuarios, $ultimoId, $data, $idcolegio, $data->users_id, $fichero, $fichero2); //inserta y envia
                else
                    $this->noenviar_eventosusers($usuarios, $ultimoId, $data, $idcolegio, $data->users_id, "no_auto"); //inserta y el envio queda en espera. no autorizado.



                $creador = $this->modelo_usuario->get_usuariosEspecificos($data->users_id);
                foreach ($creador as $row )
                {
                    $fechaA=date('Y-m-d H:i:s');
                    $this->response([
                            'status' => TRUE,
                            'message' => 'Evento reenviado',
                            'id' => $ultimoId,
                            'fechaalta'=>$fechaA,
                            'creadorname'=> $row->first_name.' '.$row->last_name,
                        ], REST_Controller::HTTP_OK);
                }
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Error al guardar'
                ]); // NOT_FOUND (404) being the HTTP response code
            }

        }else  $this->response([
                'status' => FALSE,
                'message' => 'pininvalido'
                ], REST_Controller::HTTP_OK);

        }
    }


    public function publicar_post()  //ANDRES supervisar
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $pin = $_POST['pin'];
            $existe = $this->utilidades->verifica_pin($pin);
            if ($existe == 1)
            {

                $idevento = $_POST['idevento'];


                //$rows = $this->modelo_evento->obtener_eventosuser_ParaEnviar_Debug($idevento);
                $rows = $this->modelo_evento->obtener_eventosuser_ParaEnviar($idevento);

                $data = $this->modelo_evento->obtener_datosEvento($idevento);

                //print_r($rows); die();

                $this->load->helper('email');
                $urlapi = $this->utilidades->get_url_api();
               // $fichero = $urlapi."application/controllers/api/v1/log_eventos.txt";
                //$fichero2 = $urlapi."application/controllers/api/v1/log_emails.txt";

                $this->soloenviar_eventosusers($rows, $data, $fichero, $fichero2);

                $arre = array(
                    'standby'=>0
                );
                $this->modelo_evento->update_evento($arre, $idevento);

                $creador = $this->modelo_usuario->get_usuariosEspecificos($data->users_id);
                foreach ($creador as $row )
                {
                    $fechaA=date('Y-m-d H:i:s');
                    $this->response([
                            'status' => 1,
                            'message' => 'Evento Publicado',
                            'id' => $idevento,
                            'creadorname'=> $row->first_name.' '.$row->last_name,
                        ], REST_Controller::HTTP_OK);
                }


            }else  $this->response([
                    'status' => 0,
                    'message' => 'pininvalido'
                    ], REST_Controller::HTTP_OK);

        }
    }

    function checkmodifica($datos1, $datos2){

        $modificaalgo = 0;
        if( ($datos1->titulo != $datos2->titulo) || ($datos1->confirm != $datos2->confirm) || ($datos1->lugar != $datos2->lugar) || ($datos1->descripcion != $datos2->descripcion) || ($datos1->fechaInicio != $datos2->fechaInicio) || ($datos1->fechafin != $datos2->fechafin) )
            $modificaalgo = 1;

        return $modificaalgo;
    }


    public function update_evento_post()  //ANDRES
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $pin = $_POST['pin'];
            //$idcolegio = $_POST['idcolegio'];

            $existe = $this->utilidades->verifica_pin($pin);
            if ($existe == 1)
            {
                $data = json_decode($_POST['data']) ; //datos del evento
                $idevento = json_decode($_POST['idevento']) ;
                $idsfiles = $_POST['idsfiles'];
                $edita_files = $_POST['edita_files'];

                $superEdit = false;
                if( isset( $_POST['supervisa']) )
                    $superEdit = $_POST['supervisa'];

                $this->load->helper('email');


                //print_r($data);
                $evento = $this->modelo_evento->obtener_evento($idevento);

                $idcolegio = $evento->colegioid;
                if($evento->userid == $userid || $superEdit)
                {
                    if( $this->checkmodifica($data,$evento) || ($edita_files) ){ //si modifica algun dato o edita archivos

                        // datetime mysql no admite valor vacio ''
                        if($data->fechaInicio == '' || $data->fechaInicio == '0000-00-00 00:00:00')
                            $data->fechaInicio=null;
                        if($data->fechafin == '' || $data->fechafin == '0000-00-00 00:00:00')
                            $data->fechafin=null;

                        //print_r($data);

                        $result = $this->modelo_evento->update_evento($data, $idevento);

                        if ($result)
                        {
                            $modificacion_Data = array('fecha_modificacion' => date("Y-m-d H:i:s"), 'modificado' => 1 );
                            $this->modelo_evento->update_evento($modificacion_Data, $idevento);
                            if($idsfiles!='' && $edita_files )
                            {
                                $this->load->model("modelo_upload");
                                $this->modelo_upload->set_eventoId($idevento, $idsfiles);
                            }

                            if($evento->standby == 0){

                                //echo "ENTRA";

                                $eventosusers = $this->modelo_evento->obtener_eventosuser_ByIdevento_visto($idevento,0,$idcolegio);

                                //echo "SALE";

                                //var_dump($eventosusers);

                                $eu_email = array();
                                $eu_movil = array();
                                $detallesPush = array();

                                if($eventosusers)
                                {
                                    //echo "ENTRA2";
                                    $arreAux = array();
                                    $arreAuxUs = array();

                                    foreach ($eventosusers as $row1)
                                    {
                                        $arreAux[] = $row1->id;
                                        $arreAuxUs[] = $row1->users_id;

                                    }
                                    $arreAux=join(',',$arreAux);
                                    $arreAuxUs=join(',',$arreAuxUs);

                                    //print_r($arreAux);

                                    $this->modelo_evento->cambiar_campo_alta_eventosusers2('b',$arreAux);
                                    $this->modelo_evento->cambiar_campo_alta_eventosusers_detalle('b',$arreAuxUs,$idevento);


                                    //$usuarios = $this->modelo_usuario->get_usuariosParaEnviar($arreAux, 0,$idcolegio);

                                    //if($usuarios){
                                        $datacolegio=$this->modelo_colegio->get_colegio($idcolegio); //colegio

                                        foreach ($eventosusers as $row ) {

                                            //echo $row->id.",";

                                            $arrerow = new stdClass();
                                            $arrerow->user_id = $row->users_id;
                                            $arrerow->active = 1;
                                            $arrerow->recibir_notificacion = 1;
                                            //print_r($arrerow->user_id); die();

                                            $datos_push = array("message" => "Evento Modificado: ".$evento->titulo, "userid" => $this->utilidades->encriptar($row->users_id), "nombrecolegio"=>$datacolegio->row()->nombre); //titulo viejo o nuevo? viejo!

                                            $datos = array( 'eventos_id'=>$idevento, 'users_id'=>$row->users_id, 'destinatario'=>$row->destinatario, 'medio'=>'m', 'alta'=>'m',
                                                'aceptado'=>$row->aceptado, 'fechaConfirmacion'=>$row->fechaConfirmacion,
                                                'me_gusta'=>$row->me_gusta, 'fechaMegusta'=>$row->fechaMegusta);

                                            $result = $this->enviar_notificacion_usuarios($arrerow,$datos_push,$datos,0); //solo envia.

                                            if($result['result'] == 0) //si es 0 no tiene movil o fallo el envio, enviar por email
                                            {
                                                if ( valid_email($row->email) )
                                                {
                                                    $usuariosEmail[] = $row->email;
                                                    $motivo=null;
                                                }
                                                else
                                                {
                                                    $motivo='email_invalid';
                                                }

                                               $eu_email[] = array( 'eventos_id'=>$idevento, 'users_id'=>$row->users_id, 'destinatario'=>$row->destinatario, 'enviado'=>0,'motivo'=>$motivo,'medio'=>'e', 'alta'=>'m',
                                                'aceptado'=>$row->aceptado,'fechaConfirmacion'=>$row->fechaConfirmacion,
                                                'me_gusta'=>$row->me_gusta,'fechaMegusta'=>$row->fechaMegusta );
                                            }
                                            else {
                                                $eu_movil[] = $result['data'];
                                            }

                                            //Falta: enviar una sola vez (por user) pero insertar todas

                                            if( isset($result['detallesPush']) && (count($result['detallesPush'])>0) ){

                                                if(count($detallesPush)==0)
                                                    $detallesPush = $result['detallesPush'];
                                                else
                                                    $detallesPush = array_merge($detallesPush,$result['detallesPush']);

                                            }

                                        }

                                        /*print_r($eu_movil);
                                        echo "<br><br>";
                                        print_r($detallesPush);
                                        echo "<br><br>";
                                        print_r($eu_email);
                                        //die();*/
                                        if(count($eu_movil)>0)
                                            $this->modelo_evento->insertar_eventousers2($eu_movil);

                                        if(count($detallesPush)>0)
                                            $this->modelo_evento->insertar_eventousers_detalle2($detallesPush);

                                        if( (isset($usuariosEmail)) && (count($usuariosEmail)>0) ) //enviar por GMAIL google
                                        {
                                            if(count($eu_email)>0)
                                                $this->modelo_evento->insertar_eventousers2($eu_email);

                                            $destinos = $usuariosEmail;


                                            $from1 = "contacto@smtp.e-nodos.com";
                                            $from2 = $datacolegio->row()->nombre;


                                            $asunto="Evento Editado: ".$evento->titulo;
                                            $datos['remitente'] = $this->modelo_usuario->get_usuariosEspecificos($userid);
                                            $datos['titulo']= $evento->titulo;
                                            $datos['fechaInicio']= $evento->fechaInicio;
                                            $datos['descripcion']= $evento->descripcion;
                                            $datos['lugar']= $evento->lugar;
                                            $datos['colegio_nombre']= $datacolegio->row()->nombre;
                                            //$datos['data']= $evento;


                                            $mensaje = $this->load->view('evento/mail_evento', $datos,true);
                                            $enviado = $this->usuarios->enviar_email($destinos, $mensaje, $from1, $from2, $asunto);
                                            //print_r($enviado);
                                            if($enviado['enviado'])
                                            {
                                                $valores=array('enviado'=>1);
                                                $this->modelo_evento->update_enviado_eventousers($idevento,'e','m',$valores);
                                            }
                                            else {
                                                    //$fichero2 = $_SERVER['DOCUMENT_ROOT']."/api/application/controllers/api/v1/log_emails.txt";

                                                    $urlapi = $this->utilidades->get_url_api();
                                                    $fichero2 = $urlapi."application/controllers/api/v1/log_emails.txt";

                                                    $contenido2 = "\n\n\n UPDATE  IdColegio:".$idcolegio."  IdEvento:".$idevento."\n";
                                                    $contenido2 .= "Error:".$enviado['error']."\n";
                                                    file_put_contents($fichero2, $contenido2, FILE_APPEND | LOCK_EX);
                                                }
                                        }//fin if enviar por mail

                                    //}
                                }

                            }//fin if standby


                            $evento = $this->modelo_evento->obtener_evento($idevento);
                            $data = json_encode($evento);
                            $this->response([
                                    'status' => TRUE,
                                    'data' => $data,
                                    'message' => 'Evento Modificado',
                                ], REST_Controller::HTTP_OK);
                        }
                        else
                        {
                            $this->response([
                                'status' => FALSE,
                                'message' => 'Error al modificar'
                            ]); // NOT_FOUND (404) being the HTTP response code
                        }
                    }//fin if checkmodifica!
                    else {
                        $this->response([
                                'status' => FALSE,
                                'message' => 'No modifica nada',
                            ], REST_Controller::HTTP_OK);
                    }
                }
                else {
                    $this->response([
                                'status' => FALSE,
                                'message' => 'No puede modificar',
                            ], REST_Controller::HTTP_OK);
                    }
            }
            else  {$this->response([
                'status' => FALSE,
                'message' => 'pininvalido'
                ], REST_Controller::HTTP_OK);  }
        }
    }


    public function delete_event_post()  //ANDRES
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $pin = $_POST['pin'];
            $idevento = $_POST['idevento'];
            //$idcolegio = $_POST['idcolegio'];

            $this->load->helper('email');


            $existe = $this->utilidades->verifica_pin($pin);
            if ($existe == 1)
            {
                //print_r($data);
                $evento = $this->modelo_evento->obtener_evento($idevento); //baja
                $idcolegio = $evento->colegioid;
                if($evento->userid == $userid)
                {
                    $result = $this->modelo_evento->delete_evento($idevento);  //baja


                    if ($result)
                    {
                        if($evento->standby == 0){
                            //envio notificacion a los que tienen el visto en 1
                            $eventosusers = $this->modelo_evento->obtener_eventosuser_ByIdevento_visto($idevento,0,$idcolegio);

                            if($eventosusers)
                            {
                                $arreAux = array();
                                foreach ($eventosusers as $row)
                                {
                                    $arreAux[] = $row->users_id;
                                }
                                $arreAux=join(',',$arreAux);

                                //$usuarios = $this->modelo_usuario->get_usuariosParaEnviar($arreAux, 0, $evento->colegioid);

                                //if($usuarios){
                                    $datacolegio = $this->modelo_colegio->get_colegio($idcolegio); //colegio

                                    foreach( $eventosusers as $row )
                                    {
                                        $arrerow = new stdClass();
                                        $arrerow->user_id = $row->users_id;
                                        $arrerow->active = 1;
                                        $arrerow->recibir_notificacion = 1;

                                        //enviar notificacion o mail
                                        $datos_push = array("message" => "Evento Eliminado: ".$evento->titulo, "userid" => $this->utilidades->encriptar($row->users_id), "nombrecolegio"=>$datacolegio->row()->nombre); //titulo viejo o nuevo? viejo!

                                        $correo = $this->enviar_notificacion_usuarios2($arrerow,$datos_push,0); //envia pero No inserta.

                                        if($correo==1) //si es 1 no tiene movil o fallo el envio, enviar por email
                                        {

                                            if ( valid_email($row->email) )
                                            {
                                                $usuariosEmail[] = $row->email;
                                                //$motivo=null;
                                            }

                                        }
                                    }

                                    if( (isset($usuariosEmail)) && (count($usuariosEmail)>0) ) //enviar por GMAIL google
                                    {
                                        //$destinos = array_column($usuariosEmail, 1);
                                        $destinos = $usuariosEmail;

                                        $from1 = "contacto@smtp.e-nodos.com";
                                        $from2 = $datacolegio->row()->nombre;


                                        $asunto="Evento Eliminado: ".$evento->titulo;
                                        $datos['remitente'] = $this->modelo_usuario->get_usuariosEspecificos($userid);
                                        $datos['titulo']= $evento->titulo;
                                        $datos['colegio_nombre']= $datacolegio->row()->nombre;
                                        //$datos['data']= $evento;


                                        $mensaje = $this->load->view('evento/mail_eventoDelete', $datos,true);
                                        //print_r($mensaje);
                                        $enviado = $this->usuarios->enviar_email($destinos, $mensaje, $from1, $from2, $asunto);
                                        //print_r($enviado);

                                        if(! $enviado['enviado'])
                                        {
                                            $urlapi = $this->utilidades->get_url_api();
                                            $fichero2 = $urlapi."application/controllers/api/v1/log_emails.txt";

                                            $contenido2 = "\n\n\n DELETE  IdColegio:".$evento->colegioid."  IdEvento:".$idevento."\n";
                                            $contenido2 .= "Error:".$enviado['error']."\n";
                                            file_put_contents($fichero2, $contenido2, FILE_APPEND | LOCK_EX);
                                        }

                                    }

                                //}
                            }
                        }

                        //elimino los registros en eventosusers_detalle
                        $arreAux2 = array();
                        $eventosusers2 = $this->modelo_evento->obtener_eventosuser_ByIdevento_delete($idevento);
                        if($eventosusers2)
                        {
                            foreach ($eventosusers2 as $row)
                            {
                                $arreAux2[] = $row->id;
                            }
                            $arreAux2=join(',',$arreAux2);
                        }

                        if( count($arreAux2) > 0 )
                            $this->modelo_evento->delete_eventouser_detalle($arreAux2); //para registros viejos

                        $this->modelo_evento->delete_eventouser_detalle2($idevento);


                        if($idevento)
                            $this->modelo_evento->delete_eventouser($idevento);

                        //delete files
                        $this->load->model("modelo_upload");
                        $uploaddir = $this->utilidades->get_url_adjuntos();
                        $files = $this->modelo_upload->get_files_evento($idevento);
                        if($files)
                        {
                            foreach ($files as $datos)
                            {
                                $this->modelo_upload->delete_event_file($idevento, $datos->id); 
                                $files = $this->modelo_upload->get_eventFiles_ByFileId($datos->id);
                                     
                                if($files==0){

                                    $result = $this->modelo_upload->delete_file($datos->id);
                                    if($result)
                                    {
                                        $name = $datos->id .'_'.$datos->nombre ;
                                        $urlfile =  $uploaddir.$datos->carpeta.'/'.$name;
                                        if (file_exists($urlfile))
                                            unlink($urlfile);
                                    }
                                }

                            }
                        }

                        $this->response([
                                'status' => TRUE,
                                'message' => 'Evento Eliminado',
                            ], REST_Controller::HTTP_OK);
                    }
                    else
                    {
                        $this->response([
                            'status' => FALSE,
                            'message' => 'Error al Eliminar'
                        ]); // NOT_FOUND (404) being the HTTP response code
                    }
                }
                else {
                    $this->response([
                                'status' => FALSE,
                                'message' => 'No puede eliminar',
                            ], REST_Controller::HTTP_OK);
                    }
            }
            else  {$this->response([
                'status' => FALSE,
                'message' => 'Pin Invalido'
                ], REST_Controller::HTTP_OK);  }
        }
    }

    //evalua si esta' habilitado para recibir notificacion
    public function evalua_habilitado_notif($row_usuario){
        $habilitado = 0;

        //print_r($row_usuario);

        if( ($row_usuario->recibir_notificacion==1) && ($row_usuario->active==1) )
        {
            if( isset($row_usuario->pago) && isset($row_usuario->pago_alumno) )
            {
                if( ($row_usuario->name!='Tutor') && ($row_usuario->pago==1) )
                    $habilitado = 1;
                else
                    if( ($row_usuario->name=='Tutor') && ($row_usuario->pago_alumno==1) )
                        $habilitado = 1;
                    else $habilitado = 0;
            }
            else
                if( ($row_usuario->name=='Alumno') && ($row_usuario->pago==1) )
                        $habilitado = 1;
                else {
                    if( ($row_usuario->name!='Alumno') && ($row_usuario->name!='Tutor') )
                        $habilitado = 1;
                    else
                        if( $row_usuario->name=='Tutor' ) //no interesa el campo pagó
                            $habilitado = 1;
                        else
                            $habilitado = 0;
                }

                //$habilitado = 1;

        }
        else $habilitado = 0;

        return $habilitado;
    }


    /**
     * Funcion que recorre los usuarios, obtiene el token gcm y les envia una notificación.
     * A los que no tienen token gcm deberia enviarle un mail.
     * @param $row_usuario datos del usuario que se debe enviar notificacion o mail
     * @param $datos_push array($message, $userid).
     * @return
     */


    private function enviar_notificacion_usuarios2($row_usuario, $datos_push, $eval_hab=1)//ENVIA NOTIFICACON PERO NO INSERTA, SE USA PARA EL ELIMINAR EVENTO
    {
        if($eval_hab==1)
            $habilitado = $this->evalua_habilitado_notif($row_usuario);
        else $habilitado=1;


        if( $habilitado==1 )
        {
            $userid = $row_usuario->user_id;
            //Obtengo el token gcm para enviar la notificacion
            $token_gcm = $this->modelo_usuario->get_push($userid);
            if($token_gcm)
            {
                //Recorro los token ya que el usuario puede estar en mas de un cel
                //logueado y por ende tiene un token por cada cel logueado.
                //$cantTokens=count($token_gcm); //$cont=0;
                $bandera = 0;
                foreach ($token_gcm as $key => $value) {
                    $gcmRegIds = array($value->token);

                    if($value->source == 'react'){
                        $datos_push_expo = array(
                            'token'=>$value->token,
                            'title'=>$datos_push['message'],
                            'colegio'=>$datos_push['nombrecolegio'],
                        );
                        $pushStatus = $this->usuarios->enviar_notificacion_expo($datos_push_expo);
                        $success = $pushStatus['success'];
                        $error = $pushStatus['error'];
                    }
                    else {
                        $pushStatus = $this->usuarios->enviar_notificacion($gcmRegIds, $datos_push);
                        // $pushStatus = json_decode($pushStatus,true);
                        // $success = $pushStatus['success'];
                        // if(!$success)
                        //     $error = $pushStatus["results"][0]["error"];
                        //Franco desde Aqui nuevo control de error--------
                        $aux_pushStatus =  $pushStatus;
                        //Franco: Controlo cuando no es "react" el error "deprecated.."
                        $errorJson = explode("=",$aux_pushStatus);
                        if($errorJson[1]=="DeprecatedEndpoint"){
                            $success = 0;
                            $error = "DeprecatedEndpoint";
                        }
                        else{
                            $pushStatus = json_decode($pushStatus,true);
                            $success = $pushStatus['success'];
                            if(!$success)
                                $error = $pushStatus["results"][0]["error"];
                        }
                        //------------------hasa aqui nuevo control de error
                    }


                    if($success == 0)//si es 0 debo mirar el campo result
                    {
                        //$cantTokens--;
                        if ( ($error == "DeprecatedEndpoint") || ($error == "InvalidRegistration") || ($error == "NotRegistered") || ($error == "DeviceNotRegistered") )
                        {
                            $this->modelo_usuario->delete_push($userid, $value->token);
                        }
                    }
                    else
                        if($success==1)
                            $bandera = 1;
                }

                if($bandera == 1)  //si es cero es porque todos los token son invalidos o no se envio a ningun movil, debo enviar por email
                    return 0; //por retornar algo, lo que importa es el 1
                else
                    return 1;
            }
            else
                return 1; //enviar por email
        }
        else//sino habilitado para recibir notificaciones
            return 0;

    }


    private function enviar_notificacion_usuarios($row_usuario, $datos_push, $datos, $eval_hab=1)//SEBA
    {
        if($eval_hab==1)
            $habilitado = $this->evalua_habilitado_notif($row_usuario);
        else $habilitado=1;



        $result['data'] = $datos;

        $result['data']['motivo'] = null;
        $result['data']['token_id'] = null;

        if( $habilitado==1 )
        {
            $userid = $row_usuario->user_id;
            //Obtengo el token gcm para enviar la notificacion
            $token_gcm = $this->modelo_usuario->get_push($userid);
            if($token_gcm)
            {
                //var_dump($datos);
                //exit();
                //Guardo en eventosusers y si se envia despues hago un update
                //$datos['enviado'] = 0;
                //$this->modelo_evento->insertar_eventousers($datos); //insertar
                //$eventosusers_id = $this->db->insert_id();
                //$datos['eventosusers_id'] = $eventosusers_id;

                $bandera = 0;
                $detallesPush = array();
                foreach ($token_gcm as $key => $value) {
                    $gcmRegIds = array($value->token);

                    if($value->source == 'react'){
                        $datos_push_expo = array(
                            'token'=>$value->token,
                            'title'=>$datos_push['message'],
                            'colegio'=>$datos_push['nombrecolegio'],
                        );
                        $pushStatus = $this->usuarios->enviar_notificacion_expo($datos_push_expo);
                        $success = $pushStatus['success'];
                        $error = $pushStatus['error'];
                    }
                    else {
                       
                        $pushStatus = $this->usuarios->enviar_notificacion($gcmRegIds, $datos_push);
                        // $pushStatus = json_decode($pushStatus,true);
                        // $success = $pushStatus['success'];
                        // if(!$success)
                        //     $error = $pushStatus["results"][0]["error"];
                        //Franco desde Aqui nuevo control de error--------
                        $aux_pushStatus =  $pushStatus;
                        //Franco: Controlo, cuando no es "react", el error "DeprecatedEndpoint"
                        $errorJson = explode("=",$aux_pushStatus);
                        if($errorJson[1]=="DeprecatedEndpoint"){
                            $success = 0;
                            $error = "DeprecatedEndpoint";
                        }
                        else{
                            $pushStatus = json_decode($pushStatus,true);
                            $success = $pushStatus['success'];
                            if(!$success)
                                $error = $pushStatus["results"][0]["error"];
                        }
                        //------------------hasa aqui nuevo control de error
                       
                    }



                    //Asigno algunos valores para realizar el insert en eventosusers_detalle
                    //$data['eventosusers_id'] = $datos['eventosusers_id'];
                    //$detalle["medio"] = $datos["medio"];

                    $detalle["evento_id"] = $datos["eventos_id"];
                    $detalle["alta"] = $datos["alta"];
                    $detalle['token_id'] = $value->id;
                    $detalle['user_id'] = $userid;

                    if($success == 0)//si es 0 debo mirar el campo result
                    {
                        //$cantTokens--;
                        if ( ($error == "DeprecatedEndpoint") || ($error == "InvalidRegistration") || ($error == "NotRegistered") || ($error == "DeviceNotRegistered") )
                        {
                            $this->modelo_usuario->delete_push($userid, $value->token);
                        }
                        else { //por algun motivo x, no se envio
                            $detalle['enviado'] = 0;
                            $detalle['motivo'] = $error;
                            //$this->modelo_evento->insertar_eventousers_detalle($data);
                            $detallesPush[]=$detalle;
                        }
                    }
                    else
                        if($success==1){
                            $bandera = 1;
                            $detalle['enviado'] = 1;
                            $detalle['motivo'] = null;
                            //$this->modelo_evento->insertar_eventousers_detalle($data); //insertar
                            $detallesPush[]=$detalle;

                            $result['data']['token_id'] = $value->id;
                        }

                }

                $result['detallesPush'] = $detallesPush;

                if($bandera == 1)  //si es cero es porque todos los token son invalidos o no se envio a ningun movil, debo enviar por email
                {
                    //hacer el update en eventosusers
                    //$data = array();
                    //$data['enviado'] = 1;
                    //$this->modelo_evento->update_eventosusers($data, $eventosusers_id);
                    //return 0;
                    $result['data']['enviado'] = 1;

                    $result['result'] = 1;
                }
                else {
                        $result['data']['enviado'] = 0;
                        $result['result'] = 0; //enviar por email_invalid
                        }
            }
            else
                $result['result'] = 0; //enviar por email
        }
        else{//sino esta' habilitado para recibir notificaciones, lo mismo debo insertar
            if($row_usuario->recibir_notificacion==0)
            {
                $result['data']['motivo'] = 'no_notif';
                $result['data']['medio'] = 'n';
            }
            else
                if($row_usuario->active==0)
                {
                    $result['data']['motivo'] = 'no_activo';
                    $result['data']['medio'] = 'n';
                }
                else
                    {
                        $result['data']['motivo'] = 'no_pago';
                        $result['data']['medio'] = 'n';
                    }
            $result['result'] = -1;
            $result['data']['enviado'] = 0;
            //$this->modelo_evento->insertar_eventousers($datos); //insertar
        }

        //1 = enviado
        //0 = no enviado, enviar por email
        //-1 = no enviar
        return $result;
    }


    private function enviar_notificacion_usuarios3($row_usuario, $datos_push, $evento)//ENVIA PERO NO INSERTA, se usa para el publicar evento
    {
        $habilitado = $this->evalua_habilitado_notif($row_usuario);

        if( $habilitado==1 )
        {
            $userid = $row_usuario->user_id;

            $token_gcm = $this->modelo_usuario->get_push($userid);
            if($token_gcm)
            {
                $bandera = 0;
                $detallesPush = array();
                foreach ($token_gcm as $key => $value) {
                    $gcmRegIds = array($value->token);

                    if($value->source == 'react'){
                        $datos_push_expo = array(
                            'token'=>$value->token,
                            'title'=>$datos_push['message'],
                            'colegio'=>$datos_push['nombrecolegio'],
                        );
                        $pushStatus = $this->usuarios->enviar_notificacion_expo($datos_push_expo);
                        $success = $pushStatus['success'];
                        $error = $pushStatus['error'];
                    }
                    else {
                        $pushStatus = $this->usuarios->enviar_notificacion($gcmRegIds, $datos_push);
                       // $pushStatus = json_decode($pushStatus,true);
                        // $success = $pushStatus['success'];
                        // if(!$success)
                        //     $error = $pushStatus["results"][0]["error"];
                        //Franco desde Aqui nuevo control de error--------
                        $aux_pushStatus =  $pushStatus;
                        //Franco: Controlo cuando no es "react" el error "deprecated.."
                        $errorJson = explode("=",$aux_pushStatus);
                        if($errorJson[1]=="DeprecatedEndpoint"){
                            $success = 0;
                            $error = "DeprecatedEndpoint";
                        }
                        else{
                            $pushStatus = json_decode($pushStatus,true);
                            $success = $pushStatus['success'];
                            if(!$success)
                                $error = $pushStatus["results"][0]["error"];
                        }
                    }


                    //$data['eventosusers_id'] = $eventosusers_id;
                    //$data["medio"] = 'm';
                    $detalle["evento_id"] = $evento->id;
                    $detalle["alta"] = 'a';
                    $detalle['token_id'] = $value->id;
                    $detalle['user_id'] = $userid;


                    if($success == 0)//si es 0 debo mirar el campo result
                    {
                        //$cantTokens--;
                        if ( ($error == "DeprecatedEndpoint") || ($error == "InvalidRegistration") || ($error == "NotRegistered") || ($error == "DeviceNotRegistered") )
                        {
                            $this->modelo_usuario->delete_push($userid, $value->token);
                        }
                        else { //por algun motivo x, no se envio
                             $detalle['enviado'] = 0;
                            $detalle['motivo'] = $error;
                            //$this->modelo_evento->insertar_eventousers_detalle($data);
                            $detallesPush[]=$detalle;
                        }
                    }
                    else
                        if($success==1){
                            $bandera = 1;
                            $detalle['enviado'] = 1;
                            //$this->modelo_evento->insertar_eventousers_detalle($data); //insertar
                            $detallesPush[]=$detalle;

                            $result['data']['token_id'] = $value->id;
                        }
                }

                $result['detallesPush'] = $detallesPush;

                if($bandera == 1)  //si llega a cero es porque todos los token son invalidos o no se envio a ningun movil, debo enviar por email
                {
                    //posponer el update en eventosusers
                    $result['data']['enviado'] = 1;
                    $result['data']['motivo'] = null;
                    $result['data']['medio'] = 'm';

                    $result['result'] = 1;
                }
                else $result['result'] = 0;
            }
            else
                $result['result'] = 0; //enviar por email
        }
        else{//sino quire recibir notificaciones, lo mismo debo modificar
            if($row_usuario->recibir_notificacion==0)
            {
                $result['data']['motivo'] = 'no_notif';
                $result['data']['medio'] = 'n';
            }
            else
                if($row_usuario->active==0)
                {
                    $result['data']['motivo'] = 'no_activo';
                    $result['data']['medio'] = 'n';
                }
                else{
                    $result['data']['motivo'] = 'no_pago';
                    $result['data']['medio'] = 'n';
                }
            //$datos2['enviado'] = 0;
            //$this->modelo_evento->update_eventosusers($datos2, $eventosusers_id); //update
            //return 0;

            $result['result'] = -1;
            $result['data']['enviado'] = 0;
        }

         //1 = enviado
        //0 = no enviado, enviar por email
        //-1 = no enviar
        return $result;
    }

    //elimina duplicados de un arreglo por clave
    public function unique_multidim_array($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach($array as $val) {
            if (!in_array($val->$key, $key_array)) {
                $key_array[$i] = $val->$key;
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }


    public function eventosNew_get()  //ANDRES
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_OK);//HTTP_BAD_REQUEST
        }
        else
        {
            // Obtengo todos los eventos
            $eventos = $this->modelo_evento->obtener_eventosNew($userid); // obtiene los eventos de el usuario X, que no han sido vistos
            // Si existe mas de un resultado, lo mando
            if ($eventos)
            {
                $eventos = json_encode($eventos);
                $this->response($eventos, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron eventos'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }

    public function eventos_enviados_get() // DAMIAN eventos enviados o creadospor userid
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_OK);//HTTP_BAD_REQUEST
        }
        else
        {
            $eventos = $this->modelo_evento->obtener_eventosCreados($userid); // Obtengo los creados por mi.
            if (isset($eventos))
            {
                $eventos = json_encode($eventos);
                $this->response($eventos, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => TRUE,
                    'message' => 'vacio'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

        }
    }

    public function eventos_enviados_eventosusers_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_OK);//HTTP_BAD_REQUEST
        }
        else
        {
            $idevento = $this->get('id'); // obtengo el id del evento, si es que vino en la url
            $eventos = $this->modelo_evento->obtener_eventosenviados_por_ideventos2($userid,$idevento); // Obtengo los creados por mi.
            $info_evento = $this->modelo_evento->obtener_datosEvento($idevento);
            $titulo_evento = $info_evento->titulo;
            /*if ($eventos)
            {
                $result = array();
                foreach ($eventos as $key) 
                {
                    $user = $this->modelo_usuario->get_users_id($userid);
                     //destinatario
                            $sql_user = "CALL obtener_eventos_datos_destinatario(".$key->users_id.")";
                            $sql_user = $this->db->query($sql_user);
                            $sql_user = $sql_user->row();
                            $key->nombre = $sql_user->nombre;
                            $key->apellido = $sql_user->apellido;
                            $key->users_id = $sql_user->user_id_destinatario;
                            if($sql_user->foto != '')
                            {
                                $key->foto_destinariario = $this->utilidades->get_url_img()."fotos_usuarios/".$sql_user->foto;
                            }
                            else
                            {
                                
                                $key->foto_destinariario = $this->utilidades->get_url_img()."fotos_usuarios/default-avatar.png";
                            }
                           // print_r($key);
                           // die();
                            
                            mysqli_next_result( $this->db->conn_id );

                    $grupo = $this->modelo_evento->obtener_grupo_menorOrden($key->users_id,$key->colegio_id);

                     //rol alias
                    if($grupo->group_alias!=null)
                        $grupo->name = $grupo->group_alias;
                    else
                        if( $grupo->aliasM!=null) {
                            if($sql_user->sexo=='F')
                                $grupo->name = $grupo->aliasF;
                            else
                                $grupo->name = $grupo->aliasM;
                        }

                    $key->grupo_rol = $grupo->name;
                    $result[]=$key;
                }
                $eventos = json_encode($result);
                //$this->response($eventos, REST_Controller::HTTP_OK);
                $message = [

                            'satus' => TRUE,
                            'eventousers' => $eventos,
                            'titulo' => $titulo_evento,
                        ];

                $this->set_response($message, REST_Controller::HTTP_OK);
            }*/
            if ($eventos)
            {
                $result = array();
                $cant_vistos = 0;
                $cant_no_vistos = 0;
                $confirmados = 0;
                $no_confirmados = 0;
                $confirmacion_negada = 0;
                foreach ($eventos as $key) 
                {
                    $user = $this->modelo_usuario->get_users_id($userid);
                    if($key->visto == 0)
                    {
                        $cant_no_vistos++;
                    }
                    else
                    {
                        $cant_vistos++;
                    }
                    if($info_evento->confirm == 1)
                    {
                        if($key->aceptado == -1)
                        {
                            $confirmacion_negada++;
                        }
                        if($key->aceptado == 0)
                        {
                            $no_confirmados++;
                        }
                        if($key->aceptado == 1)
                        {
                            $confirmados++;
                        }
                    }
                     //destinatario
                            //$sql_user = "CALL obtener_eventos_datos_destinatario(".$key->users_id.")";
                            $sql_user = "SELECT users.first_name nombre, users.last_name apellido, users.foto, users.id user_id_destinatario, users.sexo from users where users.id = $key->users_id";
                            $sql_user = $this->db->query($sql_user);
                            $sql_user = $sql_user->row();
                            $key->nombre = $sql_user->nombre;
                            $key->apellido = $sql_user->apellido;
                            $key->users_id = $sql_user->user_id_destinatario;
                            if($sql_user->foto != '')
                            {
                                $key->foto_destinariario = $this->utilidades->get_url_img()."fotos_usuarios/".$sql_user->foto;
                            }
                            else
                            {
                                
                                $key->foto_destinariario = $this->utilidades->get_url_img()."fotos_usuarios/default-avatar.png";
                            }
                           // print_r($key);
                           // die();
                            
                            mysqli_next_result( $this->db->conn_id );

                    $grupo = $this->modelo_evento->obtener_grupo_menorOrden($key->users_id,$key->colegio_id);

                     //rol alias
                    if($grupo->group_alias!=null)
                        $grupo->name = $grupo->group_alias;
                    else
                        if( $grupo->aliasM!=null) 
                        {
                            if($sql_user->sexo=='F')
                                $grupo->name = $grupo->aliasF;
                            else
                                $grupo->name = $grupo->aliasM;
                        }

                    $key->grupo_rol = $grupo->name;
                    $result[]=$key;
                }
                $eventos = json_encode($result);
                //$this->response($eventos, REST_Controller::HTTP_OK);
                $message = [

                            'satus' => TRUE,
                            'eventousers' => $eventos,
                            'titulo' => $titulo_evento,
                            'vistos' => $cant_vistos,
                            'no_vistos' => $cant_no_vistos,
                            'confirm' => $info_evento->confirm,
                            'confirmados' => $confirmados,
                            'no_confirmados' => $no_confirmados,
                            'confirmacion_negada' => $confirmacion_negada
                        ];

                $this->set_response($message, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => TRUE,
                    'message' => 'vacio'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

        }
    }
    //datatable nuevo
    public function obtener_destinatario($idevento)
    {
        $userid = $this->utilidades->verifica_userid();

            $respuesta="Perzonalizado\Varios";
            //$idevento = $this->get('id');
            $lista_filtro = $this->modelo_evento->obtener_filtro($idevento);
            $ciclo = 0;
            foreach ($lista_filtro as $dato)
            {
                $ciclo = $ciclo + 1;
            }
            if($ciclo == 1)
            {
                $arreglo_id = explode(",", $lista_filtro[0]->valores);
                $ciclo_valor = 0;
                foreach ($arreglo_id as $valor)
                {
                    $ciclo_valor = $ciclo_valor + 1;
                }
                if($ciclo_valor == 1)
                {
                    if($lista_filtro[0]->filtro == "nivel")
                    {
                        $aux=$this->modelo_nivel->obtener_nivel_id($arreglo_id[0]);
                        $respuesta = $aux->nombre;
                    }
                    if($lista_filtro[0]->filtro == "division")
                    {
                        $aux=$this->modelo_division->obtener_division_id($arreglo_id[0]);
                        $respuesta= $aux->nombanio.' '.$aux->nombdiv;
                    }
                    if($lista_filtro[0]->filtro == "rol")
                    {
                        $aux=$this->modelo_usuario->get_rol_id($arreglo_id[0]);
                        $respuesta= $aux->name;
                        if($aux->id == 3)
                        {
                            $respuesta = "Preceptores";
                        }
                        if($aux->name == 4)
                        {
                            $respuesta = "Docentes";
                        }
                        if($aux->name == 5)
                        {
                            $respuesta = "Tutores";
                        }
                        if($aux->name == 6)
                        {
                            $respuesta = "Alumnos";
                        }
                        if($aux->name == 7)
                        {
                            $respuesta = "Administrativos";
                        }
                        if($aux->name == 8)
                        {
                            $respuesta = "Directivos";
                        }
                    }
                    if($lista_filtro[0]->filtro == "usuario")
                    {
                        $aux=$this->modelo_usuario->get_users_id($arreglo_id[0]);
                        $respuesta= $aux->last_name.' '.$aux->first_name;
                    }
                }
                else
                {
                    $respuesta="Perzonalizado\Varios";
                }
            }
            else
            {
                $respuesta="Perzonalizado\Varios";
            }
            return $respuesta;

    }
    public function ajax_list_post()
    {
        $userid = $this->utilidades->verifica_userid();
        $obtener_privilegios_especiales =  $this->obtener_privilegios_especiales($userid);
        $obtener_privilegios_eventos_roles = $this->obtener_privilegios_eventos_roles($userid);
        $list = $this->modelo_evento->get_datatables($userid,$obtener_privilegios_especiales,$obtener_privilegios_eventos_roles);
        $ultima_query = $this->db->last_query();
        //echo $this->db->last_query();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $person) {
            //$this->obtener_destinatario($person->eventid);
            $no++;
            $row = array();
            $row[] = $person->last_name.' '.$person->first_name;
            $row[] = $person->titulo;
            $row[] = substr($person->fechaCreacion,8,2).'/'.substr($person->fechaCreacion,5,2).'/'.substr($person->fechaCreacion,0,4).' - '.substr($person->fechaCreacion,11,5).' hs';
           // $row[] = $this->obtener_destinatario($person->eventid);
            //$row[] = $person->fechaCreacion;

            //rol alias
            if($person->nombre_grupo_alias!=null)
                $person->rol_grupo = $person->nombre_grupo_alias;
            else
                if( $person->nombre_grupo_aliasM!=null) {
                    if($person->sexo_creador=='F')
                        $person->rol_grupo = $person->nombre_grupo_aliasF;
                    else
                        $person->rol_grupo = $person->nombre_grupo_aliasM;
                }
            //print_r($person->rol_grupo); die();
            if($person->cont_eventosusers == 0)
            {
                $sql_eventosusers = "SELECT COUNT(id)  as total_eventos_user FROM `eventosusers` WHERE eventos_id = ".$person->eventid;
                $sql_eventosusers = $this->db->query($sql_eventosusers);
              //  $row['cont_eventosusers'] = $sql_eventosusers->row()->total_eventos_user;
                $cont_eventosusers = $sql_eventosusers->row()->total_eventos_user;
            }
            else
            {
               // $row['cont_eventosusers'] =$person->cont_eventosusers;
                $cont_eventosusers = $person->cont_eventosusers;
            }
            $confirm_text = '';
            if($person->confirm == 1)
            {
                $confirm_text = '<strong style="color: #8A0808;">Confirmados: '.$person->cont_confirmados.', Declinados:  '.$person->cont_negados.'</strong>';
            }
           /* $row['cont_vistos'] =$person->cont_vistos;
            $row['cont_negados'] =$person->cont_negados;
            $row['cont_confirmados'] =$person->cont_confirmados;
            $row['cont_me_gusta'] =$person->cont_me_gusta;*/
            

            $row[] = $person->rol_grupo;
         //   $row[] = ;
            //add html for action
            if($person->standby == 1)
            {

                $row[] = '<center>'.
                         '<button type="button" class="btn btn-sm btn-success" title="Ver Detalles del Evento" onclick="detalles_evento('."'".$person->eventid."'".',this)" id="boton_detalles_evento'."'".$person->eventid."'".'"><i class="glyphicon glyphicon-search"></i></button>&nbsp;'.
                         '<button type="button" class="btn btn-sm btn-success" title="Ver Registro de Lectura" id="boton_registro_lecturas'.$person->eventid.'" onclick="ver_registros_de_lecturas('."'".$person->eventid."'".')"><i class="glyphicon glyphicon-th-list"></i></button>'.
                         '<img type="hidden" style="display:none;" id="loading3'."".$person->eventid."".'" src="'."".base_url("assets/images/loading.gif")."".'" height="20" width="20" /> '.
                         '<button type="button" class="btn btn-sm btn-warning" title="Ver Estado" onclick="ver_estado_autorizacion('."'".$person->eventid."'".',this)" id="boton_ver_estado_autorizacion'."'".$person->eventid."'".'"> <i class="fa fa-clock-o"></i></button>'.
                         '</center>';
            }
            else
            {
                $text = '
                         <center><button type="button" class="btn btn-sm btn-success" title="Ver Detalles del Evento" onclick="detalles_evento('."'".$person->eventid."'".',this)" id="boton_detalles_evento'."'".$person->eventid."'".'"><i class="glyphicon glyphicon-search"></i></button>
                         <button type="button" class="btn btn-sm btn-success" title="Ver Registro de Lectura" id="boton_registro_lecturas'.$person->eventid.'" onclick="ver_registros_de_lecturas('."'".$person->eventid."'".')"><i class="glyphicon glyphicon-th-list"></i></button>
                         <img type="hidden" style="display:none;" id="loading3'."".$person->eventid."".'" src="'."".base_url("assets/images/loading.gif")."".'" height="20" width="20" />
                         <button type="button" class="btn btn-sm btn-success" title="Registro de Lectura por Alumno" id="boton_registro_lecturas_x_alumno'.$person->eventid.'" onclick="ver_registros_de_lecturas_x_alumno('."'".$person->eventid."'".','.$person->colegioid.')"><i class="glyphicon glyphicon-list"></i></button>
                         <img type="hidden" style="display:none;" id="loading4'."".$person->eventid."".'" src="'."".base_url("assets/images/loading.gif")."".'" height="20" width="20" /> ';

                    if($userid == $person->userid && $person->reenviadoId==null)  //permite reenviar solo si es el propietario, y si no puede reeviar de un evento hijo. 
                        $text .= '<button type="button" class="btn btn-sm btn-success" title="Reenviar" id="btn_reenviar_'.$person->eventid.'" onclick="abrirModalReenviar(this,'.$person->eventid.')"><i class="glyphicon glyphicon-share"></i></button>';
                         

                    $text .= '</center>'.'<strong style="color: #8A0808;">Enviados: '.$cont_eventosusers.', Vistos: '.$person->cont_vistos.',</strong> '.$confirm_text;

                $row[] =  $text;

            }

            $data[] = $row;
        }

        $output = array(
                        "query" => $ultima_query,
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->modelo_evento->count_all($userid,$obtener_privilegios_especiales,$obtener_privilegios_eventos_roles),
                        "recordsFiltered" => $this->modelo_evento->count_filtered($userid,$obtener_privilegios_especiales,$obtener_privilegios_eventos_roles),
                        "data" => $data,
                        "userid" => $userid,
                        "colegios" => $obtener_privilegios_especiales,
                        "roles_eventos" => $obtener_privilegios_eventos_roles,

                );
        //output to json format
        echo json_encode($output);
    }
    public function ajax_list_app_post()// para la APP NO MODIFICAR.
    {
        $userid = $this->utilidades->verifica_userid();
        $obtener_privilegios_especiales =  $this->obtener_privilegios_especiales($userid);
        $obtener_privilegios_eventos_roles = $this->obtener_privilegios_eventos_roles($userid);
        $list = $this->modelo_evento->get_datatables($userid,$obtener_privilegios_especiales,$obtener_privilegios_eventos_roles);

        //echo $this->db->last_query();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $person) {    
            $no++;
        $cole_d = $this->modelo_colegio->get_colegio($person->colegioid);
        
        $colegio = array();
        $colegio['nivel'] = $cole_d->row()->tipo;
        $colegio['id_colegio']= $cole_d->row()->id;
            $colegio['logo'] = $this->utilidades->get_url_img()."fotos_colegios/".$cole_d->row()->logo;
            $row = array();
            $row['user'] =  $this->modelo_usuario->get_users_id($person->userid);
            $row['colegio'] = $colegio;
            //$row['nombre'] = $person->last_name.' '.$person->first_name;
            $row['titulo'] = $person->titulo;
            $row['fecha'] = substr($person->fechaCreacion,8,2).'/'.substr($person->fechaCreacion,5,2).'/'.substr($person->fechaCreacion,0,4).' - '.substr($person->fechaCreacion,11,5).' hs';
            // $row[] = $this->obtener_destinatario($person->eventid);
            //$row[] = $person->fechaCreacion;

            //rol alias
            if($person->nombre_grupo_alias!=null)
                $person->rol_grupo = $person->nombre_grupo_alias;
            else
                if( $person->nombre_grupo_aliasM!=null) {
                    if($person->sexo_creador=='F')
                        $person->rol_grupo = $person->nombre_grupo_aliasF;
                    else
                        $person->rol_grupo = $person->nombre_grupo_aliasM;
                }
            //print_r($person->rol_grupo); die();


            $row['rol'] = $person->rol_grupo;
            $row['evento_id'] = $person->eventid;
            $row['standby'] =$person->standby;
            if($person->userid == $userid)
            {
                $row['creador'] =1;
            }
            else
            {
                $row['creador'] =0;
            }
            if($person->cont_eventosusers == 0)
            {
                $sql_eventosusers = "SELECT COUNT(id)  as total_eventos_user FROM `eventosusers` WHERE eventos_id = ".$person->eventid;
                $sql_eventosusers = $this->db->query($sql_eventosusers);
                $row['cont_eventosusers'] = $sql_eventosusers->row()->total_eventos_user;
            }
            else
            {
                $row['cont_eventosusers'] =$person->cont_eventosusers;
            }
            
            $row['cont_vistos'] =$person->cont_vistos;
            $row['cont_negados'] =$person->cont_negados;
            $row['cont_confirmados'] =$person->cont_confirmados;
            $row['cont_me_gusta'] =$person->cont_me_gusta;
            //add html for action
          /*  if($person->standby == 1)
            {

                $row[] = '<center>'.
                         '<button type="button" class="btn btn-sm btn-success" title="Ver Detalles del Evento" onclick="detalles_evento('."'".$person->eventid."'".',this)" id="boton_detalles_evento'."'".$person->eventid."'".'"><i class="glyphicon glyphicon-search"></i></button>&nbsp;'.
                         '<button type="button" class="btn btn-sm btn-success" title="Ver Registro de Lectura" id="boton_registro_lecturas'.$person->eventid.'" onclick="ver_registros_de_lecturas('."'".$person->eventid."'".')"><i class="glyphicon glyphicon-th-list"></i></button>'.
                         '<img type="hidden" style="display:none;" id="loading3'."".$person->eventid."".'" src="'."".base_url("assets/images/loading.gif")."".'" height="20" width="20" /> '.
                         '<button type="button" class="btn btn-sm btn-warning" title="Ver Estado" onclick="ver_estado_autorizacion('."'".$person->eventid."'".',this)" id="boton_ver_estado_autorizacion'."'".$person->eventid."'".'"> <i class="fa fa-clock-o"></i></button>'.
                         '</center>';
            }
            else
            {
                $row[] = '
                         <center><button type="button" class="btn btn-sm btn-success" title="Ver Detalles del Evento" onclick="detalles_evento('."'".$person->eventid."'".',this)" id="boton_detalles_evento'."'".$person->eventid."'".'"><i class="glyphicon glyphicon-search"></i></button>
                         <button type="button" class="btn btn-sm btn-success" title="Ver Registro de Lectura" id="boton_registro_lecturas'.$person->eventid.'" onclick="ver_registros_de_lecturas('."'".$person->eventid."'".')"><i class="glyphicon glyphicon-th-list"></i></button>
                         <img type="hidden" style="display:none;" id="loading3'."".$person->eventid."".'" src="'."".base_url("assets/images/loading.gif")."".'" height="20" width="20" />
                         <button type="button" class="btn btn-sm btn-success" title="Registro de Lectura por Alumno" id="boton_registro_lecturas_x_alumno'.$person->eventid.'" onclick="ver_registros_de_lecturas_x_alumno('."'".$person->eventid."'".','.$person->colegioid.')"><i class="glyphicon glyphicon-list"></i></button>
                         <img type="hidden" style="display:none;" id="loading4'."".$person->eventid."".'" src="'."".base_url("assets/images/loading.gif")."".'" height="20" width="20" />
                         <button type="button" class="btn btn-sm btn-success" title="Reenviar" id="btn_reenviar_'.$person->eventid.'" onclick="abrirModalReenviar('.$person->eventid.','.$person->colegioid.')"><i class="glyphicon glyphicon-share"></i></button>
                         </center>';
            }*/

            $data[] = $row;
        }

        $output = array(
                       // "query" => $this->db->last_query(),
                       // "draw" => $_POST['draw'],
                        "recordsTotal" => $this->modelo_evento->count_all($userid,$obtener_privilegios_especiales,$obtener_privilegios_eventos_roles),
                        "recordsFiltered" => $this->modelo_evento->count_filtered($userid,$obtener_privilegios_especiales,$obtener_privilegios_eventos_roles),
                        "data" => $data,
                        "userid" => $userid,
                       // "colegios" => $obtener_privilegios_especiales,
                     //   "roles_eventos" => $obtener_privilegios_eventos_roles,
                       

                );
        //output to json format
        //echo json_encode($output);
        $this->response([
                    'status' => TRUE,
                    'data' => $output
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
    }
    //fin data table nuevo
    public function eventosTodos_get() //todos los que puede ver //ANDRES
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_OK);//HTTP_BAD_REQUEST
        }
        else
        {
            //$nombregrupo = $_GET['nombregrupo'];
            //$idcolegio = $_GET['idcolegio'];

            $grupos = $this->modelo_usuario->get_rolesXidusuario_orden($userid);
            if($grupos)
            {   $output=array();

                foreach ($grupos as $key ) //esto recorre colegios, por cada colegio tiene el rol de mayor prioridad o rango
                {
                    //$nombregrupo=$key->nombregrupo;
                    $colegio=$key->idcolegio;
                    $obtener_privilegios_especiales =  $this->obtener_privilegios_especiales($userid);
                    $obtener_privilegios_eventos_roles = $this->obtener_privilegios_eventos_roles($userid);

                 /*  if( ($nombregrupo=='Institucion') || ($nombregrupo=='Directivo') || ($nombregrupo=='')|| ($nombregrupo=='Administrativo')  || ($nombregrupo=='Representante Legal') )
                    {
                        $eventos = $this->modelo_evento->obtener_eventosTodos($colegio); // Obtengo todos los eventos del colegio.
                    }
                    else
                    {   */
                        $eventos1 = $this->modelo_evento->obtener_eventosCreados($userid,$colegio); // Obtengo los creados por mi, en un colegio.
                        $eventos2 = $this->modelo_evento->obtener_eventosRecibidos($userid,$colegio); // Obtengo los eventos recibidos, en un colegio.
                        //$eventos = array_merge($eventos1, $eventos2);

                        if( ($eventos1) && ($eventos2) )
                            $eventos = array_merge($eventos1, $eventos2);
                        else  if($eventos1)
                                    $eventos = $eventos1;
                                else $eventos = $eventos2;

                        $eventos = $this->unique_multidim_array($eventos, 'eventid');
                        //print_r($obtener_privilegios_eventos_roles);
                        /*if($obtener_privilegios_eventos_roles['eventos_roles'] == 1)
                        {
                            $eventos_x_rol = $this->modelo_evento->obtener_eventosCreadosX_rol($userid,$colegio,$obtener_privilegios_eventos_roles);
                            //print_r($eventos_x_rol);
                            if( ($eventos) && ($eventos_x_rol) )
                            {
                                $eventos = array_merge($eventos, $eventos_x_rol);

                            }
                            else if($eventos)
                            {
                                $eventos = $eventos;
                            }
                            else
                            {
                                $eventos = $eventos_x_rol;
                            }
                            $eventos = $this->unique_multidim_array($eventos, 'eventid');
                        }*/


                   // }

                    foreach ($eventos as $key )
                    { //agregar campo si es propietario o no. 1 o 0
                        if($key->userid==$userid)
                            $key->owner = 1;
                        else $key->owner = 0;


                        //rol alias
                        if($key->group_alias!=null)
                            $key->name = $key->group_alias;
                        else
                            if( $key->aliasM!=null) {
                                if($key->sexo=='F')
                                    $key->name = $key->aliasF;
                                else
                                    $key->name = $key->aliasM;
                            }
                        unset($key->aliasM);
                        unset($key->aliasF);
                        unset($key->group_alias);
                        unset($key->sexo);


                        $output[] = $key;
                    }
                }


            //$eventos = $this->modelo_evento->obtener_eventosNew($userid);
            // obtiene los eventos de el usuario X, que no han sido vistos
            // Si existe mas de un resultado, lo mando
            if (isset($output))
            {
                $output = json_encode($output);
                $this->response($output, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => TRUE,
                    'message' => 'vacio'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            }//fin if grupos
        }
    }

    /**
     * $arre_eventos es un string que viene en JSON y se convierte en arreglo PHP
     */
    public function visto_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            $arre_eventos = json_decode($this->input->post("eventosid"));
            $token = $this->input->post("token");
        	// Obtengo y proceso el arreglo de eventos y consulto que al menos tenga 1 elemento el arreglo
        	if ((isset($arre_eventos)) && (!empty($arre_eventos)) && (is_array($arre_eventos)) && (count($arre_eventos) > 0))
        	{
        		foreach ($arre_eventos as $keyev => $valev)
        		{
                    $val = json_decode($valev);
        			$this->modelo_evento->marcar_visto($val[0]);
                    //si existe token es porque viene del movil e inserto en
                    //eventosuser_detalle
                    if(isset($token)){
                        $data["eventosusers_id"] = $val[0];
                        //obtengo el id del token
                        $push_result = $this->modelo_usuario->existe_registerid_gcm($userid, $token);
                        foreach ($push_result as $key => $value) {
                            $token_id = $value->id;
                            $data["token_id"] = $token_id;
                            $this->modelo_evento->marcar_visto_detalle($data);
                        }
                    }
        		}
                $this->response("OK", REST_Controller::HTTP_OK);
        	}
        	else
        	{
	            $this->response([
	                'status' => FALSE,
	                'message' => 'Debe enviar un arreglo de IDs de eventos con al menos 1 ID'
	            ], REST_Controller::HTTP_BAD_REQUEST);
        	}
		}
    }

    /**
     * $arre_eventos es un string que viene en JSON y se convierte en arreglo PHP
     */
    public function aceptado_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            $arre_eventos = json_decode($this->input->post("eventosid"));
            $pin = $this->input->post('pin'); // obtengo el pin, si es que vino en la url
            $existe = $this->utilidades->verifica_pin($pin);//verifico que el pin ingresado por el usuario es correcto
            $token = $this->input->post("token");
            // Si existe mas de un resultado, el pin esta bien
            if ($existe == 1)
            {
                // Obtengo y proceso el arreglo de eventos y consulto que al menos tenga 1 elemento el arreglo
                if ((isset($arre_eventos)) && (!empty($arre_eventos)) && (is_array($arre_eventos)) && (count($arre_eventos) > 0))
                {
                    foreach ($arre_eventos as $keyev => $valev)
                    {
                        $val = json_decode($valev);
                        //primer parametro es el id del evento y el segundo es el id del alumno
                        $this->modelo_evento->marcar_aceptado($val[0]);
                        //si existe token es porque viene del movil e inserto en
                        //eventosuser_detalle
                        if(isset($token)){
                            $data["eventosusers_id"] = $val[0];
                            //obtengo el id del token
                            $push_result = $this->modelo_usuario->existe_registerid_gcm($userid, $token);
                            foreach ($push_result as $key => $value) {
                                $token_id = $value->id;
                                $data["token_id"] = $token_id;
                                $this->modelo_evento->marcar_aceptado_detalle($data);
                            }
                        }
                    }

                    $this->response("OK", REST_Controller::HTTP_OK);
                }
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Debe enviar un arreglo de IDs de eventos con al menos 1 ID',
                        'result' =>  $pin,
                        'consulta' =>$this->db->last_query()
                    ], REST_Controller::HTTP_BAD_REQUEST);
                }
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'El pin no es correcto'
                ], REST_Controller::HTTP_BAD_REQUEST);
            }

        }
    }

    /**
     * $arre_eventos es un string que viene en JSON y se convierte en arreglo PHP
     */
    public function rechazado_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO',
                'result' =>  $pin
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            $arre_eventos = json_decode($this->input->post("eventosid"));
            $pin = $this->input->post('pin'); // obtengo el pin, si es que vino en la url
            $existe = $this->utilidades->verifica_pin($pin);//verifico que el pin ingresado por el usuario es correcto
            $token = $this->input->post("token");
            // Si existe mas de un resultado, el pin esta bien
            if ($existe == 1)
            {
                // Obtengo y proceso el arreglo de eventos y consulto que al menos tenga 1 elemento el arreglo
                if ((isset($arre_eventos)) && (!empty($arre_eventos)) && (is_array($arre_eventos)) && (count($arre_eventos) > 0))
                {
                    foreach ($arre_eventos as $keyev => $valev)
                    {
                        $val = json_decode($valev);
                        $this->modelo_evento->marcar_rechazado($val[0]);
                        //si existe token es porque viene del movil e inserto en
                        //eventosuser_detalle
                        if(isset($token)){
                            $data["eventosusers_id"] = $val[0];
                            //obtengo el id del token
                            $push_result = $this->modelo_usuario->existe_registerid_gcm($userid, $token);
                            foreach ($push_result as $key => $value) {
                                $token_id = $value->id;
                                $data["token_id"] = $token_id;
                                $this->modelo_evento->marcar_rechazado_detalle($data);
                            }
                        }
                    }
                    $this->response("OK", REST_Controller::HTTP_OK);
                }
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Debe enviar un arreglo de IDs de eventos con al menos 1 ID'
                    ], REST_Controller::HTTP_BAD_REQUEST);
                }
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'El pin no es correcto'
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    /**
     * $arre_eventos es un string que viene en JSON y se convierte en arreglo PHP
     */
    public function megusta_post()  //y ella?
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            $eventousers_id = $this->input->post("eventousers_id");
            $token = $this->input->post("token");
            // Obtengo y proceso el arreglo de eventos y consulto que al menos tenga 1 elemento el arreglo
            if ($eventousers_id)
            {
                $me_gusta = $this->input->post("me_gusta");
                if(isset($me_gusta)){
                    if($me_gusta == 1){
                        $this->modelo_evento->marcar_megusta($eventousers_id);
                        //si existe token es porque viene del movil e inserto en
                        //eventosuser_detalle
                        if(isset($token)){
                            $data["eventosusers_id"] = $eventousers_id;
                            //obtengo el id del token
                            $push_result = $this->modelo_usuario->existe_registerid_gcm($userid, $token);
                            foreach ($push_result as $key => $value) {
                                $token_id = $value->id;
                                $data["token_id"] = $token_id;
                                $this->modelo_evento->marcar_megusta_detalle($data);
                            }
                        }
                    }
                    else{
                        $this->modelo_evento->desmarcar_megusta($eventousers_id);
                        //si existe token es porque viene del movil e inserto en
                        //eventosuser_detalle
                        if(isset($token)){
                            $data["eventosusers_id"] = $eventousers_id;
                            //obtengo el id del token
                            $push_result = $this->modelo_usuario->existe_registerid_gcm($userid, $token);
                            foreach ($push_result as $key => $value) {
                                $token_id = $value->id;
                                $data["token_id"] = $token_id;
                                $this->modelo_evento->desmarcar_megusta_detalle($data);
                            }
                        }
                    }
                    $this->response([
                        'status' => TRUE,
                        'last_query' =>  $this->db->last_query()
                    ], REST_Controller::HTTP_OK);
                }
                else{
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Faltan datos para terminar la operación'
                    ], REST_Controller::HTTP_BAD_REQUEST);
                }
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Debe enviar un evento como mínimo'
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    public function construir_mensaje()
    {
        return "

        ";
    }
    public function obtener_privilegios_especiales($userid)
    {
        $colegios = '0';
        $coleg=array();
        $result = $this->modelo_evento->obtener_privilegios_especiales($userid);
        if($result->num_rows())
        {
            foreach ($result->result() as $key)
            {
                   $colegios = $key->colegio_id.', '.$colegios;
                   $coleg[] = $key->colegio_id;
            }
           // $coleg = join(',',$coleg);
             $retorna = array ('colegios'=>$coleg, 'privilegio'=>'1');
            return  $retorna;
        }
        else
        {
            $retorna = array ('colegios'=>$coleg, 'privilegio'=>'0');
            return  $retorna;
        }


    }
    public function obtener_privilegios_eventos_roles($userid)
    {
        $roless = '0';
        $roles=array();
        $result = $this->modelo_evento->obtener_privilegios_eventos_roles($userid);
        if($result->num_rows())
        {
            foreach ($result->result() as $key)
            {
                   //$colegios = $key->roles_eventos.', '.$colegios;
                $key_aux = explode(',', $key->roles_eventos);
                foreach ($key_aux as $rol)
                {
                    # code...
                     $roles[] = $rol;
                }
                   //
              //  $roles[] = $key->roles_eventos;
            }
            //$roles = join(',',$roles);
            $retorna = array ('roles'=>$roles, 'eventos_roles'=>'1');
            return  $retorna;
        }
        else
        {
            $retorna = array ('roles'=>$roles, 'eventos_roles'=>'0');
            return  $retorna;
        }


    }

    public function obtener_eventos_autorizar_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            $id_grupo = $this->input->post("id_grupo");
            $name_grupo = $this->input->post("name_grupo");
            $colegioId = $this->input->post("colegioId");


            //determina si trabaja con areas
            $cole = $this->modelo_colegio->get_colegio($colegioId);
            if($cole)
                $areaBan = $cole->row()->areas;
            else $areaBan = 0;


            $reglascole = $this->modelo_permisos->get_reglas3($colegioId);
            $grupos = array();
            if($reglascole){
                foreach ($reglascole as $regla) {

                    $arreSup = array();

                    if($regla->supervisado!= null)
                        $arreSup = explode(",", $regla->supervisado);

                    $busq = in_array($id_grupo, $arreSup);

                    if($busq){
                        if($areaBan==1){//trabaja con areas


                            if( ($regla->rolname === 'Docente') && ($name_grupo==='Jefe Area') ){

                                $usersIds = $groupsIds = array();


                                //get usergroup
                                $data0 = array(
                                    'colegio_id'=>$colegioId,
                                    'group_id'=>$id_grupo,
                                    'user_id'=>$userid,
                                );
                                $user_group = $this->modelo_usuario->verificar_user_group($data0);

                                //traer areas del jefe_area
                                $data = array(
                                    'colegio_id'=>$colegioId,
                                    'nivel_id'=>null,
                                    'jefe_id'=>$user_group->id,
                                );
                                $this->load->model("modelo_areas");
                                $areas = $this->modelo_areas->obtener_areas($data);
                                $areasId = array();
                                if($areas){
                                    foreach ($areas as $area)
                                        $areasId[] = $area->id;
                                    $areasId = join(',',$areasId);
                                }
                                else $areasId = 0;



                                $anioA = $this->utilidades->obtener_planestudioaXcolegio( $colegioId );
                                $planest_Id = $this->utilidades->get_planestudio( $anioA );

                                //traer docentes del area; users ids
                                $docentes = $this->modelo_areas->obtener_docentes_By_areas2($areasId, $planest_Id);
                                if($docentes){
                                    foreach ($docentes as $row){
                                        $usersIds[] = $row->user_id;
                                        $groupsIds[] = $row->group_id;//debe ser siempre el mismo
                                    }

                                }

                                // print_r($usersIds);
                                //print_r( count( $usersIds));

                                if(count($usersIds)== 0)
                                    $usersIds = 0;
                                else  $usersIds = join(',',$usersIds);

                                if(count($groupsIds)== 0)
                                    $groupsIds = 0;
                                else $groupsIds = join(',',$groupsIds);


                            }
                            else
                                $grupos[] = $regla->group_id;

                        }
                        else
                            $grupos[] = $regla->group_id;
                    }
                }

            }

            if(count($grupos)>0)
                $grupos = join(',',$grupos);
            else $grupos = 0;

            //print_r($grupos);

            $eventos1 = $this->modelo_evento->obtener_eventos_autorizar($colegioId, $grupos);

            $eventos2 = false;
            if($areaBan==1)
                if( isset($usersIds) && isset($groupsIds) )
                    $eventos2 = $this->modelo_evento->obtener_eventosCreados_autorizar($usersIds, $groupsIds, $colegioId);


            //print_r($eventos1);
            if( ($eventos1) && ($eventos2) )
                $eventos = array_merge($eventos1, $eventos2);
            else  if($eventos1)
                        $eventos = $eventos1;
                    else $eventos = $eventos2;


            if ($eventos)
            {

                $arre = array(
                        'status' => 1,
                        'eventos' => $eventos,
                     );
                $this->response($arre, REST_Controller::HTTP_OK);
            }
            else
            {
                $eventos = array(
                        'status' => 0,
                        'message' => 'No_event',
                     );
                $this->response($eventos, REST_Controller::HTTP_OK);
            }
        }
    }


    public function buscar_eventos_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {

            $palabra = $this->input->post("palabra");
            if(isset($palabra))
            {
                $eventos = $this->modelo_evento->buscar_eventos($userid,$palabra); // obtiene los eventos de el usuario X
                if ($eventos)
                {

                    $result = array();
                    foreach ($eventos as $key )
                    {
                        $puede_ver = 0;
                        if($this->modelo_alumno->is_alumno($key->destinatario) == 1)
                        {
                            if($this->modelo_alumno->pago($key->destinatario) == 1)
                            {
                                $puede_ver = 1;

                            }
                        }
                        else
                        {
                            $puede_ver = 1;

                        }

                        if($puede_ver == 1)
                        {
                            $tipo = 'image';
                                $fotos = $this->modelo_upload->obtener_files_x_tipo($key->evento_id,$tipo);

                                $arrefotos = array();

                                foreach ($fotos as $url )
                                {
                                  $arrefotos[]= $url;
                                }

                                $cont_me_gusta = $this->modelo_evento->obtener_cant_me_gusta_x_evento($key->evento_id,$tipo);
                                $key->cant_me_gusta = $cont_me_gusta;
                                $key->urls_fotos = $arrefotos;

                                //parte que agregue
                               //obenter_cant_files
                                $cant_files2 = "SELECT eventos.*,COUNT(files.id) as cantfiles FROM eventos LEFT JOIN files ON eventos.id = files.evento_id WHERE  eventos.id = $key->evento_id";
                                //$cant_files2 = "CALL obtener_cant_files_x_evento(".$key->evento_id.")";
                                $cant_files2 = $this->db->query($cant_files2);
                                $cant_files2 = $cant_files2->row();
                                $key->cantfiles = $cant_files2->cantfiles;
                                //mysqli_next_result( $this->db->conn_id );
                                //fin

                                //grupo
                                 $groups = "SELECT groups.name nombre_grupo, groups.aliasM nombre_grupo_aliasM, groups.aliasF nombre_grupo_aliasF FROM groups  WHERE  id = $key->group_id";
                                //$groups = "CALL obtener_eventos_grupo_creador(".$key->group_id.")";
                                $groups = $this->db->query($groups);
                                $groups = $groups->row();
                                $key->nombre_grupo = $groups->nombre_grupo;
                                //$key->nombre_grupo_aliasM = $groups->nombre_grupo_aliasM;
                                //$key->nombre_grupo_aliasF = $groups->nombre_grupo_aliasF;

                                mysqli_next_result( $this->db->conn_id );
                                //fin

                                //datos del creador
                                $sql_creador = "SELECT users.first_name nombre_creador,users.last_name apellido_creador,users.sexo sexo_creador,users_groups.group_alias nombre_grupo_alias FROM users
                                JOIN users_groups ON users_groups.user_id = users.id
                                where users.id = $key->users_id
                                AND users_groups.estado = 1";
                                //$sql_creador = "CALL obtener_creador(".$key->users_id.")";
                                $sql_creador = $this->db->query($sql_creador);
                                $sql_creador = $sql_creador->row();
                                $key->nombre_creador = $sql_creador->nombre_creador;
                                $key->apellido_creador = $sql_creador->apellido_creador;
                                //$key->sexo_creador = $sql_creador->sexo_creador;
                                //$key->nombre_grupo_alias = $sql_creador->nombre_grupo_alias;
                                //mysqli_next_result( $this->db->conn_id );
                                //fin


                                //ALIAS ROL
                                if($sql_creador->nombre_grupo_alias!=null){
                                    //echo "entra1";
                                    $key->nombre_grupo = $sql_creador->nombre_grupo_alias;
                                }
                                else
                                    if( $groups->nombre_grupo_aliasM!=null) {
                                        //echo "entra2";
                                        if($sql_creador->sexo_creador=='F'){
                                            //echo "entra3";
                                            $key->nombre_grupo = $groups->nombre_grupo_aliasF;
                                        }
                                        else{
                                            //echo "entra4";
                                            $key->nombre_grupo = $groups->nombre_grupo_aliasM;
                                        }
                                    }
                                /*unset($key->nombre_grupo_aliasM);
                                unset($key->nombre_grupo_aliasF);
                                unset($key->nombre_grupo_alias);
                                unset($key->sexo_creador);*/


                                //destinatario
                                //$sql_destinatario = "CALL obtener_eventos_datos_destinatario(".$key->destinatario.")";
                                $sql_destinatario = "SELECT users.first_name nombre, users.last_name apellido, users.foto, users.id user_id_destinatario, users.sexo from users where users.id = $key->destinatario";
                                $sql_destinatario = $this->db->query($sql_destinatario);
                                $sql_destinatario = $sql_destinatario->row();
                                $key->nombre = $sql_destinatario->nombre;
                                $key->apellido = $sql_destinatario->apellido;
                                $key->foto = $sql_destinatario->foto;
                                if($key->foto != NULL)
                                {
                                    $key->foto = $this->utilidades->get_url_img().'fotos_usuarios/'.$key->foto;
                                }
                                else
                                {
                                     $key->foto = $this->utilidades->get_url_img().'fotos_usuarios/default-avatar.png';
                                }
                                mysqli_next_result( $this->db->conn_id );

                                //agregar foto colegio
                                if($key->logo != NULL)
                                {
                                    $key->logo = $this->utilidades->get_url_img().'fotos_colegios/'.$key->logo;
                                }
                                else
                                {
                                    $key->logo = $this->utilidades->get_url_img().'fotos_colegios/default_esc.png';
                                }
                                //fin

                                $result[]=$key;
                        }
                    }
                   // print_r($result);
                    // die();
                  $this->response($result, REST_Controller::HTTP_OK);
                }
                // Sino, envio respuesta vacia
                else{
                        $this->response($eventos, REST_Controller::HTTP_OK);
                    }
            }
        else{
                 $this->response($eventos, REST_Controller::HTTP_OK);
            }

        }
    }
    public function registro_lectura_obtener_div_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_OK);//HTTP_BAD_REQUEST
        }
        else
        {
            $result = NULL;
            $idevento = $this->get('id'); // obtengo el id del evento, si es que vino en la url
            $idcolegio = $this->get('idcolegio');
            $anioA = $this->utilidades->obtener_planestudioaXcolegio( $idcolegio );
            $planest_Id = $this->utilidades->get_planestudio( $anioA );
            $result2 = $this->modelo_division->get_divisiones_x_evento_SinEspecialidad($idcolegio,$planest_Id,$idevento);
            $result1 = $this->modelo_division->get_divisiones_x_evento_ConEspecialidad($idcolegio,$planest_Id,$idevento);
            if( ($result1)&&($result2) )
                    $result = array_merge($result2, $result1);
                else  if($result1)
                        $result = $result1;
                      else
                        if($result2)
                            $result = $result2;


            $titulo_evento = $this->modelo_evento->obtener_datosEvento($idevento);
            $titulo_evento = $titulo_evento->titulo;


            if ($result)
            {
                $result = json_encode($result);
                //$this->response($eventos, REST_Controller::HTTP_OK);
                $message = [

                            'satus' => TRUE,
                            'divisiones' => $result,
                            'titulo' => $titulo_evento,
                            'id_colegio' => $idcolegio,
                            'id_evento' => $idevento,
                            'planest_Id' => $planest_Id,
                        ];

                $this->set_response($message, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'titulo' => $titulo_evento,
                    'message' => 'No existen divisiones cargadas',
                ], REST_Controller::HTTP_OK);
            }

        }
    }
    public function registro_lectura_alumnos_x_div_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_OK);//HTTP_BAD_REQUEST
        }
        else
        {
            $id_evento = $this->get('id_evento'); // obtengo el id del evento, si es que vino en la url
            $idcolegio = $this->get('idcolegio');
            $division_id = $this->get('division_id');
            $alumnos = $this->modelo_evento->get_alumnos_eventosusers($division_id,$id_evento);
            $text = '';

            $eventos_para_alumnos = 0;
            foreach ($alumnos as $alu )
            {
                $eventos_para_alumnos = 1;

                $confirm = $alu->confirm;
                $fecha_visto = 'No Leido';
                if($alu->fechaVisto != null)
                {
                    $fecha_visto = '<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span> '.substr($alu->fechaVisto,8,2).'/'.substr($alu->fechaVisto,5,2).'/'.substr($alu->fechaVisto,0,4).' - '.substr($alu->fechaVisto,11,5).'hs';
                }
                $fechaConfirmacion = 'No Corresponde';
                if($alu->fechaConfirmacion != null and $confirm == 1)
                {
                    $fechaConfirmacion = substr($alu->fechaConfirmacion,8,2).'/'.substr($alu->fechaConfirmacion,5,2).'/'.substr($alu->fechaConfirmacion,0,4);
                }
                $aceptado = 'No Corresponde';
                if($alu->aceptado == 1 and $confirm == 1)
                {
                    $aceptado = 'SI';
                }
                $me_gusta = 'SI';
                if($alu->me_gusta == 0)
                {
                    $me_gusta = ' --- ';
                }
                $text = $text."
                                <div class='panel panel-info'>
                                  <div class='panel-heading'>
                                    <h3 class='panel-title'>Alumno: ".$alu->last_name.", ".$alu->first_name."</h3>
                                  </div>
                                  <div class='panel-body'>
                            <br>
                            <table id='' class='table table-striped table-bordered dt-responsive nowrap warning' style='width: 100%;''>
                                  <thead>
                                    <tr style='color: #8A0808;'>

                                      <th>Fecha de Lectura</th>
                                      <th>Fecha de Confirmacion</th>
                                      <th>Confirmación</th>
                                      <th>Me Gusta</th>
                                    </tr>
                                  </thead>
                                  <tbody >
                                      <tr>

                                        <th>".$fecha_visto."</th>
                                        <th>".$fechaConfirmacion."</th>
                                        <th>".$aceptado."</th>
                                        <th>".$me_gusta."</th>
                                    </tr>
                                  </tbody>
                                </table>

                            ";
                $tutores = $this->modelo_evento->get_tutores_alumno_eventosusers($id_evento,$alu->alu_user_id);
                $text = $text."
                            <h4>Registro de Lectura de Tutores</h4>
                            <br>
                            <table id='' class='table table-striped table-bordered dt-responsive nowrap ' style='width: 100%;''>
                                  <thead>
                                    <tr style='color: #8A0808;'>
                                        <th>Tutor</th>
                                        <th>Fecha de Lectura</th>
                                        <th>Fecha de Confirmacion</th>
                                        <th>Confirmación</th>
                                        <th>Me Gusta</th>
                                    </tr>
                                  </thead>
                                  <tbody >

                            ";
                $cont = 0;
                foreach ($tutores as $tutor)
                {
                    $cont = 1;
                    $fechaConfirmacion = 'No Corresponde';
                    if($tutor->fechaConfirmacion != null and $confirm == 1)
                    {
                        $fechaConfirmacion = substr($tutor->fechaConfirmacion,8,2).'/'.substr($tutor->fechaConfirmacion,5,2).'/'.substr($tutor->fechaConfirmacion,0,4);
                    }
                    $aceptado = 'No Corresponde';
                    if($tutor->aceptado == 1 and $confirm == 1)
                    {
                        $aceptado = 'SI';
                    }
                    $fecha_visto = 'No Leido';
                    if($tutor->fechaVisto != null)
                    {
                        $fecha_visto = '<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span> '.substr($tutor->fechaVisto,8,2).'/'.substr($tutor->fechaVisto,5,2).'/'.substr($tutor->fechaVisto,0,4).' - '.substr($tutor->fechaVisto,11,5).'hs';
                    }
                    $me_gusta = 'SI';
                    if($tutor->me_gusta == 0)
                    {
                        $me_gusta = ' --- ';
                    }
                    $text = $text."

                                      <tr>
                                        <th>".$tutor->last_name.", ".$tutor->first_name."</th>
                                        <th>".$fecha_visto."</th>
                                        <th>".$fechaConfirmacion."</th>
                                        <th>".$aceptado."</th>
                                        <th>".$me_gusta."</th>
                                    </tr>

                            ";
                }

                if($cont == 0)
                {
                    $this->load->model("modelo_tutor");
                    $tutores_verificar = $this->modelo_tutor->obtener_tutores_x_alumno($alu->alu_user_id);
                    if($tutores_verificar == false)
                    {
                        $text = $text."

                                          <tr>
                                            <th colspan='5' style='color: red;'><center>No Tiene Tutores Asignados</center></th>

                                        </tr>

                                ";
                    }
                    else
                    {
                        $text = $text."

                                          <tr>
                                            <th colspan='5' style='color: red;'><center>El comunicado no fue enviado a sus Tutores</center></th>

                                        </tr>

                                ";
                    }

                }
                $text = $text.'</tbody>
                                </table>

                    </div></div>

                            ';
            }
            if($eventos_para_alumnos == 0)
            {
                //die();
                $text = $text."<h4>Este Comunicado no fue Enviado a los Alumnos</h4><br>";
                $alumnos = $this->modelo_evento->get_alumnos_eventosusers_x_tutores($division_id,$id_evento);
               // echo "aa";
                foreach ($alumnos as $alu )
                {
                    $confirm = $alu->confirm;
                    $text = $text."
                                <div class='panel panel-info'>
                                  <div class='panel-heading'>
                                    <h3 class='panel-title'>Alumno: ".$alu->last_name.", ".$alu->first_name."</h3>
                                  </div>
                                  <div class='panel-body'>
                            ";
                    $tutores = $this->modelo_evento->get_tutores_alumno_eventosusers($id_evento,$alu->alu_user_id);
                    $text = $text."
                            <h4>Registro de Lectura de Tutores</h4>
                            <br>
                            <table id='' class='table table-striped table-bordered dt-responsive nowrap ' style='width: 100%;''>
                                  <thead>
                                    <tr style='color: #8A0808;'>
                                        <th>Tutor</th>
                                        <th>Fecha de Lectura</th>
                                        <th>Fecha de Confirmacion</th>
                                        <th>Confirmación</th>
                                        <th>Me Gusta</th>
                                    </tr>
                                  </thead>
                                  <tbody >

                            ";
                    $cont = 0;
                    foreach ($tutores as $tutor)
                    {
                        $cont = 1;
                        $fechaConfirmacion = 'No Corresponde';
                        if($tutor->fechaConfirmacion != null and $confirm == 1)
                        {
                            $fechaConfirmacion = substr($tutor->fechaConfirmacion,8,2).'/'.substr($tutor->fechaConfirmacion,5,2).'/'.substr($tutor->fechaConfirmacion,0,4);
                        }
                        $aceptado = 'No Corresponde';
                        if($tutor->aceptado == 1 and $confirm == 1)
                        {
                            $aceptado = 'SI';
                        }
                        $fecha_visto = 'No Leido';
                        if($tutor->fechaVisto != null)
                        {
                            $fecha_visto = '<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span> '.substr($tutor->fechaVisto,8,2).'/'.substr($tutor->fechaVisto,5,2).'/'.substr($tutor->fechaVisto,0,4).' - '.substr($tutor->fechaVisto,11,5).'hs';
                        }
                        $me_gusta = 'SI';
                        if($tutor->me_gusta == 0)
                        {
                            $me_gusta = ' --- ';
                        }
                        $text = $text."

                                          <tr>
                                            <th>".$tutor->last_name.", ".$tutor->first_name."</th>
                                            <th>".$fecha_visto."</th>
                                            <th>".$fechaConfirmacion."</th>
                                            <th>".$aceptado."</th>
                                            <th>".$me_gusta."</th>
                                        </tr>

                                ";
                    }
                    if($cont == 0)
                    {
                        $text = $text."

                                          <tr>
                                            <th colspan='5' style='color: red;'><center>No Tiene Tutores Asignados</center></th>

                                        </tr>

                                ";
                    }
                    $text = $text.'</tbody>
                                </table>

                    </div></div>

                            ';
                }
            }
            $text = $text.'';
            $text = $text.'';
            $message = [

                            'satus' => TRUE,
                            'division_id' => $division_id,
                            'registro_lectura_div' => $text,
                            'id_colegio' => $idcolegio,
                            'id_evento' => $id_evento,
                        ];

                $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }
    public function eventos_enviados_eventosusers_app_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_OK);//HTTP_BAD_REQUEST
        }
        else
        {
            $idevento = $this->get('id'); // obtengo el id del evento, si es que vino en la url
            $eventos = $this->modelo_evento->obtener_eventosenviados_por_ideventos2($userid,$idevento); // Obtengo los creados por mi.
            $info_evento = $this->modelo_evento->obtener_datosEvento($idevento);
            $titulo_evento = $info_evento->titulo;
            if ($eventos)
            {
                $result = array();
                $cant_vistos = 0;
                $cant_no_vistos = 0;
                $confirmados = 0;
                $no_confirmados = 0;
                $confirmacion_negada = 0;
                foreach ($eventos as $key) 
                {
                    $user = $this->modelo_usuario->get_users_id($userid);
                     //destinatario
                    if($key->visto == 0)
                    {
                        $cant_no_vistos++;
                    }
                    else
                    {
                        $cant_vistos++;
                    }
                    if($info_evento->confirm == 1)
                    {
                        if($key->aceptado == -1)
                        {
                            $confirmacion_negada++;
                        }
                        if($key->aceptado == 0)
                        {
                            $no_confirmados++;
                        }
                        if($key->aceptado == 1)
                        {
                            $confirmados++;
                        }
                    }
                            //$sql_user = "CALL obtener_eventos_datos_destinatario(".$key->users_id.")";
                            $sql_user = "SELECT users.first_name nombre, users.last_name apellido, users.foto, users.id user_id_destinatario, users.sexo from users where users.id = $key->users_id";
                            $sql_user = $this->db->query($sql_user);
                            $sql_user = $sql_user->row();
                            $key->nombre = $sql_user->nombre;
                            $key->apellido = $sql_user->apellido;
                            $key->users_id = $sql_user->user_id_destinatario;
                            $key->descripcion = '';
                            $key->titulo = '';
                            if($sql_user->foto != '')
                            {
                                $key->foto_destinariario = $this->utilidades->get_url_img()."fotos_usuarios/".$sql_user->foto;
                            }
                            else
                            {
                                
                                $key->foto_destinariario = $this->utilidades->get_url_img()."fotos_usuarios/default-avatar.png";
                            }
                           // print_r($key);
                           // die();
                            
                            mysqli_next_result( $this->db->conn_id );

                    $grupo = $this->modelo_evento->obtener_grupo_menorOrden($key->users_id,$key->colegio_id);

                     //rol alias
                    if($grupo->group_alias!=null)
                        $grupo->name = $grupo->group_alias;
                    else
                        if( $grupo->aliasM!=null) {
                            if($sql_user->sexo=='F')
                                $grupo->name = $grupo->aliasF;
                            else
                                $grupo->name = $grupo->aliasM;
                        }

                    $key->grupo_rol = $grupo->name;
                    $result[]=$key;
                }
                $eventos = json_encode($result);
                //$this->response($eventos, REST_Controller::HTTP_OK);
                $message = [

                            'satus' => TRUE,
                            'eventousers' => $result,
                            'titulo' => $titulo_evento,
                            'vistos' => $cant_vistos,
                            'no_vistos' => $cant_no_vistos,
                            'confirm' => $info_evento->confirm,
                            'confirmados' => $confirmados,
                            'no_confirmados' => $no_confirmados,
                            'confirmacion_negada' => $confirmacion_negada
                        ];

                $this->set_response($message, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => TRUE,
                    'message' => 'vacio'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

        }
    }
    public function registro_lectura_obtener_div_app_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_OK);//HTTP_BAD_REQUEST
        }
        else
        {
            $result = NULL;
            $idevento = $this->get('id'); // obtengo el id del evento, si es que vino en la url
            $idcolegio = $this->get('idcolegio');
            $anioA = $this->utilidades->obtener_planestudioaXcolegio( $idcolegio );
            $planest_Id = $this->utilidades->get_planestudio( $anioA );
            $result2 = $this->modelo_division->get_divisiones_x_evento_SinEspecialidad($idcolegio,$planest_Id,$idevento);
            $result1 = $this->modelo_division->get_divisiones_x_evento_ConEspecialidad($idcolegio,$planest_Id,$idevento);
            if( ($result1)&&($result2) )
                    $result = array_merge($result2, $result1);
                else  if($result1)
                        $result = $result1;
                      else
                        if($result2)
                            $result = $result2;


            $info_evento = $this->modelo_evento->obtener_datosEvento($idevento);
            $titulo_evento = $info_evento->titulo;


            if ($result)
            {
                //$result = json_encode($result);
                //$this->response($eventos, REST_Controller::HTTP_OK);
                $message = [

                            'satus' => TRUE,
                            'divisiones' => $result,
                            'titulo' => $titulo_evento,
                            'id_colegio' => $idcolegio,
                            'id_evento' => $idevento,
                            'planest_Id' => $planest_Id,
                            'confirm' => $info_evento->confirm,
                        ];

                $this->set_response($message, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'titulo' => $titulo_evento,
                    'message' => 'No existen divisiones cargadas',
                ], REST_Controller::HTTP_OK);
            }

        }
    }
    public function registro_lectura_alumnos_x_div_app_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_OK);//HTTP_BAD_REQUEST
        }
        else
        {
            $id_evento = $this->get('id_evento'); // obtengo el id del evento, si es que vino en la url
            $idcolegio = $this->get('idcolegio');
            $division_id = $this->get('division_id');
            $alumnos = $this->modelo_evento->get_alumnos_eventosusers($division_id,$id_evento);
            $text = '';
            $info_evento = $this->modelo_evento->obtener_datosEvento($id_evento);
            
            $eventos_para_alumnos = 0;
            $alumnos_array = array();
            foreach ($alumnos as $alu )
            {
                $eventos_para_alumnos = 1;
                $mensaje_alumnos = "";
                $confirm = $alu->confirm;
                $fecha_visto = 'No Leido';
                if($alu->fechaVisto != null)
                {
                    $fecha_visto = substr($alu->fechaVisto,8,2).'/'.substr($alu->fechaVisto,5,2).'/'.substr($alu->fechaVisto,0,4).' - '.substr($alu->fechaVisto,11,5).'hs';
                }
                $fechaConfirmacion = 'No Corresponde';
                if($alu->fechaConfirmacion != null and $confirm == 1)
                {
                    $fechaConfirmacion = substr($alu->fechaConfirmacion,8,2).'/'.substr($alu->fechaConfirmacion,5,2).'/'.substr($alu->fechaConfirmacion,0,4);
                }
                /*$aceptado = 'No Corresponde';
                if($alu->aceptado == 1 and $confirm == 1)
                {
                    $aceptado = 'SI';
                }
                $me_gusta_alu = $alu->me_gusta;
                /*if($alu->me_gusta == 0)
                {
                    $me_gusta_alu = 0;
                }*/
                
                $tutores = $this->modelo_evento->get_tutores_alumno_eventosusers($id_evento,$alu->alu_user_id);
                
                $cont = 0;
                $tutores_array = array();
                foreach ($tutores as $tutor)
                {
                    $cont = 1;
                    $fechaConfirmacion = 'No Corresponde';
                    if($tutor->fechaConfirmacion != null and $confirm == 1)
                    {
                        $fechaConfirmacion = substr($tutor->fechaConfirmacion,8,2).'/'.substr($tutor->fechaConfirmacion,5,2).'/'.substr($tutor->fechaConfirmacion,0,4);
                    }
                    $aceptado = $tutor->aceptado;
                    /*if($tutor->aceptado == 1 and $confirm == 1)
                    {
                        $aceptado = 'SI';
                    }*/
                    $fecha_visto = 'No Leido';
                    if($tutor->fechaVisto != null)
                    {
                        $fecha_visto = substr($tutor->fechaVisto,8,2).'/'.substr($tutor->fechaVisto,5,2).'/'.substr($tutor->fechaVisto,0,4).' - '.substr($tutor->fechaVisto,11,5).'hs';
                    }
                    $me_gusta = $tutor->me_gusta;
                   /* if($tutor->me_gusta == 0)
                    {
                        $me_gusta = 0;
                    }*/
                    if($tutor->foto != '')
                    {
                        $foto_tutor = $this->utilidades->get_url_img()."fotos_usuarios/".$tutor->foto;
                    }
                    else
                    {
                                    
                        $foto_tutor = $this->utilidades->get_url_img()."fotos_usuarios/default-avatar.png";
                    }
                    $tutor =  array(

                                    'nombre' => $tutor->last_name.", ".$tutor->first_name, 
                                    'fecha_visto' => $fecha_visto, 
                                    'fechaConfirmacion' => $fechaConfirmacion, 
                                    'aceptado' => $tutor->aceptado, 
                                    'me_gusta' => $me_gusta, 
                                    'foto' => $foto_tutor
                                    );

                    $tutores_array[] = $tutor;

                }
                $mensaje_tutores = '';
                if($cont == 0)
                {
                    $this->load->model("modelo_tutor");
                    $tutores_verificar = $this->modelo_tutor->obtener_tutores_x_alumno($alu->alu_user_id);
                    if($tutores_verificar == false)
                    {
                        $mensaje_tutores = "No Tiene Tutores Asignados";
                    }
                    else
                    {
                        $mensaje_tutores = "El comunicado no fue enviado a sus Tutores";
                    }

                }
                
                if($alu->foto != '')
                {
                    $foto_alu = $this->utilidades->get_url_img()."fotos_usuarios/".$alu->foto;
                }
                else
                {
                                
                    $foto_alu = $this->utilidades->get_url_img()."fotos_usuarios/default-avatar.png";
                }
                $alumno =  array(

                                    'nombre' => $alu->last_name.", ".$alu->first_name, 
                                    'foto' => $foto_alu,
                                    'fecha_visto' => $fecha_visto, 
                                    'fechaConfirmacion' => $fechaConfirmacion, 
                                    'aceptado' => $alu->aceptado, 
                                    'me_gusta' => $alu->me_gusta, 
                                    'contador_tutores' => $cont,
                                    'mensaje_tutores' => $mensaje_tutores,
                                    'tutores' => $tutores_array,
                                    'mensaje_alumnos' => $mensaje_alumnos,
                                    'eventos_para_alumnos' => 1,
                                                );

                $alumnos_array[] = $alumno;
                
                
            }
            if($eventos_para_alumnos == 0)
            {
                //die();
                $mensaje_alumnos = "Este Comunicado no fue Enviado a los Alumnos";
                $alumnos = $this->modelo_evento->get_alumnos_eventosusers_x_tutores($division_id,$id_evento);
               // echo "aa";
                foreach ($alumnos as $alu )
                {
                    $confirm = $alu->confirm;
                    
                    $tutores = $this->modelo_evento->get_tutores_alumno_eventosusers($id_evento,$alu->alu_user_id);
                    
                    $cont = 0;
                    $tutores_array = array();
                    foreach ($tutores as $tutor)
                    {
                        $cont = 1;
                        $fechaConfirmacion = 'No Corresponde';
                        if($tutor->fechaConfirmacion != null and $confirm == 1)
                        {
                            $fechaConfirmacion = substr($tutor->fechaConfirmacion,8,2).'/'.substr($tutor->fechaConfirmacion,5,2).'/'.substr($tutor->fechaConfirmacion,0,4);
                        }
                        $aceptado = 'No Corresponde';
                        /*if($tutor->aceptado == 1 and $confirm == 1)
                        {
                            $aceptado = 'SI';
                        }*/
                        $fecha_visto = 'No Leido';
                        if($tutor->fechaVisto != null)
                        {
                            $fecha_visto = substr($tutor->fechaVisto,8,2).'/'.substr($tutor->fechaVisto,5,2).'/'.substr($tutor->fechaVisto,0,4).' - '.substr($tutor->fechaVisto,11,5).'hs';
                        }
                        $me_gusta = $tutor->me_gusta;
                       /* if($tutor->me_gusta == 0)
                        {
                            $me_gusta = ' --- ';
                        }*/
                        if($tutor->foto != '')
                        {
                            $foto_tutor = $this->utilidades->get_url_img()."fotos_usuarios/".$tutor->foto;
                        }
                        else
                        {
                                        
                            $foto_tutor = $this->utilidades->get_url_img()."fotos_usuarios/default-avatar.png";
                        }
                        $tutor =  array(

                                    'nombre' => $tutor->last_name.", ".$tutor->first_name, 
                                    'fecha_visto' => $fecha_visto, 
                                    'fechaConfirmacion' => $fechaConfirmacion, 
                                    'aceptado' => $tutor->aceptado, 
                                    'me_gusta' => $me_gusta, 
                                    'foto' => $foto_tutor,
                                    );

                        $tutores_array[] = $tutor;
                    }
                    $mensaje_tutores = '';
                    if($cont == 0)
                    {
                       
                        $mensaje_tutores = "No Tiene Tutores Asignados";
                    }
                    if($alu->foto != '')
                    {
                        $foto_alu = $this->utilidades->get_url_img()."fotos_usuarios/".$alu->foto;
                    }
                    else
                    {
                                    
                        $foto_alu = $this->utilidades->get_url_img()."fotos_usuarios/default-avatar.png";
                    }
                    $alumno =  array(
                                    'nombre' => $alu->last_name.", ".$alu->first_name, 
                                    'foto' => $foto_alu,
                                    'contador_tutores' => $cont,
                                    'mensaje_tutores' => $mensaje_tutores,
                                    'tutores' => $tutores_array,
                                    'mensaje_alumnos' => $mensaje_alumnos,
                                    'eventos_para_alumnos' => 0,
                                                );

                    $alumnos_array[] = $alumno;
                }
            }
            $text = $text.'';
            $text = $text.'';
            $message = [

                            'satus' => TRUE,
                            'division_id' => $division_id,
                            'alumnos' => $alumnos_array,
                            'eventos_para_alumnos' => $eventos_para_alumnos,
                            'id_colegio' => $idcolegio,
                            'id_evento' => $id_evento,
                            'confirm' => $info_evento->confirm,
                        ];

                $this->set_response($message, REST_Controller::HTTP_OK);
        }

    }
    public function generar_contadores_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_OK);//HTTP_BAD_REQUEST
        }
        else
        {
            $sql_eventos = "SELECT * FROM `eventos` where YEAR(fechaCreacion) = 2019 and id > 5595";
            $sql_eventos = $this->db->query($sql_eventos);
            foreach ($sql_eventos->result() as $evento) 
            {
                $sql_eventosusers = "SELECT COUNT(id)  as total_eventos_user FROM `eventosusers` WHERE eventos_id = ".$evento->id;
                $sql_eventosusers = $this->db->query($sql_eventosusers);
                $sql_eventosusers = $sql_eventosusers->row();
                //print_r($sql_eventosusers);
                $total_eventos_user = $sql_eventosusers->total_eventos_user;

                $sql_update = "UPDATE eventos SET cont_eventosusers= $total_eventos_user WHERE id = ".$evento->id;
                $sql_update = $this->db->query($sql_update);

               /* //contador de vistos
                $sql_eventosusers = "SELECT  COUNT(id) as vistos FROM `eventosusers` WHERE visto= 1 and eventos_id = ".$evento->id;
                $sql_eventosusers = $this->db->query($sql_eventosusers);
                $sql_eventosusers = $sql_eventosusers->row();
                //print_r($sql_eventosusers);
                $vistos = $sql_eventosusers->vistos;

                $sql_update = "UPDATE eventos SET cont_vistos= $vistos WHERE id = ".$evento->id;
                $sql_update = $this->db->query($sql_update);

                //contador de me gusta
                $sql_eventosusers = "SELECT  COUNT(id) as me_gusta FROM `eventosusers` WHERE me_gusta= 1 and eventos_id = ".$evento->id;
                $sql_eventosusers = $this->db->query($sql_eventosusers);
                $sql_eventosusers = $sql_eventosusers->row();
                //print_r($sql_eventosusers);
                $me_gusta = $sql_eventosusers->me_gusta;

                $sql_update = "UPDATE eventos SET cont_me_gusta= $me_gusta WHERE id = ".$evento->id;
                $sql_update = $this->db->query($sql_update);*/

                /*if($evento->confirm == 1)
                {
                    //contador de me gusta
                    $sql_eventosusers = "SELECT  COUNT(id) as aceptados FROM `eventosusers` WHERE aceptado= 1 and eventos_id = ".$evento->id;
                    $sql_eventosusers = $this->db->query($sql_eventosusers);
                    $sql_eventosusers = $sql_eventosusers->row();
                    //print_r($sql_eventosusers);
                    $aceptados = $sql_eventosusers->aceptados;

                    $sql_update = "UPDATE eventos SET cont_confirmados= $aceptados WHERE id = ".$evento->id;
                    $sql_update = $this->db->query($sql_update);

                    $sql_eventosusers = "SELECT  COUNT(id) as negados FROM `eventosusers` WHERE aceptado= -1 and eventos_id = ".$evento->id;
                    $sql_eventosusers = $this->db->query($sql_eventosusers);
                    $sql_eventosusers = $sql_eventosusers->row();
                    //print_r($sql_eventosusers);
                    $negados = $sql_eventosusers->negados;

                    $sql_update = "UPDATE eventos SET cont_negados= $negados WHERE id = ".$evento->id;
                    $sql_update = $this->db->query($sql_update);
                }*/

            }

                $message = [

                            'satus' => TRUE,
                            'finish' => 'okkk'
                        ];
                $this->set_response($message, REST_Controller::HTTP_OK);
        }

    }


    /*RE ENVIAR*/
    public function reenviar_post()  //ANDRES, duplica un evento con un nuevo filtro 
    //(puede coincidir algunos destinatarios; tratar esos casos)
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
        $pin = $_POST['pin'];
        $existe = $this->utilidades->verifica_pin($pin);
        if ($existe == 1)
        {
            //print_r("OK re");  sleep(5); die();

            $data = json_decode($_POST['data']) ; //datos del evento
            $filtro = $_POST['filtro']; //datos del los destinos
            $idcolegio = $_POST['idcolegio'];
            $idgrupo = $_POST['idgrupo'];
            $idsfilesOld = $_POST['idsfilesOld'];
            $idsfilesNew = $_POST['idsfilesNew'];
            $parentColeId = $_POST['eventColeId'];


            $parametros = json_decode($_POST['parametros']);
            $parametros2 = json_decode($_POST['parametros']);

            if(isset($parametros->usuariostodos)){
                unset($parametros2->usuariostodos);
                $parametrosJson = json_encode($parametros2);
            }
            else
                $parametrosJson = $_POST['parametros'];


            $data->users_id = $userid;
            $data->group_id = $idgrupo;
            $data->colegio_id = $idcolegio;


            $reglas = $this->modelo_permisos->get_reglas($idcolegio,$idgrupo);
            $standby = 0;
            if($reglas)
                if($reglas->supervisado != null){
                    $standby = 1;
                    $data->standby = 1;
                }


            // datetime mysql no admite valor vacio ''
            if($data->fechaInicio == '')
                $data->fechaInicio=null;
            if($data->fechafin == '')
                $data->fechafin=null;

            $result = $this->modelo_evento->insertar_evento($data);

            $eventParentId = $data->reenviadoId;

            if ($result)
            {
                $ultimoId=$this->db->insert_id();

                //**** FILES ******

                //asignar los archivos del evento padre, asigna referencia.
                $event_files = $this->modelo_upload->get_eventFiles($eventParentId, $idsfilesOld);
                if($event_files){
                    foreach ($event_files as $rowfile) {                   
                        $newFile = array(
                            'evento_id' => $ultimoId,
                            'file_id' => $rowfile->file_id,                            
                        );
                        $this->modelo_upload->insert_event_file($newFile);
                    }
                }

                if($idsfilesNew!='')
                {
                    $this->load->model("modelo_upload");
                    $this->modelo_upload->set_eventoId($ultimoId, $idsfilesNew);
                }

                $this->load->helper('email');
                $urlapi = $this->utilidades->get_url_api();
                //$fichero = $urlapi."application/controllers/api/v1/log_eventos.txt";
                //$fichero2 = $urlapi."application/controllers/api/v1/log_emails.txt";


                //inserta filtro.............................................................
                switch($filtro)
                {
                    case 't'://todos

                        $datafiltro = array ('evento_id'=>$ultimoId, 'tipo'=>'t', 'colegio'=>$idcolegio);
                        $this->modelo_evento->insertar_filtroevento($datafiltro);

                        $destinos['alumnos'] = 0;
                        $destinos['tutores'] = 0;

                        $usuarios = $this->modelo_usuario->get_usuariosParaEnviar3(0, $idcolegio);

                        $arre=array();
                        foreach ($usuarios as $row )
                        {
                           $arre[]=$row->user_id;
                        }

                        $destinos['todos'] = join(',',$arre);

                        break;

                    case 'd'://dirigido a un/s usuario/s especifico/s

                        if ($parametrosJson) {
                            $datafiltro = array ('evento_id'=>$ultimoId, 'tipo'=>"$filtro", 'filtro'=>$parametrosJson, 'alumno_tutor'=>$parametros->ambos, 'colegio'=>$idcolegio);
                            $this->modelo_evento->insertar_filtroevento($datafiltro);
                        }


                        $destinos = $this->traer_destinosDiri($parametros, $idcolegio);

                        if( (isset($destinos)) && (count($destinos['todos']) > 0)  ) //me aseguro que no venga vacio
                        {
                             $usuarios1 = $this->modelo_usuario->get_usuariosParaEnviar($destinos['todos'], 0, $idcolegio);

                             $usuarios2 = $this->modelo_usuario->get_usuariosParaEnviar2($destinos['tutores'], $destinos['alumnos'], $idcolegio);

                            if( ($usuarios1) && ($usuarios2) )
                                $usuarios = array_merge($usuarios1, $usuarios2);
                            else  if($usuarios1)
                                        $usuarios = $usuarios1;
                                    else $usuarios = $usuarios2;
                        }

                        break;

                    case 'p'://personalizado (nivel, año, division)

                        if ($parametrosJson) {
                            $datafiltro = array ('evento_id'=>$ultimoId, 'tipo'=>"$filtro", 'filtro'=>$parametrosJson, 'alumno_tutor'=>$parametros->ambos, 'colegio'=>$idcolegio);
                            $this->modelo_evento->insertar_filtroevento($datafiltro);
                        }


                        $destinos = $this->traer_destinos($parametros, $idcolegio);
                        //estos ids son de users (no user_group)

                        if( (isset($destinos)) && (count($destinos['todos']) > 0)  ) //me aseguro que no venga vacio
                        {
                             $usuarios1 = $this->modelo_usuario->get_usuariosParaEnviar($destinos['todos'], 0, $idcolegio);

                             $usuarios2 = $this->modelo_usuario->get_usuariosParaEnviar2($destinos['tutores'], $destinos['alumnos'], $idcolegio);

                            if( ($usuarios1) && ($usuarios2) )
                                $usuarios = array_merge($usuarios1, $usuarios2);
                            else  if($usuarios1)
                                        $usuarios = $usuarios1;
                                    else $usuarios = $usuarios2;
                        }


                        break;

                    case 'a'://dirigido a usuarios relacionados con areas

                        if ($parametrosJson) {
                            $datafiltro = array ('evento_id'=>$ultimoId, 'tipo'=>"$filtro", 'filtro'=>$parametrosJson, 'alumno_tutor'=>$parametros->ambos, 'colegio'=>$idcolegio);
                            $this->modelo_evento->insertar_filtroevento($datafiltro);
                        }


                        $destinos = $this->traer_destinosDiri($parametros, $idcolegio);

                        if( (isset($destinos)) && (count($destinos['todos']) > 0)  ) //me aseguro que no venga vacio
                        {
                             $usuarios1 = $this->modelo_usuario->get_usuariosParaEnviar($destinos['todos'], 0, $idcolegio);

                             $usuarios2 = $this->modelo_usuario->get_usuariosParaEnviar2($destinos['tutores'], $destinos['alumnos'], $idcolegio);

                            if( ($usuarios1) && ($usuarios2) )
                                $usuarios = array_merge($usuarios1, $usuarios2);
                            else  if($usuarios1)
                                        $usuarios = $usuarios1;
                                    else $usuarios = $usuarios2;
                        }

                        break;

                }//fin swich

                $contenido = "\n\n\nIdColegio:".$idcolegio."  IdEvento:".$ultimoId."\n";
                $contenido .= "destinosTodos:".$destinos['todos']."\n";
                $contenido .= "destinosAlumnos:".$destinos['alumnos']."\n";
                $contenido .= "destinosTutores:".$destinos['tutores']."\n";
                // usando la bandera FILE_APPEND para añadir el contenido al final del fichero
                // y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
                //29-10-2019 08hs - Franco: Comentada la siguiente linea que bloquea el archivo de logs por cada comunicado q debe enviar
                //file_put_contents($fichero, $contenido, FILE_APPEND | LOCK_EX);



                if($idcolegio == $parentColeId){ //si es el mismo colegio, verificar que no duplique notificaciones.
                    
                    $eventosUsersParent = $this->modelo_evento->obtener_eventosuser_ByIdevento_delete($data->reenviadoId);
                    
                    //DUDA: No se si es mejor verificar en cada insert, o hacer este doble for.
                    if($eventosUsersParent)
                    {
                        if($usuarios)
                        {   
                            $auxUsuarios = array();

                            foreach ($usuarios as $rowU)
                            {
                                $encuen = 0;
                                foreach ($eventosUsersParent as $rowUParent) //MEJORAR BUSQUEDA
                                {  
                                    if($rowU->alumno_id != null)
                                        $destina = $rowU->alumno_id; //guardo el hijo
                                    else
                                        $destina = $rowU->user_id;

                                    if( ($rowU->user_id == $rowUParent->users_id) && ($destina == $rowUParent->destinatario) ){
                                        $encuen = 1;
                                        break; //corta a penas encuentra. idem a while
                                    }
                                   
                                }

                                if($encuen == 0)
                                    $auxUsuarios[] = $rowU;

                            }
                            
                            $usuarios = $auxUsuarios;
                        }
                    }
                    
                }
               

                if($standby == 0)
                        $this->enviar_eventosusers($usuarios, $ultimoId, $data, $idcolegio, $userid, $fichero, $fichero2); //inserta y envia
                    else
                        $this->noenviar_eventosusers($usuarios, $ultimoId, $data, $idcolegio, $userid, "no_auto"); //inserta y el envio queda en espera. no autorizado.



                $creador = $this->modelo_usuario->get_usuariosEspecificos($userid);
                foreach ($creador as $row )
                {
                    $fechaA=date('Y-m-d H:i:s');
                    $this->response([
                            'status' => TRUE,
                            'message' => 'Evento ReEnviado',
                            'id' => $ultimoId,
                            'fechaalta'=>$fechaA,
                            'creadorname'=> $row->first_name.' '.$row->last_name,
                        ], REST_Controller::HTTP_OK);
                }
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Error al guardar'
                ]); // NOT_FOUND (404) being the HTTP response code
            }

        }else  $this->response([
                'status' => FALSE,
                'message' => 'pininvalido'
                ], REST_Controller::HTTP_OK);

        }
    }
    public function favorito_post()  //y ella?
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            $eventousers_id = $this->input->post("eventousers_id");
          //  $token = $this->input->post("token");
            // Obtengo y proceso el arreglo de eventos y consulto que al menos tenga 1 elemento el arreglo
            if ($eventousers_id)
            {
                $favorito = $this->input->post("favorito");
                if(isset($favorito)){
                    if($favorito == 1)
                    {
                        $this->modelo_evento->marcar_favorito($eventousers_id);
                        //si existe token es porque viene del movil e inserto en
                        //eventosuser_detalle
                        /*if(isset($token)){
                            $data["eventosusers_id"] = $eventousers_id;
                            //obtengo el id del token
                            $push_result = $this->modelo_usuario->existe_registerid_gcm($userid, $token);
                            foreach ($push_result as $key => $value) {
                                $token_id = $value->id;
                                $data["token_id"] = $token_id;
                                $this->modelo_evento->marcar_megusta_detalle($data);
                            }
                        }*/
                    }
                    else{
                        $this->modelo_evento->desmarcar_favorito($eventousers_id);
                        //si existe token es porque viene del movil e inserto en
                        //eventosuser_detalle
                        /*if(isset($token)){
                            $data["eventosusers_id"] = $eventousers_id;
                            //obtengo el id del token
                            $push_result = $this->modelo_usuario->existe_registerid_gcm($userid, $token);
                            foreach ($push_result as $key => $value) {
                                $token_id = $value->id;
                                $data["token_id"] = $token_id;
                                $this->modelo_evento->desmarcar_megusta_detalle($data);
                            }
                        }*/
                    }
                    $this->response([
                        'status' => TRUE,
                        'last_query' =>  $this->db->last_query()
                    ], REST_Controller::HTTP_OK);
                }
                else{
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Faltan datos para terminar la operación'
                    ], REST_Controller::HTTP_BAD_REQUEST);
                }
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Debe enviar un evento como mínimo'
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }
    public function notificar_post()  //y ella?
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            $eventousers_id = $this->input->post("eventousers_id");
          //  $token = $this->input->post("token");
            // Obtengo y proceso el arreglo de eventos y consulto que al menos tenga 1 elemento el arreglo
            if ($eventousers_id)
            {
                $this->modelo_evento->set_notificado($eventousers_id);
                 $this->response([
                        'status' => TRUE,
                        'last_query' =>  $this->db->last_query()
                    ], REST_Controller::HTTP_OK);
                
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Debe enviar un evento como mínimo'
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }
    
    //Para envio de evento masivo
     public function batch_evento_post()  
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $pin = $_POST['pin'];
            $existe = $this->utilidades->verifica_pin($pin);
            if ($existe == 1)
            {
                $data = json_decode($_POST['data']) ; //datos del evento
                $filtro = $_POST['filtro']; //datos del los destinos
                $idcolegio = $_POST['idcolegio'];
                $idgrupo = $_POST['idgrupo'];
                $idsfiles = $_POST['idsfiles'];
                $parametros = json_decode($_POST['parametros']);
                $parametros2 = json_decode($_POST['parametros']);
                if(isset($parametros->usuariostodos)){
                    unset($parametros2->usuariostodos);
                    $parametrosJson = json_encode($parametros2);
                }
                else
                    $parametrosJson = $_POST['parametros'];
                $data->users_id = $userid;
                $data->group_id = $idgrupo;
                $data->colegio_id = $idcolegio;
                $reglas = $this->modelo_permisos->get_reglas($idcolegio,$idgrupo);
                $standby = 0;
                if($reglas)
                    if($reglas->supervisado != null){
                        $standby = 1;
                        $data->standby = 1;
                    }
                $result = $this->modelo_evento->insertar_evento($data);
                if ($result)
                {
                    $ultimoId=$this->db->insert_id();
                    if($idsfiles!='')
                    {
                        $this->load->model("modelo_upload");
                        $this->modelo_upload->set_eventoId($ultimoId, $idsfiles);
                    }
                    $this->load->helper('email');
                    $urlapi = $this->utilidades->get_url_api();
                    //$fichero = $urlapi."application/controllers/api/v1/log_eventos.txt";
                    //$fichero2 = $urlapi."application/controllers/api/v1/log_emails.txt";
                    switch($filtro)
                    {
                        case 't'://todos

                            $datafiltro = array('evento_id'=>$ultimoId, 'tipo'=>'t', 'colegio'=>$idcolegio);
                            $this->modelo_evento->insertar_filtroevento($datafiltro);
                            $destinos['alumnos'] = 0;
                            $destinos['tutores'] = 0;
                            $usuarios = $this->modelo_usuario->get_usuariosParaEnviar3(0, $idcolegio);
                            $arre=array();
                            foreach ($usuarios as $row )
                            {
                            $arre[]=$row->user_id;
                            }
                            $destinos['todos'] = join(',',$arre);
                        break;
                        case 'd'://dirigido a un/s usuario/s especifico/s
                            if ($parametrosJson) {
                                $datafiltro = array ('evento_id'=>$ultimoId, 'tipo'=>"$filtro", 'filtro'=>$parametrosJson, 'alumno_tutor'=>$parametros->ambos, 'colegio'=>$idcolegio);
                                $this->modelo_evento->insertar_filtroevento($datafiltro);
                            }
                            $destinos = $this->traer_destinosDiri($parametros, $idcolegio);
                            if( (isset($destinos)) && (count($destinos['todos']) > 0)  ) //me aseguro que no venga vacio
                            {
                                $usuarios1 = $this->modelo_usuario->get_usuariosParaEnviar($destinos['todos'], 0, $idcolegio);
                                $usuarios2 = $this->modelo_usuario->get_usuariosParaEnviar2($destinos['tutores'], $destinos['alumnos'], $idcolegio);
                                if( ($usuarios1) && ($usuarios2) )
                                    $usuarios = array_merge($usuarios1, $usuarios2);
                                else  if($usuarios1)
                                            $usuarios = $usuarios1;
                                        else $usuarios = $usuarios2;
                            }
                        break;
                        case 'p'://personalizado (nivel, año, division)
                            if ($parametrosJson) {
                                $datafiltro = array ('evento_id'=>$ultimoId, 'tipo'=>"$filtro", 'filtro'=>$parametrosJson, 'alumno_tutor'=>$parametros->ambos, 'colegio'=>$idcolegio);
                                $this->modelo_evento->insertar_filtroevento($datafiltro);
                            }
                            $destinos = $this->traer_destinos($parametros, $idcolegio);
                            //estos ids son de users (no user_group)
                            if( (isset($destinos)) && (count($destinos['todos']) > 0)  ) //me aseguro que no venga vacio
                            {
                                $usuarios1 = $this->modelo_usuario->get_usuariosParaEnviar($destinos['todos'], 0, $idcolegio);
                                $usuarios2 = $this->modelo_usuario->get_usuariosParaEnviar2($destinos['tutores'], $destinos['alumnos'], $idcolegio);
                                if( ($usuarios1) && ($usuarios2) )
                                    $usuarios = array_merge($usuarios1, $usuarios2);
                                else  if($usuarios1)
                                            $usuarios = $usuarios1;
                                        else $usuarios = $usuarios2;
                            }
                        break;
                        case 'a'://dirigido a usuarios relacionados con areas
                            if ($parametrosJson) {
                                $datafiltro = array ('evento_id'=>$ultimoId, 'tipo'=>"$filtro", 'filtro'=>$parametrosJson, 'alumno_tutor'=>$parametros->ambos, 'colegio'=>$idcolegio);
                                $this->modelo_evento->insertar_filtroevento($datafiltro);
                            }
                            $destinos = $this->traer_destinosDiri($parametros, $idcolegio);
                            if( (isset($destinos)) && (count($destinos['todos']) > 0)  ) //me aseguro que no venga vacio
                            {
                                $usuarios1 = $this->modelo_usuario->get_usuariosParaEnviar($destinos['todos'], 0, $idcolegio);
                                $usuarios2 = $this->modelo_usuario->get_usuariosParaEnviar2($destinos['tutores'], $destinos['alumnos'], $idcolegio);
                                if( ($usuarios1) && ($usuarios2) )
                                    $usuarios = array_merge($usuarios1, $usuarios2);
                                else  if($usuarios1)
                                            $usuarios = $usuarios1;
                                        else $usuarios = $usuarios2;
                            }
                        break;
                    }//fin swich
                    $contenido = "\n\n\nIdColegio:".$idcolegio."  IdEvento:".$ultimoId."\n";
                    $contenido .= "destinosTodos:".$destinos['todos']."\n";
                    $contenido .= "destinosAlumnos:".$destinos['alumnos']."\n";
                    $contenido .= "destinosTutores:".$destinos['tutores']."\n";

                    if($standby == 0)
                        $this->batch_enviar_eventosusers($usuarios, $ultimoId, $data, $idcolegio, $userid, $fichero, $fichero2); //inserta y envia
                    else
                        $this->noenviar_eventosusers($usuarios, $ultimoId, $data, $idcolegio, $userid, "no_auto"); //inserta y el envio queda en espera. no autorizado.

                    //generar contador de eventos enviados
                    $sql_eventosusers = "SELECT COUNT(id)  as total_eventos_user FROM `eventosusers` WHERE eventos_id = ".$ultimoId;
                    $sql_eventosusers = $this->db->query($sql_eventosusers);
                    $sql_eventosusers = $sql_eventosusers->row();
                    $total_eventos_user = $sql_eventosusers->total_eventos_user;
                    $sql_update = "UPDATE eventos SET cont_eventosusers= $total_eventos_user WHERE id = ".$ultimoId;
                    $sql_update = $this->db->query($sql_update);
                    $creador = $this->modelo_usuario->get_usuariosEspecificos($userid);
                    foreach ($creador as $row )
                    {
                        $fechaA=date('Y-m-d H:i:s');
                        $this->response([
                                'status' => TRUE,
                                'message' => 'Evento guardado',
                                'id' => $ultimoId,
                                'fechaalta'=>$fechaA,
                                'creadorname'=> $row->first_name.' '.$row->last_name,
                            ], REST_Controller::HTTP_OK);
                    }
                }
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Error al guardar'
                    ]); // NOT_FOUND (404) being the HTTP response code
                }
            }
            else 
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'pininvalido'
                    ], REST_Controller::HTTP_OK);
            }
        }
    }
    
    public function batch_enviar_eventosusers($usuarios, $ultimoId, $data, $idcolegio, $userid, $fichero, $fichero2){
    $usuariosEmail = array();
    $datacolegio=$this->modelo_colegio->get_colegio($idcolegio); //colegio
    $eu_email = array();
    $eu_movil = array();
    $detallesPush = array();
    $auxdetallesPush = array(); //Guarda todos los tokens de los usuarios a los q se envia el msj
    $arr_pushMobile = array(); //para enviar a los moviles
    $arr_gcm = array();         //para moviles viejos
    
    ignore_user_abort(true);
    set_time_limit(0);
    ob_start();
    if($usuarios)
    {
        foreach ($usuarios as $row )
        {
            $enviar=0;
            if($row->name == 'Tutor')
            {
                if($row->alumno_id != null)
                    $enviar = $row->alumno_id; //guardo el hijo
                else
                    $enviar = $row->user_id;
            }
            else //preceptor, docente, alumno
            {
                $enviar = $row->user_id; //guardo el mismo usuario
            }
            $arr1 = array( 'eventos_id'=>$ultimoId, 'users_id'=>$row->user_id, 'destinatario'=>$enviar, 'medio'=>'x', 'alta'=>'a');
            $datos[] = $arr1;
            $token_gcm = $this->modelo_usuario->get_push($enviar);
            if($token_gcm)
            {
                $row->tieneMovil = 1;
                foreach ($token_gcm as $key => $value) {
                    $auxdetallesPush[]  = array('evento_id'=>$ultimoId,'user_id'=> $enviar ,'enviado'=> 0 , 'motivo'=> 'Pendiente','token_id'=> $value->id, 'alta'=>'a');
                    if($value->source == 'react'){
                         $arr_pushMobile[]   = array('token'=> $value->token,'colegio' =>$datacolegio->row()->nombre, 'title' => $data->titulo,'eventos_id'=>$ultimoId,'user_id'=> $enviar ,'enviado'=> 0 , 'motivo'=> 'Pendiente','token_id'=> $value->id, 'alta'=>'a','destinatario'=>$enviar,'email' => $row->email,'active'=>$row->active,'pago'=>$row->pago,'recibir_notificacion'=>$row->recibir_notificacion);
                    }
                    else{
                        $arr_gcm[] =  array('token'=> $value->token,'colegio' =>$datacolegio->row()->nombre, 'title' => $data->titulo,'eventos_id'=>$ultimoId,'user_id'=> $enviar ,'enviado'=> 0 , 'motivo'=> 'Pendiente','token_id'=> $value->id, 'alta'=>'a','destinatario'=>$enviar,'email' => $row->email,'active'=>$row->active,'pago'=>$row->pago,'recibir_notificacion'=>$row->recibir_notificacion);
                    }
                }
            }
            else{
                $row->tieneMovil = 0;
            }
        }
        //Inserto todos los destinatarios a modo de "pendientes de actualizacion/notificacion"
        if((is_array($datos)) && (count($datos)>0))
        {
            $this->modelo_evento->insertar_eventousers2($datos);
        } 
        if((is_array($auxdetallesPush)) && (count($auxdetallesPush)>0))
        {
            $this->modelo_evento->insertar_eventousers_detalle2($auxdetallesPush);
        }
    }
    //Aqui ya deberia enviar respuesta al usuario de que "los mensajes se están enviando.."
    $creador = $this->modelo_usuario->get_usuariosEspecificos($userid);
    foreach ($creador as $row )
    {
        $fechaA=date('Y-m-d H:i:s');
        $continuar = 1; //continua procesando luego de enviar la respuesta al cliente
        $this->response([
                'status' => TRUE,
                'message' => 'Evento guardado, enviando mensajes...',
                'id' => $ultimoId,
                'fechaalta'=>$fechaA,
                'creadorname'=> $row->first_name.' '.$row->last_name,
            ], REST_Controller::HTTP_OK,
            $continuar);
    }
    header('Connection: close');
    header('Content-Length: '.ob_get_length());
    ob_end_flush();
    ob_flush();
    flush();
    session_write_close();
    //fastcgi_finish_request(); ---- Para apache con Server API fast CGI
     if($usuarios)
    {
        //Revisar usuarios a los q no se les envio por movil..
        foreach ($usuarios as $key => $value) {
            if(($value->tieneMovil == 0) && ($value->active==1)  && ($value->recibir_notificacion == 1)  && ($value->pago==1))
            {
                $eu_email[]= array( 'eventos_id'=>$ultimoId, 'users_id'=>$value->user_id, 'destinatario'=>$value->user_id, 'enviado'=>0,'motivo'=>'not_in_push','token_id'=> null, 'medio'=>'x', 'alta'=>'a');
                if( valid_email( $value->email) ){
                    $usuariosEmail[] = $value->email;
                }
            }
        }
        
        if(count($eu_email)>0){
            $this->modelo_evento->update_eventosusers_batch($eu_email);
        }
       //result retorna en un array unificado la respuesta si se enviaron o no los mensajes por movil
        $result = $this->enviar_notificacion_usuarios_array($arr_pushMobile,$arr_gcm); //envia e inserta.
        if((is_array($result)) && (count($result)>0))
        {
            foreach ($result[0] as $key => $value) {
                if( ($value['enviado'] == 0 ) && ( $value['error']== ""  ) )
                {
                    if( valid_email( $value['email']) ){
                        $usuariosEmail[] = $value['email'];
                        $motivo=null;
                    }
                    else{
                        $motivo='email_invalid';
                    }
                    $eu_email[]= array( 'token_id' => null , 'eventos_id'=>$ultimoId, 'users_id'=>$value['user_id'], 'destinatario'=>$value['destinatario'], 'enviado'=>0,'motivo'=>$motivo, 'medio'=>'e', 'alta'=>'a');
                }
            }
            if(count($eu_email)>0){
                $this->modelo_evento->update_eventosusers_batch($eu_email);
            }
            if( (isset($usuariosEmail)) && (count($usuariosEmail)>0) ) //enviar por GMAIL google
            {
                $destinos = $usuariosEmail;
                $from1 = "contacto@smtp.e-nodos.com";
                $from2 = $datacolegio->row()->nombre;
                $asunto="Nuevo Evento: ".$data->titulo;
                $datos['remitente'] = $this->modelo_usuario->get_usuariosEspecificos($userid);
                $datos['titulo']= $data->titulo;
                $datos['fechaInicio']= $data->fechaInicio;
                $datos['descripcion']= $data->descripcion;
                $datos['lugar']= $data->lugar;
                $datos['colegio_nombre']= $datacolegio->row()->nombre;
                $datos['data']= $data;
                $mensaje = $this->load->view('evento/mail_evento', $datos,true);
                $enviado = $this->usuarios->enviar_email($destinos, $mensaje, $from1, $from2, $asunto);
                //print_r($enviado);
                if($enviado['enviado']){
                    $valores=array('enviado'=>1);
                    //actualiza todos menos los que tiene email invalido
                    $this->modelo_evento->update_enviado_eventousers($ultimoId,'e','a',$valores);
                }
                else {
                        $contenido2 = "\n\n\n INSERT IdColegio:".$idcolegio."  IdEvento:".$ultimoId."\n";
                        $contenido2 .= "Error:".$enviado['error']."\n";
                        file_put_contents($fichero2, $contenido2, FILE_APPEND | LOCK_EX);
                }
                $contenido = "Emeils:".implode(';', $destinos);
            }//fin del "if hay emails"
       }//fin del "if is array result"
        
    }//fin del if "usuarios"
     
   }
   
    private function enviar_notificacion_usuarios_array($array_datos_push,$array_datos_gcm){
        $pushStatus = array();
        $pushStatus2 = array();
        $res_pushStatus = array();
        
        if(count($array_datos_push)>0){//Envio via "expo"
            $pushStatus[] = $this->usuarios->batch_enviar_notificacion_expo($array_datos_push);
        }

        if(count($array_datos_gcm)>0){//Envio via "GCM"
            $pushStatus2[] = $this->usuarios->batch_enviar_notificacion($array_datos_gcm);
        }
        $res_pushStatus = array_unique(array_merge($pushStatus,$pushStatus2), SORT_REGULAR);
       
        $updateEvUs = array();
        $updateEvUsDet = array();
        if(is_array($res_pushStatus))
        {
            if(count($res_pushStatus)>0)
            {
                foreach ($res_pushStatus[0] as $key => $value) {
                    $status_id = '';
                   if(isset($value['status_id']))
                   {
                    $status_id = $value['status_id'];
                   }
                    
                    $updateEvUs[] = array(
                        'eventos_id' =>     $value[ 'eventos_id'],
                        'users_id'=>        $value[ 'user_id'],
                        'destinatario'=>    $value[ 'destinatario'],
                        'medio'=>           'm',
                        'alta'=>            $value[ 'alta'],
                        'motivo'=>          $value[ 'error'],
                        'token_id'=>        $value[ 'token_id'],
                        'enviado'=>         $value[ 'enviado']

                    );
                    $updateEvUsDet[] = array(
                        'evento_id' =>       $value['eventos_id'],
                        'alta'=>             $value['alta'],
                        'token_id'=>         $value['token_id'],
                        'user_id'=>          $value['user_id'],
                        'enviado'=>          $value['enviado'],
                        'motivo'=>           $value['error'],
                        'status_id'=>        $status_id
                    );
                }
            }
        } 

        if(count($updateEvUs)>0){
            $this->modelo_evento->update_eventosusers_batch($updateEvUs);
        }
        if(count($updateEvUsDet)>0)
        {
            $this->modelo_evento->update_eventosusers_detalle_batch($updateEvUsDet);
        }
        //los retornos que vienen con error debo tratarlos aqui

        return $res_pushStatus;
    }

}
