<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_upload extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }

        public function insert_file($data)
        {
            $query = $this->db->insert("files",$data);         
            return $this->db->insert_id();
        }

        public function update_file($idfile, $datos)
        {
            $this->db->where('id', $idfile);
            $query = $this->db->update('files', $datos);                   
            return $query;
            
        }

        public function insert_event_file($data)
        {
            $find = $this->get_eventFiles($data['evento_id'], $data['file_id']);

            $query = false;

            //verifica que no exista
            if($find == 0)
                $query = $this->db->insert("event_file",$data);         

            return $query;
        }

        public function set_eventoId($idevento, $idsfiles)
        {
            $files = explode(",",$idsfiles);

            for ($i=0; $i < count($files); $i++) { 
                $data = array(
                    'evento_id' => $idevento,
                    'file_id' => $files[$i]                     
                );
                $query = $this->insert_event_file($data); 
            }
                        
            return $query;
        }

        public function get_file($idfile)
        {
            $sql = "SELECT * FROM files WHERE id = $idfile LIMIT 1";
            
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->row();
            }
            else{
                return 0;
            }
        }

        public function get_files_evento($idevento)
        {
            $sql = "SELECT files.*
                    FROM event_file 
                    JOIN files ON files.id = event_file.file_id
                    WHERE event_file.evento_id = $idevento";
            
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return 0;
            }
        }

        public function get_eventFiles($idevento, $ids)
        {
            $sql = "SELECT * FROM event_file WHERE evento_id = $idevento AND file_id IN ($ids)";
            
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return 0;
            }
        }

        public function get_eventFiles_ByFileId($idfile)
        {
            $sql = "SELECT * FROM event_file WHERE file_id = $idfile";
            
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return 0;
            }
        }
        
        public function delete_file($idfile)
        {
            $sql = "DELETE FROM files WHERE id = $idfile";
            $query = $this->db->query($sql);
            return $query;
        }

        public function delete_event_file($idevent, $idfile)
        {
            $sql = "DELETE FROM event_file WHERE evento_id = $idevent AND file_id = $idfile";
            $query = $this->db->query($sql);
            return $query;
        }


        public function obtener_files_x_tipo($idevento, $tipo) 
        {
            $sql = "SELECT files.* 
                    FROM event_file 
                    JOIN files ON files.id = event_file.file_id
                    WHERE event_file.evento_id = $idevento 
                    AND files.tipo like '%$tipo%'";
            $query = $this->db->query($sql);
            return $query->result();
        }



        public function update_foto_usu($ruta,$id)
        {
            $this->db->where("id",$id);
            $this->db->update("users",$ruta);
        }
       

	}