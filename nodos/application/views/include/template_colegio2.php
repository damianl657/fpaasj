<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	
	<title>Nodos Comunicaci&oacute;n escolar m&aacute;s simple</title>
	<meta name="description" content="App para una comunicaci&oacute;n escolar m&aacute;s simple" />
	<meta name="author" content="sietedefebrero.com">
	<!-- Favicon -->
	
	<link href="<?= base_url('assets/images/favicon.ico') ?>" rel="shortcut icon" type="image/x-icon" />
	
	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic|Abel' rel='stylesheet' type='text/css'>
	<!-- Font Awesome -->
	<link href="<?=base_url('tpl/css/bootstrap.min.css');?>" rel="stylesheet">
	<link href="<?=base_url('tpl/font-awesome/css/font-awesome.css');?>" rel="stylesheet">	
    
    
    <script src="<?=base_url('assets/js/jquery-2.1.4.min.js')?>" type="text/javascript"></script>
    <script src="<?=base_url('assets/js/jquery-ui.js');?>"></script>
	<script src="<?=base_url('tpl/js/bootstrap.min.js');?>"></script>
    <script src="<?=base_url('assets/js/bootbox.min.js');?>"></script> 




	
		  <!-- barra para seleccionar -->
		<script src="<?=base_url('assets/js/jquery-ui-slider-pips.js');?>"></script> 
		<link href="<?=base_url('assets/css/jquery-ui-slider-pips.css');?>" rel="stylesheet">
        
          <!-- JS NODOS -->
		<script src="<?=base_url('assets/js/main.js');?>"></script> 
		<script src="<?=base_url('assets/js/jquery-latest.min.js');?>"></script> 
		<script src="<?=base_url('assets/js/respond.min.js');?>"></script> 
		 

	    <!-- Mainly scripts -->
        <script src="<?=base_url('tpl/js/plugins/metisMenu/jquery.metisMenu.js');?>"></script>
        <script src="<?=base_url('tpl/js/plugins/slimscroll/jquery.slimscroll.min.js');?>"></script>
        
      		     
        <!-- Data Tables CSS -->
		<!--<link href="<?//=base_url('tpl/css/plugins/dataTables/dataTables.bootstrap.css');?>" rel="stylesheet">
		<link href="<?//=base_url('tpl/css/plugins/dataTables/dataTables.tableTools.min.css');?>" rel="stylesheet">
		<link href="<?//=base_url('tpl/css/plugins/dataTables/dataTables.responsive.css');?>" rel="stylesheet">-->
         <!-- Datatables  DAMIAN NUEVOS 02/07/2018-->
        <link rel="stylesheet" href="<?=base_url('assets/datatables.net-bs/css/dataTables.bootstrap.min.css')?>">
        <link rel="stylesheet" href="<?=base_url('assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')?>">
        <link rel="stylesheet" href="<?=base_url('assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')?>">
        <link rel="stylesheet" href="<?=base_url('assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')?>">
        <link rel="stylesheet" href="<?=base_url('assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')?>">
        <!-- EDITOR DE TEXTO DAMIAN-->
        <link rel="stylesheet" href="<?=base_url('assets/css/summernote/summernote.css" rel="stylesheet')?>">
        <link rel="stylesheet" href="<?=base_url('assets/css/summernote/summernote-bs3.css" rel="stylesheet')?>">
      
       
        <!--Toastr-->
		<link href="<?=base_url('tpl/css/plugins/toastr/toastr.min.css');?>" rel="stylesheet">
        <script src="<?=base_url('tpl/js/plugins/toastr/toastr.min.js');?>"></script>
       
		<link href="<?=base_url('tpl/css/animate.css');?>" rel="stylesheet">
		<!-- todo el estilo de la plantilla -->
        <link href="<?=base_url('tpl/css/style.css');?>" rel="stylesheet">	
		
		<!-- nodos css --> 
		<link href="<?=base_url('assets/css/jquery-ui.min.css');?>" rel="stylesheet">
		<link href="<?=base_url('assets/css/jquery-ui.css');?>" rel="stylesheet">
        <script src="<?=base_url('assets/js/jquery-ui.js');?>"></script>

		<link href="<?=base_url('assets/css/general_tutores.css');?>" rel="stylesheet">
		<!--Stilos Nodos estilos eze -->
		<style type="text/css" media="screen">
            .datepicker {
                z-index:2051 !important;
            }   
        </style>

        <!--FULL CALENDAR-->
         <!--<link href="<?=base_url('assets/css/fullcalendar/fullcalendar.css');?>" rel="stylesheet">-->
        <link href="<?=base_url('assets/css/fullcalendar/fullcalendar.print.css');?>" rel="stylesheet"  media='print'>
        <script src="<?=base_url('assets/js/fullcalendar/moment.min.js');?>"></script>
        <script src="<?=base_url('assets/js/scrollspy/scrollspy.js');?>"></script>
        <!--<script src="<?=base_url('assets/js/fullcalendar/fullcalendar.min.js');?>"></script>
        <script src="<?=base_url('assets/js/fullcalendar/lang/es.js');?>"></script> -->
        <script type="text/javascript" src="<?=base_url('tpl/js/plugins/ckeditor/ckeditor.js')?>"></script>
        <link rel="stylesheet" href="<?php echo base_url('tpl/js/plugins/fullcalendar/fullcalendar.min.css')?>" media="screen">
        <script src="<?php echo base_url('tpl/js/plugins/fullcalendar/fullcalendar.min.js')?>"></script>
        <!--<script src="<?php echo base_url('tpl/js/plugins/moment/moment-with-locales.min.js')?>"></script> -->
        <script src="<?php echo base_url('tpl/js/plugins/fullcalendar/lang/es.js')?>"></script> 


        <!--Notify-->
        <link href="<?=base_url('assets/css/bootstrap-notify.css');?>" rel="stylesheet">
        <!--<script src="<?//=base_url('assets/js/bootstrap-notify.js');?>"></script> -->
        <script src="<?=base_url('assets/js/notify.js');?>"></script>



         <!-- UPLOAD -->
        <!--<link href="<?//=base_url('tpl/css/plugins/upload/jquery.filer.css');?>" type="text/css" rel="stylesheet" />
        <link href="<?//=base_url('tpl/css/plugins/upload/themes/jquery.filer-dragdropbox-theme.css');?>" type="text/css" rel="stylesheet" />
        <script src="<?//=base_url('tpl/js/plugins/upload/jquery.filer.min.js');?>"></script>-->
        
		<!-- Mainly scripts -->
        <script src="<?=base_url('tpl/js/plugins/metisMenu/jquery.metisMenu.js');?>"></script>
        <script src="<?=base_url('tpl/js/plugins/slimscroll/jquery.slimscroll.min.js');?>"></script>
       
    
        

         <!--SELECT2 v3 (v4 no funca)-->
        <script src="<?=base_url('tpl/js/plugins/select2/select2-3.5.4/select2.js');?>"></script>
        <link rel="stylesheet" href="<?=base_url('tpl/js/plugins/select2/select2-3.5.4/select2.css');?>" type="text/css" />
        <link href="<?=base_url('tpl/css/select2-bootstrap.css');?>" rel="stylesheet">

        <!--SELECT2 v4 
        <script src="<?=base_url('tpl/js/plugins/select2/select2-4.0.2/js/select2.full.js');?>"></script>
        <link rel="stylesheet" href="<?=base_url('tpl/js/plugins/select2/select2-4.0.2/css/select2.css');?>" type="text/css" />-->

       
       <!-- DataTime Master Picker -->
        <script src="<?=base_url('tpl/js/plugins/datatimepicker/jquery.datetimepicker.full.js');?>"></script>
         <link href="<?=base_url('tpl/js/plugins/datatimepicker/jquery.datetimepicker.css');?>" rel="stylesheet">



        <!--slider pips-->
        <link href="<?=base_url('assets/css/jquery-ui-slider-pips.css');?>" rel="stylesheet">
        <!--<link href="<?=base_url('assets/css/jquery.ptTimeSelect.css');?>" rel="stylesheet">-->


        <script src="<?=base_url('tpl/js/jsb/segment.min.js');?>"></script>
        <script src="<?=base_url('tpl/js/jsb/ease.min.js');?>"></script>
    
    
        <script src="<?=base_url('assets/js/jquery.validate.min.js')?>"></script>
        <script src="<?=base_url('assets/js/push.min.js')?>"></script>


        <script type="text/javascript">
            jQuery(document).ready(function($) {
                
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "progressBar": true,
                    "preventDuplicates": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "showDuration": "400",
                    "hideDuration": "1000",
                    "timeOut": "7000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };        
                <?php 
                    $mensaje_exito = $this->session->flashdata("mensaje_exito");
                    if ((isset($mensaje_exito)) && (!empty($mensaje_exito)))
                    {
                ?>
                        toastr.success("<?php echo $mensaje_exito; ?>","¡Perfecto!");
                <?php 
                    } 
                ?>
                <?php 
                    $mensaje_error = $this->session->flashdata("mensaje_error");
                    if ((isset($mensaje_error)) && (!empty($mensaje_error)))
                    {
                ?>
                        toastr.error("<?php echo $mensaje_error; ?>","¡Atencion!");
                <?php 
                    } 
                ?>
            });
        </script>
		<?php //$this->load->view("include/particicles");
  //$this->load->view("include/navbar_tutores");?>
  <?php $this->load->helper('html');
            $this->load->helper('url');
            echo link_tag(base_url('assets/css/burguer.css'));
            //echo"holla";
            echo link_tag(base_url('assets/css/webapp.css'));
        ?>

</head>

<body>	
    <div id="wrapper">
        <?php //$this->load->view("include/particicles");
		 $this->load->view("include/vista_inc_menu_colegio"); 
		 $this->load->view("include/particicles");
		 ?>
        <?php
        if(!empty($contenido))
                            $this->load->view($contenido);
        
        /*if(!empty($contenido2))
                            $this->load->view($contenido2);*/
        ?>
    	
		
	    <footer>
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center">
	                     <?php 
	                        date_default_timezone_set('America/Argentina/San_Juan');?>
	                         
	                           <center><img src=<?=base_url("assets/images/i/icon-user.png")?> width="40" alt="NODOS"/><p>&copy; <? echo date("Y"); ?></p></center>
	        </div>
	    </footer>
    </div><!-- wrapper -->
    <!-- Mainly scripts -->
     <script src="<?=base_url('tpl/js/plugins/metisMenu/jquery.metisMenu.js');?>"></script>
     <script src="<?=base_url('tpl/js/plugins/slimscroll/jquery.slimscroll.min.js');?>"></script>
     
     <script src="<?=base_url('assets/js/jeditable/jquery.jeditable.js');?>"></script>
  
    <!-- Data Tables -->
     <!--<script src="<?//=base_url('tpl/js/plugins/dataTables/jquery.dataTables.js');?>"></script>
     <script src="<?//=base_url('tpl/js/plugins/dataTables/dataTables.bootstrap.js');?>"></script>
     <script src="<?//=base_url('tpl/js/plugins/dataTables/dataTables.responsive.js');?>"></script>     
     <script src="<?//=base_url('tpl/js/plugins/dataTables/dataTables.tableTools.min.js');?>"></script>-->
    <!-- Datatables 
        <script src="<?=base_url('assets/datatables.net/js/jquery.dataTables.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-buttons/js/dataTables.buttons.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-buttons/js/buttons.flash.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-buttons/js/buttons.html5.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-buttons/js/buttons.print.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-keytable/js/dataTables.keyTable.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-responsive/js/dataTables.responsive.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-responsive-bs/js/responsive.bootstrap.js')?>"></script>
        <script src="<?//=base_url('assets/datatables.net-scroller/js/dataTables.scroller.min.js')?>"></script>
        <script src="<?=base_url('assets/jszip/dist/jszip.min.js')?>"></script>
        <script src="<?=base_url('assets/pdfmake/build/pdfmake.min.js')?>"></script>
        <script src="<?=base_url('assets/pdfmake/build/vfs_fonts.js')?>"></script>
  
      <!-- Custom and plugin javascript -->
    <script src="<?=base_url('tpl/js/inspinia.js');?>"></script>  <!-- usa -->
	
    <script src="<?=base_url('tpl/js/plugins/pace/pace.min.js');?>"></script>

    <?php 
     if ((isset($carga_select_dt)) && ($carga_select_dt == true)) { ?>
    <link href="<?php echo base_url('tpl/css/plugins/dataTables/select.dataTables.min.css') ?>" rel="stylesheet"> 
    
   <!-- <script src="<?=base_url('tpl/js/plugins/dataTables/dataTables.select.min.js');?>"></script>-->
   
    <?php } ?>

    
    <script src="<?php echo base_url('tpl/js/general.js');?>"></script>

    <!-- EDITOR DE TEXTO DAMIAN
    <!-- SUMMERNOTE -->
    <script src="<?//=base_url('assets/js/summernote/summernote.min.js')?>"></script>
   

    <script>
        $(document).ready(function(){

          /*  
            $('.summernote').summernote({
              height: "500px"
  
            });*/
           /* var postForm = function() {
    var content = $('textarea[name="content"]').html($('#summernote').code());
}*/
          /*  $('.note-codable').click(function(event)
             {
              //  alert('a');
                
                $('#cke_1_top').remove();
            });*/

       });

        
        
    </script>

    <!--Para mostrar los mensajes notify 
    <div class='notifications top-right' style="margin-top: 40px"> </div>-->
    
    <style>
        .notifyjs-corner{
            margin-top:53px;
        }
    </style>
    
  </body>
</html>