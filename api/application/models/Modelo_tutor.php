<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_tutor extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }

        public function obtener_tutores()
        {
            $sql = "
                SELECT *
                FROM users_viejo
                LEFT JOIN roles
                ON users_viejo.role_id = roles.role_id
                WHERE roles.role_name = 'Tutores'
                ORDER BY users_viejo.username ASC
            ";
            $query = $this->db->query($sql);
            return $query->result();
        }

        public function obtener_tutor($id_tutor)
        {
            $sql = "
                SELECT *
                FROM users_viejo
                LEFT JOIN roles
                ON users_viejo.role_id = roles.role_id
                WHERE (roles.role_name = 'Tutores') AND (users_viejo.id = $id_tutor)
            ";
            $query = $this->db->query($sql);
            return $query->row();
        }
        
        public function obtener_tutores_x_alumno($id_alumno){
            /*$sql = "SELECT tutor_id, alumno_id
                    FROM tutoralumno JOIN users  ON  tutoralumno.alumno_id= user.id
                    AND tutoralumno.alumno_id IN ($cadena_alumnos)"; */   
            $sql = "SELECT users.* 
                    FROM tutoralumno JOIN users  ON  tutoralumno.tutor_id= users.id
                    AND tutoralumno.alumno_id = $id_alumno";
                
            $query = $this->db->query($sql);           
            if($query->num_rows()>0){
                /*foreach($query->result() as $row_alumno ){
                    $sql_tutor = "SELECT first_name, last_name, id
                                  FROM  users  WHERE id= $row_alumno->id"; 
                    $query_tutor = $this->db->query($sql_tutor);                     
                    //return $query_tutor->result();
                }*/
                return $query->result();
            }
            else{
                return false;
            }       
        }
        

        public function obtener_alumnos_del_tutor($id)
        {
            $sql = "
                SELECT users.id as alumno_id, users.first_name as nombre_alumno, users.last_name as apellido_alumno,divisiones.id as id_div, divisiones.nombre as nombre_division, anios.nombre as anio, nodoanios.id as id_nodoanio, nodocolegio.nombre as nombre_colegio, nodocolegio.id as id_colegio, sexo, username, documento,email
                FROM tutoralumno
                JOIN users ON tutoralumno.alumno_id  = users.id
                JOIN users_groups on users_groups.user_id = users.id
                
                JOIN inscripciones on inscripciones.alumno_id = users_groups.id
                JOIN divisiones on divisiones.id = inscripciones.divisiones_id
                JOIN nodoanios on nodoanios.id = divisiones.anios_id
                JOIN anios on anios.id = nodoanios.anio_id
                JOIN nodonivel ON nodonivel.id = nodoanios.nodoniv_id
                JOIN nodocolegio ON nodocolegio.id = nodonivel.nodocolegio_id
                JOIN planesestudio ON planesestudio.nombre = nodocolegio.planestudio

                WHERE tutoralumno.tutor_id  = $id
                AND inscripciones.cicloa = nodocolegio.cicloa
                AND divisiones.planesestudio_id = planesestudio.id";

            $query = $this->db->query($sql);
            return $query->result();
        }

        /*public function get_hijos_de_tutores($id_rol)//dado el rol obtener hijos de tutores
        {
            $sql = "SELECT users.id, alumnos.first_name AS nombre_alumno, alumnos.last_name AS apellido_alumno, anios.nombre AS anio, divisiones.nombre AS division, nodocolegio.nombre
                    FROM users
                    JOIN users_groups ON users_groups.user_id = users.id
                    JOIN tutoralumno ON tutoralumno.tutor_id = users.id
                    JOIN users AS alumnos ON alumnos.id = tutoralumno.alumno_id
                    JOIN users_groups AS alumnos_groups ON alumnos_groups.user_id = alumnos.id
                    JOIN inscripciones ON inscripciones.alumno_id = alumnos_groups.id
                    JOIN divisiones ON divisiones.id = inscripciones.divisiones_id
                    JOIN nodoanios ON nodoanios.id = divisiones.anios_id
                    JOIN anios ON anios.id = nodoanios.anio_id
                    join nodocolegio on nodocolegio.id = users_groups.colegio_id
                    join planesestudio on planesestudio.nombre = nodocolegio.planestudio
                    WHERE users_groups.group_id = 5
                    and inscripciones.planesestudio_id = planesestudio.id
                    and alumnos_groups.colegio_id = users_groups.colegio_id"; //AND users.active=1
                    $query = $this->db->query($sql);
            
                if($query->num_rows()>0){
                    return $query->result();
                }
                else{
                    return false;
                }
        }*/
        public function get_hijos_de_tutor($id_tutor, $id_colegio)
        {
            /*$sql = "SELECT users.id, alumnos.first_name AS nombre_alumno, alumnos.last_name AS apellido_alumno, anios.nombre AS anio, divisiones.nombre AS division, nodocolegio.nombre
                    FROM users
                    JOIN users_groups ON users_groups.user_id = users.id
                    JOIN tutoralumno ON tutoralumno.tutor_id = users.id
                    JOIN users AS alumnos ON alumnos.id = tutoralumno.alumno_id
                    JOIN users_groups AS alumnos_groups ON alumnos_groups.user_id = alumnos.id
                    JOIN inscripciones ON inscripciones.alumno_id = alumnos_groups.id
                    JOIN divisiones ON divisiones.id = inscripciones.divisiones_id
                    JOIN nodoanios ON nodoanios.id = divisiones.anios_id
                    JOIN anios ON anios.id = nodoanios.anio_id
                    JOIN nodocolegio on nodocolegio.id = alumnos_groups.colegio_id
                    JOIN planesestudio on planesestudio.nombre = nodocolegio.planestudio
                    
                    WHERE nodocolegio.id = $id_colegio
                    AND users.id = $id_tutor
                    AND inscripciones.cicloa = nodocolegio.cicloa
                    AND divisiones.planesestudio_id = planesestudio.id"; //AND users.active=1*/

            $sql = "
                SELECT users.id as alumno_id, users.first_name as nombre_alumno, users.last_name as apellido_alumno,divisiones.id as id_div, divisiones.nombre as division, anios.nombre as anio, nodoanios.id as id_nodoanio, nodocolegio.nombre as nombre_colegio, sexo, username, documento,email
                FROM tutoralumno
                JOIN users ON tutoralumno.alumno_id  = users.id
                JOIN users_groups on users_groups.user_id = users.id
                
                JOIN inscripciones on inscripciones.alumno_id = users_groups.id
                JOIN divisiones on divisiones.id = inscripciones.divisiones_id
                JOIN nodoanios on nodoanios.id = divisiones.anios_id
                JOIN anios on anios.id = nodoanios.anio_id
                JOIN nodonivel ON nodonivel.id = nodoanios.nodoniv_id
                JOIN nodocolegio ON nodocolegio.id = nodonivel.nodocolegio_id
                JOIN planesestudio ON planesestudio.nombre = nodocolegio.planestudio

                WHERE tutoralumno.tutor_id  = $id_tutor
                AND nodocolegio.id = $id_colegio
                AND inscripciones.cicloa = nodocolegio.cicloa
                AND divisiones.planesestudio_id = planesestudio.id";
                    
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }
        public function get_tutores_de_un_alumno($id_alumno)
        {
            $sql ="SELECT users.id, users.last_name, users.first_name, users.documento, users.email 
                    FROM users
                    JOIN tutoralumno ON tutoralumno.tutor_id = users.id
                    WHERE tutoralumno.alumno_id = $id_alumno";
            $query = $this->db->query($sql);
            if($query->num_rows()>0){
                    return $query->result();
                }
                else{
                    return false;
                }
        }
        public function is_tutor_en_colegio($id_user, $id_colegio)
        {
            // $sql = "SELECT * FROM `users`
            // JOIN users_groups on users_groups.user_id = users.id
            // JOIN groups on groups.id = users_groups.group_id
            // where groups.name = 'Tutor'
            // and users_groups.colegio_id = $id_colegio
            // and users.id = $id_user
            //         ";
            $sql = "CALL is_tutor_en_colegio($id_user, $id_colegio)";
            $query = $this->db->query($sql);           
            $resultado = $query;  
            mysqli_next_result( $this->db->conn_id );         
            if( $resultado->num_rows()>0){
                return 1;
            }
            else{
                return 0;
            }
        }
	}