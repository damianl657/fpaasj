<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';

    class Evento extends REST_Controller 
    {

    function __construct()
    {
        //inserto cabeceras para que no me de error cors
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        parent::__construct();
        $this->load->model("modelo_evento");
        $this->load->model("modelo_usuario");
        $this->load->model("modelo_alumno");   
        $this->load->model("modelo_colegio");      
        $this->load->model("modelo_anio");
        $this->load->model("modelo_nivel");
        $this->load->model("modelo_division");
    }


    public function eventos_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $limit = $this->get("limit");
            if(isset($limit)){
                $limit = str_replace("-", ",", $limit);
            }
            else{
                $limit = '0,5';
            }
            // Obtengo todos los eventos
            $eventos = $this->modelo_evento->obtener_eventos($userid, $limit); // obtiene los eventos de el usuario X
            // Si existe mas de un resultado, lo mando
            if ($eventos)
            {
                //print_r($eventos);
                //$eventos["query"] = $this->db->last_query();
                //$eventos = $this->db->last_query();
                $this->response($eventos, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron eventos',
                    'query' => $this->db->last_query()
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }

    public function mas_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            //$limit = $this->get("limit");
            $limit = '0,5';
            if(isset($limit)){
            }
            else{
                $limit = false;
            }
            $eventos = 0;
            $lastId = $this->get("lastId");
            if(isset($lastId)){
                $eventos = $this->modelo_evento->cargar_mas_eventos((int)$this->get("lastId"),$userid);
            }            
            else{
                $firstId = $this->get("firstId");
                if(isset($firstId))
                    $eventos = $this->modelo_evento->cargar_mas_eventos_nuevos((int)$firstId,$userid);
            }            
            // Si existe mas de un resultado, lo mando
            if ($eventos != 0 && $eventos)
            {
                //$eventos["query"] = $this->db->last_query();
                $this->response($eventos, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {   //$eventos["query"] = $this->db->last_query();
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron eventos',
                 //   'query' => $this->db->last_query()
                    'lastId'=>$lastId,
                     'firstId'=>$firstId,
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code*/
            }
        }
    }   

    public function eventosmesanio_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $mes = $this->get("mes");
            $anio = $this->get("anio");
            if  ( 
                    (isset($mes)) && (!empty($mes)) && ($mes >= 1) && ($mes <= 12) &&
                    (isset($anio)) && (!empty($anio)) && ($anio >= 2015) && ($anio <= 2100)
                )
            {
                $eventos = $this->modelo_evento->obtener_eventos_mesanio($mes, $anio, $userid); // obtiene los eventos de el usuario X
                // Si existe mas de un resultado, lo mando
                if ($eventos)
                {
                    $this->response($eventos, REST_Controller::HTTP_OK);
                }
                // Sino, envio respuesta con 404
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'No se encontraron eventos'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                }
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Debe proveer los parametros de mes y anio'
                ], REST_Controller::HTTP_BAD_REQUEST);
            }            
        }
    }

    function eventofechaseleccionada_get(){
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            $fecha = $this->get('fecha');
            if  ($fecha === NULL)
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Debe proveer una fecha'
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
            else
            {
                $eventos = $this->modelo_evento->obtener_eventos_fecha($fecha, $userid); // obtiene los eventos de el usuario X
                // Si existe mas de un resultado, lo mando
                if ($eventos)
                {
                    $this->response($eventos, REST_Controller::HTTP_OK);
                }
                // Sino, envio respuesta con 404
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'No se encontraron eventos'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                }
            }
        }       
    }

    public function evento_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $idevento = $this->get('id'); // obtengo el id del evento, si es que vino en la url
            // Obtengo un evento especifico
            $evento = $this->modelo_evento->obtener_evento($idevento); // obtiene el evento especifico
            // Si existe mas de un resultado, lo mando
               
            
            if ($evento)
            {
                //agregar campo si es propietario o no. 1 o 0
                if($evento->userid==$userid)
                    $evento->owner = 1;
                else 
                    $evento->owner = 0;
                $this->response($evento, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {   //echo $idevento;
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontro el evento con ese ID',
                    'id' => $idevento 
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
		}
    }
    public function evento_users_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $id_evento = $this->get('id_evento'); // obtengo el id del evento, si es que vino en la url
            // Obtengo un evento especifico
            $evento = $this->modelo_evento->obtener_evento_users($id_evento, $userid); // obtiene el evento especifico
            // Si existe mas de un resultado, lo mando
            if ($evento)
            {
                $this->response($evento, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'no era para mi el evento',
                    'consulta' =>$this->db->last_query()
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }
    
    public function obtener_destinatario_filtro_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {   
            $respuesta="Perzonalizado\Varios";
            $idevento = $this->get('id');
            $lista_filtro = $this->modelo_evento->obtener_filtro($idevento);
            $ciclo = 0;
            foreach ($lista_filtro as $dato)
            {
                $ciclo = $ciclo + 1;
            }
            if($ciclo == 1)
            {
                $arreglo_id = explode(",", $lista_filtro[0]->valores);
                $ciclo_valor = 0;
                foreach ($arreglo_id as $valor)
                {
                    $ciclo_valor = $ciclo_valor + 1;
                }
                if($ciclo_valor == 1)
                {
                    if($lista_filtro[0]->filtro == "nivel")
                    {
                        $aux=$this->modelo_nivel->obtener_nivel_id($arreglo_id[0]);
                        $respuesta = $aux->nombre;
                    }
                    if($lista_filtro[0]->filtro == "division")
                    {
                        $aux=$this->modelo_division->obtener_division_id($arreglo_id[0]);
                        $respuesta= $aux->nombanio.' '.$aux->nombdiv;
                    } 
                    if($lista_filtro[0]->filtro == "rol")
                    {
                        $aux=$this->modelo_usuario->get_rol_id($arreglo_id[0]);
                        $respuesta= $aux->name;
                        if($aux->id == 3)
                        {
                            $respuesta = "Preceptores";
                        }
                        if($aux->name == 4)
                        {
                            $respuesta = "Docentes";
                        }
                        if($aux->name == 5)
                        {
                            $respuesta = "Tutores";
                        }
                        if($aux->name == 6)
                        {
                            $respuesta = "Alumnos";
                        }
                        if($aux->name == 7)
                        {
                            $respuesta = "Administrativos";
                        }
                        if($aux->name == 8)
                        {
                            $respuesta = "Directivos";
                        }
                    }
                    if($lista_filtro[0]->filtro == "usuario")
                    {
                        $aux=$this->modelo_usuario->get_users_id($arreglo_id[0]);
                        $respuesta= $aux->last_name.' '.$aux->first_name;
                    }
                }
                else
                {
                    $respuesta="Perzonalizado\Varios";
                } 
            }
            else
            {
                $respuesta="Perzonalizado\Varios";
            }
            $this->response($respuesta, REST_Controller::HTTP_OK);

        }
    }
    /**
     * Recibe el id de un alumno y devuelve los tutores 
    */
    public function traer_tutores($alumnosId)
    {
        if( ($alumnosId!='vacio')&& (count($alumnosId)>0) )
        {   
            $idsalumnos = join(',',$alumnosId); 
            $tutores = $this->modelo_alumno->get_tutoresXalumnos($idsalumnos);

            foreach ($tutores as $key) {
                $arreTutores[] = $key->tutor_id;
                //$tutoresAlumnos[] = array($key->tutor_id, $key->alumno_id); 
            } 
           
            $data['tutores']=$arreTutores;
            //$data['tutoresalumnos']=$tutoresAlumnos;
            return $data;
        }
        else
            return "nada";          
    }

    public function traer_destinos($parametros, $idevento, $idcolegio)  
    {
        if (count($parametros[0]) > 0)  //hay divisiones ()
        {
            //$idsDivi = join(',',$parametros[0]); 
            $divisiones = $parametros[0];  
            $idsDivi = join(',',$divisiones);

            $datafiltro = array ('evento_id'=>$idevento, 'tipo'=>'personalizado', 'filtro'=>'division', 'valores'=>$idsDivi, 'colegio'=>$idcolegio);
            $this->modelo_evento->insertar_filtroevento($datafiltro);
        }
        else
            if (count($parametros[1]) > 0)  //hay anios
            {
                $idsAnios = join(',',$parametros[1]);
                $divisiones=array();  

                $datafiltro = array ('evento_id'=>$idevento, 'tipo'=>'personalizado', 'filtro'=>'anio', 'valores'=>$idsAnios, 'colegio'=>$idcolegio);
                $this->modelo_evento->insertar_filtroevento($datafiltro);

                //$alumnos = $this->modelo_alumno->get_alumnosXanios($idsAnios);
                /*$resultdivi = $this->modelo_division->get_divisionesXanios($idsAnios);
                foreach ($resultdivi as $key)
                {
                    $divisiones[] = $key->id;
                }
                $idsDivi = join(',',$divisiones);*/

                $idsDivi = $parametros[5];  //si no selecciona division, entonces traigo todos los del select, me ahorro tener que hace la consulta comentada de arriba 
            }
            else
                if (count($parametros[2]) > 0)  //hay niveles
                {
                    $idsniveles = join(',',$parametros[2]);
                    $datafiltro = array ('evento_id'=>$idevento, 'tipo'=>'personalizado', 'filtro'=>'nivel', 'valores'=>$idsniveles, 'colegio'=>$idcolegio);
                    $this->modelo_evento->insertar_filtroevento($datafiltro);
                    $divisiones=array();
                    
                    //DEBO SEPARAR LOS NIVELES SIN ESPECIALIDAD DE LOS CON ESPECIALIDAD
                    /*$ite=0;
                    for($i=0; $i < count($parametros[2]) ;$i++)
                    {
                        //$parametros[$i] = $parametros[$i].''; //covierto a string

                        if( strpos($parametros[2][$i], '_') === false)                
                        {
                            $idnodonivel = $parametros[2][$i];
                            $result = $this->modelo_anio->get_anios_BynodonivANDnodoespNull($idnodonivel);
                        }
                        else{
                            $arre = explode("_", $parametros[2][$i]);
                            $idnodonivel = $arre[0];
                            $idnodoesp = $arre[1];
                            //print_r("**".$idnodonivel."**".$idnodoesp."**");
                            $result = $this->modelo_anio->get_anios_BynodonivANDnodoesp($idnodonivel,  $idnodoesp);

                            //print_r($result);
                        }

                        if(count($result))
                        {  
                            foreach ($result as $key) {
                                //    $idsAnios[] = $key->id;
                                if($ite==0)
                                    $idsAnios=$key->id;
                                else 
                                    $idsAnios=$idsAnios.",".$key->id;
                                $ite++;
                            }
                        }
                        else $idsAnios=-1;

                    }
                    $resultdivi = $this->modelo_division->get_divisionesXanios($idsAnios, date('Y'));
                    foreach ($resultdivi as $key)
                    {
                        $divisiones[] = $key->id;
                    }                 
                    $idsDivi = join(',',$divisiones);
                    //print_r($idsDivi); */
                    $idsDivi = $parametros[5];
                }
                else $divisiones='vacio';

         
        //print_r($idsDivi);

        $idsalu=0; $arrePrese=0; $arreDoce=0;
        if (count($parametros[3]) > 0)  //hay roles, se supone q siempre tiene que haber al menos un rol (alumno por defecto). pero por las dudas si no viene ninguno, se va al else
        {
            //print_r('hay roles-');
            $namesroles = join("','",$parametros[3]);
            $namesroles = "'".$namesroles."'"; 

            $resultroles = $this->modelo_usuario->get_rolesXrolesname($namesroles, $idcolegio);           
            $cant = count($resultroles); $i=1; $idsroles="";
            foreach ($resultroles as $fila) 
            {
                if($i == $cant)
                    $idsroles .= $fila->id;
                else $idsroles .= $fila->id.",";

                $i++;
            }
            $datafiltro = array ('evento_id'=>$idevento, 'tipo'=>'personalizado', 'filtro'=>'rol', 'valores'=>$idsroles, 'alumno_tutor'=>$parametros[4], 'colegio'=>$idcolegio);
            $this->modelo_evento->insertar_filtroevento($datafiltro);
                    
            $alumnosId = 0;
            foreach ($parametros[3] as $rol) 
            {
                $rol = trim($rol); //elimino espacios por seguridad
                switch($rol)
                {
                    case 'Alumno':  $alumnos = $this->modelo_alumno->get_alumnosXdivisiones($idsDivi);
                            $arreAlum=array();
                            if( ($alumnos)&& (isset($parametros[4])) )
                            {
                                foreach ($alumnos as $key) {
                                    $arreAlum[] = $key->user_id;
                                }
                                
                                switch ($parametros[4]) {
                                    case 'ambos':
                                        $data = $this->traer_tutores($arreAlum);
                                        $idsalu = array_merge($arreAlum, $data['tutores']);
                                        break;
                                    case 'soloalumno':
                                        $idsalu = $arreAlum;
                                        break;
                                    case 'solotutor':
                                        $data = $this->traer_tutores($arreAlum);
                                        $idsalu = $data['tutores'];
                                        break;
                                    default:
                                        # code... nunca deberia entrar aqui'
                                        $idsalu = 0;
                                        break;
                                }
                               
                                 //tutoresalumnos
                                $idsalu = join(',',$idsalu); //alumnos y tutores
                                $alumnosId = join(',',$arreAlum); //solo alumnos
                            }
                            else $idsalu = 0;
                            break;
                    case 'Preceptor':  $preceptores = $this->modelo_alumno->get_preceptoresXdivisiones($idsDivi);
                                    
                                $arrePrese=array();

                                if( $preceptores)
                                {
                                    foreach ($preceptores as $key) {
                                        $arrePrese[] = $key->user_id;
                                    }
                                    //$idspre[] = $arrePrese;
                                    $arrePrese = join(',',$arrePrese);
                                }
                                else $arrePrese = 0;
                                break;
                    case 'Docente':  $docentes = $this->modelo_alumno->get_docentesXdivisiones($idsDivi);
                                   
                                if( $docentes)
                                {
                                    $arreDoce=array();
                                    foreach ($docentes as $key) {
                                        $arreDoce[] = $key->user_id;
                                    }
                                    //$idsdoce[] = $arreDoce;
                                    $arreDoce = join(',',$arreDoce);
                                }
                                // else print_r("no entró");
                                else $arreDoce = 0;
                                break;
                }
            }
        }
        else
            {
                $alumnos = $this->modelo_alumno->get_alumnosXdivisiones($idsDivi);
                $arreAlum=array();
                if($alumnos)
                {
                    foreach ($alumnos as $key) {
                        $arreAlum[] = $key->user_id;
                    }
                    
                    $data = $this->traer_tutores($arreAlum);
                   
                    $idsalu = array_merge($arreAlum, $data['tutores']); //tutoresalumnos
                    $idsalu = join(',',$idsalu);
                }
                else $idsalu = 0;
            }


       // print_r($idsalu."--".$arrePrese."--".$arreDoce);

        $aux['ids']=$idsalu.",".$arrePrese.",".$arreDoce;

        //print_r($aux['ids']."@@".$alumnosId); 

        $data['todos']=$aux['ids'];
        $data['alumnos']=$alumnosId;

        return $data;
    }

    public function traer_destinosDiri($parametros, $idevento, $idcolegio)  
    {
        if (count($parametros[0]) > 0)  //hay usuarios y hay roles
        {
            $usuarios = $parametros[0]; 

            $idsroles = join(',',$parametros[1]);
            $datafiltro = array ('evento_id'=>$idevento, 'tipo'=>'dirigido', 'filtro'=>'rol', 'valores'=>$idsroles, 'alumno_tutor'=>$parametros[2], 'colegio'=>$idcolegio);
            $this->modelo_evento->insertar_filtroevento($datafiltro);
            //print_r($idsroles);
            
            $idsusu = join(',',$usuarios); 
            $datafiltro = array ('evento_id'=>$idevento, 'tipo'=>'dirigido', 'filtro'=>'usuario', 'valores'=>$idsusu, 'alumno_tutor'=>$parametros[2], 'colegio'=>$idcolegio);
            $this->modelo_evento->insertar_filtroevento($datafiltro);
            
             //print_r($idsusu);
        }
        else
            if (count($parametros[1]) > 0)  //hay roles (siempre hay)
            {
                $idsroles = join(',',$parametros[1]);
                $datafiltro = array ('evento_id'=>$idevento, 'tipo'=>'dirigido', 'filtro'=>'rol', 'valores'=>$idsroles, 'alumno_tutor'=>$parametros[2], 'colegio'=>$idcolegio);
                $this->modelo_evento->insertar_filtroevento($datafiltro);
                
                $usuarios=$parametros[3]; //si no selecciona usuarios, trae todos los del select usuario, o sea solo los permitidos.
                $idsusu = join(',',$usuarios); 
            }   
            else $idsusu='vacio';


        $resultAlumnos = $this->modelo_usuario->get_usuariosXrol2($idcolegio, "Alumno", $idsusu);

        $alumnos = array();
        if($resultAlumnos)  //hay alumnos
        {
            // print_r('hay_alumnos:');
            //print_r($parametros[2]);

            foreach ($resultAlumnos as $key)
            {
                $alumnos[] = $key->id;
            }

            switch ($parametros[2])
            {  
                case 'ambos':
                    $data = $this->traer_tutores($alumnos);
                    $usuarios = array_merge($usuarios, $data['tutores']);
                    $idsusu = join(',',$usuarios);
                    break;
                case 'soloalumno':
                    //$idsusu = join(',',$usuarios);
                    $idsusu=$idsusu; //mp hacer nada
                    break;
                case 'solotutor':
                    $data = $this->traer_tutores($alumnos);
                    $diferencia = array_diff($usuarios, $alumnos); //todos menos los alumnos                 
                    $usuarios = array_merge($diferencia, $data['tutores']); //agrego los tutores
                    $idsusu = join(',',$usuarios);
                    break;
                default:
                    # code... nunca deberia entrar aqui'
                    break;
            }

            $alumnosId=join(',',$alumnos);
        }
        else { //print_r('Nohay_alumnos');
            $alumnosId=0;
            $idsusu = $idsusu; 
        }


        //print_r($idsusu."@@".$alumnosId);

       // print_r($idsusu."@@".$alumnosId);


        $data['todos']=$idsusu;
        $data['alumnos']=$alumnosId;

        return $data;
    }


    public function evento_post()  //ANDRES
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
        $pin = $_POST['pin']; 
        $existe = $this->utilidades->verifica_pin($pin);
        if ($existe == 1)
        {
            $data = json_decode($_POST['data']) ; //datos del evento
            $filtro = $_POST['filtro']; //datos del los destinos
            $idcolegio = $_POST['idcolegio']; 
            $idgrupo = $_POST['idgrupo']; 
            $idsfiles = $_POST['idsfiles']; 

            $parametros = json_decode($_POST['parametros']) ;

            $data->users_id = $userid;
            $data->group_id = $idgrupo;
            $data->colegio_id = $idcolegio;

            $fichero = $_SERVER['DOCUMENT_ROOT']."/api/application/controllers/api/v1/log_eventos.txt";

            //echo $fichero;

            //print_r($data); die();

            //$fechafin = date("d/m/Y", strtotime($data->fechafin));
            //$fechainicio = date("d/m/Y", strtotime($data->fechaInicio));
            
            $result = $this->modelo_evento->insertar_evento($data);    

            if ($result)
            {
                $ultimoId=$this->db->insert_id(); 

                if($idsfiles!='')
                {    
                    $this->load->model("modelo_upload");
                    $this->modelo_upload->set_eventoId($ultimoId, $idsfiles); 
                }
                
                //filtro.............................................................                 
                switch($filtro)
                {
                    case 't'://todos  

                        $datafiltro = array ('evento_id'=>$ultimoId, 'tipo'=>'todos', 'colegio'=>$idcolegio);
                        $this->modelo_evento->insertar_filtroevento($datafiltro);

                        $usuarios = $this->modelo_usuario->get_usuariosColegio($idcolegio);
                        $arre=array();
                        foreach ($usuarios as $row ) 
                        {   
                           $arre[]=$row->user_id;
                        }
                        $destinos['todos'] = join(',',$arre);
                        $destinos['alumnos'] = 0;
                                             
                        break;
                    
                    case 'd'://dirigido a un/s usuario/s especifico/s
                        $destinos = $this->traer_destinosDiri($parametros, $ultimoId, $idcolegio);
                        break;

                    case 'p'://personalizado (nivel, año, division) 
                        $destinos = $this->traer_destinos($parametros, $ultimoId, $idcolegio);
                         //estos ids son de users (no user_group)
                        break;

                }//fin swich
                //..................................................................filtro
                
                //print_r($destinos);
                //$destinos['todos'] = array_unique($destinos['todos']); //no necesito quitar repetidos, porque en el IN no los tiene en cuenta
                

                if( (isset($destinos)) && (count($destinos['todos']) > 0)  ) //me aseguro que no venga vacio
                {
                    
                    $contenido = "\n\n\nIdColegio:".$idcolegio."  IdEvento:".$ultimoId."\n";
                    $contenido .= "destinosTodos:".$destinos['todos']."\n";
                    $contenido .= "destinosAlumnos:".$destinos['alumnos']."\n";
                    // usando la bandera FILE_APPEND para añadir el contenido al final del fichero
                    // y la bandera LOCK_EX para evitar que cualquiera escriba en el fichero al mismo tiempo
                    file_put_contents($fichero, $contenido, FILE_APPEND | LOCK_EX);


                    $usuarios = $this->modelo_usuario->get_usuariosParaEnviar($destinos['todos'], $destinos['alumnos'], $idcolegio);

                    $usuariosEmail = array();

                    if($usuarios)
                        foreach ($usuarios as $row ) 
                        { 
                            $enviar=0;
                            if($row->name == 'Tutor')
                            {
                                if($row->alumno_id != null)
                                    $enviar = $row->alumno_id; //guardo el hijo
                                else 
                                    $enviar = $row->user_id;
                            }
                            else //preceptor, docente, alumno
                            {
                                $enviar = $row->user_id; //guardo el mismo usuario
                            }

                            //enviar notificacion o mail
                            $datos_push = array("message" => $data->titulo, 
                                "userid" => $this->utilidades->encriptar($row->user_id));
                            
                            $datos = array( 'eventos_id'=>$ultimoId, 'users_id'=>$row->user_id, 'destinatario'=>$enviar, 'medio'=>'m', 'alta'=>'a' );

                            $correo = $this->enviar_notificacion_usuarios($row,$datos_push,$datos); //envia e inserta.
                                                      
                            if($correo==1) //si es 1 no tiene movil o fallo el envio, enviar por email
                            {
                                //$usuariosEmail[] = array($row->user_id, $row->email); 
                                $usuariosEmail[] = $row->email; 

                                $datos2 = array( 'eventos_id'=>$ultimoId, 'users_id'=>$row->user_id, 'destinatario'=>$enviar, 'enviado'=>0, 'medio'=>'e', 'alta'=>'a' );
                                $this->modelo_evento->insertar_eventousers($datos2); //inserto luego actualizo campo 'enviado'.
                            }
                        }

                    if( (isset($usuariosEmail)) && (count($usuariosEmail)>0) ) //enviar por GMAIL google
                    {
                        //$destinos = array_column($usuariosEmail, 1);
                        $destinos = $usuariosEmail;
                        
                        $datacolegio=$this->modelo_colegio->get_colegio($idcolegio); //colegio 

                        $from1 = "info@e-nodos.com";
                        $from2 = $datacolegio->row()->nombre;
                        

                        $asunto="Nuevo Evento: ".$data->titulo;
                        $datos['remitente'] = $this->modelo_usuario->get_usuariosEspecificos($userid);
                        $datos['titulo']= $data->titulo;
                        $datos['fechaInicio']= $data->fechaInicio;
                        $datos['descripcion']= $data->descripcion;
                        $datos['lugar']= $data->lugar;
                        $datos['colegio_nombre']= $datacolegio->row()->nombre;
                        $datos['data']= $data;
                        

                        /*foreach ($usuariosEmail as $rowE ) 
                        {                                                      
                            $datos2 = array( 'eventos_id'=>$ultimoId, 'users_id'=>$rowE[0], 'destinatario'=>$rowE[0], 'enviado'=>$enviado, 'medio'=>'e', 'alta'=>'a' );
                            $this->modelo_evento->insertar_eventousers($datos2); //destinos 
                        }*/
                        //$datos['destinatario_dirigido']= $this->modelo_usuario->get_users_id($id_destinatario);
                        //$datos['aux']=$id_destinatario;
                        $mensaje = $this->load->view('evento/mail_evento', $datos,true);
                        $enviado = $this->usuarios->enviar_email($destinos, $mensaje, $from1, $from2, $asunto);
                        //print_r($mensaje);
                        if($enviado) 
                        {
                            $valores=array('enviado'=>1 );
                            $this->modelo_evento->update_enviado_eventousers($ultimoId,'e', $valores);
                        }

                        $contenido = "Emeils:".implode(';', $destinos);
                        file_put_contents($fichero, $contenido, FILE_APPEND | LOCK_EX);

                    }
                } 

                $creador = $this->modelo_usuario->get_usuariosEspecificos($userid);
                foreach ($creador as $row ) 
                {
                    $fechaA=date('Y-m-d H:i:s');
                    $this->response([
                            'status' => TRUE,
                            'message' => 'Evento guardado',
                            'id' => $ultimoId,
                            'fechaalta'=>$fechaA,
                            'creadorname'=> $row->first_name.' '.$row->last_name,
                        ], REST_Controller::HTTP_OK);  
                }
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Error al guardar'
                ]); // NOT_FOUND (404) being the HTTP response code
            }

        }else  $this->response([
                'status' => FALSE,
                'message' => 'pininvalido'
                ], REST_Controller::HTTP_OK);  

        }
    } 


    public function update_evento_post()  //ANDRES
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $pin = $_POST['pin']; 
            $idcolegio = $_POST['idcolegio'];
            
            $existe = $this->utilidades->verifica_pin($pin);
            if ($existe == 1)
            {
                $data = json_decode($_POST['data']) ; //datos del evento
                $idevento = json_decode($_POST['idevento']) ;             
                $idsfiles = $_POST['idsfiles'];


                //print_r($data);
                $evento = $this->modelo_evento->obtener_evento($idevento);
                if($evento->userid == $userid)
                {
                    $result = $this->modelo_evento->update_evento($data, $idevento);   

                    if($idsfiles!='')
                    {    
                        $this->load->model("modelo_upload");
                        $this->modelo_upload->set_eventoId($idevento, $idsfiles); 
                    } 

                    if ($result)
                    {  
                        $eventosusers = $this->modelo_evento->obtener_eventosuser_ByIdevento_visto($idevento);  
                         
                        if($eventosusers)
                        {
                            $arreAux = array();
                            foreach ($eventosusers as $row) 
                            { 
                                $arreAux[] = $row->users_id;
                            }
                            $arreAux=join(',',$arreAux);   

                            $usuarios = $this->modelo_usuario->get_usuariosParaEnviar($arreAux, 0);
                            
                            if($usuarios)
                            {
                                foreach ($usuarios as $row ) 
                                {
                                    //enviar notificacion o mail
                                    $datos_push = array("message" => "Evento Modificado: ".$evento->titulo, "userid" => $this->utilidades->encriptar($row->user_id)); //titulo viejo o nuevo? viejo!                     

                                    $datos = array( 'eventos_id'=>$idevento, 'users_id'=>$row->user_id, 'destinatario'=>$row->user_id, 'medio'=>'m', 'alta'=>'m' );

                                    $correo = $this->enviar_notificacion_usuarios($row,$datos_push,$datos); //envia e inserta.
                                                              
                                    if($correo==1) //si es 1 no tiene movil o fallo el envio, enviar por email
                                    {
                                        //$usuariosEmail[] = array($row->user_id, $row->email); 
                                        $usuariosEmail[] = $row->email; 

                                        $datos2 = array( 'eventos_id'=>$idevento, 'users_id'=>$row->user_id, 'destinatario'=>$row->user_id, 'enviado'=>0, 'medio'=>'e', 'alta'=>'m' );
                                        $this->modelo_evento->insertar_eventousers($datos2); //inserto luego actualizo campo 'enviado'.
                                    }

                                }

                                if( (isset($usuariosEmail)) && (count($usuariosEmail)>0) ) //enviar por GMAIL google
                                {
                                    //$destinos = array_column($usuariosEmail, 1);
                                    $destinos = $usuariosEmail;
                                    
                                    $datacolegio=$this->modelo_colegio->get_colegio($idcolegio); //colegio 

                                    $from1 = "info@e-nodos.com";
                                    $from2 = $datacolegio->row()->nombre;
                                    

                                    $asunto="Evento Editado: ".$evento->titulo;
                                    $datos['remitente'] = $this->modelo_usuario->get_usuariosEspecificos($userid);
                                    $datos['titulo']= $evento->titulo;
                                    $datos['fechaInicio']= $evento->fechaInicio;
                                    $datos['descripcion']= $evento->descripcion;
                                    $datos['lugar']= $evento->lugar;
                                    $datos['colegio_nombre']= $datacolegio->row()->nombre;
                                    //$datos['data']= $evento;
                                    

                                    $mensaje = $this->load->view('evento/mail_evento', $datos,true);
                                    $enviado = $this->usuarios->enviar_email($destinos, $mensaje, $from1, $from2, $asunto);

                                    if($enviado) 
                                    {
                                        $valores=array('enviado'=>1 );
                                        $this->modelo_evento->update_enviado_eventousers($idevento,'e', $valores);
                                    }
                                }

                            }
                        }


                        $this->response([
                                'status' => TRUE,
                                'message' => 'Evento Modificado',
                            ], REST_Controller::HTTP_OK);  
                    }
                    else
                    {
                        $this->response([
                            'status' => FALSE,
                            'message' => 'Error al modificar'
                        ]); // NOT_FOUND (404) being the HTTP response code
                    }
                }
                else {
                    $this->response([
                                'status' => FALSE,
                                'message' => 'No puede modificar',
                            ], REST_Controller::HTTP_OK);  
                    }
            }
            else  {$this->response([
                'status' => FALSE,
                'message' => 'pininvalido'
                ], REST_Controller::HTTP_OK);  }
        }
    }


    public function delete_event_post()  //ANDRES
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $pin = $_POST['pin']; 
            $idevento = $_POST['idevento'];
            //$idcolegio = $_POST['idcolegio'];
            
            $existe = $this->utilidades->verifica_pin($pin);
            if ($existe == 1)
            {
                //print_r($data);
                $evento = $this->modelo_evento->obtener_evento($idevento);
                if($evento->userid == $userid)
                {
                    $result = $this->modelo_evento->delete_evento($idevento);  
                      

                    if ($result)
                    {  
                        //envio notificacion a los que tienen el visto en 1
                        $eventosusers = $this->modelo_evento->obtener_eventosuser_ByIdevento_visto($idevento);  
                        if($eventosusers)
                        {
                            $arreAux = array();
                            foreach ($eventosusers as $row) 
                            { 
                                $arreAux[] = $row->users_id;
                            }
                            $arreAux=join(',',$arreAux);   

                            $usuarios = $this->modelo_usuario->get_usuariosParaEnviar($arreAux, 0, $evento->colegioid);
                            //print_r($usuarios);
                            if($usuarios)
                            {
                                foreach ($usuarios as $row ) 
                                {
                                    
                                    //enviar notificacion o mail
                                    $datos_push = array("message" => "Evento Eliminado: ".$evento->titulo, "userid" => $this->utilidades->encriptar($row->user_id)); //titulo viejo o nuevo? viejo!                     

                                    //$datos = array( 'eventos_id'=>$idevento, 'users_id'=>$row->user_id, 'destinatario'=>$row->user_id, 'medio'=>'m', 'alta'=>'m' );

                                    $correo = $this->enviar_notificacion_usuarios2($row,$datos_push); //envia pero No inserta.
                                                              
                                    if($correo==1) //si es 1 no tiene movil o fallo el envio, enviar por email
                                    {
                                        //$usuariosEmail[] = array($row->user_id, $row->email); 
                                        $usuariosEmail[] = $row->email; 

                                        //$datos2 = array( 'eventos_id'=>$idevento, 'users_id'=>$row->user_id, 'destinatario'=>$row->user_id, 'enviado'=>0, 'medio'=>'e', 'alta'=>'m' );
                                        //$this->modelo_evento->insertar_eventousers($datos2); //inserto luego actualizo campo 'enviado'.
                                    }
                                }

                                if( (isset($usuariosEmail)) && (count($usuariosEmail)>0) ) //enviar por GMAIL google
                                {
                                    //$destinos = array_column($usuariosEmail, 1);
                                    $destinos = $usuariosEmail;
                                    
                                    $datacolegio=$this->modelo_colegio->get_colegio($evento->colegioid); //colegio 

                                    $from1 = "info@e-nodos.com";
                                    $from2 = $datacolegio->row()->nombre;
                                    

                                    $asunto="Evento Eliminado: ".$evento->titulo;
                                    $datos['remitente'] = $this->modelo_usuario->get_usuariosEspecificos($userid);
                                    $datos['titulo']= $evento->titulo;
                                    $datos['colegio_nombre']= $datacolegio->row()->nombre;
                                    //$datos['data']= $evento;
                                    

                                    $mensaje = $this->load->view('evento/mail_eventoDelete', $datos,true);
                                    //print_r($mensaje);
                                    $enviado = $this->usuarios->enviar_email($destinos, $mensaje, $from1, $from2, $asunto);

                                }

                            }
                        }

                        //elimino los registros en eventosusers_detalle
                        $eventosusers2 = $this->modelo_evento->obtener_eventosuser_ByIdevento_delete($idevento);
                        if($eventosusers2)
                        {
                            $arreAux2 = array();
                            foreach ($eventosusers2 as $row) 
                            { 
                                $arreAux2[] = $row->id;
                            }
                            $arreAux2=join(',',$arreAux2); 
                        }
                        
                        $this->modelo_evento->delete_eventouser_detalle($arreAux2); 

                        $this->modelo_evento->delete_eventouser($idevento); 
                        
                        $this->response([
                                'status' => TRUE,
                                'message' => 'Evento Eliminado',
                            ], REST_Controller::HTTP_OK);  
                    }
                    else
                    {
                        $this->response([
                            'status' => FALSE,
                            'message' => 'Error al Eliminar'
                        ]); // NOT_FOUND (404) being the HTTP response code
                    }
                }
                else {
                    $this->response([
                                'status' => FALSE,
                                'message' => 'No puede eliminar',
                            ], REST_Controller::HTTP_OK);  
                    }
            }
            else  {$this->response([
                'status' => FALSE,
                'message' => 'Pin Invalido'
                ], REST_Controller::HTTP_OK);  }
        }
    }


    /**
     * Funcion que recorre los usuarios, obtiene el token gcm y les envia una notificación.
     * A los que no tienen token gcm deberia enviarle un mail (falta)
     * @param $row_usuario datos del usuario que se debe enviar notificacion o mail
     * @param $datos_push array($message, $userid). 
     * @return
     */
    private function enviar_notificacion_usuarios($row_usuario, $datos_push, $datos)//SEBA
    {   $habilitado = 0;

        if( ($row_usuario->recibir_notificacion==1) && ($row_usuario->active==1) )
        {
            if( isset($row_usuario->pago) && isset($row_usuario->pago_alumno) )
            {
                if( ($row_usuario->name!='Tutor') && ($row_usuario->pago==1) ) 
                     $habilitado = 1;
                else 
                    if( ($row_usuario->name=='Tutor') && ($row_usuario->pago_alumno==1) )
                        $habilitado = 1;
                    else $habilitado = 0;
            }
            else $habilitado = 1;

        }
        else $habilitado = 0;
        
        if( $habilitado==1 ) 
        {
            $userid = $row_usuario->user_id;
            //Obtengo el token gcm para enviar la notificacion
            $token_gcm = $this->modelo_usuario->get_push($userid);
            if($token_gcm)
            {
                //var_dump($datos);
                //exit();
                //Guardo en eventosusers y si se envia despues hago un update
                $datos['enviado'] = 0;
                $this->modelo_evento->insertar_eventousers($datos); //insertar
                //Obtengo el id de eventosusers insertado para usarlo cuando inserte en
                //eventosusers_detalle
                $eventosusers_id = $this->db->insert_id();
                $datos['eventosusers_id'] = $eventosusers_id;
                //Recorro los token ya que el usuario puede estar en mas de un cel
                //logueado y por ende tiene un token por cada cel logueado.
                $cantTokens=count($token_gcm); //$cont=0;
                foreach ($token_gcm as $key => $value) {
                    $gcmRegIds = array($value->token);
                    $pushStatus = $this->usuarios->enviar_notificacion($gcmRegIds, $datos_push);
                    $pushStatus = json_decode($pushStatus,true);
                    $success = $pushStatus['success'];

                    //Asigno algunos valores para realizar el insert en eventosusers_detalle
                    $data['eventosusers_id'] = $datos['eventosusers_id'];
                    $data["medio"] = $datos["medio"];
                    $data["alta"] = $datos["alta"];
                    $data['token_id'] = $value->id;

                    if($success == 0)//si es 0 debo mirar el campo result
                    {
                        $cantTokens--;
                        //en error viene el tipo de error
                        $error = $pushStatus["results"][0]["error"];
                        if ($error == "InvalidRegistration")
                        {
                            $this->modelo_usuario->delete_push($userid, $value->token);                            
                            //$cont++;
                        }           
                        elseif ($error == "NotRegistered")
                        {
                            //La apicacion cliente se desinstalo del dispositivo o se desregistro de GCM.
                            //Se debe borrar del servidor
                            $this->modelo_usuario->delete_push($userid, $value->token);
                            //$cont++;
                        }
                        else { //por algun motivo x, no se envio 
                           
                            $nada = 'nada';
                                                       
                        }

                        $data['enviado'] = 0;
                        $data['motivo'] = $error ;
                        $this->modelo_evento->insertar_eventousers_detalle($data); 
                    }
                    else{
                        $data['enviado'] = 1;
                        $this->modelo_evento->insertar_eventousers_detalle($data); //insertar
                    }
                }                

                if($cantTokens==0)  //si llega a cero es porque todos los token son invalidos o no se envio a ningun movil, debo enviar por email            
                    return 1; 
                else {
                    //hacer el update en eventosusers
                    $data = array();
                    $data['enviado'] = 1;
                    $this->modelo_evento->update_eventosusers($data, $eventosusers_id);
                    return 0; //por retornar algo, lo que importa es el 1 
                }
            }
            else 
                return 1; //enviar por email
        }
        else{//sino quire recibir notificaciones, lo mismo debo insertar
            if($row_usuario->recibir_notificacion==0)
               $datos['motivo'] = 'no recibir notificaciones';
            else
                if($row_usuario->active==0)
                    $datos['motivo'] = 'no esta activo';
                else
                    $datos['motivo'] = 'no pago';

            $datos['enviado'] = 0;
            $datos['medio'] = 'not';
            $this->modelo_evento->insertar_eventousers($datos); //insertar
            return 0;
        }
    }


    private function enviar_notificacion_usuarios2($row_usuario, $datos_push)//ENVIA NOTIFICACON PERO NO INSERTA, SE USA PARA EL ELIMINAR EVENTO
    {   $habilitado = 0;

        if( ($row_usuario->recibir_notificacion==1) && ($row_usuario->active==1) )
        {
            if( isset($row_usuario->pago) && isset($row_usuario->pago_alumno) )
            {
                if( ($row_usuario->name!='Tutor') && ($row_usuario->pago==1) ) 
                     $habilitado = 1;
                else 
                    if( ($row_usuario->name=='Tutor') && ($row_usuario->pago_alumno==1) )
                        $habilitado = 1;
                    else $habilitado = 0;
            }
            else $habilitado = 1;

        }
        else $habilitado = 0;
        
        if( $habilitado==1 ) 
        {
            $userid = $row_usuario->user_id;
            //Obtengo el token gcm para enviar la notificacion
            $token_gcm = $this->modelo_usuario->get_push($userid);
            if($token_gcm)
            {
                
                //Recorro los token ya que el usuario puede estar en mas de un cel
                //logueado y por ende tiene un token por cada cel logueado.
                $cantTokens=count($token_gcm); //$cont=0;
                foreach ($token_gcm as $key => $value) {
                    $gcmRegIds = array($value->token);
                    $pushStatus = $this->usuarios->enviar_notificacion($gcmRegIds, $datos_push);
                    $pushStatus = json_decode($pushStatus,true);
                    $success = $pushStatus['success'];


                    if($success == 0)//si es 0 debo mirar el campo result
                    {
                        $cantTokens--;
                        //en error viene el tipo de error
                        $error = $pushStatus["results"][0]["error"];
                        if ($error == "InvalidRegistration")
                        {
                            $this->modelo_usuario->delete_push($userid, $value->token);                            
                            //$cont++;
                        }           
                        elseif ($error == "NotRegistered")
                        {
                            //La apicacion cliente se desinstalo del dispositivo o se desregistro de GCM.
                            //Se debe borrar del servidor
                            $this->modelo_usuario->delete_push($userid, $value->token);
                            //$cont++;
                        }
                        else { //por algun motivo x, no se envio 
                           
                            $nada = 'nada';
                                                       
                        }

                    }
                    
                }                

                if($cantTokens==0)  //si llega a cero es porque todos los token son invalidos o no se envio a ningun movil, debo enviar por email            
                    return 1; 
                else {
                    
                    return 0; //por retornar algo, lo que importa es el 1 
                }
            }
            else 
                return 1; //enviar por email
        }
        else{//sino quire recibir notificaciones, lo mismo debo insertar
            
            return 0;
        }
    }

    //elimina duplicados de un arreglo por clave
    public function unique_multidim_array($array, $key) { 
        $temp_array = array(); 
        $i = 0; 
        $key_array = array(); 
        
        foreach($array as $val) { 
            if (!in_array($val->$key, $key_array)) { 
                $key_array[$i] = $val->$key; 
                $temp_array[$i] = $val; 
            } 
            $i++; 
        } 
        return $temp_array; 
    }


    public function eventosNew_get()  //ANDRES
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_OK);//HTTP_BAD_REQUEST
        }
        else
        {
            // Obtengo todos los eventos
            $eventos = $this->modelo_evento->obtener_eventosNew($userid); // obtiene los eventos de el usuario X, que no han sido vistos
            // Si existe mas de un resultado, lo mando
            if ($eventos)
            {
                $eventos = json_encode($eventos);
                $this->response($eventos, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron eventos'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }

    public function eventos_enviados_get() // DAMIAN eventos enviados o creadospor userid
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_OK);//HTTP_BAD_REQUEST
        }
        else
        {
            $eventos = $this->modelo_evento->obtener_eventosCreados($userid); // Obtengo los creados por mi. 
            if (isset($eventos))
            {
                $eventos = json_encode($eventos);
                $this->response($eventos, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => TRUE,
                    'message' => 'vacio'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

        }
    }

    public function eventos_enviados_eventosusers_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_OK);//HTTP_BAD_REQUEST
        }
        else
        {
            $idevento = $this->get('id'); // obtengo el id del evento, si es que vino en la url
            $eventos = $this->modelo_evento->obtener_eventosenviados_por_ideventos($userid,$idevento); // Obtengo los creados por mi. 
            if (isset($eventos))
            {
                $eventos = json_encode($eventos);
                $this->response($eventos, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => TRUE,
                    'message' => 'vacio'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

        }
    }
    //datatable nuevo
    public function obtener_destinatario($idevento)
    {
        $userid = $this->utilidades->verifica_userid();
           
            $respuesta="Perzonalizado\Varios";
            //$idevento = $this->get('id');
            $lista_filtro = $this->modelo_evento->obtener_filtro($idevento);
            $ciclo = 0;
            foreach ($lista_filtro as $dato)
            {
                $ciclo = $ciclo + 1;
            }
            if($ciclo == 1)
            {
                $arreglo_id = explode(",", $lista_filtro[0]->valores);
                $ciclo_valor = 0;
                foreach ($arreglo_id as $valor)
                {
                    $ciclo_valor = $ciclo_valor + 1;
                }
                if($ciclo_valor == 1)
                {
                    if($lista_filtro[0]->filtro == "nivel")
                    {
                        $aux=$this->modelo_nivel->obtener_nivel_id($arreglo_id[0]);
                        $respuesta = $aux->nombre;
                    }
                    if($lista_filtro[0]->filtro == "division")
                    {
                        $aux=$this->modelo_division->obtener_division_id($arreglo_id[0]);
                        $respuesta= $aux->nombanio.' '.$aux->nombdiv;
                    } 
                    if($lista_filtro[0]->filtro == "rol")
                    {
                        $aux=$this->modelo_usuario->get_rol_id($arreglo_id[0]);
                        $respuesta= $aux->name;
                        if($aux->id == 3)
                        {
                            $respuesta = "Preceptores";
                        }
                        if($aux->name == 4)
                        {
                            $respuesta = "Docentes";
                        }
                        if($aux->name == 5)
                        {
                            $respuesta = "Tutores";
                        }
                        if($aux->name == 6)
                        {
                            $respuesta = "Alumnos";
                        }
                        if($aux->name == 7)
                        {
                            $respuesta = "Administrativos";
                        }
                        if($aux->name == 8)
                        {
                            $respuesta = "Directivos";
                        }
                    }
                    if($lista_filtro[0]->filtro == "usuario")
                    {
                        $aux=$this->modelo_usuario->get_users_id($arreglo_id[0]);
                        $respuesta= $aux->last_name.' '.$aux->first_name;
                    }
                }
                else
                {
                    $respuesta="Perzonalizado\Varios";
                } 
            }
            else
            {
                $respuesta="Perzonalizado\Varios";
            }
            return $respuesta;
        
    }
    public function ajax_list_post()
    {
        $userid = $this->utilidades->verifica_userid();
        $list = $this->modelo_evento->get_datatables($userid);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $person) {
           // $this->obtener_destinatario($person->eventid);
            $no++;
            $row = array();
            $row[] = $person->last_name.' '.$person->first_name;
            $row[] = $person->titulo;
            $row[] = substr($person->fechaCreacion,8,2).'/'.substr($person->fechaCreacion,5,2).'/'.substr($person->fechaCreacion,0,4).' - '.substr($person->fechaCreacion,11,5).' hs';
            //$row[] = $this->obtener_destinatario($person->eventid);
            //$row[] = $person->fechaCreacion;
            //add html for action
            $row[] = '
                     <center><button type="button" class="btn btn-success" title="Ver Detalles del Evento" onclick="detalles_evento('."'".$person->eventid."'".')" id="boton_detalles_evento'."'".$person->eventid."'".'">Detalles</button> <button type="button" class="btn btn-success" id="boton_registro_lecturas'.$person->eventid.'" onclick="ver_registros_de_lecturas('."'".$person->eventid."'".')">Registro de Lectura</button><img type="hidden" style="display:none;" id="loading3'."".$person->eventid."".'" src="'."".base_url("assets/images/loading.gif")."".'" height="50" width="50" /></center>       
                  ';
        
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->modelo_evento->count_all($userid),
                        "recordsFiltered" => $this->modelo_evento->count_filtered($userid),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
    //fin data table nuevo
    public function eventosTodos_get() //todos los que puede ver //ANDRES
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_OK);//HTTP_BAD_REQUEST
        }
        else
        {           
            //$nombregrupo = $_GET['nombregrupo']; 
            //$idcolegio = $_GET['idcolegio'];
                      
            $grupos = $this->modelo_usuario->get_rolesXidusuario_orden($userid); 
            if($grupos)
            {   $output=array();

            foreach ($grupos as $key ) //esto recorre colegios, por cada colegio tiene el rol de mayor prioridad o rango
            {
               $nombregrupo=$key->nombregrupo;
               $colegio=$key->idcolegio;

               if( ($nombregrupo=='Institucion') || ($nombregrupo=='Directivo') || ($nombregrupo=='Nodos')|| ($nombregrupo=='Administrativo') )
                {
                    $eventos = $this->modelo_evento->obtener_eventosTodos($colegio); // Obtengo todos los eventos del colegio.
                }
                else 
                {   
                    $eventos1 = $this->modelo_evento->obtener_eventosCreados($userid,$colegio); // Obtengo los creados por mi, en un colegio. 
                    $eventos2 = $this->modelo_evento->obtener_eventosRecibidos($userid,$colegio); // Obtengo los eventos recibidos, en un colegio.
                    $eventos = array_merge($eventos1, $eventos2); 

                    $eventos = $this->unique_multidim_array($eventos, 'eventid');
                }

                foreach ($eventos as $key ) 
                { //agregar campo si es propietario o no. 1 o 0
                    if($key->userid==$userid)
                        $key->owner = 1;
                    else $key->owner = 0;
                    $output[] = $key;
                }
            }


            //$eventos = $this->modelo_evento->obtener_eventosNew($userid); 
            // obtiene los eventos de el usuario X, que no han sido vistos            
            // Si existe mas de un resultado, lo mando
            if (isset($output))
            {
                $output = json_encode($output);
                $this->response($output, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => TRUE,
                    'message' => 'vacio'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
            }

            }//fin if grupos
        }
    }  

    /**
     * $arre_eventos es un string que viene en JSON y se convierte en arreglo PHP
     */
    public function visto_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $arre_eventos = json_decode($this->input->post("eventosid"));
            $token = $this->input->post("token");
        	// Obtengo y proceso el arreglo de eventos y consulto que al menos tenga 1 elemento el arreglo
        	if ((isset($arre_eventos)) && (!empty($arre_eventos)) && (is_array($arre_eventos)) && (count($arre_eventos) > 0))
        	{
        		foreach ($arre_eventos as $keyev => $valev)
        		{
                    $val = json_decode($valev);
        			$this->modelo_evento->marcar_visto($val[0]);
                    //si existe token es porque viene del movil e inserto en 
                    //eventosuser_detalle
                    if(isset($token)){
                        $data["eventosusers_id"] = $val[0];
                        //obtengo el id del token
                        $push_result = $this->modelo_usuario->existe_registerid_gcm($userid, $token);
                        foreach ($push_result as $key => $value) {
                            $token_id = $value->id;
                            $data["token_id"] = $token_id;
                            $this->modelo_evento->marcar_visto_detalle($data);
                        }                        
                    }                    
        		}
                $this->response("OK", REST_Controller::HTTP_OK);        		
        	}
        	else
        	{
	            $this->response([
	                'status' => FALSE,
	                'message' => 'Debe enviar un arreglo de IDs de eventos con al menos 1 ID'
	            ], REST_Controller::HTTP_BAD_REQUEST);        		
        	}
		}    	
    }

    /**
     * $arre_eventos es un string que viene en JSON y se convierte en arreglo PHP
     */
    public function aceptado_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $arre_eventos = json_decode($this->input->post("eventosid"));
            $pin = $this->input->post('pin'); // obtengo el pin, si es que vino en la url
            $existe = $this->utilidades->verifica_pin($pin);//verifico que el pin ingresado por el usuario es correcto
            $token = $this->input->post("token");
            // Si existe mas de un resultado, el pin esta bien
            if ($existe == 1)
            {
                // Obtengo y proceso el arreglo de eventos y consulto que al menos tenga 1 elemento el arreglo
                if ((isset($arre_eventos)) && (!empty($arre_eventos)) && (is_array($arre_eventos)) && (count($arre_eventos) > 0))
                {                
                    foreach ($arre_eventos as $keyev => $valev) 
                    {
                        $val = json_decode($valev);
                        //primer parametro es el id del evento y el segundo es el id del alumno
                        $this->modelo_evento->marcar_aceptado($val[0]);
                        //si existe token es porque viene del movil e inserto en 
                        //eventosuser_detalle
                        if(isset($token)){
                            $data["eventosusers_id"] = $val[0];
                            //obtengo el id del token
                            $push_result = $this->modelo_usuario->existe_registerid_gcm($userid, $token);
                            foreach ($push_result as $key => $value) {
                                $token_id = $value->id;
                                $data["token_id"] = $token_id;
                                $this->modelo_evento->marcar_aceptado_detalle($data);
                            }                            
                        }                        
                    }

                    $this->response("OK", REST_Controller::HTTP_OK);                
                }
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Debe enviar un arreglo de IDs de eventos con al menos 1 ID',
                        'result' =>  $pin,
                        'consulta' =>$this->db->last_query()
                    ], REST_Controller::HTTP_BAD_REQUEST);              
                }           
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'El pin no es correcto'
                ], REST_Controller::HTTP_BAD_REQUEST);              
            }            

        }       
    }

    /**
     * $arre_eventos es un string que viene en JSON y se convierte en arreglo PHP
     */
    public function rechazado_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO',
                'result' =>  $pin
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $arre_eventos = json_decode($this->input->post("eventosid"));
            $pin = $this->input->post('pin'); // obtengo el pin, si es que vino en la url
            $existe = $this->utilidades->verifica_pin($pin);//verifico que el pin ingresado por el usuario es correcto
            $token = $this->input->post("token");
            // Si existe mas de un resultado, el pin esta bien
            if ($existe == 1)
            {
                // Obtengo y proceso el arreglo de eventos y consulto que al menos tenga 1 elemento el arreglo
                if ((isset($arre_eventos)) && (!empty($arre_eventos)) && (is_array($arre_eventos)) && (count($arre_eventos) > 0))
                {
                    foreach ($arre_eventos as $keyev => $valev) 
                    {
                        $val = json_decode($valev);
                        $this->modelo_evento->marcar_rechazado($val[0]);
                        //si existe token es porque viene del movil e inserto en 
                        //eventosuser_detalle
                        if(isset($token)){
                            $data["eventosusers_id"] = $val[0];
                            //obtengo el id del token
                            $push_result = $this->modelo_usuario->existe_registerid_gcm($userid, $token);
                            foreach ($push_result as $key => $value) {
                                $token_id = $value->id;
                                $data["token_id"] = $token_id;
                                $this->modelo_evento->marcar_rechazado_detalle($data);
                            }                            
                        }                         
                    }
                    $this->response("OK", REST_Controller::HTTP_OK);                
                }
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Debe enviar un arreglo de IDs de eventos con al menos 1 ID'
                    ], REST_Controller::HTTP_BAD_REQUEST);              
                }
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'El pin no es correcto'
                ], REST_Controller::HTTP_BAD_REQUEST);              
            }               
        }       
    }

    /**
     * $arre_eventos es un string que viene en JSON y se convierte en arreglo PHP
     */
    public function megusta_post()  //y ella?
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $eventousers_id = $this->input->post("eventousers_id");
            $token = $this->input->post("token");
            // Obtengo y proceso el arreglo de eventos y consulto que al menos tenga 1 elemento el arreglo
            if ($eventousers_id)
            {
                $me_gusta = $this->input->post("me_gusta");
                if(isset($me_gusta)){
                    if($me_gusta == 1){
                        $this->modelo_evento->marcar_megusta($eventousers_id);
                        //si existe token es porque viene del movil e inserto en 
                        //eventosuser_detalle
                        if(isset($token)){
                            $data["eventosusers_id"] = $eventousers_id;
                            //obtengo el id del token
                            $push_result = $this->modelo_usuario->existe_registerid_gcm($userid, $token);
                            foreach ($push_result as $key => $value) {
                                $token_id = $value->id;
                                $data["token_id"] = $token_id;
                                $this->modelo_evento->marcar_megusta_detalle($data);
                            }                            
                        }                         
                    }
                    else{
                        $this->modelo_evento->desmarcar_megusta($eventousers_id);
                        //si existe token es porque viene del movil e inserto en 
                        //eventosuser_detalle
                        if(isset($token)){
                            $data["eventosusers_id"] = $eventousers_id;
                            //obtengo el id del token
                            $push_result = $this->modelo_usuario->existe_registerid_gcm($userid, $token);
                            foreach ($push_result as $key => $value) {
                                $token_id = $value->id;
                                $data["token_id"] = $token_id;
                                $this->modelo_evento->desmarcar_megusta_detalle($data);
                            }
                        }                         
                    }
                    $this->response([
                        'status' => TRUE,
                        'last_query' =>  $this->db->last_query()
                    ], REST_Controller::HTTP_OK);
                }
                else{
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Faltan datos para terminar la operación'
                    ], REST_Controller::HTTP_BAD_REQUEST);
                }
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Debe enviar un evento como mínimo'
                ], REST_Controller::HTTP_BAD_REQUEST);              
            }
        }       
    }

    public function construir_mensaje()
    {
        return "

        ";
    }



}
