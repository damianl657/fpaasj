﻿
<script src="<?//=base_url('assets/js/bootstrap.min.js');?>"></script>

<script type="text/javascript">



$(document).ready(function(){

      //$('.btn_materias').attr('disabled', true);

      <?if($colegios)
            foreach ($colegios as $cole){
                $idcole = $cole['idcole'];
                echo "generar_datatable($idcole);";
            }
            ?>

      $('.table').css('display','block');

      generar_datatable_menuacciones();
      //get_roles_x_colegio(9);
  });



  function get_roles_x_colegio(idcole){

        $("#tabladatos_"+idcole).dataTable().fnDestroy();
        $("#tabladatos_body_"+idcole).empty();

        var url = '<?=base_url("/index.php/roles/get_roles_x_colegio_ajax")?>';

        $.ajax({
          url: url,
          type: 'POST',
          data:{idcole:idcole},
          beforeSend: function() {
              //$('#selectareas_'+idcole).after("<img id='loadingMat' src='<?=base_url('assets/images/loading.gif');?>' height='25' width='25' />");
          },
          success: function(data)
          {
            //console.log(data);
            var data = jQuery.parseJSON( data );
            //console.log(data);

            if( data['status']===0){
              var string = "<p class='text-danger'>No hay materias asignadas</p>";
              //console.log(string);
              $("#tabladatos_body_"+idcole).html(string);

              //tableMatAsig=null;
            }
            else
            {
              var cont = 1 ;

              data.data.forEach(function(currentValue,index){
                //console.log(currentValue);
                var id = currentValue.id;
                var name = currentValue.name;

                if(currentValue.aliasM!=null)
                  var aliasM = currentValue.aliasM;
                else var aliasM = '';

                if(currentValue.aliasF!=null)
                  var aliasF = currentValue.aliasF;
                else var aliasF = '';

                var description = currentValue.description;
                var orden = currentValue.orden;



                var fila = '<tr id="rowrol_'+idcole+'_'+id+'">'+
                              '<td>'+cont+'</td>'+
                              '<td class="nombre">'+name+'</td>'+
                              '<td class="alias1">'+aliasM+'</td>'+
                              '<td class="alias2">'+aliasF+'</td>'+
                              '<td class="desc">'+description+'</td>'+
                              '<td class="orden">'+orden+'</td>'+
                              '<td>'+
                                '<div align="center">'+
                                  <?if(count($editrol)>0)
                                    if (in_array($idcole, $editrol)) { ?>
                                      '<button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="" title="editar rol" onclick="abrir_modal_editrol(this,'+id+','+idcole+')" type="button"><i class="glyphicon glyphicon-edit"></i></button>'+
                                      '<button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="" title="borrar rol" onclick="delete_rol(this,'+id+','+idcole+')" type="button"><i class="glyphicon glyphicon-trash"></i></button>'+
                                    <? } ?>
                                '</div>'+
                              '</td>'+

                              '<td><div align="center">'+
                                <?if(count($reglascom)>0)
                                    if (in_array($idcole, $reglascom)) { ?>
                                    ' <button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="" title="asignar reglas de comunicaci&oacute;n" onclick="abrir_modal_reglascom(this,'+id+', '+idcole+',"reglas")" type="button"><i class="glyphicon fa fa-envelope"></i></button> &nbsp; &nbsp;'+
                                  <? } ?>

                                <?if(count($registrolec)>0)
                                    if (in_array($idcole, $registrolec)) { ?>
                                    '<button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="" title="asignar permisos de lectura" onclick="abrir_modal_reglascom(this,'+id+', '+idcole+',"eventos")" type="button"><i class="glyphicon fa fa-eye"></i></button>&nbsp; &nbsp; '+
                                  <? } ?>

                                <?if(count($reglassupervisa)>0)
                                    if (in_array($idcole, $reglassupervisa)) { ?>
                                    '<button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="" title="asignar permisos de supevisi&oacute;n" onclick="abrir_modal_reglascom(this,'+id+','+idcole+',"supervisa")" type="button"><i class="glyphicon fa fa-lock"></i></button>'+
                                  <? } ?>

                                <?if(count($reglasacciones)>0)
                                  if (in_array($idcole, $reglasacciones)) { ?>
                                  '<button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="" title="asignar funciones y acciones" onclick="abrir_modal_menuacciones(this,'+id+', '+idcole+')" type="button"><i class="glyphicon fa fa-list"></i></button>'+
                                <? } ?>

                              '</div></td>'+
                          '</tr>';

                $("#tabladatos_body_"+idcole).append(fila);


                var fila2 = '<tr id="rowrolreglas_'+idcole+'_'+id+'">'+
                              '<td>'+
                                '<div align="center" class="custom-control custom-checkbox">'+
                                    '<input type="checkbox" class="custom-control-input checkrol" name="checkreglascom" value="'+id+'" id="idrolregla_'+id+'">'+
                                '</div>'+
                              '</td>'+
                              '<td class="nombre">'+name+'</td>'+
                              '<td class="alias1">'+aliasM+'</td>'+
                              '<td class="alias2">'+aliasF+'</td>'+
                              //'<td class="desc">'+description+'</td>'+
                              //'<td class="orden">'+orden+'</td>'+
                          '</tr>';

                $("#tablareglascom_body_"+idcole).append(fila2);

                cont++;

              });
              generar_datatable(idcole);
            }

            //$('#loadingMat').remove();
          },
          error: function(response){
              console.log(response);

              $('#loadingMat').remove();
          }
        });
  }


  var tableRolesRCom = Array();
  var tableRoles = null;
  var tableMenuacciones = null;

  function generar_datatable(id_colegio)
  {
    //$("#tabladatos_"+id_colegio).DataTable();

            var handleDataTableButtons2 = function() {
              if ($("#tabladatos_"+id_colegio).length) {
                tableRoles=$("#tabladatos_"+id_colegio).DataTable({

            //"scrollX": true,
            //"scrollY": true,

            "scroller": {
            "displayBuffer": 2
            },


                  responsive: true,
                  keys: true,
                  deferRender: true,
                  scrollCollapse: true,

                  "language": {
                                "sProcessing":     "Procesando...",
                                "sLengthMenu":     "Mostrar _MENU_ registros",
                                "sZeroRecords":    "No se encontraron resultados",
                                "sEmptyTable":     "NingÃºn dato disponible en esta tabla",
                                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Buscar:",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":     "Ãšltimo",
                                    "sNext":     "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                              }
                });
              }
            };
            TableManageButtons2 = function() {
              "use strict";
              return {
                init: function() {

                  handleDataTableButtons2();
                }
              };
            }();
            TableManageButtons2.init();
            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true,

            });
  }

  function generar_datatable_reglascom(id_colegio)
  {

    if(!tableRolesRCom[id_colegio]) {
        var handleDataTableButtons2 = function() {
        if ($("#tablareglascom_"+id_colegio).length) {
            tableRolesRCom[id_colegio] = $("#tablareglascom_"+id_colegio).DataTable({

                //"scrollX": true,
                //"scrollY": true,

                "scroller": {
                "displayBuffer": 2
                },


                  responsive: true,
                  keys: true,
                  deferRender: true,
                  scrollCollapse: true,

                  "ordering": true,

                  "language": {
                                "sProcessing":     "Procesando...",
                                "sLengthMenu":     "Mostrar _MENU_ registros",
                                "sZeroRecords":    "No se encontraron resultados",
                                "sEmptyTable":     "NingÃºn dato disponible en esta tabla",
                                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Buscar:",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":     "Ãšltimo",
                                    "sNext":     "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                              }
                });
              }
            };

        TableManageButtons2 = function() {
          "use strict";
          return {
            init: function() {

              handleDataTableButtons2();
            }
          };
        }();

        TableManageButtons2.init();
        //var table = $('#datatable-fixed-header').DataTable({
          //fixedHeader: true,
        //});
    }
  }

  function generar_datatable_menuacciones()
  {
    //$("#tabladatos_"+id_colegio).DataTable();

            var handleDataTableButtons3 = function() {
              if ($("#tablamenuacciones").length) {
                tableMenuacciones=$("#tablamenuacciones").DataTable({

            //"scrollX": true,
            //"scrollY": true,

            "scroller": {
            "displayBuffer": 2
            },


                  responsive: true,
                  keys: true,
                  deferRender: true,
                  scrollCollapse: true,

                  "language": {
                                "sProcessing":     "Procesando...",
                                "sLengthMenu":     "Mostrar _MENU_ registros",
                                "sZeroRecords":    "No se encontraron resultados",
                                "sEmptyTable":     "NingÃºn dato disponible en esta tabla",
                                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Buscar:",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":     "Ãšltimo",
                                    "sNext":     "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                              }
                });
              }
            };
            TableManageButtons3= function() {
              "use strict";
              return {
                init: function() {

                  handleDataTableButtons3();
                }
              };
            }();
            TableManageButtons3.init();
            /*var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true,

            });  */
  }


  function delete_rol(elem, rolId, colegioId)
  {

    bootbox.confirm("Eliminar Rol?", function(result){
      if(result){

        parametros = {
          'rolId': rolId,
          'colegioId': colegioId,
        };

      var stringJson = JSON.stringify(parametros);

        var urlRequest = '<?=base_url("/index.php/roles/delete_rol_ajax")?>';

        $.ajax({
              url: urlRequest,
              type: 'POST',
              headers: {

              },
              data: {data: stringJson},
              beforeSend: function() {
                  $(elem).after("<img id='loadingdelete' src='<?=base_url('assets/images/loading.gif');?>' height='15' width='15' />");
              },
              success: function(data){
                    //console.log(data);
                    var data = jQuery.parseJSON( data );
                    //console.log(data);
                    if(data['status']==0)
                    {
                        if(data['message'] == 'error')
                        {
                           $.notify('Error al dar de baja', "warning");
                        }
                        else
                          if(data['message'] == 'has_users')
                          {
                             $.notify('Error: posee usuarios asignados', "warning");
                          }
                    }
                    else{   //alert('OK');
                          $.notify('Rol dado de baja', "success");

                          get_roles_x_colegio(colegioId);
                    }
                    $('#loadingdelete').remove();

              },
              error: function(response){
                  //console.log(response);
              }
          });

      }

    });

  }

  function add_rol()
  {
    var $myForm = $('#formaddrol');
    if ($myForm[0].checkValidity()) {

      var colegioId = $('#formaddrol #colegioId').val();

      parametros = {
        'nombre': $('#formaddrol #nombre').val(),
        'alias1': $('#formaddrol #alias1').val(),
        'alias2': $('#formaddrol #alias2').val(),
        'desc': $('#formaddrol #desc').val(),
        'orden': $('#formaddrol #orden').val(),
        'colegioId': colegioId,
      };

      var stringJson = JSON.stringify(parametros);


      var urlRequest = '<?=base_url("/index.php/roles/add_rol_ajax")?>';

      $.ajax({
            url: urlRequest,
            type: 'POST',
            headers: {

            },
            data: {datos: stringJson},
            beforeSend: function() {
                $('#guardar_rol').after("<img id='loadingAddrol' src='<?=base_url('assets/images/loading.gif');?>' height='15' width='15' />");
            },
            success: function(data){
                  //console.log(data);
                  var data = jQuery.parseJSON( data );
                  console.log(data);

                  if(data['status']==0)
                  {
                      if(data['message'] == 'error')
                      {
                        $.notify('Error al insertar Rol', "danger");
                      }

                      $('#loadingAdd').remove();
                      $('#modaladdrol').modal('hide');
                   }
                  else{
                        $.notify('Nuevo Rol Agregado', "success");

                        $('#loadingAdd').remove();
                        $('#modaladdrol').modal('hide');
                        get_roles_x_colegio(colegioId);
                  }

            },
            error: function(response){
                //console.log(response);
            }
      });

    }
    else {
        var nombre = $('#formaddrol #nombre').val();
        if(nombre==""){
          $('#formaddrol #nombre').focus();
          $('#formaddrol #nombre').notify("Ingrese un nombre", {className:"error", autoHide: true});
        }
    }
  }

  function edit_rol()
  {
    var $myForm = $('#formeditrol');
    if ($myForm[0].checkValidity()) {

      var colegioId = $('#formeditrol #colegioId').val();

      parametros = {
        'nombre': $('#formeditrol #nombre').val(),
        'alias1': $('#formeditrol #alias1').val(),
        'alias2': $('#formeditrol #alias2').val(),
        'desc': $('#formeditrol #desc').val(),
        'orden': $('#formeditrol #orden').val(),
        'colegioId': colegioId,
        'id': $('#formeditrol #idrol').val(),
      };

      var stringJson = JSON.stringify(parametros);

      //console.log(stringJson);
      var urlRequest = '<?=base_url("/index.php/roles/add_rol_ajax")?>';

      $.ajax({
            url: urlRequest,
            type: 'POST',
            headers: {

            },
            data: {datos: stringJson},
            beforeSend: function() {
                $('#editar_rol').after("<img id='loadingAddrol' src='<?=base_url('assets/images/loading.gif');?>' height='15' width='15' />");
            },
            success: function(data){
                  //console.log(data);
                  var data = jQuery.parseJSON( data );
                  //console.log(data);

                  if(data['status']==0)
                  {
                      if(data['message'] == 'error')
                      {
                        $.notify('Error al editar Rol', "danger");
                      }

                      $('#loadingAdd').remove();
                      $('#modaleditrol').modal('hide');
                  }
                  else{
                        $.notify('Rol Editado', "success");
                        $('#loadingAdd').remove();
                        $('#modaleditrol').modal('hide');
                        get_roles_x_colegio(colegioId);
                  }

            },
            error: function(response){
                //console.log(response);
            }
      });

    }
    else {
        var nombre = $('#formeditrol #nombre').val();
        if(nombre==""){
          $('#formeditrol #nombre').focus();
          $('#formeditrol #nombre').notify("Ingrese un nombre", {className:"error", autoHide: true});
        }
    }
  }

  function abrir_modal_addrol(id_colegio)
  {
    $('#formaddrol #colegioId').val(id_colegio);
    $('#formaddrol #nombre').focus();

    $('#modaladdrol').modal('show');
  }


  function abrir_modal_editrol(elem, id, id_colegio)
  {
    //var tr = $(elem).parent('div').parent('td').parent('tr');
    var tr = $('#rowrol_'+id_colegio+'_'+id);
    var nombre = $(tr).find('.nombre').text();
    var alias1 = $(tr).find('.alias1').text();
    var alias2 = $(tr).find('.alias2').text();
    var desc = $(tr).find('.desc').text();
    var orden = $(tr).find('.orden').text();

    $('#formeditrol #idrol').val(id);
    $('#formeditrol #colegioId').val(id_colegio);
    $('#formeditrol #nombre').val(nombre);
    $('#formeditrol #alias1').val(alias1);
    $('#formeditrol #alias2').val(alias2);
    $('#formeditrol #desc').val(desc);
    $('#formeditrol #orden').val(orden);

    $('#modaleditrol').modal('show');
  }


  function add_reglas_com(colegioId)
  {
      var grupoId = $('#formreglascom_'+colegioId+' .id_group').val();
      var campo = $('#formreglascom_'+colegioId+' .campo').val();


      var selected = [];
      var noselected = [];

      if(tableRolesRCom[colegioId])
        tableRolesRCom[colegioId].rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            //var data = this.data();
            var node = this.node();
            var check = $(node).find('.checkrol');
            if($(check).is(':checked'))
              selected.push($(check).val());
            else
              noselected.push($(check).val());
        } );

      var roles = JSON.stringify(selected);
      var noroles = JSON.stringify(noselected);

      //console.log(roles);
      //console.log(id_grupo);
      //console.log(campo);

      var urlRequest = '<?=base_url("/index.php/roles/add_reglascom_x_rol_ajax")?>';

      $.ajax({
            url: urlRequest,
            type: 'POST',
            headers: {

            },
            data: {roles: roles, noroles: noroles, colegioId: colegioId, grupoId:grupoId, campo:campo},
            beforeSend: function() {
                $('#guardar_reglascom_'+colegioId).after("<img id='loadingAddRC' src='<?=base_url('assets/images/loading.gif');?>' height='15' width='15' />");
            },
            success: function(data){
                  //console.log(data);
                  var data = jQuery.parseJSON( data );
                  //console.log(data);

                  if(!data['status'])
                      $.notify('Error al asignar permisos', "warning");
                  else
                      $.notify('Permisos Asignados', "success");

                  $('#loadingAddRC').remove();
                  $('#modalreglascom_'+colegioId).modal('hide');
            },
            error: function(response){
                //console.log(response);
            }
        });

  }

  function abrir_modal_reglascom(elem, id, id_colegio,campo)
  {

    var url = '<?=base_url("/index.php/roles/get_reglascom_x_rol_ajax")?>';

        $.ajax({
          url: url,
          type: 'POST',
          data:{idcole:id_colegio, idrol:id, campo:campo},
          beforeSend: function() {
             generar_datatable_reglascom(id_colegio);

            $('#formreglascom_'+id_colegio+' .id_group').val(id);
            $('#formreglascom_'+id_colegio+' .campo').val(campo);

            if(tableRolesRCom[id_colegio])
              tableRolesRCom[id_colegio].rows().every( function ( rowIdx, tableLoop, rowLoop ) {
                  //var data = this.data();
                  var node = this.node();
                  $(node).find('.checkrol').prop('checked',false);
              } );
            $(elem).after("<img id='loadingReglas' src='<?=base_url('assets/images/loading.gif');?>' height='25' width='25' />");
          },
          success: function(data)
          {
            //console.log(data);
            var data = jQuery.parseJSON( data );
            //console.log(data);

            if( data['status']===0){

            }
            else
            {
              var arre = data['result'];
              //console.log(arre);

              for (var i = 0; i < arre.length; i++) {
                //alert(arre[i]);
                var idrol = arre[i];
                //console.log(idrol);
                var row = tableRolesRCom[id_colegio].row('#rowrolreglas_'+id_colegio+'_'+idrol).node();
                $(row).find('.checkrol').prop('checked',true);
              }
            }

            $('#loadingReglas').remove();
          },
          error: function(response){
              console.log(response);

              $('#loadingReglas').remove();
          }
        });


    $('#modalreglascom_'+id_colegio).modal('show');
  }


  function abrir_modal_menuacciones(elem, id, id_colegio)
  {

    var url = '<?=base_url("/index.php/roles/get_reglascom_x_rol_ajax")?>';

    $.ajax({
      url: url,
      type: 'POST',
      data:{idcole:id_colegio, idrol:id, campo:"menu"},
      beforeSend: function() {

        if(tableMenuacciones)
          tableMenuacciones.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
              var node = this.node();
              $(node).find('.checkmenu').prop('checked',false);
          } );
        $(elem).after("<img id='loadingMenu' src='<?=base_url('assets/images/loading.gif');?>' height='25' width='25' />");

        $('#modalmenuacciones .id_group').val(id);
        $('#modalmenuacciones .id_cole').val(id_colegio);

      },
      success: function(data)
      {
        //console.log(data);
        var data = jQuery.parseJSON( data );
        //console.log(data);

        if( data['status']===0){

        }
        else
        {
          var arre = data['result'];
          //console.log(arre);

          for (var i = 0; i < arre.length; i++) {
            //alert(arre[i]);
            var idrol = arre[i];
            //console.log(idrol);
            var row = tableMenuacciones.row('#rowmenuacciones_'+idrol).node();
            $(row).find('.checkmenu').prop('checked',true);
          }
        }

        $('#loadingMenu').remove();
      },
      error: function(response){
          console.log(response);

          $('#loadingReglas').remove();
      }
    });


    $('#modalmenuacciones').modal('show');
  }


  function add_menuacciones(colegioId)
  {
      var grupoId = $('#modalmenuacciones .id_group').val();
      var colegioId = $('#modalmenuacciones .id_cole').val();

      var selected = [];

      if(tableMenuacciones)
        tableMenuacciones.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            //var data = this.data();
            var node = this.node();
            var check = $(node).find('.checkmenu');
            if($(check).is(':checked'))
              selected.push($(check).val());
        } );

      var menues = JSON.stringify(selected);

      //console.log(roles);
      //console.log(id_grupo);
      //console.log(campo);

      var urlRequest = '<?=base_url("/index.php/roles/add_reglascom_x_rol_ajax")?>';

      $.ajax({
            url: urlRequest,
            type: 'POST',
            headers: {

            },
            data: {roles: menues, colegioId: colegioId, grupoId:grupoId, campo:"menu"},
            beforeSend: function() {
                $('#guardar_menuacciones').after("<img id='loadingAddMA' src='<?=base_url('assets/images/loading.gif');?>' height='15' width='15' />");
            },
            success: function(data){
                  //console.log(data);
                  var data = jQuery.parseJSON( data );
                  //console.log(data);

                  if(!data['status'])
                      $.notify('Error al asignar permisos', "warning");
                  else
                      $.notify('Permisos Asignados', "success");

                  $('#loadingAddMA').remove();
                  $('#modalmenuacciones').modal('hide');
            },
            error: function(response){
                //console.log(response);
            }
        });
  }

</script>





<div class="wrapper wrapper-content animated fadeInRight" id="">
  <div class="row">
    <div class="col-lg-12 " id="usuarios_borrar">

        <div class="ibox collapsed2 float-e-margins">
          <div class="ibox-title" id="tituloaltaevento">
              <h5>Roles/Funciones por Club</h5>
              <div class="ibox-tools">
                  <a class="">
                      <!--<i class="fa fa-chevron-up"></i>-->
                  </a>
              </div>
          </div>
          <div class="ibox-content">
            <div class="row row2">
              <div class="col-sm-12 b-r" id="">
                <div class="tabs-container">
                  <ul class="nav nav-tabs" id="colegios_dir_prec">

                    <?
                    $cont=0;
                    if($colegios)
                      foreach ($colegios as $cole)
                      {
                        $idcole = $cole['idcole'];
                        $url = base_url()."Roles/index#tab_dir-".$idcole;
                        $name = $cole['namecole'];
                        if($cont==0)
                          echo "<li class='active'><a data-toggle='tab' href='$url'>$name</a></li>";
                        else
                          echo "<li class=''><a data-toggle='tab' href='$url'>$name</a></li>";

                        $cont++;
                      }


                    ?>
                  </ul>

                  <div class="tab-content " id="contenido_tab_dir_prec">

                    <?
                    $cont=0;
                    if($colegios)
                      foreach ($colegios as $cole)
                      {
                        $idcole = $cole['idcole'];
                        $id_grupo = $cole['idrol'];
                        $rol = $cole['namerol'];

                        if($cont==0)
                          $clase = 'active';
                        else
                          $clase = ''
                        ?>

                          <div id="tab_dir-<?=$idcole?>" class="tab-pane <?=$clase?>">
                            <div class="panel-body table-responsive">
                                <div class="row">
                                    <div class="col-sm-6">

                                    </div>
                                    <div class="col-sm-6" style="margin-top:0px">
                                        <?if(count($editrol)>0)
                                          if (in_array($idcole, $editrol)) { ?>
                                            <button class="btn btn-sm btn-info pull-right m-t-n-xs btn_materias" id="btn_rol_<?=$idcole?>" onclick="abrir_modal_addrol(<?=$idcole?> )" type="button"><i class="glyphicon glyphicon-plus "></i>Agregar Rol<strong></strong></button>
                                          <? } ?>
                                    </div>
                                </div>

                                <table class="table table-striped table-bordered dt-responsive nowrap" id="tabladatos_<?=$idcole?>" cellspacing="0" 
cellpadding="3" width="100%" style="width: 0px;" >
                                    <thead>
                                      <th>#</th>
                                      <th>Nombre</th>
                                      <th>Alias Masculino</th>
                                      <th>Alias Femenino</th>
                                      <th>Descripci&oacute;n</th>
                                      <th>Orden</th>
                                      <th>Operaciones</th>
                                      <th width="150">Permisos</th>
                                    </thead>
                                    <tbody id="tabladatos_body_<?=$idcole?>">
                                        <?
                                          $i=1;
                                          if($roles[$idcole]){
                                                foreach ($roles[$idcole] as $rol)
                                                {
                                                  //print_r($mat); die();
                                                  $id = $rol->id;
                                                  $nombre = $rol->name;
                                                  $aliasM = $rol->aliasM;
                                                  $aliasF = $rol->aliasF;
                                                  $desc = $rol->description;
                                                  $orden = $rol->orden;
                                                  ?>

                                                  <tr id='rowrol_<?echo $idcole."_".$id?>'>
                                                      <td><?=$i?></td>
                                                      <td class="nombre"><?=$nombre?></td>
                                                      <td class="alias1"><?=$aliasM?></td>
                                                      <td class="alias2"><?=$aliasF?></td>
                                                      <td class="desc"><?=$desc?></td>
                                                      <td class="orden"><?=$orden?></td>
                                                      <td>
                                                        <div align="center">
                                                          <?if(count($editrol)>0)
                                                              if (in_array($idcole, $editrol)) { ?>

                                                                <button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="" title="editar rol" onclick="abrir_modal_editrol(this,<?=$id?>, <?=$idcole?>)" type="button"><i class="glyphicon glyphicon-edit"></i></button>
                                                                <button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="" title="borrar rol" onclick="delete_rol(this,<?=$id?>, <?=$idcole?>)" type="button"><i class="glyphicon glyphicon-trash"></i></button>

                                                            <? } ?>
                                                        </div>
                                                      </td>

                                                      <td>
                                                        <div align="center">

                                                          <?if(count($reglascom)>0)
                                                              if (in_array($idcole, $reglascom)) { ?>
                                                                <button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="" title="asignar reglas de comunicaci&oacute;n" onclick="abrir_modal_reglascom(this,<?=$id?>, <?=$idcole?>,'reglas')" type="button"><i class="glyphicon fa fa-envelope"></i></button> &nbsp; &nbsp;
                                                              <? } ?>

                                                          <?if(count($registrolec)>0)
                                                              if (in_array($idcole, $registrolec)) { ?>
                                                                <button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="" title="asignar permisos de lectura" onclick="abrir_modal_reglascom(this,<?=$id?>, <?=$idcole?>,'eventos')" type="button"><i class="glyphicon fa fa-eye"></i></button>&nbsp; &nbsp;
                                                              <? } ?>

                                                          <?if(count($reglassupervisa)>0)
                                                              if (in_array($idcole, $reglassupervisa)) { ?>
                                                                <button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="" title="asignar permisos de supevisi&oacute;n" onclick="abrir_modal_reglascom(this,<?=$id?>, <?=$idcole?>,'supervisa')" type="button"><i class="glyphicon fa fa-lock"></i></button>
                                                              <? } ?>

                                                           <?if(count($reglasacciones)>0)
                                                              if (in_array($idcole, $reglasacciones)) { ?>
                                                                <button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="" title="asignar funciones y acciones" onclick="abrir_modal_menuacciones(this,<?=$id?>, <?=$idcole?>)" type="button"><i class="glyphicon fa fa-list"></i></button>
                                                              <? } ?>

                                                        </div>
                                                      </td>
                                                  </tr>

                                                  <?
                                                  $i++;
                                                }
                                          }
                                        ?>
                                    </tbody>
                                </table>

                            </div>
                          </div>





                        <!-- modal reglas comunic -->
                        <div class="modal" id="modalreglascom_<?=$idcole?>" role="dialog">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title">Reglas de Comunicaci&oacute;n</h4>
                              </div>

                              <div class="modal-body">
                                <form id="formreglascom_<?=$idcole?>" autocomplete="on" method="post" class="form-horizontal form-label-left">

                                    <div class="panel-body table-responsive">

                                        <input type="hidden" class="id_group">
                                        <input type="hidden" class="campo">

                                        <table class="table table-striped table-bordered dt-responsive nowrap" id="tablareglascom_<?=$idcole?>" cellspacing="0" 
cellpadding="3" width="100%" style="width: 0px;">
                                            <thead>
                                              <th></th>
                                              <th>Nombre</th>
                                              <th>Alias Masculino</th>
                                              <th>Alias Femenino</th>
                                              <!--<th>Descripci&oacute;n</th>
                                              <th>Orden</th>-->
                                            </thead>
                                            <tbody id="tablareglascom_body_<?=$idcole?>">
                                              <?
                                              //$cont=1;
                                              if($roles[$idcole]){
                                                foreach ($roles[$idcole] as $rol)
                                                {
                                                      //print_r($mat); die();
                                                      $id = $rol->id;
                                                      $nombre = $rol->name;
                                                      $aliasM = $rol->aliasM;
                                                      $aliasF = $rol->aliasF;
                                                      //$desc = $rol->description;
                                                      //$orden = $rol->orden;
                                                      ?>

                                                      <tr id='rowrolreglas_<?echo $idcole."_".$id?>'>
                                                          <td>
                                                            <div align="center" class="custom-control custom-checkbox">
                                                                <input type="checkbox" class="custom-control-input checkrol" name="checkreglascom" value="<?=$id?>" id="idrolregla_<?=$id?>">
                                                            </div>
                                                          </td>
                                                          <td class="nombre"><?=$nombre?></td>
                                                          <td class="alias1"><?=$aliasM?></td>
                                                          <td class="alias2"><?=$aliasF?></td>
                                                          <!--<td class="desc"><?=$desc?></td>
                                                          <td class="orden"><?=$orden?></td>-->
                                                      </tr>


                                                      <?
                                                      //$cont++;
                                                    }
                                              }
                                              ?>
                                            </tbody>
                                        </table>

                                        <?
                                        echo "<script>tableRolesRCom[$idcole]=false;</script>";

                                          /*$clave = in_array('add_materia', $acciones);
                                          if($clave)  { ?>
                                              <button class="btn btn-sm btn-info pull-left m-t-n-xs" id="btn_alumnos" onclick="abrir_modal_addmateria()" type="button"><i class="glyphicon glyphicon-plus"></i> Agregar Materia<strong></strong></button>
                                        <?}*/?>


                                    </div>

                                </form>

                              </div>

                              <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                  <button class="btn btn-primary" id ="guardar_reglascom_<?=$idcole?>" onclick="add_reglas_com(<?=$idcole?>)">Agregar</button>
                              </div>

                            </div>
                          </div>
                        </div>


                      <?$cont++;
                      } //fin for colegios


                      ?>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

    </div>
  </div>
</div>




<!--modal agregar rol-->
<div class="modal " id="modaladdrol" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Nuevo Rol</h4>
      </div>

      <div class="modal-body">
        <form id="formaddrol" autocomplete="on" method="post" class="form-horizontal form-label-left">

            <input type="hidden" id="colegioId"/>
            <input type="hidden" id="idrol"/>

            <div class="form-group">
                <label class="col-sm-2 control-label">Nombre</label><font color="red">&nbsp;*</font>
                <div class="col-sm-8"><input type="text" class="form-control" name="nombre" id="nombre" onkeyup="javascript:this.value=this.value.toUpperCase();" required></div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Alias Masculino</label>
                <div class="col-sm-8"><input type="text" class="form-control" name="alias1" id="alias1"></div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Alias Femenino</label>
                <div class="col-sm-8"><input type="text" class="form-control" name="alias2" id="alias2"></div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Descripci&oacute;n</label>
                <div class="col-sm-8"><input type="text" class="form-control" name="desc" id="desc"></div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Orden</label>
                <div class="col-sm-8"><input type="number" class="form-control" name="orden" id="orden"></div>
            </div>

        </form>
      </div>

      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button class="btn btn-primary" id ="guardar_rol" onclick="add_rol()">Guardar</button>
      </div>

    </div>
  </div>
</div>


<!--modal editar rol-->
<div class="modal" id="modaleditrol" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Editar Rol</h4>
      </div>

      <div class="modal-body">
        <form id="formeditrol" autocomplete="on" method="post" class="form-horizontal form-label-left">

            <input type="hidden" id="colegioId"/>
            <input type="hidden" id="idrol"/>

            <div class="form-group">
                <label class="col-sm-2 control-label">Nombre</label><font color="red">&nbsp;*</font>
                <div class="col-sm-8"><input type="text" class="form-control" name="nombre" id="nombre" required></div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Alias Masculino</label>
                <div class="col-sm-8"><input type="text" class="form-control" name="alias1" id="alias1"></div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Alias Femenino</label>
                <div class="col-sm-8"><input type="text" class="form-control" name="alias2" id="alias2"></div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Descripci&oacute;n</label>
                <div class="col-sm-8"><input type="text" class="form-control" name="desc" id="desc"></div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Orden</label>
                <div class="col-sm-8"><input type="number" class="form-control" name="orden" id="orden"></div>
            </div>

        </form>
      </div>

      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button class="btn btn-primary" id ="editar_rol" onclick="edit_rol()">Guardar</button>
      </div>

    </div>
  </div>
</div>

 <!--modal menu acciones-->
 <div class="modal " id="modalmenuacciones" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Permisos</h4>
        </div>

        <div class="modal-body">
          <form id="formreglascom_<?=$idcole?>" autocomplete="on" method="post" class="form-horizontal form-label-left">

              <div class="panel-body table-responsive">

                  <input type="hidden" class="id_group">
                  <input type="hidden" class="id_cole">
                  <!--<input type="hidden" class="campo">-->

                  <table class="table table-striped table-bordered dt-responsive nowrap" id="tablamenuacciones">
                      <thead>
                        <th></th>
                        <th>Nombre</th>
                        <th>Descripci&oacute;n</th>
                        <th>Tipo</th>
                        <!--<th>Descripci&oacute;n</th>
                        <th>Orden</th>-->
                      </thead>
                      <tbody id="menuacciones_body">
                        <?
                        //$cont=1;
                        if($menu_acciones){
                          foreach ($menu_acciones as $menu)
                          {
                                //print_r($mat); die();
                                $id = $menu->id_menu;
                                $nombre = $menu->nombre;
                                $descripcion = $menu->descripcion;
                                $tipo = $menu->tipo;
                                $id_padre_menu = $menu->id_padre_menu;
                                ?>

                                <tr id='rowmenuacciones_<?echo $id?>'>
                                    <td>
                                      <div align="center" class="custom-control custom-checkbox">
                                          <input type="checkbox" class="custom-control-input checkmenu" name="" value="<?=$id?>" id="idmenuaccion_<?=$id?>">
                                      </div>
                                    </td>
                                    <td class="nombre"><?=$nombre?></td>
                                    <td class="alias1"><?=$descripcion?></td>
                                    <td class="alias2"><?=$tipo?></td>
                                </tr>


                                <?
                                //$cont++;
                              }
                        }
                        ?>
                      </tbody>
                  </table>


              </div>

          </form>

        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button class="btn btn-primary" id ="guardar_menuacciones" onclick="add_menuacciones()">Agregar</button>
        </div>

      </div>
    </div>
  </div>