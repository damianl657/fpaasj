<?//include("application/views/include/vista_inc_header.php"); ?>
<?php //$this->load->view("include/vista_inc_header"); 
 ini_set("display_errors", "1");
  error_reporting(E_ALL);
?>

<?php 
    $optionesDias="";
    $optionesHorario="";

    foreach ($dias as $dia )
    {
      $optionesDias .= "<option value='".$dia->id."'>" .$dia->nombre. "</option>" ; //anio_id 
    }

    foreach ($horarios as $horario )
    {
      $arre=array();
      $arre = explode(':',$horario->hora_inicio);
      $horainicio = $arre[0].':'.$arre[1];
    
      $arre = explode(':',$horario->hora_fin);
      $horafin = $arre[0].':'.$arre[1];

      $nombre = $horainicio.' - '.$horafin;

      $optionesHorario .= "<option value='".$horario->id."'>" .$nombre. "</option>" ; //anio_id 
    }


?>

<style type="text/css">

  .td_hover {
    background-color: #ccc;
    border: 1px solid black;
    border-top: 2px solid black;
    cursor: pointer;
  }


</style>

<script type="text/javascript">

edita_calendario = 0;



 passApiKey = '<?php echo $passApiKey; ?>';
 urlApi = '<?php echo $urlApi; ?>';

 idUser = '<?php echo $idusuario; ?>';
 idcolegio = '<?php echo $idcolegio; ?>';
 idgrupo = '<?php echo $idgrupo; ?>';
 nombregrupo = '<?php echo $nombregrupo; ?>';
 ordengrupo = '<?php echo $ordengrupo; ?>';
 edita_calendario ='<?php $band = 0;
 foreach ($this->session->userdata('menu') as $accion) //validar permiso de controlador y metodo
  {
    if(($accion->nombre == 'editar_calendario'))
    {
      echo $band = 1; 
    }
  }?>';

function traer_docente_materia(nodoanioid, divisionid)
{
    var url = urlApi+"/usuario/obtener_usuariosXrol"; 
    $.ajax({
          url: url,
          type: 'GET',
          headers: {              
              'APIKEY' : passApiKey,
              'userid' : idUser,
              'Access-Control-Allow-Origin': '*'
          },
          data:{idcolegio:idcolegio, nombregrupo:'Docente'},
          success: function(docentes){  
            if(docentes['status']==false)
            { 
              if(docentes['message']=='NO LOGUIN')
                location.href = "<?php echo base_url('login'); ?>"; 
              else
                bootbox.alert("No hay docentes cargados");
            }
            else
            { 
                  var docentes = jQuery.parseJSON( docentes );  
                 
                  
                  url = urlApi+"/materia/anios_materia"; 
                  $.ajax({
                        url: url,
                        type: 'GET',
                        headers: {              
                            'APIKEY' : passApiKey,
                            'userid' : idUser,
                            'Access-Control-Allow-Origin': '*'
                        },
                        data:{idcolegio:idcolegio, nodoanioid:nodoanioid},
                        success: function(data){  
                          if(data['status']==false)
                          { 
                            if(data['message']=='NO LOGUIN')
                              location.href = "<?php echo base_url('login'); ?>"; //no està 
                            else 
                              bootbox.alert("No hay materias cargadas para el año seleccionado");
                          }
                          else
                          { 
                                var materias = jQuery.parseJSON( data );  
                                //console.log(materias);
                                $(".casilla").unbind("click" );
                                $(".nombremateria_link").unbind("click" ); 



                                $('.casilla').click(function(e){
                                    e.stopPropagation();
                                    var tdid=$(this).attr('id');
                                    var arre=Array();
                                    arre = tdid.split('_');
                                    var idhorario = arre[0];
                                    var iddia = arre[1];

                                    if(edita_calendario == 1) 
                                    {    
                                        abrir_dialog(idhorario, iddia, docentes, materias, divisionid, nodoanioid);
                                    }
                                    else {
                                          console.log("no tiene permisos para cargar un horario");

                                        }
                                }); 


                                $( '.nombremateria_link').click(function(e){
                                    //alert('click');
                                    e.stopPropagation();

                                    var idmatlink= $(this).attr('id');

                                    var arre=Array();
                                    arre = idmatlink.split('_');
                                    var idhormat = arre[1];

                                    if(edita_calendario == 1) 
                                    {    
                                        abrir_dialog_detalle( docentes, materias, idhormat, nodoanioid);
                                    }
                                    else {
                                           console.log("no tiene permisos para editar un horario");

                                        }
                                });    

                                
                            
                         }                             
                      },
                      error: function(response){
                          console.log(response);                   
                      }           
                  }); 

              
            }                             
          },
          error: function(response){
              console.log(response);                   
          }           
    }); 
}

function traer_docente_materia2(nodoanioid)
{
    var url = urlApi+"/usuario/obtener_usuariosXrol"; 
    $.ajax({
          url: url,
          type: 'GET',
          headers: {              
              'APIKEY' : passApiKey,
              'userid' : idUser,
              'Access-Control-Allow-Origin': '*'
          },
          data:{idcolegio:idcolegio, nombregrupo:'Docente'},
          success: function(docentes){  
            if(docentes['status']==false)
            { 
              if(docentes['message']=='NO LOGUIN')
                location.href = "<?php echo base_url('login'); ?>"; 
            }
            else
            { 
                  var docentes = jQuery.parseJSON( docentes );  
                  //console.log(docentes);
                  
                  url = urlApi+"/materia/anios_materia"; 
                  $.ajax({
                        url: url,
                        type: 'GET',
                        headers: {              
                            'APIKEY' : passApiKey,
                            'userid' : idUser,
                            'Access-Control-Allow-Origin': '*'
                        },
                        data:{idcolegio:idcolegio, nodoanioid:nodoanioid},
                        success: function(data){  
                          if(data['status']==false)
                          { 
                            if(data['message']=='NO LOGUIN')
                              location.href = "<?php echo base_url('login'); ?>"; //no està 
                          }
                          else
                          { 
                                var materias = jQuery.parseJSON( data );  
                                //console.log(materias);

                                $(".nombremateria_link").unbind("click" ); 

                                $( '.nombremateria_link').click(function(e){
                                    //alert('click');
                                    e.stopPropagation();

                                    var idmatlink= $(this).attr('id');

                                    var arre=Array();
                                    arre = idmatlink.split('_');
                                    var idhormat = arre[1];

                                    if(edita_calendario == 1) 
                                    {    
                                        abrir_dialog_detalle( docentes, materias, idhormat, nodoanioid );
                                    }
                                    else {
                                          console.log("no tiene permisos para editar un horario");
                                        }
                                });    

                                
                            
                         }                             
                      },
                      error: function(response){
                          console.log(response);                   
                      }           
                  }); 

              
            }                             
          },
          error: function(response){
              console.log(response);                   
          }           
    }); 
}


function guardar_horario(newH, nodoanioid){ 
               
    bootbox.prompt({
      title: "Ingrese su PIN",
      value: "", 
      inputType: "password",
      maxlength: 4,
      callback: function(pin) {
                    
        if (pin === null) {                                             
           return true; 
        } else {   
              var newhorario  = JSON.stringify(newH);
           
              var urlRequest = urlApi+"/horario/inserthorarios_materias";
              $.ajax({
                    url: urlRequest,
                    type: 'POST',
                    headers: {    
                    'APIKEY' : passApiKey,
                    'userid' : idUser
                  },
                  data: {data: newhorario, pin:pin}, 
                  beforeSend: function() {
                      $('.btn-success').after("<img id='loadingNiv' src='<?=base_url('assets/images/loading.gif');?>' height='30' width='30' />");
                  },
                  success: function(data){
                         
                        //console.log(data);
                        $('#loadingNiv').remove(); 

                        if(data['status']==false)
                        {  //alert(data['message']);
                            if(data['message'] == 'NO LOGUIN')                                        
                                location.href = "<?php echo base_url('login'); ?>";  
                            else  if(data['message'] == 'pininvalido')
                                  {    //alert('PIN INVALIDO');
                                      bootbox.alert("PIN INVALIDO!", function() { });
                                   return false;
                                  }
                        }
                        else{    
                                bootbox.hideAll();

                                var datos = jQuery.parseJSON( data['datos'] );
                                

                                if( datos[0]['first_name']==null )
                                    var nombredocente = "";
                                else var nombredocente = datos[0]['last_name']+' '+datos[0]['first_name'];

                                var divmateria = '<div align="center" class="divcasilla" id="divcasilla_'+datos[0]['id']+'">'+
                                      '<h4><a class="nombremateria_link" id="nombremateria_'+datos[0]['id']+'"><b>'+datos[0]['materia'] +'</b></a> <button type="button" class="bootbox-close-button close btn-sm" data-dismiss="modal" aria-hidden="true" style="display:none">×</button></h4>'+
                                      '<i>'+nombredocente+'</i>'+
                                 '</div>';

                                var celda=$('#tabladatos tbody').find('[id="'+datos[0]['horarios_id']+'_'+datos[0]['dias_id']+'"]');

                                $(celda).append(divmateria);

                                $(".divcasilla").mouseover(function() {
                                  var close = $(this).find('.close');
                                  $(close).css('display','block');
                                });

                                $(".divcasilla").mouseout(function() {
                                    var close = $(this).find('.close');
                                    $(close).css('display','none');
                                });

                                $('.close').click(function(e){
                                    e.stopPropagation();
                                    var div = $(this).parent('h4').parent('div');
                                    var divid = $(div).attr('id');
                                    var arre=Array();
                                    arre = divid.split('_');
                                    var divid = arre[1];

                                    if(edita_calendario == 1) 
                                    {    
                                      console.log(divid);
                                        deletehorario(divid);
                                    }
                                    else {
                                          console.log("no tiene permisos para eliminar un horario");
                                        }
                                }); 

                                traer_docente_materia2(nodoanioid);

                                $('.top-right').notify({
                                  message: { text: 'Horario Guardado' },
                                  fadeOut: { enabled: true, delay: 5000 },
                                  type: 'success2'
                                }).show(); 
                            }
                                     
                  },

                  error: function(response){
                      console.log(response);
                      $('#loadingNiv').remove(); 
                      bootbox.hideAll();               
                  }

              });                   
      
                
          }
      }
    });//fin prompt
} 

function modificar_horario(parametros, idhormat, nodoanioid){

    var  parametros2 = parametros;
    var  idhormat2 = idhormat;
              
    bootbox.prompt({
      title: "Ingrese su PIN",
      value: "", 
      inputType: "password",
      maxlength: 4,
      callback: function(pin) 
      {
                    
        if (pin === null) {                                             
           return true; 
        } 
        else 
        {   
              var parametros  = JSON.stringify(parametros2);
              
              var urlRequest = urlApi+"/horario/update_horarios_materias";
              $.ajax({
                    url: urlRequest,
                    type: 'POST',
                    headers: {    
                    'APIKEY' : passApiKey,
                    'userid' : idUser
                  },
                  data: {data: parametros, pin:pin, idhormat:idhormat}, 
                  beforeSend: function() {
                      $('.btn-success').after("<img id='loadingNiv' src='<?=base_url('assets/images/loading.gif');?>' height='30' width='30' />");
                  },
                  success: function(data){
                         
                        //console.log(data);
                        $('#loadingNiv').remove(); 

                        if(data['status']==false)
                        {  //alert(data['message']);
                            if(data['message'] == 'NO LOGUIN')                                        
                                location.href = "<?php echo base_url('login'); ?>";  
                            else  if(data['message'] == 'pininvalido')
                                  {    //alert('PIN INVALIDO');
                                      bootbox.alert("PIN INVALIDO!", function() { });
                                   return false;
                                  }
                        }
                        else{    
                                bootbox.hideAll();
                               
                                
                                var datos = jQuery.parseJSON( data['datos'] );
                                
                                $('#divcasilla_'+idhormat2).remove();

                                if( datos[0]['first_name']==null )
                                    var nombredocente = "";
                                else var nombredocente = datos[0]['last_name']+' '+datos[0]['first_name'];

                                var divmateria = '<div align="center" class="divcasilla" id="divcasilla_'+idhormat2+'">'+
                                      '<h4><a class="nombremateria_link" id="nombremateria_'+idhormat2+'"><b>'+datos[0]['materia'] +'</b></a>'+
                                        '<button type="button" class="bootbox-close-button close btn-sm" data-dismiss="modal" aria-hidden="true" style="display:none">×</button>'+
                                      ' </h4>'+
                                      '<i>'+nombredocente+'</i>'+
                                 '</div>';

                                var celda=$('#tabladatos tbody').find('[id="'+datos[0]['horarios_id']+'_'+datos[0]['dias_id']+'"]');

                                $(celda).append(divmateria);

                                $(".divcasilla").mouseover(function() {
                                  var close = $(this).find('.close');
                                  $(close).css('display','block');
                                });

                                $(".divcasilla").mouseout(function() {
                                    var close = $(this).find('.close');
                                    $(close).css('display','none');
                                });

                                $('.close').click(function(e){
                                    e.stopPropagation();
                                    var div = $(this).parent('h4').parent('div');
                                    var divid = $(div).attr('id');
                                    var arre=Array();
                                    arre = divid.split('_');
                                    var divid = arre[1];

                                    if(edita_calendario == 1) 
                                    {    
                                      console.log(divid);
                                        deletehorario(divid);
                                    }
                                    else {
                                          console.log("no tiene permisos para eliminar un horario");
                                        }
                                });

                                 traer_docente_materia2(nodoanioid); 

                                 $('.top-right').notify({
                                  message: { text: 'Horario Modificado' },
                                  fadeOut: { enabled: true, delay: 5000 },
                                  type: 'success2'
                                }).show(); 

                            }
                                     
                  },

                  error: function(response){
                      console.log(response);
                      $('#loadingNiv').remove(); 
                      bootbox.hideAll();               
                  }

              });                   
      
                
          }
      }
    });//fin prompt
} 
 
function deletehorario(id){
              
    bootbox.prompt({
      title: "Seguro que quiere borrar horario? Ingrese su PIN para confirmar:",
      value: "", 
      inputType: "password",
      maxlength: 4,
      callback: function(pin) 
      {                 
        if (pin === null) {                                             
           return true; 
        } 
        else 
        {     
              var urlRequest = urlApi+"/horario/delete_horarios_materias";
              $.ajax({
                    url: urlRequest,
                    type: 'POST',
                    headers: {    
                    'APIKEY' : passApiKey,
                    'userid' : idUser
                  },
                  data: {horarioid: id, pin:pin}, 
                  beforeSend: function() {
                      /*$('.btn-success').after("<img id='loadingNiv' src='<=base_url('assets/images/loading.gif');?>' height='30' width='30' />");*/
                  },
                  success: function(data){
                        console.log(data);
                        $('#loadingNiv').remove(); 

                        if(data['status']==false)
                        {  //alert(data['message']);
                            if(data['message'] == 'NO LOGUIN')                                        
                                location.href = "<?php echo base_url('login'); ?>";  
                            else  if(data['message'] == 'pininvalido')
                                  {    //alert('PIN INVALIDO');
                                      bootbox.alert("PIN INVALIDO!", function() { });
                                   return false;
                                  }
                        }
                        else{   
                                $('#divcasilla_'+id).remove();

                                 $('.top-right').notify({
                                  message: { text: 'Horario Eliminado' },
                                  fadeOut: { enabled: true, delay: 5000 },
                                  type: 'success2'
                                }).show(); 

                            }
                                     
                  },

                  error: function(response){
                      console.log(response);
                      $('#loadingNiv').remove(); 
                  }

              });                   
      
                
          }
      }
    });//fin prompt
} 

function retorna_formularioaltahorario (horario, dia)
{
  var texto=  
              '<form role="form" id="Nhorario" name="Nhorario">'+
                
                '<div id="DivDatos">'+  
                 //div de la izquierda
                '<div class="col-sm-12 b-r">'+  

                      '<div class="form-group" id="div_select_dia"> ' +
                        '<label>Dia</label>' +
                        '<select id="dia" name="dia" class="form-control input-md" required/> *' +
                      '</div>'+

                      '<div class="form-group" id="div_select_horario"> ' +
                        '<label>Horario</label>' +
                        '<select id="horario" name="horario" class="form-control input-md" required/> *' +
                      '</div>'+

                      '<div class="form-group" id="div_select_materia"> ' +
                        '<label>Materia</label>' +

                        '<select id="materia" name="materia" class="form-control input-md"  required/> *' +

                      '</div>'+

                       '<div class="form-group" id="div_select_docente"> ' +
                        '<label>Docente</label>' +
                        '<select id="docente" name="docente" class="form-control input-md" />' +
                      '</div>'+

                      '<div class="form-group"><label>Lugar</label> <input type="text" placeholder="Ingrese Aula" class="form-control" id="lugar" name="lugar"/></div>'+

                '</div>'+
              
              '</div>' + //fin DivFiltro
            '</form>';
                            
        return texto; 
}

function abrir_dialog(horario, dia, docentes, materias, divisionid, nodoanioid)
{ 

  bootbox.dialog({
              backdrop: true,
              title: "Cargar Horario...",
              message:  
                  '<div class="row ">'+
                    '<div class="col-lg-12">'+
                        '<div class="ibox collapsed float-e-margins" id="ibox_collapsed">'+
                            '<div class="ibox-content">'+
                                '<div class="row">'+
                                    retorna_formularioaltahorario() +
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                  '</div>',
              buttons: {
                  success: {
                      label: "Guardar",
                      className: "btn-success",
                      callback: function (e) {  

                          var $myForm = $('#Nhorario');

                          var selectmat = $('#materia').val();

                          if( ($myForm[0].checkValidity()) && (selectmat!=-1) ) 
                          {
                            var horario = $('#horario').val();
                            var dia = $('#dia').val();
                            var docente = $('#docente').val();
                            var materia = $('#materia').val();
                            var lugar = $('#lugar').val();

                            var parametros = {
                              'horarios_id': horario,
                              'dias_id': dia,     
                              'docente_id': docente,    
                              'materias_id': materia, 
                              'lugar': lugar,
                              'divisiones_id':divisionid,
                              'colegio_id':idcolegio,

                            };
                            
                            guardar_horario(parametros, nodoanioid); 
                            return false;
                          }
                          else {
                                bootbox.alert("Ingrese los campos obligatorios");
                                return false;
                                }
                                                                 
                      }//fin calback
                  }//fin success
              },//fin Buttons

              onEscape: function() {return ;},
        });  //FIN DIALOG


    /*movimientos dentro del dialog guardar horario.......*/
    $('.ibox-content').css('display','block');

    
    
    var optiondias="<?php echo $optionesDias; ?>";
    $('#dia').append(optiondias); 
      
    var optionhorario="<?php echo $optionesHorario; ?>";
    $('#horario').append(optionhorario); 
  

    var opcion = "<option value='-1'>Seleccione una materia</option>" ; //anio_id 
    $('#materia').append(opcion); 
    for(var j=0; j < materias.length; j++) 
    {
      $options = $('#div_select_materia').find('option');
      $targetOption = $options.filter(
         function () {return jQuery(this).val() == materias[j]['id']}
      );//esto es para buscar un option por text. manso lio pero no hay otra.                         
      
      if($targetOption.length == 0)
      {   
          opcion = "<option value='"+materias[j]['id']+"'>" +materias[j]['nombre']+ "</option>" ; //anio_id 
          $('#materia').append(opcion); 
      }
    }

    opcion = "<option value='-1'>Seleccione un docente</option>" ; //anio_id 
    $('#docente').append(opcion); 
    for(var j=0; j < docentes.length; j++) 
    {
      $options = $('#div_select_docente').find('option');
      $targetOption = $options.filter(
         function () {return jQuery(this).val() == docentes[j]['id']}
      );//esto es para buscar un option por text. manso lio pero no hay otra.                         
      
      if($targetOption.length == 0)
      {   
          var nombre = docentes[j]['last_name'] +' '+ docentes[j]['first_name'];
          var opcion = "<option value='"+docentes[j]['id']+"'>" +nombre+ "</option>" ; //anio_id 
          $('#docente').append(opcion); 
      }
    }

    $("#dia").val(dia);
    $("#horario").val(horario);
    
    $( "#dia" ).select2({
     
    });
    $('#s2id_dia').css('display','block');

    $( "#horario" ).select2({
     
    });
    $('#s2id_horario').css('display','block');

    $( "#docente" ).select2({
    
    });
    $('#s2id_docente').css('display','block');

    $( "#materia" ).select2({
      
    });
    $('#s2id_materia').css('display','block');
}

function abrir_dialog_detalle( docentes, materias, idhormat, nodoanioid)
{ 
  url = urlApi+"/horario/horarioXid"; 
  $.ajax({
      url: url,
      type: 'GET',
      headers: {              
          'APIKEY' : passApiKey,
          'userid' : idUser,
          'Access-Control-Allow-Origin': '*'
      },
      data:{id:idhormat},
      success: function(data){  
        if(data['status']==false)
        { 
          if(data['message']=='NO LOGUIN')
            location.href = "<?php echo base_url('login'); ?>"; //no està 
        }
        else
        { 
            var data = jQuery.parseJSON( data );
            //console.log(data);

            bootbox.dialog({
                  backdrop: true,
                  title: "Modificar Horario...",
                  message:  
                      '<div class="row ">'+
                        '<div class="col-lg-12">'+
                            '<div class="ibox collapsed float-e-margins" id="ibox_collapsed">'+
                                '<div class="ibox-content">'+
                                    '<div class="row">'+
                                        retorna_formularioaltahorario() +
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                      '</div>',
                  buttons: {
                      success: {
                          label: "Guardar",
                          className: "btn-success",
                          callback: function (e) {  

                              var $myForm = $('#Nhorario');
                              if ($myForm[0].checkValidity()) 
                              {
                                var horario = $('#horario').val();
                                var dia = $('#dia').val();
                                var docente = $('#docente').val();
                                var materia = $('#materia').val();
                                var lugar = $('#lugar').val();

                                var parametros = {
                                  'horarios_id': horario,
                                  'dias_id': dia,     
                                  'docente_id': docente,    
                                  'materias_id': materia, 
                                  'lugar': lugar
                                };
                                
                                modificar_horario(parametros, idhormat, nodoanioid); 
                                return false;
                              }
                              else {
                                    bootbox.alert("Ingrese los campos obligatorios");
                                    return false;
                                    }
                                                                     
                          }//fin calback
                      }//fin success
                  },//fin Buttons

                  onEscape: function() {return ;},
            });  //FIN DIALOG

            //movimientos dentro del dialog guardar evento......./
            $('.ibox-content').css('display','block');
            
            
            var optiondias="<?php echo $optionesDias; ?>";
            $('#dia').append(optiondias); 
              
            var optionhorario="<?php echo $optionesHorario; ?>";
            $('#horario').append(optionhorario); 
          

            var opcion = "<option value='-1'>Seleccione una materia</option>" ; //anio_id 
            $('#materia').append(opcion); 
            for(var j=0; j < materias.length; j++) 
            {
              $options = $('#div_select_materia').find('option');
              $targetOption = $options.filter(
                 function () {return jQuery(this).val() == materias[j]['id']}
              );//esto es para buscar un option por text. manso lio pero no hay otra.                         
              
              if($targetOption.length == 0)
              {   
                  opcion = "<option value='"+materias[j]['id']+"'>" +materias[j]['nombre']+ "</option>" ; //anio_id 
                  $('#materia').append(opcion); 
              }
            }

            opcion = "<option value='-1'>Seleccione un docente</option>" ; //anio_id 
            $('#docente').append(opcion); 
            for(var j=0; j < docentes.length; j++) 
            {
              $options = $('#div_select_docente').find('option');
              $targetOption = $options.filter(
                 function () {return jQuery(this).val() == docentes[j]['id']}
              );//esto es para buscar un option por text. manso lio pero no hay otra.                         
              
              if($targetOption.length == 0)
              {   
                  var nombre = docentes[j]['last_name'] +' '+ docentes[j]['first_name'];
                  var opcion = "<option value='"+docentes[j]['id']+"'>" +nombre+ "</option>" ; //anio_id 
                  $('#docente').append(opcion); 
              }
            }

            $("#dia").val(data[0]['dias_id']);
            $("#horario").val(data[0]['horarios_id']);
            $("#materia").val(data[0]['nombresmateria_id']);
            if(data[0]['docente_id'] != null)
              $("#docente").val(data[0]['docente_id']);
            $("#lugar").val(data[0]['lugar']);

            
            $( "#dia" ).select2({
              theme: "bootstrap",
              placeholder: "Seleccione los usuarios",
              //allowClear: true
            });
            $('#s2id_dia').css('display','block');

            $( "#horario" ).select2({
              theme: "bootstrap",
              placeholder: "Seleccione los roles",
              allowClear: true
            });
            $('#s2id_horario').css('display','block');

            $( "#docente" ).select2({
              theme: "bootstrap",
              placeholder: "Seleccione los niveles",
              allowClear: true
            });
            $('#s2id_docente').css('display','block');

            $( "#materia" ).select2({
              theme: "bootstrap",
              placeholder: "Seleccione los niveles",
              allowClear: true
            });
            $('#s2id_materia').css('display','block');


        }                             
      },
      error: function(response){
          console.log(response);                   
      }           
  }); 
}


function completar_calendario(divisionid, nodoanioid)
{
      var url = urlApi+"/horario/horariosXdivision";   
      
      $.ajax({
            url: url,
            type: 'GET',
            headers: {              
                'APIKEY' : passApiKey,
                'userid' : idUser,
                'Access-Control-Allow-Origin': '*'
            },
            data:{divisionid:divisionid},
            beforeSend: function() {
              $('#calendar').append("<img id='loadingNiv' src='<?=base_url('assets/images/loading.gif');?>' height='50' width='50' />");
            },
            success: function(data){  
              
              if( (data['status']==false) && (data['message']=='NO LOGUIN') )
              { 
                //console.log(data['message']);
                if(data['message']=='NO LOGUIN')
                  location.href = "<?php echo base_url('login'); ?>"; //no està logueado
                /*else
                  if(data['message']=='vacio')
                      bootbox.alert("no hay materias cargadas", function() { });*/
              }
              else
              { console.log(data);
                
                if(data['message']!='vacio')
                { var data = jQuery.parseJSON( data );
                  for (var j=0; j < data.length; j++ )
                  {          
                    var hora = data[j]['horarios_id'];
                    var dia = data[j]['dias_id'];

                    if( data[j]['first_name']==null )
                      var nombredocente = "";
                    else var nombredocente = data[j]['first_name']+' '+data[j]['last_name'];
                              
                    var divmateria = '<div align="center" class="divcasilla" id="divcasilla_'+data[j]['id']+'">'+
                                          '<h4><a class="nombremateria_link" id="nombremateria_'+data[j]['id']+'"><b>'+data[j]['materia'] +'</b></a> <button type="button" class="bootbox-close-button close btn-sm" data-dismiss="modal" aria-hidden="true" style="display:none">×</button></h4>'+
                                          '<i>'+nombredocente+'</i>'+
                                     '</div>';

                    var celda=$('#tabladatos tbody').find('[id="'+data[j]['horarios_id']+'_'+data[j]['dias_id']+'"]');

                    $(celda).append(divmateria);
                  }
                }

                $(".divcasilla").mouseover(function() {
                  var close = $(this).find('.close');
                  $(close).css('display','block');
                });

                $(".divcasilla").mouseout(function() {
                    var close = $(this).find('.close');
                    $(close).css('display','none');
                });

                $('.close').click(function(e){
                    e.stopPropagation();
                    var div = $(this).parent('h4').parent('div');
                    var divid = $(div).attr('id');
                    var arre=Array();
                    arre = divid.split('_');
                    var divid = arre[1];

                    if(edita_calendario == 1) 
                    {    
                      console.log(divid);
                        deletehorario(divid);
                    }
                    else {
                          console.log("no tiene permisos para eliminar un horario");
                        }
                }); 

                traer_docente_materia(nodoanioid, divisionid);

              } 

              $('#loadingNiv').remove();                            
          },
          error: function(response){
              console.log(response); 
               $('#loadingNiv').remove();                  
          }           
      }); 
}

$(document).ready(function()
{

    $('.select2').select2({
         theme: "bootstrap",
         placeholder: "Seleccione division",
        allowClear: true
    }); 

    $('#btn_alumnos').click(function(e){
        e.preventDefault();

        var diviid = $('#division_id').val();
        var arre = Array();
        arre=diviid.split('_');

        $('#tabladatos tbody').find('.divcasilla').remove()
        
        $(".casilla").unbind("click" ); //importante elimino el evento click.
        $(".nombremateria_link").unbind("click" ); //importante elimino el evento click.

        
        completar_calendario(arre[0], arre[1]); //id division, id nodoanio

        if(edita_calendario==1)
        {
          $(".casilla").mouseover(function() {
            $(this).addClass("td_hover");
          });

          $(".casilla").mouseout(function() {
               $(this).removeClass("td_hover");
          });
        }

    });


    $('.pull-down').css('margin-bottom', '-10px');

});

</script>

    <!-- Comienzo HTML -->     
     <div class="wrapper wrapper-content animated fadeInRight">        
         
   <!-- Funcionalidad -->     
    <div class="row">
        <div class="col-lg-12">


            <div class="ibox-content">
                <div class="row">      
                    <div id='calendar'>
                

                      <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title collapse-link2">
                                <h5>HORARIOS POR DIVISION</h5>
                                <div class="ibox-tools">
                                    <a class="">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>

                            
                            <div class="ibox-content">
                              <div class="row">
                                  <div class="col-sm-6">
                                      <?php $attributes = array('id' => 'formulario', 'class' => 'form');
                                                      echo form_open('alumno/alumnos_detaill',$attributes); ?>
                                    <div class="form-group ">
                                          <label>Seleccione una Divisi&oacute;n: </label>
                                    </div>
                                    <div class="form-group ">
                                      <?php  //print_r($divisiones); ?>
                                        <!--<table cellspacing="10"><tr><td>-->
                                        <select name="divisiones" id="division_id" class="select2 form-control">
                                         <?php 
                                        foreach($divisiones as $key)
                                        {
                                          if(isset($key->nombre_especialidad)){ ?>
                                            <option value="<?php echo $key->division_id.'_'.$key->nodoanios_id ?>"><?php echo $key->nombre_nivel." ".$key->nombre_especialidad."  ".$key->nombre_anio ?></option>
                                          <?php
                                         }else{ ?>
                                          <option value="<?php echo $key->division_id.'_'.$key->nodoanios_id ?>"><?php echo $key->nombre_nivel." ".$key->nombre_anio." ".$key->nombre_division ?></option><?php 
                                         }
                                        } ?> 
                                        </select>
                                        </td><td>
                                       
                                        <!--</td></tr></table>-->
                                    </div> 

                                    <?php echo form_close( ); ?>
                                    
                                  </div>

                                  <div class="col-sm-6" style="margin-top:35px">
                                    <button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="btn_alumnos" type="button"><strong>Listar</strong></button>
                                  </div>  
                                  

                                </div>
                            </div>  




                            <!--TABLA DATOS-->          
                            <div class="ibox-content">

                                <table class="table table-striped" id="tabladatos">
                                  <thead>
                                    <tr id="cabeceratabla">
                                      <th  rowspan="2"><div align="center">HORA</div></th>
                                      <th  colspan="10"><div align="center">DIAS</div></th>
                                    </tr> 
                                    <?php 
                                    $textodias='<tr>';
                                    foreach($dias as $dia)
                                    {
                                        $textodias .= '<td id="'.$dia->id.'"><div align="center">'.$dia->nombre.'</div></td>';
                                    }               
                                    $textodias .= '</tr>';
                                    echo $textodias;
                                    ?>
                                   
                                  </thead>
                                  <tbody>

                                    <?php

                                    foreach($horarios as $horario)
                                    {
                                      
                                        $fila='<tr>';
                                        $horaid = $horario->id;
                                        $horainicio = $horario->hora_inicio;
                                        $horafin = $horario->hora_fin;
                                        

                                        $arre=array();
                                        $arre = explode(':',$horainicio);
                                        $horainicio = $arre[0].':'.$arre[1];
                                      
                                        $arre = explode(':',$horafin);
                                        $horafin = $arre[0].':'.$arre[1];

                                        $fila = $fila . '<td id="'.$horaid.'" ><div align="center"><b>'.$horario->numero.'&deg; </b></div><div align="center">'.$horainicio.' - '.$horafin.'</div></td>';

                                        foreach($dias as $dia)
                                        {
                                            $diaid = $dia->id;
                                            $fila = $fila . '<td id="'.$horaid.'_'.$diaid.'" class="casilla" ></td>';  

                                            //<a href="#" class="pull-right" style="position: relative; bottom: 0; right: 0;" ><i class="fa fa-plus-circle" ></i></a>
                                        }

                                        $fila = $fila .'</tr>';  

                                        echo $fila;
                                    }
                                    ?>  

                                  </tbody>
                                </table>
                            </div>
                        </div>
                      </div>


                    </div> 
                </div>
            </div>
        </div>
    </div>        
    <br>
            
        
  </div>  

  
<style type="text/css">

  #calendar {
    max-width: 100%;
    margin: 0 auto;
  }

  
  .row2{
    min-height: 411px;
  }

  select
  {
    min-width: 100px;
  }

</style>

<?php //$this->load->view("include/vista_inc_pie"); ?>
