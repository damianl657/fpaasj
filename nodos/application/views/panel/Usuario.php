<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';

    class Usuario extends REST_Controller {

    function __construct()
    {        
        parent::__construct();
        $this->load->model("modelo_usuario");
    }

    /**
     * Recibe el token gcm (google cloud message) y lo guarda en la bd
     * @param  $registerid_gcm
     * @return boolean true/false
     */
    public function gcmToken_post()
    {
        //verifico que el usuario
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {   //si el usuario es correcto     
            $registerid_gcm = $this->input->post("registerid_gcm");
            // Si no vienen seteado
            if ((empty($registerid_gcm)))
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Debe proveer el id de registro del Google Cloud Message'
                ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code            
            }
            else
            {// Si vienen los datos
                //me fijo si ya existe el token y el usuario
                if($this->modelo_usuario->existe_registerid_gcm($userid, $registerid_gcm)){
                    // si ya existe no hago nada y devuelvo OK
                    $message = [
                        'satus' => TRUE,
                        'message' => 'El token gcm ya existe'
                    ];
                    $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
                }
                else
                {   //Si no existe token y usuario procedo a guardar
                    if ($this->modelo_usuario->guardar_registerid_gcm($userid, $registerid_gcm))// gcm = google cloud message
                    {
                        // Se guardo correctamente
                        $message = [
                            'satus' => TRUE,
                            'message' => 'Id del gcm guardado'
                        ];
                        $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
                    }
                    else
                    {   //Error al guardar
                        $this->response([
                            'status' => FALSE,
                            'message' => 'El id del gcm no pudo ser guardado'
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                    }            
                }
            }
        }
    }

    public function pasos_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            
            $idusergrupo = $_GET['idusergrupo']; 

            $result = $this->modelo_usuario->obtener_pasos($idusergrupo);
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
                $fila = $result->row();  
                //$this->response($fila->pasos, REST_Controller::HTTP_OK);

                $this->response([
                            'status' => TRUE,
                            'paso' => $fila->configuracion, 
                        ], REST_Controller::HTTP_OK);  
                
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Error al consultar paso'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }

    public function obtener_roles_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $result = $this->modelo_usuario->get_rolesXidusuario($userid);
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
                $result = json_encode($result);
                $this->response($result, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron roles'
                ], REST_Controller::HTTP_OK); 
            }
        }
    }     

    
   public function obtener_usuarios_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $idcolegio = $_GET['idcolegio'];
            
            $ids_roles = json_decode($_GET['ids_roles']);
            
            if(count($ids_roles)>0)
            {    $ids_roles = join(',',$ids_roles);
                $result = $this->modelo_usuario->get_usuariosColegio2($idcolegio, $ids_roles);
            }
            else $result = $this->modelo_usuario->get_usuariosColegio2($idcolegio,"");
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
                $result = json_encode($result);
                $this->response($result, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron roles'
                ], REST_Controller::HTTP_OK); 
            }
        }
    }

    public function obtener_rol_PreceDoceYalumnos_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $arre = "'Docente','Preceptor','Alumno'";
            $result = $this->modelo_usuario->get_rolesXrolesname($arre);
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
                $result = json_encode($result);
                $this->response($result, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron roles'
                ], REST_Controller::HTTP_OK); 
            }
        }
    } 

    //SOLO ÉSTA, REVISAR LA DE ARRIBA
    public function obtener_rolesTodos_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {   
            $idgrupo = $_GET['idgrupo'];
            $idcolegio= $_GET['idcolegio'];

            $reglas = $this->modelo_usuario->get_reglas($idcolegio,$idgrupo);
            if(count($reglas) == 0)
                 $result = $this->modelo_usuario->get_rolesTodos();
            else { //print_r($reglas->roles_id);
                 $result = $this->modelo_usuario->get_rolesXrolesid($reglas->roles_id);
                    }
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
                $result = json_encode($result);
                $this->response($result, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron roles'
                ], REST_Controller::HTTP_OK); 
            }
        }
    }

    /*public function comprobar_pin_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $pin = $_POST['pin']; 
            $existe = $this->utilidades->verifica_pin($pin);//verifico que el pin ingresado por el usuario es correcto
            
            if ($existe == 1)
            { 
                $this->response([
                            'status' => TRUE,
                        ], REST_Controller::HTTP_OK); 
            }
            else
            {
                $this->response([
                'status' => FALSE,
                'message' => 'invalido'
                ], REST_Controller::HTTP_OK);  
            }
        }
    }*/
}