<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';

    class Horario extends REST_Controller {

    function __construct()
    {
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        parent::__construct();
        $this->load->model("modelo_horario");
        $this->load->model("modelo_cicloa");
        $this->load->model("modelo_horarios");
        $this->load->model("modelo_usuario");
    }

    public function obtener_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            // Hago lo que debe hacer el metodo especificamente

            $idalumno = $this->get('alumno_id'); // obtengo el id del alumno, si es que vino en la url
            $dia = $this->get('dia'); // obtengo el dia, si es que vino en la url
            if ($idalumno === NULL || $dia === NULL)
            {
                $idhorario = $this->get('horario_id');//obtengo el id del horario, si es que viene en la url
                if($idhorario === NULL)
                {
                    // Obtengo todos los horarios
                    $horario = $this->modelo_horario->obtener_horarios($idalumno); // obtiene los eventos del alumno X
                    // Si existe mas de un resultado, lo mando
                    if ($eventos)
                    {
                        $this->response($horario, REST_Controller::HTTP_OK);
                    }
                    // Sino, envio respuesta con 404
                    else
                    {
                        $this->response([
                            'status' => FALSE,
                            'message' => 'No se encontraron horarios'
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                    }                  
                }
                else
                {
                    // Obtengo el horario especifico
                    $horario = $this->modelo_horario->obtener_horario($idhorario); // obtiene el horario especifico
                    // Si existe mas de un resultado, lo mando
                    if ($horario)
                    {
                        $this->response($horario, REST_Controller::HTTP_OK);
                    }
                    // Sino, envio respuesta con 404
                    else
                    {
                        $this->response([
                            'status' => FALSE,
                            'message' => 'No se encontro el horarios con ese ID'
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                    }
                }
            }
            else
            {
                // Obtengo el horario especifico
                $horario = $this->modelo_horario->obtener_horario_por_dia($idalumno, $dia); // obtiene el horario por dia
                // Si existe mas de un resultado, lo mando
                if ($horario)
                {
                    $this->response($horario, REST_Controller::HTTP_OK);
                }
                // Sino, envio respuesta con 404
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'No se encontro el horario en el dia especificado'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                }
            }
        }
    }


    public function inserthorarios_post()
    {
        $userid = $this->utilidades->verifica_userid();
    
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $data = json_decode($_POST['data']) ;
          
            $idusergrupo = $_POST['idusergrupo']; 
            $idcolegio = $_POST['idcolegio']; 


            $this->modelo_horarios->baja_horarios($idcolegio);
      
            foreach ($data as $key ) 
            { 
                //var_dump($key);
                $res = $this->modelo_horarios->get_horario($key->hora_inicio, $key->hora_fin, $key->colegio_id); 
                
                if (!$res) //si no existe inserto                
                    $this->modelo_horarios->insertar_horario($key);
                else{ //existe
                    foreach ($res as $row )
                        if ($row->estado==0) //si existe pero fue dada de baja, vuelvo a habilitar
                        {    
                            $result = $this->modelo_horarios->update_estado_horario($row->id,1);
                            $result = $this->modelo_horarios->update_numero_horario($row->id, $key->numero);
                        }
                    } 
                
            }
            
            $this->load->model("modelo_usuario");
            $result2 = $this->modelo_usuario->update_pasos($idusergrupo,'5'); 

            if ($result2)
            {
                $this->response([
                        'status' => TRUE,
                        'message' => 'Configuracion de Horarios Completa', 
                    ], REST_Controller::HTTP_OK);  
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Error al Guardar Horarios'
                ]); // NOT_FOUND (404) being the HTTP response code
            }   
        }    
    }


    public function inserthorarios_materias_post()
    {
        $userid = $this->utilidades->verifica_userid();
    
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            /*$pin = $_POST['pin']; 

            $existe = $this->utilidades->verifica_pin($pin);
            if ($existe == 1)
            {*/
                $data = json_decode($_POST['data']) ;
                $id_colegio = $_POST['id_colegio']; 
               // $anioA=date('Y');
                $anioA = $this->utilidades->obtener_planestudioaXcolegio($id_colegio);
                $result = $this->modelo_cicloa->obtener_planestudio($anioA);
                $data->planesestudio_id = $result;
                             
                $result = $this->modelo_horario->insertar_horario_materia($data);

                $id = $this->db->insert_id();

                $horario = $this->modelo_horario->get_HorarioMateria($id);

                if ($result)
                {
                    $this->response([
                            'datos'=> json_encode($horario),
                            'status' => TRUE,
                            'message' => 'Horarios cargado', 
                        ], REST_Controller::HTTP_OK);  
                }
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Error al Guardar Horarios Materias'
                    ]); // NOT_FOUND (404) being the HTTP response code
                }   
            /*} 
            else  $this->response([
                    'status' => FALSE,
                    'message' => 'pininvalido'
                    ], REST_Controller::HTTP_OK);  */
        }   
    }
    
    public function update_horarios_materias_post()
    {
        $userid = $this->utilidades->verifica_userid();
    
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            /*$pin = $_POST['pin']; 
            $existe = $this->utilidades->verifica_pin($pin);
            if ($existe == 1)
            {*/
                $data = json_decode($_POST['data']);
                $id = json_decode($_POST['idhormat']);
                             
                $result = $this->modelo_horario->update_horario_materia($data, $id);

                $horario = $this->modelo_horario->get_HorarioMateria($id);

                if ($result)
                {
                    $this->response([
                            'datos'=> json_encode($horario),
                            'status' => TRUE,
                            'message' => 'Horarios modificado', 
                        ], REST_Controller::HTTP_OK);  
                }
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Error al Modificar Horarios Materias'
                    ]); // NOT_FOUND (404) being the HTTP response code
                }   
            /*} 

            else  $this->response([
                    'status' => FALSE,
                    'message' => 'pininvalido'
                    ], REST_Controller::HTTP_OK);  */

        }   
    }

    public function delete_horarios_materias_post()
    {
        $userid = $this->utilidades->verifica_userid();
    
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
        $pin = $_POST['pin']; 
        $existe = $this->utilidades->verifica_pin($pin);
        if ($existe == 1)
        {
            $id = $_POST['horarioid'];
                         
            $result = $this->modelo_horario->delete_horario_materia($id);


            if ($result)
            {
                $this->response([
                        'status' => TRUE,
                        'message' => 'Horario eliminado', 
                    ], REST_Controller::HTTP_OK);  
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Error al Modificar Horarios Materias'
                ]); // NOT_FOUND (404) being the HTTP response code
            }   
        } 

        else  $this->response([
                'status' => FALSE,
                'message' => 'pininvalido'
                ], REST_Controller::HTTP_OK);  

        }   
    }


    public function horariosXdivision_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $divisionid = $_GET['divisionid'] ; 
            //$idcolegio = $_GET['idcolegio'] ;
            
            $anioA = date('Y');
           //$anioA = '2016';
            //$dias = $this->modelo_horario->get_dias();
            //$horas = $this->modelo_horario->get_horariosXcolegio($idcolegio);
            $horariosmat = $this->modelo_horario->get_HorariosMateriasXdivision($divisionid, $anioA);
            
            /*$arre = array(
                            //'dias'=>$dias,
                            //'horas'=>$horas, 
                            'horariosmat'=>$horariosmat
                         );*/

            if ($horariosmat)
            {
                $data = json_encode($horariosmat);
                $this->response($data, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'vacio'
                ], REST_Controller::HTTP_OK); 
            }
        }
    }

    public function horarioXid_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $id = $_GET['id'] ; 
           
            $horariosmat = $this->modelo_horario->get_HorarioMateria($id);

            if ($horariosmat)
            {
                $data = json_encode($horariosmat) ;
                $this->response($data, REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'vacio'
                ], REST_Controller::HTTP_OK); 
            }
        }
    }

    public function horariosXcolegio_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        { 
            //$idcolegio = $_GET['idcolegio'] ;
            $idcolegio = $this->get('idcolegio');
            
            
            $anioA = date('Y');
           
            $horas = $this->modelo_horario->get_horariosXcolegio($idcolegio, $anioA);
                   
            if ($horas)
            {
                $data = json_encode($horas);
                $this->response($data, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'no hay horarios cargados'
                ], REST_Controller::HTTP_OK); 
            }
        }
    }

    public function dias_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        { 
            $dias = $this->modelo_horario->get_dias();
                   
            if ($dias)
            {
                $data = json_encode($dias);
                $this->response($data, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'no hay dias cargados'
                ], REST_Controller::HTTP_OK); 
            }
        }
    }
    public function obtener_all_horarios_docente_id_x_colegio_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        { 
            $idcolegio = $_GET['idcolegio'] ;
            //$horarios = $this->modelo_horario->obtener_horario_docente_x_id_x_colegio($userid,$idcolegio);
            $anioA=date('Y');
            //$cicloa = $this->modelo_cicloa->obtener_cicloa_nombre();
            $cicloa = '';
            $result1 = $this->modelo_horario->obtener_horario_docente_x_id_x_colegio_con_especialidad($userid, $idcolegio, $cicloa);
            $result2 = $this->modelo_horario->obtener_horario_docente_x_id_x_colegio_sin_especialidad($userid, $idcolegio, $cicloa);
                    
                    if( ($result1)&&($result2) )
                            $result = array_merge($result2, $result1);
                        else  if($result1)
                                $result = $result1;
                              else 
                                if($result2)
                                    $result = $result2;
            /*if($result1)
                {$result = $result1;}
            else {

                $result = $result2;}*/
            

                   
            if ($result)
            {
                $data = json_encode($result);
                $this->response($data, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'no hay Horarios cargados',
                    'cicloa'=> $cicloa
                   // 'query' => $this->db->last_query()

                ], REST_Controller::HTTP_OK); 
            }
        }
    }

    public function obtener_mis_horarios_alumno_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $horarios = $this->modelo_horarios->get_horarios_all_user_id_colegio_all($userid);
            $dias = $this->modelo_horario->get_dias();
             $escuelas = $this->modelo_usuario->get_escuelas_all_userd($userid);
             $mis_horarios = $this->modelo_horario->obtener_mis_horarios_alu($userid); // 
            
            // Si existe mas de un resultado, lo mando
            
                
                $horarios = json_encode($horarios);
                $dias = json_encode($dias);
                $escuelas = json_encode($escuelas);
                $mis_horarios = json_encode($mis_horarios);
                 $this->response([
                    'status' => TRUE,
                    'horarios' => $horarios,
                    'dias'=> $dias,
                    'escuelas' => $escuelas,
                    'mis_horarios' => $mis_horarios,
                ], REST_Controller::HTTP_OK); 
            
            // Sino, envio respuesta con 404
            
        }

    }

}
