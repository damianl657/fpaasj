<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    /**
     * Clase para almacenar funciones varias que tengan
     * que ver con operaciones con Division
     */
    class Torneos
    {
        private $CI; // Por si hace falta acceder desde algun metodo a la instancia global de CodeIgniter

        public function __construct()
        {
            $this->CI = get_instance();
            $this->CI->load->model("modelo_torneo");
        }

        /**
         * Busca la division en el ciclo actual de un alumno
         * @param  [type] $idalumno [idalumno encriptado]
         * @return [type]         [division del alumno]
         */
         function get_fecha_inicio_x_t($idtorneo){
        $CI = & get_instance();
        $sql="SELECT inicio FROM `torneo` WHERE id = $idtorneo";
           $query= $CI->db->query($sql);
           if ($query->num_rows()>0) {
                foreach($query->result() as $row){
                return $row->inicio;
            }
           }else 
           return false;
        } 

        function get_fecha_fin_x_t($idtorneo)
        {   $CI = & get_instance();
        $sql="SELECT fin FROM `torneo` WHERE id = $idtorneo";
           $query= $CI->db->query($sql);
           if ($query->num_rows()>0) {
                foreach($query->result() as $row){
                return $row->fin;
            }
           }else 
           return false;
        }
    }
        
    