<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Alumno extends REST_Controller {

    function __construct()
    {
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        parent::__construct();
        $this->load->model("modelo_alumno");
        $this->load->model("modelo_tutor");
        $this->load->model("modelo_usuario");
        $this->load->model("modelo_inscripciones");
        $this->load->model("Modelo_especialidad");
        $this->load->model("Modelo_division");
        $this->load->model("Modelo_usuario");
    }

    public function obtener_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            // Hago lo que debe hacer el metodo especificamente
            $idalumno = $this->get('alumno_id'); // obtengo el id del alumno, si es que vino en la url
            if ($idalumno === NULL)
            {
                $user_id = $this->utilidades->desencriptar($this->input->get_request_header('userid'));
                // Obtengo los alumnos relacionado con tutor
                $alumnos = $this->modelo_alumno->obtener_alumnos($user_id); //obtiene los alumnos a cargo de un tutor
                // Si existe mas de un resultado, lo mando
                if ($alumnos)
                {
                    $this->response($alumnos, REST_Controller::HTTP_OK);
                }
                // Sino, envio respuesta con 404
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'No se encontraron alumnos'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                }
            }
        }
    }
    
    public function cambiar_division_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            //$anioA = $this->utilidades->obtener_planestudioaXcolegio( $this->input->post('id_colegio_cambio') );
            //$planest_Id = $this->utilidades->get_planestudio( $anioA );
            $cicloA = $this->utilidades->obtener_cicloaXcolegio( $this->input->post('id_colegio_cambio') );
            $user_group = $this->modelo_usuario->obtener_users_group_alumno($this->input->post('id_alumno_cambio'));

            $inscripcion_actual_data = array(
                'alumno_id' => $user_group->id,
                'activo' => 1,
                'cicloa' => $cicloA,
                       // 'divisiones_id' => $this->input->post('id_division_x_al'),

            );
            $inscripcion_actual = $this->modelo_inscripciones->get($inscripcion_actual_data);

            $inscripcion_nueva_data = array(
                'alumno_id' => $user_group->id,
                'activo' => 1,
                'cicloa' => $cicloA,
                'divisiones_id' => $this->input->post('id_division_x_al'),

            );
            $this->modelo_inscripciones->update($inscripcion_actual->id, $inscripcion_nueva_data);
            $message = [
                'status' => TRUE,
                'message' => 'El cambio de curso se registro con Exito',
                            //'post' =>  $this->input->post,
                'id_alumno' => $this->input->post('id_alumno_cambio'),
                'id_division' => $this->input->post('id_division_x_al'),
                'id_colegio' => $this->input->post('id_colegio_cambio'),
                'user_group' => $user_group,
                'inscripcion_actual_data' => $inscripcion_actual_data,
                'inscripcion_actual' => $inscripcion_actual,
                'last_query' => $this->db->last_query(),
            ];
            $this->set_response($message, REST_Controller::HTTP_OK);
        }
    }

    public function obtener_alumnos_padres_x_division_get(){
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $division_id = $this->get('division_id'); 
            $colegio_id = $this->get('idcolegio'); 
            
            
            $cicloA = $this->utilidades->obtener_cicloaXcolegio( $colegio_id );
            //$planest_Id = $this->utilidades->get_planestudio( $anioA ); 

            $result_alumnos = $this->modelo_alumno->get_alumnos_x_division($division_id, $colegio_id, $cicloA); // obtiene los alumnos por division y colegio

            //$result_alumnos = json_encode($result);
            //$result_alumnos = $this->modelo_tutores->obtener_tutores_x_id_alumno($id_division); // obtiene el evento especifico
            // Si existe mas de un resultado, lo mando
            if ($result_alumnos)
            {   //$arreAlum = array();
                $dataAlumnoTutor = array();
                foreach ($result_alumnos as $row_alumno) { // Recorrer los alumnos.por id
                    //$arreAlum[] = $row_alumno->id;

                    $result_tutor = $this->modelo_tutor->obtener_tutores_x_alumno($row_alumno->id);

                    $cadena_alumnos = $row_alumno->first_name." ".$row_alumno->last_name;
                    if($result_tutor){
                        $dataTutores = array();
                        foreach ($result_tutor as $key_tutor) {   // Recorrer los alumnos.por id
                           //$cadena_alumnos.= "@".$key_tutor->id." ".$key_tutor->first_name." ".$key_tutor->last_name;                      
                         $dataTutores[] = array ('nombre_tutor'=>$key_tutor->first_name , 'apellido_tutor'=>$key_tutor->last_name, 'id' => $key_tutor->id, 'email_tutor' => $key_tutor->email,'documento_tutor'=>$key_tutor->documento);
                           //$dataAlumnoTutor[$row_alumno->id] = $cadena_alumnos;
                     } 
                     $dataAlumnoTutor[] =  array ('nombre_alumno'=>$row_alumno->first_name , 
                        'apellido_alumno'=>$row_alumno->last_name, 
                        'id' => $row_alumno->id,
                        'email_alumno' => $row_alumno->email, 
                        'documento_alumno' => $row_alumno->documento,
                        'tutores' => $dataTutores);                      
                 }
                 else{
                    $dataAlumnoTutor[] =  array ('nombre_alumno'=>$row_alumno->first_name , 'apellido_alumno'=>$row_alumno->last_name, 'id' => $row_alumno->id, 'email_alumno' => $row_alumno->email, 'documento_alumno' => $row_alumno->documento);
                }
            }
            $result = json_encode($dataAlumnoTutor);
            $this->response($result, REST_Controller::HTTP_OK);
        }
            // Sino, envio respuesta con 404
        else
        {
            $this->response([
                'status' => FALSE,
                'message' => 'No se encontraron alumnos para la division seleccionada'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
    }// fin function      

    public function obtener_alumnos_x_division_get(){
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            $division_id = $this->get('division_id'); 
            $colegio_id = $this->get('idcolegio'); 
            

            $cicloA = $this->utilidades->obtener_cicloaXcolegio( $colegio_id );
            //$planest_Id = $this->utilidades->get_planestudio( $anioA );
            
            $result_alumnos = $this->modelo_alumno->get_alumnos_x_division($division_id, $colegio_id, $cicloA); // obtiene el evento especifico

            //$result_alumnos = json_encode($result);
            //$result_alumnos = $this->modelo_tutores->obtener_tutores_x_id_alumno($id_division); // obtiene el evento especifico
            // Si existe mas de un resultado, lo mando
            if ($result_alumnos)
            {   //$arreAlum = array();
                $dataAlumnoTutor = array();
                foreach ($result_alumnos as $row_alumno) { // Recorrer los alumnos.por id
                    //$arreAlum[] = $row_alumno->id;

                    $result_tutor = $this->modelo_tutor->obtener_tutores_x_alumno($row_alumno->id);

                    $cadena_alumnos = $row_alumno->first_name." ".$row_alumno->last_name;
                    $dataAlumnoTutor[] =  array ('nombre_alumno'=>$row_alumno->first_name , 'apellido_alumno'=>$row_alumno->last_name, 'id' => $row_alumno->id );
                    if($result_tutor){
                        foreach ($result_tutor as $key_tutor) {   // Recorrer los alumnos.por id
                           //$cadena_alumnos.= "@".$key_tutor->id." ".$key_tutor->first_name." ".$key_tutor->last_name;                      
                         $dataAlumnoTutor[] = array ('nombre_tutor'=>$key_tutor->first_name , 'apellido_tutor'=>$key_tutor->last_name, 'id' => $key_tutor->id );
                           //$dataAlumnoTutor[$row_alumno->id] = $cadena_alumnos;
                     }

                 }

             }


                //var_dump($dataAlumnoTutor);
                //die();

             $result = json_encode($dataAlumnoTutor);
             $this->response($result, REST_Controller::HTTP_OK);
         }
            // Sino, envio respuesta con 404
         else
         {
            $this->response([
                'status' => FALSE,
                'message' => 'No se encontraron alumnos para la division seleccionada'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
}
public function obtener_especialidad_post() {

    $userid = $this->utilidades->verifica_userid();
    if ($userid == -1)
    {
        $this->response([
            'status' => FALSE,
            'message' => 'NO LOGUIN'
        ], REST_Controller::HTTP_OK);            
    }
    else
    {   

        $result = $this->Modelo_especialidad->obtener_especialidades();



        if ($result)
        {
                // $result = json_encode($result);

            $this->response($result, REST_Controller::HTTP_OK);
        }
            // Sino, envio respuesta con 404
        else
        {
            $this->response([
                'status' => FALSE,
                'message' => 'No se encontraron Torneo'
            ], REST_Controller::HTTP_OK); 
        }
    }
}
public function obtener_division_post() {

    $userid = $this->utilidades->verifica_userid();
    if ($userid == -1)
    {
        $this->response([
            'status' => FALSE,
            'message' => 'NO LOGUIN'
        ], REST_Controller::HTTP_OK);            
    }
    else
    {   
        $espec= $this->input->get_post('esp_id');

        $result = $this->Modelo_division->get_division_x_especialidad($espec);



        if ($result)
        {
                // $result = json_encode($result);

            $this->response($result, REST_Controller::HTTP_OK);
        }
            // Sino, envio respuesta con 404
        else
        {
            $this->response([
                'status' => FALSE,
                'message' => 'No se encontraron Torneo'
            ], REST_Controller::HTTP_OK); 
        }
    }
}
public function obtener_eficiencia_post() {

    $userid = $this->utilidades->verifica_userid();
    if ($userid == -1)
    {
        $this->response([
            'status' => FALSE,
            'message' => 'NO LOGUIN'
        ], REST_Controller::HTTP_OK);            
    }
    else
    {   
        $divi= $this->input->get_post('div_id');
      
        $result = $this->Modelo_division->get_eficiencia_x_division($divi);




        if ($result)
        {
                // $result = json_encode($result);

            $this->response($result, REST_Controller::HTTP_OK);
        }
            // Sino, envio respuesta con 404
        else
        {
            $this->response([
                'status' => FALSE,
                'message' => 'No se encontraron especialidad'
            ], REST_Controller::HTTP_OK); 
        }
    }
}
public function obtener_datos_user_post() {

    $userid = $this->utilidades->verifica_userid();
    if ($userid == -1)
    {
        $this->response([
            'status' => FALSE,
            'message' => 'NO LOGUIN'
        ], REST_Controller::HTTP_OK);            
    }
    else
    {   
        $doc= $this->input->get_post('documento');

        $result = $this->Modelo_usuario->obtener_datos_user($doc);



        if ($result)
        {
                // $result = json_encode($result);

            $this->response($result, REST_Controller::HTTP_OK);
        }
            // Sino, envio respuesta con 404
        else
        {
            $this->response([
                'status' => FALSE,
                'message' => 'No se encontraron Deportista'
            ], REST_Controller::HTTP_OK); 
        }
    }
}
public function guardar_inscripcion_post() {
        // echo "api_: ";

    $datos=explode("_", $this->input->post('datos'));
        //  print_r($datos);
        // die();
    $userid = $this->utilidades->verifica_userid();
    if ($userid == -1)
    {
        $this->response([
            'status' => FALSE,
            'message' => 'NO LOGUIN'
        ], REST_Controller::HTTP_OK);            
    }
    else
    {   
        $arre= array(
            'user_id'       => $datos[0],
            'divisiones_id' => $datos[1],
            'cicloa'        =>2020,
            'activo'        =>1          
        );

        $this->db->insert('inscripciones_div', $arre);

        $this->response(REST_Controller::HTTP_OK);          

    }
}

public function listar_inscriptos_post() { 

    $userid = $this->utilidades->verifica_userid();
    if ($userid == -1)
    {
        $this->response([
            'status' => FALSE,
            'message' => 'NO LOGUIN'
        ], REST_Controller::HTTP_OK);            
    }
    else
    {   

        $club=$this->input->post('idclub');

        $result=$this->modelo_inscripciones->obtener_inscriptos($club);

        $this->response($result,REST_Controller::HTTP_OK);          

    }
}
public function buscar_user_post(){

  $userid = $this->utilidades->verifica_userid();
  if ($userid == -1)
  {
    $this->response([
        'status' => FALSE,
        'message' => 'NO LOGUIN'
    ], REST_Controller::HTTP_OK);            
}
else
{   
    $iduser= $this->input->get_post('id');
    $divuser= $this->input->get_post('div');


    $result = $this->modelo_inscripciones->get_datos_para_modificar($iduser,$divuser);



    if ($result)
    {
                // $result = json_encode($result);                
        $this->response($result, REST_Controller::HTTP_OK);
    }
            // Sino, envio respuesta con 404
    else
    {
        $this->response([
            'status' => FALSE,
            'message' => 'No se encontraron Usuario'
        ], REST_Controller::HTTP_OK); 
    }
}
}
public function buscar_participante_post(){

  $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {   //print_r($_POST);
            $partic= $this->input->get_post('participante');
            $id_club =$this->input->get_post('idclub');

            $result = $this->modelo_inscripciones->obtener_dato_inscriptos($partic,$id_club);



            if ($result)
            {
                    // $result = json_encode($result);                
                $this->response($result, REST_Controller::HTTP_OK);
            }
                    // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No hay participante'
                ], REST_Controller::HTTP_OK); 
            }
        }
}
public function obtener_categoria_post(){
     $userid = $this->utilidades->verifica_userid();
      if ($userid == -1)
      {
        $this->response([
            'status' => FALSE,
            'message' => 'NO LOGUIN'
        ], REST_Controller::HTTP_OK);            
    }
    else
    {   
        $edad= $this->input->get_post('edad');
        $division= $this->input->get_post('division');
        

        $result = $this->modelo_inscripciones->obtener_categoria($division,$edad);



        if ($result)
        {
                    // $result = json_encode($result);                
            $this->response($result, REST_Controller::HTTP_OK);
        }
                // Sino, envio respuesta con 404
        else
        {
            $this->response([
                'status' => FALSE,
                'message' => 'No se encontraron categoria'
            ], REST_Controller::HTTP_OK); 
        }
    }
}


    public function update_inscripcion_post(){
            print_r($_POST);
            
            $userid = $this->utilidades->verifica_userid();
              if ($userid == -1)
              {
                $this->response([
                    'status' => FALSE,
                    'message' => 'NO LOGUIN'
                ], REST_Controller::HTTP_OK);            
            }
            else
            {   
                $division= $this->input->get_post('div');
                $inscripcion= $this->input->get_post('insc');

                $result = $this->modelo_inscripciones->update_inscripcion($division,$inscripcion);



                if ($result)
                {
                            // $result = json_encode($result);                
                    $this->response($result, REST_Controller::HTTP_OK);
                }
                        // Sino, envio respuesta con 404
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'No se modifico'
                    ], REST_Controller::HTTP_OK); 
                }
            }
            
        }
}