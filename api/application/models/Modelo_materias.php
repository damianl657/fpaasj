<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_materias extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }

        public function obtener_materias()
        {
            $sql = "SELECT *
                    FROM nombresmateria
                     ORDER BY nombre ASC ";
            $query = $this->db->query($sql);
            return $query->result();
        }


        public function obtener_aniosmaterias($idcolegio, $nodoanioid)
        {
            $sql = "SELECT anios_materias.id, nombresmateria.nombre, nombresmateria.id as idmateria
                    FROM anios_materias 
                    JOIN nombresmateria on nombresmateria.id = anios_materias.nombresmateria_id
                    JOIN nodocolegio on nodocolegio.id = anios_materias.colegio_id
                    JOIN planesestudio on planesestudio.nombre = nodocolegio.planestudio
                    WHERE anios_materias.colegio_id=$idcolegio
                    AND anios_materias.nodoanios_id=$nodoanioid
                    AND anios_materias.nombresmateria_id=nombresmateria.id
                    AND anios_materias.cicloa = planesestudio.id
                    ORDER BY nombresmateria.nombre ASC ";
            $query = $this->db->query($sql);
            return $query->result();
        }


        public function obtener_aniosmaterias_bycolegio($idcolegio)
        {
            $sql = "SELECT anios_materias.id, nombresmateria.nombre, nombresmateria.id as idmateria, anios.nombre as nombreanio, niveles.nombre as nombrenivel, especialidades.nombre as nombreespecialidad
                    FROM anios_materias 
                    JOIN nombresmateria on nombresmateria.id = anios_materias.nombresmateria_id
                    JOIN nodocolegio on nodocolegio.id = anios_materias.colegio_id
                    JOIN planesestudio on planesestudio.nombre = nodocolegio.planestudio
                    JOIN nodoanios ON anios_materias.nodoanios_id = nodoanios.id
                    JOIN anios ON nodoanios.anio_id = anios.id

                    JOIN nodonivel ON nodonivel.id = nodoanios.nodoniv_id
                    JOIN niveles ON niveles.id = nodonivel.niveles_id
                    LEFT JOIN especialidades ON especialidades.id = nodonivel.esp_id

                    WHERE anios_materias.colegio_id=$idcolegio
                    AND anios_materias.nombresmateria_id=nombresmateria.id
                    AND anios_materias.cicloa = planesestudio.id
                    ORDER BY nombresmateria.nombre, anios.nombre, niveles.nombre, especialidades.nombre ASC ";
            $query = $this->db->query($sql);
            return $query->result();
        }


        public function obtener_materia($name)
        {
            $sql = "SELECT id FROM nombresmateria WHERE nombre='$name'";
            $query = $this->db->query($sql);
            return $query->result();
        }

        public function insertar_materia($data)
        {
            $query = $this->db->insert("nombresmateria",$data);
            return $query;
        }

        public function insertar_aniosmaterias($data)
        {
            return $this->db->insert("anios_materias",$data);
        }

        public function insertar_aniosmaterias2($data, $data2)
        {
            $this->db->where($data);
            $query = $this->db->get("anios_materias");

            if($query->num_rows()>0){
                return false;
            }
            else{
                return $this->db->insert("anios_materias",$data2);
            }    
        }

        public function insertar_areasmaterias($data, $data2)
        {
            $this->db->where($data);
            $query = $this->db->get("area_materias");

            if($query->num_rows()>0){
                /*$this->db->where($data);
                $valores = array(
                    'estado'=>1,
                ); 
                return $this->db->update("area_materias", $valores);*/
                return false;
            }
            else{
                return $this->db->insert("area_materias",$data2);
            }    
        }

        /*public function obtener_niveles_cargados($idcolegio, $idcicloa)
        {
            $sql = "SELECT nodonivel.id as idnodonivel, nombre, niveles_id FROM nodonivel JOIN niveles
                    WHERE nodocolegio_id = $idcolegio 
                    AND nodonivel.niveles_id = niveles.id 
                    AND nodonivel.cicloa_id = $idcicloa
                    AND estado=1
                    ";
            $query = $this->db->query($sql);
            return $query->result();
        }*/


        public function obtener_aniosmaterias_pase1($idcolegio, $ciclo)
        {
            $sql = "SELECT *
                    FROM anios_materias
                    WHERE anios_materias.colegio_id=$idcolegio
                    AND anios_materias.cicloa=$ciclo
                    ORDER BY anios_materias.id ASC ";
            $query = $this->db->query($sql);
            return $query->result();
        }


       public function get_aniomateria_new($id) //se usa para el pase de anio
       {
          $sql = "SELECT *
                  FROM anios_materias 
                  WHERE anio_materia = $id
                  ";
          $query = $this->db->query($sql);
          
          if($query->num_rows()>0){
              return $query->result();
          }
          else{
              return false;
          }    
       }


        public function get_aniomateria($data)
        {
            $this->db->where($data);
            $query = $this->db->get("anios_materias");

            if($query->num_rows()>0){
                return $query->row();
            }
            else{
                return false;
            }    
        }

        public function get_horariosmateria($data)
        {
            $this->db->where($data);
            $query = $this->db->get("horariosmaterias");

            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }    
        }


        public function get_areasmateria($data)
        {
            $this->db->where($data);
            $query = $this->db->get("area_materias");

            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }    
        }


        public function delete_aniosmaterias($data)
        {
            $this->db->where($data);
            return $this->db->delete("anios_materias");
        }

        public function delete_horariosmaterias($data)
        {
            $this->db->where($data);
            return $this->db->delete("horariosmaterias");
        }

        public function delete_areamateria($data)
        {
            $this->db->where($data);
            return $this->db->delete("area_materias");
        }
        
              

	}