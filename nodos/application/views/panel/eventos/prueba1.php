'<div class="bloque-info">'+
                '<div class="bloque-info-header">'+
                    '<div class="row">'+
                        '<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 text-left">'+
                            '<div class="avatar">'+
                                '<img src="images/avatar.png" alt=""/>'+
                            '</div>'+
                            '<div class="hijo">'+
                                '<h4>'+'Lucas Gallardo'+'</h4>'+
                                '<p>'+
                                    '<span class="hora">'+'9:30 AM'+'</span>'+
                                    '<span class="fecha">'+'Abril 20, 2016'+'</span>'+
                                '</p>'+
                            '</div>'+
                            
                        '</div>'+
                        '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-right">'+
                            '<img src="images/escudo.png" alt=""/>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<hr>'+
                '<div class="bloque-info-main">'+
                    '<p>'+'La App Nodos entra en funcionamiento en los colegios de San Juan.'+'</p>'+
                '</div>'+
                '<hr>'+
                '<div class="bloque-info-footer">'+
                    '<div class="row">'+
                        '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-left">'+
                            '<a class="like">'+'<i class="fa fa-heart">'+'</i>'+'</a>'+
                        '</div>'+
                        '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">'+
                            '<a>'+'<i class="fa fa-ellipsis-h">'+'</i>'+'</a>'+
                        '</div>'+
                    '</div>'+
                    
                '</div>'+
            '</div>'