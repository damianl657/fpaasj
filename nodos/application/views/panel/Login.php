<?php 
 
class Login extends CI_Controller {
      
	function __construct(){
		parent::__construct();
			//$this->output->enable_profiler(TRUE);
	}
		
    public function index(){         
        //$data['action'] = base_url('auth/validar');
        //$this->load->view('admin/niveles',$data);	
        
        $this->load->view('include/header');
		//$this->load->view('include/navbar');
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		$data['urlApi'] = $urlApi;
		$data['passApiKey'] = $passApiKey;
		$this->load->view('admin/loginView',$data);	
		//$this->load->view('include/footer');		
		
		//redirect('/');
    }


    /**
    * Obtiene los roles de la api y verifica si es institucion o no. 
    * Peticion controller web a controller api. uso de curl 
    * @params $idUser que es el id del usuario
	* return si es institucion muestra la vista de configuracion (pasos)
	* 
    */
    public function get_roles($urlApi, $passApiKey, $idUser)  
    {  
    	$url = $urlApi."/usuario/obtener_roles";
		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser") ,
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
	
		//var_dump($response);	
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}

	/*public function loginApi($urlApi, $passApiKey, $idUser)  
    {  
    	$url = $urlApi."/usuario/obtener_roles";
		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser") ,
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
	
		//var_dump($response);	
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}*/

    public function principal()  
    {  //echo "principal";
    	//$idUser = str_replace("***", "/", $idUser);  //quitar caracter de escape!!    	

    	$idUser = $_POST['idusuario']; 
    	//$this->session->sess_destroy();
		//$this->session->sess_create();
		$this->session->set_userdata( array('logged_in' => true, 'idusuario' => $idUser) ); 

		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
	
		$response = $this->get_roles( $urlApi, $passApiKey, $idUser);				
		
		if( (isset($response->status)) && ($response->status==false) && ($response->message=='NO LOGUIN') )
		{	
			//echo "NO LOGIN";
			redirect(base_url('/index.php/Login'));
		}
		else
		{	
			$response = json_decode($response);
			//var_dump($response);	
			$rolAdmin='Institucion'; //valores fijos DB
			$pasosFin= 6;
			$confHorasSteep=4;
			$confMateriasSteep=5;

			$contInst = 0;
			foreach ($response as $key) {
				if($key->nombregrupo==$rolAdmin)
					$contInst++;

				/*$rolesNomb[]=$key->nombregrupo;
				$rolesId[]=$key->idgrupo;*/
			}
			/*$rolesNomb = join(',',$rolesNomb); 
			$rolesId = join(',',$rolesId);*/

			//ELEGIR EL DE MAYOR JERARQUIA
			$rolesNomb = 'Nodos';
			$rolesId = 9;

			//$this->load->view('include/header');
			$data['idusuario'] = $this->session->userdata('idusuario');
			$data['urlApi'] = $urlApi;
			$data['passApiKey'] = $passApiKey;
			$data['idcolegio'] = $response[0]->idcolegio;
			$data['idgrupo'] = $rolesId;
			$data['nombregrupo'] = $rolesNomb;  
			$data['nombrecolegio'] = $response[0]->nombrecolegio;

			
			$this->session->set_userdata( array('idcolegio' => $data['idcolegio'], 'idgrupo' => $data['idgrupo'], 'nombregrupo' => $data['nombregrupo'], 'nombrecolegio' => $data['nombrecolegio']  ) ); 
			
			//echo $contInst;
			if($contInst==1)
			{						
				$this->session->set_userdata( array('idcolegio' => $data['idcolegio'], 'idgrupo' => $data['idgrupo'], 'nombregrupo' => $data['nombregrupo'], 'nombrecolegio' => $data['nombrecolegio']  ) ); 
				//echo "cont institucion: ".$key->configuracion;
				if($key->configuracion!=$pasosFin)
				{
					if($key->configuracion<$confHorasSteep) //<4
					{
						//var_dump($this->session->userdata);
						$this->load->view('admin/confNiveles',$data);
					}
					else 
						if($key->configuracion==$confHorasSteep) //==4
						{
							$this->load->view('admin/confHoras',$data);
						}
					else 
						if($key->configuracion==$confMateriasSteep) //==4
						{
							$this->load->view('admin/confMaterias',$data);
						}
				}
				else{
					//$this->load->view('panel/panel',$data);
					/*$data['opcion_menu'] = 'home';
				    $this->load->view('panel/vista_principal',$data);*/
				    redirect('calendario');	
				} 
					

			}
			else{
				//$this->load->view('panel/panel',$data);// Version anterior Eze.	 
				/*$data['opcion_menu'] = 'home';
				$this->load->view('panel/vista_principal',$data);	*/
				redirect('calendario');
			} 
				
		}
		 	
    }	


    public function logout(){  
    	$newdata = array(
                'idusuario'  =>'',
                'idcolegio' => '',
                'logged_in' => FALSE,
                'idgrupo'  =>'',
                'nombregrupo' => '',
                'nombrecolegio' => '',
               );

     	$this->session->unset_userdata($newdata);
    	$this->session->sess_destroy(); 

    	redirect('login');
	}

	public function home(){
		if(!$this->session->userdata("logged_in")){
               redirect("login");
         }
         redirect("calendario");
		//print_r($this->session->userdata());
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();	
	  	$data['idusuario'] = $this->session->userdata('idusuario');
		$data['urlApi'] = $urlApi;
		$data['passApiKey'] = $passApiKey;
		$data['idcolegio'] = $this->session->userdata('idcolegio');
		$data['idgrupo'] = $this->session->userdata('idgrupo');
		$data['nombregrupo'] = $this->session->userdata('nombregrupo');
		$data['nombrecolegio'] = $this->session->userdata('nombrecolegio');
		$data['opcion_menu'] = 'home';
		$this->load->view('panel/vista_principal',$data);
	}
}
?>
