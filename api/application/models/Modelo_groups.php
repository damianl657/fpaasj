<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_groups extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }

        public function get_name_rol($id)
        {
            $sql = "SELECT *
                FROM groups
                WHERE id = $id";
            $query = $this->db->query($sql);
            return $query->row();
        }

        
        public function get_usersgroups($data)
        {
            $this->db->where($data);
            $query = $this->db->get('users_groups');
            if($query->num_rows()>0)
                return $query->result();
            else
                return 0;   
        }


        public function insert_group($data)
        {
            $result = $this->db->insert('groups', $data);
            if($result)
                return $this->db->insert_id();  
            else return 0;
        }

        public function update_group($id,$data)
        {
            $this->db->where("id",$id);
            return $this->db->update("groups",$data);
        }
        public function get_id_rol($name,$id_colegio)
        {
            $sql = "SELECT *
                FROM groups
                WHERE name = '$name' and id_colegio = $id_colegio";
            $query = $this->db->query($sql);
            return $query;
        }
        
       
	}