<script>
		var passApiKey = '<?php echo $passApiKey; ?>';

		urlApi = '<?php echo $urlApi; ?>';

		idUser = '<?php echo $idusuario; ?>';
		 
		idusergrupo = '<?php if(isset($idUserGrupoInst)) echo $idUserGrupoInst; else echo ""; ?>';
       
        idcolegio = '<?php echo $idColegioInst; ?>';

		idgrupo = '<?php echo $idgrupo; ?>';

		nombregrupo = '<?php echo $nombregrupo; ?>';

		ordengrupo = '<?php echo $ordengrupo; ?>';

		function comenzar(){   	

			  var urlRequest = urlApi+"/usuario/pasos";													

			  $.ajax({
	                url: urlRequest,
	                type: 'GET',
	                headers: {
				                'APIKEY' : passApiKey,
	                      'userid' : idUser			   
	          			    },
	          	    data: {idusergrupo:idusergrupo},
	                beforeSend: function() {
	                  //$('#cuerpo').append("<img src='<?=base_url('assets/images/loading.gif');?>' height='60' width='60' />");
	                },
	                success: function(data){  //console.log(data);
	  			        if(data['status']==false)
	                    {    //alert(data['message']);

	                       location.href = "<?php echo base_url('login'); ?>";
	                    }
	                   else{ //alert('paso: '+data['paso']);							        
	                   		switch(data['paso'])
	                   		{
                       			case '0':  //si es cero voy al paso 1
                   						mostrar_nivelesP1();
									    listar_niveles_cargados();            
    						            
    						            $('#introNiveles').css('display','none');
    						            $('#paso2').css('display','none');        						           
    						            $('#paso3').css('display','none');
										$('#paso4').css('display','none');
    						           
                       					break;

                       			case '1': //si es uno voy al paso 2
                       						            							
										mostrar_especialidad_step2(); // COlumna Derecha- Lista Especialidades precargadas.
										
            							//mostrar_niveles_step2();
            							listar_especialidades_cargadas(); // Columna Izquierda - Especialidades asignadas						       
            						        						           
            				            $('#paso1').css('display','none');
            				            $('#introNiveles').css('display','none');
            						    $('#paso2').css('display','block');
            						    $('#paso3').css('display','none');
    								    $('#paso4').css('display','none');    						            
                       				break;

                       			case '2': //si es dos voy al paso 3
									    listar_nivelesEspecialidadesAnios();
                       					
        						        $('#introNiveles').css('display','none');         
            						    $('#paso1').css('display','none');
            						    $('#paso2').css('display','none');
            						    //$('#paso3').css('display','block');
    									$('#paso4').css('display','none');
                       				break;

	                            case '3': //si es tres voy al paso 4
	                                listar_nivelesEspecialidades(); 

	                               
	                                $('#introNiveles').css('display','none');                       
	                                $('#paso1').css('display','none');
	                                $('#paso2').css('display','none');
	                                $('#paso3').css('display','none'); 
	                                $('#paso4').css('display','block');                               
	                                break;
	                   		}
	                   	}
	                  
	                },
	                error: function(response){
	                    console.log(response);
	                    
	                }
	            });
		}

	$(document).ready(function(){ 

			$.noConflict();

			//comenzar();

			$('#comenzar').click(function(e){
				e.preventDefault();
				comenzar();
			});
	  });

	</script>
	<!--     -->
	<body>
		
		<?php //include("application/views/include/navbar.php"); ?>
		
		<div class="container">
			<div class="row" id="introNiveles">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center vspace4">
					<h1>Configuraci&oacute;n de 4 pasos</h1>
					<h6>de instituci&oacute;n en sistema nodos</h6>
				</div>
				<div class="col-lg-offset-3 col-lg-6 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-12 center vspace3">
					<p>Por favor complete los 4 PASOS para confirgurar su instituci&oacute;n en nuestro sistema. 
					Una vez completada la configuraci&oacute;n se podr&aacute;n crear los diferentes nodos que forman parte de la instituci&oacute;n y agregar sus respectivos usuarios.<br><br>Siga los pasos y dejaremos la confiruaci&oacute;n lista en minutos.</p>
				</div>
				<div class="col-lg-offset-3 col-lg-6 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-12 center vspace3">
					<a href="#" class="btn-principal" id="comenzar">Comenzar Configuraci&oacute;n</a>
				</div>
			</div>


			<div id="cuerpo" >
		        <!-- #first_step -->
		        <?php require('step1.php'); ?>     <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
				 <br>			
		        <!-- #second_step --> 
		         <?php require('step2.php'); ?>      <!-- clearfix --><div class="clear"></div><!-- /clearfix -->          
		  		<br>     			
		        <!--  -->
		  		<?php require('step3.php'); ?>     <!-- clearfix --><div class="clear"></div><!-- /clearfix -->  
		  		 <br>     
		        <!-- #4to_step -->
		        <?php require('step4.php'); ?>  

		        <!-- #outro -->
		        <?php require('confNiveles_outro.php'); ?> 
		    </div> 


		</div>

		<div class='notifications top-right' style="margin-top: 40px">
			<!--<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button> -->
			
		</div>
		
		

<?php //include("application/views/include/footer.php"); ?>

<?php //include("application/views/include/websocket.php"); ?>   

</body>		