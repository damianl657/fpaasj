﻿<?php

class Materias_area extends CI_Controller {

	function __construct(){
		parent::__construct();

		if(!$this->session->userdata("logged_in")){
               redirect("login");
         }
	}

    //-------------------------------------------------------

    public function index(){

	    $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		//$data['urlApi'] = $urlApi;
		//$data['passApiKey'] = $passApiKey;
		$data['idusuario'] = $this->session->userdata('idusuario');



		$band = 0;
		$controlador = "Materias_area";
		$metodo = "index";
		//$accion1 = "add_materia";

        $colegios = $this->session->userdata("colegios");
        $colegiosPermi = array();
        $acciones = array();
        $banAddMat = 0;
		foreach ($colegios as $cole)
		{
			$idcolegio = $cole["id_colegio"];
			$menus = $cole["menu"];

			if($cole['areas']){

			 	foreach ($menus as $menu)
				{
					if( ($menu->controlador === $controlador) && ($menu->metodo === $metodo))
					{
					    $band = 1;

					    $key = array_search($idcolegio, array_column($colegiosPermi, 'idcole'));

					    if(!$key){

						    $arre = array(
						    	'idcole'=>$idcolegio,
						    	'namecole'=>$cole["nombre_colegio"],
						    	'idrol'=>$cole["id_grupo"],
						    	'namerol'=>$cole["rol"],
						    );
						    $colegiosPermi[]=$arre;


						    $result =$this->get_areas_x_colegio_curl($urlApi, $passApiKey, $data['idusuario'], $idcolegio, $cole["id_grupo"], $cole["rol"]);
							if($result->status)
								$data['areas'][$idcolegio] = $result->result;

							$result2 =$this->get_aniosmaterias_x_colegio_curl($urlApi, $passApiKey, $data['idusuario'], $idcolegio);
							//print_r($result2);
							if($result2->result)
								$data['materias'][$idcolegio] = $result2->result;

						}
					}
				}

				/*foreach ($menus as $menu)
				{
					if( $menu->nombre === $accion1)
					    $banAddMat = 1;
				}*/

			}
		}

		/*if($banAddMat == 1){

		    $acciones[]=$accion1;
		}*/



        if($band == 1)
        {
    		$data['colegios'] = $colegiosPermi;
    		//$data['acciones'] = $acciones;

		    $data['contenido'] = 'admin/confMaterias_area';
			$this->load->view('include/template_colegio',$data);

		}
		else redirect(404);

	}


	//---------------------------------------- CURL request to API -------------------------------------------

    public function get_areas_x_colegio_curl($urlApi, $passApiKey, $idUser, $idcolegio, $idgrupo, $nombregrupo)
    {
    	$url = $urlApi."/area/obtener_areas";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('idgrupo' => $idgrupo, 'nombregrupo' => $nombregrupo,'idcolegio' =>$idcolegio),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);

		//print_r($response);

		$response=json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}

	public function get_aniosmaterias_x_colegio_curl($urlApi, $passApiKey, $idUser, $idcolegio)
    {
    	$url = $urlApi."/materia/anios_materia_bycolegio/idcolegio/$idcolegio";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			//CURLOPT_POST => true,
			//CURLOPT_POSTFIELDS => array('idgrupo' => $idgrupo, 'nombregrupo' => $nombregrupo,'idcolegio' =>$idcolegio),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);
		//print_r($response);

		$response=json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}


	public function get_materias_x_area_curl($urlApi, $passApiKey, $idUser, $idcolegio, $idgrupo, $nombregrupo, $idarea)
    {
    	$url = $urlApi."/area/obtener_materias/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('idcolegio'=>$idcolegio, 'idgrupo'=>$idgrupo, 'nombregrupo'=>$nombregrupo, 'areas'=>$idarea  ),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//print_r($response);
		//$response = substr($response, 3);
		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}



	public function add_materias_x_area_curl($urlApi, $passApiKey, $idUser, $colegioId, $areaId, $materiasId)
    {
    	$url = $urlApi."/materia/materias_area/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('colegioId'=>$colegioId, 'areaId'=>$areaId, 'materiasId'=>$materiasId),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);
		//$response = json_decode($response);
		//print_r($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}


	public function delete_areamat_curl($urlApi, $passApiKey, $idUser, $idareamat)
    {
    	$url = $urlApi."/materia/delete_areamateria/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('idareamat'=>$idareamat),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);
		//$response = json_decode($response);
		//print_r($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}



	//---------------------------------------- AJAX request from WEB -------------------------------------------

	public function get_materias_x_area_ajax(){

	    $urlApi = $this->variables->get_urlapi();
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');

		$idcole = $_POST['idcole'];
        $idarea = $_POST['idarea'];
        $rol = $_POST['rol'];
        $idrol = $_POST['idrol'];


		$result=$this->get_materias_x_area_curl($urlApi, $passApiKey, $idusuario, $idcole, $idrol, $rol, $idarea);

		//print_r($result);

		if( (isset($result->status)) && ($result->status=='') ){
			$arre = array(
				'status' => 0,
             	'message' => 'No se encontraron materias'
			);
			$arre=json_encode($arre);
			print_r($arre);
		}
		else
			print_r($result);
	}

	public function add_materias_x_area_ajax(){

	    $urlApi = $this->variables->get_urlapi();
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');

		$idcolegio = $_POST['colegio'];
        $idarea = $_POST['area'];
        $materias = $_POST['materias'];


		$result=$this->add_materias_x_area_curl($urlApi, $passApiKey, $idusuario, $idcolegio, $idarea, $materias);
		//print_r($result);

		if( (isset($result->status)) && ($result->status=='') ){
			$arre = array(
				'status' => 0,
             	'message' => 'No se encontraron materias'
			);
			$arre=json_encode($arre);
			print_r($arre);
		}
		else
			print_r($result);
	}


	public function delete_aniomat_ajax(){

	    $urlApi = $this->variables->get_urlapi();
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');

		$idareamat = $_POST['idareamat'];


		$result=$this->delete_areamat_curl($urlApi, $passApiKey, $idusuario, $idareamat);
		print_r($result);
	}



} // fin Controller Alumno.