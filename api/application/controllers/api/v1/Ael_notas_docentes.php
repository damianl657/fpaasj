<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . '/libraries/REST_Controller.php';
    class Ael_notas_docentes extends REST_Controller {
    function __construct()
    {
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        parent::__construct();

        $this->load->model("modelo_alumno");

        $this->load->model("modelo_tutor");

        $this->load->model("modelo_usuario");

        $this->load->model("modelo_inscripciones");

        $this->load->model("modelo_ael_notas");
        $this->load->model("modelo_colegio");
    }



    public function obtener_divisiones_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            // Hago lo que debe hacer el metodo especificamente
            //$docuemnto = $this->get('documento'); // documento_docente
            $user_id= $this->get('user_id');
            $id_colegio = $this->get('id_colegio'); // documento_docente
            $user = $this->modelo_usuario->get_users_id($user_id);
            $colegio = $this->modelo_colegio->get_colegio($id_colegio);
            if($colegio != false)
            {
               // print_r($colegio->row());
                if($colegio->row()->id == 9)//escuela industrial
                {
                    $url ="https://industrialsarmiento.edu.ar/api_ael/index.php/docentes/obtener_divisiones/".$user->documento;
                }
                else
                {
                    
                    $link_ael = $colegio->row()->link;
                    //$url = $link_ael."/ael_api/docentes/obtener_divisiones/".$user->documento;
                    if($user->documento == 987654329)
                    {
                        $url = $link_ael."/ael_api/docentes/obtener_divisiones/14711964";
                    }
                    else
                    {
                        if($colegio->row()->tipo == 'Primaria')
                        {
                            $url = $link_ael."/ael_api/docentes/obtener_divisiones_primaria/".$user->documento;
                        }
                        else
                        {
                            $url = $link_ael."/ael_api/docentes/obtener_divisiones/".$user->documento;
                        }
                        
                        
                    }
                }
            }
            if($id_colegio == 9)// el curl va a otro lado en la escuela industrial
            {
                $ch = curl_init();
                    $options = array(
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_URL => $url,
                       // CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","code: $code"),
                        CURLOPT_POST => true,
                       // CURLOPT_POSTFIELDS => array('code' => $code),
                        CURLOPT_SSL_VERIFYPEER=> false,
                        );

                    curl_setopt_array( $ch, $options);
                    $response = curl_exec($ch);
                    curl_close($ch);
                    $response2 = json_decode($response);
                   // print_r($response2->padre_bloqueado);
                    
                        $this->response(['status' => true,
                        'url' => $url,
                        'message' => 'OK',
                        'data' => json_decode($response),
                        //'dato_alu' => $user,
                        'data_sin_json' => $response,
                        
                        ], REST_Controller::HTTP_BAD_REQUEST);
            }
            else
            {
               // $url = $link_ael."/ael_api/docentes/obtener_divisiones/".$docuemnto;
                    
                    
                    //print_r( $url);
                    $ch = curl_init();
                    $options = array(
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_URL => $url,
                       // CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","code: $code"),
                        CURLOPT_POST => true,
                       // CURLOPT_POSTFIELDS => array('code' => $code),
                        CURLOPT_SSL_VERIFYPEER=> false,
                        );

                    curl_setopt_array( $ch, $options);
                    $response = curl_exec($ch);
                    curl_close($ch);
                    $response2 = json_decode($response);
                   // print_r($response2->padre_bloqueado);
                    
                        $this->response(['status' => true,
                        'url' => $url,
                        'message' => 'OK',
                        'data' => json_decode($response),
                        //'dato_alu' => $user,
                        'data_sin_json' => $response,
                        
                        ], REST_Controller::HTTP_BAD_REQUEST);
                    
            }
        }

    }
    public function obtener_promedios_x_alu_x_periodo_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            // Hago lo que debe hacer el metodo especificamente
            //$docuemnto = $this->get('documento'); // documento_docente
            $id_colegio = $this->get('id_colegio'); // documento_docente
            $id_division = $this->get('id_division'); // id_division de ael
            $id_periodo = $this->get('id_periodo'); // id_periodo de ael
            $id_materia = $this->get('id_materia'); // id_materia de ael
            $id_docente = $this->get('id_docente'); // id_docente de ael

            //$user = $this->modelo_ael_notas->obtener_datos_alumno($idalumno);
            $colegio = $this->modelo_colegio->get_colegio($id_colegio);
            if($colegio != false)
            {
                //print_r($colegio->row());
                if($colegio->row()->id == 9)//escuela industrial
                {
                   // echo "crik crik crik";
                    $url ="https://industrialsarmiento.edu.ar/api_ael/index.php/docentes/obtener_promedios_x_alu_x_periodo/";
                    
                }
                else
                {
                    
                    $link_ael = $colegio->row()->link;
                   // $url = $link_ael."/ael_api/docentes/obtener_promedios_x_alu_x_periodo/";
                 //  echo $colegio->row()->tipo;
                   //die();
                    if($colegio->row()->tipo == 'Primaria')
                        {

                            $url = $link_ael."/ael_api/docentes/obtener_promedios_x_alu_x_periodo_primaria/";
                        }
                        else
                        {
                            $url = $link_ael."/ael_api/docentes/obtener_promedios_x_alu_x_periodo/";
                        }

                    
                }
            }
            if($id_colegio == 9)// el curl va a otro lado en la escuela industrial
            {
                $ch = curl_init();
                    $options = array(
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_URL => $url,
                       // CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","code: $code"),
                        CURLOPT_POST => true,
                        CURLOPT_POSTFIELDS => array('id_division' => $id_division, 'id_colegio' => $id_colegio, 'id_periodo' => $id_periodo, 'id_materia' => $id_materia, 'id_docente' => $id_docente),
                        CURLOPT_SSL_VERIFYPEER=> false,
                        );

                    curl_setopt_array( $ch, $options);
                    $response = curl_exec($ch);
                    curl_close($ch);
                    $response2 = json_decode($response);
                   // print_r($response2->padre_bloqueado);
                        $this->response(['status' => true,
                        'message' => 'OK',
                        'data' => json_decode($response),
                        //'dato_alu' => $user,
                        'data_sin_json' => $response,
                        'url' => $url,
                        ], REST_Controller::HTTP_BAD_REQUEST);
            }
            else
            {
               // $url = $link_ael."/ael_api/docentes/obtener_divisiones/".$docuemnto;
                    
                    
                    //print_r( $url);
                    $ch = curl_init();
                    $options = array(
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_URL => $url,
                       // CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","code: $code"),
                        CURLOPT_POST => true,
                        CURLOPT_POSTFIELDS => array('id_division' => $id_division, 'id_colegio' => $id_colegio, 'id_periodo' => $id_periodo, 'id_materia' => $id_materia, 'id_docente' => $id_docente),
                        CURLOPT_SSL_VERIFYPEER=> false,
                        );

                    curl_setopt_array( $ch, $options);
                    $response = curl_exec($ch);
                    curl_close($ch);
                    $response2 = json_decode($response);
                   // print_r($response2->padre_bloqueado);
                        $this->response(['status' => true,
                        'message' => 'OK',
                        'data' => json_decode($response),
                        //'dato_alu' => $user,
                        'data_sin_json' => $response,
                        'url' => $url,
                        ], REST_Controller::HTTP_BAD_REQUEST);
                    
            }
        }
    }
    public function obtener_evaluaciones_x_alu_x_periodo_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
        else
        {
            // Hago lo que debe hacer el metodo especificamente
            //$docuemnto = $this->get('documento'); // documento_docente
            $id_colegio = $this->get('id_colegio'); // documento_docente
            $id_division = $this->get('id_division'); // id_division de ael
            $id_periodo = $this->get('id_periodo'); // id_periodo de ael
            $id_materia = $this->get('id_materia'); // id_materia de ael
            $id_docente = $this->get('id_docente'); // id_docente de ael
            $id_alumno = $this->get('id_alumno');
            $colegio = $this->modelo_colegio->get_colegio($id_colegio);
            if($colegio != false)
            {
                //print_r($colegio->row());
                if($colegio->row()->id == 9)//escuela industrial
                {
                    //echo "crik crik crik";
                    $url ="https://industrialsarmiento.edu.ar/api_ael/index.php/docentes/obtener_evaluaciones_x_alu_x_periodo/";
                }
                else
                {
                    
                    $link_ael = $colegio->row()->link;
                  //  $url = $link_ael."/ael_api/docentes/obtener_evaluaciones_x_alu_x_periodo/";
                    if($colegio->row()->tipo == 'Primaria')
                        {

                            $url = $link_ael."/ael_api/docentes/obtener_evaluaciones_x_alu_x_periodo_primaria/";
                        }
                        else
                        {
                            $url = $link_ael."/ael_api/docentes/obtener_evaluaciones_x_alu_x_periodo/";
                        }
                }
            }
            if($id_colegio == 9)// el curl va a otro lado en la escuela industrial
            {
                $ch = curl_init();
                    $options = array(
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_URL => $url,
                       // CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","code: $code"),
                        CURLOPT_POST => true,
                        CURLOPT_POSTFIELDS => array('id_division' => $id_division, 'id_colegio' => $id_colegio, 'id_periodo' => $id_periodo, 'id_materia' => $id_materia, 'id_docente' => $id_docente, 'id_alumno' => $id_alumno),
                        CURLOPT_SSL_VERIFYPEER=> false,
                        );

                    curl_setopt_array( $ch, $options);
                    $response = curl_exec($ch);
                    curl_close($ch);
                    $response2 = json_decode($response);
                   // print_r($response2->padre_bloqueado);
                        $this->response(['status' => true,
                        'message' => 'OK',
                        'data' => json_decode($response),
                        //'dato_alu' => $user,
                        'data_sin_json' => $response,
                        'url' => $url,
                        ], REST_Controller::HTTP_BAD_REQUEST);
            }
            else
            {
                $ch = curl_init();
                    $options = array(
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_URL => $url,
                       // CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","code: $code"),
                        CURLOPT_POST => true,
                        CURLOPT_POSTFIELDS => array('id_division' => $id_division, 'id_colegio' => $id_colegio, 'id_periodo' => $id_periodo, 'id_materia' => $id_materia, 'id_docente' => $id_docente, 'id_alumno' => $id_alumno),
                        CURLOPT_SSL_VERIFYPEER=> false,
                        );

                    curl_setopt_array( $ch, $options);
                    $response = curl_exec($ch);
                    curl_close($ch);
                    $response2 = json_decode($response);
                   // print_r($response2->padre_bloqueado);
                        $this->response(['status' => true,
                        'message' => 'OK',
                        'data' => json_decode($response),
                        //'dato_alu' => $user,
                        'data_sin_json' => $response,
                        'url' => $url,
                        ], REST_Controller::HTTP_BAD_REQUEST);
            }

        }
    }

    

    



    

}