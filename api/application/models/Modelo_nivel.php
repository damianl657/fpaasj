<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_nivel extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }

        public function obtener_niveles()
        {
            $sql = "
                SELECT *
                FROM niveles
                ORDER BY nombre ASC
            ";
            $query = $this->db->query($sql);
            return $query->result();
        }


        public function obtener_niveles_cargados($idcolegio, $idcicloa)
        {
            $sql = "SELECT nodonivel.id as idnodonivel,niveles_id, niveles.nombre as nombre_nivel, nodonivel.esp_id, especialidades.nombre as nombre_especialidad, ultimo
                    FROM nodonivel 
                    JOIN niveles ON nodonivel.niveles_id = niveles.id 
                    LEFT JOIN especialidades ON nodonivel.esp_id = especialidades.id 
                    WHERE nodocolegio_id = $idcolegio 
                    AND nodonivel.cicloa_id = $idcicloa
                    AND nodonivel.estado=1
                    ";    

            $query = $this->db->query($sql);
            //return $query->result();   
             if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }


         public function obtener_nivel($name)
        {
            $sql = "
                SELECT id
                FROM niveles
                WHERE nombre = '$name'           
            ";
            $query = $this->db->query($sql);
            return $query->result();
        }

        public function obtener_nivel_id($id)
        {
            $sql = "
                SELECT id
                FROM niveles
                WHERE id = '$id'           
            ";
            $query = $this->db->query($sql);
            return $query->row();
        }

        public function insertar_nivel($data)
        {
            $query = $this->db->insert("niveles",$data);
            return $query;
        }

        public function insertar_nodonivel($data)
        {
            $query = $this->db->insert("nodonivel",$data);
            return $query;
        }
        
        
        public function update_estado_nodonivel($idnodonivel, $estadoNew) 
        {
            $sql = "UPDATE nodonivel SET estado=$estadoNew WHERE id=$idnodonivel ";
            $query = $this->db->query($sql);
            return $query;
        }

       
        public function get_nodonivel($colegio_id, $nivel_id, $cicloa_id)
        {
            $sql="SELECT *
                  FROM nodonivel
                  WHERE nodocolegio_id = $colegio_id  
                  AND  niveles_id = $nivel_id
                  AND  cicloa_id = $cicloa_id      
                 ";
            $query = $this->db->query($sql);
            //return $query->result();
            return $query->result();
            
        }

        public function get_nodonivelXid($id)
        {
            $sql="SELECT *
                  FROM nodonivel
                  WHERE id = $id  
                  AND  estado=1    
                 ";
            $query = $this->db->query($sql);
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }


        /*public function get_nodonivel_sig($id, $colegio, $ciclo)
        {
            //dado un id traigo el proximo. por seguridad
            $sql = "SELECT * FROM nodonivel WHERE id>$id  
                    AND nodocolegio_id=$colegio 
                    AND cicloa_id=$ciclo
                    ORDER BY id LIMIT 1  
                    ";
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->row();
            }
            else{
                return false;
            }    
        }*/

        public function get_nodonivel_sig($nivel, $colegio, $ciclo)
        { 
            $sql = "SELECT * FROM nodonivel 
                    WHERE niveles_id = $nivel  
                    AND nodocolegio_id = $colegio 
                    AND cicloa_id = $ciclo
                    ORDER BY id LIMIT 1  
                    ";
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->row();
            }
            else{
                return false;
            }    
        }

        public function get_nivel_sig($orden)
        {
            $sql = "SELECT * FROM niveles WHERE orden>$orden ORDER BY orden ASC LIMIT 1";
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->row();
            }
            else{
                return false;
            }    
        }
       



        /*public function obtener_NivConEsp($idcolegio,  $anioA)
        {   
            $sql = "SELECT nodonivel.id as nodonivel_id, nodoespecialidad.id as nodoesp_id, niveles.nombre as nombre_nivel, especialidades.nombre as nombre_especialidad
                    FROM  nodonivel 
                    JOIN nodoespecialidad ON nodoespecialidad.nodonivel_id = nodonivel.id 
                    JOIN niveles ON niveles.id=nodonivel.niveles_id 
                    JOIN especialidades ON especialidades.id= nodoespecialidad.especialidad_id
                    JOIN cicloa ON nodonivel.cicloa_id= cicloa.id              
                    AND cicloa.cicloa=$anioA
                    AND nodonivel.nodocolegio_id=$idcolegio
                    AND  nodonivel.estado=1
                    AND  nodoespecialidad.estado=1              
            ";
            
            $query = $this->db->query($sql);
            return $query->result();
        }


         public function obtener_NivSinEsp($idcolegio,  $anioA)
        {   
            //OPERACION DIFERENCIA. la segunda tabla solo debe tener una columna
            $sql = "SELECT nodonivel.id as nodonivel_id, niveles.nombre as nombre_nivel
                    FROM  nodonivel 
                    JOIN niveles ON niveles.id=nodonivel.niveles_id 
                    JOIN cicloa ON nodonivel.cicloa_id= cicloa.id
                    AND cicloa.cicloa=$anioA
                    AND nodonivel.nodocolegio_id=$idcolegio
                    AND  nodonivel.estado=1 

                    AND nodonivel.id NOT IN

                    (SELECT nodonivel.id
                    FROM  nodonivel 
                    JOIN nodoespecialidad ON nodoespecialidad.nodonivel_id = nodonivel.id 
                    JOIN niveles ON niveles.id=nodonivel.niveles_id 
                    JOIN cicloa ON nodonivel.cicloa_id= cicloa.id
                    AND cicloa.cicloa=$anioA
                    AND nodonivel.nodocolegio_id=$idcolegio
                    AND  nodonivel.estado=1
                    AND  nodoespecialidad.estado=1   
                    GROUP BY nodonivel_id)
                      ";
            
            $query = $this->db->query($sql);
             return $query->result();
        }*/


        public function nivelesXanios($idcolegio, $anios, $planA) //niveles con especialidad
        {   
            /*$sql = "SELECT nodonivel.id as nodonivel_id, nodoespecialidad.id as nodoesp_id, niveles.nombre as nombre_nivel, especialidades.nombre as nombre_especialidad
                    FROM  nodonivel 
                    JOIN nodoespecialidad ON nodoespecialidad.nodonivel_id = nodonivel.id 
                    JOIN niveles ON niveles.id = nodonivel.niveles_id 
                    JOIN especialidades ON especialidades.id = nodoespecialidad.especialidad_id            
                    JOIN nodoanios ON nodoanios.nodoniv_id = nodonivel.id            
                    WHERE nodoanios.id IN ($anios)
                    AND nodonivel.nodocolegio_id=$idcolegio
                    AND  nodonivel.estado=1
                    AND  nodoespecialidad.estado=1 
                    GROUP BY nodonivel.id             
                ";*/

            $sql = "SELECT nodonivel.id as idnodonivel,niveles_id, niveles.nombre as nombre_nivel, nodonivel.esp_id, especialidades.nombre as nombre_especialidad
                    FROM  nodoanios 
                    JOIN nodonivel ON nodoanios.nodoniv_id = nodonivel.id 
                    JOIN niveles ON niveles.id = nodonivel.niveles_id
                    LEFT JOIN especialidades ON nodonivel.esp_id = especialidades.id
                    WHERE nodoanios.id IN ($anios)
                    AND nodonivel.nodocolegio_id=$idcolegio
                    AND nodonivel.cicloa_id = $planA
                    AND  nodonivel.estado=1
                    GROUP BY nodonivel.id
                ";
            
            $query = $this->db->query($sql);
            return $query->result();
        }


        /*public function nivelesSEXanios($idcolegio, $anios) //niveles sin especialidad
        {   
            //OPERACION DIFERENCIA. la segunda tabla solo debe tener una columna
            $sql = "SELECT nodonivel.id as nodonivel_id, niveles.nombre as nombre_nivel
                    FROM  nodonivel 
                    JOIN niveles ON niveles.id=nodonivel.niveles_id 
                    JOIN nodoanios ON nodoanios.nodoniv_id = nodonivel.id   
                    WHERE nodoanios.id IN ($anios)
                    AND nodonivel.nodocolegio_id=$idcolegio
                    AND  nodonivel.estado=1 

                    AND nodonivel.id NOT IN

                    (   SELECT nodonivel.id
                        FROM  nodonivel 
                        JOIN nodoespecialidad ON nodoespecialidad.nodonivel_id = nodonivel.id 
                        JOIN niveles ON niveles.id=nodonivel.niveles_id 
                        JOIN nodoanios ON nodoanios.nodoniv_id = nodonivel.id   
                        WHERE nodoanios.id IN ($anios)
                        AND nodonivel.nodocolegio_id=$idcolegio
                        AND  nodonivel.estado=1
                        AND  nodoespecialidad.estado=1   
                        GROUP BY nodonivel_id)
                    GROUP BY nodonivel.id 
                      ";
            
            $query = $this->db->query($sql);
             return $query->result();
        }*/
        public function get_all_niveles()
        {
            $sql ="SELECT * FROM `niveles`";
            $query = $this->db->query($sql);    
            return $query;
        }

       

	}