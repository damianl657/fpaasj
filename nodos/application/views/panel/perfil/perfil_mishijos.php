

<script> 


  function abrir_modal_editar_alu(id_alu)
  {
    $("#documento_txt_edit").val('');
    $("#apellido_txt_edit").val('');
    $("#nombre_txt_edit").val('');
    $("#email_txt_edit").val('');
    $("#user_id_edit").val('');
    $("#username_txt_edit").val('');

    $("#radio_masculino_edit").attr('checked', false);
    $("#radio_femenino_edit").attr('checked', false);


    
    var urlRequest = urlApi+"/usuario/obtener_user/"; 
      $.ajax({
              url: urlRequest,
                type: 'POST',
                headers: {              
                 'APIKEY' : passApiKey,
                 'userid' : idUser,
                 'Access-Control-Allow-Origin': '*'
              },
              data :{id_user:id_alu},
              
              success : function(data) 
              {   
                  //console.log(data);
                 // users = data.users;

                  $("#documento_txt_edit").val(data["documento"]);
                  $("#apellido_txt_edit").val(data["last_name"]);
                  $("#nombre_txt_edit").val(data["first_name"]);
                  $("#email_txt_edit").val(data["email"]);
                  $("#username_txt_edit").val(data["username"]);
                  //alert(data["id"]);
                  $("#user_id_edit").val(data["id"]);
                  
                  if(data['sexo'] == 'F') 
                  {
                    $("#radio_femenino_edit").prop('checked', true);
                    //jQuery("#radio_masculino_edit").attr('checked', false);
                  }
                  else if(data['sexo'] == 'M')
                      {
                        $("#radio_masculino_edit").prop('checked', true);
                       // jQuery("#radio_femenino_edit").attr('checked', false);
                      }

                  $("#modal_editar_usuario").modal('show');  
                  
              },
              error: function(response)
              {
                  //hacer algo cuando ocurra un error
                  console.log(response);
              }
      }); 

  }

  function abrir_modal_editar_password(id_alu)
  {
    $("#user_id_reset").val(id_alu);
    $("#modal_reset_clave").modal('show');
    //$('#modal_cambio_contraseña').modal('show');
  }



  function generar_datatable_hijos()
  {
    var oTable;
    //console.log(id_rol);
      var handleDataTableButtons3 = function() {
        if ($("#table_usuarios").length) {
          oTable = $("#table_usuarios").DataTable({
             
            responsive: true,
            keys: true,
            deferRender: true,
            scrollCollapse: true,
            scroller: true,
            
            "language": {
                          "sProcessing":     "Procesando...",
                          "sLengthMenu":     "Mostrar _MENU_ registros",
                          "sZeroRecords":    "No se encontraron resultados",
                          "sEmptyTable":     "Ningún dato disponible en esta tabla",
                          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                          "sInfoPostFix":    "",
                          "sSearch":         "Buscar:",
                          "sUrl":            "",
                          "sInfoThousands":  ",",
                          "sLoadingRecords": "Cargando...",
                          "oPaginate": {
                              "sFirst":    "Primero",
                              "sLast":     "Último",
                              "sNext":     "Siguiente",
                              "sPrevious": "Anterior"
                          },
                          "oAria": {
                              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                          } 
                        }
          });
        }
      };
      TableManageButtons3 = function() {
        "use strict";
        return {
          init: function() {

            handleDataTableButtons3();
          }
        };
      }();
      /*var table = $('#datatable-fixed-header').DataTable({
        fixedHeader: true,

      });*/
      
      TableManageButtons3.init();

      /*oTable.$('td').editable( '../example_ajax.php', {
      "callback": function( sValue, y ) {
          var aPos = oTable.fnGetPosition( this );
          oTable.fnUpdate( sValue, aPos[0], aPos[1] );
      },
      "submitdata": function ( value, settings ) {
          return {
              "row_id": this.parentNode.getAttribute('id'),
              "column": oTable.fnGetPosition( this )[2]
          };
      },

      "width": "90%",
      "height": "100%"
  } );*/
  }


  function traer_hijos()
  {
    var urlRequest = urlApi+"/tutor/obtener_hijos/"; 
    $.ajax({
          url: urlRequest,
            type: 'GET',
            headers: {              
             'APIKEY' : passApiKey,
             'userid' : idUser,
             'Access-Control-Allow-Origin': '*'
          },
          data :{},
          
          success : function(data) 
          {   
              var users = jQuery.parseJSON( data.alumnos ); 
             
              //console.log(users);
              $("#tbody_hijos tr").remove();

              
              for(var i=0; i < users.length ; i++) 
              {
                  //var id_evento = datos[i]['id'];
                  
                  //console.log(users[i]);

                  var fila = '<tr id="fila_hijo_'+users[i]['alumno_id']+'">'+
                                  '<td>'+(i+1)+'</td>'+
                                  '<td>'+users[i]['apellido_alumno']+'</td>'+
                                  '<td>'+users[i]['nombre_alumno']+'</td>'+
                                  '<td>'+users[i]['documento']+'</td>'+
                                  '<td>'+users[i]['username']+'</td>'+
                                  '<td>'+users[i]['email']+'</td>'+
                                  '<td>'+users[i]['sexo']+'</td>'+
                                  '<td>'+users[i]['anio']+' ' +users[i]['nombre_division']+'</td>'+
                                  
                                  '<td><center>';

                  if (editaPerfil == 1)
                      fila = fila +
                                    '<button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="btn_alumnos" title="Editar" onclick="abrir_modal_editar_alu('+users[i]['alumno_id']+')" ><i class="glyphicon glyphicon-pencil"></i></button> ';

                   if (editaContrasenia == 1)
                      fila = fila +
                                     '<button class="btn btn-sm btn-danger pull-left m-t-n-xs" id="btn_alumnos" title="Resetear Clave" onclick="abrir_modal_editar_password('+users[i]['alumno_id']+')" type="button"><i class="glyphicon glyphicon-retweet"></i></button>';
                                     
                  fila = fila + '</center></td></tr>';
                  //editaPerfil
                  //editaContrasenia

                  $("#tbody_hijos").append(fila);

                  
              }
              //generar_datatable_hijos();
              
          },
          error: function(response)
          {
              //hacer algo cuando ocurra un error
              console.log(response);
          }
    }); 
  }


  function resetear_clave()
  {
      var urlRequest = urlApi+"/Usuario/reset_clave_usuario";  
      // 
      $('#form_reset').validate
      ({
          /*rules: {
              user_id_reset: { required: true},
              
          },
          messages: {
              user_id_reset:  {required: "Debe introducir el Nombre."}, 
              
          },*/
          submitHandler: function(form)
          {
              
              $.ajax
              ({
                  url: urlRequest,
                  type: 'POST',
                  headers: {              
                   'APIKEY' : passApiKey,
                   'userid' : idUser,
                   'Access-Control-Allow-Origin': '*'},
                  data: $('#form_reset').serialize(),
                  success: function(data)
                  {
                      if(data.status == true)
                      {
                        $.notify(data.message, "success");
                        $('#modal_reset_clave').modal('hide'); 
                      }
                      else
                      {
                        bootbox.alert(data.message);
                      }
                      
                  },
                  error: function (jqXHR, textStatus, errorThrown,data)
                  {
                      //document.getElementById("myModalLabel").innerHTML = 'Registro de Usuarios';
                          //document.getElementById("mensaje").innerHTML = 'ERROR';
                          //$("#myModal").modal('show');
                      console.log(jqXHR);
                      console.log(textStatus);
                      console.log(errorThrown);
                      //console.log(data);
                  }
              });
          }
      });
  }


  $(document).ready(function()
  { 
    traer_hijos();
    
    $('#reset_clave_usuario').click(function()
    { //alert('hola');
        resetear_clave();
    });

    /*$('#update_usuario').click(function(){
        editar_usuario();
    });*/

  });
  </script>



<div class="wrapper wrapper-content animated fadeIn">
            <div class="row animated">

                <div class="col-lg-7 col-md-8 col-sm-10 col-xs-12 col-centered">
                    <div class="ibox float-e-margins">
                        <!--<div class="ibox-title">
                            <center><h5>Perfil</h5></center>
                        </div>-->

                        <div class="ibox-title collapse-link" id="tituloperfil" style="cursor: pointer;">
                            <h5>Mis Hijos <small> Editar Datos Personales de Mis Hijos</small></h5>
                            
                            <div class="ibox-tools">
                                <a class="">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        
                        <div>
                            <div class="ibox-content no-padding border-left-right">
                            	 
                                
                            </div>
                      
                            
                            <div  class="ibox-content profile-content panel-body table-responsive"> <!-- class="ibox-content profile-content " -->
                                
                                <table id="table_usuarios" class="table table-striped table-bordered ">
                                      <thead>
                                        <tr>
                                          <th>#</th>
                                          <th>Apellido</th>
                                          <th>Nombre</th>
                                          <th>Documento</th>
                                          <th>Username</th>
                                          <th>Email</th>
                                          <th>Sexo</th>
                                          <th>Divisi&oacute;n</th>
                                        </tr>
                                      </thead>
                                      <tbody id="tbody_hijos">

                                      </tbody>
                                    
                                  </table>
                                
                               
                                       <?php //var_dump($this->session->userdata('menu'));
                                          $editaPerfil = 0; $editaContrasenia = 0; 
                                          foreach ($this->session->userdata('menu') as $boton) 
                                          {   
                                              if($boton->tipo == 'boton')
                                              {
                                                  if( $boton->nombre == 'Editar_Perfil' )
                                                  {
                                                     $editaPerfil = 1; 
                                                  }
                                                  else
                                                    if( $boton->nombre == 'Cambiar_Contraseña' )
                                                    {
                                                         $editaContrasenia = 1; 
                                                    }
                                              }
                                              //print_r($menu);
                                          }
                                        ?>
                                  <script >
                                    editaPerfil = '<?php echo $editaPerfil; ?>';
                                    editaContrasenia = '<?php echo $editaContrasenia; ?>';
                                  </script>

                            </div>
                    	</div>
                	</div>
            	</div>
    		</div>
</div>





<!-- modal REsetear clave usuario-->
<div class="modal fade" id="modal_reset_clave" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Resetear Clave de Usuario</h4>
      </div>
      <center>
      <form id="form_reset" autocomplete="on" method="post" class="form-horizontal form-label-left">
        <input type="hidden" id="user_id_edit" name="user_id_edit" value="0" />
        <div class="modal-body">
        
           <input type="hidden" id="user_id_reset" name="user_id_reset" value="0" />
         
          
          
          <div class="form-group">
            <h3 class="col-sm-8 control-label">Seleccione la Clave Inicial</h3>

           
          </div>
          <div class="form-group">
           
            <center>
            <div class="col-sm-8 control-label">
                  <div class="i-checks"><label>DNI (La clave Inicial será el DNI)  <input type="radio" value="dni" name="clave_txt" id="radio_dni">  <i></i>  </label></div>
                  <div class="i-checks"><label>EMAIL (La clave Inicial será la primer parte del Email antes del @) <input type="radio" checked="" value="email" name="clave_txt" id="radio_email"> <i></i>  </label></div>
            </div>
            </center>
          </div>
              
        
      </div>
        <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                
                 <button class="btn btn-primary" id ="reset_clave_usuario" >Resetear Clave</button>
      </form>
      </center>
      </div>
    </div>
  </div>
</div>
</div>








        
