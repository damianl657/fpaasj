<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';


    class Division extends REST_Controller {

    function __construct()
    {
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        parent::__construct();
        $this->load->model("modelo_division");
        $this->load->model("modelo_doc_div_mat_alu");
        $this->load->model("Modelo_permisos");
       
        // Configurar limites para cada uno de los metodos, no solo controlador
        // La tabla limits tiene que estar creada y la opcion limits TRUE en application/config/rest.php
        
        //$this->methods['tutores_get']['limit'] = 1; // 500 peticiones por hora por usuario/key
        //$this->methods['user_post']['limit'] = 100; // 100 peticiones por hora por usuario/key
        //$this->methods['user_delete']['limit'] = 50; // 50 peticiones por hora por usuario/key
    }


    public function insertdivisiones_post()
    {
        $userid = $this->utilidades->verifica_userid();
    
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $data = json_decode($_POST['data']) ;
            $idusergrupo = $_POST['idusergrupo']; 

            $anioA = $this->utilidades->obtener_planestudioaXcolegio( $idcolegio );
            $planest_Id = $this->utilidades->get_planestudio( $anioA ); 
      
            foreach ($data as $key ) 
            { 
                //var_dump($key);
                $res = $this->modelo_division->get_division($key->anios_id, $key->nombre, $planest_Id); 
                
                if (!$res) //si no existe inserto      
                {          
                    $key->planesestudio_id = $planest_Id;
                    $this->modelo_division->insertar_nododivision($key);
                }
                else{ //existe
                    foreach ($res as $row )
                        if ($row->estado==0) //si existe pero fue dada de baja, vuelvo a habilitar
                            $result = $this->modelo_division->update_estado_division($row->id,1);
                    }               
            }
            
            $this->load->model("modelo_usuario");
            $result2 = $this->modelo_usuario->update_pasos($idusergrupo,'4'); 

            if ($result2)
            {
                $this->response([
                        'status' => TRUE,
                        'message' => 'Paso 4 Guardado', 
                    ], REST_Controller::HTTP_OK);  
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Error al Guardar Paso 4'
                ]); // NOT_FOUND (404) being the HTTP response code
            }   
        }    
    }

    
    public function divisiones_cargadas_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {

            $parametros = $_GET['parametros'] ;
            $todos = $_GET['todos'] ;
            $nombregrupo = $_GET['nombregrupo']; 
            $idcolegio = $_GET['idcolegio']; 
            
            //$anioA = date('Y');
            $anioA = $this->utilidades->obtener_planestudioaXcolegio( $idcolegio );  
            //$anioA = '2016';
            $planest_Id = $this->utilidades->get_planestudio( $anioA ); 

            if($todos == 1) //traer todos los añios
            { 
                $nada="";  //POR AHORA NO SE USA
            }
            else
            {   
                $parametros = json_decode($parametros);
                foreach ($parametros as $row)
                { 
                    $divi = $this->modelo_division->get_divisionesXanios($row[0]->idnodoanio,$planest_Id);

                    $arre = array ( 'nombrenodoanio' => $row[0]->nombrenodoanio,
                                        'divisiones' => $divi
                                        );
                    $result[]=$arre;
                }    
            }

            //print_r($result);

           /*if( ($nombregrupo=='Institucion') || ($nombregrupo=='Directivo') || ($nombregrupo=='Nodos')|| ($nombregrupo=='Administrativo') || ($nombregrupo=='Gabinete') || ($nombregrupo=='Administrativo Gral') || ($nombregrupo=='Vice Directivo') || ($nombregrupo=='Regente Basico') || ($nombregrupo=='Regente Orientado') || ($nombregrupo=='Jefe Area') || ($nombregrupo=='Regente Alumnos') || ($nombregrupo=='Sub Regente Alumnos') || ($nombregrupo=='Jefe de Preceptores') || ($nombregrupo == 'Secretaria Inicial') || ($nombregrupo == 'Secretaria Primaria'))              
            {   //todos los niveles del colegio
                $resfinal = $result;
            }*/
            $div_todas = $this->Modelo_permisos->get_permiso_div_todas($idcolegio,$nombregrupo);
                if($div_todas == true)
                {
                    $resfinal = $result;
                }
            else if($nombregrupo=='Docente')
                 {
                    $this->load->model("modelo_division");
                    
                    $diviDoce = $this->modelo_division->divisionesXdocentes($idcolegio, $planest_Id, $userid);
                    foreach ($diviDoce as $key) {
                        $divis[] = $key->divisiones_id;
                    }
                    $divisiones = join(',',$divis);             
                    //print_r($diviDoce);
                    
                    foreach ($result as $clave) 
                    {
                        foreach ($clave['divisiones'] as $arredivi) 
                        {
                            foreach ($diviDoce as $key)
                            {
                                $divipermitido = $key->divisiones_id;
                           
                                if( $arredivi->id == $divipermitido )
                                {
                                    $divisfinal[]= $arredivi;
                                }
                            }
                        }

                        if( $divisfinal != null)
                        {
                            $arreaux = array ( 'nombrenodoanio' => $clave['nombrenodoanio'],
                                'divisiones' => $divisfinal
                                );
                            $resfinal[]= $arreaux;
                            $divisfinal = null;
                        }

                        $divisfinal=null;
                    }
     
                 }

                else if($nombregrupo=='Preceptor')
                 {
                    $this->load->model("modelo_division");
                    $diviPrece = $this->modelo_division->divisionesXpreceptores($idcolegio,  $planest_Id, $userid);
                    foreach ($diviPrece as $key) {
                        $divis[] = $key->division_id;
                    }
                    $divisiones = join(',', $divis);                
                    //print_r($diviPrece);
                    
                    foreach ($result as $clave) 
                    {
                        foreach ($clave['divisiones'] as $arredivi) 
                        {
                            foreach ($diviPrece as $key)
                            {
                                $divipermitido = $key->division_id;
                           
                                if( $arredivi->id == $divipermitido )
                                {
                                    $divisfinal[]= $arredivi;
                                }
                            }
                        }

                        if( $divisfinal != null)
                        {
                            $arreaux = array ( 'nombrenodoanio' => $clave['nombrenodoanio'],
                                'divisiones' => $divisfinal
                                );
                            $resfinal[]= $arreaux;
                            $divisfinal = null;
                        }

                        $divisfinal=null;
                    }
                    
                 }
                

            //print_r($resfinal);

            if ($resfinal)
            {
                $data = json_encode($resfinal);
                $this->response($data, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No se encontraron Divisiones'
                ], REST_Controller::HTTP_OK); 
            }
        }
    }


    public function baja_division_post()
    {
        $userid = $this->utilidades->verifica_userid();
    
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $iddiv = $_POST['iddiv'] ;

            //$anioA = $this->utilidades->obtener_planestudioaXcolegio( $idcolegio );
            //$planest_Id = $this->utilidades->get_planestudio( $anioA );

            $result2 = $this->modelo_division->update_estado_division($iddiv, 0); 
                      
            if ($result2)
            {
                $this->response([
                        'status' => TRUE,
                        'message' => 'Division dada de Baja', 
                    ], REST_Controller::HTTP_OK);  
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Error al Dar de Baja'
                ]); // NOT_FOUND (404) being the HTTP response code
            }   
        }    
    }

    public function obtener_divisiones_x_colegio_get(){
        
        $userid = $this->utilidades->verifica_userid();
        $idcolegio = $this->get('idcolegio'); 
        $nombregrupo = $this->get('nombregrupo'); 
        $arre;


        $anioA = $this->utilidades->obtener_planestudioaXcolegio( $idcolegio );
      
        $planest_Id = $this->utilidades->get_planestudio( $anioA ); 
        
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $div_todas = $this->Modelo_permisos->get_permiso_div_todas($idcolegio,$nombregrupo);

          /* if( ($nombregrupo=='Institucion') || ($nombregrupo=='Directivo') || ($nombregrupo=='Nodos')|| ($nombregrupo=='Administrativo') || ($nombregrupo=='Gabinete') || ($nombregrupo=='Administrativo Gral') || ($nombregrupo=='Vice Directivo') || ($nombregrupo=='Regente Basico') || ($nombregrupo=='Regente Orientado') || ($nombregrupo=='Jefe Area') || ($nombregrupo=='Regente Alumnos') || ($nombregrupo=='Sub Regente Alumnos') || ($nombregrupo=='Jefe de Preceptores') || ($nombregrupo=='Secretaria Inicial') || ($nombregrupo=='Secretaria Primaria'))              
            {   //todas las divisiones del colegio 
                $result2 = $this->modelo_division->get_divisiones_SinEspecialidad($idcolegio,$planest_Id);
                $result1 = $this->modelo_division->get_divisiones_ConEspecialidad($idcolegio,$planest_Id);
                //$result = $this->modelo_division->get_divisiones_SinEspecialidad($idcolegio);
                //$result = array_merge($result2, $result1);

                if( ($result1)&&($result2) )
                    $result = array_merge($result2, $result1);
                else  if($result1)
                        $result = $result1;
                      else 
                        if($result2)
                            $result = $result2;

            }*/
            $result= '';
            if($div_todas == true)
            {
               // echo "aa";
                $result2 = $this->modelo_division->get_divisiones_SinEspecialidad($idcolegio,$planest_Id);
                $result1 = $this->modelo_division->get_divisiones_ConEspecialidad($idcolegio,$planest_Id);
                //$result = $this->modelo_division->get_divisiones_SinEspecialidad($idcolegio);
                //$result = array_merge($result2, $result1);

                if( ($result1)&&($result2) )
                    $result = array_merge($result2, $result1);
                else  if($result1)
                        $result = $result1;
                      else 
                        if($result2)
                            $result = $result2;
            }
            else if($nombregrupo=='Docente')
                 {  
                    $this->load->model("modelo_division");
                    $diviDoce = $this->modelo_division->divisionesXdocentes($idcolegio,  $planest_Id, $userid);
                    foreach ($diviDoce as $key) {
                        $arre[] = $key->divisiones_id;
                    }
                    $divisiones = join(',',$arre);
                    
                    $result2 = $this->modelo_division->get_divisiones_SinEspecialidad_Xdivision($idcolegio, $divisiones,$planest_Id);
                    $result1 = $this->modelo_division->get_divisiones_ConEspecialidad_Xdivision($idcolegio, $divisiones,$planest_Id);
                    
                    if( ($result1)&&($result2) )
                            $result = array_merge($result2, $result1);
                        else  if($result1)
                                $result = $result1;
                              else 
                                if($result2)
                                    $result = $result2;
                 }
                 else if($nombregrupo=='Preceptor')
                     {
                        $this->load->model("modelo_division");
                        $diviPrece = $this->modelo_division->divisionesXpreceptores($idcolegio,  $planest_Id, $userid);
                        foreach ($diviPrece as $key) {
                            $arre[] = $key->division_id;
                        }
                        if($arre != null)
                        {
                            $divisiones = join(',',$arre);


                            $result2 = $this->modelo_division->get_divisiones_SinEspecialidad_Xdivision($idcolegio, $divisiones,$planest_Id);
                            $result1 = $this->modelo_division->get_divisiones_ConEspecialidad_Xdivision($idcolegio, $divisiones,$planest_Id);
                            
                            if( ($result1)&&($result2) )
                                $result = array_merge($result2, $result1);
                            else  if($result1)
                                    $result = $result1;
                                  else 
                                    if($result2)
                                        $result = $result2;
                        }
                            

                     }
                     //else echo "no entra";
                       
        
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
                $result = json_encode($result);
                $this->response($result, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No existen divisiones cargadas'
                ], REST_Controller::HTTP_OK); 
            }
        }
        
    }
    public function obtener_divisiones_x_colegio2_get(){
        
        $userid = $this->utilidades->verifica_userid();
        $idcolegio = $this->get('idcolegio'); 
        //$nombregrupo = $this->get('nombregrupo'); 
        $arre;

        $anioA = $this->utilidades->obtener_planestudioaXcolegio( $idcolegio );
        $planest_Id = $this->utilidades->get_planestudio( $anioA ); 
      
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {

            
                $result2 = $this->modelo_division->get_divisiones_SinEspecialidad($idcolegio,$planest_Id);
                $result1 = $this->modelo_division->get_divisiones_ConEspecialidad($idcolegio,$planest_Id);
               
                if( ($result1)&&($result2) )
                    $result = array_merge($result2, $result1);
                else  if($result1)
                        $result = $result1;
                      else 
                        if($result2)
                            $result = $result2;

            
            
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
                $result = json_encode($result);
                $this->response($result, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No existen divisiones cargadas'
                ], REST_Controller::HTTP_OK); 
            }
        }
        
    }
    public function obtener_divisiones_x_colegio_post(){
        
        $userid = $this->utilidades->verifica_userid();
        $idcolegio = $this->post('idcolegio'); 
        $nombregrupo = $this->post('nombregrupo'); 
        $arre;


        $anioA = $this->utilidades->obtener_planestudioaXcolegio( $idcolegio );
      
        $planest_Id = $this->utilidades->get_planestudio( $anioA ); 
        
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $div_todas = $this->Modelo_permisos->get_permiso_div_todas($idcolegio,$nombregrupo);

          /* if( ($nombregrupo=='Institucion') || ($nombregrupo=='Directivo') || ($nombregrupo=='Nodos')|| ($nombregrupo=='Administrativo') || ($nombregrupo=='Gabinete') || ($nombregrupo=='Administrativo Gral') || ($nombregrupo=='Vice Directivo') || ($nombregrupo=='Regente Basico') || ($nombregrupo=='Regente Orientado') || ($nombregrupo=='Jefe Area') || ($nombregrupo=='Regente Alumnos') || ($nombregrupo=='Sub Regente Alumnos') || ($nombregrupo=='Jefe de Preceptores') || ($nombregrupo=='Secretaria Inicial') || ($nombregrupo=='Secretaria Primaria'))              
            {   //todas las divisiones del colegio 
                $result2 = $this->modelo_division->get_divisiones_SinEspecialidad($idcolegio,$planest_Id);
                $result1 = $this->modelo_division->get_divisiones_ConEspecialidad($idcolegio,$planest_Id);
                //$result = $this->modelo_division->get_divisiones_SinEspecialidad($idcolegio);
                //$result = array_merge($result2, $result1);

                if( ($result1)&&($result2) )
                    $result = array_merge($result2, $result1);
                else  if($result1)
                        $result = $result1;
                      else 
                        if($result2)
                            $result = $result2;

            }*/
            $result= '';
            if($div_todas == true)
            {
               // echo "aa";
                $result2 = $this->modelo_division->get_divisiones_SinEspecialidad($idcolegio,$planest_Id);
                $result1 = $this->modelo_division->get_divisiones_ConEspecialidad($idcolegio,$planest_Id);
                //$result = $this->modelo_division->get_divisiones_SinEspecialidad($idcolegio);
                //$result = array_merge($result2, $result1);

                if( ($result1)&&($result2) )
                    $result = array_merge($result2, $result1);
                else  if($result1)
                        $result = $result1;
                      else 
                        if($result2)
                            $result = $result2;
            }
            else if($nombregrupo=='Docente')
                 {  
                    $this->load->model("modelo_division");
                    $diviDoce = $this->modelo_division->divisionesXdocentes($idcolegio,  $planest_Id, $userid);
                    foreach ($diviDoce as $key) {
                        $arre[] = $key->divisiones_id;
                    }
                    $divisiones = join(',',$arre);
                    
                    $result2 = $this->modelo_division->get_divisiones_SinEspecialidad_Xdivision($idcolegio, $divisiones,$planest_Id);
                    $result1 = $this->modelo_division->get_divisiones_ConEspecialidad_Xdivision($idcolegio, $divisiones,$planest_Id);
                    
                    if( ($result1)&&($result2) )
                            $result = array_merge($result2, $result1);
                        else  if($result1)
                                $result = $result1;
                              else 
                                if($result2)
                                    $result = $result2;
                 }
                 else if($nombregrupo=='Preceptor')
                     {
                        $this->load->model("modelo_division");
                        $diviPrece = $this->modelo_division->divisionesXpreceptores($idcolegio,  $planest_Id, $userid);
                        foreach ($diviPrece as $key) {
                            $arre[] = $key->division_id;
                        }
                        if($arre != null)
                        {
                            $divisiones = join(',',$arre);


                            $result2 = $this->modelo_division->get_divisiones_SinEspecialidad_Xdivision($idcolegio, $divisiones,$planest_Id);
                            $result1 = $this->modelo_division->get_divisiones_ConEspecialidad_Xdivision($idcolegio, $divisiones,$planest_Id);
                            
                            if( ($result1)&&($result2) )
                                $result = array_merge($result2, $result1);
                            else  if($result1)
                                    $result = $result1;
                                  else 
                                    if($result2)
                                        $result = $result2;
                        }
                            

                     }
                     //else echo "no entra";
                       
        
            // Si existe mas de un resultado, lo mando
            if ($result)
            {
                $result = json_encode($result);
                $this->response($result, REST_Controller::HTTP_OK);
            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No existen divisiones cargadas'
                ], REST_Controller::HTTP_OK); 
            }
        }
        
    }
    public function obtener_docentes_x_div_colegio_post(){
        $userid = $this->utilidades->verifica_userid();
        //$nombregrupo = $this->get('nombregrupo'); 
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $id_colegio = $this->post('id_colegio'); 
            $id_division = $this->post('id_division'); 
            $id_materia = $this->post('id_materia'); 
            
            
            $result = $this->modelo_doc_div_mat_alu->obtener_docentes_x_div_x_colegio($id_division,$id_colegio,$id_materia);
                

            
            
            // Si existe mas de un resultado, lo mando
            if ($result->num_rows() > 0)
            {
                $string_docentes = '';
                $i = 0;
                foreach ($result->result() as $key) 
                {
                    if($i == 0)
                    {
                        $string_docentes = $key->last_name.' '.$key->first_name;
                        $i++;
                    }
                    else
                    {
                        $string_docentes = $key->last_name.' '.$key->first_name.', '.$string_docentes;
                    }
                        
                }
                $array_result = array('cant_docentes' => $result->num_rows(), 'docentes' => $result->result(),'string_docentes' => $string_docentes, 'sql' => $this->db->last_query() );
                $array_result = json_encode($array_result);
                $this->response($array_result, REST_Controller::HTTP_OK);
                    
            }
            else
            {
                $string_docentes = 'Sin Docentes Asignados';
                $array_result = array('cant_docentes' => $result->num_rows(), 'docentes' => '','string_docentes' => $string_docentes, 'sql' => $this->db->last_query() );
                $array_result = json_encode($array_result);
                $this->response($array_result, REST_Controller::HTTP_OK);
            }
        }
        
    }

}
