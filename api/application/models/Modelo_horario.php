<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_horario extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }

        public function obtener_horario_por_dia($idalumno, $dia)
        {
            //obtengo la division en la que esta inscripto el alumno
            $division = $this->division->obtenerDivisionPorAlumno($idalumno);
            if ($division) {
                $sql = "SELECT horariosmaterias.id horarios_id, hora_inicio, nombresmateria.nombre materia
                    FROM horariosmaterias
                    JOIN horarios ON horarios.id = horariosmaterias.horarios_id
                    JOIN dias ON dias.id = horariosmaterias.dias_id
                    join anios_materias on horariosmaterias.materias_id = anios_materias.id 
                    join nombresmateria on nombresmateria.id = anios_materias.nombresmateria_id
                    WHERE divisiones_id = ".$division."
                    AND dias_id = ".$dia;
            $query = $this->db->query($sql);
            return $query->result();
            }
            else return null;
        }

        //Falta obtener el nombre del profesor
        public function obtener_horario($id)
        {
            $sql = "SELECT hora_inicio,
                        hora_fin,
                        divisiones.nombre curso,
                        horariosmaterias.lugar,
                        nombresmateria.nombre materia
                    FROM horariosmaterias
                    JOIN horarios ON horarios.id = horariosmaterias.horarios_id
                    JOIN dias ON dias.id = horariosmaterias.dias_id
                    JOIN nombresmateria ON nombresmateria.materias_id = horariosmaterias.materias_id
                    JOIN divisiones ON divisiones.id = horariosmaterias.divisiones_id
                    WHERE horariosmaterias.id = ".$id;
            $query = $this->db->query($sql);
            return $query->result();
        }        

        public function insertar_horario($data)
        {
            $query = $this->db->insert("horarios",$data);
            return $query;
        }

        public function insertar_horario_materia($data)
        {
            $query = $this->db->insert("horariosmaterias",$data);
            return $query;
        }
        
        public function update_horario_materia($data, $id)
        {
            $this->db->where('id', $id);
            $query = $this->db->update("horariosmaterias",$data);
            return $query;
        }
        
        public function delete_horario_materia($id)
        {
            $sql="DELETE FROM horariosmaterias WHERE id=$id";
            $query = $this->db->query($sql);
            return $query;
        }

        public function update_numero_horario($horarioId, $num) 
        {
            $sql = "UPDATE horarios SET numero='$num' WHERE id=$horarioId ";
            $query = $this->db->query($sql);
            return $query;
        }

        public function get_horario($hora_inicio, $hora_fin, $colegio_id)
        {
            $sql = "SELECT *
                    FROM horarios
                    WHERE hora_inicio='$hora_inicio' 
                    AND colegio_id='$colegio_id' 
                    AND hora_fin='$hora_fin'
                    ";

            $query = $this->db->query($sql);
            return $query->result();
        }

         public function baja_horarios($idcolegio) 
        {
            $sql = "UPDATE horarios SET estado=0 WHERE colegio_id=$idcolegio ";
            $query = $this->db->query($sql);
            return $query;
        }

         public function update_estado_horario($horarioId, $estadoNew) 
        {
            $sql = "UPDATE horarios SET estado='$estadoNew' WHERE id=$horarioId ";
            $query = $this->db->query($sql);
            return $query;
        }


        public function get_horariosXcolegio($idcolegio, $cicloa)
        {
            $sql = "SELECT horarios.id, hora_inicio, hora_fin, numero, colegio_id
                    FROM horarios 
                    JOIN planesestudio ON horarios.planesestudio_id = planesestudio.id
                    JOIN nodocolegio on nodocolegio.id = horarios.colegio_id
                    WHERE horarios.colegio_id = $idcolegio 
                    AND planesestudio.nombre = nodocolegio.planestudio
                    AND horarios.estado = 1
                    ORDER BY horarios.numero";
            $query = $this->db->query($sql);
            return $query->result();
        }


        public function get_dias()
        {
            $sql = "SELECT *
                    FROM dias
                    ORDER BY id";
            $query = $this->db->query($sql);
            return $query->result();
        }
//JOIN nodocolegio ON nodocolegio.id  = horariosmaterias.id_colegio nodocolegio.cicloa

        public function get_HorariosMateriasXdivision($divi_id, $cicloa)
        {
            $sql = "  
            
            SELECT horariosmaterias.id, horarios.id as horarios_id, dias_id, nombresmateria_id, nombresmateria.nombre as materia, lugar, docente_id, first_name, last_name, horariosmaterias.colegio_id
                    FROM horariosmaterias 
                    JOIN nodocolegio ON nodocolegio.id  = horariosmaterias.colegio_id
                    LEFT JOIN 
                    (
                        SELECT users_groups.id, users.first_name, users.last_name
                        FROM users_groups
                        JOIN users ON users_groups.user_id = users.id

                    ) as T
                    ON horariosmaterias.docente_id = T.id

                    JOIN anios_materias, nombresmateria, planesestudio, horarios
                    
                    WHERE horariosmaterias.divisiones_id = $divi_id 
                    AND horariosmaterias.planesestudio_id = planesestudio.id
                    AND planesestudio.nombre = nodocolegio.planestudio
                    AND horariosmaterias.materias_id = anios_materias.id
                    AND anios_materias.nombresmateria_id = nombresmateria.id
                    AND horariosmaterias.horarios_id = horarios.id
                    ORDER BY horarios.numero, dias_id
            
            ";

            $query = $this->db->query($sql);
            return $query->result();
        }

        public function get_HorarioMateria($id)
        {
            $sql = "  
            
            SELECT horariosmaterias.id, horarios_id, dias_id, nombresmateria_id, nombresmateria.nombre as materia, lugar, docente_id, first_name, last_name, horariosmaterias.materias_id
                    FROM horariosmaterias 
                    
                    LEFT JOIN 
                    (
                        SELECT users_groups.id, users.first_name, users.last_name
                        FROM users_groups
                        JOIN users ON users_groups.user_id = users.id

                    ) as T
                    ON horariosmaterias.docente_id = T.id

                    JOIN anios_materias, nombresmateria
                    WHERE horariosmaterias.id = $id
                    AND horariosmaterias.materias_id = anios_materias.id
                    AND anios_materias.nombresmateria_id = nombresmateria.id
                    LIMIT 1
            ";

            $query = $this->db->query($sql);
            return $query->result();
        }

        /*public function obtener_alumnos_del_tutor($id) 
        {
            $sql = "
                SELECT users.id as alumno_id, users.first_name as nombre_alumno, users.last_name as apellido_alumno,divisiones.id as id_div, divisiones.nombre as nombre_division, anios.nombre as anio, nodoanios.id as id_nodoanio, nodocolegio.nombre as nombre_colegio, nodocolegio.id as id_colegio
                FROM users              
                JOIN tutoralumno ON tutoralumno.alumno_id  = users.id
                JOIN inscripciones on inscripciones.alumno_id = tutoralumno.alumno_id
                JOIN divisiones on divisiones.id = inscripciones.divisiones_id
                JOIN nodoanios on nodoanios.id = divisiones.anios_id
                JOIN anios on anios.id = nodoanios.anio_id
                JOIN nodonivel ON nodonivel.id = nodoanios.nodoniv_id
                JOIN nodocolegio ON nodocolegio.id = nodonivel.nodocolegio_id

                WHERE tutoralumno.tutor_id  = $id";
            $query = $this->db->query($sql);
            return $query->result();
        }*/
        
        /*public function obtener_mis_horarios_alu2($id)
        {
            $sql = "
                SELECT users.id as alumno_id, users.first_name as nombre_alumno, users.last_name as apellido_alumno,divisiones.id as id_div, divisiones.nombre as nombre_division, anios.nombre as anio, nodoanios.id as id_nodoanio, nodocolegio.nombre as nombre_colegio, nodocolegio.id as id_colegio
                FROM users              
                JOIN tutoralumno ON tutoralumno.alumno_id  = users.id
                JOIN inscripciones on inscripciones.alumno_id = tutoralumno.alumno_id
                JOIN divisiones on divisiones.id = inscripciones.divisiones_id
                JOIN nodoanios on nodoanios.id = divisiones.anios_id
                JOIN anios on anios.id = nodoanios.anio_id
                JOIN nodonivel ON nodonivel.id = nodoanios.nodoniv_id
                JOIN nodocolegio ON nodocolegio.id = nodonivel.nodocolegio_id

                WHERE tutoralumno.alumno_id  = $id";
            $query = $this->db->query($sql);
            return $query->result();
        }*/
        public function obtener_mis_horarios_alu($id)
        {
            $sql = "
                SELECT users.id as alumno_id, users.first_name as nombre_alumno, users.last_name as apellido_alumno,divisiones.id as id_div, divisiones.nombre as nombre_division, anios.nombre as anio, nodoanios.id as id_nodoanio, nodocolegio.nombre as nombre_colegio, nodocolegio.id as id_colegio
                FROM users              
                JOIN users_groups ON users_groups.user_id  = users.id
                JOIN inscripciones on inscripciones.alumno_id = users_groups.id
                JOIN divisiones on divisiones.id = inscripciones.divisiones_id
                JOIN nodoanios on nodoanios.id = divisiones.anios_id
                JOIN anios on anios.id = nodoanios.anio_id
                JOIN nodonivel ON nodonivel.id = nodoanios.nodoniv_id
                JOIN nodocolegio ON nodocolegio.id = nodonivel.nodocolegio_id

                WHERE users_groups.user_id  = $id
                AND inscripciones.cicloa = nodocolegio.cicloa
                ";
            $query = $this->db->query($sql);
            return $query->result();
        }
        public function obtener_horario_docente_x_id_x_colegio_con_especialidad($id_docente,$idcolegio, $cicloa)
        {
            $sql = "SELECT horariosmaterias.*, nodoanios.id as nodoanios_id,niveles.nombre as nombre_nivel,especialidades.nombre as nombre_especialidad,anios.nombre as nombre_anio, divisiones.nombre as nombre_division, divisiones.id as division_id, nombresmateria.nombre as nombre_materia  
            FROM horariosmaterias 
            join users_groups on users_groups.id = horariosmaterias.docente_id 
            join users on users.id = users_groups.user_id 
            join anios_materias on horariosmaterias.materias_id = anios_materias.id 
            join nombresmateria on nombresmateria.id = anios_materias.nombresmateria_id 
            join nodoanios on nodoanios.id = anios_materias.nodoanios_id 
            join divisiones on divisiones.id = horariosmaterias.divisiones_id 
            join anios on anios.id = nodoanios.anio_id 
            join nodocolegio on nodocolegio.id = horariosmaterias.colegio_id
            join planesestudio on planesestudio.id = horariosmaterias.planesestudio_id
            JOIN nodonivel ON nodonivel.id= nodoanios.nodoniv_id 
            JOIN niveles ON niveles.id = nodonivel.niveles_id 
            
            JOIN especialidades ON especialidades.id = nodonivel.esp_id
            where users.id = $id_docente
            AND planesestudio.nombre = nodocolegio.planestudio
            and nodocolegio.id = $idcolegio";
            $query = $this->db->query($sql);
            return $query->result();
            //JOIN nodoespecialidad ON nodoespecialidad.id= nodoanios.nodoesp_id 
            //nodoespecialidad buscar esa tabla en todas las consultas
        }
        public function obtener_horario_docente_x_id_x_colegio_sin_especialidad($id_docente,$idcolegio, $cicloa)
        {
            $sql = "SELECT horariosmaterias.*, nodoanios.id as nodoanios_id, niveles.nombre as nombre_nivel,anios.nombre as nombre_anio ,divisiones.nombre as nombre_division,divisiones.id as division_id, nombresmateria.nombre as nombre_materia 
            FROM horariosmaterias 
            join users_groups on users_groups.id = horariosmaterias.docente_id 
            join users on users.id = users_groups.user_id 
            join anios_materias on horariosmaterias.materias_id = anios_materias.id 
            join nombresmateria on nombresmateria.id = anios_materias.nombresmateria_id 
            join nodoanios on nodoanios.id = anios_materias.nodoanios_id 
            join divisiones on divisiones.id = horariosmaterias.divisiones_id 
            join anios on anios.id = nodoanios.anio_id 
            join nodocolegio on nodocolegio.id = horariosmaterias.colegio_id
            join planesestudio on planesestudio.id = horariosmaterias.planesestudio_id
            JOIN nodonivel ON nodonivel.id= nodoanios.nodoniv_id 
            JOIN niveles ON niveles.id = nodonivel.niveles_id 
            
            where users.id = $id_docente
            AND planesestudio.nombre = nodocolegio.planestudio
            and nodocolegio.id = $idcolegio
            and nodonivel.esp_id IS NULL";
            $query = $this->db->query($sql);
            return $query->result();
        }



	}