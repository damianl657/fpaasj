<?php //session_start();

class Autorizar_eventos extends CI_Controller {

	function __construct(){
		parent::__construct();
			//$this->output->enable_profiler(TRUE);
		if(!$this->session->userdata("logged_in")){
               redirect("login");
         }
	}


    public function index(){

	    $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		//$data['urlApi'] = $urlApi;
		//$data['passApiKey'] = $passApiKey;
		$data['idusuario'] = $this->session->userdata('idusuario');


		$band = 0;
		$controlador = "autorizar_eventos";
		$metodo = "index";
		$accion1 = "edit_evento_supervisa";


        $colegios = $this->session->userdata("colegios");
        $colegiosPermi = array();
        $colegiosEdit = array();
        //$acciones = array();
        $banAddMat = 0;
		foreach ($colegios as $cole)
		{
			//print_r($cole); die();
			$idcolegio = $cole["id_colegio"];
			$menus = $cole["menu"];
		 	foreach ($menus as $menu)
			{
				//if($idcolegio == 9) {print_r($menu); echo "<br><br>";}

				if( ($menu->controlador === $controlador) && ($menu->metodo === $metodo))
				{
				    $band = 1;

				    $arre = array(
				    	'idcole'=>$idcolegio,
				    	'namecole'=>$cole["nombre_colegio"],
				    	'idrol'=>$cole["id_grupo"],
				    	'namerol'=>$cole["rol"],
				    );
				    $colegiosPermi[]=$arre;

				    $result = $this->get_eventos_para_autorizar_curl($urlApi, $passApiKey, $data['idusuario'], $idcolegio, $cole["id_grupo"], $cole["rol"]);

				    //print_r($result['eventos'][0]); //die();

				    // /echo $result['status'];
				    if($result['status']){
				    	$data['eventos'][$idcolegio] =  $result['eventos'];

				    }
				}

				if( $menu->nombre === $accion1)
				    $colegiosEdit[] = $idcolegio;
			}

			/*foreach ($menus as $menu)
			{
				if( $menu->nombre === $accion1)
				    $banAddMat = 1;
			}*/
		}

        if($band == 1)
        {
    		$data['colegios'] = $colegiosPermi;
    		$data['opcion_menu'] = 'autorizar';
    		$data['colegiosEdit'] = $colegiosEdit;

		    $data['contenido'] = 'panel/autorizar/autorizar_comunicado';
			$this->load->view('include/template_colegio',$data);

		}
		else redirect(404);
	}


	//---------------------------------------- CURL request to API -------------------------------------------

    public function get_eventos_para_autorizar_curl($urlApi, $passApiKey, $idUser, $colegioId, $id_grupo, $name_grupo)
    {
    	$url = $urlApi."/evento/obtener_eventos_autorizar/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('colegioId'=>$colegioId, 'id_grupo'=>$id_grupo, 'name_grupo'=>$name_grupo),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);
		//$response = json_decode($response);
		//print_r($response); die();
		return json_decode($response,true);
	}


    public function autorizar_curl($urlApi, $passApiKey, $idUser, $idevento, $pin)
    {
    	$url = $urlApi."/evento/publicar";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('idevento' => $idevento, 'pin' => $pin),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);
		//$response = json_decode($response);
		//return json_decode($response);

		return $response;
	}


	public function editar_evento_curl($idevento, $pin, $data, $idcolegio, $idsfiles, $edita_files)
    {
    	$urlApi = $this->variables->get_urlapi();
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');

    	$url = $urlApi."/evento/update_evento";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('idevento' => $idevento, 'pin' => $pin,
										'data' => $data, 'idcolegio' => $idcolegio,
										'idsfiles' => $idsfiles, 'edita_files' => $edita_files,
										'supervisa' =>true),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array($ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);
		//$response = json_decode($response);
		//return json_decode($response);

		return $response;
	}


	//---------------------------------------- AJAX request from WEB -------------------------------------------

    public function autorizar_ajax(){

	    $urlApi = $this->variables->get_urlapi();
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');

		$idevento = $_POST['id'];
		$pin = $_POST['pin'];



		$result=$this->autorizar_curl($urlApi, $passApiKey, $idusuario, $idevento, $pin);

		print_r($result);

		//die();

		/*if( (isset($result->status)) && (!$result->status) ){
			$arre = array(
				'status' => 0,
             	'message' => 'No se encuentra registrado tal evento'
			);
			$arre=json_encode($arre);
			print_r($arre);
		}
		else
			print_r($result);*/
	}


	public function editar_evento_ajax(){

		$idevento = $_POST['idevento'];
		$pin = $_POST['pin'];
		$data = $_POST['data'];
		$idcolegio = $_POST['idcolegio'];
		$idsfiles = $_POST['idsfiles'];
		$edita_files = $_POST['edita_files'];

		$result=$this->editar_evento_curl($idevento, $pin, $data, $idcolegio, $idsfiles, $edita_files);

		print_r($result);
	}


}
?>
