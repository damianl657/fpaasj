<script src="<?=base_url('assets/js/bootstrap-notify.js');?>"></script>
<div  id="horarios_gral">

</div>
<script type="text/javascript">
$(document).ready(function()
{
	passApiKey = '<?php echo $passApiKey; ?>';
	 urlApi = '<?php echo $urlApi; ?>';
	 idUser = '<?php echo $idusuario; ?>';
	 var roles_agrupados = "";
     var roles = "";
     var escuelas = "";
     var edita_calendario = 0;
     var edita_horario = 0;
     var optionesDias="";
     var optionesHorario="";


	

	    $('.select2').select2({
	         theme: "bootstrap",
	         placeholder: "Seleccione division",
	        allowClear: true
	    }); 


	
	obtener_datos();
	function obtener_datos()
	{
		var urlRequest = urlApi+"/usuario/get_rolex_colegios_all_users/"; 
    	$.ajax
    	({
            url: urlRequest,
              type: 'GET',
              headers: {              
               'APIKEY' : passApiKey,
               'userid' : idUser,
               'Access-Control-Allow-Origin': '*'
            },
            data :{},
            
            success : function(data) 
            {   
            	//console.log(data.roles_agrupados);
            	roles_agrupados = JSON.parse(data.roles_agrupados);
            	roles = JSON.parse(data.roles);
            	horarios = JSON.parse(data.horarios);
            	dias = JSON.parse(data.dias);
            	escuelas = JSON.parse(data.escuelas);

            	//console.log(roles_agrupados);
            	//console.log(escuelas);
            	 
            	console.log(roles);
            	console.log(data);
            	var escuela_id = '';
                var escuela_nombre = '';
                var horarios_aux_doc = '';
                var estado = 1;
                var estado2 = 1;
                var hora_inicio = '';
				var hora_fin = '';
				var nombre_esc = '';
            	//construyo los dias en td
            	var dias_aux = '';
            	var tab_doc= '';
            	var horarios_aux_dir ='';
                for(var i=0; i < dias.length ; i++)
                {
                	//console.log(dias[i]['nombre']);
                	dias_aux = dias_aux + '<td id="'+dias[i]["id"]+'"><div align="center">'+dias[i]["nombre"]+'</div></td>';
                	optionesDias = optionesDias + '<option value="'+dias[i]["id"]+'">'+dias[i]["nombre"]+'</option>' ;
                }
                //fin de td de dias
                
            	for(var i=0; i < roles_agrupados.length ; i++)
            	{
            		//console.log(roles_agrupados[i]["nombre_grupo"]);
            		//buscar en roles si el rol agrupado se encuentra en la escuela id
            		//for(var i=0; i < roles.length ; i++)
            		//
            		
            		if(roles_agrupados[i]["nombre_grupo"] == 'Docente')
            		{
            			//console.log(roles_agrupados[i]["nombre_grupo"]);
            			$('#horarios_gral').append(content_docente);
            			 estado = 1;
                		 estado2 = 1;
                		 horarios_aux_doc ='';
            			for(var j=0; j < escuelas.length ; j++)
            			{
            				//horarios del colegio
            				escuela_id = escuelas[j]["colegio_id"];
                			escuela_nombre = escuelas[j]["nombre_colegio"]; 
            				estado_doc = 0;
            				for(var r=0; r < roles.length ; r++)
            				{
            					if((roles[r]["nombre_grupo"] == 'Docente') && (roles[r]["id_colegio"] == escuela_id) && (estado_doc == 0))
            					{
            						//alert('entro');
		                			horarios_aux_doc ='';
		            				for(var k=0; k < horarios.length ; k++)
					                  {
						                 if((horarios[k]["colegio_id"] == escuela_id)&&(roles[r]["nombre_grupo"] == 'Docente') )
						                 //if((horarios[k]["colegio_id"] == escuela_id) && (roles[r]["id_colegio"] == escuela_id)&&(roles[r]["nombre_grupo"] == 'Docente') )
						                 {
						                 	 hora_inicio = horarios[k]["hora_inicio"];
						                    hora_inicio = hora_inicio.substr(0,5);
						                     hora_fin = horarios[k]["hora_fin"];
						                    hora_fin = hora_fin.substr(0,5);
						                    //console.log(horarios[i]['hora_inicio']);
						                    //console.log(hora_inicio);
						                    horarios_aux_doc = horarios_aux_doc +'<tr>';
						                    horarios_aux_doc = horarios_aux_doc + '<td id="doc_'+horarios[k]["id"]+'" ><div align="center"><b>'+horarios[k]["numero"]+'&deg; </b></div><div align="center">'+hora_inicio+' - '+hora_fin+'</div></td>';
						                    for(var d=0; d < dias.length ; d++)
						                    {
						                      //console.log(dias[j]['nombre']);
						                      horarios_aux_doc = horarios_aux_doc + '<td id="doc_'+horarios[k]["id"]+'_'+dias[d]["id"]+'_'+escuela_id+'" class="casilla" ></td>';
						                    }
						                    horarios_aux_doc = horarios_aux_doc +'</tr>';
						                 }
					                    //dias_aux = dias_aux + '<td id="'+dias[i]["id"]+'"><div align="center">'+dias[i]["nombre"]+'</div></td>';
				                  	}
				                  	estado_doc = 1;
            					}
            					//
            					if((roles[r]["nombre_grupo"] == 'Docente') && (roles[r]["id_colegio"] == escuela_id))
            					{

            						if(estado == 1)
			                          {
			                             nombre_esc = '<li class="active">'+'<a data-toggle="tab" href="<?php echo base_url()."Horarios/index#tab-'+escuela_id+'";?>">'+escuela_nombre+'</a>'+'</li>';
			                            estado = 0;
			                          }
			                          else
			                          {
			                          	 nombre_esc = '<li class="">'+'<a data-toggle="tab" href="<?php echo base_url()."Horarios/index#tab-'+escuela_id+'";?>">'+escuela_nombre+'</a>'+'</li>';
			                          }
			                          if(estado2 == 1)
			                          {
			                			 tab_doc = '<div id="tab-'+escuela_id+'" class="tab-pane active">'+
			                                              '<div class="panel-body">'+
			                                                  '<table class="table table-striped" id="tabladatos_doc_'+escuela_id+'">'+
								                                  '<thead>'+
								                                    '<tr id="cabeceratabla">'+
								                                      '<th  rowspan="2"><div align="center">HORA</div></th>'+
								                                      '<th  colspan="10"><div align="center">DIAS</div></th>'+
								                                    '</tr>'+ 
								                                    '<tr>'+
								                                    dias_aux+
								                                    '</tr>'+
								                                  '</thead>'+
								                                  '<tbody>'+
								                                  	horarios_aux_doc+
								                                  '</tbody>'+
								                                '</table>'
			                                              '</div>'+
			                                          '</div>';
			                            estado2 = 0;
			                        }
			                        else
			                        {
			                        	 tab_doc = '<div id="tab-'+escuela_id+'" class="tab-pane">'+
			                                              '<div class="panel-body">'+
			                                                  '<table class="table table-striped" id="tabladatos_doc_'+escuela_id+'">'+
								                                  '<thead>'+
								                                    '<tr id="cabeceratabla">'+
								                                      '<th  rowspan="2"><div align="center">HORA</div></th>'+
								                                      '<th  colspan="10"><div align="center">DIAS</div></th>'+
								                                    '</tr>'+ 
								                                    '<tr>'+
								                                    dias_aux+
								                                    '</tr>'+
								                                  '</thead>'+
								                                  '<tbody>'+
								                                  	horarios_aux_doc+
								                                  '</tbody>'+
								                                '</table>'
			                                              '</div>'+
			                                          '</div>';
			                        }
            					}
            					if((roles[r]["nombre_grupo"] == 'Docente') && (roles[r]["id_colegio"] == escuela_id))
            					{
            						$("#colegios_doc").append(nombre_esc);
				                     $("#contenido_tab_docente").append(tab_doc);
				                      horarios_aux_doc ='';
				                      completar_agenda_docente(escuela_id);
            					}

            				}
            				
		                     /*$("#colegios_doc").append(nombre_esc);
		                     $("#contenido_tab_docente").append(tab_doc);
		                      horarios_aux_doc ='';
		                      completar_agenda_docente(escuela_id);*/
		                
            			}
            		}
            		//fin docente
            	}	
            		//Precetor o directivo
            	escuela_id = '';
                 escuela_nombre = '';
                 horarios_aux = '';
                 estado = 1;
                 estado2 = 1;
                 hora_inicio = '';
				 hora_fin = '';
				 nombre_esc = '';
				 var nombre_de_grupo ='';
            	for(var i=0; i < roles_agrupados.length ; i++)
            	{
            		if((roles_agrupados[i]["nombre_grupo"] == 'Directivo') || (roles_agrupados[i]["nombre_grupo"] == 'Preceptor') || (roles_agrupados[i]["nombre_grupo"] == 'Administrativo'))
            		{
            			//alert('entro 1');
            			$('#horarios_gral').append(content_dir_prec);
            			//var estado = 1;
                		//var estado2 = 1;
            			for(var j=0; j < escuelas.length ; j++)
            			{
            				//horarios del colegio
            				
                			 escuela_id = escuelas[j]["colegio_id"];
                			 escuela_nombre = escuelas[j]["nombre_colegio"];
                			 horarios_aux_dir ='';
            				for(var k=0; k < horarios.length ; k++)
			                  {
				                 if(horarios[k]["colegio_id"] == escuela_id)
				                 {
				                 	 hora_inicio = horarios[k]["hora_inicio"];
				                    hora_inicio = hora_inicio.substr(0,5);
				                     hora_fin = horarios[k]["hora_fin"];
				                    hora_fin = hora_fin.substr(0,5);

				                    //console.log(horarios[i]['hora_inicio']);
				                    //console.log(hora_inicio);
				                    horarios_aux_dir = horarios_aux_dir +'<tr>';
				                    horarios_aux_dir = horarios_aux_dir + '<td id="dir_'+horarios[k]["id"]+'" ><div align="center"><b>'+horarios[k]["numero"]+'&deg; </b></div><div align="center">'+hora_inicio+' - '+hora_fin+'</div></td>';
				                    
				                    for(var d=0; d < dias.length ; d++)
				                    {
				                      //console.log(dias[j]['nombre']);
				                      horarios_aux_dir = horarios_aux_dir + '<td id="dir_'+horarios[k]["id"]+'_'+dias[d]["id"]+'_'+escuela_id+'" class="casilla" ></td>';
				                    }
				                    horarios_aux_dir = horarios_aux_dir +'</tr>';
				                 }
				                    
			                    //dias_aux = dias_aux + '<td id="'+dias[i]["id"]+'"><div align="center">'+dias[i]["nombre"]+'</div></td>';
			                  }
            				//fin horarios del colegio
            				//buscar en la variable roles si el rol esta en la escuela del ciclo i
            				for(var r=0; r < roles.length ; r++)
            				{//alert('entro 2');
            					if(((roles[r]["nombre_grupo"] == 'Directivo') || (roles[r]["nombre_grupo"] == "Preceptor") || (roles[r]["nombre_grupo"] == "Administrativo")) && (roles[r]["id_colegio"] == escuela_id))
            					{
            						nombre_de_grupo = roles[r]["nombre_grupo"];
            						
		            				if(estado == 1)
				                          {
				                             nombre_esc = '<li class="active">'+'<a data-toggle="tab" href="<?php echo base_url()."Horarios/index#tab_dir-'+escuela_id+'";?>">'+escuela_nombre+'</a>'+'</li>';
				                            estado = 0;
				                          }
				                          else
				                          {
				                          	 nombre_esc = '<li class="">'+'<a data-toggle="tab" href="<?php echo base_url()."Horarios/index#tab_dir-'+escuela_id+'";?>">'+escuela_nombre+'</a>'+'</li>';
				                          }
				                          if(estado2 == 1)
				                          {
				                			var tab_dir_pre = '<div id="tab_dir-'+escuela_id+'" class="tab-pane active">'+
				                                              '<div class="panel-body">'+
				                                              		'<div class="row">'+
										                        		'<div class="col-sm-6">'+
											                        		'<form id="form_dir'+escuela_id+'" autocomplete="on" method="post" class="form-horizontal form-label-left">'+
											                        			' <div class="form-group ">'+
										                                          	'<label>Seleccione una Divisi&oacute;n: </label>'+
										                                    	'</div>'+
										                                    	'<div class="form-group ">'+
										                                    		'<select name="divisiones" id="dir_prec_'+escuela_id+'" class="select2 form-control">'+
										                                    		'</select>'+
										                                    	'</div>'+ 
											                        		'</form>'+
											                        	'</div>'+
											                        	'<div class="col-sm-6" style="margin-top:35px">'+
									                                    '<button class="btn btn-sm btn-primary pull-left m-t-n-xs" value="'+escuela_id+'" id="btn_listar_alumnos1"  type=""><strong>Listar</strong></button>'+
									                                  		'<div type="hidden" id="loading_horarios_dir_esc_'+escuela_id+'">'+
										                                  	'</div>'+
									                                  	'</div>'+
										                        	'</div>'+
				                                                  '<table class="table table-striped" id="tabladatos_'+escuela_id+'"">'+
									                                  '<thead>'+
									                                    '<tr id="cabeceratabla">'+
									                                      '<th  rowspan="2"><div align="center">HORA</div></th>'+
									                                      '<th  colspan="10"><div align="center">DIAS</div></th>'+
									                                    '</tr>'+ 
									                                    '<tr>'+
									                                    dias_aux+
									                                    '</tr>'+
									                                  '</thead>'+
									                                  '<tbody>'+
									                                  	horarios_aux_dir+
									                                  '</tbody>'+
									                                '</table>'
				                                              '</div>'+
				                                          '</div>';
				                            estado2 = 0;
				                        }
				                        else
				                        {
				                        	var tab_dir_pre = '<div id="tab_dir-'+escuela_id+'" class="tab-pane">'+
				                                              '<div class="panel-body">'+
				                                              		'<div class="row">'+
										                        		'<div class="col-sm-6">'+
											                        		'<form id="form_dir'+escuela_id+'" autocomplete="on" method="post" class="form-horizontal form-label-left">'+
											                        			' <div class="form-group ">'+
										                                          	'<label>Seleccione una Divisi&oacute;n: </label>'+
										                                    	'</div>'+
										                                    	'<div class="form-group ">'+
										                                    		'<select name="divisiones" id="dir_prec_'+escuela_id+'" class="select2 form-control">'+
										                                    		'</select>'+
										                                    	'</div>'+ 
											                        		'</form>'+
											                        	'</div>'+
											                        	'<div class="col-sm-6" style="margin-top:35px">'+
									                                    '<button class="btn btn-sm btn-primary pull-left m-t-n-xs" value="'+escuela_id+'" id="btn_listar_alumnos2"  type="button"><strong>Listar</strong></button>'+
										                                  	'<div type="hidden" id="loading_horarios_dir_esc_'+escuela_id+'">'+
										                                  	'</div>'+
									                                  	'</div>'+
										                        	'</div>'+
				                                                  '<table class="table table-striped" id="tabladatos">'+
									                                  '<thead>'+
									                                    '<tr id="cabeceratabla">'+
									                                      '<th  rowspan="2"><div align="center">HORA</div></th>'+
									                                      '<th  colspan="10"><div align="center">DIAS</div></th>'+
									                                    '</tr>'+ 
									                                    '<tr>'+
									                                    dias_aux+
									                                    '</tr>'+
									                                  '</thead>'+
									                                  '<tbody>'+
									                                  	horarios_aux_dir+
									                                  '</tbody>'+
									                                '</table>'
				                                              '</div>'+
				                                          '</div>';
				                        }
				                     $("#colegios_dir_prec").append(nombre_esc);
				                     $("#contenido_tab_dir_prec").append(tab_dir_pre);
				                     get_divisiones_x_colegio(escuela_id, nombre_de_grupo);
				                
		            			}
		            		}
		            	}
            		}
            		if((roles_agrupados[i]["nombre_grupo"] == 'Tutor') )
            		{
            			//alert('es tutor');
            			$('#horarios_gral').append(content_tutor);
            			rol_tutor();

            		}
            	}
            	

            },
            error: function(response)
            {
                //hacer algo cuando ocurra un error
                console.log(response);

            }
    	}); 
	}


	function rol_tutor()
	{
		var urlRequest = urlApi+"/horarios/obtener_alumnos/"; 
	    $.ajax({
	            url: urlRequest,
	              type: 'GET',
	              headers: {              
	               'APIKEY' : passApiKey,
	               'userid' : idUser,
	               'Access-Control-Allow-Origin': '*'
	            },
	            data :{},
	            
	            success : function(data) 
	            {   
	            	//console.log(data.horarios);
	            	var dias = JSON.parse('<?php echo json_encode($dias); ?>');
	            	//console.log(dias);
	                
	               
	                //var horarios = data.horarios;
	                var horarios = JSON.parse(data.horarios);
	                var alumnos = JSON.parse(data.alumnos);
	                //alert('entro');
					//console.log(d.getHours());
					//console.log(d);
	               //console.log(dias);
	               //console.log(data);
	               var dias_aux = '';
	               
	                for(var i=0; i < dias.length ; i++)
	                {
	                	//console.log(dias[i]['nombre']);
	                	dias_aux = dias_aux + '<td id="'+dias[i]["id"]+'"><div align="center">'+dias[i]["nombre"]+'</div></td>';
	                }
	                
	                //console.log(data);
	                var estado = 1;
	                var estado2 = 1;
	                for(var i=0; i < alumnos.length ; i++)
	                {//console.log('entro al arre hijos');
	                	//console.log(alumnos[i]);
	                  var escuela_curso = alumnos[i]['nombre_colegio']+' - '+alumnos[i]['anio']+' '+alumnos[i]['nombre_division'];
	                	var alumno_id = alumnos[i]['alumno_id'];
	                    var nombre_alumno = alumnos[i]['nombre_alumno'];
	                    var apellido_alumno = alumnos[i]['apellido_alumno'];
	                    //console.log(alumno_id);
	                	var horarios_aux ='';
	                  for(var k=0; k < horarios.length ; k++)
	                  {
	                  	//console.log(horarios[k]);
	                    var hora_inicio = horarios[k]["hora_inicio"];
	                      hora_inicio = hora_inicio.substr(0,5);
	                     var hora_fin = horarios[k]["hora_fin"];
	                      hora_fin = hora_fin.substr(0,5);

	                    //console.log(horarios[i]['hora_inicio']);
	                    //console.log(hora_inicio);
	                    horarios_aux = horarios_aux +'<tr>';
	                    horarios_aux = horarios_aux + '<td id="'+horarios[k]["id"]+'" ><div align="center"><b>'+horarios[k]["numero"]+'&deg; </b></div><div align="center">'+hora_inicio+' - '+hora_fin+'</div></td>';
	                    
	                    for(var j=0; j < dias.length ; j++)
	                    {
	                      //console.log(dias[j]['nombre']);
	                      horarios_aux = horarios_aux + '<td id="'+horarios[k]["id"]+'_'+dias[j]["id"]+'_'+alumno_id+'" class="2casilla" ></td>';
	                    }
	                    horarios_aux = horarios_aux +'</tr>';
	                    //dias_aux = dias_aux + '<td id="'+dias[i]["id"]+'"><div align="center">'+dias[i]["nombre"]+'</div></td>';
	                  }
	                	if(estado == 1)
	                          {
	                            var nombre_alu = '<li class="active">'+'<a data-toggle="tab" href="<?php echo base_url()."Horarios/index#tab-'+alumno_id+'";?>">'+nombre_alumno+' '+apellido_alumno+'</a>'+'</li>';
	                            estado = 0;
	                          }
	                          else
	                          {
	                          	var nombre_alu = '<li class="">'+'<a data-toggle="tab" href="<?php echo base_url()."Horarios/index#tab-'+alumno_id+'";?>">'+nombre_alumno+' '+apellido_alumno+'</a>'+'</li>';
	                          }
	                          if(estado2 == 1)
	                          {
	                			var tab = '<div id="tab-'+alumno_id+'" class="tab-pane active">'+
	                                              '<div class="panel-body">'+
	                                              '<h3>'+escuela_curso+'</h3>'+
	                                                  '<table class="table table-striped" id="tabladatos_tutor">'+
						                                  '<thead>'+
						                                    '<tr id="cabeceratabla">'+
						                                      '<th  rowspan="2"><div align="center">HORA</div></th>'+
						                                      '<th  colspan="10"><div align="center">DIAS</div></th>'+
						                                    '</tr>'+ 
						                                    '<tr>'+
						                                    dias_aux+
						                                    '</tr>'+
						                                  '</thead>'+
						                                  '<tbody>'+
						                                  	horarios_aux+
						                                  '</tbody>'+
						                                '</table>'
	                                              '</div>'+
	                                          '</div>';
	                            estado2 = 0;
	                        }
	                        else
	                        {
	                        	var tab = '<div id="tab-'+alumno_id+'" class="tab-pane">'+
	                                              '<div class="panel-body">'+
	                                                  '<table class="table table-striped" id="tabladatos_tutor">'+
	                                                  '<h3>'+escuela_curso+'</h3>'+
						                                  '<thead>'+
						                                    '<tr id="cabeceratabla">'+
						                                      '<th  rowspan="2"><div align="center">HORA</div></th>'+
						                                      '<th  colspan="10"><div align="center">DIAS</div></th>'+
						                                    '</tr>'+ 
						                                    '<tr>'+
						                                    dias_aux+
						                                    '</tr>'+
						                                  '</thead>'+
						                                  '<tbody>'+
						                                  	horarios_aux+
						                                  '</tbody>'+
						                                '</table>'
	                                              '</div>'+
	                                          '</div>';
	                        }
	                     $("#alumnos").append(nombre_alu);
	                     $("#contenido_tab").append(tab);
	                     completar_calendario_tutor(alumnos[i]['id_div'],alumnos[i]['id_nodoanio'], alumnos[i]['id_colegio'], alumno_id); //id division, id nodoanio
	                }
	                

	                //generar_datatable2();
	                
	            },
	            error: function(response)
	            {
	                //hacer algo cuando ocurra un error
	                console.log('entro por errror');
	                console.log(response);
	            }
	    }); 
	}

	function completar_calendario_tutor(divisionid, nodoanioid, id_colegio, alumno_id)
	{
      var url = urlApi+"/horario/horariosXdivision";   
      
      $.ajax({
            url: url,
            type: 'GET',
            headers: {              
                'APIKEY' : passApiKey,
                'userid' : idUser,
                'Access-Control-Allow-Origin': '*'
            },
            data:{divisionid:divisionid},
            beforeSend: function() {
              $('#calendar').append("<img id='loadingNiv' src='<?=base_url('assets/images/loading.gif');?>' height='50' width='50' />");
            },
            success: function(data){  
              
              if( (data['status']==false) && (data['message']=='NO LOGUIN') )
              { 
                //console.log(data['message']);
                if(data['message']=='NO LOGUIN')
                  location.href = "<?php echo base_url('login'); ?>"; //no està logueado
                /*else
                  if(data['message']=='vacio')
                      bootbox.alert("no hay materias cargadas", function() { });*/
              }
              else
              { //console.log(data);
                
                if(data['message']!='vacio')
                { var data = jQuery.parseJSON( data );
                  for (var j=0; j < data.length; j++ )
                  {          
                    var hora = data[j]['horarios_id'];
                    var dia = data[j]['dias_id'];

                    if( data[j]['first_name']==null )
                      var nombredocente = "";
                    else var nombredocente = data[j]['first_name']+' '+data[j]['last_name'];
                              
                    var divmateria = '<div align="center" class="divcasilla2" id="divcasilla_tutor_'+data[j]['id']+'">'+
                                          '<h4 style="color: #1E90FF; font-weight: bold">'+data[j]['materia']+'</h4>'+
                                          '<i>'+nombredocente+'</i>'+
                                     '</div>';

                    var celda=$('#tabladatos_tutor tbody').find('[id="'+data[j]['horarios_id']+'_'+data[j]['dias_id']+'_'+alumno_id+'"]');

                    $(celda).append(divmateria);
                  }
                }

               

                

                traer_docente_materia(nodoanioid, divisionid,id_colegio);

              } 

              $('#loadingNiv').remove();                            
          },
          error: function(response){
              //console.log(response); 
               $('#loadingNiv').remove();                  
          }           
      }); 
		$(".casilla2").mouseover(function() {
            $(this).addClass("td_hover");
          });

          $(".casilla2").mouseout(function() {
               $(this).removeClass("td_hover");
          });
	}

	var content_docente ='<div class="wrapper wrapper-content animated fadeInRight" id="">'+
	'<div class="row">'+
                '<div class="col-lg-12 " id="usuarios_borrar">'+
                    '<div class="ibox collapsed2 float-e-margins">'+
                        '<div class="ibox-title collapse-link2" id="tituloaltaevento">'+
                            '<h5>Horarios - Docente <small> Presione aquí</small></h5>'+
                            '<div class="ibox-tools">'+
                                '<a class="">'+
                                    '<i class="fa fa-chevron-up"></i>'+
                                '</a>'+
                            '</div>'+
                        '</div>'+
                        '<div class="ibox-content">'+
                            '<div class="row row2">'+
                                '<div class="col-sm-12 b-r" id="">'+
                                    '<div class="tabs-container">'+
                                      '<ul class="nav nav-tabs" id="colegios_doc">'+
                                      '</ul>'+
                                      '<div class="tab-content" id="contenido_tab_docente">'+
                                      '</div>'+
                                   '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                   ' </div>'+
                '</div>'+
                  '<div class="col-lg-6">'+
                  '</div>'+
             '</div>'+
            '</div>';

    var content_dir_prec = '<div class="wrapper wrapper-content animated fadeInRight" id="">'+
    '<div class="row">'+
                '<div class="col-lg-12 " id="usuarios_borrar">'+
                    '<div class="ibox collapsed2 float-e-margins">'+
                        '<div class="ibox-title collapse-link2" id="tituloaltaevento">'+
                            '<h5>Horarios <small> Presione aquí</small></h5>'+
                            '<div class="ibox-tools">'+
                                '<a class="">'+
                                    '<i class="fa fa-chevron-up"></i>'+
                                '</a>'+
                            '</div>'+
                        '</div>'+
                        '<div class="ibox-content">'+
                            '<div class="row row2">'+
                                '<div class="col-sm-12 b-r" id="">'+
                                    '<div class="tabs-container">'+
                                      '<ul class="nav nav-tabs" id="colegios_dir_prec">'+
                                      '</ul>'+
                                      '<div class="tab-content" id="contenido_tab_dir_prec">'+
                                      '</div>'+
                                   '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                   ' </div>'+
                '</div>'+
                  '<div class="col-lg-6">'+
                  '</div>'+
            '</div>'+
            '</div>';
    var content_tutor ='<div class="wrapper wrapper-content animated fadeInRight" id="general">'+
            '<div class="row" id="usuarios_contenido">'+
                '<div class="col-lg-12 " id="usuarios_borrar">'+
                    '<div class="ibox collapsed2 float-e-margins">'+
                        '<div class="ibox-title collapse-link2" id="tituloaltaevento">'+
                            '<h5>Horarios - Hijos/as <small> Presione aquí</small></h5>'+
                            '<div class="ibox-tools">'+
                                '<a class="">'+
                                    '<i class="fa fa-chevron-up"></i>'+
                                '</a>'+
                            '</div>'+
                        '</div>'+
                        '<div class="ibox-content">'+
                            '<div class="row row2">'+
                                '<div class="col-sm-12 b-r" id="">'+
                                    '<div class="tabs-container">'+
                                      '<ul class="nav nav-tabs" id="alumnos">'+
                                      '</ul>'+
                                      '<div class="tab-content" id="contenido_tab">'+
                                      '</div>'+
                                   '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                  '<div class="col-lg-6">'+
                  '</div>'+
            '</div>'+
'</div>';



	function listar_horarios(id_colegio)
	{
		
		var diviid = $('#dir_prec_'+id_colegio).val();
        var arre = Array();
        arre=diviid.split('_');
        //alert(id_colegio);
        //alert(diviid);
        console.log(roles);
        $('#tabladatos_'+id_colegio+' tbody').find('.divcasilla').remove();
        $(".casilla").unbind("click" ); //importante elimino el evento click.
        $(".nombremateria_link").unbind("click" ); //importante elimino el evento click.
        completar_calendario(arre[0], arre[1], arre[2]); //id division, id nodoanio, id_colegio

        var colegios = JSON.parse('<?php echo json_encode($this->session->userdata("colegios")); ?>');
		//obtener valor de edita calendario de la variable multicolegio
		
		for(var i=0; i < colegios.length ; i++)
		        {
		         console.log(colegios[i]["nombre_colegio"]);
		     
		         console.log(idcolegio = colegios[i]["menu"]);
		         menu = colegios[i]["menu"];
		         for(var j=0; j < menu.length ; j++)
		         {
		          console.log(menu[i]["nombre"]);
		          if((menu[i]["nombre"] == 'editar_horario') && (colegios[i]["id_colegio"] == id_colegio))
		          {
		            edita_horario = 1;
		          }
		         }
		       
		        }
		        //fin
		//alert(edita_horario);        
        if(edita_horario==1)
        {
          $(".casilla").mouseover(function() {
            $(this).addClass("td_hover");
          });

          $(".casilla").mouseout(function() {
               $(this).removeClass("td_hover");
          });
        }
	};

	function completar_calendario(divisionid, nodoanioid, id_colegio)
	{
		var url = urlApi+"/horario/horariosXdivision";   
      	//$('#loading_horarios_dir_esc_'+id_colegio).show();
      	$('#loading_horarios_dir_esc_'+id_colegio).append('<img type="hidden" id="img_loading_horarios_dir_esc_'+id_colegio+'" src="<?=base_url("assets/images/loading.gif");?>" height="50" width="50" />');
      	
      $.ajax({
            url: url,
            type: 'GET',
            headers: {              
                'APIKEY' : passApiKey,
                'userid' : idUser,
                'Access-Control-Allow-Origin': '*'
            },
            data:{divisionid:divisionid},
            
            success: function(data)
            {  
            	console.log(data);
            $('#img_loading_horarios_dir_esc_'+id_colegio).remove();  
            if(data['message']!='vacio')
            {	
            	var data = jQuery.parseJSON( data );
                for (var j=0; j < data.length; j++ )
                  {
            		var hora = data[j]['horarios_id'];
                    var dia = data[j]['dias_id'];

                    if( data[j]['first_name']==null )
                      var nombredocente = "";
                    else var nombredocente = data[j]['first_name']+' '+data[j]['last_name'];
                              
                    var divmateria = '<div align="center" class="divcasilla" id="divcasilla_dir_'+data[j]['id']+'_'+id_colegio+'">'+
                                          '<h4><a class="nombremateria_link" id="nombremateria_'+data[j]['id']+'"><b>'+data[j]['materia'] +'</b></a> <button type="button" class="bootbox-close-button close btn-sm" data-dismiss="modal" aria-hidden="true" style="display:none">×</button></h4>'+
                                          '<i>'+nombredocente+'</i>'+
                                     '</div>';

                    var celda=$('#tabladatos_'+id_colegio+' tbody').find('[id="dir_'+data[j]['horarios_id']+'_'+data[j]['dias_id']+'_'+id_colegio+'"]');

                    $(celda).append(divmateria);
                   }
                   
                   $(".divcasilla").mouseover(function() {
                  var close = $(this).find('.close');
                  $(close).css('display','block');
	                });

	                $(".divcasilla").mouseout(function() {
	                    var close = $(this).find('.close');
	                    $(close).css('display','none');
	                });

	                 $('.close').click(function(e){
                                    e.stopPropagation();
                                    var div = $(this).parent('h4').parent('div');
                                    var divid = $(div).attr('id');
                                    var arre=Array();
                                    arre = divid.split('_');
                                    var divid = arre[2];
                                    //alert(divid);

                                    var colegios = JSON.parse('<?php echo json_encode($this->session->userdata("colegios")); ?>');
									//obtener valor de edita calendario de la variable multicolegio
									
									for(var i=0; i < colegios.length ; i++)
									        {
									         console.log(colegios[i]["nombre_colegio"]);
									     
									         //console.log(idcolegio = colegios[i]["menu"]);
									         menu = colegios[i]["menu"];
									         for(var j=0; j < menu.length ; j++)
									         {
									          console.log(menu[j]["nombre"]);
									          if((menu[j]["nombre"] == 'editar_horario') && (colegios[i]["id_colegio"] == id_colegio))
									          {
									            edita_horario = 1;
									          }
									         }
									       
									        }

                                    if(edita_horario == 1) 
                                    {    
                                      console.log(divid);
                                        deletehorario(divid, id_colegio);
                                    }
                                    else {
                                    	//alert('por aqui');
                                          console.log("no tiene permisos para eliminar un horario");
                                        }
                                });
	                traer_docente_materia(nodoanioid, divisionid, id_colegio);
	               
				    
					
              
            }
            
                

                                   
           },
           error: function(response){
              console.log(response); 
               $('#loadingNiv').remove();                  
           }           
      }); 
	$(".casilla").mouseover(function() {
			$(this).addClass("td_hover");
	});


	$(".casilla").mouseout(function() {
	     $(this).removeClass("td_hover");
	});
		
	}

function get_divisiones_x_colegio(idcolegio, nombregrupo)
{
		var urlRequest = urlApi+'/division/obtener_divisiones_x_colegio/'; 
		
    		$.ajax({
            url: urlRequest,
              type: 'GET',
              headers: {              
               'APIKEY' : passApiKey,
               'userid' : idUser,
               'Access-Control-Allow-Origin': '*'
            },
            data :{idcolegio:idcolegio, nombregrupo:nombregrupo },
            success : function(data) 
            { 
            	//onsole.log(data);
            	var data = jQuery.parseJSON( data );        
                //console.log(data);             
                //var selecMat = "<select id='nivel' name='nivel' lass='form-control input-md' multiple >*";

                for(var i=0; i < data.length ; i++) //aca solo listo niveles y especialidades
                {                                                                   
                    //console.log(data[i]);
                    if(data[i]['nombre_especialidad'])               
                    {
                      var nombre_nivel = data[i]['nombre_nivel']+" "+data[i]['nombre_especialidad']+" "+data[i]['nombre_anio']+"</span>"; 

                      var opcion = "<option value='"+data[i]['division_id']+"_"+data[i]['nodoanios_id']+"_"+idcolegio+"'>" +nombre_nivel+"</option>" ;
                    }
                    else
                    {
                      var nombre_nivel = data[i]['nombre_nivel']+" "+data[i]['nombre_anio']+" "+data[i]['nombre_division'];
                      var opcion  = "<option value='"+data[i]['division_id']+"_"+data[i]['nodoanios_id']+"_"+idcolegio+"'>" +nombre_nivel+"</option>" ;
                    }   

                    $('#dir_prec_'+idcolegio).append(opcion);               
                }
            	//alert('entro');
            },
            error: function(response)
            {
                //hacer algo cuando ocurra un error
                console.log(response);
                //alert('error');

            }
    }); 
	
}

function traer_docente_materia(nodoanioid, divisionid, id_colegio)
{
    var url = urlApi+"/usuario/obtener_usuariosXrol"; 
    $.ajax({
          url: url,
          type: 'GET',
          headers: {              
              'APIKEY' : passApiKey,
              'userid' : idUser,
              'Access-Control-Allow-Origin': '*'
          },
          data:{idcolegio:id_colegio, nombregrupo:'Docente'},
          success: function(docentes){  
            if(docentes['status']==false)
            { 
              if(docentes['message']=='NO LOGUIN')
                location.href = "<?php echo base_url('login'); ?>"; 
              else
                bootbox.alert("No hay docentes cargados");
            }
            else
            { 
                  var docentes = jQuery.parseJSON( docentes );  
                 
                  
                  url = urlApi+"/materia/anios_materia"; 
                  $.ajax({
                        url: url,
                        type: 'GET',
                        headers: {              
                            'APIKEY' : passApiKey,
                            'userid' : idUser,
                            'Access-Control-Allow-Origin': '*'
                        },
                        data:{idcolegio:id_colegio, nodoanioid:nodoanioid},
                        success: function(data){  
                          if(data['status']==false)
                          { 
                            if(data['message']=='NO LOGUIN')
                              location.href = "<?php echo base_url('login'); ?>"; //no està 
                            else 
                              bootbox.alert("No hay materias cargadas para el año seleccionado");
                          }
                          else
                          { 
                                var materias = jQuery.parseJSON( data );  
                                //console.log(materias);
                                $(".casilla").unbind("click" );
                                $(".nombremateria_link").unbind("click" ); 



                                $('.casilla').click(function(e){
                                    e.stopPropagation();
                                    var tdid=$(this).attr('id');
                                    var arre=Array();
                                    arre = tdid.split('_');
                                    var idhorario = arre[0];
                                    var iddia = arre[1];

                                    optionesHorario="";

                                    for(var k=0; k < horarios.length ; k++)
					                  {
						                 if(horarios[k]["colegio_id"] == id_colegio)
						                 {
						                 	 hora_inicio = horarios[k]["hora_inicio"];
						                    hora_inicio = hora_inicio.substr(0,5);
						                     hora_fin = horarios[k]["hora_fin"];
						                    hora_fin = hora_fin.substr(0,5);
						                    optionesHorario = optionesHorario + '<option value="'+horarios[k]["id"]+'">'+hora_inicio+' - '+hora_fin+'</option>' ;

						                    //console.log(horarios[i]['hora_inicio']);
						                    //console.log(hora_inicio);
						                    
						                 }
						                    
					                  }
					                var colegios = JSON.parse('<?php echo json_encode($this->session->userdata("colegios")); ?>');
									//obtener valor de edita calendario de la variable multicolegio
									//edita_horario = 1;
									for(var i=0; i < colegios.length ; i++)
									        {
									         //console.log(colegios[i]["nombre_colegio"]);
									     
									         //console.log(idcolegio = colegios[i]["menu"]);
									         menu = colegios[i]["menu"];
									         for(var j=0; j < menu.length ; j++)
									         {
									          //console.log('menu: '+menu[j]["nombre"]);
									          if((menu[j]["nombre"] == 'editar_horario') && (colegios[i]["id_colegio"] == id_colegio))
									          {
									            edita_horario = 1;
									          }
									         }
									       
									        }

                                    if(edita_horario == 1) 
                                    {    
                                        abrir_dialog(idhorario, iddia, docentes, materias, divisionid, nodoanioid, id_colegio);
                                    }
                                    else {
                                    	//alert('por aquiiiii');
                                          console.log("no tiene permisos para cargar un horario");

                                        }
                                }); 


                                $( '.nombremateria_link').click(function(e){
                                    //alert('click');

                                    e.stopPropagation();
                                    //armar option de horarios segun id_colegio
                                    optionesHorario="";
                                    for(var k=0; k < horarios.length ; k++)
					                  {
						                 if(horarios[k]["colegio_id"] == id_colegio)
						                 {
						                 	 hora_inicio = horarios[k]["hora_inicio"];
						                    hora_inicio = hora_inicio.substr(0,5);
						                     hora_fin = horarios[k]["hora_fin"];
						                    hora_fin = hora_fin.substr(0,5);
						                    optionesHorario = optionesHorario + '<option value="'+horarios[k]["id"]+'">'+hora_inicio+' - '+hora_fin+'</option>' ;

						                    //console.log(horarios[i]['hora_inicio']);
						                    //console.log(hora_inicio);
						                    
						                 }
						                    
					                  }
                                    //

                                    var idmatlink= $(this).attr('id');

                                    var arre=Array();
                                    arre = idmatlink.split('_');
                                    var idhormat = arre[1];
                                    
                                    var colegios = JSON.parse('<?php echo json_encode($this->session->userdata("colegios")); ?>');
                                    //console.log(colegios);
                                    for(var i=0; i < colegios.length ; i++)
									        {
									         //console.log(colegios[i]["nombre_colegio"]);
									     
									         //console.log(idcolegio = colegios[i]["menu"]);
									         menu = colegios[i]["menu"];
									         for(var j=0; j < menu.length ; j++)
									         {
									         // console.log('menu: '+menu[j]["nombre"]);
									          if((menu[j]["nombre"] == 'editar_horario') && (colegios[i]["id_colegio"] == id_colegio))
									          {
									          	//alert('entro');
									            edita_horario = 1;
									          }
									         }
									       
									        }
									        //edita_horario = 1;
                                    if(edita_horario == 1) 
                                    {    //alert(optionesHorario);
                                        abrir_dialog_detalle( docentes, materias, idhormat, nodoanioid, id_colegio);
                                    }
                                    else {
                                    	//alert('por aqui');
                                           console.log("no tiene permisos para editar un horario");

                                        }
                                });    

                                
                            
                         }                             
                      },
                      error: function(response){
                          console.log(response);                   
                      }           
                  }); 

              
            }                             
          },
          error: function(response){
              console.log(response);                   
          }           
    }); 
}



function retorna_formularioaltahorario (horario, dia)
{
  var texto=  
              '<form role="form" id="Nhorario" name="Nhorario">'+
                
                '<div id="DivDatos">'+  
                 //div de la izquierda
                '<div class="col-sm-12 b-r">'+  

                      '<div class="form-group" id="div_select_dia"> ' +
                        '<label>Dia</label>' +
                        '<select id="dia" name="dia" class="form-control input-md" required/> *' +
                      '</div>'+

                      '<div class="form-group" id="div_select_horario"> ' +
                        '<label>Horario</label>' +
                        '<select id="horario" name="horario" class="form-control input-md" required/> *' +
                      '</div>'+

                      '<div class="form-group" id="div_select_materia"> ' +
                        '<label>Materia</label>' +

                        '<select id="materia" name="materia" class="form-control input-md"  required/> *' +

                      '</div>'+

                       '<div class="form-group" id="div_select_docente"> ' +
                        '<label>Docente</label>' +
                        '<select id="docente" name="docente" class="form-control input-md" />' +
                      '</div>'+

                      '<div class="form-group"><label>Lugar</label> <input type="text" placeholder="Ingrese Aula" class="form-control" id="lugar" name="lugar"/></div>'+

                '</div>'+
              
              '</div>' + //fin DivFiltro
            '</form>';
                            
        return texto; 
}

	function abrir_dialog(horario, dia, docentes, materias, divisionid, nodoanioid, idcolegio)
{ 

  bootbox.dialog({
              backdrop: true,
              title: "Cargar Horario...",
              message:  
                  '<div class="row ">'+
                    '<div class="col-lg-12">'+
                        '<div class="ibox collapsed float-e-margins" id="ibox_collapsed">'+
                            '<div class="ibox-content">'+
                                '<div class="row">'+
                                    retorna_formularioaltahorario() +
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                  '</div>',
              buttons: {
                  success: {
                      label: "Guardar",
                      className: "btn-success",
                      callback: function (e) {  

                          var $myForm = $('#Nhorario');

                          var selectmat = $('#materia').val();

                          if( ($myForm[0].checkValidity()) && (selectmat!=-1) ) 
                          {
                            var horario = $('#horario').val();
                            var dia = $('#dia').val();
                            var docente = $('#docente').val();
                            var materia = $('#materia').val();
                            var lugar = $('#lugar').val();

                            var parametros = {
                              'horarios_id': horario,
                              'dias_id': dia,     
                              'docente_id': docente,    
                              'materias_id': materia, 
                              'lugar': lugar,
                              'divisiones_id':divisionid,
                              'colegio_id':idcolegio,

                            };
                            
                            guardar_horario(parametros, nodoanioid, idcolegio); 
                            return false;
                          }
                          else {
                                bootbox.alert("Ingrese los campos obligatorios");
                                return false;
                                }
                                                                 
                      }//fin calback
                  }//fin success
              },//fin Buttons

              onEscape: function() {return ;},
        });  //FIN DIALOG
	//cargar los seelect
	/*movimientos dentro del dialog guardar horario.......*/
    $('.ibox-content').css('display','block');

    $('#dia').append(optionesDias); 
    $('#horario').append(optionesHorario); 

    var opcion = "<option value='-1'>Seleccione una materia</option>" ; //anio_id 
    $('#materia').append(opcion); 
    for(var j=0; j < materias.length; j++) 
    {
      $options = $('#div_select_materia').find('option');
      $targetOption = $options.filter(
         function () {return jQuery(this).val() == materias[j]['id']}
      );//esto es para buscar un option por text. manso lio pero no hay otra.                         
      
      if($targetOption.length == 0)
      {   
          opcion = "<option value='"+materias[j]['id']+"'>" +materias[j]['nombre']+ "</option>" ; //anio_id 
          $('#materia').append(opcion); 
      }
    }
    opcion = "<option value='-1'>Seleccione un docente</option>" ; //anio_id 
    $('#docente').append(opcion); 
    for(var j=0; j < docentes.length; j++) 
    {
      $options = $('#div_select_docente').find('option');
      $targetOption = $options.filter(
         function () {return jQuery(this).val() == docentes[j]['id']}
      );//esto es para buscar un option por text. manso lio pero no hay otra.                         
      
      if($targetOption.length == 0)
      {   
          var nombre = docentes[j]['last_name'] +' '+ docentes[j]['first_name'];
          var opcion = "<option value='"+docentes[j]['id']+"'>" +nombre+ "</option>" ; //anio_id 
          $('#docente').append(opcion); 
      }
    }
    $("#dia").val(dia);
    $("#horario").val(horario);
    
    $( "#dia" ).select2({
    });
    $('#s2id_dia').css('display','block');
    $( "#horario" ).select2({
    });
    $('#s2id_horario').css('display','block');

    $( "#docente" ).select2({
    
    });
    $('#s2id_docente').css('display','block');

    $( "#materia" ).select2({
      
    });
    $('#s2id_materia').css('display','block');
}

function abrir_dialog_detalle( docentes, materias, idhormat, nodoanioid, id_colegio)
{ 
  url = urlApi+"/horario/horarioXid"; 
  //alert('abrir_dialog_detalle');
  $.ajax({
      url: url,
      type: 'GET',
      headers: {              
          'APIKEY' : passApiKey,
          'userid' : idUser,
          'Access-Control-Allow-Origin': '*'
      },
      data:{id:idhormat},
      success: function(data)
      {  
        if(data['status']==false)
        { 
          if(data['message']=='NO LOGUIN')
            location.href = "<?php echo base_url('login'); ?>"; //no està 
        }
        else
        { 
            var data = jQuery.parseJSON( data );
            //console.log(data);

            bootbox.dialog({
                  backdrop: true,
                  title: "Modificar Horario...",
                  message:  
                      '<div class="row ">'+
                        '<div class="col-lg-12">'+
                            '<div class="ibox collapsed float-e-margins" id="ibox_collapsed">'+
                                '<div class="ibox-content">'+
                                    '<div class="row">'+
                                        retorna_formularioaltahorario() +
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                      '</div>',
                  buttons: {
                      success: {
                          label: "Guardar",
                          className: "btn-success",
                          callback: function (e) {  

                              var $myForm = $('#Nhorario');
                              if ($myForm[0].checkValidity()) 
                              {
                                var horario = $('#horario').val();
                                var dia = $('#dia').val();
                                var docente = $('#docente').val();
                                var materia = $('#materia').val();
                                var lugar = $('#lugar').val();

                                var parametros = {
                                  'horarios_id': horario,
                                  'dias_id': dia,     
                                  'docente_id': docente,    
                                  'materias_id': materia, 
                                  'lugar': lugar
                                };
                                
                                modificar_horario(parametros, idhormat, nodoanioid, id_colegio); 
                                return false;
                              }
                              else {
                                    bootbox.alert("Ingrese los campos obligatorios");
                                    return false;
                                    }
                                                                     
                          }//fin calback
                      }//fin success
                  },//fin Buttons

                  onEscape: function() {return ;}
            });  //FIN DIALOG
			/*movimientos dentro del dialog guardar horario.......*/
		    //movimientos dentro del dialog guardar evento......./
            $('.ibox-content').css('display','block');
            //alert('llego hasta aca');
            
            
            $('#dia').append(optionesDias); 
              
            
            $('#horario').append(optionesHorario); 
          

            var opcion = "<option value='-1'>Seleccione una materia</option>" ; //anio_id 
            $('#materia').append(opcion); 
            for(var j=0; j < materias.length; j++) 
            {
              $options = $('#div_select_materia').find('option');
              $targetOption = $options.filter(
                 function () {return jQuery(this).val() == materias[j]['id']}
              );//esto es para buscar un option por text. manso lio pero no hay otra.                         
              
              if($targetOption.length == 0)
              {   
                  opcion = "<option value='"+materias[j]['id']+"'>" +materias[j]['nombre']+ "</option>" ; //anio_id 
                  $('#materia').append(opcion); 
              }
            }

            opcion = "<option value='-1'>Seleccione un docente</option>" ; //anio_id 
            $('#docente').append(opcion); 
            for(var j=0; j < docentes.length; j++) 
            {
              $options = $('#div_select_docente').find('option');
              $targetOption = $options.filter(
                 function () {return jQuery(this).val() == docentes[j]['id']}
              );//esto es para buscar un option por text. manso lio pero no hay otra.                         
              
              if($targetOption.length == 0)
              {   
                  var nombre = docentes[j]['last_name'] +' '+ docentes[j]['first_name'];
                  var opcion = "<option value='"+docentes[j]['id']+"'>" +nombre+ "</option>" ; //anio_id 
                  $('#docente').append(opcion); 
              }
            }

            $("#dia").val(data[0]['dias_id']);
            $("#horario").val(data[0]['horarios_id']);
            $("#materia").val(data[0]['nombresmateria_id']);
            if(data[0]['docente_id'] != null)
              $("#docente").val(data[0]['docente_id']);
            $("#lugar").val(data[0]['lugar']);

            
            $( "#dia" ).select2({
              theme: "bootstrap",
              placeholder: "Seleccione los usuarios",
              //allowClear: true
            });
            $('#s2id_dia').css('display','block');

            $( "#horario" ).select2({
              theme: "bootstrap",
              placeholder: "Seleccione los roles",
              allowClear: true
            });
            $('#s2id_horario').css('display','block');

            $( "#docente" ).select2({
              theme: "bootstrap",
              placeholder: "Seleccione los niveles",
              allowClear: true
            });
            $('#s2id_docente').css('display','block');

            $( "#materia" ).select2({
              theme: "bootstrap",
              placeholder: "Seleccione los niveles",
              allowClear: true
            });
            $('#s2id_materia').css('display','block');
  
		}
		}
	}); 
}

function traer_docente_materia2(nodoanioid, idcolegio)
{
    var url = urlApi+"/usuario/obtener_usuariosXrol"; 
    $.ajax({
          url: url,
          type: 'GET',
          headers: {              
              'APIKEY' : passApiKey,
              'userid' : idUser,
              'Access-Control-Allow-Origin': '*'
          },
          data:{idcolegio:idcolegio, nombregrupo:'Docente'},
          success: function(docentes){  
            if(docentes['status']==false)
            { 
              if(docentes['message']=='NO LOGUIN')
                location.href = "<?php echo base_url('login'); ?>"; 
            }
            else
            { 
                  var docentes = jQuery.parseJSON( docentes );  
                  //console.log(docentes);
                  
                  url = urlApi+"/materia/anios_materia"; 
                  $.ajax({
                        url: url,
                        type: 'GET',
                        headers: {              
                            'APIKEY' : passApiKey,
                            'userid' : idUser,
                            'Access-Control-Allow-Origin': '*'
                        },
                        data:{idcolegio:idcolegio, nodoanioid:nodoanioid},
                        success: function(data){  
                          if(data['status']==false)
                          { 
                            if(data['message']=='NO LOGUIN')
                              location.href = "<?php echo base_url('login'); ?>"; //no està 
                          }
                          else
                          { 
                                var materias = jQuery.parseJSON( data );  
                                //console.log(materias);

                                $(".nombremateria_link").unbind("click" ); 

                                $( '.nombremateria_link').click(function(e){
                                    //alert('click');
                                    e.stopPropagation();

                                    var idmatlink= $(this).attr('id');

                                    var arre=Array();
                                    arre = idmatlink.split('_');
                                    var idhormat = arre[1];
                                    var colegios = JSON.parse('<?php echo json_encode($this->session->userdata("colegios")); ?>');
									//obtener valor de edita calendario de la variable multicolegio
									
									for(var i=0; i < colegios.length ; i++)
									        {
									         //console.log(colegios[i]["nombre_colegio"]);
									     
									         //console.log(idcolegio = colegios[i]["menu"]);
									         menu = colegios[i]["menu"];
									         for(var j=0; j < menu.length ; j++)
									         {
									          //console.log(menu[j]["nombre"]);
									          if((menu[j]["nombre"] == 'editar_horario') && (colegios[i]["id_colegio"] == idcolegio))
									          {
									            edita_horario = 1;
									          }
									         }
									       
									        }

                                    if(edita_horario == 1) 

                                    {    
                                        abrir_dialog_detalle( docentes, materias, idhormat, nodoanioid,idcolegio );
                                    }
                                    else {
                                    	//alert('por aqui');
                                          console.log("no tiene permisos para editar un horario");
                                        }
                                });    

                                
                            
                         }                             
                      },
                      error: function(response){
                          console.log(response);                   
                      }           
                  }); 

              
            }                             
          },
          error: function(response){
              console.log(response);                   
          }           
    }); 
}

function guardar_horario(newH, nodoanioid, id_colegio){ 
               
    bootbox.prompt({
      title: "Ingrese su PIN",
      value: "", 
      inputType: "password",
      maxlength: 4,
      callback: function(pin) {
                    
        if (pin === null) {                                             
           return true; 
        } else {   
              var newhorario  = JSON.stringify(newH);
           
              var urlRequest = urlApi+"/horario/inserthorarios_materias";
              $.ajax({
                    url: urlRequest,
                    type: 'POST',
                    headers: {    
                    'APIKEY' : passApiKey,
                    'userid' : idUser
                  },
                  data: {data: newhorario, pin:pin, id_colegio: id_colegio}, 
                  beforeSend: function() {
                      $('.btn-success').after("<img id='loadingNiv' src='<?=base_url('assets/images/loading.gif');?>' height='30' width='30' />");
                  },
                  success: function(data){
                         
                        console.log(data);
                        $('#loadingNiv').remove(); 

                        if(data['status']==false)
                        {  //alert(data['message']);
                            if(data['message'] == 'NO LOGUIN')                                        
                                location.href = "<?php echo base_url('login'); ?>";  
                            else  if(data['message'] == 'pininvalido')
                                  {    //alert('PIN INVALIDO');
                                      bootbox.alert("PIN INVALIDO!", function() { });
                                   return false;
                                  }
                        }
                        else{    
                                bootbox.hideAll();

                                var datos = jQuery.parseJSON( data['datos'] );
                                

                                if( datos[0]['first_name']==null )
                                    var nombredocente = "";
                                else var nombredocente = datos[0]['last_name']+' '+datos[0]['first_name'];

                                var divmateria = '<div align="center" class="divcasilla" id="divcasilla_dir_'+datos[0]['id']+'_'+id_colegio+'">'+
                                      '<h4><a class="nombremateria_link" id="nombremateria_'+datos[0]['id']+'"><b>'+datos[0]['materia'] +'</b></a> <button type="button" class="bootbox-close-button close btn-sm" data-dismiss="modal" aria-hidden="true" style="display:none">×</button></h4>'+
                                      '<i>'+nombredocente+'</i>'+
                                 '</div>';


                                //var celda=$('#tabladatos_'+id_colegio+' tbody').find('[id="'+datos[0]['horarios_id']+'_'+datos[0]['dias_id']+'"]');
                                var celda=$('#tabladatos_'+id_colegio+' tbody').find('[id="dir_'+datos[0]['horarios_id']+'_'+datos[0]['dias_id']+'_'+id_colegio+'"]');


                                $(celda).append(divmateria);

                                $(".divcasilla").mouseover(function() {
                                  var close = $(this).find('.close');
                                  $(close).css('display','block');
                                });

                                $(".divcasilla").mouseout(function() {
                                    var close = $(this).find('.close');
                                    $(close).css('display','none');
                                });

                                $('.close').click(function(e){
                                    e.stopPropagation();
                                    var div = $(this).parent('h4').parent('div');
                                    var divid = $(div).attr('id');
                                    var arre=Array();
                                    arre = divid.split('_');
                                    var divid = arre[2];
                                    //alert(divid);

                                    var colegios = JSON.parse('<?php echo json_encode($this->session->userdata("colegios")); ?>');
									//obtener valor de edita calendario de la variable multicolegio
									
									for(var i=0; i < colegios.length ; i++)
									        {
									         //console.log(colegios[i]["nombre_colegio"]);
									     
									         //console.log(idcolegio = colegios[i]["menu"]);
									         menu = colegios[i]["menu"];
									         for(var j=0; j < menu.length ; j++)
									         {
									          //console.log(menu[j]["nombre"]);
									          if((menu[j]["nombre"] == 'editar_horario') && (colegios[i]["id_colegio"] == id_colegio))
									          {
									            edita_horario = 1;
									          }
									         }
									       
									        }

                                    if(edita_horario == 1) 
                                    {    
                                      //console.log(divid);
                                        deletehorario(divid, id_colegio);
                                    }
                                    else {
                                    	//alert('por aqui');
                                          console.log("no tiene permisos para eliminar un horario");
                                        }
                                }); 

                                traer_docente_materia2(nodoanioid);

                                $('.top-right').notify({
                                  message: { text: 'Horario Guardado' },
                                  fadeOut: { enabled: true, delay: 5000 },
                                  type: 'success2'
                                }).show(); 
                            }
                                     
                  },

                  error: function(response){
                      console.log(response);
                      $('#loadingNiv').remove(); 
                      bootbox.hideAll();               
                  }

              });                   
      
                
          }
      }
    });//fin prompt
} 

function modificar_horario(parametros, idhormat, nodoanioid, id_colegio){

    var  parametros2 = parametros;
    var  idhormat2 = idhormat;
              
    bootbox.prompt({
      title: "Ingrese su PIN",
      value: "", 
      inputType: "password",
      maxlength: 4,
      callback: function(pin) 
      {
                    
        if (pin === null) {                                             
           return true; 
        } 
        else 
        {   
              var parametros  = JSON.stringify(parametros2);
              
              var urlRequest = urlApi+"/horario/update_horarios_materias";
              $.ajax({
                    url: urlRequest,
                    type: 'POST',
                    headers: {    
                    'APIKEY' : passApiKey,
                    'userid' : idUser
                  },
                  data: {data: parametros, pin:pin, idhormat:idhormat}, 
                  beforeSend: function() {
                      $('.btn-success').after("<img id='loadingNiv' src='<?=base_url('assets/images/loading.gif');?>' height='30' width='30' />");
                  },
                  success: function(data){
                         
                        //console.log(data);
                        $('#loadingNiv').remove(); 

                        if(data['status']==false)
                        {  //alert(data['message']);
                            if(data['message'] == 'NO LOGUIN')                                        
                                location.href = "<?php echo base_url('login'); ?>";  
                            else  if(data['message'] == 'pininvalido')
                                  {    //alert('PIN INVALIDO');
                                      bootbox.alert("PIN INVALIDO!", function() { });
                                   return false;
                                  }
                        }
                        else{    
                                bootbox.hideAll();
                               
                                
                                var datos = jQuery.parseJSON( data['datos'] );
                                
                                $('#divcasilla_dir_'+idhormat2+'_'+id_colegio).remove();

                                if( datos[0]['first_name']==null )
                                    var nombredocente = "";
                                else var nombredocente = datos[0]['last_name']+' '+datos[0]['first_name'];

                                var divmateria = '<div align="center" class="divcasilla" id="divcasilla_dir_'+datos[0]['id']+'_'+id_colegio+'">'+
                                      '<h4><a class="nombremateria_link" id="nombremateria_'+idhormat2+'"><b>'+datos[0]['materia'] +'</b></a>'+
                                        '<button type="button" class="bootbox-close-button close btn-sm" data-dismiss="modal" aria-hidden="true" style="display:none">×</button>'+
                                      ' </h4>'+
                                      '<i>'+nombredocente+'</i>'+
                                 '</div>';

                                //var celda=$('#tabladatos_'+id_colegio+' tbody').find('[id="'+datos[0]['horarios_id']+'_'+datos[0]['dias_id']+'"]');
                                var celda=$('#tabladatos_'+id_colegio+' tbody').find('[id="dir_'+datos[0]['horarios_id']+'_'+datos[0]['dias_id']+'_'+id_colegio+'"]');
                                $(celda).append(divmateria);

                                $(".divcasilla").mouseover(function() {
                                  var close = $(this).find('.close');
                                  $(close).css('display','block');
                                });

                                $(".divcasilla").mouseout(function() {
                                    var close = $(this).find('.close');
                                    $(close).css('display','none');
                                });

                                $('.close').click(function(e){
                                    e.stopPropagation();
                                    var div = $(this).parent('h4').parent('div');
                                    var divid = $(div).attr('id');
                                    var arre=Array();
                                    arre = divid.split('_');
                                    var divid = arre[2];
                                    alert(divid);
                                    var colegios = JSON.parse('<?php echo json_encode($this->session->userdata("colegios")); ?>');
                                    for(var i=0; i < colegios.length ; i++)
								        {
								        // console.log(colegios[i]["nombre_colegio"]);
								     
								         //console.log(idcolegio = colegios[i]["menu"]);
								         menu = colegios[i]["menu"];
								         for(var j=0; j < menu.length ; j++)
								         {
								         // console.log(menu[j]["nombre"]);
								          if((menu[j]["nombre"] == 'editar_horario') && (colegios[i]["id_colegio"] == id_colegio))
								          {
								            edita_horario = 1;
								          }
								         }
								       
								        }

                                    if(edita_horario == 1) 
                                    {    
                                     // console.log(divid);
                                        deletehorario(divid, id_colegio);
                                    }
                                    else {
                                    	//alert('por aqui');
                                          console.log("no tiene permisos para eliminar un horario");
                                        }
                                });

                                 traer_docente_materia2(nodoanioid, id_colegio); 

                                 $('.top-right').notify({
                                  message: { text: 'Horario Modificado' },
                                  fadeOut: { enabled: true, delay: 5000 },
                                  type: 'success2'
                                }).show(); 

                            }
                                     
                  },

                  error: function(response){
                      console.log(response);
                      $('#loadingNiv').remove(); 
                      bootbox.hideAll();               
                  }

              });                   
      
                
          }
      }
    });//fin prompt
} 
 
function deletehorario(id, id_colegio){
              
    bootbox.prompt({
      title: "Seguro que quiere borrar horario? Ingrese su PIN para confirmar:",
      value: "", 
      inputType: "password",
      maxlength: 4,
      callback: function(pin) 
      {                 
        if (pin === null) {                                             
           return true; 
        } 
        else 
        {     
              var urlRequest = urlApi+"/horario/delete_horarios_materias";
              $.ajax({
                    url: urlRequest,
                    type: 'POST',
                    headers: {    
                    'APIKEY' : passApiKey,
                    'userid' : idUser
                  },
                  data: {horarioid: id, pin:pin}, 
                  beforeSend: function() {
                      /*$('.btn-success').after("<img id='loadingNiv' src='<=base_url('assets/images/loading.gif');?>' height='30' width='30' />");*/
                  },
                  success: function(data){
                       // console.log(data);
                        $('#loadingNiv').remove(); 

                        if(data['status']==false)
                        {  //alert(data['message']);
                            if(data['message'] == 'NO LOGUIN')                                        
                                location.href = "<?php echo base_url('login'); ?>";  
                            else  if(data['message'] == 'pininvalido')
                                  {    //alert('PIN INVALIDO');
                                      bootbox.alert("PIN INVALIDO!", function() { });
                                   return false;
                                  }
                        }
                        else{   
                                $('#divcasilla_dir_'+id+'_'+id_colegio).remove();

                                 /*$('.top-right').notify({
                                  message: { text: 'Horario Eliminado' },
                                  fadeOut: { enabled: true, delay: 5000 },
                                  type: 'success2'
                                }).show(); */
								bootbox.alert("Horario Eliminado");

                            }
                                     
                  },

                  error: function(response){
                      console.log(response);
                      $('#loadingNiv').remove(); 
                  }

              });                   
      
                
          }
      }
    });//fin prompt
} 

	/*$(document).ready(function()
	{
		$('#btn_alumnos').click(function(e){
        //e.preventDefault();
        //alert('entro al btn_alumnos');
		    $(".casilla").mouseover(function() {
		            $(this).addClass("td_hover");
		    });
		    
	          $(".casilla").mouseout(function() {
	               $(this).removeClass("td_hover");
	          });
	    	$('.pull-down').css('margin-bottom', '-10px');
    	});
	     
	});*/

	//aqui comienza la carga de la agenda del docente
function completar_agenda_docente(idcolegio)
{
		//alert(idcolegio);
	var url = urlApi+"/horario/obtener_all_horarios_docente_id_x_colegio"; 
    $.ajax({
        url: url,
        type: 'GET',
        headers: {              
              'APIKEY' : passApiKey,
              'userid' : idUser,
              'Access-Control-Allow-Origin': '*'
        },
        data:{idcolegio:idcolegio},
        success: function(data)
        {  
	        if(data['status']==false)
	        { 
	          if(data['message']=='NO LOGUIN')
	            location.href = "<?php echo base_url('login'); ?>"; 
	        }
	        else
	        { 
	        	//console.log(data);
	        	var data = jQuery.parseJSON( data );
	        	//console.log(data);
                for (var j=0; j < data.length; j++ )
                  {
            		var hora = data[j]['horarios_id'];
                    var dia = data[j]['dias_id'];
                    var materia = data[j]['nombre_materia'];
                    if(data[j]['nombre_especialidad'])
                    {
                    	
                    	var curso = data[j]['nombre_anio']+' '+data[j]['nombre_division']+' - '+data[j]['nombre_especialidad'];
                    }
                    else
                    {
                    	var curso = data[j]['nombre_anio']+' '+data[j]['nombre_division'];
                    }
                    
                    var lugar = data[j]['lugar'];
                    var divmateria = '<div align="center" class="divcasilla" id="divcasilla_doc_'+data[j]['id']+'_'+idcolegio+'">'+
                                          '<h4 style="color: #1E90FF; font-weight: bold">'+materia+'</h4>'+
                                          '<h5 style="color: #B1251E; font-style: italic">'+curso+'</h5>'+
                                          '<h5 style="color: #B1251E; font-style: italic">'+lugar+'</h5>'+
                                     '</div>';

                    var celda=$('#tabladatos_doc_'+idcolegio+' tbody').find('[id="doc_'+data[j]['horarios_id']+'_'+data[j]['dias_id']+'_'+idcolegio+'"]');

                    $(celda).append(divmateria);
                   }
	              
	        }                             
        },
          error: function(response){
              console.log(response);                   
          }           
    }); 
}

$("#btn_listar_alumnos1").on("click",function(event) 
		{
			alert('hola');
			var id_colegio = $("#btn_listar_alumnos1").attr("value");
			alert(id_colegio);
		});
});
</script>