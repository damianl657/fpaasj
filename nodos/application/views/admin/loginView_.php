﻿<!--<!DOCTYPE html>-->
<!-- saved from url=(0051)http://works.sietedefebrero.com/nodos/dev/login.php -->
<!--<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<title>Nodos Comunicación escolar más simple</title>
	<meta name="description" content="App para una comunicación escolar más simple">
	<meta name="author" content="sietedefebrero.com">
	Favicon

</head>-->
<script>
 var passApiKey = '<?php echo $passApiKey; ?>';
 var urlApi = '<?php echo $urlApi; ?>';

function ingresar(){
        var urlRequest = urlApi+"/login/acceder";

        var email = $('#correo').val();

        var password = $('#clave').val();

        //console.log(password);
        $.ajax({
            url: urlRequest,
            type: 'POST',
            headers: {
              'APIKEY' : passApiKey
            },
            data: {email: email, password:password},
            beforeSend: function() {
                    /*$('.top-right').notify({
                        message: { text: 'Cargando...' },
                        fadeOut: { enabled: true, delay: 2000 },
                         type: 'warning2'
                    }).show(); */
                    //$.notify("Cargando...", "warn");
                    $.notify("Cargando...", "info");
            },
            success: function(data){
                //console.log(data);
                if(data['userid'])
                {
                    if(data['iteration'] == 0){  // si es la primera vez que ingresa que cambie contraseña y pin
              			var url_action = "<?php echo base_url('login/actualizar_contrasenia') ?>";
            			  $('#idusuario').val(data['userid']);
                   // alert(data['userid']);
                    //alert( $('#idusuario').val());
            			  $('#formlogin').attr('action', url_action);
            			  //$('#formlogin').submit();
                    document.forms['formlogin'].submit();
          	        }else{
          				  $('#idusuario').val(data['userid']);
                          /*$('.top-right').notify({
      											message: { text: data['message'] },
      											fadeOut: { enabled: true, delay: 3000 },
      											type: 'success2'
      										}).show(); */
                          $.notify("Acceso Correcto. redireccionando...", "success");
                          document.forms['formlogin'].submit();
                    }
                }
                else
                  {
                    if(data['responseText'])
                    {
                      data = JSON.parse(data['responseText'],true);
                      /*$('.top-right').notify({
                          message: { text: data['message'] },
                          fadeOut: { enabled: true, delay: 3000 },
                           type: 'danger2'
                       }).show();*/
                      $.notify(data['message'], "error");
                      $('#correo').focus();
                    }
                  }
              },
              error: function(response){
                  //console.log(response);
                  if(response['responseText'])
                  {
                    response = JSON.parse(response['responseText'],true);
                     /*$('.top-right').notify({
                          message: { text: response['message']},
                          fadeOut: { enabled: true, delay: 3000 },
                           type: 'danger2'
                        }).show();   */
                      $.notify(response['message'], "error");
                  }

              }

        });
        return false;
}

$(document).ready(function(){
    $.noConflict();
    $('#correo').focus();
    /* $('#encriptar').click(function(event){   //REGISTRO
          event.preventDefault();
          var urlRequest = urlApi+"/login/prueba_encriptar";
          var password = $('#password').val();
         alert(password);
          $.ajax({

              url: urlRequest,

              type: 'POST',

              headers: {

                'APIKEY' : passApiKey

              },

              data: {password:password},

              success: function(data){

                  console.log(data);

                  if(data['status']==true)

                  {

                      alert(data['userid']);



                  }

                    else{

                       alert(data['message']);

                      }

                },

                error: function(response){

                    console.log(response);

                }

              });

    });*/





    /*$('#create').click(function(event){   //REGISTRO

          event.preventDefault();

          var urlRequest = urlApi+"/login/registro";



          var email = $('#email').val();

          var password = $('#password').val();

          var username = $('#username').val();

          var nombre = $('#nombre').val();

          var apellido = $('#apellido').val();



          $.ajax({

              url: urlRequest,

              type: 'POST',

              headers: {

                'APIKEY' : passApiKey

              },

              data: {email: email, password:password, username:username, nombre:nombre, apellido:apellido},

              success: function(data){

                  console.log(data);

                  if(data['status']==true)

                  {

                      alert(data['message']);



                      $('form').animate({height: "toggle", opacity: "toggle"}, "slow");

                      $('#correo').val(email);

                      $('#clave').val(password);

                  }

                    else{

                       alert(data['message']);

                      }

                },

                error: function(response){

                    console.log(response);

                }

              });

    });*/





    $('#login').click(function(event){    //LOGIN

        event.preventDefault();

        ingresar();

    });

    $(document).keypress(function(e) {

        if(e.which == 13) {

            ingresar();

        }

    });



  });

</script>





<body>



	<!-- count particles -->

	<div class="count-particles">

		<span class="js-count-particles">80</span> particles

	</div>



	<!-- particles.js container -->

	<div id="particles-js"><canvas class="particles-js-canvas-el" width="1059" height="599" style="width: 100%; height: 100%;"></canvas></div>



	<link rel="shortcut icon" href="http://works.sietedefebrero.com/nodos/dev/favicon.ico?v=2" type="image/x-icon">

	<link rel="icon" href="http://works.sietedefebrero.com/nodos/dev/favicon.ico?v=2" type="image/x-icon">



	<link href="<?=base_url('assets/css/style.css');?>" rel="stylesheet">

	<!-- Google Fonts -->

	<link href="<?=base_url('assets/css/css');?>" rel="stylesheet" type="text/css">



	<!-- scripts -->

	<script src="<?=base_url('assets/js/particles/particles.js');?>"></script>

	<script src="<?=base_url('assets/js/particles/app.js');?>"></script>



	<!-- stats.js -->

	<script src="<?=base_url('assets/js/particles/stats.js');?>"></script>

	<script>

	var count_particles, stats, update;

	stats = new Stats;

	stats.setMode(0);

	stats.domElement.style.position = 'absolute';

	stats.domElement.style.left = '0px';

	stats.domElement.style.top = '0px';

	document.body.appendChild(stats.domElement);

	count_particles = document.querySelector('.js-count-particles');

	update = function() {

		stats.begin();

		stats.end();

		if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {

			count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;

		}

		requestAnimationFrame(update);

	};

	requestAnimationFrame(update);

	</script>





	<div class="container">

		<div class="row">

			<div class="col-lg-offset-4 col-lg-4 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-12">



				<div class="login">

					<img src="<?=base_url('assets/images/logo-login.png');?>" width="90" alt="NODOS">



					<!--<form>

						<input type="email" id='email' placeholder="email">

            <input type="text" id="username" placeholder="username">

						<input type="password" id="password" placeholder="Contraseña">

            <input type="text" id="nombre" placeholder="nombre">

            <input type="text" id="apellido" placeholder="apellido">



						<input id='create' type="button" value="registrar">

            <input id='encriptar' type="button" value="encriptar">

					</form>-->

					<form method="post" action="login/principal" id="formlogin" >

			            <input id='correo' type="text" name="username" placeholder="Username, DNI o Email, " required>

			            <input id='clave' type="password" name="password" placeholder="Contraseña" required>

                  <input id='idusuario' type="hidden" name="idusuario" >

			            <input id='login' type="button" value="INGRESAR" class="btn2  btn-primary2">

                  <!--<input id='login' type="submit" value="INGRESAR" >-->

<div class="row">

	 <div class="form-group col-sm-6">

     		<a href="login/olvido_pass">¿Olvid&oacute; su Contraseña?</a>

 	</div>



	 <div class="form-group col-sm-6">

		<a href="https://e-nodos.com/formulario/" target="_blank">Asistencia T&eacutecnica</a>

    	</div>

</div>









			        </form>

				</div>



			</div>

		</div>

	</div>



  <!--<div class='notifications top-right'></div>-->



</body></html>