<script type="text/javascript">
  $(document).ready(function()
  {

      $('.select2').select2({
           theme: "bootstrap",
           placeholder: "Seleccione division",
          allowClear: true
      }); 
  }); 
</script>
<div class="wrapper wrapper-content animated fadeInRight" id="">
    <div class="row">
        <div class="col-lg-12 " id="usuarios_borrar">
            <div class="ibox collapsed float-e-margins">
                <div class="ibox-title collapse-link" id="tituloaltaevento">
                    <h5>Asignacion de Materias a Profesores </h5>
                        <div class="ibox-tools">
                            <a class="">
                                  
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display:block">
                        <div class="row row2">
                            <div class="col-sm-12 b-r" id="">
                                <div class="tabs-container">
                                    <ul class="nav nav-tabs" id="colegios_dir_prec">
                                      <?php
                                        $active_class = "active";
                                        foreach ($acciones as $accion) 
                                        {
                                          $href = base_url()."/Materias/docentes_division#tab_dir-".$accion['idcole'];
                                        
                                          ?>
                                            <li class="<?php echo $active_class;?>"><a data-toggle="tab" href="<?php echo $href?>"><?php echo $accion['namecole'] ; ?></a></li>
                                          <?
                                          $active_class = '';
                                          //print_r($accion);
                                        }
                                      ?>
                                      
                                    </ul>
                                    <div class="tab-content" id="contenido_tab_dir_prec">
                                       <?php
                                        $active_class = "active";
                                        foreach ($acciones as $accion) 
                                        {
                                          
                                          $colegio_id = $accion['idcole'];
                                          
                                          ?>
                                            <div id="tab_dir-<?php echo $colegio_id;?>" class="tab-pane <?php echo $active_class;?>">
                                                <div class="panel-body table-responsive">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <form id="form_dir<?php echo $colegio_id;?>" autocomplete="on" method="post" class="form-horizontal form-label-left">
                                                                <div class="form-group ">
                                                                    <label>Seleccione una Divisi&oacute;n: </label>
                                                                </div>
                                                                <div class="form-group ">
                                                                    <select name="divisiones" id="dir_prec_<?php echo $colegio_id;?>" class="select2 form-control">
                                                                      <?php 
                                                                      foreach (json_decode( $accion['divisiones']) as $division)
                                                                      {
                                                                      //  print_r($division);
                                                                        $option_id = $division->division_id.'_'.$division->nodoanios_id.'_'.$colegio_id;
                                                                        $nombre_nivel = $division->nombre_nivel." ".$division->nombre_anio." ".$division->nombre_division;
                                                                        $value = $division->nodoanios_id."_".$colegio_id;
                                                                        ?>
                                                                          <option value="<?php echo $value; ?>"><?php echo $nombre_nivel; ?></option>
                                                                        <?
                                                                      }
                                                                      ?>
                                                                          
                                                                    </select>
                                                                </div>
                                                                <? 
                                                                  //print_r($accion['divisiones']);

                                                                      foreach (json_decode( $accion['divisiones']) as $division)
                                                                      {
                                                                       // print_r($division);break;
                                                                      }
                                                                      
                                                                ?>
                                                            </form>
                                                        </div>
                                                        <div class="col-sm-6" style="margin-top:35px">
                                                          <button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="btn_alumnos" onclick="get_materias_x_anio(<?php echo $colegio_id;?>)" type="button"><strong>Listar Materias</strong></button>
                                                          <div type="hidden" id="loading_horarios_dir_esc_<?php echo $colegio_id;?>" style="margin-top:-5px">
                                                          </div>
                                                        </div>
                                                    </div>
                                                    <table class="table table-striped table-bordered dt-responsive nowrap" id="tabladatos_<?=$colegio_id?>">
                                                        <thead>
                                                          <th>#</th>
                                                          <th>Nombre Materia</th>
                                                          <th></th>
                                                        </thead>
                                                    <tbody id="tabladatos_body_<?=$colegio_id?>"></tbody>
                                              </table>
                                                </div>
                                            </div>
                                          <?
                                          $active_class = '';
                                          //print_r($accion);
                                        }
                                      ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
            </div>
        </div>
    </div>
<script type="text/javascript">
  function get_materias_x_anio(id_colegio){

        var aux = $('#dir_prec_'+id_colegio).val();
        var arre = Array();
        arre=aux.split('_');
        var idanio = arre[0]; 
        //var idanio = arre[1]; 
        //alert(idanio);
           
        $("#tabladatos_"+arre[1]).dataTable().fnDestroy();
        $("#tabladatos_body_"+arre[1]).empty();
        
        var url = '<?=base_url("/index.php/materias/get_materias_x_anio_2_ajax")?>';   
        
        //console.log(url);
        $.ajax({
          url: url,
          type: 'GET',
          
          data:{idcolegio: arre[1], nodoanioid: arre[0] },
          success: function(data)
          { 
            var data = jQuery.parseJSON( data );
            if( data['status']===0){
              var string = "<p class='text-danger'>No hay materias asignadas</p>";
              //console.log(string);
              $("#tabladatos_body_"+arre[1]).html(string);

              tableMatAsig=null;
            }
            else
            {
              //var data = jQuery.parseJSON( data );

              var cont = 1 ;
    
              data.forEach(function(currentValue,index){
                //console.log(currentValue);
                var id = currentValue.id;
                var nombre = currentValue.nombre;
                var idmat = currentValue.idmateria;

            
        

                var fila = '<tr id="materia_'+id+'_'+idmat+'">'+
                              '<td>'+cont+'</td>'+'<td>'+nombre+'</td>'+

                              '<td><div align="center"><button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="" title="borrar materia" onclick="delete_aniomateria(this,'+id+','+id_colegio+')" type="button"><i class="glyphicon glyphicon-trash"></i></button></div></td>'+

                          '</tr>';

                cont++;

                $("#tabladatos_body_"+arre[1]).append(fila);

              });
              generar_datatable2(arre[1]);
            }
          },
          error: function(response){
            console.log(response);
              var string = "No hay alumnos asignados";
              $(".alumnos_tutores").html(string);         
          }
        });
  } 
</script>
