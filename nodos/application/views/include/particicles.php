<!-- INICIO PARTICULAS -->
<div id="particles-js"></div>
<!-- scripts -->
<script src="<?=base_url('assets/js/particles/particles.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/js/particles/app.js')?>" type="text/javascript"></script>

<!-- stats.js -->
<script src="<?=base_url('assets/js/particles/stats.js')?>" type="text/javascript"></script>

<script>
var count_particles, stats, update;
stats = new Stats;
stats.setMode(0);
stats.domElement.style.position = 'absolute';
stats.domElement.style.left = '0px';
stats.domElement.style.top = '0px';
document.body.appendChild(stats.domElement);
count_particles = document.querySelector('.js-count-particles');
if (count_particles!== null){
	update = function() {
		stats.begin();
		stats.end();
		if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
			count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
		}
		requestAnimationFrame(update);
	};
requestAnimationFrame(update);
} 
</script>
<!-- FIN PARTICULAS -->