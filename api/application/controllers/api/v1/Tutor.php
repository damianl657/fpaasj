<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';

    class Tutor extends REST_Controller {

    function __construct()
    {        
        parent::__construct();
        $this->load->model("modelo_tutor");

        // Configurar limites para cada uno de los metodos, no solo controlador
        // La tabla limits tiene que estar creada y la opcion limits TRUE en application/config/rest.php
        
        //$this->methods['tutores_get']['limit'] = 1; // 500 peticiones por hora por usuario/key
        //$this->methods['user_post']['limit'] = 100; // 100 peticiones por hora por usuario/key
        //$this->methods['user_delete']['limit'] = 50; // 50 peticiones por hora por usuario/key
    }

    /*public function obtener_get()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO_LOGUEADO'
            ], REST_Controller::HTTP_BAD_REQUEST);            
        }
        else
        {
            // Hago lo que debe hacer el metodo especificamente

            $idtutor = $this->get('id'); // obtengo el id del evento, si es que vino en la url
            if ($idtutor === NULL)
            {
                // Obtengo todos los tutores
                $tutores = $this->modelo_tutor->obtener_tutores($userid); // obtiene los tutores de el usuario X
                // Si existe mas de un resultado, lo mando
                if ($tutores)
                {
                    $this->response($tutores, REST_Controller::HTTP_OK);
                }
                // Sino, envio respuesta con 404
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'No se encontraron tutores'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                }
            }
            else
            {
                // Obtengo un tutor especifico
                $tutor = $this->modelo_tutor->obtener_tutor($idtutor); // obtiene el tutor especifico
                // Si existe un resultado, lo mando
                if ($tutor)
                {
                    $this->response($tutor, REST_Controller::HTTP_OK);
                }
                // Sino, envio respuesta con 404
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'No se encontro el tutor con ese ID'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                }
            }
        }
    }*/


    public function obtener_hijos_get()
    {
        $userid = $this->utilidades->verifica_userid();
    
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {   
            //$cicloA = $this->utilidades->obtener_cicloaXcolegio( $colegio_id );
            $alumnos = $this->modelo_tutor->obtener_alumnos_del_tutor($userid); // 
            
            // Si existe mas de un resultado, lo mando
            if ($alumnos)
            {
                $alumnos = json_encode($alumnos);
               
                $this->response([
                    'status' => TRUE,
                    'alumnos' => $alumnos,
                ], REST_Controller::HTTP_OK); 

            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No Tiene Hijos Asignados a su Cargo',
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

    }

}
