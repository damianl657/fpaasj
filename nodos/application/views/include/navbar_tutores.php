<!-- Barra de Navegación -->		
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
		
		<div id="menu-icon-wrapper" class="menu-icon-wrapper scaled visible-xs" style="visibility: hidden">
			<svg width="1000px" height="1000px">
				<path id="pathA" d="M 300 400 L 700 400 C 900 400 900 750 600 850 A 400 400 0 0 1 200 200 L 800 800"></path>
				<path id="pathB" d="M 300 500 L 700 500"></path>
				<path id="pathC" d="M 700 600 L 300 600 C 100 600 100 200 400 150 A 400 380 0 1 1 200 800 L 800 200"></path>
			</svg>
			<button id="menu-icon-trigger" class="menu-icon-trigger" data-toggle="collapse" data-target="#nodos-collapse"></button>
		</div>
		<!-- menu-icon-wrapper -->

		<a class="navbar-brand" href="<?=base_url('Login/home/');?>" title="Comunicaci&oacute;n escolar m&aacute;s simple"><img src="<?=base_url('assets/images/nodos-logo-white.png')?>" width="70" alt="Nodos App Logo"/></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="nodos-collapse">
      <ul class="nav navbar-nav navbar-left">
        <?php //var_dump($this->session->userdata('colegios'));
        //die();
                foreach ($this->session->userdata('menu') as $menu) 
                {
                    $encontrado = false;
                    if($menu->tipo == 'menu')
                    {   
                        //echo $menu->nombre;
                        foreach ($this->session->userdata('colegios') as $colegio) 
                        {
                            //print_r($colegio['menu']);
                            foreach ($colegio['menu'] as $menu_col) 
                            {
                              //  print_r($menu_col->nombre);
                                if($menu_col->tipo == 'menu' and $menu_col->nombre == $menu->nombre)
                                {
                                    $encontrado = true;
                                }
                            }
                        }

                        if($encontrado == true)
                        {


                        ?>

                            <li>
                                <a href="<?php $link = "index.php/".$menu->link; echo base_url($link); ?> "><i class="<?php echo $menu->icono; ?>"></i><span class="nav-label"><?php echo $menu->nombre?></span></a>
                            </li>
                        <?php 
                        }
                    }
                    //print_r($menu);
                }
                ?>
        </ul>
        <ul class="nav navbar-nav navbar-right">           
                <li style="padding: 0px 20px;" title="editar mi perfil"><a href="<?php echo base_url('index.php/Usuario/perfil_user'); ?>" ><?php echo $this->session->userdata('nombreusuario');?></a>
                        <!--<ul class="dropdown-menu animated fadeInRight m-t-xs" style="display:block">           
                            <li><a href="http://localhost/nodosweb/index.php/Usuario/perfil_user">Mis Datos</a></li>
                      </ul>-->
                </li>

                <!--<li><a href="<?php //echo base_url('index.php/Usuario/perfil_user'); ?>"><i class="fa fa-reorder"></i></a></li>-->

        <li><a href="<?php echo base_url('index.php/Login/logout'); ?>"><i class="fa fa-power-off"></i></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!-- Fin Barra de Navegación -->