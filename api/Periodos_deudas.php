<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
set_time_limit(1200);
//ini_set('memory_limit', '256M');
ini_set('memory_limit', '-1');
use Illuminate\Database\Capsule\Manager as Capsule;
use \Illuminate\Container\Container as Container;
use \Illuminate\Support\Facades\Facade as Facade;



class Periodos_deudas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Groups_eloquent');
		$this->load->model('Clientes_eloquent');
		$this->load->model('Users_groups_eloquent');
		$this->load->model('Users_eloquent');
		$this->load->model('Lugares_trabajo_clientes_eloquent');
		$this->load->model('Provincias_eloquent');
		$this->load->model('Tarjetas_eloquent');
		$this->load->model('Empresas_eloquent');
        $this->load->model('Bancos_eloquent');
        $this->load->model('Tipos_cuentas_bancarias_eloquent');
        $this->load->model('Cuentas_bancarias_clientes_eloquent');
        $this->load->model('Tarjeta_credito_cliente_eloquent');
        $this->load->model('Tipos_tarjetas_eloquent');
        $this->load->model('Periodos_deudas_eloquent');
        $this->load->model('Items_periodo_deuda_eloquent');
        $this->load->model('Items_periodo_deuda_detalle_eloquent');
        $this->load->model('Productos_solicitud_eloquent');
        $this->load->model('Solicitudes_eloquent');
        $this->load->model('Anexos_eloquent');
        $this->load->model('Items_cuenta_corriente_eloquent');
        $this->load->model('Items_periodo_pago_parcial_eloquent');
        $this->load->model('Cuentas_corrientes_eloquent');
        $this->load->model('Productos_eloquent');
        $this->load->model('Clientes_admin_publica_eloquent');
        $this->load->model('Model_periodos_deudas');
        $this->load->model('Model_clientes');
        $this->load->helper('file');
        //$this->load->library('Procesar_pagos_libreria');
        
		//$this->load->library(array('ion_auth','funciones'));
		
	}
	
	public function index()
	{
		$aux['controlador'] = 'Periodos_deudas';
		$aux['metodo'] = 'index';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        	
        	$data['contenido'] = "/cobros/periodos_deudas/index";
			$this->load->view('templates/template_admin', $data);
        }
        else
        {
        	die();
        }
	}
	public function listar_periodos_deudas()
    {
        $empresa_id = $this->session->userdata('id_empresa');
        $data['listado'] = Periodos_deudas_eloquent::where('id_empresa','=',$empresa_id)->activos()->get();
                                        
        $this->load->view('cobros/periodos_deudas/listar_periodos_deudas', $data);
        //echo json_encode(array("status" => TRUE, "clientes" => $this->load->view('clientes/listar_clientes', $data)));
    }
    public function nueva_deuda()
	{
		$aux['controlador'] = 'Periodos_deudas';
		$aux['metodo'] = 'nueva_deuda';
		$permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
        		
			
			$data['contenido'] = "/cobros/periodos_deudas/nueva_deuda";
            $data['tarjetas_de_credito'] = Tarjetas_eloquent::activos()->get();
            $data['bancos'] = Bancos_eloquent::activos()->get();
            $data['tipos_cuentas_bancarias'] = Tipos_cuentas_bancarias_eloquent::activos()->get();
            $data['tipos_tarjetas'] = Tipos_tarjetas_eloquent::activos()->get();
			//print_r($data['bancos']);
			//die();
			$this->load->view('templates/template_admin', $data);
        }
        else
        {
        	echo"error permiso";
        	die();
        }
	}
    
    public function nueva_deuda_admin_publica()
    {
        $aux['controlador'] = 'Periodos_deudas';
        $aux['metodo'] = 'nueva_deuda_admin_publica';
        $permisos_string = $this->session->userdata('permisos');
        $permisos = explode(",", $permisos_string);
        $aux['pemisos_user'] = $permisos;
        $obtener_permiso = $this->funciones->validar_permiso($aux);
        if($obtener_permiso)
        {
                
            
            $data['contenido'] = "/cobros/periodos_deudas/nueva_deuda_admin_publica";
            
           
            $this->load->view('templates/template_admin', $data);
        }
        else
        {
            echo"error permiso";
            die();
        }
    }
    public function cobrar_deuda_manual()
    {
        $id_cliente = $this->input->post('id_cliente');
        $pago_total_recibido = $this->input->post('importe_recibido');
        //formatear fecha
        $fecha_cobro = $this->input->post('fecha_cobro');
                    //Procesamiento de fecha y hora
                    if($fecha_cobro != '')
                    {

                      $fecha_cobro = str_replace('/', '-', $fecha_cobro);
                      $separa = explode("-",$fecha_cobro);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha_cobro = $anio.'-'.$mes.'-'.$dia;
                    }
       
        //
        $cliente = Clientes_eloquent::find($id_cliente);
        $id_items_cuenta_corriente = 0;
        foreach ($cliente->cuentas_corrientes->items_cuenta_corriente as $key) 
        {
            if($key->estado == 1)
            {

                  $id_items_cuenta_corriente = $key->id;
            }
        }       
        if($id_items_cuenta_corriente > 0)
        {
            $items_cuenta_corriente = Items_cuenta_corriente_eloquent::find($id_items_cuenta_corriente);
            $items_periodo_deuda = Items_periodo_deuda_eloquent::find($items_cuenta_corriente->id_items_periodo_deuda);
            $periodo_deuda = Periodos_deudas_eloquent::find($items_periodo_deuda->id_periodo_deuda);

            //crear el pago parcial
            $items_periodo_pago_parcial = new Items_periodo_pago_parcial_eloquent();
            $items_periodo_pago_parcial->id_items_periodo_deuda = $items_periodo_deuda->id;
            $items_periodo_pago_parcial->id_periodo_deuda =$periodo_deuda->id;
            $items_periodo_pago_parcial->id_cliente = $cliente->id;
             $items_periodo_pago_parcial->fecha = $fecha_cobro;
            $items_periodo_pago_parcial->estado = 1;
            $items_periodo_pago_parcial->imp_mov = $items_periodo_deuda->importe_total;
            $items_periodo_pago_parcial->imp_aplic =$pago_total_recibido;
            if(($items_periodo_deuda->importe_total - $pago_total_recibido) < 0)
            {
                 $items_periodo_pago_parcial->imp_recha = 0;
            }
            else
            {
                $items_periodo_pago_parcial->imp_recha =$items_periodo_deuda->importe_total - $pago_total_recibido;
            }

            if($this->input->post('motivo_pago_manual') == '')
            {
                $items_periodo_pago_parcial->tipo_pago="MANUAL/EFECTIVO";
            }
            else
            {
                $items_periodo_pago_parcial->tipo_pago= $this->input->post('motivo_pago_manual');
            }
            
            $items_periodo_pago_parcial->save();
            //
            //$items_periodo_deuda actualizar variables
                                    if($items_periodo_deuda->importe_total_recibido == NULL)
                                    {
                                        $items_periodo_deuda->importe_total_recibido = 0;
                                    }
                                    $items_periodo_deuda->importe_total_recibido = $items_periodo_deuda->importe_total_recibido + $items_periodo_pago_parcial->imp_aplic;
                                    $items_periodo_deuda->save();

                                    //items_cuenta_corriente actualizar variables
                                    //$items_cuenta_corriente = Items_cuenta_corriente_eloquent::where('id_cliente',$cliente->id)->where('id_items_periodo_deuda',$items_periodo_deuda->id)->first();
                                    if($items_cuenta_corriente->importe_recibo == NULL)
                                    {
                                        $items_cuenta_corriente->importe_recibo = 0;
                                    }
                                    $items_cuenta_corriente->importe_recibo = $items_cuenta_corriente->importe_recibo + $items_periodo_pago_parcial->imp_aplic;
                                    $items_cuenta_corriente->save();

                                    //actualizar cuenta corriente

                                    $cliente->cuentas_corrientes->saldo = $cliente->cuentas_corrientes->saldo -$items_periodo_pago_parcial->imp_aplic;
                                    $cliente->cuentas_corrientes->save();


        }
        echo json_encode(array(
            "status" => TRUE,
            "id_cliente" => $id_cliente,
            "importe_recibido" => $pago_total_recibido,
            "id_items_cuenta_corriente" => $id_items_cuenta_corriente
            ));
    }
	public function guardar_periodo_deuda()
	{
		//echo "ok";
        $cont_registros = 0;
		$periodo_deuda = new Periodos_deudas_eloquent();
        $periodo_deuda->tope_deuda = $this->input->post('tope_deuda');
        $periodo = $this->input->post('periodo');
        
                    //Procesamiento de fecha y hora
                    if($periodo != '')
                    {

                      //$periodo = str_replace('/', '-', $periodo);
                      $separa = explode("-",$periodo);
                      $mes = $separa[0];
                      //$dia = $separa[0];
                      $anio = $separa[1];
                      
                      $periodo = $mes.'-'.$anio;
                    }
       
        $periodo_deuda->periodo = $periodo;
        
        $periodo_date = date_create_from_format('Y-m-d', $anio.'-'.$mes.'-01');

        $tope_mes = $this->input->post('tope_mes');
        $periodo_deuda->tope_mes = $tope_mes;
        /*echo "periodo_Deuda: ";
        print_r($periodo_date);
        echo "fecha de inicio:";*/

        $fecha_inicio = $this->input->post('fecha_inicio');
                    //Procesamiento de fecha y hora
                    if($fecha_inicio != '')
                    {

                      $fecha_inicio = str_replace('/', '-', $fecha_inicio);
                      $separa = explode("-",$fecha_inicio);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha_inicio = $anio.'-'.$mes.'-'.$dia;
                    }
        $periodo_deuda->fecha_inicio = $fecha_inicio;
       
        /*$fecha_inicio = date_create_from_format('Y-m-d', $fecha_inicio);
         print_r($fecha_inicio);
        if($periodo_date > $fecha_inicio)
            {echo " es mas grande periodo date";}
        else
            {echo "es mas grande fecha inicio";}
        die();*/

        $fecha_fin = $this->input->post('fecha_fin');
                    //Procesamiento de fecha y hora
                    if($fecha_fin != '')
                    {

                      $fecha_fin = str_replace('/', '-', $fecha_fin);
                      $separa = explode("-",$fecha_fin);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha_fin = $anio.'-'.$mes.'-'.$dia;
                    }
        $periodo_deuda->fecha_fin = $fecha_fin;

        $fecha_envio = $this->input->post('fecha_envio');
                    //Procesamiento de fecha y hora
                    if($fecha_envio != '')
                    {

                      $fecha_envio = str_replace('/', '-', $fecha_envio);
                      $separa = explode("-",$fecha_envio);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha_envio = $anio.'-'.$mes.'-'.$dia;
                    }
        $periodo_deuda->fecha_envio = $fecha_envio;
        $periodo_deuda->id_empresa = $this->session->userdata('id_empresa');

        $periodo_deuda->estado = 1;

        $tipo_cobro = $this->input->post('tipo_cobro');
        //$clientes = Clientes_eloquent::activos()->empresa($this->session->userdata('id_empresa'))->get();
        if($tipo_cobro == 'banco')
        {
            $id_banco = $this->input->post('id_banco');
            //$id_tipo_cuenta_bancaria = $this->input->post('id_tipo_cuenta_bancaria');

            $periodo_deuda->id_banco = $id_banco;
            //$periodo_deuda->id_tipo_cuenta_bancaria = $id_tipo_cuenta_bancaria;
            $periodo_deuda->id_tarjeta = 0;
            $periodo_deuda->id_tipo_tarjeta = 0;
            $periodo_deuda->saldo_anterior = 0;

            $periodo_deuda->save();
            $clientes_banco = Cuentas_bancarias_clientes_eloquent::activos()
                                                                  ->where('banco_id','=',$id_banco)->get();
            foreach ($clientes_banco as $cliente_banco) 
            {
                $cliente = Clientes_eloquent::empresa($this->session->userdata('id_empresa'))
                                            ->where('id_user_group','=',$cliente_banco->id_user_group)
                                            ->first();
                if($cliente)
                {
                    if(($cliente->cuenta_bancaria_id != NULL )&& ($cliente->id_cliente_admin_publica == NULL))
                    {
                        //if(($cliente->cuentas_bancarias_clientes->banco_id == $id_banco) && ($cliente->cuentas_bancarias_clientes->tipo_cuenta_bancaria_id == $id_tipo_cuenta_bancaria))
                        if(($cliente->cuentas_bancarias_clientes->banco_id == $id_banco))
                        {
                            /*if(($cliente->solicitudes->estado == 1) && ($cliente->solicitudes->fecha_vigencia <= $periodo_date))
                            {
                                $acum_importes = 0;
                                $items_periodo_deuda = new Items_periodo_deuda_eloquent();
                                $items_periodo_deuda->id_periodo_deuda = $periodo_deuda->id;
                                $items_periodo_deuda->id_cliente = $cliente->id;
                                $items_periodo_deuda->estado = 1;
                                $items_periodo_deuda->save();
                                $cont_registros++;//contador de registros para el txt
                                if($periodo_deuda->tope_deuda > 0)//sirve para el contador de registros del txt si tiene deuda tendria 2 registros el mismo socio
                                {
                                    if($cliente->cuentas_corrientes->saldo > 0)
                                    {
                                        $cont_registros++;
                                    }
                                }
                                //crear items_detalle_periodo para asiganr el producto de la solicitud 

                                $items_periodo_deuda_detalle = new Items_periodo_deuda_detalle_eloquent();
                                $items_periodo_deuda_detalle->id_items_periodo_deuda = $items_periodo_deuda->id;
                                $items_periodo_deuda_detalle->id_solicitud = $cliente->solicitudes->id;
                                $items_periodo_deuda_detalle->importe = $cliente->solicitudes->importe;
                                $items_periodo_deuda_detalle->estado = 1;

                                $acum_importes = $acum_importes + $cliente->solicitudes->importe;
                                //buscar el producto contratados en la solicitud
                                $producto_solicitud = Productos_solicitud_eloquent::where('id_solicitud', $cliente->solicitudes->id)->where('id_anexo',0)->first();
                                $items_periodo_deuda_detalle->id_producto = $producto_solicitud->id_producto;
                                $items_periodo_deuda_detalle->save();



                                //obtener anexos
                                foreach ($cliente->solicitudes->anexos as $anexo) 
                                {
                                    if(($anexo->estado == 1) && ($anexo->fecha_vigencia <= $periodo_date))
                                    {
                                        $items_periodo_deuda_detalle = new Items_periodo_deuda_detalle_eloquent();
                                        $items_periodo_deuda_detalle->id_items_periodo_deuda = $items_periodo_deuda->id;
                                        $items_periodo_deuda_detalle->id_solicitud = $cliente->solicitudes->id;
                                        $items_periodo_deuda_detalle->id_anexo = $anexo->id;
                                        $items_periodo_deuda_detalle->importe = $anexo->importe;
                                        $items_periodo_deuda_detalle->estado = 1;
                                        //buscar el producto contratados en la solicitud
                                        $producto_solicitud = Productos_solicitud_eloquent::where('id_solicitud', $cliente->solicitudes->id)->where('id_anexo',$anexo->id)->first();
                                        $items_periodo_deuda_detalle->id_producto = $producto_solicitud->id_producto;
                                        $items_periodo_deuda_detalle->save();

                                        $acum_importes = $acum_importes +$anexo->importe;
                                    }    
                                        
                                }
                                
                                $items_periodo_deuda->deuda = $cliente->cuentas_corrientes->saldo;
                                $items_periodo_deuda->importe_total = $acum_importes;
                                $items_periodo_deuda->save();

                                //items cuenta corriente
                                $items_cuenta_corriente = new Items_cuenta_corriente_eloquent();
                                $items_cuenta_corriente->id_cuenta_corriente = $cliente->cuentas_corrientes->id;
                                $items_cuenta_corriente->id_cliente = $cliente->id;
                                $items_cuenta_corriente->importe = $acum_importes;
                                $items_cuenta_corriente->periodo = $this->input->post('periodo');
                                $items_cuenta_corriente->id_items_periodo_deuda = $items_periodo_deuda->id;
                                $items_cuenta_corriente->importe_recibo = 0;
                                $items_cuenta_corriente->deuda_recibida = 0;
                                $items_cuenta_corriente->estado = 1;
                                $items_cuenta_corriente->save();

                                 //actualizar saldo cuenta corriente
                                $saldo_anterior = $cliente->cuentas_corrientes->saldo;
                                $periodo_deuda->saldo_anterior = $saldo_anterior;
                                $cliente->cuentas_corrientes->saldo = $acum_importes + $saldo_anterior;
                                $cliente->cuentas_corrientes->save();

                                //agrego el salgo al items cuenta corriente
                                $items_cuenta_corriente->saldo = $cliente->cuentas_corrientes->saldo;
                                $items_cuenta_corriente->save();
                                $periodo_deuda->save();
                            }*/
                            //else//pueden ver anexos activos
                            //{
                                //recorrer anexos para saber cual esta activo
                                $contador_anexos_activos = 0;
                                foreach ($cliente->solicitudes->anexos as $anexo) 
                                {
                                    $f_anex_vig_aux = date_create_from_format('Y-m-d', $anexo->fecha_vigencia);
                                    $f_anex_vig_aux = date_format($f_anex_vig_aux,"Y-m-d");
                                    $periodo_date_aux = date_format($periodo_date,"Y-m-d");
                                    if(($anexo->estado == 1) && ($f_anex_vig_aux <= $periodo_date_aux))
                                    {
                                        $contador_anexos_activos++;
                                    }
                                    else if($anexo->estado == 0) 
                                    {
                                        if($anexo->fecha_baja != NULL)
                                        {
                                            $f_anex_baja_aux = date_create_from_format('Y-m-d', $anexo->fecha_baja);
                                            $f_anex_baja_aux = date_format($f_anex_baja_aux,"Y-m-d");
                                            $periodo_date_aux = date_format($periodo_date,"Y-m-d");
                                                
                                            if(($f_anex_baja_aux > $periodo_date_aux)&&($f_anex_vig_aux != $f_anex_baja_aux))
                                            {
                                                $contador_anexos_activos++;
                                            }
                                        }
                                    }
                                }
                                if($contador_anexos_activos > 0)//hay al menos 1 anexo activo
                                {
                                    $cont_registros++;
                                    $acum_importes = 0;
                                    if($periodo_deuda->tope_deuda > 0)//sirve para el contador de registros del txt si tiene deuda tendria 2 registros el mismo socio
                                    {
                                        if($cliente->cuentas_corrientes->saldo > 0)
                                        {
                                            $cont_registros++;
                                        }
                                    }
                                    $items_periodo_deuda = new Items_periodo_deuda_eloquent();
                                    $items_periodo_deuda->id_periodo_deuda = $periodo_deuda->id;
                                    $items_periodo_deuda->id_cliente = $cliente->id;
                                    $items_periodo_deuda->estado = 1;
                                    $items_periodo_deuda->save();
                                    //recorrer anexos
                                    //obtener anexos
                                    foreach ($cliente->solicitudes->anexos as $anexo) 
                                    {
                                        $f_anex_vig_aux = date_create_from_format('Y-m-d', $anexo->fecha_vigencia);
                                        $f_anex_vig_aux = date_format($f_anex_vig_aux,"Y-m-d");
                                        $periodo_date_aux = date_format($periodo_date,"Y-m-d");
                                        if(($anexo->estado == 1) && ($f_anex_vig_aux <= $periodo_date_aux))
                                            
                                        {
                                            $items_periodo_deuda_detalle = new Items_periodo_deuda_detalle_eloquent();
                                            $items_periodo_deuda_detalle->id_items_periodo_deuda = $items_periodo_deuda->id;
                                            $items_periodo_deuda_detalle->id_solicitud = $cliente->solicitudes->id;
                                            $items_periodo_deuda_detalle->id_anexo = $anexo->id;
                                            $items_periodo_deuda_detalle->importe = $anexo->importe;
                                            $items_periodo_deuda_detalle->estado = 1;
                                            //buscar el producto contratados en la solicitud
                                            $producto_solicitud = Productos_solicitud_eloquent::where('id_solicitud', $cliente->solicitudes->id)->where('id_anexo',$anexo->id)->first();
                                            $items_periodo_deuda_detalle->id_producto = $producto_solicitud->id_producto;
                                            $items_periodo_deuda_detalle->save();

                                            $acum_importes = $acum_importes +$anexo->importe;
                                        }
                                        else if($anexo->estado == 0) 
                                        {
                                            if($anexo->fecha_baja != NULL)
                                            {   
                                                $f_anex_baja_aux = date_create_from_format('Y-m-d', $anexo->fecha_baja);
                                                $f_anex_baja_aux = date_format($f_anex_baja_aux,"Y-m-d");
                                                $periodo_date_aux = date_format($periodo_date,"Y-m-d");
                                                /*if($anexo->id == 81277)
                                                    {
                                                        echo "periodo baja: ";
                                                        print_r($f_anex_baja_aux);
                                                        echo "periodo cobrar: ";
                                                        print_r($periodo_date_aux);
                                                        die();
                                                    }*/
                                                if(($f_anex_baja_aux > $periodo_date_aux)&&($f_anex_vig_aux != $f_anex_baja_aux))
                                                {
                                                    
                                                    $items_periodo_deuda_detalle = new Items_periodo_deuda_detalle_eloquent();
                                                    $items_periodo_deuda_detalle->id_items_periodo_deuda = $items_periodo_deuda->id;
                                                    $items_periodo_deuda_detalle->id_solicitud = $cliente->solicitudes->id;
                                                    $items_periodo_deuda_detalle->id_anexo = $anexo->id;
                                                    $items_periodo_deuda_detalle->importe = $anexo->importe;
                                                    $items_periodo_deuda_detalle->estado = 1;
                                                    //buscar el producto contratados en la solicitud
                                                    $producto_solicitud = Productos_solicitud_eloquent::where('id_solicitud', $cliente->solicitudes->id)->where('id_anexo',$anexo->id)->first();
                                                    $items_periodo_deuda_detalle->id_producto = $producto_solicitud->id_producto;
                                                    $items_periodo_deuda_detalle->save();

                                                    $acum_importes = $acum_importes +$anexo->importe;
                                                }
                                            }
                                        }    
                                            
                                    }
                                    $items_periodo_deuda->deuda = $cliente->cuentas_corrientes->saldo;
                                    $items_periodo_deuda->importe_total = $acum_importes;
                                    $items_periodo_deuda->save();

                                    //items cuenta corriente
                                    $items_cuenta_corriente = new Items_cuenta_corriente_eloquent();
                                    $items_cuenta_corriente->id_cuenta_corriente = $cliente->cuentas_corrientes->id;
                                    $items_cuenta_corriente->id_cliente = $cliente->id;
                                    $items_cuenta_corriente->importe = $acum_importes;
                                    $items_cuenta_corriente->periodo = $this->input->post('periodo');
                                    $items_cuenta_corriente->id_items_periodo_deuda = $items_periodo_deuda->id;
                                    $items_cuenta_corriente->importe_recibo = 0;
                                    $items_cuenta_corriente->deuda_recibida = 0;
                                    $items_cuenta_corriente->estado = 1;
                                    $items_cuenta_corriente->save();

                                     //actualizar saldo cuenta corriente
                                    $saldo_anterior = $cliente->cuentas_corrientes->saldo;
                                    $periodo_deuda->saldo_anterior = $saldo_anterior;
                                    $cliente->cuentas_corrientes->saldo = $acum_importes + $saldo_anterior;
                                    $cliente->cuentas_corrientes->save();

                                    //agrego el salgo al items cuenta corriente
                                    $items_cuenta_corriente->saldo = $cliente->cuentas_corrientes->saldo;
                                    $items_cuenta_corriente->save();
                                    $periodo_deuda->registros_txt = $cont_registros;
                                    $periodo_deuda->save();

                                    //

                                }
                            //}*/
                        }
                    }
                }
                    

            }
            //print_r($tipo_cobro);
            //$this->generador_txt_bancos($periodo_deuda->id);
        }
        else if($tipo_cobro == 'tarjeta')
        {
            $id_tarjeta_de_credito = $this->input->post('id_tarjeta_de_credito');
            $id_tipo_tarjeta = $this->input->post('id_tipo_tarjeta');

            $periodo_deuda->id_tarjeta = $id_tarjeta_de_credito;
            $periodo_deuda->id_tipo_tarjeta = $id_tipo_tarjeta;
            $periodo_deuda->id_banco = 0;
            $periodo_deuda->id_tipo_cuenta_bancaria = 0;
            $periodo_deuda->saldo_anterior = 0;
            $periodo_deuda->save();
            //print_r($tipo_cobro);
            $clientes_tarjeta = Tarjeta_credito_cliente_eloquent::activos()
                                                                ->where('id_tarjeta_credito','=',$id_tarjeta_de_credito)
                                                                ->where('id_tipo_tarjeta','=',$id_tipo_tarjeta)
                                                                ->get();
            foreach ($clientes_tarjeta as $cliente_tarjeta) 
            {
                $cliente = Clientes_eloquent::empresa($this->session->userdata('id_empresa'))
                                            ->where('id_user_group','=',$cliente_tarjeta->id_user_group)
                                            ->first();
                if($cliente)
                {
                    if(($cliente->tarjeta_credito_id != NULL) && ($cliente->id_cliente_admin_publica == NULL))
                    {
                        if(($cliente->tarjeta_credito_cliente->id_tarjeta_credito == $id_tarjeta_de_credito ) && ($cliente->tarjeta_credito_cliente->id_tipo_tarjeta == $id_tipo_tarjeta))
                        {
                            /*if(($cliente->solicitudes->estado == 1) && ($cliente->solicitudes->fecha_vigencia <= $periodo_date)) 
                            {  
                                $acum_importes = 0;
                                $items_periodo_deuda = new Items_periodo_deuda_eloquent();
                                $items_periodo_deuda->id_periodo_deuda = $periodo_deuda->id;
                                $items_periodo_deuda->id_cliente = $cliente->id;
                                $items_periodo_deuda->estado = 1;
                                $items_periodo_deuda->save();
                                //crear items_detalle_periodo para asiganr el producto de la solicitud 

                                $items_periodo_deuda_detalle = new Items_periodo_deuda_detalle_eloquent();
                                $items_periodo_deuda_detalle->id_items_periodo_deuda = $items_periodo_deuda->id;
                                $items_periodo_deuda_detalle->id_solicitud = $cliente->solicitudes->id;
                                $items_periodo_deuda_detalle->importe = $cliente->solicitudes->importe;
                                $items_periodo_deuda_detalle->estado = 1;

                                $acum_importes = $acum_importes + $cliente->solicitudes->importe;
                                //buscar el producto contratados en la solicitud
                                $producto_solicitud = Productos_solicitud_eloquent::where('id_solicitud', $cliente->solicitudes->id)->where('id_anexo',0)->first();
                                $items_periodo_deuda_detalle->id_producto = $producto_solicitud->id_producto;
                                $items_periodo_deuda_detalle->save();
                                //obtener anexos
                                foreach ($cliente->solicitudes->anexos as $anexo) 
                                {
                                    if(($anexo->estado == 1) && ($anexo->fecha_vigencia <= $periodo_date))
                                    {    
                                        $items_periodo_deuda_detalle = new Items_periodo_deuda_detalle_eloquent();
                                        $items_periodo_deuda_detalle->id_items_periodo_deuda = $items_periodo_deuda->id;
                                        $items_periodo_deuda_detalle->id_solicitud = $cliente->solicitudes->id;
                                        $items_periodo_deuda_detalle->id_anexo = $anexo->id;
                                        $items_periodo_deuda_detalle->importe = $anexo->importe;
                                        $items_periodo_deuda_detalle->estado = 1;
                                        //buscar el producto contratados en la solicitud
                                        $producto_solicitud = Productos_solicitud_eloquent::where('id_solicitud', $cliente->solicitudes->id)->where('id_anexo',$anexo->id)->first();
                                        $items_periodo_deuda_detalle->id_producto = $producto_solicitud->id_producto;
                                        $items_periodo_deuda_detalle->save();

                                        $acum_importes = $acum_importes +$anexo->importe;
                                    }
                                }
                                $items_periodo_deuda->deuda = $cliente->cuentas_corrientes->saldo;
                                $items_periodo_deuda->importe_total = $acum_importes;
                                $items_periodo_deuda->save();

                                //items cuenta corriente
                                $items_cuenta_corriente = new Items_cuenta_corriente_eloquent();
                                $items_cuenta_corriente->id_cuenta_corriente = $cliente->cuentas_corrientes->id;
                                $items_cuenta_corriente->id_cliente = $cliente->id;
                                $items_cuenta_corriente->importe = $acum_importes;
                                $items_cuenta_corriente->periodo = $this->input->post('periodo');
                                $items_cuenta_corriente->id_items_periodo_deuda = $items_periodo_deuda->id;
                                $items_cuenta_corriente->importe_recibo = 0;
                                $items_cuenta_corriente->deuda_recibida = 0;
                                $items_cuenta_corriente->estado = 1;
                                $items_cuenta_corriente->save();

                                //actualizar saldo cuenta corriente

                                $saldo_anterior = $cliente->cuentas_corrientes->saldo;
                                $periodo_deuda->saldo_anterior = $saldo_anterior;
                                $cliente->cuentas_corrientes->saldo = $acum_importes + $saldo_anterior;
                                $cliente->cuentas_corrientes->save();
                                //$periodo_deuda->save();

                                //agrego el salgo al items cuenta corriente
                                $items_cuenta_corriente->saldo = $cliente->cuentas_corrientes->saldo;
                                $items_cuenta_corriente->save();
                                $periodo_deuda->save();
                            } */
                            //else//pueden ver anexos activos
                            //{
                                //recorrer anexos para saber cual esta activo
                                $contador_anexos_activos = 0;

                                foreach ($cliente->solicitudes->anexos as $anexo) 
                                {
                                    $f_anex_vig_aux = date_create_from_format('Y-m-d', $anexo->fecha_vigencia);
                                    $f_anex_vig_aux = date_format($f_anex_vig_aux,"Y-m-d");
                                    $periodo_date_aux = date_format($periodo_date,"Y-m-d");
                                    if(($anexo->estado == 1) && ($f_anex_vig_aux <= $periodo_date_aux))
                                    {
                                        $contador_anexos_activos++;
                                    }
                                    else if($anexo->estado == 0) 
                                    {
                                        if($anexo->fecha_baja != NULL)
                                        {
                                            $f_anex_baja_aux = date_create_from_format('Y-m-d', $anexo->fecha_baja);
                                            $f_anex_baja_aux = date_format($f_anex_baja_aux,"Y-m-d");
                                            $periodo_date_aux = date_format($periodo_date,"Y-m-d");
                                                
                                            if(($f_anex_baja_aux > $periodo_date_aux)&&($f_anex_vig_aux != $f_anex_baja_aux))
                                            {
                                                $contador_anexos_activos++;
                                            }
                                        }
                                    }
                                }
                                if($contador_anexos_activos > 0)//hay al menos 1 anexo activo
                                {
                                    $acum_importes = 0;
                                    $items_periodo_deuda = new Items_periodo_deuda_eloquent();
                                    $items_periodo_deuda->id_periodo_deuda = $periodo_deuda->id;
                                    $items_periodo_deuda->id_cliente = $cliente->id;
                                    $items_periodo_deuda->estado = 1;
                                    $items_periodo_deuda->save();
                                    //recorrer anexos
                                    //obtener anexos
                                    foreach ($cliente->solicitudes->anexos as $anexo) 
                                    {
                                        $f_anex_vig_aux = date_create_from_format('Y-m-d', $anexo->fecha_vigencia);
                                        $f_anex_vig_aux = date_format($f_anex_vig_aux,"Y-m-d");
                                        $periodo_date_aux = date_format($periodo_date,"Y-m-d");
                                        if(($anexo->estado == 1) && ($f_anex_vig_aux <= $periodo_date_aux))
                                            
                                        {
                                            $items_periodo_deuda_detalle = new Items_periodo_deuda_detalle_eloquent();
                                            $items_periodo_deuda_detalle->id_items_periodo_deuda = $items_periodo_deuda->id;
                                            $items_periodo_deuda_detalle->id_solicitud = $cliente->solicitudes->id;
                                            $items_periodo_deuda_detalle->id_anexo = $anexo->id;
                                            $items_periodo_deuda_detalle->importe = $anexo->importe;
                                            $items_periodo_deuda_detalle->estado = 1;
                                            //buscar el producto contratados en la solicitud
                                            $producto_solicitud = Productos_solicitud_eloquent::where('id_solicitud', $cliente->solicitudes->id)->where('id_anexo',$anexo->id)->first();
                                            $items_periodo_deuda_detalle->id_producto = $producto_solicitud->id_producto;
                                            $items_periodo_deuda_detalle->save();

                                            $acum_importes = $acum_importes +$anexo->importe;
                                        }
                                        else if($anexo->estado == 0) 
                                        {
                                            if($anexo->fecha_baja != NULL)
                                            {   
                                                $f_anex_baja_aux = date_create_from_format('Y-m-d', $anexo->fecha_baja);
                                                $f_anex_baja_aux = date_format($f_anex_baja_aux,"Y-m-d");
                                                $periodo_date_aux = date_format($periodo_date,"Y-m-d");
                                                /*if($anexo->id == 81277)
                                                    {
                                                        echo "periodo baja: ";
                                                        print_r($f_anex_baja_aux);
                                                        echo "periodo cobrar: ";
                                                        print_r($periodo_date_aux);
                                                        die();
                                                    }*/
                                                if(($f_anex_baja_aux > $periodo_date_aux)&&($f_anex_vig_aux != $f_anex_baja_aux))
                                                {
                                                    
                                                    $items_periodo_deuda_detalle = new Items_periodo_deuda_detalle_eloquent();
                                                    $items_periodo_deuda_detalle->id_items_periodo_deuda = $items_periodo_deuda->id;
                                                    $items_periodo_deuda_detalle->id_solicitud = $cliente->solicitudes->id;
                                                    $items_periodo_deuda_detalle->id_anexo = $anexo->id;
                                                    $items_periodo_deuda_detalle->importe = $anexo->importe;
                                                    $items_periodo_deuda_detalle->estado = 1;
                                                    //buscar el producto contratados en la solicitud
                                                    $producto_solicitud = Productos_solicitud_eloquent::where('id_solicitud', $cliente->solicitudes->id)->where('id_anexo',$anexo->id)->first();
                                                    $items_periodo_deuda_detalle->id_producto = $producto_solicitud->id_producto;
                                                    $items_periodo_deuda_detalle->save();

                                                    $acum_importes = $acum_importes +$anexo->importe;
                                                }
                                            }
                                        }    
                                            
                                    }
                                    $items_periodo_deuda->deuda = $cliente->cuentas_corrientes->saldo;
                                    $items_periodo_deuda->importe_total = $acum_importes;
                                    $items_periodo_deuda->save();

                                    //items cuenta corriente
                                    $items_cuenta_corriente = new Items_cuenta_corriente_eloquent();
                                    $items_cuenta_corriente->id_cuenta_corriente = $cliente->cuentas_corrientes->id;
                                    $items_cuenta_corriente->id_cliente = $cliente->id;
                                    $items_cuenta_corriente->importe = $acum_importes;
                                    $items_cuenta_corriente->periodo = $this->input->post('periodo');
                                    $items_cuenta_corriente->id_items_periodo_deuda = $items_periodo_deuda->id;
                                    $items_cuenta_corriente->importe_recibo = 0;
                                    $items_cuenta_corriente->deuda_recibida = 0;
                                    $items_cuenta_corriente->estado = 1;
                                    $items_cuenta_corriente->save();

                                     //actualizar saldo cuenta corriente
                                    $saldo_anterior = $cliente->cuentas_corrientes->saldo;
                                    $periodo_deuda->saldo_anterior = $saldo_anterior;
                                    $cliente->cuentas_corrientes->saldo = $acum_importes + $saldo_anterior;
                                    $cliente->cuentas_corrientes->save();

                                    //agrego el salgo al items cuenta corriente
                                    $items_cuenta_corriente->saldo = $cliente->cuentas_corrientes->saldo;
                                    $items_cuenta_corriente->save();
                                    $periodo_deuda->save();

                                    //

                                }
                            //}   
                        }
                    }
                }   
            }

            //$this->generador_txt_tarjetas($periodo_deuda->id);

        }
        echo json_encode(array(
            "status" => TRUE,
            ));   
        //echo "fin";   
        //redirect('Periodos_deudas/index');
	}
    public function generador_txt_tarjetas($id)
    {
        
        $periodo_deuda = Periodos_deudas_eloquent::find($id);
        //print_r($periodo_deuda);
        $tarjeta = Tarjetas_eloquent::find($periodo_deuda->id_tarjeta);
        $tipo_tarjeta = Tipos_tarjetas_eloquent::find($periodo_deuda->id_tipo_tarjeta);
        //print_r($tarjeta);
        if($tarjeta->nombre == 'Nevada')
        {
            if($tipo_tarjeta->nombre == 'Credito')
            {
                //unlink('SENDERO.01.txt');
                $txt= fopen('SENDERO.01.txt', 'w+') or die ('Problemas al crear el archivo');
                
                $archivo ='SENDERO.01.txt';
                $fecha = substr($periodo_deuda->fecha_envio,2,2).substr($periodo_deuda->fecha_envio,5,2).substr($periodo_deuda->fecha_envio,8,2);
                $header = '610001968                   '.$fecha;

                fwrite($txt, $header.PHP_EOL);  
                #  Se establecen los datos que va a conterner el archivo
                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                    $nro_tarjeta=$key->clientes->tarjeta_credito_cliente->nro_tarjeta;
                    $total = $key->importe_total;

                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        
                        $parte_entera_rellenada = str_pad($decimales[0], 10, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 00, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }


                    $producto = Productos_solicitud_eloquent::where('id_solicitud',$key->clientes->solicitudes->id)->where('id_anexo',0)->first();
                    $numero_cliente = str_pad($key->clientes->nro_socio, 10, '0', STR_PAD_LEFT);
                    $numero_cliente = 'Socio N§ '.$numero_cliente;

                    $cadena = $nro_tarjeta.$total_cobrar.$fecha.$numero_cliente.'                ';

                    //agregar la deuda al renglon de abajo
                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {

                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $total = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $total = $key->deuda;
                            }
                            if (is_float($total))
                            {
                                $decimales = explode(".",$total);
                                $parte_entera_rellenada = str_pad($decimales[0], 12, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($total, 12, '0', STR_PAD_LEFT);
                                $total_cobrar = $parte_entera_rellenada.'00';
                            }
                            $cadena = $nro_tarjeta.$total_cobrar.$fecha.$numero_cliente.' DEUDA          ';
                            fwrite($txt, $cadena.PHP_EOL);
                            
                        }
                    }
                    fwrite($txt, $cadena.PHP_EOL);   
                }
                //fwrite($txt, "primer dato,");
                //fwrite($txt, "segundo dato,");
                #  Se hace el ciere para no sobre escribir datos 
                $pie = '000154          000002471700'.$fecha.'                                   ';
                fwrite($txt, $pie.PHP_EOL);  
                fwrite($txt, '                                                                         '.PHP_EOL);
                fwrite($txt, '                                                                         '.PHP_EOL);
                fclose($txt);
            }    
                
        }
        if($tarjeta->nombre == 'Visa')
        {
            if($tipo_tarjeta->nombre == 'Debito')
            {
                //unlink('DEBLIQD.txt');
                $txt= fopen('DEBLIQD.txt', 'w+') or die ('Problemas al crear el archivo');
                $archivo ='DEBLIQD.txt';
                $fecha_envio = $periodo_deuda->fecha_envio;
                $fecha_envio = explode("-",$fecha_envio);
                $fecha_envio = $fecha_envio[0].$fecha_envio[1].$fecha_envio[2];
                $cabecera = '0DEBLIQD 0018692491900000    '.$fecha_envio.'08460                                                         *';
                fwrite($txt, $cabecera.PHP_EOL); 
                //fwrite($txt,'*'.PHP_EOL); 
                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                    $nro_tarjeta=$key->clientes->tarjeta_credito_cliente->nro_tarjeta;
                    $total = $key->importe_total;
                    $fecha = substr($periodo_deuda->periodo,2,2).substr($periodo_deuda->periodo,5,2).substr($periodo_deuda->periodo,8,2);
                    $fecha_inicio = $periodo_deuda->fecha_inicio;
                    $fecha_inicio = explode("-",$fecha_inicio);
                    $fecha_inicio = $fecha_inicio[0].$fecha_inicio[1].$fecha_inicio[2];
                    $num_unic_x_archivo = str_pad($key->id, 8, '0', STR_PAD_LEFT);
                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 13, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 13, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }
                    $numero_cliente = str_pad($key->clientes->nro_socio, 15, '0', STR_PAD_LEFT);
                    $cadena = '1'.$nro_tarjeta.'   '.$num_unic_x_archivo.$fecha_inicio.'0005'.$total_cobrar.$numero_cliente;
                    $cadena = str_pad($cadena, 99, ' ', STR_PAD_RIGHT).'*';
                    fwrite($txt, $cadena.PHP_EOL);  
                    //fwrite($txt,'*'.PHP_EOL); 

                    //agregar la deuda al renglon de abajo
                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {

                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $total = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $total = $key->deuda;
                            }
                            if (is_float($total))
                            {
                                $decimales = explode(".",$total);
                                $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                                $total_cobrar = $parte_entera_rellenada.'00';
                            }
                            $cadena = '1'.$nro_tarjeta.'   '.$num_unic_x_archivo.$fecha_inicio.'0005'.$total_cobrar.$numero_cliente;
                            $cadena = str_pad($cadena, 99, ' ', STR_PAD_RIGHT).'*';
                            fwrite($txt, $cadena.PHP_EOL);
                            
                        }
                    }
                }

                $pie = '9DEBLIQD 0018692491900000    '.$fecha_envio.'08460000010000000000117400                                    *';
                fwrite($txt, $pie.PHP_EOL); 
                fwrite($txt, '                                                                         '.PHP_EOL);
                fwrite($txt, '                                                                         '.PHP_EOL);
                fclose($txt);
            }
            else if($tipo_tarjeta->nombre == 'Credito')
            {
                //unlink('DEBLIQC.txt');
                $txt= fopen('DEBLIQC.txt', 'w+') or die ('Problemas al crear el archivo');
                $archivo ='DEBLIQC.txt';
                $fecha_envio = $periodo_deuda->fecha_envio;
                $fecha_envio = explode("-",$fecha_envio);
                $fecha_envio = $fecha_envio[0].$fecha_envio[1].$fecha_envio[2];
                $cabecera = '0DEBLIQC 0018692491900000    '.$fecha_envio.'08460                                                         *';
                fwrite($txt, $cabecera.PHP_EOL); 
                //fwrite($txt,'*'.PHP_EOL); 
                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                    $nro_tarjeta=$key->clientes->tarjeta_credito_cliente->nro_tarjeta;
                    $total = $key->importe_total;
                    $fecha = substr($periodo_deuda->periodo,2,2).substr($periodo_deuda->periodo,5,2).substr($periodo_deuda->periodo,8,2);
                    $fecha_inicio = $periodo_deuda->fecha_inicio;
                    $fecha_inicio = explode("-",$fecha_inicio);
                    $fecha_inicio = $fecha_inicio[0].$fecha_inicio[1].$fecha_inicio[2];
                    $num_unic_x_archivo = str_pad($key->id, 8, '0', STR_PAD_LEFT);
                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 13, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 13, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }
                    $numero_cliente = str_pad($key->clientes->nro_socio, 15, '0', STR_PAD_LEFT);
                    $cadena = '1'.$nro_tarjeta.'   '.$num_unic_x_archivo.$fecha_inicio.'0005'.$total_cobrar.$numero_cliente;
                    $cadena = str_pad($cadena, 99, ' ', STR_PAD_RIGHT).'*';
                    fwrite($txt, $cadena.PHP_EOL);  
                   // fwrite($txt,'*'.PHP_EOL); 
                    //agregar la deuda al renglon de abajo
                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {

                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $total = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $total = $key->deuda;
                            }
                            if (is_float($total))
                            {
                                $decimales = explode(".",$total);
                                $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                                $total_cobrar = $parte_entera_rellenada.'00';
                            }
                            $cadena = '1'.$nro_tarjeta.'   '.$num_unic_x_archivo.$fecha_inicio.'0005'.$total_cobrar.$numero_cliente;
                            $cadena = str_pad($cadena, 99, ' ', STR_PAD_RIGHT).'*';
                            fwrite($txt, $cadena.PHP_EOL);
                            
                        }
                    }
                }
                $pie = '9DEBLIQC 0018692491900000    '.$fecha_envio.'08460000010000000000117400                                    *';
                fwrite($txt, $pie.PHP_EOL); 
                fwrite($txt, '                                                                         '.PHP_EOL);
                fclose($txt);
            }

        }
        if($tarjeta->nombre == 'Fiel')
        {
           // die();
            //if($tipo_tarjeta->nombre == 'Debito')
            //{
                
                //para el archivo
                $fecha_envio = $periodo_deuda->fecha_envio;
                $fecha_envio = explode("-",$fecha_envio);
                $fecha_envio = $fecha_envio[0].$fecha_envio[1].$fecha_envio[2];

               // unlink('F1615'.$fecha_envio.'.txt');
                $txt= fopen('F1615'.$fecha_envio.'.txt', 'w+') or die ('Problemas al crear el archivo');
                $archivo ='F1615'.$fecha_envio.'.txt';
                //para los registro
                $fecha_envio = $periodo_deuda->fecha_envio;
                $fecha_envio = explode("-",$fecha_envio);
                $fecha_envio = $fecha_envio[0].$fecha_envio[1].$fecha_envio[2];

                $periodo = $periodo_deuda->periodo;
                $periodo = explode("-",$periodo);
                $periodo = $periodo[0];

                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                    $nro_tarjeta=$key->clientes->tarjeta_credito_cliente->nro_tarjeta;
                    $total = $key->importe_total;
                    $fecha = substr($periodo_deuda->periodo,2,2).substr($periodo_deuda->periodo,5,2).substr($periodo_deuda->periodo,8,2);
                   // $fecha_inicio = $periodo_deuda->fecha_inicio;
                    //$fecha_inicio = explode("-",$fecha_inicio);
                   // $fecha_inicio = $fecha_inicio[0].$fecha_inicio[1].$fecha_inicio[2];
                   // $num_unic_x_archivo = str_pad($key->id, 8, '0', STR_PAD_LEFT);
                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 15, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 15, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }
                    //$numero_cliente = str_pad($key->clientes->nro_socio, 15, '0', STR_PAD_LEFT);
                    $cadena = $fecha_envio.'01615'.$nro_tarjeta.'00000000001'.$total_cobrar.$periodo;
                    //$cadena = str_pad($cadena, 99, ' ', STR_PAD_RIGHT).'*';
                    fwrite($txt, $cadena.PHP_EOL);  

                    //agregar la deuda al renglon de abajo
                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {

                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $total = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $total = $key->deuda;
                            }
                            if (is_float($total))
                            {
                                $decimales = explode(".",$total);
                                $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                                $total_cobrar = $parte_entera_rellenada.'00';
                            }
                            $cadena = $fecha_envio.'01615'.$nro_tarjeta.'00000000001'.$total_cobrar.$periodo;
                            fwrite($txt, $cadena.PHP_EOL);
                            
                        }
                    }
                    
                }
                $pie = '';

                fwrite($txt, $pie.PHP_EOL);
                fwrite($txt, '                                                                         '.PHP_EOL);
                
                fclose($txt);
           // }
        }
        
        /*header('Content-type: text/plain');
        header('Content-Disposition: attachment; filename="'.$archivo.'"');*/

        //$objWriter->save('php://output');
        //print_r('holaaaa');
        header ("Content-Disposition: attachment; filename=".$archivo); 
        header ("Content-Type: application/octet-stream"); 
        header ("Content-Length: ".filesize($archivo)); 
        readfile($archivo); 
        return;
    }

    public function generador_txt_bancos($id)
    {
        $periodo_deuda = Periodos_deudas_eloquent::find($id);
        $banco = Bancos_eloquent::find($periodo_deuda->id_banco);
        if($banco->nombre == 'Credicoop')//este banco usa CBU
        {
            
            if($this->session->userdata('id_empresa') == 1) // sendero salud
            {
                //echo "llego";
                 //die();
                //unlink('main486.txt');
                $txt= fopen('main486.txt', 'w+') or die ('Problemas al crear el archivo');
                $archivo ='main486.txt';
                $fecha_envio = $periodo_deuda->fecha_envio;
                $fecha_envio = explode("-",$fecha_envio);
                $fecha_envio = $fecha_envio[0].$fecha_envio[1].$fecha_envio[2];
                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    /*$entidad = substr($cbu,0,3);
                    $sucursal = substr($cbu,3,4);
                    $nro_cuenta =substr($cbu,7,13);*/


                    $total = $key->importe_total;
                    $fecha = substr($periodo_deuda->periodo,2,2).substr($periodo_deuda->periodo,5,2).substr($periodo_deuda->periodo,8,2);
                    $fecha_inicio = $periodo_deuda->fecha_inicio;
                    $fecha_inicio = explode("-",$fecha_inicio);
                    $fecha_inicio = $fecha_inicio[0].$fecha_inicio[1].$fecha_inicio[2];
                    $fecha_inicio = substr($fecha_inicio, 2);
                    $num_unic_x_archivo = str_pad($key->id, 8, '0', STR_PAD_LEFT);
                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }
                    $numero_cliente = str_pad($key->clientes->nro_socio, 10, '0', STR_PAD_LEFT);
                    $cadena = '19151'.$fecha_inicio.'486'.$numero_cliente.'              '.'P'.$cbu.$total_cobrar.$key->clientes->empresas->cuit.$key->clientes->empresas->alias.'    '.' PERIODO                                                  ';
                    //$cadena = str_pad($cadena, 99, ' ', STR_PAD_RIGHT).'*';
                    fwrite($txt, $cadena.PHP_EOL);  
                    
                    //agregar la deuda al renglon de abajo
                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {

                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $total = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $total = $key->deuda;
                            }
                            if (is_float($total))
                            {
                                $decimales = explode(".",$total);
                                $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                                $total_cobrar = $parte_entera_rellenada.'00';
                            }
                            $numero_cliente = str_pad($key->clientes->nro_socio, 10, '0', STR_PAD_LEFT);
                            $cadena = '19151'.$fecha_inicio.'486'.$numero_cliente.'              '.'P'.$cbu.$total_cobrar.$key->clientes->empresas->cuit.$key->clientes->empresas->alias.'    '.' PERIODO                                                  ';
                            fwrite($txt, $cadena.PHP_EOL);
                            
                        }
                    }
                    
                }
                fwrite($txt, '                                                                         '.PHP_EOL);
                fclose($txt);


            }
            if($this->session->userdata('id_empresa') == 2) // sendero salud
            {
                //echo "llego";
                 //die();
                //unlink('main486.txt');
                $txt= fopen('main486.txt', 'w+') or die ('Problemas al crear el archivo');
                $archivo ='main486.txt';
                $fecha_envio = $periodo_deuda->fecha_envio;
                $fecha_envio = explode("-",$fecha_envio);
                $fecha_envio = $fecha_envio[0].$fecha_envio[1].$fecha_envio[2];
                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    /*$entidad = substr($cbu,0,3);
                    $sucursal = substr($cbu,3,4);
                    $nro_cuenta =substr($cbu,7,13);*/


                    $total = $key->importe_total;
                    $fecha = substr($periodo_deuda->periodo,2,2).substr($periodo_deuda->periodo,5,2).substr($periodo_deuda->periodo,8,2);
                    $fecha_inicio = $periodo_deuda->fecha_inicio;
                    $fecha_inicio = explode("-",$fecha_inicio);
                    $fecha_inicio = $fecha_inicio[0].$fecha_inicio[1].$fecha_inicio[2];
                    $fecha_inicio = substr($fecha_inicio, 2);
                    $num_unic_x_archivo = str_pad($key->id, 8, '0', STR_PAD_LEFT);
                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }
                    $numero_cliente = str_pad($key->clientes->nro_socio, 10, '0', STR_PAD_LEFT);
                    $cadena = '19151'.$fecha_inicio.'486'.$numero_cliente.'              '.'P'.$cbu.$total_cobrar.$key->clientes->empresas->cuit.$key->clientes->empresas->alias.'    '.'PERIODO                                                  ';
                    //$cadena = str_pad($cadena, 99, ' ', STR_PAD_RIGHT).'*';
                    fwrite($txt, $cadena.PHP_EOL);  
                    
                    //agregar la deuda al renglon de abajo
                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {

                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $total = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $total = $key->deuda;
                            }
                            if (is_float($total))
                            {
                                $decimales = explode(".",$total);
                                $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                                $total_cobrar = $parte_entera_rellenada.'00';
                            }
                            $numero_cliente = str_pad($key->clientes->nro_socio, 10, '0', STR_PAD_LEFT);
                            $cadena = '19151'.$fecha_inicio.'486'.$numero_cliente.'              '.'P'.$cbu.$total_cobrar.$key->clientes->empresas->cuit.$key->clientes->empresas->alias.'    '.'PERIODO                                                  ';
                            fwrite($txt, $cadena.PHP_EOL);
                            
                        }
                    }
                    
                }
                fwrite($txt, '                                                                         '.PHP_EOL);
                fclose($txt);


            }
            
        }
        if($banco->nombre == 'Patagonia')
        {
            if($this->session->userdata('id_empresa') == 1) // sendero Salud
            {

                $fecha_inicio = $periodo_deuda->fecha_inicio;
                $fecha_inicio = explode("-",$fecha_inicio);
                $fecha_inicio = $fecha_inicio[2].$fecha_inicio[1].$fecha_inicio[0];

                $txt= fopen('DEBITOS DIRECTOS SENDERO S.A. 985.txt', 'w+') or die ('Problemas al crear el archivo');
                $archivo ='DEBITOS DIRECTOS SENDERO S.A. 985.txt';
                $cabecera = 'H30708011432CUOTAS    985'.$fecha_inicio.'            SENDERO SA                                                                                                                                                 
';
                fwrite($txt, $cabecera.PHP_EOL);
                $fecha_fin = $periodo_deuda->fecha_fin;
                $fecha_fin = explode("-",$fecha_fin);
                $fecha_fin = $fecha_fin[2].$fecha_fin[1].$fecha_fin[0];

                $periodo = $periodo_deuda->periodo;
                $periodo = explode("-",$periodo);
                $periodo = $periodo[1].'-'.$periodo[0];
                $cont = 0;
                $acum = 0;
                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                    
                    $total = $key->importe_total;
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    $num_unic_x_archivo = str_pad($key->id, 8, '0', STR_PAD_LEFT);
                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }

                    $numero_cliente = str_pad($key->clientes->nro_socio, 6, '0', STR_PAD_LEFT);
                    $numero_cliente = str_pad($numero_cliente, 22, ' ', STR_PAD_RIGHT);
                    
                    $dni = str_pad($key->clientes->users_groups->users->documento, 11, '0', STR_PAD_LEFT);
                    $cadena = 'D'.$dni.$cbu.$numero_cliente.$fecha_fin.'CUOTAS                   CUOTA '.$periodo.'  ';
                    //fwrite($txt, $cadena.PHP_EOL);
                    //en este banco hay q insertar dos regitros por cliente
                    $cadena2 =  $total_cobrar.'P'.'                                                                          '.$key->clientes->empresas->cuit;
                    $cadena = $cadena.$cadena2;
                    fwrite($txt, $cadena.PHP_EOL);
                    $cont++;
                    $acum = $acum + $key->importe_total;
                    //agregar la deuda al renglon de abajo
                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {

                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $total = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $total = $key->deuda;
                            }
                            if (is_float($total))
                            {
                                $decimales = explode(".",$total);
                                $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                                $total_cobrar = $parte_entera_rellenada.'00';
                            }
                            /*$numero_cliente = str_pad($key->clientes->nro_socio, 6, '0', STR_PAD_LEFT);
                            $numero_cliente = str_pad($numero_cliente, 22, ' ', STR_PAD_RIGHT);*/
                    
                            $dni = str_pad($key->clientes->users_groups->users->documento, 11, '0', STR_PAD_LEFT);
                            $cadena = 'D'.$dni.$cbu.$numero_cliente.$fecha_fin.'CUOTAS                   DEUDA '.$periodo.'  ';
                            //fwrite($txt, $cadena.PHP_EOL);
                            //en este banco hay q insertar dos regitros por cliente
                            $cadena2 =  $total_cobrar.'P'.'                                                                          '.$key->clientes->empresas->cuit;
                            $cadena = $cadena.$cadena2;
                            fwrite($txt, $cadena.PHP_EOL);
                            $cont++;
                            $acum = $acum + $total;
                            
                        }
                    }

                  
                }  
                $acum = str_pad($acum, 6, '0', STR_PAD_LEFT);  
                $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);       
                fwrite($txt, 'T000'.$cont.'0000000'.$acum.'00                                                                                                                                                                                 '.PHP_EOL);     
                fclose($txt);
            }
            if($this->session->userdata('id_empresa') == 2) // sendero Servicio
            {
                $txt= fopen('DEBITOS DIRECTOS SENDERO S.A. 985.txt', 'w+') or die ('Problemas al crear el archivo');
                $archivo ='DEBITOS DIRECTOS SENDERO S.A. 985.txt';
                $cabecera = 'H30708011432CUOTAS    98531012017            SENDERO SA                                                                                                                                                 
';
                fwrite($txt, $cabecera.PHP_EOL);
                $fecha_fin = $periodo_deuda->fecha_fin;
                $fecha_fin = explode("-",$fecha_fin);
                $fecha_fin = $fecha_fin[2].$fecha_fin[1].$fecha_fin[0];

                $periodo = $periodo_deuda->periodo;
                $periodo = explode("-",$periodo);
                $periodo = $periodo[1].'-'.$periodo[0];
                $cont = 0;
                $acum = 0;
                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                    
                    $total = $key->importe_total;
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    $num_unic_x_archivo = str_pad($key->id, 8, '0', STR_PAD_LEFT);
                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }

                    $numero_cliente = str_pad($key->clientes->nro_socio, 22, ' ', STR_PAD_RIGHT);
                    
                    $dni = str_pad($key->clientes->users_groups->users->documento, 11, '0', STR_PAD_LEFT);
                    $cadena = 'D'.$dni.$cbu.$numero_cliente.$fecha_fin.'CUOTAS                   DEUDA '.$periodo.'  ';
                    //fwrite($txt, $cadena.PHP_EOL);
                    //en este banco hay q insertar dos regitros por cliente
                    $cadena2 =  $total_cobrar.'P'.'                                                                          '.$key->clientes->empresas->cuit;
                    $cadena = $cadena.$cadena2;
                    fwrite($txt, $cadena.PHP_EOL);
                    $cont++;
                    $acum = $acum + $key->importe_total;
                    //agregar la deuda al renglon de abajo
                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {

                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $total = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $total = $key->deuda;
                            }
                            if (is_float($total))
                            {
                                $decimales = explode(".",$total);
                                $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                                $total_cobrar = $parte_entera_rellenada.'00';
                            }
                            $numero_cliente = str_pad($key->clientes->nro_socio, 22, ' ', STR_PAD_RIGHT);
                    
                            $dni = str_pad($key->clientes->users_groups->users->documento, 11, '0', STR_PAD_LEFT);
                            $cadena = 'D'.$dni.'0'.$cbu.$numero_cliente.$fecha_fin.'CUOTAS                   CUOTA '.$periodo.'  ';
                            //fwrite($txt, $cadena.PHP_EOL);
                            //en este banco hay q insertar dos regitros por cliente
                            $cadena2 =  $total_cobrar.'P'.'                                                                          '.$key->clientes->empresas->cuit;
                            $cadena = $cadena.$cadena2;
                            fwrite($txt, $cadena.PHP_EOL);
                            $cont++;
                            $acum = $acum + $total;
                            
                        }
                    }

                  
                }         
                $acum = str_pad($acum, 6, '0', STR_PAD_LEFT);  
                $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);       
                fwrite($txt, 'T000'.$cont.'0000000'.$acum.'00                                                                                                                                                                                 '.PHP_EOL);    
                fclose($txt);
            }
        }
        if($banco->nombre == 'Macro')//solamente hay socios en macro
        {
            if($this->session->userdata('id_empresa') == 2) // sendero servicios
            {
                $txt= fopen('DD73813.txt', 'w+') or die ('Problemas al crear el archivo');
                $archivo ='DD73813.txt';
                
                $fecha_envio = $periodo_deuda->fecha_envio;
                $fecha_envio = explode("-",$fecha_envio);
                $fecha_envio = $fecha_envio[0].$fecha_envio[1].$fecha_envio[2];

                $cabecera = '173813          00000'.$fecha_envio.'0000000000002293000800100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000                                                                     0';
                fwrite($txt, $cabecera.PHP_EOL);

                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                    $total = $key->importe_total;
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    $num_unic_x_archivo = str_pad($key->id, 8, '0', STR_PAD_LEFT);
                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 11, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 11, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }
                    $numero_cliente = str_pad($key->clientes->nro_socio, 22, ' ', STR_PAD_RIGHT);
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    $cbu = substr($cbu,0,9).'41'.substr($cbu,9,12);
                    $ape_nom = $key->clientes->users_groups->users->last_name.' '.$key->clientes->users_groups->users->first_name;
                    $ape_nom = substr($ape_nom, 0,15);
                    $cadena = '073813          '.'00000'.$cbu.$numero_cliente.' '.$ape_nom.'     ';
                    //fwrite($txt, $cadena.PHP_EOL);
                    //en este banco hay q insertar dos regitros por cliente

                    $fecha_inicio = $periodo_deuda->fecha_inicio;
                    $fecha_inicio = explode("-",$fecha_inicio);
                    $fecha_inicio = $fecha_inicio[0].$fecha_inicio[1].$fecha_inicio[2];
                    $cadena2 = $fecha_inicio.'080'.$total_cobrar.'00000000000000000000000000000000000000000                                                                   0';
                    $cadena = $cadena.$cadena2;
                    fwrite($txt, $cadena.PHP_EOL);

                    //agregar la deuda al renglon de abajo
                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {

                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $total = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $total = $key->deuda;
                            }
                            if (is_float($total))
                            {
                                $decimales = explode(".",$total);
                                $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                                $total_cobrar = $parte_entera_rellenada.'00';
                            }
                            $cadena = '073813          '.'00000'.'0'.$cbu.$numero_cliente.' '.$ape_nom.'     ';
                            //fwrite($txt, $cadena.PHP_EOL);
                            //en este banco hay q insertar dos regitros por cliente

                            $fecha_inicio = $periodo_deuda->fecha_inicio;
                            $fecha_inicio = explode("-",$fecha_inicio);
                            $fecha_inicio = $fecha_inicio[0].$fecha_inicio[1].$fecha_inicio[2];
                            $cadena2 = $fecha_inicio.'080'.$total_cobrar.'00000000000000000000000000000000000000000                                                                   0';
                            $cadena = $cadena.$cadena2;
                            fwrite($txt, $cadena.PHP_EOL);
                            
                        }
                    }


                }
                fwrite($txt, '                                                                         '.PHP_EOL);
                fclose($txt);



            }
        }
        if($banco->nombre == 'Nacion')//solamente hay socios en macro
        {
            if($this->session->userdata('id_empresa') == 1) // sendero Salud
            {
                $txt= fopen('SNDERO1E.txt', 'w+') or die ('Problemas al crear el archivo');
                $archivo ='SNDERO1E.txt';

                $fecha_inicio = $periodo_deuda->fecha_inicio;
                $fecha_inicio = explode("-",$fecha_inicio);
                $fecha_inicio = $fecha_inicio[0].$fecha_inicio[1].$fecha_inicio[2];

                $fecha_envio = $periodo_deuda->fecha_envio;
                $fecha_envio = explode("-",$fecha_envio);
                $fecha_envio = $fecha_envio[0].$fecha_envio[1].$fecha_envio[2];

                $fecha_fin = $periodo_deuda->fecha_fin;
                $fecha_fin = explode("-",$fecha_fin);
                $fecha_fin = $fecha_fin[0].$fecha_fin[1].$fecha_fin[2];

                $tope_mes = $periodo_deuda->tope_mes;
                $tope_mes = explode("-",$tope_mes);
                $tope_mes = $tope_mes[0];
                $cabecera = '13200104740076507PE'.$tope_mes.'01'.$fecha_fin.'REE                                                                                              ';
                fwrite($txt, $cabecera.PHP_EOL);
                $acumulador_para_txt = 0;
                $registos = 0;
                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                   
                    $total = $key->importe_total;
                    $sucursal=$key->clientes->cuentas_bancarias_clientes->sucursal;
                    $nro_cuenta = $key->clientes->cuentas_bancarias_clientes->nro_cuenta;
                   
                    if(is_float($total) == TRUE)
                    {
                       
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 13, '0', STR_PAD_LEFT);

                       // print_r($decimales);

                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;

                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 13, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }

                    $numero_cliente = str_pad($key->clientes->nro_socio, 6, '0', STR_PAD_LEFT);
                    $cont_renglon = str_pad($registos + 1, 4, '0', STR_PAD_LEFT);
                    //$cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    

                    $tipo_cuenta = $key->clientes->cuentas_bancarias_clientes->tipos_cuentas_bancarias->alias1;

                    $fecha_fin = $periodo_deuda->fecha_fin;
                    $fecha_fin = explode("-",$fecha_fin);
                    $fecha_fin = $fecha_fin[0].$fecha_fin[1].$fecha_fin[2];

                    $cadena = '2'.$sucursal.$tipo_cuenta.''.$nro_cuenta.$total_cobrar.'000000000'.'                              '.$numero_cliente.$cont_renglon.'                                              ';
                    fwrite($txt, $cadena.PHP_EOL);
                    $acumulador_para_txt = $acumulador_para_txt + $total_cobrar;
                    $registos++;
                    //agregar la deuda al renglon de abajo
                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {

                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $total = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $total = $key->deuda;
                            }
                            if (is_float($total))
                            {
                                $decimales = explode(".",$total);
                                $parte_entera_rellenada = str_pad($decimales[0], 13, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($total, 13, '0', STR_PAD_LEFT);
                                $total_cobrar = $parte_entera_rellenada.'00';
                            }
                            $cont_renglon = str_pad($registos + 1, 4, '0', STR_PAD_LEFT);
                            $cadena = '2'.$sucursal.$tipo_cuenta.''.$nro_cuenta.$total_cobrar.'000000000'.'                              '.$numero_cliente.$cont_renglon.'                                              ';
                            $acumulador_para_txt = $acumulador_para_txt + $total_cobrar;
                            $registos++;
                            
                            fwrite($txt, $cadena.PHP_EOL);
                        }
                    }
                

                }
                $acumulador_para_txt = str_replace(',','',$acumulador_para_txt); 
                $acumulador_para_txt = str_replace('.','',$acumulador_para_txt); 
                $acumulador_para_txt = str_pad($acumulador_para_txt, 7, '0', STR_PAD_RIGHT);
                $registos = str_pad($registos, 5, '0', STR_PAD_RIGHT);
                $pie = '30000000'.$acumulador_para_txt.'00'.$registos.'00000000000000000000                                                                                     
';
                fwrite($txt, $pie.PHP_EOL);
                fwrite($txt, '                                                                         '.PHP_EOL);
                fclose($txt);

            }
        }
        if($banco->nombre == 'Supervielle')
        {
            //if($this->session->userdata('id_empresa') == 1) // sendero Salud
           // {
                $txt= fopen('DSEND.txt', 'w+') or die ('Problemas al crear el archivo');
                $archivo ='DSEND.txt';

                $periodo = $periodo_deuda->periodo;
                $periodo = explode("-",$periodo);
                $periodo = $periodo[0];
                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                    $total = $key->importe_total;
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    $num_unic_x_archivo = str_pad($key->id, 8, '0', STR_PAD_LEFT);
                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);

                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }
                    $numero_cliente = str_pad($key->clientes->nro_socio, 6, '0', STR_PAD_LEFT);
                    $numero_cliente = str_pad($numero_cliente, 22, ' ', STR_PAD_RIGHT);
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    //rellenar cbu en la parte de la cuenta
                    $ente = substr($cbu,0,3);
                    $sucursal = substr($cbu, 3,4);
                    $primer_digito_verificador = substr($cbu, 7, 1);
                    $nro_de_cuenta = substr($cbu, 8, 21);
                    $cbu_rellenado = $ente.$sucursal.$primer_digito_verificador.'000'.$nro_de_cuenta;
                    //
                    $fecha_inicio = $periodo_deuda->fecha_inicio;
                    $fecha_inicio = explode("-",$fecha_inicio);
                    $fecha_inicio = $fecha_inicio[2].$fecha_inicio[1].$fecha_inicio[0];
                    
                    $periodo_mes_anio = $periodo_deuda->periodo;
                    $periodo_mes_anio = explode("-",$periodo_mes_anio);
                    $periodo_mes_anio = $periodo_mes_anio[1].$periodo_mes_anio[0];

                    $cadena = 'D'.$key->clientes->empresas->cuit.'0'.$periodo.'CUOTA     '.$fecha_inicio.$cbu_rellenado.$numero_cliente.'00000000'.$periodo_mes_anio.'         ';
                    //fwrite($txt, $cadena.PHP_EOL);
                    //en este banco hay q insertar dos regitros por cliente


                    $fecha_fin = $periodo_deuda->fecha_fin;
                    $fecha_fin = explode("-",$fecha_fin);
                    $fecha_fin = $fecha_fin[2].$fecha_fin[1].$fecha_fin[0];
                    
                    $cadena2 = $total_cobrar.'80'.$fecha_fin.$total_cobrar.'000000000000000000                         00000000000000000000                                                      ';
                    $cadena = $cadena.$cadena2;
                    fwrite($txt, $cadena.PHP_EOL);


                    //agregar la deuda al renglon de abajo
                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {

                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $total = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $total = $key->deuda;
                            }
                            if (is_float($total))
                            {
                                $decimales = explode(".",$total);
                                $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                                $total_cobrar = $parte_entera_rellenada.'00';
                            }
                            $cadena = 'D'.$key->clientes->empresas->cuit.'0'.$periodo.'CUOTA     '.$fecha_inicio.$cbu_rellenado.$numero_cliente.'00000000'.$periodo_mes_anio.'         ';
                            $fecha_fin = $periodo_deuda->fecha_fin;
                            $fecha_fin = explode("-",$fecha_fin);
                            $fecha_fin = $fecha_fin[2].$fecha_fin[1].$fecha_fin[0];
                               
                            $cadena2 = $total_cobrar.'80'.$fecha_fin.$total_cobrar.'000000000000000000                         00000000000000000000                                                      ';
                            $cadena = $cadena.$cadena2;
                            fwrite($txt, $cadena.PHP_EOL);
                        }
                    }

                }
                fwrite($txt, '                                                                         '.PHP_EOL);
                fclose($txt);

            //}
        }
        if($banco->nombre == 'San Juan')// 
        {
            
                $txt= fopen('ENTRECAUT.txt', 'w+') or die ('Problemas al crear el archivo');
                $archivo ='ENTRECAUT.txt';
                $cont = 1;
                if($this->session->userdata('id_empresa') == 1)
                {
                    $num_entidad_cliente = 232;
                }
                else if($this->session->userdata('id_empresa') == 2)
                {
                    $num_entidad_cliente = 317;
                }

                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                    $total = $key->importe_total;
                    
                    $num_unic= str_pad($key->id, 10, '0', STR_PAD_LEFT);
                    /*if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 13, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 13, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }*/
                    $total_new = explode(".",$total);
                            if(isset($total_new[1]) == FALSE)
                            {
                                $total_cobrar = $total_new[0].'00';
                                $total_cobrar = str_pad($total_cobrar, 15, '0', STR_PAD_LEFT);
                            }
                            else
                            {
                                $total_cobrar = $total_new[0].str_pad($total_new[1], 2, '0', STR_PAD_RIGHT);
                                $total_cobrar = str_pad($total_cobrar, 15, '0', STR_PAD_LEFT);
                            }
                    $numero_cliente = str_pad($key->clientes->nro_socio, 25, ' ', STR_PAD_RIGHT);
                    

                    
                    $nro_aux = str_pad($cont, 25, ' ', STR_PAD_RIGHT);

                    $periodo = $periodo_deuda->periodo;
                    $periodo = explode("-",$periodo);
                    //$periodo = $periodo[0];
                    //$periodo = str_pad($periodo, 4, '0', STR_PAD_LEFT);
                    $periodo = '0001'.substr($periodo[1],2,2).$periodo[0];


                    $periodo_d = $periodo_deuda->periodo;
                    $periodo_d = substr($periodo_d, 5,2).substr($periodo_d, 0,2);

                    $fecha_inicio = $periodo_deuda->fecha_inicio;
                    $fecha_inicio = explode("-",$fecha_inicio);
                    $fecha_inicio = substr($fecha_inicio[0],2,2).$fecha_inicio[1].$fecha_inicio[2];
                    //$fecha_inicio = '0001'.substr($fecha_inicio[0],2,2).$fecha_inicio[1];

                    $fecha_fin = $periodo_deuda->fecha_fin;
                    $fecha_fin = explode("-",$fecha_fin);
                    $fecha_fin = substr($fecha_fin[0],2,2).$fecha_fin[1].$fecha_fin[2];

                    //$imp_vacio ='               ';
                    $imp_vacio ='000000000000000';

                    $sucursal = $key->clientes->cuentas_bancarias_clientes->sucursal;
                    $nro_cuenta = $key->clientes->cuentas_bancarias_clientes->nro_cuenta;
                    $tipo_cuenta = $key->clientes->cuentas_bancarias_clientes->tipos_cuentas_bancarias->alias2;

                    $cadena = '04500000000000000'.$num_entidad_cliente.'01'.$numero_cliente.$periodo.'0000000001'.'                '.$fecha_inicio.$total_cobrar.$imp_vacio.$fecha_fin.$total_cobrar.$imp_vacio.$fecha_fin.$total_cobrar.$imp_vacio.'                                                0000000'.$tipo_cuenta.$sucursal.$nro_cuenta.'    ';
                    fwrite($txt, $cadena.PHP_EOL);
                    //en este banco hay q insertar dos regitros por cliente

                    
                    
                    $cont = $cont + 1; 
                    //agregar la deuda al renglon de abajo
                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {

                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $total = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $total = $key->deuda;
                            }
                            /*if (is_float($total))
                            {
                                $decimales = explode(".",$total);
                                $parte_entera_rellenada = str_pad($decimales[0], 13, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($total, 13, '0', STR_PAD_LEFT);
                                $total_cobrar = $parte_entera_rellenada.'00';
                            }*/
                            $total_new = explode(".",$total);
                            if(isset($total_new[1]) == FALSE)
                            {
                                $total_cobrar = $total_new[0].'00';
                                $total_cobrar = str_pad($total_cobrar, 15, '0', STR_PAD_LEFT);
                            }
                            else
                            {
                                $total_cobrar = $total_new[0].str_pad($total_new[1], 2, '0', STR_PAD_RIGHT);
                                $total_cobrar = str_pad($total_cobrar, 15, '0', STR_PAD_LEFT);
                            }
                            $cadena = '04500000000000000'.$num_entidad_cliente.'01'.$numero_cliente.$periodo.'0000000002'.'                '.$fecha_inicio.$total_cobrar.$imp_vacio.$fecha_fin.$total_cobrar.$imp_vacio.$fecha_fin.$total_cobrar.$imp_vacio.'                                                0000000'.$tipo_cuenta.$sucursal.$nro_cuenta.'    ';
                            $cont = $cont + 1;

                            fwrite($txt, $cadena.PHP_EOL);
                        }
                    }

                }
                fwrite($txt, '                                                                         '.PHP_EOL);
                fclose($txt);
            
        }
        
        if($banco->nombre == 'Comafi')// 
        {
            //if($this->session->userdata('id_empresa') == 2) // sendero Servicios// es = 2, cambiar a 2, esta en 1 para hacer testing
           // {
                $txt= fopen('DEBAUT.txt', 'w+') or die ('Problemas al crear el archivo');
                $archivo ='DEBAUT.txt';

                $fecha_envio = $periodo_deuda->fecha_envio;
                $fecha_envio = explode("-",$fecha_envio);
                $fecha_envio = $fecha_envio[0].$fecha_envio[1].$fecha_envio[2];

                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                    $total = $key->importe_total;
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    $num_unic_x_archivo = str_pad($key->id, 8, '0', STR_PAD_LEFT);
                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 10, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 10, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }
                    $numero_cliente = str_pad($key->clientes->nro_socio, 8, '0', STR_PAD_LEFT);
                    $numero_cliente = str_pad($numero_cliente, 18, ' ', STR_PAD_RIGHT);
                    //$cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    //nro de cuenta
                    //$sucursal = $key->clientes->cuentas_bancarias_clientes->sucursal;
                    $nro_cuenta = $key->clientes->cuentas_bancarias_clientes->nro_cuenta;
                    $tipo_cuenta = $key->clientes->cuentas_bancarias_clientes->tipos_cuentas_bancarias->alias2;
                    //
                    $fecha_inicio = $periodo_deuda->fecha_inicio;
                    $fecha_inicio = explode("-",$fecha_inicio);
                    $fecha_inicio = $fecha_inicio[0].$fecha_inicio[1].$fecha_inicio[2];
                    
                    $dato_comafi = '';
                    if($this->session->userdata('id_empresa') == 2)
                    {
                        $dato_comafi = '114';
                    }
                    else
                    {
                        $dato_comafi = '111';
                    }
                    $aux_s = substr($key->clientes->cuentas_bancarias_clientes->nro_cuenta, 0, 3).'0';
                    $cadena = $fecha_envio.'00000000000000000'.'550'.$fecha_inicio.$total_cobrar.'00'.$aux_s.$nro_cuenta.'000001000'.$tipo_cuenta.'0'.$dato_comafi.'0990099000000000000SENDEROPago Cuota Mensual            '.$numero_cliente.'                                        0000000000000000000000';

                    fwrite($txt, $cadena.PHP_EOL);


                    //agregar la deuda al renglon de abajo
                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {

                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $total = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $total = $key->deuda;
                            }
                            if (is_float($total))
                            {
                                $decimales = explode(".",$total);
                                $parte_entera_rellenada = str_pad($decimales[0], 10, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($total, 10, '0', STR_PAD_LEFT);
                                $total_cobrar = $parte_entera_rellenada.'00';
                            }
                            $cadena = $fecha_envio.'00000000000000000'.'550'.$fecha_inicio.$total_cobrar.'00'.$aux_s.$nro_cuenta.'000001000'.$tipo_cuenta.'0'.$dato_comafi.'0990099000000000000SENDERODEUDA                         '.$numero_cliente.'                                        0000000000000000000000';
                            fwrite($txt, $cadena.PHP_EOL);
                        }
                    }
                }
                fwrite($txt, '                            '.PHP_EOL);
                fclose($txt);



            //}
        }

        if($banco->nombre == 'HSBC')
        {
            if($this->session->userdata('id_empresa') == 1) // sendero Salud
            {
                $txt= fopen('65127.txt', 'w+') or die ('Problemas al crear el archivo');
                $archivo ='65127.txt';
                $cont = 1;
                $sumatoria_ult_dig = 0;
                $acum = 0;
                $cont_rellenado =str_pad($cont, 7,'0',STR_PAD_LEFT);
                $cabecera = '56512730708011432000                                                                                                                                                         '.$cont_rellenado;
                
                fwrite($txt, $cabecera.PHP_EOL);
                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                    $cont = $cont + 1;
                    $total = $key->importe_total;
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    $num_unic_x_archivo = str_pad($key->id, 8, '0', STR_PAD_LEFT);
                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }
                    $numero_cliente = str_pad($key->clientes->nro_socio, 8, '0', STR_PAD_LEFT);
                    $numero_cliente = str_pad($numero_cliente, 22, ' ', STR_PAD_RIGHT);
                    //$cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    //nro de cuenta
                   
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    $entidad = substr($cbu,0,3);
                    $sucursal = substr($cbu,3,5);//incluye didigito verificador
                    $ultimo_digito = substr($cbu,18,1);
                    $nro_cuenta =substr($cbu,8);
                    //
                    $fecha_inicio = $periodo_deuda->fecha_inicio;
                    $fecha_inicio = explode("-",$fecha_inicio);
                    $fecha_inicio = $fecha_inicio[0].$fecha_inicio[1].$fecha_inicio[2];

                    $fecha_envio = $periodo_deuda->fecha_envio;
                    $fecha_envio = explode("-", $fecha_envio);
                    $fecha_envio = $fecha_envio[0].$fecha_envio[1].$fecha_envio[2];
                    
                    $periodo = $periodo_deuda->periodo;
                    $periodo = explode("-",$periodo);
                    $periodo = $periodo[1].$periodo[0];
                    $periodo = str_pad($periodo, 15, ' ', STR_PAD_RIGHT);

                    //$cont = $cont + 1;
                    $cont_rellenado =str_pad($cont, 7,'0',STR_PAD_LEFT);
                    $sumatoria_ult_dig = $sumatoria_ult_dig  + $ultimo_digito;
                    
                    $cadena = '665127001'.'SENDERO SA'.'080'.$fecha_envio.$fecha_inicio.$entidad.$sucursal.'000000'.$nro_cuenta.$total_cobrar.$periodo.$numero_cliente.'3700000                                                        '.$cont_rellenado;

                    fwrite($txt, $cadena.PHP_EOL);
                    $acum = $acum + $key->importe_total;
                    //agregar la deuda al renglon de abajo

                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {

                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $total = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $total = $key->deuda;
                            }
                            
                            if (is_float($total))
                            {
                                $decimales = explode(".",$total);
                                $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                                $total_cobrar = $parte_entera_rellenada.'00';
                            }

                            $cont = $cont + 1;
                            $cont_rellenado =str_pad($cont, 7,'0',STR_PAD_LEFT);

                            $sumatoria_ult_dig = $sumatoria_ult_dig  + $ultimo_digito;
                            $cadena = '665127001'.'SENDERO SA'.'080'.$fecha_inicio.$fecha_fin.$entidad.$sucursal.'000000'.$nro_cuenta.$total_cobrar.$periodo.$numero_cliente.'3700000                                                        '.$cont_rellenado;
                            fwrite($txt, $cadena.PHP_EOL);
                            $acum = $acum + $total;
                        }

                    }

                }
                $cont = $cont + 1;
                $cont_reg = $cont - 2;
                $cont_reg =str_pad($cont_reg, 4,'0',STR_PAD_LEFT);
                $cont_rellenado =str_pad($cont, 7,'0',STR_PAD_LEFT);
                $sumatoria_ult_dig = str_pad($sumatoria_ult_dig, 17,'0',STR_PAD_LEFT);
                $acum = str_pad($acum, 6,'0',STR_PAD_LEFT);
                $pie = '86512700'.$cont_reg.'0000'.$acum.'00'.$sumatoria_ult_dig.'                                                                                                                                    '.$cont_rellenado;
                fwrite($txt, $pie.PHP_EOL);
                fwrite($txt, '                            '.PHP_EOL);

            }
            if($this->session->userdata('id_empresa') == 2) // sendero Servicios
            {
                $txt= fopen('65127.txt', 'w+') or die ('Problemas al crear el archivo');
                $archivo ='65127.txt';
                $cont = 1;
                $cont_rellenado =str_pad($cont, 7,'0',STR_PAD_LEFT);
                $cabecera = '56512730708011432000                                                                                                                                                         '.$cont_rellenado;
                
                fwrite($txt, $cabecera.PHP_EOL);
                $acum = 0;
                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                    $cont = $cont + 1;
                    $total = $key->importe_total;
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    $num_unic_x_archivo = str_pad($key->id, 8, '0', STR_PAD_LEFT);
                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }
                    $numero_cliente = str_pad($key->clientes->nro_socio, 22, ' ', STR_PAD_RIGHT);
                    //$cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    //nro de cuenta
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    $entidad = substr($cbu,0,3);
                    $sucursal = substr($cbu,3,5);//incluye didigito verificador

                    $nro_cuenta =substr($cbu,8);
                    //
                    $fecha_inicio = $periodo_deuda->fecha_inicio;
                    $fecha_inicio = explode("-",$fecha_inicio);
                    $fecha_inicio = $fecha_inicio[0].$fecha_inicio[1].$fecha_inicio[2];

                    $fecha_fin = $periodo_deuda->fecha_fin;
                    $fecha_fin = explode("-", $fecha_fin);
                    $fecha_fin = $fecha_fin[0].$fecha_fin[1].$fecha_fin[2];
                    
                    $periodo = $periodo_deuda->periodo;
                    $periodo = explode("-",$periodo);
                    $periodo = $periodo[1].$periodo[0];
                    $periodo = str_pad($periodo, 15, ' ', STR_PAD_RIGHT);

                    //$cont = $cont + 1;
                    $cont_rellenado =str_pad($cont, 7,'0',STR_PAD_LEFT);
                    
                    $cadena = '665127001'.'SENDERO SA'.'080'.$fecha_inicio.$fecha_fin.$entidad.$sucursal.'000'.$nro_cuenta.$total_cobrar.$periodo.$numero_cliente.'3700000                                                        '.$cont_rellenado;

                    fwrite($txt, $cadena.PHP_EOL);
                    $acum = $acum + $key->importe_total;
                    //agregar la deuda al renglon de abajo

                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {

                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $total = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $total = $key->deuda;
                            }
                            
                            if (is_float($total))
                            {
                                $decimales = explode(".",$total);
                                $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                                $total_cobrar = $parte_entera_rellenada.'00';
                            }

                            $cont = $cont + 1;
                            $cont_rellenado =str_pad($cont, 7,'0',STR_PAD_LEFT);
                            
                            $cadena = '665127001'.'SENDERO SA'.'080'.$fecha_inicio.$fecha_fin.$entidad.$sucursal.'000'.$nro_cuenta.$total_cobrar.$periodo.$numero_cliente.'3700000                                                        '.$cont_rellenado;
                            fwrite($txt, $cadena.PHP_EOL);
                            $acum = $acum + $total;
                        }

                    }

                }
                $cont = $cont + 1;
                $cont_reg = $cont -2;
                $cont_reg =str_pad($cont_reg, 4,'0',STR_PAD_LEFT);
                $cont_rellenado =str_pad($cont, 7,'0',STR_PAD_LEFT);
                $acum = str_pad($acum, 6,'0',STR_PAD_LEFT);
                $pie = '86512700'.$cont_reg.'0000'.$acum.'00'.$sumatoria_ult_dig.'                                                                                                                                    '.$cont_rellenado;
                fwrite($txt, $pie.PHP_EOL);
                fwrite($txt, '                            '.PHP_EOL);

            }
        }
        if($banco->nombre == 'SANTANDER')
        {
            if($this->session->userdata('id_empresa') == 1) // sendero Salud = 2/ EN EL TESTING SE UTILIZARA = 1
            {
                $txt= fopen('DEBITOS.txt', 'w+') or die ('Problemas al crear el archivo');
                $archivo ='DEBITOS.txt';
                
                $cont = 0;
                $acum = 0;
                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                   
                    $total = $key->importe_total;
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    $num_unic_x_archivo = str_pad($key->id, 8, '0', STR_PAD_LEFT);
                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 14, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 14, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }
                    $numero_cliente = str_pad($key->clientes->nro_socio, 6, '0', STR_PAD_LEFT);
                    //$cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    //nro de cuenta
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    //rellenar cbu en la parte de la cuenta
                    $ente = substr($cbu,0,3);
                    $sucursal = substr($cbu, 3,4);
                    $primer_digito_verificador = substr($cbu, 7, 1);
                    $nro_de_cuenta = substr($cbu, 8);//no incluye primer digito verificador si el segundo
                    $cbu_rellenado = $ente.$sucursal.'000'.$nro_de_cuenta;

                    //
                    $fecha_inicio = $periodo_deuda->fecha_inicio;
                    $fecha_inicio = explode("-",$fecha_inicio);
                    $fecha_inicio = $fecha_inicio[0].$fecha_inicio[1].$fecha_inicio[2];

                    $fecha_envio = $periodo_deuda->fecha_envio;
                    $fecha_envio = substr($fecha_envio, 2);
                    
                    $fecha_envio2 = $periodo_deuda->fecha_envio;
                    $fecha_envio2 = explode("-", $fecha_envio2);
                    $fecha_envio2 = $fecha_envio2[0].$fecha_envio2[1].$fecha_envio2[2];

                    $cadena = '30708011432DEBITO    370000007200010'.$cbu_rellenado.$numero_cliente.'                               6'.$fecha_inicio.'                                             '.$total_cobrar.$fecha_envio.'                                                                                                                                                                                                                                      '.$fecha_envio2.'1030                                                              SENDERO SA                                                                                         '.$fecha_envio2.'101215        ';
                    
                   

                    fwrite($txt, $cadena.PHP_EOL);
                    $cont++;
                    $acum = $acum + $key->importe_total;
                    //agregar la deuda al renglon de abajo

                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {

                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $total = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $total = $key->deuda;
                            }
                            
                            if (is_float($total))
                            {
                                $decimales = explode(".",$total);
                                $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                                $total_cobrar = $parte_entera_rellenada.'00';
                            }

                            
                            
                            $cadena = '30708011432DEBITO    370000007200010'.$cbu_rellenado.$numero_cliente.'                               6'.$fecha_inicio.'                                             '.$total_cobrar.$fecha_envio.'                                                                                                                                                                                                                                      '.$fecha_envio2.'1030                                                              SENDERO SA                                                                                         '.$fecha_envio2.'101215        ';
                            fwrite($txt, $cadena.PHP_EOL);
                            $cont++;
                            $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                            $acum = $acum + $total;

                        }

                    }



                }
                $acum = str_pad($acum, 6, '0', STR_PAD_LEFT);
                $pie = '999999999999999999999       '.$fecha_envio2.'100000     000000000000000000000000000000000000000000000000000000000000'.$cont.'0000000000'.$acum.'00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000152                                                                                                                                                '.$fecha_envio2.'112025        ';
                //$pie = str_pad($pie, 601, ' ', STR_PAD_RIGHT);
                fwrite($txt, $pie.PHP_EOL);
                fwrite($txt, '                                                          '.PHP_EOL);

            }
            if($this->session->userdata('id_empresa') == 2) // sendero Servicios = 2/ EN EL TESTING SE UTILIZARA = 1
            {
                $txt= fopen('DEBITOS.txt', 'w+') or die ('Problemas al crear el archivo');
                $archivo ='DEBITOS.txt';
                
                $cont = 0;
                $acum = 0;
                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                   
                    $total = $key->importe_total;
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    $num_unic_x_archivo = str_pad($key->id, 8, '0', STR_PAD_LEFT);
                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 14, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 14, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }
                    $numero_cliente = str_pad($key->clientes->nro_socio, 6, '0', STR_PAD_LEFT);
                    //$cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    //nro de cuenta
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    //rellenar cbu en la parte de la cuenta
                    $ente = substr($cbu,0,3);
                    $sucursal = substr($cbu, 3,4);
                    $primer_digito_verificador = substr($cbu, 7, 1);
                    $nro_de_cuenta = substr($cbu, 8);//no incluye primer digito verificador si el segundo
                    $cbu_rellenado = $ente.$sucursal.'000'.$nro_de_cuenta;

                    //
                    $fecha_inicio = $periodo_deuda->fecha_inicio;
                    $fecha_inicio = explode("-",$fecha_inicio);
                    $fecha_inicio = $fecha_inicio[0].$fecha_inicio[1].$fecha_inicio[2];

                    $fecha_envio = $periodo_deuda->fecha_envio;
                    $fecha_envio = substr($fecha_envio, 2);
                    
                    $fecha_envio2 = $periodo_deuda->fecha_envio;
                    $fecha_envio2 = explode("-", $fecha_envio2);
                    $fecha_envio2 = $fecha_envio2[0].$fecha_envio2[1].$fecha_envio2[2];

                    $cadena = '30708011432DEBITO    370000007200010'.'0'.$cbu_rellenado.$numero_cliente.'                               6'.$fecha_inicio.'                                             '.$total_cobrar.$fecha_envio.'                                                                                                                                                                                                                                      '.$fecha_envio2.'1030                                                              SENDERO SA                                                                                         '.$fecha_envio2.'101215        ';
                    
                   

                    fwrite($txt, $cadena.PHP_EOL);
                    $cont++;
                    $acum = $acum + $key->importe_total;

                    //agregar la deuda al renglon de abajo

                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {

                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $total = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $total = $key->deuda;
                            }
                            
                            if (is_float($total))
                            {
                                $decimales = explode(".",$total);
                                $parte_entera_rellenada = str_pad($decimales[0], 8, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($total, 8, '0', STR_PAD_LEFT);
                                $total_cobrar = $parte_entera_rellenada.'00';
                            }

                            
                            
                            $cadena = '30708011432DEBITO    370000007200010'.'0'.$cbu_rellenado.$numero_cliente.'                               6'.$fecha_inicio.'                                             '.$total_cobrar.$fecha_envio.'                                                                                                                                                                                                                                      '.$fecha_envio2.'1030                                                              SENDERO SA                                                                                         '.$fecha_envio2.'101215        ';
                            fwrite($txt, $cadena.PHP_EOL);
                            $cont++;
                            $cont = str_pad($cont, 4, '0', STR_PAD_LEFT);
                            $acum = $acum + $total;

                        }

                    }



                }
                $pie = '999999999999999999999       '.$fecha_envio2.'100000     000000000000000000000000000000000000000000000000000000000000'.$cont.'0000000000'.$acum.'00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000152                                                                                                                                                '.$fecha_envio2.'112025        ';
                fwrite($txt, $pie.PHP_EOL);
                fwrite($txt, '                                                          '.PHP_EOL);
            }
        }
        if($banco->nombre == 'Frances')
        {
            if($this->session->userdata('id_empresa') == 1) // sendero Salud
            {
                $txt= fopen('frances.txt', 'w+') or die ('Problemas al crear el archivo');
                $archivo ='frances.txt';
                $acum = 0;
                $fecha_inicio = $periodo_deuda->fecha_inicio;
                $fecha_inicio = explode("-",$fecha_inicio);
                $fecha_inicio = $fecha_inicio[0].$fecha_inicio[1].$fecha_inicio[2];

                $fecha_envio = $periodo_deuda->fecha_envio;
                $fecha_envio = explode("-",$fecha_envio);
                $fecha_envio = $fecha_envio[0].$fecha_envio[1].$fecha_envio[2];

                $cabecera = '411019485'.$fecha_envio.$fecha_inicio.'0017048702010000xxxxCUOTAS    ARS020080227.txtXxxxxxxx Xxx SENDERO';
                fwrite($txt, $cabecera.PHP_EOL);
                $fecha_fin = $periodo_deuda->fecha_fin;
                $fecha_fin = explode("-",$fecha_fin);
                $fecha_fin = $fecha_fin[0].$fecha_fin[1].$fecha_fin[2];

                $periodo = $periodo_deuda->periodo;
                $periodo = explode("-",$periodo);
                $periodo = $periodo[1].'-'.$periodo[0];
                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                    
                    $total = $key->importe_total;
                    $acum = $acum + $key->importe_total;
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    $num_unic_x_archivo = str_pad($key->id, 13, '0', STR_PAD_LEFT);
                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 13, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 13, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }

                    $numero_cliente = str_pad($key->clientes->nro_socio, 8, '0', STR_PAD_LEFT);
                    
                    $dni = str_pad($key->clientes->users_groups->users->documento, 11, '0', STR_PAD_LEFT);
                    $cadena = '421019485  0000250043            0'.$cbu.$total_cobrar.'      0000000000000000000000'.$fecha_fin.'  000000000000000';
                   
                    fwrite($txt, $cadena.PHP_EOL);
                    $cadena2 = '422019485  0000250043            '.$numero_cliente.'                     ';
                    fwrite($txt, $cadena2.PHP_EOL);
                    $cadena3 = '423019485  0000250043                                                              ';
                    fwrite($txt, $cadena3.PHP_EOL);
                    $cadena4 = '424019485  0000250043            CUOTA';
                    fwrite($txt, $cadena4.PHP_EOL);
                    //agregar la deuda al renglon de abajo
                    

                  
                } 
                if (is_float($acum))
                    {
                        $decimales = explode(".",$acum);
                        $parte_entera_rellenada = str_pad($decimales[0], 13, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_acumulado = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($acum, 13, '0', STR_PAD_LEFT);
                        $total_acumulado = $parte_entera_rellenada.'00';
                    }
                $pie ='491019485'.$total_acumulado.'00000001000000006';
                fwrite($txt, $pie.PHP_EOL); 
                fwrite($txt, '                                                                         '.PHP_EOL);           
                fclose($txt);
            }
            if($this->session->userdata('id_empresa') == 2) // sendero Servicio
            {
                $txt= fopen('frances.txt', 'w+') or die ('Problemas al crear el archivo');
                $archivo ='frances.txt';
                $acum = 0;
                $fecha_inicio = $periodo_deuda->fecha_inicio;
                $fecha_inicio = explode("-",$fecha_inicio);
                $fecha_inicio = $fecha_inicio[0].$fecha_inicio[1].$fecha_inicio[2];

                $fecha_envio = $periodo_deuda->fecha_envio;
                $fecha_envio = explode("-",$fecha_envio);
                $fecha_envio = $fecha_envio[0].$fecha_envio[1].$fecha_envio[2];

                $cabecera = '411019485'.$fecha_envio.$fecha_inicio.'0017048702010000xxxxCUOTAS    ARS020080227.txtXxxxxxxx Xxx SENDERO';
                fwrite($txt, $cabecera.PHP_EOL);
                $fecha_fin = $periodo_deuda->fecha_fin;
                $fecha_fin = explode("-",$fecha_fin);
                $fecha_fin = $fecha_fin[0].$fecha_fin[1].$fecha_fin[2];

                $periodo = $periodo_deuda->periodo;
                $periodo = explode("-",$periodo);
                $periodo = $periodo[1].'-'.$periodo[0];
                foreach ($periodo_deuda->items_periodo_deuda as $key ) 
                {
                    
                    $total = $key->importe_total;
                    $acum = $acum + $key->importe_total;
                    $cbu=$key->clientes->cuentas_bancarias_clientes->cbu;
                    $num_unic_x_archivo = str_pad($key->id, 13, '0', STR_PAD_LEFT);
                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 13, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 13, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    }

                    $numero_cliente = str_pad($key->clientes->nro_socio, 8, '0', STR_PAD_LEFT);
                    
                    $dni = str_pad($key->clientes->users_groups->users->documento, 11, '0', STR_PAD_LEFT);
                    $cadena = '421019485  0000250043            0'.$cbu.$total_cobrar.'      0000000000000000000000'.$fecha_fin.'  000000000000000';
                   
                    fwrite($txt, $cadena.PHP_EOL);
                    $cadena2 = '422019485  0000250043            '.$numero_cliente.'                     ';
                    fwrite($txt, $cadena2.PHP_EOL);
                    $cadena3 = '423019485  0000250043                                                              ';
                    fwrite($txt, $cadena3.PHP_EOL);
                    $cadena4 = '424019485  0000250043            CUOTA';
                    fwrite($txt, $cadena4.PHP_EOL);
                    //agregar la deuda al renglon de abajo
                    

                  
                } 
                if (is_float($acum))
                    {
                        $decimales = explode(".",$acum);
                        $parte_entera_rellenada = str_pad($decimales[0], 13, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_acumulado = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($acum, 13, '0', STR_PAD_LEFT);
                        $total_acumulado = $parte_entera_rellenada.'00';
                    }
                $pie ='491019485'.$total_acumulado.'00000001000000006';
                fwrite($txt, $pie.PHP_EOL); 
                fwrite($txt, '                                                                         '.PHP_EOL);           
                fclose($txt);
            }
        }

        header ("Content-Disposition: attachment; filename=".$archivo); 
        header ("Content-Type: application/octet-stream"); 
        header ("Content-Length: ".filesize($archivo)); 
        readfile($archivo); 
        return;

    }
    
    public function items_periodo_deuda($id)
    {
        $data['periodo_deuda']= Periodos_deudas_eloquent::find($id);
        $data['contenido'] = "/cobros/periodos_deudas/items_periodo_deuda";
            $this->load->view('templates/template_admin', $data);
    }
    public function items_periodo_deuda_banco($id)
    {
        $data['periodo_deuda']= Periodos_deudas_eloquent::find($id);
        $data['contenido'] = "/cobros/periodos_deudas/items_periodo_deuda_banco";
            $this->load->view('templates/template_admin', $data);
    }
    public function listar_items_periodo_deuda_banco($id)
    {
          //$id_periodo_deuda = $_POST['id_banco'];
      //  echo 'ID BANCO: '.$id_banco;
        $list = $this->Model_periodos_deudas->get_datatables($id);
        $data = array();
        $no = $_POST['start'];
        $periodo_deuda = Periodos_deudas_eloquent::find($id);
        foreach ($list as $person)
        {
             
            $no++;
            $row = array();
            $row[] = $person->first_name;
            $row[] = $person->last_name;
            $row[] = $person->importe_total;
            $row[] = $person->importe_total_recibido;
            $row[] = '
        
         <a class="btn btn-sm btn-success" onclick="ver_detalles('.$person->id_items_periodo_deuda.')" title="Ver Detalles"><i class="fa fa-list"></i> </a>
         <a class="btn btn-sm btn-danger" onclick="ver_movimientos('.$periodo_deuda->id.','.$person->cliente_id.')" title="Ver Movimientos"><i class="fa fa-list"></i> </a>';
            $data[] = $row;
            //


            //
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Model_periodos_deudas->count_all($id),
                        "recordsFiltered" => $this->Model_periodos_deudas->count_filtered($id),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
    public function listar_items_periodo_deuda($id)
    {
        
        $periodo_deuda = Periodos_deudas_eloquent::find($id);
        $data['listado']= $periodo_deuda;
        if($periodo_deuda->adm_pub > 0)
        {
            $this->load->view('/cobros/periodos_deudas/listar_items_periodo_deuda_adm_pub', $data);
        }
        else
        {
            $this->load->view('/cobros/periodos_deudas/listar_items_periodo_deuda', $data);
        }
        
    }
    public function listar_items_periodo_deuda_adm_pub($id)
    {
        $data['listado']= Periodos_deudas_eloquent::find($id);
        $this->load->view('/cobros/periodos_deudas/listar_items_periodo_deuda_adm_pub', $data);
    }
	public function detalle_periodo_cliente()
    {
        $id = $this->input->get('id');
        //echo $id;
        $data['items_periodo_deuda'] = Items_periodo_deuda_eloquent::find($id);

        $this->load->view('/cobros/periodos_deudas/detalle_periodo_cliente', $data);
             
    }
    public function detalle_pago_periodo_movimiento_cliente()
    {
        $id = $this->input->get('id');
        $id_cliente = $this->input->get('id_cliente');

        //echo $id;
        $data['listado'] = Items_periodo_pago_parcial_eloquent::where('id_periodo_deuda',$id)
                                                              ->where('id_cliente',$id_cliente)
                                                              ->where('estado','=',1)
                                                              ->get();

        $this->load->view('/cobros/periodos_deudas/detalle_pago_periodo_movimiento_cliente', $data);
             
    }
    public function validar_periodo()
    {
        $periodo = $this->input->post('periodo');
        $id_empresa = $this->session->userdata('id_empresa');
        $tipo_cobro = $this->input->post('tipo_cobro');
        $periodo_seleccionado = $this->input->post('periodo');
                    //Procesamiento de fecha y hora
                    if($periodo_seleccionado != '')
                    {

                      //$periodo = str_replace('/', '-', $periodo);
                      $separa = explode("-",$periodo_seleccionado);
                      $mes = $separa[0];
                      //$dia = $separa[0];
                      $anio = $separa[1];
                      
                      $periodo_seleccionado = $mes.'-'.$anio;
                    }
        if($tipo_cobro == 'banco')
        {
            $id_banco = $this->input->post('id_banco');
            $periodo = Periodos_deudas_eloquent::where('id_empresa','=',$id_empresa)->where('id_banco','=',$id_banco)->where('periodo','=',$periodo_seleccionado)->where('estado','=',1)->first();
            if($periodo != NULL)
            {
                //existe
                echo json_encode(array("status" => TRUE));
            }
            else
            {
                //no existe
                echo json_encode(array("status" => FALSE));
            }

        }
        else if($tipo_cobro == 'tarjeta')
        {
            $id_tarjeta_de_credito = $this->input->post('id_tarjeta_de_credito');
            $id_tipo_tarjeta = $this->input->post('id_tipo_tarjeta');
            $periodo = Periodos_deudas_eloquent::where('id_empresa','=',$id_empresa)->where('id_tarjeta','=',$id_tarjeta_de_credito)->where('periodo','=',$periodo_seleccionado)->where('id_tipo_tarjeta','=',$id_tipo_tarjeta)->first();
            if($periodo != NULL)
            {
                //existe
                echo json_encode(array("status" => TRUE));
            }
            else
            {
                //no existe
                echo json_encode(array("status" => FALSE));
            }
        }
    }
    public function validar_periodo_adm_pub()
    {
        $periodo = $this->input->post('periodo');
        $id_empresa = $this->session->userdata('id_empresa');
        $periodo_seleccionado = $this->input->post('periodo');
        $tipo_cobro = $this->input->post('tipo_cobro');
                    //Procesamiento de fecha y hora
                    if($periodo_seleccionado != '')
                    {

                      //$periodo = str_replace('/', '-', $periodo);
                      $separa = explode("-",$periodo_seleccionado);
                      $mes = $separa[0];
                      //$dia = $separa[0];
                      $anio = $separa[1];
                      
                      $periodo_seleccionado = $mes.'-'.$anio;
                    }
        $periodo = Periodos_deudas_eloquent::where('id_empresa','=',$id_empresa)->where('adm_pub','=',1)->where('periodo','=',$periodo_seleccionado)->where('tipo_cobro_adm_pub','=',$tipo_cobro)->where('estado','=',1)->first();
        if($periodo != NULL)
            {
                //existe
                echo json_encode(array("status" => TRUE));
            }
            else
            {
                //no existe
                echo json_encode(array("status" => FALSE));
            }

    }
    public function procesar_cobros_entidad()
    {
                    $this->load->helper(array('form', 'url'));
                    $config['upload_path'] = './uploads/';
                    $config['allowed_types'] = '*';
                    $config['max_size']    = '30000';
                    $this->load->library('upload', $config);
                    //$this->upload->initialize($config);

        if (!empty($_FILES['archivo']['name']))
        {
            $nombre_file = $_FILES['archivo']['name'];
            $ruta = "uploads/".$nombre_file;
            //$ruta = base_url('uploads/'.$nombre_file);
            if (file_exists($ruta)) 
            {
                    unlink($ruta);
            }
        }
        if(!$this->upload->do_upload('archivo'))
          {
           /* error en la subida del archivo */

           /* obtenemos el error en un array */
           $error = array('error' => $this->upload->display_errors());

           /* cargamos la vista inicial pero ya con el array de errores lleno */
           //$file = $_FILES['archivo']['name'];
           //var_dump($_POST);
           //var_dump($_FILES);
           print_r($error);
           
          }
          else
          {
           /* éxito en la subida del archivo */

           
            $data = array('upload_data' => $this->upload->data());
                                /* obtenemos los datos del upload del archivo en un array, lo 
                                  pasamos a la vista form_success */
            //var_dump($_POST);
            //var_dump($_FILES);
            $nombre_file = $_FILES['archivo']['name'];
            /*$file = fopen($nombre_file, "r");
                while(!feof($file)) {
                echo fgets($file). "<br />";
                }
            fclose($file);*/
            //procesar la deuda

            $periodo_deuda = Periodos_deudas_eloquent::find($_POST['id_periodo_deuda']);
            //print_r($periodo_deuda->id);
            //preguntar si es de un banco o una tarjeta

            if($periodo_deuda->id_banco > 0)
            {
                $banco = Bancos_eloquent::find($periodo_deuda->id_banco);
                
                if($banco->nombre == 'HSBC')
                {
                    $this->procesar_pagos_libreria->banco_hsbc($periodo_deuda->id,$nombre_file,$this->session->userdata('id_empresa'));
                }
                else if($banco->nombre == 'Patagonia')
                {
                    $this->procesar_pagos_libreria->banco_patagonia($periodo_deuda->id,$nombre_file,$this->session->userdata('id_empresa'));   
                }
                else if($banco->nombre == 'Comafi')
                {
                    $this->procesar_pagos_libreria->banco_comafi($periodo_deuda->id,$nombre_file,$this->session->userdata('id_empresa'));   
                }
                else if($banco->nombre == 'San Juan')
                {

                    $this->procesar_pagos_libreria->banco_san_juan($periodo_deuda->id,$nombre_file,$this->session->userdata('id_empresa'));   
                }
                else if($banco->nombre == 'Credicoop')
                {
                    $this->procesar_pagos_libreria->banco_credicoop($periodo_deuda->id,$nombre_file,$this->session->userdata('id_empresa'));   
                }
                else if($banco->nombre == 'Supervielle')
                {
                    $this->procesar_pagos_libreria->banco_supervielle($periodo_deuda->id,$nombre_file,$this->session->userdata('id_empresa'));   
                }
                else if($banco->nombre == 'SANTANDER')
                {
                    $this->procesar_pagos_libreria->banco_santander($periodo_deuda->id,$nombre_file,$this->session->userdata('id_empresa'));   
                }
                else if($banco->nombre == 'Nacion')
                {
                    $this->procesar_pagos_libreria->banco_nacion($periodo_deuda->id,$nombre_file,$this->session->userdata('id_empresa'));   
                }
                else if($banco->nombre == 'Macro')
                {
                    $this->procesar_pagos_libreria->banco_macro($periodo_deuda->id,$nombre_file,$this->session->userdata('id_empresa'));   
                }
                
                //$this->procesar_pagos_libreria->banco_san_juan($periodo_deuda->id,$nombre_file);

            }
            if($periodo_deuda->id_tarjeta > 0)
            {
                $tarjeta = Tarjetas_eloquent::find($periodo_deuda->id_tarjeta);
                
                if($tarjeta->nombre == 'Nevada')
                {
                    $this->procesar_pagos_libreria->tarjeta_nevada($periodo_deuda->id,$nombre_file,$this->session->userdata('id_empresa'));
                }
                else if($tarjeta->nombre == 'Fiel')
                {
                    $this->procesar_pagos_libreria->tarjeta_fiel($periodo_deuda->id,$nombre_file,$this->session->userdata('id_empresa'));
                }
                else if($tarjeta->nombre == 'Visa')
                {
                    if($periodo_deuda->tipos_tarjetas->nombre == 'Credito')
                    {
                        $this->procesar_pagos_libreria->tarjeta_visa_credito($periodo_deuda->id,$nombre_file,$this->session->userdata('id_empresa'));
                    }
                    else if($periodo_deuda->tipos_tarjetas->nombre == 'Debito')
                    {
                        $this->procesar_pagos_libreria->tarjeta_visa_debito($periodo_deuda->id,$nombre_file,$this->session->userdata('id_empresa'));
                    }
                }

               
            }
            if($periodo_deuda->adm_pub > 0)
            {
                $this->procesar_pagos_libreria->adm_pub($periodo_deuda->id,$nombre_file,$this->session->userdata('id_empresa'));
            }
            /*echo json_encode(
                    array(
                        "status" => TRUE, 
                        "mensaje" => "Exito",
                        "archivo" => $data
                    ));*/
          }

                    
        
    }

    //PROCESOS DE ADMINISTRACION PUBLICA

    public function guardar_periodo_deuda_admin_publica()
    {
        //echo "ok";
       // echo "llego";
        $cont_registros = 0;
        $cont_registros_sup = 0;
        $cont_registros_sgr = 0;
        $cont_registros_lis = 0;
        $periodo_deuda = new Periodos_deudas_eloquent();
        $periodo_deuda->tope_deuda = $this->input->post('tope_deuda');
        $periodo = $this->input->post('periodo');
                    //Procesamiento de fecha y hora
                    if($periodo != '')
                    {

                      //$periodo = str_replace('/', '-', $periodo);
                      $separa = explode("-",$periodo);
                      $mes = $separa[0];
                      //$dia = $separa[0];
                      $anio = $separa[1];
                      
                      $periodo = $mes.'-'.$anio;
                    }
        /*echo $periodo;
        die();*/
        $periodo_deuda->periodo = $periodo;

        $fecha_inicio = $this->input->post('fecha_inicio');
                    //Procesamiento de fecha y hora
                    if($fecha_inicio != '')
                    {

                      $fecha_inicio = str_replace('/', '-', $fecha_inicio);
                      $separa = explode("-",$fecha_inicio);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha_inicio = $anio.'-'.$mes.'-'.$dia;
                    }
      //  $periodo_deuda->fecha_inicio = $fecha_inicio;

        $periodo_date = date_create_from_format('Y-m-d', $anio.'-'.$mes.'-01');

        $fecha_fin = $this->input->post('fecha_fin');
                    //Procesamiento de fecha y hora
                    if($fecha_fin != '')
                    {

                      $fecha_fin = str_replace('/', '-', $fecha_fin);
                      $separa = explode("-",$fecha_fin);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha_fin = $anio.'-'.$mes.'-'.$dia;
                    }
     //   $periodo_deuda->fecha_fin = $fecha_fin;

        $fecha_envio = $this->input->post('fecha_envio');
                    //Procesamiento de fecha y hora
                    if($fecha_envio != '')
                    {

                      $fecha_envio = str_replace('/', '-', $fecha_envio);
                      $separa = explode("-",$fecha_envio);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha_envio = $anio.'-'.$mes.'-'.$dia;
                    }
        $periodo_deuda->fecha_envio = $fecha_envio;
        $periodo_deuda->id_empresa = $this->session->userdata('id_empresa');

        $periodo_deuda->estado = 1;
        $tipo_cobro = $this->input->post('tipo_cobro');
        $clientes = $this->Model_clientes->get_id_clientes_admin_publica($this->session->userdata('id_empresa'));
       // $tipo_cobro = $this->input->post('tipo_cobro');
        /*$clientes = Clientes_eloquent::empresa($this->session->userdata('id_empresa'))
                                    ->where('id_cliente_admin_publica','!=','NULL')
                                    ->get();*/
        
            //$id_banco = $this->input->post('id_banco');
            //$id_tipo_cuenta_bancaria = $this->input->post('id_tipo_cuenta_bancaria');
        if($tipo_cobro == 'cc')
        {
            $periodo_deuda->id_banco = 0;
            //$periodo_deuda->id_tipo_cuenta_bancaria = $id_tipo_cuenta_bancaria;
            $periodo_deuda->id_tarjeta = 0;
            $periodo_deuda->id_tipo_tarjeta = 0;
            $periodo_deuda->saldo_anterior = 0;
            $periodo_deuda->adm_pub = 1;
            $periodo_deuda->tipo_cobro_adm_pub = 'cc';
            $periodo_deuda->save();
            
            foreach ($clientes as $clie) 
            {
                $cliente = Clientes_eloquent::find($clie->id);
            
                if($cliente->id_cliente_admin_publica != NULL)
                {
                    if(($cliente->clientes_admin_publica->sist == 'SGR') || ($cliente->clientes_admin_publica->sist == 'SUP') )
                    {
                        
                            
                            //recorrer anexos para saber cual esta activo
                            $contador_anexos_activos = 0;
                            foreach ($cliente->solicitudes->anexos as $anexo) 
                                {
                                    $f_anex_vig_aux = date_create_from_format('Y-m-d', $anexo->fecha_vigencia);
                                    $f_anex_vig_aux = date_format($f_anex_vig_aux,"Y-m-d");
                                    $periodo_date_aux = date_format($periodo_date,"Y-m-d");
                                    if(($anexo->estado == 1) && ($f_anex_vig_aux <= $periodo_date_aux))
                                    {
                                        $contador_anexos_activos++;
                                    }
                                    else if($anexo->estado == 0) 
                                    {
                                        if($anexo->fecha_baja != NULL)
                                        {
                                            $f_anex_baja_aux = date_create_from_format('Y-m-d', $anexo->fecha_baja);
                                            $f_anex_baja_aux = date_format($f_anex_baja_aux,"Y-m-d");
                                            $periodo_date_aux = date_format($periodo_date,"Y-m-d");
                                                
                                            if(($f_anex_baja_aux > $periodo_date_aux)&&($f_anex_vig_aux != $f_anex_baja_aux))
                                            {
                                                $contador_anexos_activos++;
                                            }
                                        }
                                    }
                                }
                            if($contador_anexos_activos > 0)//hay al menos 1 anexo activo
                            {
                                $acum_importes = 0;
                                $items_periodo_deuda = new Items_periodo_deuda_eloquent();
                                $items_periodo_deuda->id_periodo_deuda = $periodo_deuda->id;
                                $items_periodo_deuda->id_cliente = $cliente->id;
                                $items_periodo_deuda->estado = 1;
                                $items_periodo_deuda->save();
                                $cont_registros++;
                                if(($cliente->clientes_admin_publica->sist != 'SUP') && $cliente->clientes_admin_publica->sist != 'SGR')
                                {
                                    $cont_registros_lis++;
                                }
                                
                                //recorrer anexos
                                //obtener anexos
                                foreach ($cliente->solicitudes->anexos as $anexo) 
                                    {
                                        $f_anex_vig_aux = date_create_from_format('Y-m-d', $anexo->fecha_vigencia);
                                        $f_anex_vig_aux = date_format($f_anex_vig_aux,"Y-m-d");
                                        $periodo_date_aux = date_format($periodo_date,"Y-m-d");
                                        if(($anexo->estado == 1) && ($f_anex_vig_aux <= $periodo_date_aux))
                                            
                                        {
                                            $items_periodo_deuda_detalle = new Items_periodo_deuda_detalle_eloquent();
                                            $items_periodo_deuda_detalle->id_items_periodo_deuda = $items_periodo_deuda->id;
                                            $items_periodo_deuda_detalle->id_solicitud = $cliente->solicitudes->id;
                                            $items_periodo_deuda_detalle->id_anexo = $anexo->id;
                                            $items_periodo_deuda_detalle->importe = $anexo->importe;
                                            $items_periodo_deuda_detalle->estado = 1;
                                            //buscar el producto contratados en la solicitud
                                            $producto_solicitud = Productos_solicitud_eloquent::where('id_solicitud', $cliente->solicitudes->id)->where('id_anexo',$anexo->id)->first();
                                            $items_periodo_deuda_detalle->id_producto = $producto_solicitud->id_producto;
                                            $items_periodo_deuda_detalle->save();

                                            $acum_importes = $acum_importes +$anexo->importe;
                                        }
                                        else if($anexo->estado == 0) 
                                        {
                                            if($anexo->fecha_baja != NULL)
                                            {   
                                                $f_anex_baja_aux = date_create_from_format('Y-m-d', $anexo->fecha_baja);
                                                $f_anex_baja_aux = date_format($f_anex_baja_aux,"Y-m-d");
                                                $periodo_date_aux = date_format($periodo_date,"Y-m-d");
                                                /*if($anexo->id == 81277)
                                                    {
                                                        echo "periodo baja: ";
                                                        print_r($f_anex_baja_aux);
                                                        echo "periodo cobrar: ";
                                                        print_r($periodo_date_aux);
                                                        die();
                                                    }*/
                                                if(($f_anex_baja_aux > $periodo_date_aux)&&($f_anex_vig_aux != $f_anex_baja_aux))
                                                {
                                                    
                                                    $items_periodo_deuda_detalle = new Items_periodo_deuda_detalle_eloquent();
                                                    $items_periodo_deuda_detalle->id_items_periodo_deuda = $items_periodo_deuda->id;
                                                    $items_periodo_deuda_detalle->id_solicitud = $cliente->solicitudes->id;
                                                    $items_periodo_deuda_detalle->id_anexo = $anexo->id;
                                                    $items_periodo_deuda_detalle->importe = $anexo->importe;
                                                    $items_periodo_deuda_detalle->estado = 1;
                                                    //buscar el producto contratados en la solicitud
                                                    $producto_solicitud = Productos_solicitud_eloquent::where('id_solicitud', $cliente->solicitudes->id)->where('id_anexo',$anexo->id)->first();
                                                    $items_periodo_deuda_detalle->id_producto = $producto_solicitud->id_producto;
                                                    $items_periodo_deuda_detalle->save();

                                                    $acum_importes = $acum_importes +$anexo->importe;
                                                }
                                            }
                                        }    
                                            
                                    }
                                $items_periodo_deuda->deuda = $cliente->cuentas_corrientes->saldo;
                                $items_periodo_deuda->importe_total = $acum_importes;
                                $items_periodo_deuda->save();

                                //items cuenta corriente
                                $items_cuenta_corriente = new Items_cuenta_corriente_eloquent();
                                $items_cuenta_corriente->id_cuenta_corriente = $cliente->cuentas_corrientes->id;
                                $items_cuenta_corriente->id_cliente = $cliente->id;
                                $items_cuenta_corriente->importe = $acum_importes;
                                $items_cuenta_corriente->periodo = $this->input->post('periodo');
                                $items_cuenta_corriente->id_items_periodo_deuda = $items_periodo_deuda->id;
                                $items_cuenta_corriente->importe_recibo = 0;
                                $items_cuenta_corriente->deuda_recibida = 0;
                                $items_cuenta_corriente->estado = 1;
                                $items_cuenta_corriente->save();

                                 //actualizar saldo cuenta corriente
                                $saldo_anterior = $cliente->cuentas_corrientes->saldo;
                                $periodo_deuda->saldo_anterior = $saldo_anterior;
                                $cliente->cuentas_corrientes->saldo = $acum_importes + $saldo_anterior;
                                $cliente->cuentas_corrientes->save();

                                //agrego el salgo al items cuenta corriente
                                $items_cuenta_corriente->saldo = $cliente->cuentas_corrientes->saldo;
                                $items_cuenta_corriente->save();
                                $periodo_deuda->save();

                                //

                            }
                        
                    }
                        
                    
                }

            }
        }
        if($tipo_cobro == 'listado')
        {
            $periodo_deuda->id_banco = 0;
            //$periodo_deuda->id_tipo_cuenta_bancaria = $id_tipo_cuenta_bancaria;
            $periodo_deuda->id_tarjeta = 0;
            $periodo_deuda->id_tipo_tarjeta = 0;
            $periodo_deuda->saldo_anterior = 0;
            $periodo_deuda->adm_pub = 1;
            $periodo_deuda->tipo_cobro_adm_pub = 'listado';
            $periodo_deuda->save();
            foreach ($clientes as $clie) 
            {
                $cliente = Clientes_eloquent::find($clie->id);
            //foreach ($clientes as $cliente) 
            //{
                //print_r($cliente->id);
                if($cliente->id_cliente_admin_publica != NULL)
                {
                    if(($cliente->clientes_admin_publica->sist != 'SGR') && ($cliente->clientes_admin_publica->sist != 'SUP') && ($cliente->clientes_admin_publica->sist != 'OSS') && ($cliente->clientes_admin_publica->sist != 'JOR'))
                    {
                        
                            
                            //recorrer anexos para saber cual esta activo
                            $contador_anexos_activos = 0;
                            foreach ($cliente->solicitudes->anexos as $anexo) 
                                {
                                    $f_anex_vig_aux = date_create_from_format('Y-m-d', $anexo->fecha_vigencia);
                                    $f_anex_vig_aux = date_format($f_anex_vig_aux,"Y-m-d");
                                    $periodo_date_aux = date_format($periodo_date,"Y-m-d");
                                    if(($anexo->estado == 1) && ($f_anex_vig_aux <= $periodo_date_aux))
                                    {
                                        $contador_anexos_activos++;
                                    }
                                    else if($anexo->estado == 0) 
                                    {
                                        if($anexo->fecha_baja != NULL)
                                        {
                                            $f_anex_baja_aux = date_create_from_format('Y-m-d', $anexo->fecha_baja);
                                            $f_anex_baja_aux = date_format($f_anex_baja_aux,"Y-m-d");
                                            $periodo_date_aux = date_format($periodo_date,"Y-m-d");
                                                
                                            if(($f_anex_baja_aux > $periodo_date_aux)&&($f_anex_vig_aux != $f_anex_baja_aux))
                                            {
                                                $contador_anexos_activos++;
                                            }
                                        }
                                    }
                                }
                            if($contador_anexos_activos > 0)//hay al menos 1 anexo activo
                            {
                                $acum_importes = 0;
                                $items_periodo_deuda = new Items_periodo_deuda_eloquent();
                                $items_periodo_deuda->id_periodo_deuda = $periodo_deuda->id;
                                $items_periodo_deuda->id_cliente = $cliente->id;
                                $items_periodo_deuda->estado = 1;
                                $items_periodo_deuda->save();
                                $cont_registros++;
                                 if(($cliente->clientes_admin_publica->sist != 'SGR') && ($cliente->clientes_admin_publica->sist != 'SUP') && ($cliente->clientes_admin_publica->sist != 'OSS') && ($cliente->clientes_admin_publica->sist != 'JOR'))
                                {
                                    $cont_registros_lis++;
                                }
                                
                                //recorrer anexos
                                //obtener anexos
                                foreach ($cliente->solicitudes->anexos as $anexo) 
                                    {
                                        $f_anex_vig_aux = date_create_from_format('Y-m-d', $anexo->fecha_vigencia);
                                        $f_anex_vig_aux = date_format($f_anex_vig_aux,"Y-m-d");
                                        $periodo_date_aux = date_format($periodo_date,"Y-m-d");
                                        if(($anexo->estado == 1) && ($f_anex_vig_aux <= $periodo_date_aux))
                                            
                                        {
                                            $items_periodo_deuda_detalle = new Items_periodo_deuda_detalle_eloquent();
                                            $items_periodo_deuda_detalle->id_items_periodo_deuda = $items_periodo_deuda->id;
                                            $items_periodo_deuda_detalle->id_solicitud = $cliente->solicitudes->id;
                                            $items_periodo_deuda_detalle->id_anexo = $anexo->id;
                                            $items_periodo_deuda_detalle->importe = $anexo->importe;
                                            $items_periodo_deuda_detalle->estado = 1;
                                            //buscar el producto contratados en la solicitud
                                            $producto_solicitud = Productos_solicitud_eloquent::where('id_solicitud', $cliente->solicitudes->id)->where('id_anexo',$anexo->id)->first();
                                            $items_periodo_deuda_detalle->id_producto = $producto_solicitud->id_producto;
                                            $items_periodo_deuda_detalle->save();

                                            $acum_importes = $acum_importes +$anexo->importe;
                                        }
                                        else if($anexo->estado == 0) 
                                        {
                                            if($anexo->fecha_baja != NULL)
                                            {   
                                                $f_anex_baja_aux = date_create_from_format('Y-m-d', $anexo->fecha_baja);
                                                $f_anex_baja_aux = date_format($f_anex_baja_aux,"Y-m-d");
                                                $periodo_date_aux = date_format($periodo_date,"Y-m-d");
                                                /*if($anexo->id == 81277)
                                                    {
                                                        echo "periodo baja: ";
                                                        print_r($f_anex_baja_aux);
                                                        echo "periodo cobrar: ";
                                                        print_r($periodo_date_aux);
                                                        die();
                                                    }*/
                                                if(($f_anex_baja_aux > $periodo_date_aux)&&($f_anex_vig_aux != $f_anex_baja_aux))
                                                {
                                                    
                                                    $items_periodo_deuda_detalle = new Items_periodo_deuda_detalle_eloquent();
                                                    $items_periodo_deuda_detalle->id_items_periodo_deuda = $items_periodo_deuda->id;
                                                    $items_periodo_deuda_detalle->id_solicitud = $cliente->solicitudes->id;
                                                    $items_periodo_deuda_detalle->id_anexo = $anexo->id;
                                                    $items_periodo_deuda_detalle->importe = $anexo->importe;
                                                    $items_periodo_deuda_detalle->estado = 1;
                                                    //buscar el producto contratados en la solicitud
                                                    $producto_solicitud = Productos_solicitud_eloquent::where('id_solicitud', $cliente->solicitudes->id)->where('id_anexo',$anexo->id)->first();
                                                    $items_periodo_deuda_detalle->id_producto = $producto_solicitud->id_producto;
                                                    $items_periodo_deuda_detalle->save();

                                                    $acum_importes = $acum_importes +$anexo->importe;
                                                }
                                            }
                                        }    
                                            
                                    }
                                $items_periodo_deuda->deuda = $cliente->cuentas_corrientes->saldo;
                                $items_periodo_deuda->importe_total = $acum_importes;
                                $items_periodo_deuda->save();

                                //items cuenta corriente
                                $items_cuenta_corriente = new Items_cuenta_corriente_eloquent();
                                $items_cuenta_corriente->id_cuenta_corriente = $cliente->cuentas_corrientes->id;
                                $items_cuenta_corriente->id_cliente = $cliente->id;
                                $items_cuenta_corriente->importe = $acum_importes;
                                $items_cuenta_corriente->periodo = $this->input->post('periodo');
                                $items_cuenta_corriente->id_items_periodo_deuda = $items_periodo_deuda->id;
                                $items_cuenta_corriente->importe_recibo = 0;
                                $items_cuenta_corriente->deuda_recibida = 0;
                                $items_cuenta_corriente->estado = 1;
                                $items_cuenta_corriente->save();

                                 //actualizar saldo cuenta corriente
                                $saldo_anterior = $cliente->cuentas_corrientes->saldo;
                                $periodo_deuda->saldo_anterior = $saldo_anterior;
                                $cliente->cuentas_corrientes->saldo = $acum_importes + $saldo_anterior;
                                $cliente->cuentas_corrientes->save();

                                //agrego el salgo al items cuenta corriente
                                $items_cuenta_corriente->saldo = $cliente->cuentas_corrientes->saldo;
                                $items_cuenta_corriente->save();
                                $periodo_deuda->save();

                                //

                            }
                        
                    }
                        
                    
                }

            }
        }    
            //print_r($tipo_cobro);
            //$this->generador_txt_bancos($periodo_deuda->id);
        
        $periodo_deuda->registros_txt = $cont_registros;
        $periodo_deuda->registros_txt_sup = $cont_registros_sup;
        $periodo_deuda->registros_txt_sgr = $cont_registros_sgr;
        $periodo_deuda->registros_txt_listado = $cont_registros_lis;
            $periodo_deuda->save();
        echo json_encode(array(
            "status" => TRUE,
            ));   
        //echo "fin";   
        //redirect('Periodos_deudas/index');
    }
    public function generador_txt_sup($id)
    {
        $periodo_deuda = Periodos_deudas_eloquent::find($id);

        $mes_periodo = substr($periodo_deuda->periodo, 0,2);
        $nombre_archivo = 'ESSENDER.';
        if($mes_periodo == '01')
        {
            $nombre_archivo = $nombre_archivo.'ENE';
        }else if($mes_periodo == '02')
        {
            $nombre_archivo = $nombre_archivo.'FEB';
        }else if($mes_periodo == '03')
        {
            $nombre_archivo = $nombre_archivo.'MAR';
        }else if($mes_periodo == '04')
        {
            $nombre_archivo = $nombre_archivo.'ABR';
        }else if($mes_periodo == '05')
        {
            $nombre_archivo = $nombre_archivo.'MAY';
        }else if($mes_periodo == '06')
        {
            $nombre_archivo = $nombre_archivo.'JUN';
        }else if($mes_periodo == '07')
        {
            $nombre_archivo = $nombre_archivo.'JUL';
        }else if($mes_periodo == '08')
        {
            $nombre_archivo = $nombre_archivo.'AGO';
        }else if($mes_periodo == '09')
        {
            $nombre_archivo = $nombre_archivo.'SET';
        }else if($mes_periodo == '10')
        {
            $nombre_archivo = $nombre_archivo.'OCT';
        }else if($mes_periodo == '11')
        {
            $nombre_archivo = $nombre_archivo.'NOV';
        }else if($mes_periodo == '12')
        {
            $nombre_archivo = $nombre_archivo.'DIC';
        } 

        $txt= fopen($nombre_archivo , 'w+') or die ('Problemas al crear el archivo');
                $archivo =$nombre_archivo;

        foreach ($periodo_deuda->items_periodo_deuda as $key ) 
        {
            if($key->clientes->clientes_admin_publica->sist == 'SUP')
            {       
                    $total = $key->importe_total;
                    $total_deuda = '0000000';
                    
                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 5, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 5, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    } 
                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {
                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $deuda = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $deuda = $key->deuda;
                            }
                            
                            if (is_float($deuda))
                            {
                                $decimales = explode(".",$deuda);
                                $parte_entera_rellenada = str_pad($decimales[0], 5, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_deuda = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($deuda, 5, '0', STR_PAD_LEFT);
                                $total_deuda = $parte_entera_rellenada.'00';
                            }

                        }   
                    }
                        
                $centro = $key->clientes->clientes_admin_publica->centro;
                $centro = str_pad($centro, 2, '0', STR_PAD_LEFT);
                $nro_padron = $key->clientes->clientes_admin_publica->nro_padron;
                $nro_padron = str_pad($nro_padron, 6, '0', STR_PAD_LEFT);
                $digito = $key->clientes->clientes_admin_publica->digito;
                $cadena = $centro.'      '.$nro_padron.$digito.'P72'.$total_cobrar.'Q17'.$total_deuda.'Q190000000                                 B3';
                fwrite($txt, $cadena.PHP_EOL);
            }

        }
        $clientes_bajas = $this->Model_clientes->get_id_clientes_admin_publica($this->session->userdata('id_empresa'));
        foreach ($clientes_bajas as $clie) 
        {
            $cliente = Clientes_eloquent::find($clie->id);
            $periodo_a_cobrar = substr($periodo_deuda->periodo, 3, 4).'-'.substr($periodo_deuda->periodo, 0, 2).'-01';
            if($cliente->clientes_admin_publica->sist == 'SUP')
            {
                if($cliente->solicitudes->fecha_periodo_baja == $periodo_a_cobrar)
                {
                    $centro = $cliente->clientes_admin_publica->centro;
                    $centro = str_pad($centro, 2, '0', STR_PAD_LEFT);
                    $nro_padron = $cliente->clientes_admin_publica->nro_padron;
                    $nro_padron = str_pad($nro_padron, 6, '0', STR_PAD_LEFT);
                    $digito = $cliente->clientes_admin_publica->digito;
                    $cadena = $centro.'      '.$nro_padron.$digito.'P720000000Q170000000Q190000000                                 B3';
                    fwrite($txt, $cadena.PHP_EOL);
                }
            }
        }
        $cadena = '';
        fwrite($txt, $cadena.PHP_EOL);
        header ("Content-Disposition: attachment; filename=".$archivo); 
        header ("Content-Type: application/octet-stream"); 
        header ("Content-Length: ".filesize($archivo)); 
        readfile($archivo); 
        return;
    }
    public function generador_txt_sgr($id)
    {
        $periodo_deuda = Periodos_deudas_eloquent::find($id);

        $mes_periodo = substr($periodo_deuda->periodo, 0,2);
        $nombre_archivo = 'SGSENDER.';
        if($mes_periodo == '01')
        {
            $nombre_archivo = $nombre_archivo.'ENE';
        }else if($mes_periodo == '02')
        {
            $nombre_archivo = $nombre_archivo.'FEB';
        }else if($mes_periodo == '03')
        {
            $nombre_archivo = $nombre_archivo.'MAR';
        }else if($mes_periodo == '04')
        {
            $nombre_archivo = $nombre_archivo.'ABR';
        }else if($mes_periodo == '05')
        {
            $nombre_archivo = $nombre_archivo.'MAY';
        }else if($mes_periodo == '06')
        {
            $nombre_archivo = $nombre_archivo.'JUN';
        }else if($mes_periodo == '07')
        {
            $nombre_archivo = $nombre_archivo.'JUL';
        }else if($mes_periodo == '08')
        {
            $nombre_archivo = $nombre_archivo.'AGO';
        }else if($mes_periodo == '09')
        {
            $nombre_archivo = $nombre_archivo.'SET';
        }else if($mes_periodo == '10')
        {
            $nombre_archivo = $nombre_archivo.'OCT';
        }else if($mes_periodo == '11')
        {
            $nombre_archivo = $nombre_archivo.'NOV';
        }else if($mes_periodo == '12')
        {
            $nombre_archivo = $nombre_archivo.'DIC';
        } 

        $txt= fopen($nombre_archivo , 'w+') or die ('Problemas al crear el archivo');
                $archivo =$nombre_archivo;

        foreach ($periodo_deuda->items_periodo_deuda as $key ) 
        {
            if($key->clientes->clientes_admin_publica->sist != 'SUP')
            {       
                    $total = $key->importe_total;
                    $total_deuda = '0000000';
                    
                    if (is_float($total))
                    {
                        $decimales = explode(".",$total);
                        $parte_entera_rellenada = str_pad($decimales[0], 5, '0', STR_PAD_LEFT);
                        
                        $tipo_total_var = sizeof($decimales);
                        //print_r($tipo_total_var);
                        if($tipo_total_var == 2)
                        {
                            $parte_flotante_redondeada = round($decimales[1], 2);
                            $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                        }
                        else
                        {
                            $parte_flotante_redondeada = '00';
                        }
                        $total_cobrar = $parte_entera_rellenada.$parte_flotante_redondeada;
                    }   
                    else
                    {
                        $parte_entera_rellenada = str_pad($total, 5, '0', STR_PAD_LEFT);
                        $total_cobrar = $parte_entera_rellenada.'00';
                    } 
                    if($periodo_deuda->tope_deuda > 0)
                    {
                        if($key->deuda > 0)
                        {
                            if($key->deuda > $periodo_deuda->tope_deuda)
                            {
                                $deuda = $periodo_deuda->tope_deuda;
                            }
                            else
                            {
                                $deuda = $key->deuda;
                            }
                            
                            if (is_float($deuda))
                            {
                                $decimales = explode(".",$deuda);
                                $parte_entera_rellenada = str_pad($decimales[0], 5, '0', STR_PAD_LEFT);
                                
                                $tipo_total_var = sizeof($decimales);
                                //print_r($tipo_total_var);
                                if($tipo_total_var == 2)
                                {
                                    $parte_flotante_redondeada = round($decimales[1], 2);
                                    $parte_flotante_redondeada = str_pad($parte_flotante_redondeada, 2, '0', STR_PAD_RIGHT);
                                }
                                else
                                {
                                    $parte_flotante_redondeada = '00';
                                }
                                $total_deuda = $parte_entera_rellenada.$parte_flotante_redondeada;
                            }   
                            else
                            {
                                $parte_entera_rellenada = str_pad($deuda, 5, '0', STR_PAD_LEFT);
                                $total_deuda = $parte_entera_rellenada.'00';
                            }

                        }   
                    }
                        
                $centro = $key->clientes->clientes_admin_publica->centro;
                $centro = str_pad($centro, 2, '0', STR_PAD_LEFT);
                $nro_padron = $key->clientes->clientes_admin_publica->nro_padron;
                $nro_padron = str_pad($nro_padron, 6, '0', STR_PAD_LEFT);
                $digito = $key->clientes->clientes_admin_publica->digito;
                $cadena = $centro.'      '.$nro_padron.$digito.'P72'.$total_cobrar.'Q17'.$total_deuda.'Q190000000                                 B3';
                fwrite($txt, $cadena.PHP_EOL);

            }

        }
        $clientes_bajas = $this->Model_clientes->get_id_clientes_admin_publica($this->session->userdata('id_empresa'));
        foreach ($clientes_bajas as $clie) 
        {
            $cliente = Clientes_eloquent::find($clie->id);
            $periodo_a_cobrar = substr($periodo_deuda->periodo, 3, 4).'-'.substr($periodo_deuda->periodo, 0, 2).'-01';
            if($cliente->clientes_admin_publica->sist == 'SGR')
            {
                if($cliente->solicitudes->fecha_periodo_baja == $periodo_a_cobrar)
                {
                    $centro = $cliente->clientes_admin_publica->centro;
                    $centro = str_pad($centro, 2, '0', STR_PAD_LEFT);
                    $nro_padron = $cliente->clientes_admin_publica->nro_padron;
                    $nro_padron = str_pad($nro_padron, 6, '0', STR_PAD_LEFT);
                    $digito = $cliente->clientes_admin_publica->digito;
                    $cadena = $centro.'      '.$nro_padron.$digito.'P720000000Q170000000Q190000000                                 B3';
                    fwrite($txt, $cadena.PHP_EOL);
                }
            }
        }


        $cadena = '';
                fwrite($txt, $cadena.PHP_EOL);
        header ("Content-Disposition: attachment; filename=".$archivo); 
        header ("Content-Type: application/octet-stream"); 
        header ("Content-Length: ".filesize($archivo)); 
        readfile($archivo); 
        return;
    }

    public function prueba_cant_empleados_pub()
    {
        $clientes = Clientes_eloquent::activos()
                                    ->empresa($this->session->userdata('id_empresa'))
                                    ->where('id_cliente_admin_publica','!=','NULL')
                                    ->get();
        $cont = 0;
        foreach ($clientes as $cliente ) 
        {
            
            if(($cliente->id_cliente_admin_publica != NULL) && ($cliente->id == 333))
                {

                        if(($cliente->solicitudes->estado == 1) )
                        {

                        }
                        else//pueden ver anexos activos
                        {
                            echo "id cliente: ".$cliente->id;echo "<br>";
                            echo "id solicitud: ".$cliente->solicitudes->id;echo "<br>";
                            //recorrer anexos para saber cual esta activo
                            $contador_anexos_activos = 0;
                            foreach ($cliente->solicitudes->anexos as $anexo) 
                            { echo "id anexo: ".$anexo->id;
                                echo "<br>";
                                if(($anexo->estado == '1'))
                                {
                                    $contador_anexos_activos++;
                                    $cont++;
                                }
                            }
                        }
                }
        }
        //echo "empleados publicos hay: ".$cont;
        echo "anexos activos: ".$cont;
        //&& ($anexo->fecha_vigencia <= $periodo_date)
    }
    public function baja_periodo_deuda()
    {
        //$cliente = Clientes_eloquent::find($id);
        $periodo = Periodos_deudas_eloquent::find($this->input->post('id_periodo'));
        $id_empresa = $this->session->userdata('id_empresa');
        $existe_periodo_superior = FALSE;
        if($periodo->id_banco != 0)
        {
            $periodo_superior = Periodos_deudas_eloquent::where('id','>',$periodo->id)
                                                        ->where('estado','=',1)
                                                        ->where('id_banco','=',$periodo->id_banco)
                                                        ->where('id_empresa','=',$id_empresa)
                                                        ->first();
            if($periodo_superior != NULL)
            {
                $existe_periodo_superior = TRUE;
            }
        }
        else if($periodo->id_tarjeta != 0)
        {
            $periodo_superior = Periodos_deudas_eloquent::where('id','>',$periodo->id)
                                                        ->where('estado','=',1)
                                                        ->where('id_tarjeta','=',$periodo->id_tarjeta)
                                                        ->where('id_empresa','=',$id_empresa)
                                                        ->first();
            if($periodo_superior != NULL)
            {
                $existe_periodo_superior = TRUE;
            }
        }
        else if(($periodo->adm_pub != 0) && ($periodo->tipo_cobro_adm_pub == 'cc'))
        {
            
            $periodo_superior = Periodos_deudas_eloquent::where('id','>',$periodo->id)
                                                        ->where('estado','=',1)
                                                        ->where('adm_pub','=',$periodo->adm_pub)
                                                        ->where('tipo_cobro_adm_pub','=','cc')
                                                        ->where('id_empresa','=',$id_empresa)
                                                        ->first();
            if($periodo_superior != NULL)
            {
                $existe_periodo_superior = TRUE;
            }
        }
        else if(($periodo->adm_pub != 0) && ($periodo->tipo_cobro_adm_pub == 'listado'))
        {
            
            $periodo_superior = Periodos_deudas_eloquent::where('id','>',$periodo->id)
                                                        ->where('estado','=',1)
                                                        ->where('adm_pub','=',$periodo->adm_pub)
                                                        ->where('tipo_cobro_adm_pub','=','listado')
                                                        ->where('id_empresa','=',$id_empresa)
                                                        ->first();
            if($periodo_superior != NULL)
            {
                $existe_periodo_superior = TRUE;
            }
        }
        if($existe_periodo_superior == FALSE)
        {
            $ids_items_periodo_deuda = $list = $this->Model_periodos_deudas->get_id_items_periodo_deuda($periodo->id);
           
            //foreach ($periodo->items_periodo_deuda as $items_periodo_deuda) 
            foreach ($ids_items_periodo_deuda as $items_periodo_deuda_aux) 
            {
                $items_periodo_deuda = Items_periodo_deuda_eloquent::find($items_periodo_deuda_aux->id);
                $items_cuenta_corriente = Items_cuenta_corriente_eloquent::where('id_items_periodo_deuda','=',$items_periodo_deuda_aux->id)->first();;
                if($items_cuenta_corriente != NULL)
                {
                    $importe_anular = 0;
                    /*if(($items_cuenta_corriente->importe_recibo > '0') || ($items_cuenta_corriente->deuda_recibida > '0'))
                    {
                        if($items_cuenta_corriente->importe_recibo > '0')
                        {
                           if($items_cuenta_corriente->importe_recibo > $items_cuenta_corriente->importe)
                            {
                               
                                $importe_anular = $items_cuenta_corriente->importe_recibo - $items_cuenta_corriente->importe;
                                $items_periodo_deuda->clientes->cuentas_corrientes->saldo = $items_periodo_deuda->clientes->cuentas_corrientes->saldo + $importe_anular;

                            }
                            else if($items_cuenta_corriente->importe_recibo < $items_cuenta_corriente->importe)
                            {
                               
                                $importe_anular = $items_cuenta_corriente->importe - $items_cuenta_corriente->importe_recibo;
                                $items_periodo_deuda->clientes->cuentas_corrientes->saldo = $items_periodo_deuda->clientes->cuentas_corrientes->saldo - $importe_anular;
                            }
                        }
                        if($items_cuenta_corriente->deuda_recibida > '0')
                        {
                            $items_periodo_deuda->clientes->cuentas_corrientes->saldo = $items_periodo_deuda->clientes->cuentas_corrientes->saldo + $items_cuenta_corriente->deuda_recibida;
                        }
                    }
                    else
                    {
                        $items_periodo_deuda->clientes->cuentas_corrientes->saldo = $items_periodo_deuda->clientes->cuentas_corrientes->saldo - $items_cuenta_corriente->importe;
                    }*/
                    $items_periodo_deuda->clientes->cuentas_corrientes->saldo = $items_cuenta_corriente->saldo - $items_cuenta_corriente->importe; 
                    $items_periodo_deuda->clientes->cuentas_corrientes->save();

                    $items_cuenta_corriente->estado = 0;
                    $items_cuenta_corriente->save();
                    $items_periodo_deuda->clientes->cuentas_corrientes->save();
                    $items_periodo_deuda->estado = 0;
                }
                $items_periodo_deuda->save();
                foreach ($items_periodo_deuda->items_periodo_pago_parcial as $parcial) 
                {
                    $parcial->estado = 0;
                    $parcial->save();
                }
            }
            $periodo->estado = 0;
            $periodo->save();
            $data = array(
                        "status" => true, 
                        "id_periodo" => $periodo->id, 
                        "periodo_superior" => FALSE
                        );
            echo json_encode($data);
        }
        else
        {
            $data = array(
                        "status" => true, 
                        "id_periodo" => $periodo->id, 
                        "periodo_superior" => TRUE
                        );
            echo json_encode($data);
        }
        


        

        //echo "hola mundo";
    }
    public function borrar_pagos_recibido()
    {
        $periodo = Periodos_deudas_eloquent::find($this->input->post('id_periodo_borrar_pago'));
        $id_empresa = $this->session->userdata('id_empresa');
        $existe_periodo_superior = FALSE;
        if($periodo->id_banco != 0)
        {
            $periodo_superior = Periodos_deudas_eloquent::where('id','>',$periodo->id)
                                                        ->where('estado','=',1)
                                                        ->where('id_banco','=',$periodo->id_banco)
                                                        ->where('id_empresa','=',$id_empresa)
                                                        ->first();
            if($periodo_superior != NULL)
            {
                $existe_periodo_superior = TRUE;
            }
        }
        else if($periodo->id_tarjeta != 0)
        {
            $periodo_superior = Periodos_deudas_eloquent::where('id','>',$periodo->id)
                                                        ->where('estado','=',1)
                                                        ->where('id_tarjeta','=',$periodo->id_tarjeta)
                                                        ->where('id_empresa','=',$id_empresa)
                                                        ->first();
            if($periodo_superior != NULL)
            {
                $existe_periodo_superior = TRUE;
            }
        }
        else if(($periodo->adm_pub != 0) && ($periodo->tipo_cobro_adm_pub == 'cc'))
        {
            
            $periodo_superior = Periodos_deudas_eloquent::where('id','>',$periodo->id)
                                                        ->where('estado','=',1)
                                                        ->where('adm_pub','=',$periodo->adm_pub)
                                                        ->where('tipo_cobro_adm_pub','=','cc')
                                                        ->where('id_empresa','=',$id_empresa)
                                                        ->first();
            if($periodo_superior != NULL)
            {
                $existe_periodo_superior = TRUE;
            }
        }
        else if(($periodo->adm_pub != 0) && ($periodo->tipo_cobro_adm_pub == 'listado'))
        {
            
            $periodo_superior = Periodos_deudas_eloquent::where('id','>',$periodo->id)
                                                        ->where('estado','=',1)
                                                        ->where('adm_pub','=',$periodo->adm_pub)
                                                        ->where('tipo_cobro_adm_pub','=','listado')
                                                        ->where('id_empresa','=',$id_empresa)
                                                        ->first();
            if($periodo_superior != NULL)
            {
                $existe_periodo_superior = TRUE;
            }
        }
        if($existe_periodo_superior == FALSE)
        {
            $ids_items_periodo_deuda = $list = $this->Model_periodos_deudas->get_id_items_periodo_deuda($periodo->id);
           
            //foreach ($periodo->items_periodo_deuda as $items_periodo_deuda) 
            foreach ($ids_items_periodo_deuda as $items_periodo_deuda_aux) 
            {
                $items_periodo_deuda = Items_periodo_deuda_eloquent::find($items_periodo_deuda_aux->id);
                $items_cuenta_corriente = Items_cuenta_corriente_eloquent::where('id_items_periodo_deuda','=',$items_periodo_deuda->id)->first();;
                if($items_cuenta_corriente != NULL)
                {
                    $importe_anular = 0;
                    /*if(($items_cuenta_corriente->importe_recibo > '0') || ($items_cuenta_corriente->deuda_recibida > '0'))
                    {
                        if($items_cuenta_corriente->importe_recibo > '0')
                        {
                           if($items_cuenta_corriente->importe_recibo > $items_cuenta_corriente->importe)
                            {
                               
                                $importe_anular = $items_cuenta_corriente->importe_recibo - $items_cuenta_corriente->importe;
                                $items_periodo_deuda->clientes->cuentas_corrientes->saldo = $items_periodo_deuda->clientes->cuentas_corrientes->saldo + $importe_anular;
                                $items_periodo_deuda->clientes->cuentas_corrientes->saldo =  $items_cuenta_corriente->saldo;

                            }
                            else if($items_cuenta_corriente->importe_recibo < $items_cuenta_corriente->importe)
                            {
                               
                                $importe_anular = $items_cuenta_corriente->importe - $items_cuenta_corriente->importe_recibo;
                                $items_periodo_deuda->clientes->cuentas_corrientes->saldo = $items_periodo_deuda->clientes->cuentas_corrientes->saldo - $importe_anular;
                            }
                        }
                        if($items_cuenta_corriente->deuda_recibida > '0')
                        {
                            $items_periodo_deuda->clientes->cuentas_corrientes->saldo = $items_periodo_deuda->clientes->cuentas_corrientes->saldo + $items_cuenta_corriente->deuda_recibida;
                        }
                    }
                    else
                    {
                        $items_periodo_deuda->clientes->cuentas_corrientes->saldo = $items_periodo_deuda->clientes->cuentas_corrientes->saldo - $items_cuenta_corriente->importe;
                    }*/
                    //$items_cuenta_corriente->estado = 0;
                    $items_periodo_deuda->clientes->cuentas_corrientes->saldo =  $items_cuenta_corriente->saldo;
                    $items_periodo_deuda->clientes->cuentas_corrientes->save();
                    $items_cuenta_corriente->estado_cobro = 0;
                    $items_cuenta_corriente->fecha_cobro = NULL;
                    $items_cuenta_corriente->importe_recibo = 0;
                    $items_cuenta_corriente->deuda_recibida = 0;
                    $items_cuenta_corriente->save();


                    $items_periodo_deuda->clientes->cuentas_corrientes->save();


                    //$items_periodo_deuda->estado = 0;
                }
                $items_periodo_deuda->importe_total_recibido = 0;
                $items_periodo_deuda->deuda_recibida =0;
                $items_periodo_deuda->estado_cobro_deuda = NULL;
                $items_periodo_deuda->estado_cobro = NULL;
                $items_periodo_deuda->fecha_cobro_periodo = NULL;
                $items_periodo_deuda->fecha_cobro_deuda = NULL;
                $items_periodo_deuda->save();
                foreach ($items_periodo_deuda->items_periodo_pago_parcial as $parcial) 
                {
                    $parcial->importe_recibido = 0;
                    $parcial->fecha = '0000-00-00';
                    $parcial->importe_total = 0;
                    $parcial->estado = 0;
                    $parcial->imp_mov = 0;
                    $parcial->imp_aplic = 0;
                    $parcial->imp_recha = 0;
                    $parcial->save();
                }
                
            }
            //$periodo->estado = 0;
            //$periodo->save();
            $data = array(
                        "status" => true, 
                        "id_periodo" => $periodo->id, 
                        "periodo_superior" => FALSE
                        );
            echo json_encode($data);
        }
        else
        {
            $data = array(
                        "status" => true, 
                        "id_periodo" => $periodo->id, 
                        "periodo_superior" => TRUE
                        );
            echo json_encode($data);
        }
    } 

    public function prueba_eloquent()
    {
       $clientes = Clientes_eloquent::empresa($this->session->userdata('id_empresa'))
                                    ->where('id_cliente_admin_publica','!=','NULL')
                                   // ->solicitudes('id_asesor','=','2')
                                    ->get();
        print_r($clientes);

    } 

    public function obtener_periodo()
    {
        $periodo = Periodos_deudas_eloquent::find($this->input->post('id_periodo_fechas_deuda'));
        $data = array(
                        "status" => true, 
                        "id_periodo" => $periodo->id, 
                        "periodo" => $periodo
                        );
            echo json_encode($data);
    }

    public function update_fechas_periodo_generado()
    {
        $periodo_deuda = Periodos_deudas_eloquent::find($this->input->post('id_periodo_fechas_deuda'));
        $tope_mes = $this->input->post('tope_mes');
        
                    //Procesamiento de fecha y hora
                    if($tope_mes != '')
                    {
                      $separa = explode("-",$tope_mes);
                      $mes = $separa[0];
                      $anio = $separa[1];
                      $tope_mes = $mes.'-'.$anio;
                    }
       
        $periodo_deuda->tope_mes = $tope_mes;

        $fecha_inicio = $this->input->post('fecha_inicio');
                    //Procesamiento de fecha y hora
                    if($fecha_inicio != '')
                    {

                      $fecha_inicio = str_replace('/', '-', $fecha_inicio);
                      $separa = explode("-",$fecha_inicio);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha_inicio = $anio.'-'.$mes.'-'.$dia;
                    }
        $periodo_deuda->fecha_inicio = $fecha_inicio;
       
      

        $fecha_fin = $this->input->post('fecha_fin');
                    //Procesamiento de fecha y hora
                    if($fecha_fin != '')
                    {

                      $fecha_fin = str_replace('/', '-', $fecha_fin);
                      $separa = explode("-",$fecha_fin);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha_fin = $anio.'-'.$mes.'-'.$dia;
                    }
        $periodo_deuda->fecha_fin = $fecha_fin;

        $fecha_envio = $this->input->post('fecha_envio');
                    //Procesamiento de fecha y hora
                    if($fecha_envio != '')
                    {

                      $fecha_envio = str_replace('/', '-', $fecha_envio);
                      $separa = explode("-",$fecha_envio);
                      $mes = $separa[1];
                      $dia = $separa[0];
                      $anio = $separa[2];
                      
                      $fecha_envio = $anio.'-'.$mes.'-'.$dia;
                    }
        $periodo_deuda->fecha_envio = $fecha_envio;
        $periodo_deuda->save();
        $data = array(
                        "status" => true, 
                        "id_periodo" => $periodo_deuda->id, 
                        "periodo" => $periodo_deuda
                        );
            echo json_encode($data);
    }
    public function clientes_dados_de_baja_prod_activos()
    {
        $id_banco = 13;
        $periodo_date = date_create_from_format('Y-m-d', '2017'.'-'.'07'.'-01');
        //$periodo_date = "2017-07-01";
        //$periodo_date_aux = "2017-07-01";
        for ($i=1; $i < 14; $i++) 
        { 
          $id_banco = $i;
            $clientes_banco = Cuentas_bancarias_clientes_eloquent::activos()
                                                                      ->where('banco_id','=',$id_banco)->get();
            $banco = Bancos_eloquent::find($id_banco);
            if($banco != false)
            {
                echo "-------------------------------";
                echo "<br>";
                echo "Banco: ".$banco->nombre;
                echo "<br>";
                foreach ($clientes_banco as $cliente_banco) 
                {
                    $cliente = Clientes_eloquent::empresa($this->session->userdata('id_empresa'))
                                                ->where('id_user_group','=',$cliente_banco->id_user_group)
                                                ->first();
                    if($cliente)
                    {
                        if(($cliente->cuenta_bancaria_id != NULL )&& ($cliente->id_cliente_admin_publica == NULL) && ($cliente->estado == 0))
                        {
                            $bandera_clie_baja = FALSE;
                            //if(($cliente->cuentas_bancarias_clientes->banco_id == $id_banco) && ($cliente->cuentas_bancarias_clientes->tipo_cuenta_bancaria_id == $id_tipo_cuenta_bancaria))
                            if(($cliente->cuentas_bancarias_clientes->banco_id == $id_banco))
                            {
                                foreach ($cliente->solicitudes->anexos as $anexo) 
                                        {
                                            $f_anex_vig_aux = date_create_from_format('Y-m-d', $anexo->fecha_vigencia);
                                            $f_anex_vig_aux = date_format($f_anex_vig_aux,"Y-m-d");
                                           /* print_r($f_anex_vig_aux);
                                            print_r('<br>');
                                            print_r($periodo_date_aux);
                                            
                                            die();*/
                                           $periodo_date_aux = date_format($periodo_date,"Y-m-d");
                                            if(($anexo->estado == 1) && ($f_anex_vig_aux <= $periodo_date_aux))
                                                
                                            {
                                                
                                                //buscar el producto contratados en la solicitud
                                                $producto_solicitud = Productos_solicitud_eloquent::where('id_solicitud', $cliente->solicitudes->id)->where('id_anexo',$anexo->id)->first();
                                             /*   echo "nro_socio: ".$cliente->nro_socio;
                                                echo "<br>";*/
                                                $bandera_clie_baja = TRUE;
                                                
                                            }
                                            else if($anexo->estado == 0) 
                                            {
                                                if($anexo->fecha_baja != NULL)
                                                {   
                                                    $f_anex_baja_aux = date_create_from_format('Y-m-d', $anexo->fecha_baja);
                                                    $f_anex_baja_aux = date_format($f_anex_baja_aux,"Y-m-d");
                                                    $periodo_date_aux = date_format($periodo_date,"Y-m-d");
                                                    /*if($anexo->id == 81277)
                                                        {
                                                            echo "periodo baja: ";
                                                            print_r($f_anex_baja_aux);
                                                            echo "periodo cobrar: ";
                                                            print_r($periodo_date_aux);
                                                            die();
                                                        }*/
                                                    if(($f_anex_baja_aux > $periodo_date_aux)&&($f_anex_vig_aux != $f_anex_baja_aux))
                                                    {
                                                        
                                                        
                                                        //buscar el producto contratados en la solicitud
                                                        $producto_solicitud = Productos_solicitud_eloquent::where('id_solicitud', $cliente->solicitudes->id)->where('id_anexo',$anexo->id)->first();
                                                        /*echo "nro_socio: ".$cliente->nro_socio;
                                                        echo "<br>";*/
                                                        $bandera_clie_baja = TRUE;
                                                    }
                                                }
                                            }    
                                                
                                        }
                                if($bandera_clie_baja == TRUE)
                                {
                                    echo "nro_socio: ".$cliente->nro_socio;
                                                        echo "<br>";
                                }
                            }
                        }
                    }
                }
                echo "<br>";
                echo "-------------------------------";
            }
        }
    }
}