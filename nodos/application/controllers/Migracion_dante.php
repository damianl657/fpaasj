<?php 

 

class Migracion_dante extends CI_Controller 

{	

      

	function __construct(){

		parent::__construct();

		

			//$this->output->enable_profiler(TRUE);

        if(!$this->session->userdata("logged_in")){

            //echo "pierde sesion"; die();

               redirect("login");

         }



	}



	public function crear_pass($pass)

    {

        $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales

                    $passApiKey = $this->variables->get_apikey();

                    //$correo = "nodos";

                    $url = $urlApi."/login/prueba_encriptar";

                    $ch = curl_init();

                    $options = array(

                                CURLOPT_RETURNTRANSFER => true,

                                CURLOPT_URL => $url,

                                CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey"),

                                CURLOPT_POST => true,

                                //CURLOPT_POSTFIELDS => json_encode($fields),

                                //CURLOPT_POSTFIELDS => $fields,

                                CURLOPT_POSTFIELDS => array(

                                                            'data' => $pass

                                                    ),  

                                CURLOPT_SSL_VERIFYPEER=> false,

                            );

                      curl_setopt_array( $ch, $options);

                      $response = curl_exec($ch);

                      curl_close($ch);

                     $response = json_decode($response, True);

                    

                     $arre = (array) $response; //convierto el obj en un arreglo.

                     $pass_encriptado = $arre['pass'];

                     return $pass_encriptado;

    }

    //damian

    public function importar_docentes_primaria()
    {

        //VARIABLES

        $tabla = "docentes_nodos_dante_primaria"; //--modificar por la division

        

        $group_id = 240;

        $colegio_id = 19;

        //$plan_estudio_id = 1;



        //SCRIPT        

        $sql = "SELECT * FROM ".$tabla;

        $cont = 0;

        $query = $this->db->query($sql);

        if($query->num_rows() > 0)

        {

            foreach ($query->result() as $key) 

            {

                $sql_check= "SELECT * FROM users WHERE documento='".$key->documento."'";

                $query_ch = $this->db->query($sql_check);

                if($query_ch->num_rows() == 0)

                {

                    $pass_alu = $this->crear_pass($key->documento);

                   



                    $insert_docente = "INSERT INTO users (first_name, last_name, active, documento, password,pago)

                            VALUES ('$key->nombre', '$key->apellido',1, '$key->documento','$pass_alu',1)";

                                

                   if($this->db->query($insert_docente))

                   {

                        $id_user_docente = $this->db->insert_id();                    

                        //insertar en users_groups

                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)

                                VALUES ($id_user_docente, $group_id, $colegio_id, 1)";

                        $this->db->query($insert_users_groups);

                         $cont = $cont + 1;

                           

                    }

                } // fin if el alumno ya existe.

                else{

                    $docente = $query_ch->row();

                    $id_doc = $docente->id;



                    echo "EL Docente".$key->documento." ".$key->apellido." ".$key->nombre." Existe en USERS <br>";

                    $sql_check_user_group= "SELECT * FROM users_groups WHERE user_id='".$id_doc."'and group_id = $group_id";

                    $query_ch_user_group = $this->db->query($sql_check_user_group);

                    if($query_ch_user_group->num_rows() > 0)

                    {

                        echo "EL Docente YA ES DOCENTE EN LA ESCUELA SELECCIONADA<br>";

                    }

                    else

                    {

                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)

                                VALUES ($id_doc, $group_id, $colegio_id, 1)";

                        $this->db->query($insert_users_groups);

                         //$cont = $cont + 1;

                    }

                    

                }

                

            } // fin for recorrido de los alumnos.

            echo "se insertaron: ".$cont." Docentes<br>";

        }

    } 
    public function importar_docentes_secundaria()
    {

        //VARIABLES

        $tabla = "docentes_nodos_dante_secundaria"; //--modificar por la division

        

        $group_id = 249;

        $colegio_id = 20;

        //$plan_estudio_id = 1;



        //SCRIPT        

        $sql = "SELECT * FROM ".$tabla;

        $cont = 0;

        $query = $this->db->query($sql);

        if($query->num_rows() > 0)

        {

            foreach ($query->result() as $key) 

            {

                $sql_check= "SELECT * FROM users WHERE documento='".$key->documento."'";

                $query_ch = $this->db->query($sql_check);

                if($query_ch->num_rows() == 0)

                {

                    $pass_alu = $this->crear_pass($key->documento);

                   



                    $insert_docente = "INSERT INTO users (first_name, last_name, active, documento, password,pago)

                            VALUES ('$key->nombre', '$key->apellido',1, '$key->documento','$pass_alu',1)";

                                

                   if($this->db->query($insert_docente))

                   {

                        $id_user_docente = $this->db->insert_id();                    

                        //insertar en users_groups

                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)

                                VALUES ($id_user_docente, $group_id, $colegio_id, 1)";

                        $this->db->query($insert_users_groups);

                         $cont = $cont + 1;

                           

                    }

                } // fin if el alumno ya existe.

                else{

                    $docente = $query_ch->row();

                    $id_doc = $docente->id;



                    echo "EL Docente".$key->documento." ".$key->apellido." ".$key->nombre." Existe en USERS <br>";

                    $sql_check_user_group= "SELECT * FROM users_groups WHERE user_id='".$id_doc."'and group_id = $group_id";

                    $query_ch_user_group = $this->db->query($sql_check_user_group);

                    if($query_ch_user_group->num_rows() > 0)

                    {

                        echo "EL Docente YA ES DOCENTE EN LA ESCUELA SELECCIONADA<br>";

                    }

                    else

                    {

                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)

                                VALUES ($id_doc, $group_id, $colegio_id, 1)";

                        $this->db->query($insert_users_groups);

                         //$cont = $cont + 1;

                    }

                    

                }

                

            } // fin for recorrido de los alumnos.

            echo "se insertaron: ".$cont." Docentes<br>";

        }

    }


    public function importar_alumnos_primaria()

    {

        //VARIABLES

        $tabla = "alumnos_nodos_dante_alighieri"; //--modificar por la division

       // $division_id = 63;

        

        $plan_estudio_id = 3;

        $cicloa = 2019;



        $sql = "SELECT * FROM alumnos_nodos_dante_alighieri where id_division in (361,362,363)";

        $cont = 0;

        $query = $this->db->query($sql);

        if($query->num_rows() > 0)

            foreach ($query->result() as $key => $value) 

            {

                

                

                    $group_id = 237;

                    $colegio_id = 19;

                

                $sql_check= "SELECT * FROM users WHERE documento='".$value->documento."'";

                $query_ch = $this->db->query($sql_check);

                if($query_ch->num_rows() == 0)

                {

                    $pass_alu = $this->crear_pass($value->documento);

                    if($value->sexo == 1)

                    {

                        $sexo = 'M';

                    }

                    else

                    {

                        $sexo = 'F';

                    }

                   // $insert_alumno = "INSERT INTO users (first_name, last_name, active, documento, password, recibir_notificacion)

                          //  VALUES ('$value->nombre', '$value->apellido',1, '$value->dni','$pass_alu',1)";

                    $insert_alumno = "INSERT INTO users (first_name,last_name, active, documento, password, recibir_notificacion,pago,sexo)

                            VALUES ('$value->nombre','$value->apellido',1, '$value->documento','$pass_alu',1,1,'$sexo')";

                    if($this->db->query($insert_alumno))

                    {

                        $id_user_alumno = $this->db->insert_id();                    

                        //insertar en users_groups

                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)

                                VALUES ($id_user_alumno, $group_id, $colegio_id,1)";

                        if($this->db->query($insert_users_groups))

                        {

                            //insertar en inscripcion

                            $id_alumno = $this->db->insert_id();

                            $insert_inscripcion = "INSERT INTO inscripciones (alumno_id,activo, cicloa,divisiones_id)

                                    VALUES ($id_alumno,1,$cicloa,$value->id_division)";

                                   //echo $insert_inscripcion."<br>";        



                                    if($this->db->query($insert_inscripcion))

                                    {

                                        echo "OK -".$value->documento." ".$value->apellido."<br>";

                                        //echo "OK -".$value->dni." ".$value->apellido." ".$value->nombre."<br>";

                                        $cont = $cont + 1;

                                    }else

                                    {

                                      echo "NoInsertoAlumno: ".$value->documento." ".$value->apellido." ".$value->nombre."<br>";

                                      //echo "NoInsertoAlumno: <br>";

                                    }

                        }    

                    }





                }

                else

                {

                    echo "ELAlumnoExiste: ".$value->documento." ".$value->apellido." ".$value->nombre." <br>";





                }

            echo "se insertaron: ".$cont." alumnos";

            }

    }
    public function importar_alumnos_secundaria()

    {

        //VARIABLES

        $tabla = "alumnos_nodos_dante_alighieri"; //--modificar por la division

       // $division_id = 63;

        

        $plan_estudio_id = 3;

        $cicloa = 2019;



        $sql = "SELECT * FROM alumnos_nodos_dante_alighieri where id_division in (372)";

        $cont = 0;

        $query = $this->db->query($sql);

        if($query->num_rows() > 0)

            foreach ($query->result() as $key => $value) 

            {

                

                

                    $group_id = 251;

                    $colegio_id = 20;

                

                $sql_check= "SELECT * FROM users WHERE documento='".$value->documento."'";

                $query_ch = $this->db->query($sql_check);

                if($query_ch->num_rows() == 0)

                {

                    $pass_alu = $this->crear_pass($value->documento);

                    if($value->sexo == 1)

                    {

                        $sexo = 'M';

                    }

                    else

                    {

                        $sexo = 'F';

                    }

                   // $insert_alumno = "INSERT INTO users (first_name, last_name, active, documento, password, recibir_notificacion)

                          //  VALUES ('$value->nombre', '$value->apellido',1, '$value->dni','$pass_alu',1)";

                    $insert_alumno = "INSERT INTO users (first_name,last_name, active, documento, password, recibir_notificacion,pago,sexo)

                            VALUES ('$value->nombre','$value->apellido',1, '$value->documento','$pass_alu',1,1,'$sexo')";

                    if($this->db->query($insert_alumno))

                    {

                        $id_user_alumno = $this->db->insert_id();                    

                        //insertar en users_groups

                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)

                                VALUES ($id_user_alumno, $group_id, $colegio_id,1)";

                        if($this->db->query($insert_users_groups))

                        {

                            //insertar en inscripcion

                            $id_alumno = $this->db->insert_id();

                            $insert_inscripcion = "INSERT INTO inscripciones (alumno_id,activo, cicloa,divisiones_id)

                                    VALUES ($id_alumno,1,$cicloa,$value->id_division)";

                                   //echo $insert_inscripcion."<br>";        



                                    if($this->db->query($insert_inscripcion))

                                    {

                                        echo "OK -".$value->documento." ".$value->apellido."<br>";

                                        //echo "OK -".$value->dni." ".$value->apellido." ".$value->nombre."<br>";

                                        $cont = $cont + 1;

                                    }else

                                    {

                                      echo "NoInsertoAlumno: ".$value->documento." ".$value->apellido." ".$value->nombre."<br>";

                                      //echo "NoInsertoAlumno: <br>";

                                    }

                        }    

                    }





                }

                else

                {

                    echo "ELAlumnoExiste: ".$value->documento." ".$value->apellido." ".$value->nombre." <br>";





                }

            echo "se insertaron: ".$cont." alumnos";

            }

    }
    public function importar_alumnos_niv_inicial()

    {

        //VARIABLES

        $tabla = "alumnos_santo_domingo_niv_inicial"; //--modificar por la division

       // $division_id = 63;

        

        $plan_estudio_id = 3;

        $cicloa = 2019;



        $sql = "SELECT * FROM alumnos_santo_domingo_niv_inicial where documento <> '' ";

        $cont = 0;

        $query = $this->db->query($sql);

        if($query->num_rows() > 0)

            foreach ($query->result() as $key => $value) 

            {

                

                

                    $group_id = 207;

                    $colegio_id = 17;

              

                $sql_check= "SELECT * FROM users WHERE documento='".$value->documento."'";

                $query_ch = $this->db->query($sql_check);

                if($query_ch->num_rows() == 0)

                {

                    $pass_alu = $this->crear_pass($value->documento);

                  

                        $sexo = 'M';

                   

                   // $insert_alumno = "INSERT INTO users (first_name, last_name, active, documento, password, recibir_notificacion)

                          //  VALUES ('$value->nombre', '$value->apellido',1, '$value->dni','$pass_alu',1)";

                    $insert_alumno = "INSERT INTO users (first_name,last_name, active, documento, password, recibir_notificacion,pago,sexo)

                            VALUES ('$value->nombre','$value->apellido',1, '$value->documento','$pass_alu',1,1,'$sexo')";

                    if($this->db->query($insert_alumno))

                    {

                        $id_user_alumno = $this->db->insert_id();                    

                        //insertar en users_groups

                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)

                                VALUES ($id_user_alumno, $group_id, $colegio_id,1)";

                        if($this->db->query($insert_users_groups))

                        {

                            //insertar en inscripcion

                            $id_alumno = $this->db->insert_id();

                            $insert_inscripcion = "INSERT INTO inscripciones (alumno_id,activo, cicloa,divisiones_id)

                                    VALUES ($id_alumno,1,$cicloa,$value->id_division)";

                                   //echo $insert_inscripcion."<br>";        



                                    if($this->db->query($insert_inscripcion))

                                    {

                                        echo "OK -".$value->documento." ".$value->apellido."<br>";

                                        //echo "OK -".$value->dni." ".$value->apellido." ".$value->nombre."<br>";

                                        $cont = $cont + 1;

                                    }else

                                    {

                                      echo "NoInsertoAlumno: ".$value->documento." ".$value->apellido." ".$value->nombre."<br>";

                                      //echo "NoInsertoAlumno: <br>";

                                    }

                        }    

                    }





                }

                else

                {

                    echo "ELAlumnoExiste: ".$value->documento." ".$value->apellido." ".$value->nombre." <br>";





                }

            echo "se insertaron: ".$cont." alumnos";

            }

    }

    public function importar_preceptores()

    {

        $colegio_id = 9;

        $group_id = 101;

      

        $bf_preceptores_nodos = "bf_preceptores_nodos";

        $sql2 = "SELECT * FROM ".$bf_preceptores_nodos;

        $query_preceptores = $this->db->query($sql2);



       

        if($query_preceptores->num_rows() > 0)

        {

            //print_r($query_alumnos->num_rows())."<br>";

            foreach ($query_preceptores->result() as $key)

            {

                $dni = $key->dni;



                $sql_preceptor_aux = "SELECT * FROM users WHERE documento = '$dni'";

                $sql_preceptor_aux = $this->db->query($sql_preceptor_aux);

                if($sql_preceptor_aux->num_rows() > 0)

                {

                    echo "YaExiste Preceptor:".$key->apellido.", ".$key->nombre." <br>";

                    $users_preceptor = $sql_preceptor_aux->row();

                    //averiguar si ya esta en user_groups

                    $sql_users_group_colegio = "SELECT users_groups.* FROM users 

                                                            JOIN users_groups on users_groups.user_id =  users.id 

                                                            WHERE users.documento = $key->dni and users_groups.colegio_id = $colegio_id and users_groups.group_id = $group_id";

                    $query_users_groups = $this->db->query($sql_users_group_colegio);

                    if($query_users_groups->num_rows() > 0)//existe en el colegio y tiene el mismo rol

                    {

                        $query_users_groups = $query_users_groups->row();



                        $insert_preceptor_tabla_pre = "INSERT INTO preceptores (user_id, division_id, colegio_id, planesestudio_id, activo)

                        VALUES ($query_users_groups->id, $key->id_div_nodos, $colegio_id, 3, 1)";

                        if($this->db->query($insert_preceptor_tabla_pre))

                        {

                            echo "ok Insertado en preceptores<br>";

                        }

                    }

                    else

                    {

                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)

                                                        VALUES ($users_preceptor->id, $group_id, $colegio_id, 1)";

                                if($this->db->query($insert_users_groups))

                                {

                                    $id_user_group = $this->db->insert_id();

                                    $insert_preceptor_tabla_pre = "INSERT INTO preceptores (user_id, division_id, colegio_id, planesestudio_id, activo)

                                                        VALUES ($id_user_group, $key->id_div_nodos, $colegio_id, 3, 1)";

                                    if($this->db->query($insert_preceptor_tabla_pre))

                                    {

                                        echo "ok Insertado en preceptores<br>";

                                    }



                                    

                                }

                    }

                }

                else

                {

                    $pass = $this->crear_pass($dni);

                    

                    $insert_preceptor = "INSERT INTO users (first_name, last_name, active, password, pago, documento)

                                                VALUES ('$key->nombre', '$key->apellido',1, '$pass',1, $dni)";



                            if($this->db->query($insert_preceptor))

                            {

                               $user_id = $this->db->insert_id();

                               echo "ok Insertado en users_groups<br>";

                               //-- USERS_GRUPS Insert ---

                                $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)

                                                        VALUES ($user_id, $group_id, $colegio_id, 1)";

                                if($this->db->query($insert_users_groups))

                                {

                                    $id_user_group = $this->db->insert_id();

                                    $insert_preceptor_tabla_pre = "INSERT INTO preceptores (user_id, division_id, colegio_id, planesestudio_id, activo)

                                                        VALUES ($id_user_group, $key->id_div_nodos, $colegio_id, 3, 1)";

                                    if($this->db->query($insert_preceptor_tabla_pre))

                                    {

                                        echo "ok Insertado en preceptores<br>";

                                    }



                                    

                                }

                            }

                            else

                            {

                                echo "NO INSERTADO :".$key->apellido.", ".$key->nombre." <br>";

                            }





                }

                



                    

                

            }



        }

    }

    public function importar_administrativos()

    {

        //VARIABLES

        $tabla = "bf_administrativos_nodos"; //--modificar por la division

        

        $group_id = 105;

        $colegio_id = 9;

        //$plan_estudio_id = 1;



        //SCRIPT        

        $sql = "SELECT * FROM ".$tabla;

        $cont = 0;

        $query = $this->db->query($sql);

        if($query->num_rows() > 0)

        {

            foreach ($query->result() as $key) 

            {

                $sql_check= "SELECT * FROM users WHERE documento='".$key->dni."'";

                $query_ch = $this->db->query($sql_check);

                if($query_ch->num_rows() == 0)

                {

                    $pass_alu = $this->crear_pass($key->dni);

                   



                    $insert_docente = "INSERT INTO users (first_name, last_name, active, documento, password)

                            VALUES ('$key->nombre', '$key->apellido',1, '$key->dni','$pass_alu')";

                                

                   if($this->db->query($insert_docente))

                   {

                        $id_user_docente = $this->db->insert_id();                    

                        //insertar en users_groups

                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)

                                VALUES ($id_user_docente, $group_id, $colegio_id, 1)";

                        $this->db->query($insert_users_groups);

                         $cont = $cont + 1;

                           

                    }

                } // fin if el alumno ya existe.

                else{

                    $docente = $query_ch->row();

                    $id_doc = $docente->id;



                    echo "EL Docente".$key->dni." ".$key->apellido." ".$key->nombre." Existe en USERS <br>";

                    $sql_check_user_group= "SELECT * FROM users_groups WHERE user_id='".$id_doc."'and group_id = $group_id";

                    $query_ch_user_group = $this->db->query($sql_check_user_group);

                    if($query_ch_user_group->num_rows() > 0)

                    {

                        echo "EL administrativo YA ES administrativo EN LA ESCUELA SELECCIONADA<br>";

                    }

                    else

                    {

                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)

                                VALUES ($id_doc, $group_id, $colegio_id, 1)";

                        $this->db->query($insert_users_groups);

                         //$cont = $cont + 1;

                    }

                    

                }

                

            } // fin for recorrido de los alumnos.

            echo "se insertaron: ".$cont." administrativo<br>";

        }

    }

    public function importar_tutores()

    {

        //$colegio_id = 9;

        //$group_id = 103;

       



        $tabla_tutores = "tutores_nodos_dante_alighieri where id_division in (371,372)";

        $sql2 = "SELECT * FROM ".$tabla_tutores;

        $query_tutores = $this->db->query($sql2);

        

        foreach ($query_tutores->result() as $tutor)

                {

                    if($tutor->id_division > 363)// es DANTE SECUNDARIA

                    {

                        $group_id = 250;

                        $colegio_id = 20;

                    }

                    else

                    {

                        $group_id = 244;

                        $colegio_id = 19;

                    }

                    $sql_alumno_aux = "SELECT * FROM users WHERE documento = $tutor->documento_alumno";

                    $query_alumno_aux = $this->db->query($sql_alumno_aux);

                    $alumno_aux = $query_alumno_aux->row();

                    $id_alumno = $alumno_aux->id;

                    if($id_alumno != null)

                    {

                    //echo "encontrado";

                    //validar si existe en users el tutor

                        $sql_users = "SELECT * FROM users WHERE documento = '$tutor->documento'";

                  

                        $query_users = $this->db->query($sql_users);

                        print_r($this->db->last_query());

                        if($query_users->num_rows() > 0)

                        {

                            foreach ($query_users->result() as $user)

                            {

                                echo "tutor encontrado";

                                //ver si es padre esta en el mismo colegio

                                $sql_users_group_colegio = "SELECT * FROM users 

                                                            JOIN users_groups on users_groups.user_id =  users.id 

                                                            WHERE users.documento = $tutor->documento and users_groups.colegio_id = $colegio_id";

                                $query_users_groups = $this->db->query($sql_users_group_colegio);

                                if($query_users_groups->num_rows() > 0)//existe en el colegio

                                {

                                    //verificar que no exista la relacion en tutoralumno

                                    $sql_tutor_alumno_aux = "SELECT * FROM tutoralumno where alumno_id = $id_alumno and tutor_id = $user->id";

                                    $query_tutor_alumno_aux = $this->db->query($sql_tutor_alumno_aux);

                                    if($query_tutor_alumno_aux->num_rows() > 0)

                                    {

                                        echo "Existe Relacion alumno tutor (user_id: ".$user->id.")<br>";

                                    }

                                    else

                                    {

                                        $table = 'tutoralumno';

                                    

                                        $insert_relacion = "INSERT INTO tutoralumno (alumno_id,tutor_id)

                                        VALUES ($id_alumno,$user->id)";



                                       if($this->db->query($insert_relacion))

                                       {

                                        echo "ok Padre(".$tutor->documento." ".$tutor->apellido." ".$tutor->nombre.") Insertado en la Relacion alumno(".$tutor->documento_alumno." ".$alumno_aux->first_name." ".$alumno_aux->last_name.") tutor<br>";

                                       }

                                    }

                                    //--- TUTORALUMNO insert

                                    

                                    

                                }

                                else

                                {

                                    

                                    $sql_tutor_alumno_aux = "SELECT * FROM tutoralumno where alumno_id = $id_alumno and tutor_id = $user->id";

                                    $query_tutor_alumno_aux = $this->db->query($sql_tutor_alumno_aux);

                                    if($query_tutor_alumno_aux->num_rows() > 0)

                                    {

                                        echo "Existe Relacion alumno tutor <br>";

                                    }

                                    else

                                    {

                                        //-- USERS_GRUPS Insert ---

                                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)

                                                                VALUES ($user->id, $group_id, $colegio_id, 1)";

                                        if($this->db->query($insert_users_groups)){

                                            $id_user_group = $this->db->insert_id();

                                        } 

                                        //--- TUTORALUMNO insert

                                        $table = 'tutoralumno';

                                        

                                            $insert_relacion = "INSERT INTO tutoralumno (alumno_id,tutor_id)

                                            VALUES ($id_alumno,$user->id)";



                                           if($this->db->query($insert_relacion))

                                           {

                                            echo "ok Padre(".$tutor->documento." ".$tutor->apellido." ".$tutor->nombre.") Insertado en la Relacion alumno(".$tutor->documento_alumno." ".$alumno_aux->last_name." ".$alumno_aux->first_name.") tutor<br>";

                                           }   

                                    }

                                    

                                    



                                    //die();

                                }



                                

                            }

                        }

                        else

                        {

                            $pass_tutor= $this->crear_pass($tutor->documento);

                            $active = 1; 

                            $pago = 1;

                            

                               // $email = $tutor->email;

                            

                            

                                //$sexo_tutor = $tutor->sexo;

                           

                            //$insert_tutor = "INSERT INTO users (first_name, last_name, active, email, password, sexo, pago, documento)

                                              //  VALUES ('$tutor->nombre', '$tutor->apellido',$active,'$email', '$pass_tutor', '$sexo_tutor',1, '$tutor->documento')";
                            $email = '';
                            if($tutor->email != '')
                            {
                                $email = $tutor->email;
                            }
                                
                            $insert_tutor = "INSERT INTO users (first_name, last_name, active, password, pago, documento, email) VALUES ('$tutor->nombre', '$tutor->apellido',$active, '$pass_tutor',1, '$tutor->documento','$email')";

                            

                            if($this->db->query($insert_tutor))

                            {

                               $id_tutor = $this->db->insert_id();

                               echo "ok Padre nuevo Insertado <br>";

                               //-- USERS_GRUPS Insert ---

                                $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)

                                                        VALUES ($id_tutor, $group_id, $colegio_id, 1)";

                                if($this->db->query($insert_users_groups))

                                {

                                    $id_user_group = $this->db->insert_id();

                                }    

                               //--- TUTORALUMNO insert

                                $table = 'tutoralumno';

                                

                                    $insert_relacion = "INSERT INTO tutoralumno (alumno_id,tutor_id)

                                    VALUES ($id_alumno,$id_tutor)";



                                   if($this->db->query($insert_relacion))

                                   {

                                    echo "ok Padre(".$tutor->documento." ".$tutor->apellido." ".$tutor->nombre.") Insertado en la Relacion alumno(".$tutor->documento_alumno." ".$alumno_aux->last_name." ".$alumno_aux->first_name.") tutor<br>";

                                   }

                                

                                else

                                {

                                   echo "YaExiste-R-P-A <br>";

                                }   

                            

                            }

                            else

                            {

                              echo "PadreYaInsertado <br>";

                            }

                            

                        }

                        //print_r($this->db->last_query());

                        //die();

                    }

                   

                

                }

        

    }

  
  


   

   

    

    

    //



}