<?php //echo ($users->id);//echo $users; ?>
<?php //var_dump($code);//echo $users; ?>
<script src="<?=base_url('assets/js/bootstrap.min.js');?>"></script>
<script>

    var passApiKey = '<?php echo $passApiKey; ?>';
    var urlApi = '<?php echo $urlApi; ?>';
    var user_id = '<?php echo $users->id; ?>';
    var code = '<?php echo $code; ?>';
 

$(document).ready(function(){
	 
    $.noConflict();  
	
   
    //-------------------------------------------//
    //----------- Click en LOGIN ---------------//
	//-------------------------------------------//
    $('#login').click(function(event){    
       
        event.preventDefault();
        var pass = $('#clave').val();
        var repass = $('#repite_clave').val();
        
        $('.error').fadeOut();
	   //$('.ErrorPin').fadeOut();
        if( pass != "")
        {
          if($('#clave').val().length > 4)
          {   
            if( repass != "")
            {
              if(pass != repass)
              {
                    var msj = "Las contraseñas no coinciden";
                    $('#repite_clave').val(''); 
                    $("#repite_clave").focus().after("<span class='error'>"+msj+"</span>");      
                    return false;
              }
              else
              {
					      $('.error').fadeOut(); 
					      prep_datos(pass,code);
              }
			      }
			      else
			      {
				      $('.error').fadeOut(); 
				      var msj = "Repita la Contraseña";
					    $('#repite_clave').val('');
				      $("#repite_clave").focus().after("<span class='error'>"+ msj +"</span>");
					
					    return false;
			      }
          }
          else
          {
                var msj = "La Contraseña debe tener mínimo 6 caracteres";
                $('#clave').val('');
                $('.error').fadeOut(); 
                $("#clave").focus().after("<span class='error'>"+ msj +"</span>");
                return false;
          }
      	}
      	else
      	{
      	    var msj = "Complete la Contraseña";
            $('#clave').val('');
			$('.error').fadeOut(); 
            $("#clave").focus().after("<span class='error'>"+ msj +"</span>");
			return false;
      	}

	  
       // ingresar();

    }); // fin function Login
	
	//-------------------------------------------------
	//--------- VAlidar Formulario --------------------
	//-------------------------------------------------
	function validar_formulario()
	{
		
		var pass = $('#clave').val();
        var repass = $('#repite_clave').val();
        $('.error').fadeOut();
	   //$('.ErrorPin').fadeOut();
        if( pass != "")
        {
          if($('#clave').val().length > 5)
          {  
            if( repass != "")
            {
              if(pass != repass)
              {
                var msj = "Las contraseñas no coinciden";
                $('#repite_clave').val(''); 
                $("#repite_clave").focus().after("<span class='error'>"+msj+"</span>");      
                return false;
              }
              else
              {
  					    $('.error').fadeOut(); 
                       // Si las contraseñas coinciden -- Verifica Pin  //
  				    }
  			    }
  			    else
  			    {
  				    $('.error').fadeOut(); 
  				    var msj = "Repita la Contraseña";
  					  $('#repite_clave').val('');
  				    $("#repite_clave").focus().after("<span class='error'>"+ msj +"</span>");
  					  return false;
  			    }
          }
          else
          {
            var msj = "Complete la Contraseña";
            $('#clave').val('');
            $('.error').fadeOut(); 
            $("#clave").focus().after("<span class='error'>"+ msj +"</span>");
            return false;
          }
      	}
      	else
      	{
      	  var msj = "Complete la Contraseña";
          $('#clave').val('');
			    $('.error').fadeOut(); 
          $("#clave").focus().after("<span class='error'>"+ msj +"</span>");
			    return false;
      	}
		
	}

    $(document).keypress(function(e) {
        if(e.which == 13) {
            //ingresar();  // arreglar.
			validar_formulario();
        }
    });

  }); //-- fin Document Ready.

 //-----------------------------------
	function enableSubmit (idForm) {
	  $(idForm + " button.submit").removeAttr("disabled");
	}
	 
	function disableSubmit (idForm) {
	  $(idForm + " button.submit").attr("disabled", "disabled");
	}

</script>
<!-- Modal de Error-->
        <div class="modal fade" id="modal_msj" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titulo_error"></h4>
              </div>
              <div class="modal-body">
                  
                    <div class="form-group">
                      <center><label id="error_msj_pass" class="control-label" ></label></center>
                      <h4></h4>
                    </div>
                  
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" id="cerrar_modal_msj"  >Cerrar</button>
              </div>
            </div>
          </div>
        </div>
        <script type="text/javascript">
$(document).ready(function()
    {
    	$('#cerrar_modal_msj').click(function(event) {
    		$('#modal_msj').modal('show');
    		document.location.href = "<?php echo base_url();?>";
    		/* Act on the event */
    	});
    })
        </script>
        
<script type="text/javascript">
	function prep_datos( password, code){
     
        var urlRequest = urlApi+"/login/reset_contrasenia";                           
   
        $.ajax({
            url: urlRequest,
            type: 'POST',
            headers: {    
					  'APIKEY' : passApiKey
            },
            data: {password:password, code:code, user_id:user_id},
            beforeSend: function() {
                    $('.top-right').notify({
                        message: { text: 'Cargando...' },
                        fadeOut: { enabled: true, delay: 2500 },
                        type: 'success2'
                    }).show(); 
					//$('.login').after("<img id='login src='<?=base_url('assets/images/loading.gif');?>' height='30' width='30' />");
            },
            success: function(data){
				//$('#idusuario').val(idUser); 
				/*$('.top-right').notify({
                            message: { text: data['message'] },
                            fadeOut: { enabled: true, delay: 4500 },
                            type: 'success2'
                }).show(); */
		           
				/*bootbox.alert(data['message'], function() { 
				     document.forms['formChangePass'].submit();
				});
				console.log(data);*/
                                document.getElementById("titulo_error").innerHTML = 'Exito';
                                document.getElementById("error_msj_pass").innerHTML = data['message'];
                $('#modal_msj').modal('show');
				//console.log(data);
	
            },
            error: function(response){
                  //console.log(response);
                  console.log(response)
                  if(response['responseText'])
                  { 
                    response = JSON.parse(response['responseText'],true);
                   
                    $('.top-right').notify({
                          message: { text: response['message']},
                          fadeOut: { enabled: true, delay: 3000 },
                           type: 'danger2'
                        }).show();                       
                    }
            }
        });
}
</script>	

<body>

	<!-- count particles -->
	<div class="count-particles">
		<span class="js-count-particles">80</span> particles
	</div>

	<!-- particles.js container -->
	<div id="particles-js"><canvas class="particles-js-canvas-el" width="1059" height="599" style="width: 100%; height: 100%;"></canvas></div>

	<link rel="shortcut icon" href="http://works.sietedefebrero.com/nodos/dev/favicon.ico?v=2" type="image/x-icon">
	<link rel="icon" href="http://works.sietedefebrero.com/nodos/dev/favicon.ico?v=2" type="image/x-icon">
	
	<link href="<?=base_url('assets/css/style.css');?>" rel="stylesheet">	
	<!-- Google Fonts -->
	<link href="<?=base_url('assets/css/css');?>" rel="stylesheet" type="text/css"> 

	<!-- scripts -->
	<script src="<?=base_url('assets/js/particles/particles.js');?>"></script>
	<script src="<?=base_url('assets/js/particles/app.js');?>"></script>

	<!-- stats.js -->
	<script src="<?=base_url('assets/js/particles/stats.js');?>"></script>
	<script>
	var count_particles, stats, update;
	stats = new Stats;
	stats.setMode(0);
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.left = '0px';
	stats.domElement.style.top = '0px';
	document.body.appendChild(stats.domElement);
	count_particles = document.querySelector('.js-count-particles');
	update = function() {
		stats.begin();
		stats.end();
		if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
			count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
		}
		requestAnimationFrame(update);
	};
	requestAnimationFrame(update);
	</script>
	
	
	<div class="container">
		<div class="row">
			<div class="col-lg-offset-4 col-lg-4 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-12">
				
				<div class="login" style="margin-top: 100px;">
				 <img src="<?=base_url('assets/images/logo-login.png');?>" width="90" alt="NODOS">

				<form method="post" action="login" id="formChangePass">
                 <input id='clave' type="password" name="password" required placeholder="Nueva Contraseña">
			     <input id='repite_clave' type="password" name="repite_password" required placeholder="Repita Contraseña">
                  <input id='code' type="hidden" name="code" value = <?php echo $code?>>
                  <input id='user_id' type="hidden" name="user_id" value = <?php echo $users->id?>>

                  <!--<span class="warning2">This field is required</span> -->
                  <input id='login' type="button" value="Cambiar Contraseña">
			    </form>
				</div>
				
			</div>
		</div>
	</div>
	
  <div class='notifications top-right'></div>
 
</body></html>