    <?class Torneos extends CI_Controller 
    {	
    	function __construct()
       {

          parent::__construct();

          if(!$this->session->userdata("logged_in")){
           redirect("login");
       }
   }
   public function index(){

    		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
            $passApiKey = $this->variables->get_apikey();
            $data['urlApi'] = $urlApi;
            $data['passApiKey'] = $passApiKey;
            $data['idusuario'] = $this->session->userdata('idusuario');
            $data['idcolegio'] = $this->session->userdata('idcolegio');
            $data['idgrupo'] = $this->session->userdata('idgrupo');
            $data['nombregrupo'] = $this->session->userdata('nombregrupo');
            $data['club'] = $this->mis_funciones->get_club();




            $data['opcion_menu'] = 'Usuario';
            $band = 0;
            $controlador = "Usuario";
            foreach ($this->session->userdata('menu') as $menu) //validar permiso de controlador y metodo
            {
                if(($menu->controlador == $controlador) && ($menu->metodo == 'index'))
                {
                            $band = 1;// encontro el controlador y el metodo en la sesion, el user tiene permiso
                        }
                    }
                    if($band == 1)
                    {
                    //echo "hola tutores";


                        $data['opcion_menu'] = 'usuarios';
                        $data['contenido'] = 'admin/addtorneo';
                    //$data['contenido'] = 'panel/vista_horarios';
                        $this->load->view('include/template_colegio',$data);

                    }
                    else redirect(404);        

                }
                public function crear_torneo()
                { 
                 $fechaig1= explode('-',$this->input->post('fechacomienzo'));
                 $fechaig2= explode('-',$this->input->post('fechafin'));
                 $arreglo= $this->input->post('campo_clubesparticipante'); 
                 $cparti=implode(",",  $arreglo);
                 $torneo = array(
                    'txt_torneo' => $this->input->post('campo_nametorneo'),
                    'txt_organizador' => $this->input->post('campo_organizador') ,
                    'txt_cparticipantes' => $cparti,
                    'txt_campo_descripcion' =>  $this->input->post('campo_descripcion'),
                    'txt_fechacomienzo' => $fechaig1[2]."-".$fechaig1[1]."-".$fechaig1[0],
                    'txt_fechafin' => $fechaig2[2]."-".$fechaig2[1]."-".$fechaig2[0]
                );
                 $urlApi = $this->variables->get_urlapi(); 
                 $passApiKey = $this->variables->get_apikey();

                 $idusuario = $this->session->userdata('idusuario');
                 $url = $urlApi."/Torneo/guardar_torneo/";
                 $ch = curl_init();
                 $options = array(
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_URL => $url,
                    CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => $torneo,
                    CURLOPT_SSL_VERIFYPEER=> false,
                );
                 curl_setopt_array( $ch, $options);
                 $response = curl_exec($ch);
                 curl_close($ch);
                 var_dump($response);
            // echo $response;
            // die();        
                 if ( $response== 200) {

                    $data['confirmacion'] = "Se guardo el torneo satifactoriamente";
                    $data['opcion_menu']  = 'usuarios';
                    $data['contenido']    = 'admin/addtorneo';
                //$data['contenido'] = 'panel/vista_horarios';
                    print_r($data);
                }else echo "error";



            }
            public function obtener_torneos()
            {

         $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
         $passApiKey = $this->variables->get_apikey();

         $idusuario = $this->session->userdata('idusuario');
         $url = $urlApi."/Torneo/listartorneo/";
         $ch = curl_init();
         $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
            CURLOPT_POST => true,
                // CURLOPT_POSTFIELDS => "",
            CURLOPT_SSL_VERIFYPEER=> false,
        );
         curl_setopt_array( $ch, $options);
         $response = curl_exec($ch);
         curl_close($ch);
            //var_dump($response);
            // $response=json_decode($response);                  
         print_r($response); 


     } 
     public function obtener_torneo_x_id()
            {

         $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
         $passApiKey = $this->variables->get_apikey();
         $arre = array('id' => $this->input->post('tor'));
         
         $idusuario = $this->session->userdata('idusuario');
         $url = $urlApi."/Torneo/obtener_torneo_x_id/";
         $ch = curl_init();
         $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $arre,
            CURLOPT_SSL_VERIFYPEER=> false,
        );
         curl_setopt_array( $ch, $options);
         $response = curl_exec($ch);
         curl_close($ch);
            //var_dump($response);
            // $response=json_decode($response);                  
         print_r($response); 


     }

     public function inscripcion()
     {
            //echo $this->session->userdata('idcolegio');    
            $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
            $passApiKey = $this->variables->get_apikey();
            $data['urlApi'] = $urlApi;
            $data['passApiKey'] = $passApiKey;
            $data['idusuario'] = $this->session->userdata('idusuario');
            $data['idcolegio'] = $this->session->userdata('idcolegio');

            $data['idgrupo'] = $this->session->userdata('idgrupo');
            $data['nombregrupo'] = $this->session->userdata('nombregrupo');
            //$data['club'] = $this->mis_funciones->get_club($this->session->userdata('coleio'));
            //
            //
            
            $club=$this->session->userdata('colegios');





            $data['opcion_menu'] = 'Usuario';
            $band = 0;
            $controlador = "Usuario";
            foreach ($this->session->userdata('menu') as $menu) //validar permiso de controlador y metodo
            {
                if(($menu->controlador == $controlador) && ($menu->metodo == 'index'))
                {
                            $band = 1;// encontro el controlador y el metodo en la sesion, el user tiene permiso
                        }
                    }
                    if($band == 1)
                    {
                    //echo "hola tutores";


                        $data['opcion_menu'] = 'usuarios';
                        $data['contenido'] = 'admin/inscripcion_torneo';
                        //$data['contenido'] = 'panel/vista_horarios';
                        $this->load->view('include/template_colegio',$data);
                    }

                }


                public function obtener_torneo_p_club(){

                    $club = array('idclub' => $this->input->post('idclub'));     
            $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas 
            $passApiKey = $this->variables->get_apikey();

            $idusuario = $this->session->userdata('idusuario');
            $url = $urlApi."/Torneo/obtener_torneoxclub/";
            $ch = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $club,
                CURLOPT_SSL_VERIFYPEER=> false,
            );
            curl_setopt_array( $ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
            //var_dump($response);
            //$response = json_encode($response);
            //return $response;      
            print_r($response);
        }
        public function inscribir_a_torneo(){
            
            $arre = array(
                'fechainscr'=> strtotime(date("Y-m-d")), 
                'idtorneo' => $this->input->post('tor'),
                'iddeportista' => $this->input->post('dep'),
                'idespecialidad' => $this->input->post('esp'),
                'ideficiencia' => $this->input->post('efi'),
                'iddivision' => $this->input->post('div'),
                'idcategoria' => $this->input->post('cat'),                
                'idclub' => $this->input->post('cl')            
            );     
            $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas 
            $passApiKey = $this->variables->get_apikey();

            $idusuario = $this->session->userdata('idusuario');
            $url = $urlApi."/Torneo/inscribir_a_torneo/";
            $ch = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $arre,
                CURLOPT_SSL_VERIFYPEER=> false,
            );
            curl_setopt_array( $ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
            //var_dump($response);
            //$response = json_encode($response);
            //return $response;    
            
            print_r($response);
        }
        public function obtener_inscripcion_a_torneo(){

            $arre = array(
                'idtorneo' => $this->input->post('tor'),
                'iddeportista' => $this->input->post('dep'),
                'idespecialidad' => $this->input->post('esp'),
                'iddivision' => $this->input->post('div'),
                'idclub' => $this->input->post('cl')            
            );  

            $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas 
            $passApiKey = $this->variables->get_apikey();

            $idusuario = $this->session->userdata('idusuario');
            $url = $urlApi."/Torneo/obtener_inscriptos_torneo/";
            $ch = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $arre,
                CURLOPT_SSL_VERIFYPEER=> false,
            );
            curl_setopt_array( $ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
            //var_dump($response);
            //$response = json_decode($response);
            print_r($response);    
            
            //return $response;
        }
        public function listar_inscriptos_torneo(){

            $arre = array(
                'idtorneo' => $this->input->post('tor'),
                
                'idclub' => $this->input->post('cl')            
            );  

            $urlApi = $this->variables->get_urlapi(); 
            $passApiKey = $this->variables->get_apikey();

            $idusuario = $this->session->userdata('idusuario');
            $url = $urlApi."/Torneo/listar_inscriptos_torneo/";
            $ch = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $arre,
                CURLOPT_SSL_VERIFYPEER=> false,
            );
            curl_setopt_array( $ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
            //var_dump($response);
            //$response = json_encode($response);
            print_r($response);
             
        }
        public function buscar_torneo(){

            $torneo=$this->input->post('tor');

            $urlApi = $this->variables->get_urlapi(); 
            $passApiKey = $this->variables->get_apikey();       
            $idusuario = $this->session->userdata('idusuario');
            $url = $urlApi."/Torneo/buscar_torneo";
            $arre = array('idtorneo' => $torneo);
            $ch = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $arre ,
                CURLOPT_SSL_VERIFYPEER=> false,
            );
            curl_setopt_array( $ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
            //var_dump($response);
            //$response = json_encode($response);
            //return $response;      
            print_r($response);
        }

        public function editar_torneo(){
           
         $fechaig2= explode('-',$this->input->post('editar_fechafin'));

         $fechaig1= explode('-',$this->input->post('editar_fechacomienzo'));
        $arreglo= $this->input->post('editar_campo_clubesparticipante'); 
       /* print_r($arreglo);
        die();*/
         $cparti=implode(",",  $arreglo);
         $torneo = array(
            'txt_idtorneo'=> $this->input->post('editar_idtorneo'),
            'txt_torneo' => $this->input->post('editar_campo_nametorneo'),
            'txt_organizador' => $this->input->post('editar_campo_organizador'),
            'txt_cparticipantes' => $cparti,
            'txt_campo_descripcion' =>  $this->input->post('editar_campo_descripcion'),
            'txt_fechacomienzo' => $fechaig1[2]."-".$fechaig1[1]."-".$fechaig1[0],
            'txt_fechafin' => $fechaig2[2]."-".$fechaig2[1]."-".$fechaig2[0]
         );

            $urlApi = $this->variables->get_urlapi(); 
            $passApiKey = $this->variables->get_apikey();
           
            $idusuario = $this->session->userdata('idusuario');
            $url = $urlApi."/Torneo/update_torneo";
            $ch = curl_init();
            $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $torneo,
            CURLOPT_SSL_VERIFYPEER=> false,
            );
            curl_setopt_array( $ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
            var_dump($response);
            // echo $response;
            // die();   
            print_r($response);
       
                     
    }
     public function obtener_club_x_torneo(){

            $arre = array(
                'idtorneo' => $this->input->post('tor'),
                
                           
            );  

            $urlApi = $this->variables->get_urlapi(); 
            $passApiKey = $this->variables->get_apikey();

            $idusuario = $this->session->userdata('idusuario');
            $url = $urlApi."/Torneo/obtener_club_x_torneo/";
            $ch = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $arre,
                CURLOPT_SSL_VERIFYPEER=> false,
            );
            curl_setopt_array( $ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
            // var_dump($response);
            //$response = json_decode($response);
            print_r($response);
             
        } 
        public function eliminar_torneo(){

            $arre = array(
                'idtorneo' => $this->input->post('torneo'),                
                           
            );  

            $urlApi = $this->variables->get_urlapi(); 
            $passApiKey = $this->variables->get_apikey();

            $idusuario = $this->session->userdata('idusuario');
            $url = $urlApi."/Torneo/eliminar_torneo/";
            $ch = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $arre,
                CURLOPT_SSL_VERIFYPEER=> false,
            );
            curl_setopt_array( $ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
            // var_dump($response);
            //$response = json_decode($response);
            print_r($response);
             
        }
public function eliminar_inscripto(){

            $arre = array(
                'idinsc' => $this->input->post('insc'),                
                           
            );  

            $urlApi = $this->variables->get_urlapi(); 
            $passApiKey = $this->variables->get_apikey();

            $idusuario = $this->session->userdata('idusuario');
            $url = $urlApi."/Torneo/eliminar_inscripto/";
            $ch = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $arre,
                CURLOPT_SSL_VERIFYPEER=> false,
            );
            curl_setopt_array( $ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
            // var_dump($response);
            //$response = json_decode($response);
            print_r($response);
             
        }
        public function traer_categoria_inscripcion(){

            $arre = array(
                'idinsc' => $this->input->post('ins'),                
                           
            );

            $urlApi = $this->variables->get_urlapi(); 
            $passApiKey = $this->variables->get_apikey();

            $idusuario = $this->session->userdata('idusuario');
            $url = $urlApi."/Torneo/traer_categoria_inscripcion/";
            $ch = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL => $url,
                CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $arre,
                CURLOPT_SSL_VERIFYPEER=> false,
            );
            curl_setopt_array( $ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);
            //var_dump($response);
            //$response = json_encode($response);
            print_r($response);
             
        }

}?>