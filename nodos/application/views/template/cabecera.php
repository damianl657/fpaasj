<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>F.P.A.A.S.J.</title>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

    <!-- Bootstrap -->
    <link href="<?= base_url('vendors'); ?>/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    <!-- Font Awesome -->
    <link href="<?= base_url('vendors'); ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?= base_url('vendors') ?>/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?= base_url('vendors') ?>/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- jVectorMap -->
    <link href="<?= base_url('vendors') ?>/css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="<?= base_url('vendors') ?>/css/custom.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?= base_url('vendors') ?>/css/custom.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="<?= base_url('vendors') ?>/select2/dist/css/select2.min.css" rel="stylesheet">

     <!-- Datatables -->
    <link href="<?= base_url('vendors') ?>/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url('vendors') ?>/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url('vendors') ?>/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url('vendors') ?>/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url('vendors') ?>/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
  </head>

  <body class="nav-md" style="background:#092c4e;">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0; ">
             
              <a href="<?php base_url('home');?>" class="site_title" style="background:#092c4e; color: #164d88" ><i class="fa fa-windows"></i> <span style="font-size: 12px"><?//=$this->mis_funciones->get_nombre_colegio()?></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile" style="background:#092c4e;">
              <div class="profile_pic">
                
                <img src="<?=base_url('vendors/avatar_2x.png')?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Bienvendido,</span>
                <h2><?=$this->session->userdata('nombreusuario')?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />
<div id="wrapper">
        <?php //$this->load->view("include/particicles");
     $this->load->view("include/vista_inc_menu_colegio"); 
     $this->load->view("include/particicles");
     ?>
        <?php
        if(!empty($contenido))
                            $this->load->view($contenido);
        
        /*if(!empty($contenido2))
                            $this->load->view($contenido2);*/
        ?>
      
    
      <footer>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center">
                       <?php 
                          date_default_timezone_set('America/Argentina/San_Juan');?>
                           
                             <center><img src=<?=base_url("assets/images/i/icon-user.png")?> width="40" alt="NODOS"/><p>&copy; <? echo date("Y"); ?></p></center>
          </div>
      </footer>
    </div>

            
          </div>
        </div>
        <? include("application/views/template/top_navigation.php"); ?>
        


        
        
        