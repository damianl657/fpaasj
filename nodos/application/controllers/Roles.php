﻿<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Roles extends CI_Controller {	
      
	function __construct(){
		parent::__construct();
			
		if(!$this->session->userdata("logged_in")){
               redirect("login");
        }
	}		

    //-------------------------------------------------------

    public function index(){ 
		
		$urlApi               = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey           = $this->variables->get_apikey();
		//$data['urlApi']     = $urlApi;
		//$data['passApiKey'] = $passApiKey;
		$idusuario            = $this->session->userdata('idusuario');
		
		
		$band        = 0;
		$controlador = "Roles";
		$metodo      = "index";
		$accion1     = "set_reglascomunic";
		$accion2     = "set_registrolectura";
		$accion3     = "edit_rol";
		$accion4     = "set_supervisa";
		$accion5     = "set_acciones";
		
		
		$colegios        = $this->session->userdata("colegios"); 
		$colegiosPermi   = array();
		$reglascom       = array();
		$registrolec     = array();
		$editrol         = array();
		$reglas_acciones = array();
		$supervisa       = array();


		foreach ($colegios as $cole)
		{
			$idcolegio = $cole["id_colegio"];
			$menus = $cole["menu"];

		 	foreach ($menus as $menu)
			{	
				if( ($menu->controlador === $controlador) && ($menu->metodo === $metodo))
				{
				    $band = 1;

				    $key = array_search($idcolegio, array_column($colegiosPermi, 'idcole'));

				    if(!$key){

					    $arre = array(
					    	'idcole'=>$idcolegio,
					    	'namecole'=>$cole["nombre_colegio"],
					    	'idrol'=>$cole["id_grupo"],
					    	'namerol'=>$cole["rol"],
					    );
					    $colegiosPermi[]=$arre;


					    $result =$this->get_roles_x_colegio_curl($urlApi, $passApiKey, $idusuario, $idcolegio);

					    //print_r($result); die();
						if($result->status)
							$data['roles'][$idcolegio] = $result->data;

					}
				}

				if( $menu->nombre === $accion1)
				    $reglascom[] = $idcolegio;
				else 
					if( $menu->nombre === $accion2)
				    	$registrolec[] = $idcolegio;
				    else 
						if( $menu->nombre === $accion3)
				    		$editrol[] = $idcolegio;
				    	else 
							if( $menu->nombre === $accion4)
				    			$supervisa[] = $idcolegio;
				    		else 
								if( $menu->nombre === $accion5)
				    				$reglas_acciones[] = $idcolegio;
			}
				
		}

		//print_r($data['roles']); die();

		$result2 =$this->get_menu_acciones_curl($urlApi, $passApiKey, $idusuario);
	    //print_r($result2); die();
		if($result2->status)
			$data['menu_acciones'] = $result2->data;


        if($band == 1)
        {
    		$data['colegios'] = $colegiosPermi;
    		$data['reglascom'] = $reglascom;
    		$data['registrolec'] = $registrolec;
    		$data['reglassupervisa'] = $supervisa;
    		$data['reglasacciones'] = $reglas_acciones;
    		$data['editrol'] = $editrol;

    		//print_r($data); die();
    		
		    $data['contenido'] = 'panel/roles/adminroles';
			$this->load->view('include/template_colegio',$data);
		}
		else //redirect(404);
			redirect("login");
		
	}	
		
   
	//---------------------------------------- CURL request to API -------------------------------------------

	public function get_menu_acciones_curl($urlApi, $passApiKey, $idUser)  
    {
    	$url = $urlApi."/permisos/menu_acciones/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			//CURLOPT_POSTFIELDS => array('idcolegio' =>$idcolegio),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);

		$response=json_decode($response);
		//print_r($response);  die();

		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}

    public function get_roles_x_colegio_curl($urlApi, $passApiKey, $idUser, $idcolegio)  
    {
    	$url = $urlApi."/usuario/obtener_roles_colegio/idcolegio/$idcolegio";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			//CURLOPT_POST => true,
			//CURLOPT_POSTFIELDS => array('idcolegio' =>$idcolegio),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		$response = substr($response, 3);

		//$response=json_decode($response);
		//..print_r($response);  die();

		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}

	
	public function add_rol_curl($urlApi, $passApiKey, $idUser, $datos)  
    {
    	$url = $urlApi."/group/add_group";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('data' =>$datos),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);

		//$response=json_decode($response);
		//print_r($response);  die();

		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}

	public function delete_rol_curl($urlApi, $passApiKey, $idUser, $datos)  
    {
    	$url = $urlApi."/group/delete_group";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('data' =>$datos),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);

		//$response=json_decode($response);
		//print_r($response);  die();

		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}

	public function get_reglascom_x_rol_curl($urlApi, $passApiKey, $idusuario, $idcole, $idrol, $campo) 
    {
    	$url = $urlApi."/permisos/obtener_permisos";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('idcolegio'=>$idcole,'idrol'=>$idrol,'campo'=>$campo),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);

		//$response=json_decode($response);
		//print_r($response);  die();

		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	
	public function add_reglascom_x_rol_curl($urlApi, $passApiKey, $idUser, $datos)  
    {
    	$url = $urlApi."/permisos/set_permisos";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('data' => json_encode($datos)),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);

		//$response=json_decode($response);
		//print_r($response);  die();

		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	
	//---------------------------------------- AJAX request from WEB -------------------------------------------

	public function get_roles_x_colegio_ajax(){ 

	    $urlApi = $this->variables->get_urlapi(); 
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');
		
		$idcole = $_POST['idcole'];		

		$result=$this->get_roles_x_colegio_curl($urlApi, $passApiKey, $idusuario, $idcole);

		//if($result->status)
				//$data['roles'][$idcolegio] = $result->data;
		
		if( (isset($result->status)) && ($result->status=='') ){
			$arre = array(
				'status' => 0,
             	'message' => 'No se encontraron materias'
			);
			$arre=json_encode($arre);
			print_r($arre);
		}
		else{
			$arre = array(
				'status' => 1,
             	'data' => $result->data
			);
			$arre=json_encode($arre);
			print_r($arre);
		}
	}

	
	public function add_rol_ajax(){ 

	    $urlApi = $this->variables->get_urlapi(); 
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');
		
		$datos = $_POST['datos'];			

		$result=$this->add_rol_curl($urlApi, $passApiKey, $idusuario, $datos);
		echo "entro";
		print_r($result);
	}	
	
	public function delete_rol_ajax(){ 

	    $urlApi = $this->variables->get_urlapi(); 
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');
		
		$datos = $_POST['data'];			

		$result=$this->delete_rol_curl($urlApi, $passApiKey, $idusuario, $datos);

		print_r($result);
	}

	
	public function get_reglascom_x_rol_ajax(){ 

	    $urlApi = $this->variables->get_urlapi(); 
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');
		
		$idcole = $_POST['idcole'];	
		$idrol = $_POST['idrol'];		
		$campo = $_POST['campo'];		


		$result=$this->get_reglascom_x_rol_curl($urlApi, $passApiKey, $idusuario, $idcole, $idrol, $campo);

		//if($result->status)
				//$data['roles'][$idcolegio] = $result->data;
		
		print_r($result);
	}

	public function add_reglascom_x_rol_ajax(){

	    $urlApi = $this->variables->get_urlapi(); 
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');
		
		/*$roles = $_POST['roles'];	
		$colegioId = $_POST['colegioId'];
		$grupoId = $_POST['grupoId'];	
		$campo = $_POST['campo'];*/	

		if(isset($_POST['noroles']))
			$datos = array(
				'roles'=>$_POST['roles'],
				'noroles'=>$_POST['noroles'],
				'colegioId'=>$_POST['colegioId'],
				'grupoId'=>$_POST['grupoId'],
				'campo'=>$_POST['campo'],
			);	
		else 
			$datos = array(
				'roles'=>$_POST['roles'],
				//'noroles'=>$_POST['noroles'],
				'colegioId'=>$_POST['colegioId'],
				'grupoId'=>$_POST['grupoId'],
				'campo'=>$_POST['campo'],
			);	

		$result=$this->add_reglascom_x_rol_curl($urlApi, $passApiKey, $idusuario, $datos);

		print_r($result);
	}
	public function asignar_user_group(){
		$iduser        =16174;
		$sqlgroups     ="SELECT * FROM `groups` WHERE name='NODOS'";
		$querygroups   =$this->db->query($sqlgroups);

		foreach ($querygroups->result() as $key ) {
			if (($key->id!= 9)&&($key->id!= 3621)&&($key->id!=3628)) {
				
				$arre= array(
				'user_id'         => $iduser , 
				'group_id'        => $key->id, 
				'colegio_id'      => $key->id_colegio,
				'club_id'         => $key->id_colegio, 
				'configuracion	' => 0, 
				'group_alias	' => '', 
				'estado	'         =>  1);
				$this->db->insert('users_groups', $arre);
				$sqlinsert = "INSERT INTO `reglas_comunicacion` (`id`, `colegio_id`, `group_id`, `roles_id`, `roles_eventos`, `supervisado`, `id_menus_acciones`, `div_todas`) VALUES (NULL, '$key->id_colegio', '$key->id', '7,121,6,10,8,4,76,1,9,3,5', '1,2,3,4,5,6,7,8,9,10,76,121', '8', '1,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,26,27,30,32,33,34,35,36,37,39,40,41,47,48,49,42,43,44\r\n', '1')";
				$this->db->query($sqlinsert);
			}
			
		}
	}	

	 
} // fin Controller Alumno.