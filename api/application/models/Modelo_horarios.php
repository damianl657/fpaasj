<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_horarios extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }


        public function insertar_horario($data)
        {
            $query = $this->db->insert("horarios",$data);
            return $query;
        }

    

        public function get_horario($hora_inicio, $hora_fin, $colegio_id)
        {
            $sql = "SELECT *
                    FROM horarios
                    WHERE hora_inicio='$hora_inicio' 
                    AND colegio_id='$colegio_id' 
                    AND hora_fin='$hora_fin'
                    ";

            $query = $this->db->query($sql);
            return $query->result();
        }
        public function get_horarios_all_user_id_colegio_all($id_user)
        {
            $sql = "SELECT horarios.* 
                FROM horarios 
                join nodocolegio on nodocolegio.id = horarios.colegio_id 
                join users_groups on users_groups.colegio_id = horarios.colegio_id 
                JOIN planesestudio on planesestudio.nombre = nodocolegio.planestudio
                where users_groups.user_id = $id_user 
                AND horarios.planesestudio_id = planesestudio.id
                GROUP BY horarios.id
            ";
            $query = $this->db->query($sql);
            return $query->result();
        }
         public function baja_horarios($idcolegio) 
        {
            $sql = "UPDATE horarios SET estado=0 WHERE colegio_id=$idcolegio ";
            $query = $this->db->query($sql);
            return $query;
        }

         public function update_estado_horario($horarioId, $estadoNew) 
        {
            $sql = "UPDATE horarios SET estado='$estadoNew' WHERE id=$horarioId ";
            $query = $this->db->query($sql);
            return $query;
        }

         public function update_numero_horario($horarioId, $num) 
        {
            $sql = "UPDATE horarios SET numero='$num' WHERE id=$horarioId ";
            $query = $this->db->query($sql);
            return $query;
        }


        public function obtener_horarios_pase1($idcolegio, $ciclo)
        {
            $sql = "SELECT *
                    FROM horarios
                    WHERE horarios.colegio_id=$idcolegio
                    AND horarios.planesestudio_id=$ciclo
                    ORDER BY horarios.id ASC ";
            $query = $this->db->query($sql);
            return $query->result();
        }

        
        public function obtener_horariosmaterias_pase1($idcolegio, $ciclo)
        {
            $sql = "SELECT *
                    FROM horariosmaterias
                    WHERE colegio_id=$idcolegio
                    AND planesestudio_id=$ciclo
                    ORDER BY id ASC ";
            $query = $this->db->query($sql);
            return $query->result();
        }


        public function get_horario_new($id) //se usa para el pase de anio
        {
            $sql = "SELECT *
                    FROM horarios 
                    WHERE horario_id = $id
                    AND estado=1   
                    ";
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }    
        }


        public function insertar_horario_materia($data)
        {
            $query = $this->db->insert("horariosmaterias",$data);
            return $query;
        }


       
    
	}