<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_ael_notas extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }

        
        public function obtener_datos_alumno($user_id)
        {
            $sql_user = "SELECT users.documento, nodocolegio.link FROM `nodocolegio`
            JOIN users_groups on users_groups.colegio_id = nodocolegio.id
            JOIN users on users.id = users_groups.user_id
            JOIN groups on groups.id = users_groups.group_id
            JOIN inscripciones on inscripciones.alumno_id = users_groups.id
            where groups.name = 'Alumno' and users.id = $user_id and users_groups.estado = 1 and inscripciones.cicloa = nodocolegio.cicloa";
            $query = $this->db->query($sql_user);
            if($query->num_rows() > 0)
            {
                return $query->row();
            }
            else
            {
                return false;
            }

        }

	}