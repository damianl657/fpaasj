<script type="text/javascript">

 passApiKey = '<?php echo $passApiKey; ?>';
  urlApi = '<?php echo $urlApi; ?>';
  idUser = '<?php echo $idusuario; ?>';
  //idcolegio = '<?php echo $idcolegio; ?>';
  //idgrupo = '<?php echo $idgrupo; ?>';
  //nombregrupo = '<?php echo $nombregrupo; ?>';
  //ordengrupo = '<?php echo $ordengrupo; ?>';
  url_img = '<?php echo $this->variables->get_url_img(); ?>';

   editaHijo = '<?php echo $editaHijo; ?>';


</script>
<script  type="text/javascript">
  var passApiKey = '<?php echo $passApiKey; ?>';
  var urlApi = '<?php echo $urlApi; ?>';
  var idUser = '<?php echo $idusuario; ?>';

$(document).ready(function()
    {
  //alert('aaa');
  validar_datos_perfil();
  function validar_datos_perfil()
  {
    //alert('ddd');
    var urlRequest = urlApi+"/usuario/validar_datos_perfil";  
    $.ajax
        ({
          url: urlRequest,
              type: 'GET',
              headers: {              
               'APIKEY' : passApiKey,
               'userid' : idUser,
               'Access-Control-Allow-Origin': '*'
              },
              data: {},
              success: function(data)
              {  
                  console.log(data);
                  if(data['datos_completo'] == 0)
                  { 
                    
                      $("#modal_completar_datos_perfil").modal('show');
                  }
              }, //--fun success
              error: function(response)
              {
                  console.log(response);
                  
              }
        });
  }
  
});   
</script>
<script src="<?php echo base_url('tpl/js/bootstrap.min.js');?>"></script>



<script src="<?php echo base_url('tpl/js/plugins/Jcrop-master/js/jquery.Jcrop.js');?>"></script>
<script src="<?php echo base_url('tpl/js/plugins/Jcrop-master/js/jquery.color.js');?>"></script>

<link rel="stylesheet" href="<?php echo base_url('tpl/js/plugins/Jcrop-master/css/jquery.Jcrop.css');?>" type="text/css" />


<script>

  var sizeArchivo=6; //MB

   //var jcrop_api;
   


  function guardarRecortes()
  { 
    var elem = $('#imagen').find('.jcrop-tracker');
    var wtotal = $(elem[1]).css('width');
    var htotal = $(elem[1]).css('height');

    //console.log(wtotal+'x'+htotal);

    var x = $('#x').val();
    var y = $('#y').val();
    var w = $('#w').val();
    var h = $('#h').val();

    var urlRequest = urlApi+"/Upload/recortar_foto";
    $.ajax({
        url: urlRequest,
        type: 'POST',
        headers: {              
            'APIKEY' : passApiKey,
            'userid' : idUser,
            'Access-Control-Allow-Origin': '*'
        },
        data:{x:x, y:y, w:w, h:h, wtotal:wtotal, htotal:htotal},
        beforeSend: function() {
            $('#filer_input3').after("<img id='loadingUP' src='<?=base_url('assets/images/loading.gif');?>' height='15' width='15' />");
        },
        success: function(data)
        {   
            var data = jQuery.parseJSON( data ); 
            //console.log(data);

            var url = url_img+'/'+data['name'];
            $('#userphoto').attr("src",url); //modifica
            $('#userphoto').attr('src', $('#userphoto').attr('src')+'?'+Math.random()); //refresca

            $('#fotouser_menu').attr("src",url); //modifica
            $('#fotouser_menu').attr('src', $('#fotouser_menu').attr('src')+'?'+Math.random()); //refresca
           

            $('#loadingUP').remove();


            $('#userphoto').Jcrop({
              },function(){
                api = this;
              });
            api.destroy();


            $('#guardarResize').attr('disabled',true);
            $('#recortar').attr('disabled',false);
            $('#filer_input3').attr('disabled', false);

            $.notify("foto recortada con exito", "success");
          
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            // Handle errors here
            console.log('ERRORS: ' + textStatus);
            // STOP LOADING SPINNER
            $('#loadingUP').remove();
        }
    });
  }


  function subirarchivo(data, name)
  { 
    var urlRequest = urlApi+"/Upload/subir_foto";
    $.ajax({
        url: urlRequest,
        type: 'POST',
        headers: {              
               'APIKEY' : passApiKey,
               'userid' : idUser,
               'Access-Control-Allow-Origin': '*'
        },
        data: data,
        cache: false,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        beforeSend: function() {
            $('#filer_input3').after("<img id='loadingUP' src='<?=base_url('assets/images/loading.gif');?>' height='15' width='15' />");
        },
        success: function(data, textStatus, jqXHR)
        {   
            //console.log(data);
            var data = jQuery.parseJSON( data ); 
            if( data['error'] )
            {  
                $.notify(data['error'], "danger");
                
            }
            else{ 

                var url = url_img+'/'+data['name'];
                $('#userphoto').attr("src",url); //modifica
                $('#userphoto').attr('src', $('#userphoto').attr('src')+'?'+Math.random()); //refresca

                $('#filer_input3').attr('disabled', true);

                editar_imagen();

                $.notify("foto guardada con exito", "success");
            }

            $('#loadingUP').remove();
          
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            // Handle errors here
            console.log('ERRORS: ' + textStatus);
            // STOP LOADING SPINNER
            $('#loadingUP').remove();

            $('#userphoto').attr('src', $('#userphoto').attr('src')+'?'+Math.random()); //refresca
        }
    });
  }

  //boton examinar
  function prepareUpload(event)
  {   
      event.stopPropagation(); // Stop stuff happening
      event.preventDefault(); // Totally stop stuff happening

      //console.log('prepareUpload');

      var ecxedesize=0;

      files = event.target.files;  //esta linea es la unica diferencia con el prepareUpload2
      
      $.each(files, function(key, value)
      {
          var data = new FormData();
          data.append(key, value);

          var size = value.size;
          if( size > 1024)
          {
              var temp = size/1024;
              //temp = number_format(temp, 2, '.', '');
              if (temp > 1024)
              {
                  size = (temp/1024).toFixed(2)+" MB";
                  if( (temp/1024).toFixed(2) > sizeArchivo )
                      ecxedesize=1;
              }
              else
                  size = temp.toFixed(2)+" KB";
          }
          else 
              size = size.toFixed(2)+" Bytes";
          
        
          if(ecxedesize==0)
          {
              //delete_files(alta);
              subirarchivo(data, value.name);
          }
          else 
              $.notify("La foto debe ser menor a "+sizeArchivo+" MB", "warn");


      });   
    
  }

  //drag and drop
  function prepareUpload2(files)
  {   //console.log('prepareUpload2');

      var ecxedesize=0;

      $.each(files, function(key, value)
      {
          var data = new FormData();
          data.append(key, value);

          var size = value.size;
          if( size > 1024)
          {
              var temp = size/1024;
              //temp = number_format(temp, 2, '.', '');
              if (temp > 1024)
              {
                  size = (temp/1024).toFixed(2)+" MB";
                  if( (temp/1024).toFixed(2) > sizeArchivo )
                      ecxedesize=1;
              }
              else
                  size = temp.toFixed(2)+" KB";
          }
          else 
              size = size.toFixed(2)+" Bytes";
          
        
          if(ecxedesize==0)
          {
              //delete_files(alta);
              subirarchivo(data, value.name);
          }
          else 
              $.notify("La foto debe ser menor a "+sizeArchivo+" MB", "warn");


      });   
  }




  function draganddropUpload() //alta o edit
  { 
      var obj = $("#dropzone");

      obj.on('dragenter', function (e) 
      {
          e.stopPropagation();
          e.preventDefault();
          //$(this).add('border', '4px solid #0B85A1');
          
          $(this).addClass('dragandrophandler2');
      });
      obj.on('dragover', function (e) 
      {
           e.stopPropagation();
           e.preventDefault();
      });
      obj.on('drop', function (e) 
      {
          e.stopPropagation();
          e.preventDefault();
          
          $(this).removeClass('dragandrophandler2');
          var files = e.originalEvent.dataTransfer.files;
          //We need to send dropped files to Server
          prepareUpload2(files);
      });



      $(document).on('dragenter', function (e) 
      {
          e.stopPropagation();
          e.preventDefault();
      });
      $(document).on('dragover', function (e) 
      {
        e.stopPropagation();
        e.preventDefault();
       
        $(obj).removeClass('dragandrophandler2');
      });
      $(document).on('drop', function (e) 
      {
          e.stopPropagation();
          e.preventDefault();
      });

  }


  function editar_imagen()
  { 
      $('#userphoto').Jcrop({
        // start off with jcrop-light class
        bgOpacity: 0.5,
        bgColor: 'white',
        addClass: 'jcrop-light',
        allowSelect: false,
        aspectRatio: 0,
        onSelect: updateCoords
      },function(){
        api = this;
        api.setSelect([130,65,130+350,65+285]);
        api.setOptions({ bgFade: true });
        api.ui.selection.addClass('jcrop-selection');
      });

      //aspectRatio:0   no hay relacion entre el alto y ancho
      //jcrop_api = this;

      $('#recortar').css('display', 'block');
      $('#guardarResize').css('display', 'block');

      $('#guardarResize').attr('disabled',false);
      $('#recortar').attr('disabled',true);

  }



   function updateCoords(c)
   {  
      $('#x').val(c.x);
      $('#y').val(c.y);
      $('#w').val(c.w);
      $('#h').val(c.h);
   }
   function checkCoords()
   {
      if (parseInt($('#w').val()))
         return true;
      bootbox.alert("Recorte la foto!", function() { });
      return false;
    }




  function abrir_modal_editar_usuario(id_user)
  {
    $("#documento_txt_edit").val('');
    $("#apellido_txt_edit").val('');
    $("#nombre_txt_edit").val('');
    $("#email_txt_edit").val('');
    $("#user_id_edit").val('');
    $("#username_txt_edit").val('');

    $("#radio_masculino_edit").attr('checked', false);
    $("#radio_femenino_edit").attr('checked', false);
    
    var urlRequest = urlApi+"/usuario/obtener_usuario/"; 
        $.ajax({
                url: urlRequest,
                  type: 'GET',
                  headers: {              
                   'APIKEY' : passApiKey,
                   'userid' : idUser,
                   'Access-Control-Allow-Origin': '*'
                },
                data :{},
                beforeSend: function() 
                {
                  $('#editar_perfil').before("<img  id='loading_get_user' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");

                  $('#editar_perfil').css('display','none');
                },
 
                success : function(data) 
                {   
                    //console.log(data);
                   // users = data.users;
                    $('#loading_get_user').remove();
                    $('#editar_perfil').css('display','inline');

                    $("#documento_txt_edit").val(data["documento"]);
                    $("#apellido_txt_edit").val(data["last_name"]);
                    $("#nombre_txt_edit").val(data["first_name"]);
                    $("#email_txt_edit").val(data["email"]);
                    $("#username_txt_edit").val(data["username"]);
                    //alert(data["id"]);
                    $("#user_id_edit").val(data["id"]);
                    
                    if(data['sexo'] == 'F') 
                    {
                      jQuery("#radio_femenino_edit").prop('checked', true);
                      //jQuery("#radio_masculino_edit").attr('checked', false);
                    }
                    else if(data['sexo'] == 'M')
                        {
                          jQuery("#radio_masculino_edit").prop('checked', true);
                         // jQuery("#radio_femenino_edit").attr('checked', false);
                        }
                    $("#modal_editar_usuario").modal('show');



                    
                },
                error: function(response)
                {
                    //hacer algo cuando ocurra un error
                    console.log(response);
                }
        }); 

  }

  function editar_usuario()
  {
       
      $('#form_edit_users').validate
      ({
          rules: {
              nombre_txt_edit: { required: true},
              apellido_txt_edit: { required: true},
              // email_txt_edit: { required: true},
              documento_txt_edit: { required: true},
          },
          messages: {
              nombre_txt_edit:  {required: "Debe introducir el Nombre."}, 
              apellido_txt_edit:  {required: "Debe Introducir el Apellido."},
              documento_txt_edit:  {required: "Debe Introducir el Documento.", number: "Debe introducir un valor Numérico"},
             email_txt_edit:  {email: "Debe Introducir un E-mail Válido"},
          },
          submitHandler: function(form)
          { 
              var urlRequest = urlApi+"/Usuario/update_usuario"; 

               if( $('#radio_masculino_edit').is(':checked') )
                var sexo='M';
              else
                var sexo='F';

              var parametros = {
                  'first_name': $('#nombre_txt_edit').val(),
                  'last_name': $('#apellido_txt_edit').val(),     
                  'email': $('#email_txt_edit').val(),   
                  'sexo': sexo,
                  'documento': $('#documento_txt_edit').val(),
                  'username': $('#username_txt_edit').val(),
              };
              var parametros = JSON.stringify(parametros);

              $.ajax
              ({  
                    url: urlRequest,
                    type: 'POST',
                    headers: {              
                     'APIKEY' : passApiKey,
                     'userid' : idUser,
                     'Access-Control-Allow-Origin': '*'},
                    data: {data:parametros, user_id_edit:$('#user_id_edit').val()},
                    beforeSend: function() 
                    {
                      $('#update_usuario').before("<img  id='loading_edit_user' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");

                      $('#update_usuario').css('display','none');
                    },
                  
                    success: function(data)
                    {
                        //alert('entro');
                        //console.log(data);
                        $('#loading_edit_user').remove();
                        $('#update_usuario').css('display','inline');

                        if(data.status == true)
                        {
                          

                          $('#nombre_txt_edit').val('');
                          $('#documento_txt_edit').val('');
                          $('#apellido_txt_edit').val('');
                          $('#email_txt_edit').val('');
                          $('#user_id_edit').val('');
                          $('#username_id_edit').val('');

                          $.notify(data.message, "success");
                         

                          $("#modal_editar_usuario").modal('hide');

                         
                          if(editaHijo==1)
                          {
                              traer_hijos();
                          }
                          
                           obtener_usuario();
                          
                        }
                        else
                        {
                            bootbox.alert({
                                message: data.message,
                                size: 'small'
                            });

                            switch(data.campo)
                            { 
                                case 'dni': $('#documento_txt_edit').val('');
                                            $('#documento_txt_edit').focus();
                                            break;
                                case 'username': $('#username_txt_edit').val('');
                                                  $('#username_txt_edit').focus();
                                            break;
                                case 'email': $('#email_txt_edit').val('');
                                                  $('#email_txt_edit').focus();
                                                break;
                            }
                             
                        }
                            
                        
                    },
                    error: function (jqXHR, textStatus, errorThrown,data)
                    {
                      // document.getElementById("myModalLabel").innerHTML = 'Registro de Usuarios';
                            //document.getElementById("mensaje").innerHTML = 'ERROR';
                            //$("#myModal").modal('show');
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                        alert('error');
                        //console.log(data);

                    }
              });
          }
      });
  
  }


  $(document).ready(function()
  {

    draganddropUpload();

    $('input[type=file]').on('change', function(event) { prepareUpload(event); });

    $('#recortar').click(function(){
      $('#filer_input3').attr('disabled', true);
      editar_imagen();
    });

    $('#guardarResize').click(function(){
      if(checkCoords())
        guardarRecortes();
    });

    
    $('#editar_perfil').click(function(){

        abrir_modal_editar_usuario(idUser);
    });

    $('#update_usuario').click(function(){
        editar_usuario();
    });

  });
  </script>



<div class="wrapper wrapper-content animated fadeIn">
            <div class="row animated">

                <div class="col-lg-7 col-md-8 col-sm-10 col-xs-12 col-centered">
                    <div class="ibox float-e-margins">
                        <!--<div class="ibox-title">
                            <center><h5>Perfil</h5></center>
                        </div>-->

                        <div class="ibox-title collapse-link" id="tituloperfil" style="cursor: pointer;">
                            <h5>Mi Perfil <small> Editar Datos Personales</small></h5>
                            
                            <div class="ibox-tools">
                                <a class="">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        
                        <div>
                            <div class="ibox-content no-padding border-left-right">
                            	 
                                <div id="dropzone" class="dropzone" action="#">
                                  
                                    <div id="imagen" ></div>
                                                       
                                </div>

                                <input type="hidden" id="x" name="x" />
                                <input type="hidden" id="y" name="y" />
                                <input type="hidden" id="w" name="w" />
                                <input type="hidden" id="h" name="h" />

                                <!--UPLOAD-->
                                <div class="ibox-content profile-content">
                                  <div class="row"> 

                                     <div class="col-md-4">
                                      <span class="btn btn-success btn-sm btn-block btn-file">
                                        <i class="fa fa-folder-open m-right-xs"></i> &nbsp;&nbsp;Examinar
                                        <input type="file" name="files[]" id="filer_input3" placeholder="Browse computer" style="color: transparent; max-width:120px; "  data-toggle="tooltip" title="Seleccione o arrastre su foto de perfil" class="col-md-4"/>
                                      </span>
                                    </div>

                                    <div class="col-md-4">
                                      <button id="guardarResize" disabled class="btn btn-success btn-sm btn-block" style="display: none"><i class="fa fa-stop m-right-xs"></i> &nbsp;&nbsp;Terminar Recorte</button>
                                    </div>

                                    <div class="col-md-4">
                                      <button id="recortar" class="btn btn-success btn-sm btn-block" style="display: none"><i class="fa fa-cut m-right-xs"></i>&nbsp;&nbsp;Recortar</button>
                                    </div>
                                  </div>
                                </div>

                                <!--<div class="ibox-content profile-content">
                                  <div class="row"> 
                                    <div class="col-md-6">
                                      <button id="guardarResize" disabled class="btn btn-success btn-sm btn-block">Terminar Recorte</button>
                                    </div>

                                    <div class="col-md-6">
                                      <button id="recortar" class="btn btn-success btn-sm btn-block">Recortar</button>
                                    </div>

                                  </div>
                                </div>-->
                               
                            </div>
                      
                            
                            <div class="ibox-content profile-content">
                              
                                <h4><i class="fa fa-user"></i> Nombre y Apellido: <strong><label id="nomb_ape"></label></strong></h4>
                                <br>
                                <h4><i class="fa fa-phone-square"></i> Telefono: <strong><label id="telefono"></label></strong></h4>
                                 <br>
                                <h4><i class="fa fa-google-plus"></i> Email: <strong><label id="email"></label></strong></h4>
                                <br>
                                <h4><i class="fa fa-vcard"></i> Documento: <strong><label id="dni"></label></strong></h4>
                                <br>
                                <h4><i class="fa fa-user"></i> Username: <strong><label id="username"></label></strong></h4>
                                 <br>
                                <h4><i class="fa fa-venus-mars"></i> Sexo: <strong><label id="sexo"></label></strong></h4>

                                 <br>

                                <div class="user-button">
                                    <div class="row">
                                       <?php //var_dump($this->session->userdata('menu'));
                                          foreach ($this->session->userdata('menu') as $boton) 
                                          {
                                              if($boton->tipo == 'boton')
                                              {
                                                  $nombreb = $boton->nombre;

                                                  $nombreb = str_replace('_',' ',$nombreb)
                                                  ?>
                                                        <div class="col-md-6">
                                                            <button type="button" id="<?php if($boton->id_accion_boton!= NULL) echo $boton->id_accion_boton; ?>" class="btn btn-success btn-sm btn-block"><i class="<?php echo $boton->icono; ?> m-right-xs"></i> <?php echo $nombreb;?></button>
                                                        </div>
                                                  <?php 
                                              }
                                              //print_r($menu);
                                          }
                                          ?>


                                        
                                    </div>
                                </div>
                            </div>
                    	</div>
                	</div>
            	</div>
    		</div>
</div>
    <!-- Modal de Error-->
    <div class="modal fade" id="modal_error" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="titulo_error"></h4>
          </div>
          <div class="modal-body">
              
                <div class="form-group">
                  <center><label id="error_msj_pass" class="control-label" ></label></center>
                  <h4></h4>
                </div>
              
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" id="cerrar_modal_error">Cerrar</button>
          </div>
        </div>
      </div>
    </div>

<!-- Modal de Cambiar Contraseña-->
        <div class="modal fade" id="modal_cambio_contraseña" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="titulo">Cambio de Contraseña</h4>
              </div>
              <div class="modal-body">
                
                <form action="#" id="formulario_contraseña" method="post" class="form-horizontal">
                  <input type="hidden" value="" name="eventosid"/> 
                  
                  <div class="form-body">
                    <div class="form-group">
                      <label class="control-label col-md-4">Ingrese Contraseña Actual</label>
                      <div class="col-md-8">
                        <input name="actual" id="actual" placeholder="Ingrese Contraseña Actual" class="form-control" type="password" required>
                        <label id="error_msj"></label>
                        <input type="hidden" value="" id="msj_error" name="msj_error"/> 
                      </div>
                      <h4></h4>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-4">Ingrese Contraseña Nueva</label>
                      <div class="col-md-8">
                        <input name="password" id="password" placeholder="Ingrese Contraseña Nueva" class="form-control" type="password" required>
                        <label id="error_msj"></label>
                        <input type="hidden" value="" id="msj_error" name="msj_error"/> 
                      </div>
                      <h4></h4>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-4">Confirme Contraseña Nueva</label>
                      <div class="col-md-8">
                        <input name="password_confirm" id="password_confirm" placeholder="Confirme Contraseña Nueva"  class="form-control" type="password" required>
                        <label id="error_msj"></label>
                        <input type="hidden" value="" id="msj_error" name="msj_error"/> 
                      </div>
                      <h4></h4>
                    </div>
                    
                        
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" id="realizar_cambio_contraseña" class="btn btn-primary">Guardar</button>
              </div>
            </div>
          </div>
        </div>

       <!-- modal editar usuario-->
      <div class="modal fade" id="modal_editar_usuario" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Editar Usuario</h4>
            </div>
            <form id="form_edit_users" autocomplete="on" method="post" class="form-horizontal form-label-left">
                <input type="hidden" id="user_id_edit" name="user_id_edit" value="0" />
                <div class="modal-body">
                  <input type="hidden" id="user_id" name="user_id" value="0" />
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Documento</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="documento_txt_edit" id="documento_txt_edit">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="username_txt_edit" id="username_txt_edit">
                    </div>
                  </div>
                  
                  <div class="form-group">
                      <label class="col-sm-2 control-label">Nombre</label>

                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="nombre_txt_edit" id="nombre_txt_edit">
                      </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label">Apellido</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="apellido_txt_edit" id="apellido_txt_edit">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-8">
                      <input type="email" id="email_txt_edit" name="email_txt_edit"class="form-control">
                      </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label">Sexo <br/><small class="text-navy">Seleccione el Sexo</small></label>

                    <div class="col-sm-8">
                          <div class="i-checks"><label> <input type="radio" value="M" name="sexo_txt_edit" id="radio_masculino_edit">  <i></i> Masculino </label></div>
                          <div class="i-checks"><label> <input type="radio" value="F" name="sexo_txt_edit" id="radio_femenino_edit"> <i></i> Femenino </label></div>
                    </div>
                  </div>
              
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  <button class="btn btn-primary" id ="update_usuario" >Guardar</button>
              </div>

            </form>
            
          </div>
        </div>
      </div>
      

<script type="text/javascript">
  $('#cerrar_modal_error').click(function(){
    $('#modal_error').modal('hide');
    //$('#modal_cambio_contraseña').modal('show');
  }) 
</script>


<script type="text/javascript">
	$(document).ready(function()
  {

  	$('#cambiar_contraseña').click(function(){
  		$('#modal_cambio_contraseña').modal('show');
    });


    $('#realizar_cambio_contraseña').click(function(){
      var validacion=0;

      if($('#actual').val() == '')
      {
          $('#actual').notify("Ingrese su clave actual", {className:"error", autoHide: true});
          $('#actual').focus();
          return false;
      }
      else
        if( ($('#password').val() == '') || ($('#password').val().length < 5) )
        {
            $('#password').notify("Ingrese su nueva actual", {className:"error", autoHide: true});
            $('#password').focus();
            return false;
        }
        else
          if( ($('#password_confirm').val() == '') || ($('#password_confirm').val().length < 5) )
          {
            $('#password_confirm').notify("Repita su nueva clave. 5 caracteres minimo", {className:"error", autoHide: true});
            $('#password_confirm').focus();
            return false;
          }
          else 
            if($('#password').val() == $('#password_confirm').val())
            {
                validacion=1;
            }
            else
              {
                  $('#password_confirm').notify("Las claves no coinciden", {className:"error", autoHide: true});
                  return false;
              }


      var urlRequest = urlApi+"/Login/cambiar_password";
      if(validacion)
          $.ajax
          ({
              url: urlRequest,
              //dataType:'text',
              type: 'POST',
              dataType: "JSON",
              headers: {              
                     'APIKEY' : passApiKey,
                     'userid' : idUser,
                     'Access-Control-Allow-Origin': '*'
              },
              data: $('#formulario_contraseña').serialize(),
              beforeSend: function() 
              {
                $('#realizar_cambio_contraseña').before("<img  id='loading_edit_pass' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");

                $('#realizar_cambio_contraseña').css('display','none');
              },
                  
              success: function(data)
              {
                  //console.log('success entro');
                  //console.log(data['status']);
                  $('#loading_edit_pass').remove();
                  $('#realizar_cambio_contraseña').css('display','inline');


                  if(data['status']==true)
                  {
                    $.notify(data['message'], "success");
                    $("#actual").val('');
                    $("#password").val('');
                    $("#password_confirm").val('');
                  
                    $('#modal_cambio_contraseña').modal('hide');
                  }
                  else 
                    bootbox.alert(data['message'], function() { });
              },
              error: function(data)
              { 
                $('#loading_edit_pass').remove();
                $('#realizar_cambio_contraseña').css('display','inline');
                
                console.log(data['responseText']); 

                
              }

          }); 

    })

  })
</script>

<script type="text/javascript">

	function obtener_usuario()
  {
    //$('#loading2').show();
    var urlRequest = urlApi+"/usuario/obtener_usuario/"; 
    $.ajax({
      url: urlRequest,
        type: 'GET',
        headers: {              
         'APIKEY' : passApiKey,
         'userid' : idUser,
         'Access-Control-Allow-Origin': '*'
      },
      data :{},//la última id
      success : function(data) 
      {   
          //$('#loading2').hide();
          //console.log(data);
          //console.log(url_img);
          document.getElementById("nomb_ape").innerHTML = ' '+data.first_name+' '+data.last_name;
            $('#userphoto').remove();
            if( (data.foto == null) || (data.foto == '') )
            {
            	var imagen = 
                        '<center><img id="userphoto" alt="image" class="img-responsive" src="'+url_img+'/default-avatar.png"></center>';
                    /*var imagen = '<div id="my-awesome-dropzone" class="dropzone" action="#">
                                                <div class="dropzone-previews"><center>Haga Click para adjuntar un foto de Perfil</center></div>
                                               
                                            </div>';*/
              $('#recortar').css('display', 'none');
              $('#guardarResize').css('display', 'none');
            }
            else 
            {
              
            	var imagen = '<center><img id="userphoto" alt="image" class="feed-photo" src="'+url_img+'/'+data.foto+'"></center>';
                /*var imagen = '<div id="my-awesome-dropzone" class="dropzone" action="#">
                                                <div class="dropzone-previews"><center>Haga Click para adjuntar un foto de Perfil</center></div>
                                               
                                            </div>';*/
              $('#recortar').css('display', 'block');
              $('#guardarResize').css('display', 'block');
            }
            
            //console.log(imagen);
            $("#imagen").append(imagen);
            $('#userphoto').attr('src', $('#userphoto').attr('src')+'?'+Math.random()); //refresca

            if(data.phone == null)
            {
            	var phone ="No Registrado";   
            	document.getElementById("telefono").innerHTML = ' '+phone;       
            }
            else
            {
            	document.getElementById("telefono").innerHTML = ' '+data.phone;
            }
            document.getElementById("email").innerHTML = ' '+data.email;

            document.getElementById("dni").innerHTML = ' '+data.documento;
            document.getElementById("sexo").innerHTML = ' '+data.sexo;
            document.getElementById("username").innerHTML = ' '+data.username;

         // $('#descripcion').val(data.descripcion);
          //$('#titulo').val(data.titulo);
      },
      error: function(response)
      {
          //hacer algo cuando ocurra un error
          console.log(response);
      }

    });             

  }

  obtener_usuario();


</script>


<style type="text/css">

.dragandrophandler2
{
  border:3px dashed #0B85A1;
  /*width:400px;*/
  height: auto;
  color:#92AAB0;
  text-align:left;vertical-align:middle;
  padding:10px 10px 10 10px;
  margin-bottom:10px;
  font-size:100%;
}

.btn-file {
    position: relative;
    overflow: hidden;
}

.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

</style>
<!-- Modal de Error-->
    <div class="modal fade" id="modal_completar_datos_perfil" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="">Mi Perfil</h4>
          </div>
          <div class="modal-body">
              
                <div class="form-group">
                  <center><label id="" class="control-label" >Por favor complete su Correo Electrónico, haciendo click en "Editar Perfil"</label></center>
                  <h4></h4>
                </div>
              
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" id="cerrar_ompletar_datos_perfil" >Cerrar</button>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
  $('#cerrar_ompletar_datos_perfil').click(function(){
    $('#modal_completar_datos_perfil').modal('hide');
    //$('#modal_cambio_contraseña').modal('show');
  }) 
</script>
        
