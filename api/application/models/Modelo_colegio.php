<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_colegio extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }

        
        public function get_colegio($idcolegio) 
        {
            $sql = "SELECT *
                    FROM nodocolegio 
                    WHERE id = '$idcolegio'                
                    ";
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query;
            }
            else{
                return false;
            }
        }

        public function get_colegio2($idcolegio) 
        {
            $sql = "SELECT *
                    FROM nodocolegio 
                    WHERE id = '$idcolegio'                
                    ";
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->row();
            }
            else{
                return false;
            }
        }


        public function obtener_colegios_activos() 
        {
            $sql = "SELECT *
                    FROM nodocolegio 
                    WHERE estado = 1               
                    ";
            $query = $this->db->query($sql);
            
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }
        public function obtener_colegios() 
        {
            $sql = "SELECT *
                    FROM nodocolegio 
                    ";
            $query = $this->db->query($sql);
            return $query;
            
        }
        public function obtener_ciclos_lectivos() 
        {
            $sql = "SELECT *
                    FROM ciclos_lectivos  ORDER BY `nombre` DESC
                    ";
            $query = $this->db->query($sql);
            return $query;
            
        }
        public function obtener_tipos_colegios() 
        {
            $sql = "SELECT *
                    FROM tipo_nododolegio ORDER BY `nombre` DESC
                    ";
            $query = $this->db->query($sql);
            return $query;
            
        }
        public function obtener_planesestudio() 
        {
            $sql = "SELECT *
                    FROM planesestudio ORDER BY `nombre` DESC
                    ";
            $query = $this->db->query($sql);
            return $query;
            
        }
        public function registrar_colegio($data)
        {
            $query = $this->db->insert("nodocolegio",$data);
            return $query;
        }
        public function update_colegio($data, $id)
        {
            $this->db->where('id', $id);
                return $this->db->update('nodocolegio',$data);
        }
        
        
       

	}