<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_alumno extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }

        public function obtener_alumnos($idtutor)
        {
            //obtengo la division en la que esta inscripto el alumno
            //$division = $this->division->obtenerDivisionPorAlumno($idalumno);
            $sql = "SELECT alumno_id, first_name nombre, last_name apellido, IFNULL( CONCAT('".$this->utilidades->get_url_img()."fotos_usuarios/', users.foto,'') , CONCAT('".$this->utilidades->get_url_img()."fotos_usuarios/', 'default-avatar.png','' ) ) foto FROM tutoralumno JOIN users ON users.id = tutoralumno.alumno_id WHERE tutoralumno.tutor_id  = ".$idtutor;
            $query = $this->db->query($sql);
            return $query->result();
        }


        public function get_alumnosXdivisiones($idsDivisiones, $cicloA)
        {
            $sql = "SELECT users_groups.user_id
                    FROM inscripciones 
                    JOIN users_groups ON inscripciones.alumno_id = users_groups.id
                    JOIN groups ON users_groups.group_id = groups.id
                    WHERE inscripciones.divisiones_id IN ($idsDivisiones)
                    AND inscripciones.cicloa = $cicloA
                    AND users_groups.estado = 1
                    AND groups.name='Alumno'

                    GROUP BY user_id
                    ";
            //echo $sql;       
            $query = $this->db->query($sql);           
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }
        public function get_tutoresXdivisiones($idsDivisiones, $cicloA)
        {
            $sql = "SELECT users.id as user_id 
FROM `users`
join tutoralumno on users.id = tutoralumno.tutor_id
where tutoralumno.alumno_id in (
            SELECT users_groups.user_id
                    FROM inscripciones 
                    JOIN users_groups ON inscripciones.alumno_id = users_groups.id
                    JOIN groups ON users_groups.group_id = groups.id
                    WHERE inscripciones.divisiones_id IN ($idsDivisiones)
                    AND inscripciones.cicloa = $cicloA
                    AND users_groups.estado = 1
                    AND groups.name='Alumno')

                    GROUP BY user_id
                    ";
           // echo $sql;       
            $query = $this->db->query($sql);           
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }

        public function get_alumnos_x_division($id_division, $id_colegio,$cicloA) //no harian falta el colegio.
        {
            $sql = "SELECT users.*, inscripciones.alumno_id
                    FROM inscripciones 
                    inner JOIN users_groups ON inscripciones.alumno_id = users_groups.user_id
                    inner JOIN users ON users_groups.user_id = users.id                     
                    WHERE inscripciones.divisiones_id = $id_division and inscripciones.activo = 1
                    AND users_groups.colegio_id = $id_colegio
                    AND users_groups.estado = 1
                    AND inscripciones.cicloa = $cicloA
                    GROUP BY user_id
                    ORDER BY users.last_name, users.first_name ASC";
            
            $query = $this->db->query($sql);           
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }


        public function get_tutoresXalumnos($idsalumnos)
        {
            $sql = "SELECT tutor_id, alumno_id
                    FROM users_groups
                    JOIN groups ON groups.id=users_groups.group_id
                    JOIN users ON users.id=users_groups.user_id
                    JOIN tutoralumno ON tutoralumno.alumno_id=users.id
                    WHERE tutoralumno.alumno_id IN ($idsalumnos)  
                    AND groups.name='Alumno'
                    AND users_groups.estado = 1
                    ";

            $query = $this->db->query($sql);           
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }

        public function get_preceptoresXdivisiones($idsDivisiones)
        {
            $sql = "SELECT users_groups.user_id
                    FROM preceptores JOIN users_groups, groups
                    WHERE preceptores.division_id IN ($idsDivisiones)
                    AND preceptores.user_id = users_groups.id
                    AND users_groups.group_id = groups.id 
                    AND groups.name='Preceptor'
                    AND users_groups.estado = 1
                    GROUP BY user_id
                    ";

            $query = $this->db->query($sql);           
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }

        /*public function get_docentesXdivisiones($idsDivisiones)
        {
            $sql = "SELECT users_groups.user_id
                    FROM horariosmaterias JOIN users_groups, groups
                    WHERE horariosmaterias.divisiones_id IN ($idsDivisiones)
                    AND horariosmaterias.docente_id = users_groups.id
                    AND users_groups.group_id = groups.id 
                    AND groups.name='Docente'
                    AND users_groups.estado = 1
                    GROUP BY user_id
                    ";

            $query = $this->db->query($sql);           
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }*/
        public function get_docentesXdivisiones($idsDivisiones)//usar join con nodocoloegio
        {
            $sql = "SELECT users_groups.user_id 
            FROM nodo_doc_div_mat_alu 
            JOIN users_groups, groups 
            WHERE nodo_doc_div_mat_alu.id_division IN ($idsDivisiones) 
            AND nodo_doc_div_mat_alu.id_docente = users_groups.id 
            AND users_groups.group_id = groups.id 
            AND groups.name='Docente' 
            AND users_groups.estado = 1 
            GROUP BY user_id";

            $query = $this->db->query($sql);           
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }
        //SELECT users_groups.user_id FROM nodo_doc_div_mat_alu JOIN users_groups, groups WHERE nodo_doc_div_mat_alu.id_division IN (133,134) AND nodo_doc_div_mat_alu.id_docente = users_groups.id AND users_groups.group_id = groups.id AND groups.name='Docente' AND users_groups.estado = 1 GROUP BY user_id


        public function get_inscripcionesXciclo($ciclo)
        {
            $sql = "SELECT inscripciones.*
                    FROM inscripciones 
                    JOIN users_groups, groups
                    WHERE inscripciones.cicloa = $ciclo
                    AND inscripciones.alumno_id = users_groups.id
                    AND users_groups.group_id = groups.id               
                    AND groups.name='Alumno'
                    AND inscripciones.activo=1
                    AND users_groups.estado=1
                    GROUP BY user_id
                    ";

            $query = $this->db->query($sql);           
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }

       
        public function  insert_egresado($datos)
        {
            $query = $this->db->insert("egresados",$datos);
            return $query;
        }

        public function  insert_inscripcion($datos)
        {
            $query = $this->db->insert("inscripciones",$datos);
            return $query;
        }




        /*public function  insert_inscripcion_temp($datos)
        {
            $query = $this->db->insert("inscripciones_temp",$datos);
            return $query;
        }*/

        public function is_alumno($id_user)
        {
            $sql = "SELECT * FROM `users`
            JOIN users_groups on users_groups.user_id = users.id
            JOIN groups on groups.id = users_groups.group_id
            where groups.name = 'Alumno'
            and users.id = $id_user
                    ";

            $query = $this->db->query($sql);           
            if($query->num_rows()>0){
                return 1;
            }
            else{
                return 0;
            }
        }

        public function pago($id_user)
        {
            //obtengo la division en la que esta inscripto el alumno
            //$division = $this->division->obtenerDivisionPorAlumno($idalumno);
            $sql = "SELECT * FROM `users`
                    WHERE id = $id_user 
                    and pago = 1";
            $query = $this->db->query($sql);           
            if($query->num_rows()>0){
                return 1;
            }
            else{
                return 0;
            }
        }
    }