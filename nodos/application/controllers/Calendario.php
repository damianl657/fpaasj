<?php //session_start();
 
class Calendario extends CI_Controller {
      
	function __construct(){
		parent::__construct();
			//$this->output->enable_profiler(TRUE);
		if(!$this->session->userdata("logged_in")){
               redirect("login");
         }
	}

    public function index(){         
        $controlador = "Calendario";	
       // $this->load->view('include/vista_inc_header');
		
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		
		$passApiKey = $this->variables->get_apikey();
		$data['urlApi'] = $urlApi;
		
		$data['passApiKey'] = $passApiKey;
		$data['idusuario'] = $this->session->userdata('idusuario');
		/*$data['idcolegio'] = $this->session->userdata('idcolegio');
		$data['idgrupo'] = $this->session->userdata('idgrupo');
		$data['nombregrupo'] = $this->session->userdata('nombregrupo');
		$data['ordengrupo'] = $this->session->userdata('ordengrupo');*/
		$data['opcion_menu'] = 'calendario';
		$band = 0;
		foreach ($this->session->userdata('menu') as $menu) //validar permiso de controlador y metodo
                {
                	if(($menu->controlador == $controlador) && ($menu->metodo == 'index'))
                	{
                		$band = 1;// encontro el controlador y el metodo en la sesion, el user tiene permiso
                	}
                }
        if($band == 1)
        {
			//var_dump($this->session->userdata('colegios'));
			$tutor = true;
			$alumno = true;
			foreach ($this->session->userdata('colegios') as $colegio) 
			{
				//var_dump($colegio['rol']);
				if($colegio['rol'] != 'Tutor')
				{
					$tutor = false;
					//die();
				}
				if($colegio['rol'] != 'Alumno')
				{
					$alumno = false;
					//die();
				}

			}
				# code...
			
			if($tutor == true)
						{
							//echo "hola tutores";
							//$data['contenido'] = 'panel/funciones_calendario';
							$data['contenido'] = 'panel/vista_calendario';
							$this->load->view('include/template_tutores',$data);
							//$this->load->view('panel/vista_principal_tutores',$data);
						}
			else if($alumno == true)
			{
				
							//echo "hola tutores";
							//$data['contenido'] = 'panel/funciones_calendario';
							$data['contenido'] = 'panel/vista_calendario';
							$this->load->view('include/template_tutores',$data);
							//$this->load->view('panel/vista_principal_tutores',$data);
				
			}
			else
			{
		    	/*$this->load->view("panel/funciones_calendario",$data); 
		    		
			    $this->load->view('panel/vista_calendario',$data);
			    $this->load->view("include/vista_inc_pie"); */
			    //$this->load->view("include/template_colegio");
			    //$data['contenido'] = 'panel/funciones_calendario';
				$data['contenido'] = 'panel/vista_calendario';
				$this->load->view('include/template_colegio',$data);
			}
				
			//$this->load->view('panel/eventosView',$data);	*/	
		}
		else redirect(404); 
    }
}
?>
