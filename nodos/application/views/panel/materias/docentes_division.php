<script type="text/javascript">
  $(document).ready(function()
  {

      $('.select2').select2({
           theme: "bootstrap",
           placeholder: "Seleccione division",
          allowClear: true
      }); 
  }); 
  
 
</script>
<div class="wrapper wrapper-content animated fadeInRight" id="">
    <div class="row">
        <div class="col-lg-12 " id="usuarios_borrar">
            <div class="ibox collapsed float-e-margins">
                <div class="ibox-title collapse-link" id="tituloaltaevento">
                    <h5>Asignaci&oacute;n de Materias a Profesores </h5>
                        <div class="ibox-tools">
                            <a class="">
                                  
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display:block">
                        
                        <div class="form-group ">
	                    	<LABEL>Seleccione Colegio:</LABEL>
	                    </div>
	                    <div class="row">

	                    	<div class="col-sm-4">
	                        	
		                        <div class="form-group " id="div_select_colegios">
		                           	<select name="select_colegios" id="select_colegios" class="select2 form-control">
		                                    <?php 
		                                        foreach ($acciones as $accion) 
		                                        {
		                                            $value = $accion['idcole'];
		                                            ?>
		                                                <option value="<?php echo $value; ?>"><?php echo $accion['namecole'] ?></option>
		                                            <?
		                                        }
		                                    ?>
		                                                                          
		                            </select>
		                        </div>
		                    </div>
		                    <div class="col-sm-6">
		                        <div class="form-group ">
		                           		<button class="btn btn-sm btn-success " id="btn_colegios" onclick="traer_divisiones(this)" type="button"><strong>Listar Divisiones</strong></button>
		                                                          <div type="hidden" id="loading_horarios_dir_esc_" style="margin-top:-5px">
		                                                          </div>
		                        </div>
		                    </div>

		                </div>
		                 <div  class="form-group " >
		                 	<div >
		                 		<LABEL id="label_seleccione_curso">Seleccione el Curso:</LABEL>
		                 	</div>
	                    	
	                    </div>
		                <div class="row">
		                	<div class="col-sm-4">
		                		<div id="divisiones_colegio">
				                    	
				                </div>
		                	</div>
                      <div class="col-sm-6">
                            <div class="form-group ">
                                  <button class="btn btn-sm btn-success " id="btn_seleccionar_division" onclick="traer_materias_x_anio_div(this)" type="button"><strong>Listar Divisiones</strong></button>
                                                              <div type="hidden" id="loading_horarios_dir_esc_" style="margin-top:-5px">
                                                              </div>
                            </div>
                        </div>
		                </div>
				                
		                <div id="materias_colegio_anio">
		                    	
		                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

// seleccionado el colegio, traer divisiones del colegio y cargar el select de docentes
$('#btn_seleccionar_division').hide();
 $('#label_seleccione_curso').hide();
    function traer_divisiones(img)
    {
      $('#btn_seleccionar_division').show();
    	$('#label_seleccione_curso').show();
    	$('#divisiones_colegio').empty();
    	$('#materias_colegio_anio').empty();
    	$('#div_col_select_docentes').empty();
    	$('#permiso_eliminar_docentens_modal').val(0);
        var url = '<?=base_url("/index.php/materias/docentes_division_traer_divisiones_colegio_ajax")?>';   
      	var idcolegio = $('#select_colegios').val();
      	$.ajax({
          url: url,
          type: 'GET',
          
          data:{idcolegio: idcolegio },
          beforeSend: function() {
          			$('#btn_colegios').hide();
                  $(img).after("<img id='loadingtraer_div' src='<?=base_url('assets/images/loading.gif');?>' height='25' width='25' />");
              },
          success: function(data)
          { 
          	$('#btn_colegios').show();  
            $('#loadingtraer_div').remove();
            var data = jQuery.parseJSON( data );
            //console.log(data['divisiones']);
            divisiones = jQuery.parseJSON( data['divisiones'] );
            if(divisiones.length > 0)
            {
            	var option = '';
            	for(var i=0; i < divisiones.length ; i++)
	            {
	            	//console.log(divisiones[i]);   
	            	var nombre_nivel = divisiones[i]['nombre_nivel']+' '+divisiones[i]['nombre_anio']+' '+divisiones[i]['nombre_division'];
	            	option = option+'<option value="'+divisiones[i]['nodoanios_id']+'_'+idcolegio+'_'+divisiones[i]['division_id']+'">'+nombre_nivel+'</option>';
	            }

	          //  console.log(option);  
	            var select = '<select name="select_divisiones" id="select_divisiones" class="select2 form-control">'+option+'</select>';
	            $('#divisiones_colegio').append(select);
	            $('#select_divisiones').select2({
		           theme: "bootstrap",
		           placeholder: "Seleccione Division",
		          allowClear: true
		        }); 
		        //
		        //console.log(docentes);
		        //alert('sss');
		        if(data['editar'] == 1 )
		        {
		        	$("#div_select_docentes").show();
		        	docentes = jQuery.parseJSON( data['docentes'] );
		        	var option_d = '';
	            	for(var i=0; i < docentes.length ; i++)
		            {
		            	//console.log(divisiones[i]);   
		            	var nombre = docentes[i]['first_name']+' '+docentes[i]['last_name'];
		            	option_d = option_d+'<option value="'+docentes[i]['id_users_groups']+'">'+nombre+'</option>';
		            }
		            var select_d = '<select name="select_docentes" id="select_docentes" class="select2 form-control">'+option_d+'</select>';
		            $('#div_col_select_docentes').append(select_d);
		            $('#select_docentes').select2({
			           theme: "bootstrap",
			           placeholder: "Seleccione Docente",
			          allowClear: true
			        }); 
			        $('#permiso_eliminar_docentens_modal').val(1);
			        $('#button_modal_add_alumno').show();

		        	//console.log(docentes);
		        }
		        else
		        {
		        	//alert('aa');
		        	$('#button_modal_add_alumno').hide();
		        	$("#div_select_docentes").hide();
		        }

		    }
	        

          },
          error: function(response){
            console.log(response);
             $('#btn_colegios').show();  
          }
        });
    	
    }
    function traer_materias_x_anio_div(img)
    {
      var id = $('select[id=select_divisiones]').val();
        $('#divisiones_colegio').hide();
        $('#btn_seleccionar_division').hide();
      //  alert(id);
              //traer_materias(id)
              
        var arre = Array();
        arre=id.split('_');
        var idanio = arre[0];
        var idcolegio = arre[1];
        var iddivision = arre[2];
        //console.log(id);
        //alert(id);
        get_materias_x_anio(img,idcolegio,idanio,iddivision);
    }
    //seleccionado la division, traer materias de la division 
    function get_materias_x_anio(img,id_colegio,idanio,id_division)
    {
    	var url = '<?=base_url("/index.php/materias/get_materias_x_anio_2_ajax")?>';  
    	$('#materias_colegio_anio').empty(); 
    	$.ajax({
          url: url,
          type: 'GET',
          
          data:{idcolegio: id_colegio, nodoanioid: idanio, id_division:id_division },
          beforeSend: function() {
          			//$('#btn_colegios').hide();
                  $(img).after("<img id='loadingtraer_materias' src='<?=base_url('assets/images/loading.gif');?>' height='25' width='25' />");
              },
          success: function(data)
          { 
            $('#btn_seleccionar_division').show();
            var data = jQuery.parseJSON( data );
            if( data['status']===0){
              var string = "<p class='text-danger'>No hay materias asignadas</p>";
              //console.log(string);
              $("#materias_colegio_anio").html(string);

              tableMatAsig=null;
            }
            else
            {
              	//var data = jQuery.parseJSON( data );

              	var cont = 1 ;
    			
    		  	$("#materias_colegio_anio").html(table);
    		  	var filas = '';
    		  	var id_division = 0;
              	data.forEach(function(currentValue,index)
              	{
                //console.log(currentValue);
                	id_division = currentValue.iddivision;
	                var id = currentValue.id;//id de la materia
	                var nombre = currentValue.nombre;
	                var idmat = currentValue.idmateria;// este no es el id de la materia que necesito, es otro id
	                var iddivision = currentValue.iddivision;
	                var string_docente = currentValue.string_docentes;
	            	td_docentes = '<button  class="btn btn-sm btn-info  m-t-n-xs" id="button_add_docente_'+id+'" title="Agregar Docente" onclick="abrir_modal_add_docente(this,'+id+','+id_colegio+','+iddivision+')" type="button"><i class="glyphicon glyphicon-plus"></i></button>';
	            	td_alumnos = '<button class="btn btn-sm btn-success  m-t-n-xs" id="button_add_alumno_'+id+'" title="Asignar Alumnos" onclick="abrir_modal_add_alumnos(this,'+id+','+id_colegio+','+iddivision+')" type="button"><i class="glyphicon glyphicon-user"></i></button>';
	                filas = filas + '<tr id="materia_'+id+'_'+idmat+'">'+
	                              '<td>'+cont+'</td>'+'<td>'+nombre+'</td>'+'<td><label class="" id="string_docente_'+id+'"> '+string_docente+'</label></td>'+

	                              '<td><div align="center">'+
	                              td_docentes+'&nbsp;  &nbsp;'+td_alumnos+
	                              '</div></td>'+

	                          '</tr>';

	                cont++;


              });
              var table = '<br><h3><center>Materias Cargadas</center></h3><br> <table class="table table-striped table-bordered dt-responsive nowrap" id="tabladatos"> <thead> <th>#</th><th>Materia</th><th>Docentes</th><th>Acciones</th></thead><tbody id="tabladatos_body">'+filas+'</tbody></table>';
             
              $("#materias_colegio_anio").html(table);
              
              data.forEach(function(currentValue,index)
              	{
                //console.log(currentValue);
	                var id = currentValue.id;
	                var nombre = currentValue.nombre;
	                var idmat = currentValue.idmateria;
	                if(currentValue.bandera_docentes == 0)
	                {
	                	$('#button_add_alumno_'+id).hide();
	                	if(currentValue.cant_docentes_materia == 0)
						{
							$('#string_docente_'+id).css("color", "red");
						}
	                }

				});
              	cargar_alumnos_tabla_division(id_colegio, id_division);
              //generar_datatable2(arre[1]);
            }
            $('#loadingtraer_materias').remove();
            $('#divisiones_colegio').show();
            $('#btn_colegios').show(); 
          },
          error: function(response){
            console.log(response);
              var string = "No hay alumnos asignados";
              $(".alumnos_tutores").html(string);         
          }
        });
    }
    //abrir modal para asignar y eliminar docentes
    function abrir_modal_add_docente(img,id_materia ,id_colegio, id_division)
    {
    	//alert(id_materia);
    	$('#tbody_docentes_cargados').empty();
    	$('#div_button_asigna_docente').empty();

    	var url = '<?=base_url("/index.php/materias/get_docentes_x_mat_x_div_ajax")?>';  
    	$.ajax({
          url: url,
          type: 'GET',
          
          data:{id_colegio: id_colegio, id_division: id_division,  id_materia:id_materia},
          beforeSend: function() {
          			$('#button_add_docente_'+id_materia).hide();
          			//$('#button_add_alumno_'+id_materia).hide();
                  $(img).after("<img id='loading_modal_docente' src='<?=base_url('assets/images/loading.gif');?>' height='25' width='25' />");
              },
          success: function(data)
          { 
          
              	var data = jQuery.parseJSON( data );
              	console.log(data.cant_docentes);

              	if(data.cant_docentes > 0)
              	{
              		var filas = '';
              		var docentes = data.docentes;
              		for(var i=0; i < docentes.length ; i++)
              		{	
              			var permiso_eliminar = $('#permiso_eliminar_docentens_modal').val();
              			//alert(permiso_eliminar);
              			button_eliminar = '';
              			if(permiso_eliminar == 1)
              			{
              				button_eliminar = '<td><button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="btn_alumnos" title="Eliminar Asignacion" onclick="abrir_modal_confirm_eliminar_docente('+id_colegio+','+id_division+','+id_materia+','+docentes[i]['id_docente']+')" type="button"><i class="fa fa-trash-o"><i></button></td>';
              			}
              			
              			apellido = docentes[i]['last_name'];
              			nombre= docentes[i]['first_name'];
              			documento= docentes[i]['documento'];
              			console.log(docentes[i]);
              			filas = filas + '<tr id="fila_docente_materia_'+id_materia+'_'+id_colegio+'_'+id_division+'_'+docentes[i]['id_docente']+'">'+
	                              '<td>'+apellido+'</td>'+'<td>'+nombre+'</td>'+'<td>'+documento+'</td>'+button_eliminar+
	                          '</tr>';
              		}
              		$('#tbody_docentes_cargados').append(filas);
              		
              	}
              	id_value = id_colegio+'_'+id_division+'_'+id_materia;
              	//agregar el boton asingar al modal, es aqui por q tengo el id de la materia, colegio y division
              	var button_add_docente_div = '<center><a class="btn btn-primary" id ="guardar_registrar_asignacion" onclick="registrar_asignacion_docente(this,'+id_colegio+','+id_division+','+id_materia+')">Asignar Docente</a></center>';

              	$('#div_button_asigna_docente').append(button_add_docente_div);
              	$('#loading_modal_docente').remove();
              	$('#button_add_docente_'+id_materia).show();
          		//$('#button_add_alumno_'+id_materia).show();
            
          },
          error: function(response)
          {
            console.log(response);
              var string = "No hay alumnos asignados";
              $(".alumnos_tutores").html(string);         
          }
        });
        $("#modal_add_docente_materia").modal('show');
    }
    function registrar_asignacion_docente(img,id_colegio,id_division,id_materia)
    {
    	var id_docente = $('select[id=select_docentes]').val();
    	console.log(id_materia);
		var url = '<?=base_url("/index.php/materias/asignar_docente_materia_ajax")?>';   
      	
      	$.ajax({
          url: url,
          type: 'GET',
          
          data:{id_colegio: id_colegio, id_division:id_division,  id_materia:id_materia,  id_docente:id_docente},
          beforeSend: function() {
               
                $('#guardar_registrar_asignacion').hide();
                  $(img).after("<img id='loading_registrar_docente' src='<?=base_url('assets/images/loading.gif');?>' height='25' width='25' />");
              },
          success: function(data)
          { 
            $('#loading_registrar_docente').remove();
            $('#guardar_registrar_asignacion').show();
          	var data = jQuery.parseJSON( data );
            console.log(data);
            if(data.status == 1)
            {
            	button_eliminar = '<td><button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="btn_alumnos" title="Eliminar Asignacion" onclick="abrir_modal_confirm_eliminar_docente('+id_colegio+','+id_division+','+id_materia+','+id_docente+')" type="button"><i class="fa fa-trash-o"><i></button></td>';
            	filas = '<tr id="fila_docente_materia_'+id_materia+'_'+id_colegio+'_'+id_division+'_'+id_docente+'">'+
	                              '<td>'+data.last_name+'</td>'+'<td>'+data.first_name+'</td>'+'<td>'+data.documento+'</td>'+button_eliminar+
	                          '</tr>'
				$('#tbody_docentes_cargados').append(filas);

            	bootbox.alert({
            					title: "Docente: "+data.last_name+' '+data.first_name,
                                 message: '<h3><center>'+data.message+'</center></h3>',
                                size: 'small',
                                //className: 'rubberBand animated'
                               });
            	$('#button_add_alumno_'+id_materia).show();

            	//actrualizar string docentes
            	document.getElementById("string_docente_"+id_materia).innerHTML=data.string_docentes;
            	if(data.cant_docentes_materia > 0)
				{
							$('#string_docente_'+id_materia).css("color", "black");
				}

            }
            else
            {
            	bootbox.alert({
            					title: "Docente: "+data.last_name+' '+data.first_name,
                                message: '<h3><center>'+data.message+'</center></h3>',
                                size: 'small'
                               });
            }
           
	          

          },
          error: function(response){
            console.log(response);
             
          }
        });
		

    }
    function abrir_modal_confirm_eliminar_docente(id_colegio,id_division,id_materia,id_docente)
    {
    	var string_modal = '<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> <button type="button" class="btn btn-primary" onclick="eliminar_docente_materia('+id_colegio+','+id_division+','+id_materia+','+id_docente+')">Eliminar </button>';
    	$('#modal_footer_eliminar_docente').empty();
    	$('#modal_footer_eliminar_docente').append(string_modal);
    	$("#modal_eliminar_docente").modal('show');
    }
    function eliminar_docente_materia(id_colegio,id_division,id_materia,id_docente)
    {
    	$("#modal_eliminar_docente").modal('hide');
    	var url = '<?=base_url("/index.php/materias/borrar_doc_div_mat_ajax")?>';  
    	$.ajax({
          url: url,
          type: 'GET',
          
          data:{id_colegio: id_colegio, id_division: id_division,  id_materia:id_materia, id_docente:id_docente},
          success: function(data)
          { 
          	var data = jQuery.parseJSON( data );
			console.log(data);
			bootbox.alert({
            					title: "Docente: "+data.last_name+' '+data.first_name,
                                 message: '<h3><center>'+data.message+'</center></h3>',
                                size: 'small',
                                //className: 'rubberBand animated'
                               });
			$("#fila_docente_materia_"+id_materia+"_"+id_colegio+"_"+id_division+"_"+id_docente).remove();
			if(data.cant_docentes_materia == 0)
			{
				$('#button_add_alumno_'+id_materia).hide();
			}
			//actrualizar string docentes
				if(data.cant_docentes_materia == 0)
				{
					$('#string_docente_'+id_materia).css("color", "red");
				}
            	document.getElementById("string_docente_"+id_materia).innerHTML=data.string_docentes;
            
          },
          error: function(response)
          {
            console.log(response);
              var string = "No hay alumnos asignados";
              $(".alumnos_tutores").html(string);         
          }
        });
    }

    //ahora viene la parte de asignacion de alumnos a docentes por materia
    function cargar_selec_docentes_registrados_division_materia(id_materia ,id_colegio, id_division)
    {
    	$('#div_col_select_docentes_div_mat').empty();
    	var url = '<?=base_url("/index.php/materias/get_docentes_divsion_materia_ajax")?>';  
    	$.ajax({
          url: url,
          type: 'GET',
          
          data:{id_colegio: id_colegio, id_division: id_division,  id_materia:id_materia},
          success: function(data)
          { 
          	var data = jQuery.parseJSON( data );
			console.log(data.docentes);
			docentes = data.docentes;
		        	var option_d = '';
	            	for(var i=0; i < docentes.length ; i++)
		            {
		            	//console.log(divisiones[i]);   
		            	var nombre = docentes[i]['first_name']+' '+docentes[i]['last_name'];
		            	option_d = option_d+'<option value="'+docentes[i]['id_docente']+'">'+nombre+'</option>';
		            }
		            var select_d = '<select name="select_docentes_div_mat" id="select_docentes_div_mat" class="select2 form-control">'+option_d+'</select>';
		            $('#div_col_select_docentes_div_mat').append(select_d);
		            $('#select_docentes_div_mat').select2({
			           theme: "bootstrap",
			           placeholder: "Seleccione Docente",
			          allowClear: true
			        }); 
			       //alert(docentes[0]['id_docente']);
		            	//$('select[id=select_docentes_div_mat]').val(docentes[0]['id_docente']);
		            	$('#modal_add_alumno_id_docente').val(docentes[0]['id_docente']);
					
			     		id_docente = $('#modal_add_alumno_id_docente').val();
			     	$('#modal_add_alumno_id_materia').val(id_materia);
			    	$('#modal_add_alumno_id_colegio').val(id_colegio);
			    	$('#modal_add_alumno_id_division').val(id_division);
			    	marcar_checkbock_alumnos(id_colegio,id_division,id_materia,id_docente);
    			//	alert(id_docente);
			
            
          },
          error: function(response)
          {
            console.log(response);
              var string = "No hay alumnos asignados";
              $(".alumnos_tutores").html(string);         
          }
        });
    }
    function cargar_alumnos_tabla_division(id_colegio, id_division)
    {
    	//
    	$('#tbody_alumnos_division').empty();
    	var url = '<?=base_url("/index.php/materias/get_alumnos_x_div_ajax")?>';  
    	$.ajax({
          url: url,
          type: 'GET',
          
          data:{id_colegio: id_colegio, id_division: id_division},
          success: function(data)
          { 
          	var data = jQuery.parseJSON( data );
			//console.log(data);
			//console.log(data.alumnos);
			alumnos = data.alumnos;
			var filas = '';
		        	var option_d = '';
	        for(var i=0; i < alumnos.length ; i++)
		    {
		    	j = i+1;
		            	//console.log(divisiones[i]);   
		            	//console.log(alumnos[i]);
		            	var apellido = alumnos[i]['last_name'];
		            	var nombre = alumnos[i]['first_name'];
		            	var documento = alumnos[i]['documento'];
		            	var id_alumno = alumnos[i]['alumno_id'];//es el de users_groups , incripciones
		            	input = '<td><input type="checkbox" name="checkbox_alumno_'+id_alumno+'_'+id_colegio+'_'+id_division+'" id="checkbox_alumno_'+id_alumno+'_'+id_colegio+'_'+id_division+'" /></td>'
		            	
				filas = filas + '<tr id="fila_alumno_'+id_alumno+'_'+id_colegio+'_'+id_division+'">'+input+'<td>'+j+'</td>'+
	                              '<td>'+apellido+'</td>'+'<td>'+nombre+'</td>'+'<td>'+documento+'</td>'+
	                          '</tr>';

		    }
		    $('#tbody_alumnos_division').append(filas);
			
			
            
          },
          error: function(response)
          {
            console.log(response);
              var string = "No hay alumnos asignados";
              $(".alumnos_tutores").html(string);         
          }
        });
    }
    function abrir_modal_add_alumnos(img,id_materia ,id_colegio, id_division)
    {
    	//$('#tbody_alumnos_division').empty();
    	$('#button_add_docente_'+id_materia).hide();
        $('#button_add_alumno_'+id_materia).hide();
        $(img).after("<img id='loading_modal_alumnos' src='<?=base_url('assets/images/loading.gif');?>' height='25' width='25' />");
    	$('#modal_add_alumno_id_docente').val(0);
    	$('#div_button_asigna_alumno').empty();
    	$("#modal_add_alumno_docente_materia").modal('show');
    	cargar_selec_docentes_registrados_division_materia(id_materia ,id_colegio, id_division);

          			
             
    	//marcar alumnos de la division que estan asignados a ese docente
    	/*var id_docente = $('select[id=select_docentes_div_mat]').val();
    	if(typeof id_docente != 'number')
    	{
    		//alert('no es numerico');
    		id_docente = $('#modal_add_alumno_id_docente').val();
    		alert(id_docente);
    	}*/
    	

    	/*$('#modal_add_alumno_id_materia').val(id_materia);
    	$('#modal_add_alumno_id_colegio').val(id_colegio);
    	$('#modal_add_alumno_id_division').val(id_division);
    	marcar_checkbock_alumnos(id_colegio,id_division,id_materia,id_docente);*/
    	//cargar_alumnos_tabla_division(id_colegio, id_division);
    }
    function marcar_checkbock_alumnos(id_colegio,id_division,id_materia,id_docente)
    {
    	var url = '<?=base_url("/index.php/materias/get_alumnos_x_doc_div_mat_ajax")?>';  
    	$.ajax({
          url: url,
          type: 'GET',
          
          data:{id_colegio: id_colegio, id_division: id_division, id_docente:id_docente, id_materia:id_materia },
          success: function(data)
          { 
          	var data = jQuery.parseJSON( data );
			console.log(data);
			//console.log(data.alumnos);
			alumnos = data.alumnos;
			var filas = '';
		        	var option_d = '';
	        for(var i=0; i < alumnos.length ; i++)
		    { 

		            	var id_alumno = alumnos[i]['id_user_group'];//es el de users_groups , incripciones
				//$('#checkbox_alumno_'+id_alumno+'_'+id_colegio+'_'+id_division).attr('checked');
				//console.log('#checkbox_alumno_'+id_alumno+'_'+id_colegio+'_'+id_division);
				//$('#checkbox_alumno_'+id_alumno+'_'+id_colegio+'_'+id_division).prop("checked");  
				$('#checkbox_alumno_'+id_alumno+'_'+id_colegio+'_'+id_division).prop('checked', true);
				//$('#checkbox_alumno_'+id_alumno+'_'+id_colegio+'_'+id_division).prop('checked', false);

				
		    }
		   
			$('#loading_modal_alumnos').remove();
			$('#loading_modal_alumnos_select_docente').hide();
              	$('#button_add_docente_'+id_materia).show();
          		$('#button_add_alumno_'+id_materia).show();
          		$('#div_col_select_docentes_div_mat').show();
            
          },
          error: function(response)
          {
            console.log(response);
              var string = "No hay alumnos asignados";
              $(".alumnos_tutores").html(string);         
          }
        });
    }
    function save_doc_div_mat_alu()
    {
    	var url = '<?=base_url("/index.php/materias/save_doc_div_mat_alu_ajax")?>';  
    	$.ajax({
          url: url,
          type: 'POST',
          
          data: $('#form_doc_div_mat_alu').serialize(),
          success: function(data)
          { 
          	var data = jQuery.parseJSON( data );
			//console.log(data);
			$("#modal_add_alumno_docente_materia").modal('hide');
			//console.log(data.alumnos);
			bootbox.alert({
            					
                                 message: '<h3><center>'+data.message+'</center></h3>',
                                size: 'small',
                                //className: 'rubberBand animated'
                               });
		   
			$('#loading_update_doc_div_mat_alu').remove();
			$('#button_modal_add_alumno').show();
            
          },
          error: function(response)
          {
            console.log(response);
              var string = "No hay alumnos asignados";
              $(".alumnos_tutores").html(string);         
          }
        });
    }
  	
</script>
<script type="text/javascript">
  $(document).ready(function()
  {
  	$('#divisiones_colegio').change(function() 
  	{
  		//alert('id');
  		var id = $('select[id=select_divisiones]').val();
        $('#divisiones_colegio').hide();
      //  alert(id);
            	//traer_materias(id)
              
        var arre = Array();
        arre=id.split('_');
        var idanio = arre[0];
        var idcolegio = arre[1];
        var iddivision = arre[2];
        //console.log(id);
        //alert(id);
        get_materias_x_anio(this,idcolegio,idanio,iddivision);
  	});
  	$('#div_col_select_docentes_div_mat').change(function() 
  	{	
  		$('#loading_modal_alumnos_select_docente').show();
  		$('#div_col_select_docentes_div_mat').hide();
  		
		$("input:checkbox:checked").each(   
		    function() 
		    {
		        //alert("El checkbox con valor " + $(this).val() + " está seleccionado");
		        $(this).prop('checked', false);
		    },
		);
		id_docente = $('select[id=select_docentes_div_mat]').val();
		id_colegio = $('#modal_add_alumno_id_colegio').val();
		id_division = $('#modal_add_alumno_id_division').val();
		
		id_materia = $('#modal_add_alumno_id_materia').val();
		marcar_checkbock_alumnos(id_colegio,id_division,id_materia,id_docente);
  		
  	});
  	$('#button_modal_add_alumno').click(function(event) 
  	{
  		//alert('guardar');
  		$('#button_modal_add_alumno').hide();
                  $('#footer_modal_doc_div_mat_alu').after("<img id='loading_update_doc_div_mat_alu' src='<?=base_url('assets/images/loading.gif');?>' height='35' width='35' />");
  		save_doc_div_mat_alu();

  		
  	});
  	
  	$("#checkbox_select_all_alumnos").on( 'change', function() 
  	{
	    if( $(this).is(':checked') ) 
	    {
	        // Hacer algo si el checkbox ha sido seleccionado
	        console.log("El checkbox con valor " + $(this).val() + " ha sido seleccionado");
	        $("input[type=checkbox]").prop('checked', $(this).prop('checked'));
	       /* $("input:checkbox:checked").each(   
			    function() 
			    {
			        //alert("El checkbox con valor " + $(this).val() + " está seleccionado");
			        $(this).prop('checked');
			    },
			);*/
	    } else {
	        // Hacer algo si el checkbox ha sido deseleccionado
	        console.log("El checkbox con valor " + $(this).val() + " ha sido deseleccionado");
	        $("input:checkbox:checked").each(   
			    function() 
			    {
			        //alert("El checkbox con valor " + $(this).val() + " está seleccionado");
			        $(this).prop('checked', false);
			    },
			);
	    }
	});

	$('#div_select_colegios').change(function() 
  	{
  		//alert('aa');
        traer_divisiones(this);
  	});
     
  }); 
</script>

<!-- modal asignar docentes a materias-->
<div class="modal fade" id="modal_add_docente_materia" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
          		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          		<h4 class="modal-title">Docentes</h4>
      		</div>
      		<center>
      		<form id="form_hijos" autocomplete="on" method="post" class="form-horizontal form-label-left">
        		<input type="hidden" id="user_id_edit" name="user_id_edit" value="0" />
        		<div class="modal-body">
        
           			<input type="hidden" id="permiso_eliminar_docentens_modal" name="permiso_eliminar_docentens_modal" value="0" />
           			<input type="hidden" id="id_colegio_modal_hijos" name="id_colegio_modal_hijos" value="0" />
         			<div id="div_select_docentes" >  

			            <div class="form-group" id="">
			                <label class="col-sm-2 control-label">Seleccione el Docente</label>
			                <div class="col-sm-8" id="div_col_select_docentes">
			                  
			                </div>
			            </div>
			            <div class="form-group" id="div_button_asigna_docente">
			             
			            </div>

			        </div>
					<table class="table table-striped table-bordered dt-responsive nowrap" id="">
						<thead>
							<th>Apellido</th>
							<th>Nombre</th>
							<th>DNI</th>
							
						</thead>
						<tbody id="tbody_docentes_cargados">
										                                  		
						</tbody>
					</table>
      			</div>
        		<div class="modal-footer">

                	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                
                
      		</form>
      		</center>
      	</div>
    </div>
</div>
<!-- modal eliminar docentes a materias-->
<div class="modal fade" id="modal_eliminar_docente" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Eliminar Docente</h4>
								                
			</div>
			<div class="modal-body">
				<p>¿Desea Eliminar el Docente Seleccionado?</p>
			</div>
			<div class="modal-footer" id="modal_footer_eliminar_docente">
				
			</div>
		</div>
	</div>
</div>
<!-- modal asignar alumnos a docentes x materia-->
<div class="modal fade" id="modal_add_alumno_docente_materia" role="dialog">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
          		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          		<h4 class="modal-title">Alumnos</h4>
      		</div>
      		<center>
      		<form id="form_doc_div_mat_alu" autocomplete="on" method="post" class="form-horizontal form-label-left">
        		
        		<div class="modal-body">
        
           			<input type="hidden" id="permiso_eliminar_alumnos_modal" name="permiso_eliminar_alumnos_modal" value="0" />
           			<input type="hidden" id="modal_add_alumno_id_materia" name="modal_add_alumno_id_materia" value="0" />
           			<input type="hidden" id="modal_add_alumno_id_division" name="modal_add_alumno_id_division" value="0" />
           			<input type="hidden" id="modal_add_alumno_id_colegio" name="modal_add_alumno_id_colegio" value="0" />
           			<!--SE USA SOLAMENTE PARA EL PRIMER DOCENTE DEL SELECT -->
           			<input type="hidden" id="modal_add_alumno_id_docente" name="modal_add_alumno_id_docente" value="0" />
           			
           			
         			<div id="div_select_docentes_div_mat" >  
         				<img type="hidden" style="display:none;" id="loading_modal_alumnos_select_docente" src='<?=base_url('assets/images/loading.gif');?>' height="20" width="20">
			            <div class="form-group" id="">
			                <label class="col-sm-2 control-label">Seleccione el Docente</label>

			                <div class="col-sm-8" id="div_col_select_docentes_div_mat">
			                  
			                </div>
			                
			            </div>
			            <div class="form-group" id="div_button_asigna_alumno">
			             
			            </div>

			        </div>
					<table class="table table-striped table-bordered dt-responsive nowrap" id="">
						<thead>
							<th><input type="checkbox" name="checkbox_select_all_alumnos" id="checkbox_select_all_alumnos"/></th>
							<th>#</th>
							<th>Apellido</th>
							<th>Nombre</th>
							<th>DNI</th>
							
						</thead>
						<tbody id="tbody_alumnos_division">
										                                  		
						</tbody>
					</table>
      			</div>
        		<div class="modal-footer" id="footer_modal_doc_div_mat_alu">

                	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                	<button type="button" class="btn btn-success" id="button_modal_add_alumno">Guardar</button>
                
      		</form>
      		</center>
      	</div>
    </div>
</div>
