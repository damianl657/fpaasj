DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtener_eventos_recientes`(IN `user_id` INT)
    NO SQL
SELECT T.id evento_id, 
            T.titulo, 
            T.descripcion, 
            T.fechaInicio, 
            T.confirm, 
            T.fechaCreacion,
            T.group_id,
            T.users_id,
            eventosusers.aceptado,
            eventosusers.visto, 
            eventosusers.id, 
            eventosusers.me_gusta, 
            eventosusers.destinatario,
            eventosusers.notificado,
            eventosusers.motivo,
            eventosusers.medio,
            eventosusers.favorito,
            eventosusers.fecha_notificado,
            T.destacado,
            T.fecha_modificacion,
            T.modificado,
            nodocolegio.logo
            
            FROM eventosusers
            JOIN eventos T ON eventosusers.eventos_id = T.id
            JOIN nodocolegio ON nodocolegio.id = T.colegio_id
            WHERE eventosusers.users_id = user_id
            AND eventosusers.alta != 'b'
            AND eventosusers.visto = 0
            AND eventosusers.notificado = 0
            AND YEAR(T.fechaCreacion) >= nodocolegio.cicloa
            AND T.standby =0
            GROUP BY eventosusers.id, eventosusers.eventos_id, eventosusers.users_id, eventosusers.destinatario
            ORDER BY eventosusers.fechaCreacion DESC$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtener_eventos_enviados`(IN `id_evento` INT)
    NO SQL
SELECT eventos.id evento_id, 
            eventos.id evento_id, 
            eventos.titulo, 
            eventos.descripcion, 
            eventos.fechaInicio, 
            eventos.confirm, 
            eventos.colegio_id,
            eventosusers.aceptado, 
            eventosusers.visto, 
            eventosusers.id, 
            eventosusers.users_id, 
            eventosusers.fechaConfirmacion, 
            eventosusers.fechaVisto, 
            eventosusers.me_gusta, 
            eventosusers.enviado,
            eventosusers.motivo,
            eventosusers.notificado,
            eventosusers.fecha_notificado,
            eventosusers.medio,
            eventosusers.favorito,
            eventos.fecha_modificacion,
            eventos.modificado,
            eventos.destacado
            FROM eventosusers 
            join eventos on eventos.id = eventosusers.eventos_id
            where eventosusers.eventos_id = id_evento 
            and eventosusers.alta <> 'b'$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtener_creador`(IN `user_id` INT)
    NO SQL
SELECT users.first_name nombre_creador,users.last_name apellido_creador,users.sexo sexo_creador,users_groups.group_alias nombre_grupo_alias FROM users 
                            JOIN users_groups ON users_groups.user_id = users.id
                            where users.id = user_id
                            $$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtener_cant_files_x_evento`(IN `id_evento` INT)
    NO SQL
SELECT eventos.*, COUNT(event_file.id) as cantfiles 
FROM eventos 
LEFT JOIN event_file ON eventos.id = event_file.evento_id WHERE eventos.id = id_evento$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtener_eventos`(IN `user_id` INT)
    NO SQL
SELECT T.id evento_id, 
            T.titulo, 
            T.descripcion, 
            T.fechaInicio, 
            T.confirm, 
            T.fechaCreacion,
            T.group_id,
            T.users_id,
            eventosusers.aceptado,
            eventosusers.visto, 
            eventosusers.id, 
            eventosusers.me_gusta, 
            eventosusers.destinatario,
            nodocolegio.logo,
            eventosusers.notificado,
            eventosusers.motivo,
            eventosusers.fecha_notificado,
            eventosusers.favorito,
            T.destacado,
            T.fecha_modificacion,
            T.modificado,
            eventosusers.medio
            FROM eventosusers
            JOIN eventos T ON eventosusers.eventos_id = T.id
            JOIN nodocolegio ON nodocolegio.id = T.colegio_id
            WHERE eventosusers.users_id = user_id
            AND eventosusers.alta != 'b'
            AND YEAR(T.fechaCreacion) >= nodocolegio.cicloa
            AND T.standby =0
            GROUP BY eventosusers.id, eventosusers.eventos_id, eventosusers.users_id, eventosusers.destinatario
            ORDER BY eventosusers.id DESC, eventosusers.fechaCreacion DESC
            LIMIT 5$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `cargar_mas_eventos`(IN `user_id` INT, IN `ultimo` INT)
    NO SQL
SELECT T.id evento_id, 
            T.titulo, 
            T.descripcion, 
            T.fechaInicio, 
            T.confirm, 
            T.fechaCreacion,
            T.group_id,
            T.users_id,
            eventosusers.aceptado,
            eventosusers.visto, 
            eventosusers.id, 
            eventosusers.me_gusta, 
            eventosusers.destinatario,
            eventosusers.notificado,
            eventosusers.motivo,
            eventosusers.medio,
            eventosusers.favorito,
            eventosusers.fecha_notificado,
            T.destacado,
            T.fecha_modificacion,
            T.modificado,
            nodocolegio.logo
            FROM eventosusers
            JOIN eventos T ON eventosusers.eventos_id = T.id
            JOIN nodocolegio ON nodocolegio.id = T.colegio_id
            WHERE eventosusers.users_id = user_id
            AND eventosusers.alta != 'b'
            AND YEAR(T.fechaCreacion) >= nodocolegio.cicloa
            AND T.standby =0
            AND eventosusers.id < ultimo
            GROUP BY eventosusers.id, eventosusers.eventos_id, eventosusers.users_id, eventosusers.destinatario
            ORDER BY eventosusers.id DESC, eventosusers.fechaCreacion DESC
            LIMIT 5$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtener_evento_user_id`(IN `user_id` INT, IN `event_id` INT)
    NO SQL
SELECT T.id evento_id, 
            T.titulo, 
            T.descripcion, 
            T.fechaInicio, 
            T.confirm, 
            T.fechaCreacion,
            T.group_id,
            T.users_id,
            eventosusers.aceptado,
            eventosusers.visto, 
            eventosusers.id, 
            eventosusers.me_gusta, 
            eventosusers.destinatario,
            eventosusers.notificado,
            eventosusers.motivo,
            eventosusers.medio,
            eventosusers.fecha_notificado,
            eventosusers.favorito,
            T.destacado,
            T.fecha_modificacion,
            T.modificado,
            nodocolegio.logo
            FROM eventosusers
            JOIN eventos T ON eventosusers.eventos_id = T.id
            JOIN nodocolegio ON nodocolegio.id = T.colegio_id
            WHERE eventosusers.users_id = user_id
            AND eventosusers.alta != 'b'
            AND eventosusers.eventos_id
            AND YEAR(T.fechaCreacion) = nodocolegio.cicloa
            AND T.standby =0
            GROUP BY eventosusers.eventos_id, eventosusers.users_id, eventosusers.destinatario$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtener_eventos_datos_destinatario`(IN `user_id` INT)
    NO SQL
SELECT users.first_name nombre, users.last_name apellido, users.foto, users.id user_id_destinatario, users.sexo
from users
where users.id = user_id$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtener_eventos_grupo_creador`(IN `group_id` INT)
    NO SQL
SELECT groups.name nombre_grupo, groups.aliasM nombre_grupo_aliasM, groups.aliasF nombre_grupo_aliasF FROM groups  WHERE  id = group_id$$
DELIMITER ;

CREATE DEFINER=`root`@`localhost` PROCEDURE `obtener_eventos_hijos`(IN `destinatarios_id` TEXT, IN `tutor_id` INT)
    NO SQL
SELECT T.id evento_id, 
            T.titulo, 
            T.descripcion, 
            T.fechaInicio, 
            T.confirm, 
            T.fechaCreacion,
            T.group_id,
            T.users_id,
            eventosusers.aceptado,
            eventosusers.visto, 
            eventosusers.id, 
            eventosusers.me_gusta, 
            eventosusers.destinatario,
            eventosusers.notificado,
            eventosusers.motivo,
            eventosusers.fecha_notificado,
            eventosusers.medio,
            eventosusers.favorito,
            T.destacado,
            T.fecha_modificacion,
            T.modificado,
            nodocolegio.logo
            
            FROM eventosusers
            JOIN eventos T ON eventosusers.eventos_id = T.id
            JOIN nodocolegio ON nodocolegio.id = T.colegio_id
            WHERE (FIND_IN_SET(eventosusers.destinatario, destinatarios_id) or eventosusers.users_id = tutor_id)
            AND eventosusers.alta != 'b'
            AND YEAR(T.fechaCreacion) >= nodocolegio.cicloa
            AND T.standby =0
            GROUP BY eventosusers.eventos_id
            ORDER BY eventosusers.id DESC, eventosusers.fechaCreacion DESC
            LIMIT 5$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtener_mas_eventos_hijos`(IN `destinatarios_id` TEXT, IN `tutor_id` INT, IN `ultimo` INT)
    NO SQL
SELECT T.id evento_id, 
            T.titulo, 
            T.descripcion, 
            T.fechaInicio, 
            T.confirm, 
            T.fechaCreacion,
            T.group_id,
            T.users_id,
            eventosusers.aceptado,
            eventosusers.visto, 
            eventosusers.id, 
            eventosusers.me_gusta, 
            eventosusers.destinatario,
            eventosusers.notificado,
            eventosusers.motivo,
            eventosusers.medio,
            eventosusers.favorito,
            eventosusers.fecha_notificado,
            T.destacado,
            T.fecha_modificacion,
            T.modificado,
            nodocolegio.logo
            
            FROM eventosusers
            JOIN eventos T ON eventosusers.eventos_id = T.id
            JOIN nodocolegio ON nodocolegio.id = T.colegio_id
            WHERE (FIND_IN_SET(eventosusers.destinatario, destinatarios_id) or eventosusers.users_id = tutor_id)
            AND eventosusers.alta != 'b'
            AND YEAR(T.fechaCreacion) >= nodocolegio.cicloa
            AND T.standby =0
            AND eventosusers.id < ultimo
            GROUP BY GROUP BY eventosusers.eventos_id
            ORDER BY eventosusers.id DESC, eventosusers.fechaCreacion DESC
            LIMIT 5$$
DELIMITER ;


CREATE PROCEDURE `get_obtener_colegios_x_users`(IN `userid` INT) 
NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER 
SELECT groups.name as nombre_grupo, groups.id as id_grupo, nodocolegio.id as colegio_id, nodocolegio.nombre as nombre_colegio, groups.orden as orden_grupo, nodocolegio.areas 
FROM users 
LEFT JOIN users_groups ON users_groups.user_id = users.id 
LEFT JOIN groups ON groups.id = users_groups.group_id 
LEFT JOIN nodocolegio ON nodocolegio.id = users_groups.colegio_id 
WHERE users.id = userid 
AND users_groups.estado = 1 
GROUP BY nodocolegio.id

CREATE PROCEDURE `get_roles_colegio_users`(IN `userid` INT, IN `idcolegio` INT) NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER SELECT groups.name as nombre_grupo, groups.id as id_grupo, nodocolegio.id as colegio_id, nodocolegio.nombre as nombre_colegio, groups.orden as orden_grupo FROM users JOIN users_groups ON users_groups.user_id = users.id JOIN groups ON groups.id = users_groups.group_id JOIN nodocolegio ON nodocolegio.id = users_groups.colegio_id WHERE users.id = userid AND nodocolegio.id = idcolegio AND users_groups.estado = 1


CREATE PROCEDURE `get_reglas`(IN `idcolegio` INT, IN `idgrupo` INT) NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER SELECT * FROM reglas_comunicacion WHERE colegio_id = idcolegio AND group_id = idgrupo LIMIT 1

CREATE PROCEDURE `get_users_id`(IN `id` INT) NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER SELECT * FROM users WHERE users.id = id

CREATE PROCEDURE `get_nombre_usuario`(IN `id_usser` INT) NOT DETERMINISTIC NO SQL SQL SECURITY DEFINER SELECT first_name, last_name, foto FROM users WHERE users.id = id_usser

--------------------------------------------------------------------------------------
DELIMITER $$
CREATE  PROCEDURE `bajaAlumnoTutores`(in colegio_id int,in division_id int, in alumno_id int )
BEGIN
 
set @rolesTutores := 0; -- cantidad de OTROS roles que tienen los tutores del alumno ( con users_groups.estado = 1 )
select count(u.group_id) as cant into @rolesTutores
from users_groups as u where u.estado = 1 and  u.user_id in
  (select tutoralumno.tutor_id from tutoralumno where tutoralumno.alumno_id = alumno_id)
  and u.group_id not in
  (select groups.id from groups where groups.name = "Tutor" and groups.id_colegio = id_colegio ) ;

set @nrOfBro := 0; 	-- CANTIDAD DE HERMANOS con user_group.estado = 1
SELECT count(*) from (select distinct(t.alumno_id) as id 
				from tutoralumno as t inner join users_groups as u on t.alumno_id = u.user_id
				where t.tutor_id in 
				(select tutoralumno.tutor_id 
                 from tutoralumno
                 where tutoralumno.alumno_id = alumno_id) 
and t.alumno_id != alumno_id and u.estado = 1 ) as a into @nrOfBro;

set @rolesBros := 0; -- cantidad de roles que tienen los hermanos del alumno ( con users_groups.estado = 1 )
SELECT count(usg.group_id) as cant from 
(select distinct(u.user_id) as id 
from tutoralumno as t inner join users_groups as u on t.alumno_id = u.user_id
where t.tutor_id in (select tutoralumno.tutor_id 
from tutoralumno
where tutoralumno.alumno_id = alumno_id) 
and t.alumno_id != alumno_id and u.estado = 1 ) as a 
inner join users_groups as usg on a.id = usg.user_id into @rolesBro;

set @rolesAlumno := 0; -- cantidad de OTROS roles que tienen el alumno ( con users_groups.estado = 1 )
SELECT count(usg.group_id)  from users_groups as usg
where usg.user_id = alumno_id and usg.estado = 1 and usg.group_id not in 
( select distinct(id) from groups where groups.id_colegio = colegio_id and groups.name = "Alumno")
 into @rolesAlumno;

set @rowsAffected := 0;        -- para retornar la cantidad de registros afectados
-- si no tiene hermanos y sus tutores no tienen mas roles  > 
-- > actualizo alumno y tutores (users_groups.estado = 0, inscripciones.activo = 0, user.active = 0)
if  @nrOfBro = 0 and @rolesTutores= 0 and @rolesAlumno = 0 THEN 
   update users_groups as u1 left join inscripciones as i1 
	 on u1.id = i1.alumno_id and i1.divisiones_id = division_id
     left join users as us1 on u1.user_id = us1.id
     left join groups as gps on gps.id = u1.group_id
      set  u1.estado = 0, i1.activo = 0, us1.active = 0
	 where  u1.user_id in 
	 (select tutoralumno.tutor_id from 
     tutoralumno where tutoralumno.alumno_id = alumno_id )
      or (u1.user_id = alumno_id and gps.name = "Alumno"
     and i1.divisiones_id = division_id and u1.estado = 1 and u1.colegio_id = colegio_id);
  end if;
-- si tiene hermanos activos o los tutores tienen mas roles en otros colegios 
-- > actualizo SOLO EL alumno SIN tutores

 if  @nrOfBro > 0 or @rolesBros > 0 or @rolesTutores > 0 or @rolesAlumno > 0 THEN 
       update users_groups as u1 left join 
       inscripciones as i on i.alumno_id = u1.id 
       left join users as us on us.id = u1.user_id
	   left join groups as gps on gps.id = u1.group_id
	   set  u1.estado = 0, i.activo = 0
       where gps.name ="Alumno" and  i.divisiones_id = division_id
       and u1.colegio_id = colegio_id and u1.user_id = alumno_id
	   and  u1.estado = 1;
end if;
select  @rowsAffected := ROW_COUNT() as rowsAffected; -- retorno la cantidad de registros afectados
END$$
DELIMITER ;
--------------------------------------------------------------------------------------
DELIMITER $$
CREATE  PROCEDURE `get_usuariosXrol2`(in p_idcolegio INT, IN p_grupo TEXT ,in p_ids TEXT)
BEGIN
-- Variables para transformar el string de "p_ids" : "(1,2,3,4,...,n)" => en una tabla temporal
DECLARE front TEXT DEFAULT NULL;
DECLARE frontlen INT DEFAULT NULL;
DECLARE TempValue TEXT DEFAULT NULL;
-- creo tabla para almacenar la cadena de "ids de usuarios"
CREATE TEMPORARY TABLE IF NOT EXISTS result_set(v_user_id int(12) DEFAULT 0
) engine = memory;

iterator:
LOOP  
IF LENGTH(TRIM(p_ids)) = 0 OR p_ids IS NULL THEN
LEAVE iterator;
END IF;
SET front = SUBSTRING_INDEX(p_ids,',',1);
SET frontlen = LENGTH(front);
SET TempValue = TRIM(front);
INSERT INTO result_set (v_user_id) VALUES (TempValue);
SET p_ids = INSERT(p_ids,1,frontlen + 1,'');
END LOOP;

SELECT users.id, users.first_name, users.last_name
FROM users_groups inner JOIN users
on users_groups.user_id = users.id
inner join  groups on groups.id = users_groups.group_id
WHERE users_groups.colegio_id =  p_idcolegio 
AND users_groups.estado = 1
AND users.id IN (select * from result_set)
AND groups.name = p_grupo
GROUP BY users.id
ORDER BY users.last_name, users.first_name;
drop table result_set;
END$$
DELIMITER ;
-----------------------------------------------------------------------------------------------
DELIMITER $$
CREATE PROCEDURE `get_reglas`(IN `idcolegio` INT, IN `idgrupo` INT)
    NO SQL
SELECT * FROM reglas_comunicacion WHERE colegio_id = idcolegio AND group_id = idgrupo LIMIT 1$$
DELIMITER ;
-----------------------------------------------------------------------------------------------
CREATE PROCEDURE `get_usuariosEspecificos`(IN p_ids TEXT)
BEGIN
DECLARE front TEXT DEFAULT NULL;
DECLARE frontlen INT DEFAULT NULL;
DECLARE TempValue TEXT DEFAULT NULL;
-- creo tabla para almacenar la cadena de "ids de usuarios"
CREATE TEMPORARY TABLE IF NOT EXISTS result_set(v_user_id int(12) DEFAULT 0
) engine = memory;

iterator:
LOOP  
IF LENGTH(TRIM(p_ids)) = 0 OR p_ids IS NULL THEN
LEAVE iterator;
END IF;
SET front = SUBSTRING_INDEX(p_ids,',',1);
SET frontlen = LENGTH(front);
SET TempValue = TRIM(front);
INSERT INTO result_set (v_user_id) VALUES (TempValue);
SET p_ids = INSERT(p_ids,1,frontlen + 1,'');
END LOOP;

SELECT user_id,first_name, last_name, recibir_notificacion, group_id, email, active, groups.name
FROM users_groups 
INNER JOIN users ON users_groups.user_id = users.id
INNER JOIN groups ON users_groups.group_id = groups.id
WHERE users_groups.user_id IN (select * from result_set)
AND users_groups.estado = 1
GROUP BY user_id;
drop table result_set;
END
---------------------------------------------------------------------------------------------------
DELIMITER $$
CREATE PROCEDURE `is_tutor_en_colegio`(IN p_id_user INT, IN p_id_colegio INT)
BEGIN
SELECT * FROM `users`
            inner JOIN users_groups on users_groups.user_id = users.id
            inner JOIN groups on groups.id = users_groups.group_id
            where groups.name = 'Tutor'
            and users_groups.colegio_id = p_id_colegio
            and users.id = p_id_user;
END$$
DELIMITER ;
