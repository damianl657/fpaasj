<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_migracion extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }

       

        public function  insert_user($datos){
            $this->load->database();
            if($datos['documento']!=''){
                $datosUser = array(
                   'documento'=>$datos['documento'],
                );

                $this->db->where($datosUser);
                
                if(isset($datos['email'])){
                    $this->db->or_where("email", $datos['email']);
                } 

                $query = $this->db->get('users');        
                
                if($query->num_rows()>0){
                    $this->db->where('id',$query->row()->id);
                    $this->db->update("users",$datos);                    
                    return $query->row()->id;
                }
                else {
                        $query2 = $this->db->insert("users",$datos);
                        if($query2)
                            return $this->db->insert_id();
                        else return false;
                    }
            }
            else return false;
        }

        public function  insert_user2($datos){
            $this->load->database();

            if($datos['email']!=''){
                $datosUser = array(
                   'email'=>$datos['email'],
                );
                $this->db->where($datosUser);

                if(isset($datos['documento'])){
                    $this->db->or_where("documento", $datos['documento']);
                }                

                
                $query = $this->db->get('users');        
                
                if($query->num_rows()>0){
                    $this->db->where('id',$query->row()->id);
                    $this->db->update("users",$datos);
                    return $query->row()->id;
                }
                else {
                        $query2 = $this->db->insert("users",$datos);
                        if($query2)
                            return $this->db->insert_id();
                        else return false;
                    }
            }
            else return false;
        }


        public function  insert_user_group($datos){
            $this->load->database();

            $datosUserG = array(
                'user_id' => $datos['user_id'],
                'group_id' => $datos['group_id'],
                'colegio_id' =>$datos['colegio_id'],
            );  

            $this->db->where($datosUserG);
            $query = $this->db->get('users_groups');               
            
            if($query->num_rows()>0){
                return $query->row()->id;
            }
            else {
                    $query2 = $this->db->insert("users_groups",$datos);
                    if($query2)
                        return $this->db->insert_id();
                    else return false;
                }
        }

        public function  insert_user_group2($datos){
            $this->load->database();

            $datosUserG = array(
                'user_id' => $datos['user_id'],
                //'group_id' => $datos['group_id'],
                //'colegio_id' =>$datos['colegio_id'],
            );  

            $this->db->where($datosUserG);
            $query = $this->db->get('users_groups');

            $datosUserG = array(
                'user_id' => $datos['user_id'],
                'group_id' => $datos['group_id'],
                'colegio_id' =>$datos['colegio_id'],
            );                 
            
            if($query->num_rows()>0){
                $this->db->update("users_groups",$datosUserG,array("id"=>$query->row()->id));
                return $query->row()->id;
            }
            else {
                    $query2 = $this->db->insert("users_groups",$datos);
                    if($query2)
                        return $this->db->insert_id();
                    else return false;
                }
        }        

        public function  insert_inscripcion2($datos){
            $this->load->database();

            $datosInsc = array(
                'alumno_id' => $datos['alumno_id'],
                //'divisiones_id' => $datos['divisiones_id'],
                //'planesestudio_id' =>$datos['planesestudio_id'],
            );  

            $this->db->where($datosInsc);
            $query = $this->db->get('inscripciones');
            $datosInsc = array(
                'alumno_id' => $datos['alumno_id'],
                'divisiones_id' => $datos['divisiones_id'],
                'planesestudio_id' =>$datos['planesestudio_id'],
            );              
            
            if($query->num_rows()>0){
                $this->db->update("inscripciones",$datosInsc,array("id"=>$query->row()->id));
                
                return $query->row()->id;
            }
            else {
                    $query2 = $this->db->insert("inscripciones",$datos);
                    if($query2)
                        return $this->db->insert_id();
                    else return false;
                }
        }

        public function  insert_tutoralumno($datos){
            $this->load->database();

            $this->db->where($datos);
            $query = $this->db->get('tutoralumno');        
            
            if($query->num_rows()>0){
                return false;
            }
            else {
                    $query2 = $this->db->insert("tutoralumno",$datos);
                    if($query2)
                        return $this->db->insert_id();
                    else return false;
                }
        }
            
    } // fin Model_usuario