<?php //session_start();
 
class Eventos extends CI_Controller {
      
	function __construct(){
		parent::__construct();
		//$this->output->enable_profiler(TRUE);
		if(!$this->session->userdata("logged_in")){
               redirect("login");
        }
	}

    public function index(){         
        $urlApi = $this->variables->get_urlapi(); 
		$passApiKey = $this->variables->get_apikey();
		$data['urlApi'] = $urlApi;
		$data['passApiKey'] = $passApiKey;
		$data['idusuario'] = $this->session->userdata('idusuario');
		
		
		$band = 0;
		$controlador = "Eventos";
		$metodo = "index";
		

        $colegios = $this->session->userdata("colegios"); 
        $colegiosPermi = array();
        $acciones = array();
        $banAddMat = 0;
		foreach ($colegios as $cole)
		{	
			//print_r($cole); die();
			$idcolegio = $cole["id_colegio"];
			$menus = $cole["menu"];
		 	foreach ($menus as $menu)
			{
				if( ($menu->controlador === $controlador) && ($menu->metodo === $metodo) )
				{
				    $band = 1;

				    /*$arre = array(
				    	'idcole'=>$idcolegio,
				    	'namecole'=>$cole["nombre_colegio"],
				    	'idrol'=>$cole["id_grupo"],
				    	'namerol'=>$cole["rol"],
				    );
				    $colegiosPermi[]=$arre;

				    $result = $this->get_eventos_para_autorizar_curl($urlApi, $passApiKey, $data['idusuario'], $idcolegio, $cole["id_grupo"], $cole["rol"]);

				    //print_r($result); die(); 
				    if($result->status){
				    	$data['eventos'][$idcolegio] =  $result->eventos;
				    }*/
				}
			}
			
			/*foreach ($menus as $menu)
			{	
				if( $menu->nombre === $accion1)
				    $banAddMat = 1;
			}*/

		}

		//echo $band;
        if($band == 1)
        {
    		//$data['colegios'] = $colegiosPermi;
    		//$data['opcion_menu'] = 'autorizar';

    		$data['opcion_menu'] = 'eventos';
			
		    $data['contenido'] = 'panel/eventosView';
			$this->load->view('include/template_colegio',$data);
				
		}
		//else redirect(404);
			
    }	

    //public function traer_vista($id,$descripcion,$titulo,$apellido_creador,$nombre_creador,$apellido,$nombre,$fechaInicio,$aceptado,$confirm,$visto,$me_gusta,$nombre_grupo,$foto_url,$logo_url)
    public function traer_vista()
    {
    	
    	//	var_dump($this->input->post("prueba"));
    	$evento = $this->input->post("prueba");
    	//echo $evento['id_e'];
		//echo 'id:'.$id;echo 'descripcion:'.$descripcion;echo 'titulo:'.$titulo;echo 'apellido_creador:'.$apellido_creador;echo 'nombre_creador:'.$nombre_creador;echo 'apellido:'.$apellido;echo 'nombre:'.$nombre;echo 'fechaInicio:'.$fechaInicio;echo 'confirm:'.$confirm;echo 'visto:'.$visto;echo 'nombre_grupo:'.$nombre_grupo;echo 'foto_url:'.$foto_url;echo 'logo_url:'.$logo_url;

		/*$caracteres_malos = array("<", ">", "\"", "'", "/", "<", ">", "'", "/", "%20");
		$caracteres_buenos = array("& lt;", "& gt;", "& quot;", "& #x27;", "& #x2F;", "& #060;", "& #062;", "& #039;", "& #047;", " ");
		$descripcion = str_replace($caracteres_malos, $caracteres_buenos, $descripcion);
		$titulo = str_replace($caracteres_malos, $caracteres_buenos, $titulo);
		$visto = str_replace($caracteres_malos, $caracteres_buenos, $visto);
		$apellido_creador = str_replace($caracteres_malos, $caracteres_buenos, $apellido_creador);
		$nombre_creador = str_replace($caracteres_malos, $caracteres_buenos, $nombre_creador);
		$apellido = str_replace($caracteres_malos, $caracteres_buenos, $apellido);
		$nombre = str_replace($caracteres_malos, $caracteres_buenos, $nombre);
		$fechaInicio = str_replace($caracteres_malos, $caracteres_buenos, $fechaInicio);
		$id = str_replace($caracteres_malos, $caracteres_buenos, $id);
		$nombre_grupo = str_replace($caracteres_malos, $caracteres_buenos, $nombre_grupo);
		$foto_url = str_replace("***", "/", $foto_url);
		$logo_url = str_replace("***", "/", $logo_url);*/
		//$confirm = str_replace($caracteres_malos, $caracteres_buenos, $confirm);
		$data['id'] = $evento['id_e'];
		$data['descripcion'] = $evento['descripcion_e'];
		$data['titulo'] = $evento['titulo_e'];
		$data['apellido_creador'] = $evento['apellido_creador_e'];
		$data['nombre_creador'] = $evento['nombre_creador_e'];
		$data['apellido'] = $evento['apellido_e'];
		$data['nombre'] = $evento['nombre_e'];
		$data['fechaInicio'] = $evento['fechaInicio_e'];
		$data['foto'] = $evento['foto_e'];
		$data['logo'] = $evento['logo_e'];
		$data['confirm'] = $evento['confirm_e'];
		$data['visto'] = $evento['visto_e'];
		$data['me_gusta'] = $evento['me_gusta_e'];
		$data['nombre_grupo'] = $evento['nombre_grupo_e'];
		$data['eventoid'] = $evento['eventoid'];
		$data['cantfiles'] = $evento['cantfiles'];
		$data['cant_me_gusta'] = $evento['cant_me_gusta'];
		
		if(isset($evento['urls_fotos']))
		{
			$data['urls_fotos'] = $evento['urls_fotos'];
		}
		


		//print_r($data);
		//$this->input->post("prueba");
		//$data['prueba'] = $this->input->post("prueba");
		//var_dump($this->input->post("prueba"));
		$confirm = $evento['confirm_e'];
		$aceptado = $evento['aceptado_e'];
		if($confirm == 1)
		{
			if($aceptado == 0)
			{
				$this->load->view('panel/eventos/evento2',$data);
			}
			if($aceptado == -1)
			{
				$this->load->view('panel/eventos/evento1',$data);//aceptado -1
			} 
			
		}
		
		if($confirm == 1 && $aceptado == 1)
		{
			$this->load->view('panel/eventos/evento3',$data);
		}
		if($confirm == 0)
		{
			$this->load->view('panel/eventos/evento4',$data);
		}
    
    }

    

    //---------------------------------------- CURL request to API -------------------------------------------


    public function get_eventodatos_curl($urlApi, $passApiKey, $idUser, $idevento)  
    {
    	$url = $urlApi."/evento/evento/id/".$idevento;

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			//CURLOPT_POST => true,
			//CURLOPT_POSTFIELDS => array('rol' => $rol,'id_colegio' =>$idcolegio),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);
		//$response = json_decode($response);
		//return json_decode($response); 

		return $response;
	}


	public function get_eventofiles_curl($urlApi, $passApiKey, $idUser, $idevento)  
    {
    	$url = $urlApi."/upload/get_files/idevento/".$idevento;

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			//CURLOPT_POST => true,
			//CURLOPT_POSTFIELDS => array('rol' => $rol,'id_colegio' =>$idcolegio),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);

		//$response = substr($response, 3);
		//$response = json_decode($response);
		//return json_decode($response); 

		return $response;
	}



    //---------------------------------------- AJAX request from WEB -------------------------------------------

    public function get_eventodatos_ajax(){ 

	    $urlApi = $this->variables->get_urlapi(); 
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');
		
		$idevento = $_GET['id'];
		

		$result=$this->get_eventodatos_curl($urlApi, $passApiKey, $idusuario, $idevento);

		//print_r($result); die();
		
		if( (isset($result->status)) && ($result->status=='') ){
			$arre = array(
				'status' => 0,
             	'message' => 'No se encuentra registrado tal evento'
			);
			$arre=json_encode($arre);
			print_r($arre);
		}
		else 
			print_r($result);
	}


	public function get_eventofiles_ajax(){ 

	    $urlApi = $this->variables->get_urlapi(); 
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');
		
		$idevento = $_GET['idevento'];
		

		$result=$this->get_eventofiles_curl($urlApi, $passApiKey, $idusuario, $idevento);

		print_r($result); die();
		
		if( (isset($result->status)) && ($result->status=='') ){
			$arre = array(
				'status' => 0,
             	'message' => 'No se encuentra registrado tal evento'
			);
			$arre=json_encode($arre);
			print_r($arre);
		}
		else 
			print_r($result);
	}		


}
?>
