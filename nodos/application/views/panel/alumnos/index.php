
<script type="text/javascript">


  passApiKey = '<?php echo $passApiKey; ?>';
   urlApi = '<?php echo $urlApi; ?>';

   idUser = '<?php echo $idusuario; ?>';
   //idcolegio = '<?php //echo $idcolegio; ?>';
   //idgrupo = '<?php //echo $idgrupo; ?>';
   //nombregrupo = '<?php //echo $nombregrupo; ?>';
   //ordengrupo = '<?php //echo $ordengrupo; ?>';
   var colegios = JSON.parse('<?php echo json_encode($this->session->userdata("colegios")); ?>');

   console.log(colegios);
   var arre_activar_user = Array();
   var arre_alta_user = Array();
   var arre_admin_roles = Array();
   var arre_edita_user = Array();
   var arre_listar_user = Array();
   var arre_asigna_tutor = Array();




  if(colegios)
  {
      for(var i=0; i < colegios.length ; i++)
      {
        menu = colegios[i]["menu"];
        if(menu)
          for(var j=0; j < menu.length ; j++)
          {
            //console.log(menu[i]["nombre"]);
            if(menu[j]["nombre"] == "activar_user")
            {
                //colegios a los que puedo activar usuarios
                arre_activar_user.push(colegios[i]["id_colegio"]);
            }
            else
            if(menu[j]["nombre"] == "alta_usuarios")
            {
                //colegios a los que puedo cargar usuarios
                arre_alta_user.push(colegios[i]["id_colegio"]);
            }
            else
            if(menu[j]["nombre"] == "admin_roles")
            {
                //colegios a los que puedo administrar (alta y baja) roles
                arre_admin_roles.push(colegios[i]["id_colegio"]);
            }
            else
            if(menu[j]["nombre"] == "editar_user")
            {
                //colegios a los que puedo administrar (alta y baja) roles
                arre_edita_user.push(colegios[i]["id_colegio"]);
            }
            else
            if(menu[j]["nombre"] == "listado_usuarios")
            {
                //colegios a los que puedo administrar (alta y baja) roles
                arre_listar_user.push(colegios[i]["id_colegio"]);
            }
            else
            if(menu[j]["nombre"] == "agregar_tutor")
            {
                arre_asigna_tutor.push(colegios[i]["id_colegio"]);
            }
           
          }
       
      }

      var arre_alta_user_join = arre_alta_user.join();
      var arre_listar_user_join = arre_listar_user.join();

  }


</script>

  
<div class="wrapper wrapper-content animated fadeInRight" id="general2">
  <div class="row">
    <div class="col-lg-12 ">
      <div class="ibox collapsed float-e-margins">
        <div class="ibox-title collapse-link2" id="tituloaltaevento">
          <h5>Deportista <small>Inscripciones al Torneo</small></h5>
          <div class="ibox-tools">
            <a class="">
              <i class="fa fa-chevron-down"></i>
            </a>
          </div>
        </div>
        <div class="ibox-content" style="display: block">
          <div class="row row2">
            <div class="col-sm-12 b-r " id="contenido2">                                     
              <form id="altainscripcion" class="form-horizontal form-label-left" method="post">
                <div id="form_roles_edit">        
                  <div class="form-group" id ="colegios_edit"></div>
                  <div class="form-group"  id="grupo_documento">
                    <label for="txt_documento">Documento</label>
                    <input type="text" class="form-control" id="txt_documento" placeholder="Ingrese un numero de Documento">
                  </div>
                  <div class="form-group"  id="grupo_documento">
                    <button id="btn_buscar" class=" btn btn-primary">Buscar</button>
                  </div>
                  <div id="datosuser">
                    <div class="form-group" id="grupo_documento">
                      <label for="txt_apellido">Apellido</label>
                      <input type="text" class="form-control" id="txt_apellido" placeholder="" disabled>
                    </div>
                    <div class="form-group" id="grupo_documento">
                      <label for="txt_nombre">Nombre:</label>
                      <input type="text" class="form-control" id="txt_nombre" placeholder="" disabled>
                      <input type="hidden" class="form-control" id="txt_id_user" placeholder="" disabled>
                    </div>  
                  </div>
                  <div class="form-group" id="grupoEspecialidad">
                    <label for="campo_especialidad">Especialidad</label>
                    <select class="form-control select2" id="select_especialidad" name="select_especialidad" placeholder="Seleccione Torneo" required>                                            
                      <div id="opcion"></div>                                                  
                    </select>
                  </div>
                  <div class="form-group" id="grupoDivision">
                    <label for="select_division">Divisi&oacute;n</label>
                    <select class="form-control select2" id="select_division" name="select_division" placeholder="Seleccione Torneo" required>                                            
                      <div id="opcion"></div>                                                  
                    </select>
                  </div>
                  <div class="form-group" id="grupocaregoria">
                    <label for="select_division">Categoria</label>
                    <select class="form-control select2" id="select_categoria" name="select_categoria" placeholder="Seleccione Torneo" required>                                            
                      <div id="opcion"></div>                                                  
                    </select>
                  </div>
                    <input id="btn_guardar" type="button" class="btn btn-outline-success" value="Guardar">
                  </div>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight" id="general2">
  <div class="row">
    <div class="col-lg-12 ">
      <div class="ibox collapsed float-e-margins">
        <div class="ibox-title collapse-link2" id="tituloaltaevento">
          <h5>LISTADO <small>Lista de inscriptos</small></h5>
          <div class="ibox-tools">
            <a class="">
              <i class="fa fa-chevron-down"></i>
            </a>
          </div>
          <div class="ibox-content" style="display: block">
            <div class="row row2">
              <div class="col-sm-12 b-r  table-responsive" id="contenido2"> 
                <button id="btn_imprimir">Imprimir</button>
                <table class="table table-striped" id="tablainscriptos">
                  <thead>
                    <tr>
                      <th scope="col">Documento</th>
                      <th scope="col">Apellido</th>
                      <th scope="col">Nombre</th>
                      <th scope="col">Divisional</th>
                      <th scope="col">Club</th>
                      <th scope="col">Categoria</th>
                    </tr>
                  </thead>
                  <tbody id="listainscriptos"> 

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal modificar -->
<div class="modal" id="modalmodificar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modificar </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
           <div class="col-sm-12 b-r " id="contenido2">          
              <form id="" class="form-horizontal form-label-left" method="post">
                <div id="form_roles_edit">        
                  <div class="form-group" id ="colegios_edit"></div>
                  <div class="form-group"  id="grupo_documento">
                    <label for="txt_documento">Documento</label>
                    <input type="text" class="form-control" id="editar_documento" placeholder="Ingrese un numero de Documento" disabled>
                    <input type="hidden" class="form-control" id="editar_idisncripcion" placeholder="" disabled>

                  </div>                 
                  <div id="datosuser">
                    <div class="form-group" id="grupo_apellido">
                      <label for="txt_apellido">Apellido</label>
                      <input type="text" class="form-control" id="editar_apellido" placeholder="" disabled>
                    </div>
                    <div class="form-group" id="grupo_nombre">
                      <label for="txt_nombre">Nombre:</label>
                      <input type="text" class="form-control" id="editar_nombre" placeholder="" disabled>
                      <!--  <input type="hidden" class="form-control" id="txt_id_user" placeholder="" disabled> -->
                    </div>  
                  </div>
                  <div class="form-group" id="grupoEspecialidad">
                    <label for="campo_especialidad">Especialidad</label>
                    <select class="form-control select2" id="selecteditar_especialidad" name="select_especialidad" placeholder="Seleccione Torneo" required>
                      <div id="opcion"></div>                                                  
                    </select>
                  </div>
                  <div class="form-group" id="grupoDivision">
                    <label for="selecteditar_division">Divisi&oacute;n</label>
                    <select class="form-control select2" id="selecteditar_division" name="selecteditar_division" placeholder="Seleccione Torneo" required>
                      <div id="opcion"></div>                                                  
                    </select>
                  </div>
                </div>
              </form>
           </div>
      </div>
      <div class="modal-footer">
        <div>
            <input id="btn_guardar_editar" type="button" class="btn btn-outline-success" value="Guardar">
            <input id="cerrar" type="button" class="btn btn-outline-danger" data-dismiss="modal" value="Cerrar">
        </div>       
      </div>
    </div>
  </div>
</div>
<!-- fin del modal modificar -->
<!-- Modal eliminar -->
<div class="modal" id="modaleliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Eliminar inscripci&acute;n</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-outline-success">Eliminar</button>
      </div>
    </div>
  </div>
</div>
<!-- fin del modal eliminar -->

<script>
 
 function crear_select_colegios(accion)
{   
      //verificar si puede editar datos de usuarios. LISTADO DE USUARIOS!!
      var cont_col = 0;
      var edit_users = [];
      //var colegios = JSON.parse('<?php echo json_encode($this->session->userdata("colegios")); ?>');
      for(var i=0; i < colegios.length ; i++)
      {
        var bandera = false;                 
        menu = colegios[i]["menu"];
        //console.log('menu');
        //console.log(colegios[i]);
        for(var j=0; j < menu.length ; j++)
        {
                            //console.log('menu: '+menu[j]["nombre"]);
          if((menu[j]["nombre"] == accion))
          {
            cont_col = cont_col + 1;
            bandera = true;
            //alert('entro');
          }
          
        }
        if(bandera == true)
        {
          edit_users[i] = 1; //1 SI PUEDE
          //alert('entro');
        } 
        else
        {
          edit_users[i] = 0; //1 SI PUEDE
        }
                         
      }
      //console.log('el arreglo de colegios quedo asi:');
     

      if( cont_col > 0)
      {
        options = '<option value="0">Seleccione Club.....</option>';
        for(var i=0; i < colegios.length ; i++)
        {
          if(edit_users[i] == 1)
          {
             options = options + '<option value="'+colegios[i]["id_colegio"]+'"  >'+colegios[i]["nombre_colegio"]+'</option>';
          }
        }
        select_edit = '<label class="col-sm-2 control-label"  >Club:</label><div class="col-sm-8">'+
        '<select class="form-control input-md" id="select_colegio_edit" name="select_colegio_edit">'+options+      
        '</select></div>';
        $("#colegios_edit").append(select_edit);
        $('#select_colegio_edit').select2({
           theme: "bootstrap",
           placeholder: "Seleccione..",
           allowClear: true
        }); 
         //alert('entro');
       
      }
     
    }
    function obtener_datos_user(dni){
       var url ='<?=base_url("/alumno/obtener_datos_user") ?>';    
    
      $.ajax({
        url: url,
        type: 'POST',
        data: {doc: dni},
        beforeSend: function() 
          {
            $('#btn_buscar').after("<img  id='loading_alta_user' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");
            $('')

           
          },
        success: function(data){
          
          $("#txt_apellido").val("");
          $("#txt_nombre").val("");
          data= JSON.parse(data);         
          console.log(data);
                
                  $.each(data, function(index, val) {                       
                     
                      $("#txt_apellido").val(val.first_name);
                      $("#txt_nombre").val(val.last_name);
                      $("#txt_id_user").val(val.id_depo);
                      
                      $("#loading_alta_user").remove();                                  
                  });  
                  $("#loading_alta_user").remove();
                
                   
        }, error: function (jqXHR, textStatus, errorThrown,data)
            {
              // document.getElementById("myModalLabel").innerHTML = 'Registro de Usuarios';
                    //document.getElementById("mensaje").innerHTML = 'ERROR';
                    //$("#myModal").modal('show');
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                //console.log(data);
              


            }
      }) 
      
   

    }
    function Edad(FechaNacimiento) {

    var fechaNace = new Date(FechaNacimiento);
    var fechaActual = new Date()

    var mes = fechaActual.getMonth();
    var dia = fechaActual.getDate();
    var año = fechaActual.getFullYear();

    fechaActual.setDate(dia);
    fechaActual.setMonth(mes);
    fechaActual.setFullYear(año);

    edad = Math.floor(((fechaActual - fechaNace) / (1000 * 60 * 60 * 24) / 365));
    
    return edad;
}
    function listar_especialidad(){
      var url ='<?=base_url("/alumno/obtener_especialidad") ?>';    

      $.ajax({
        url: url,
        type: 'POST',
        data: {},
        success: function(data){
          //alert(data);
          opt="<option value='0' disabled>Selecione una Especialidad........</option>";
          data= JSON.parse(data);
          console.log(data);  
          $('#select_especialidad').html("");
         
          $.each(data, function(index, val) {                       
            opt= opt +'<option value="'+val.id+'">'+val.nombre+'</option>';              
                       });  
             $('#select_especialidad').append(opt);
        }
      }) 
      $('#select_especialidad').select2({        
        placeholder: "Select a state",
        allowClear: true
  
      });  
      
  }
function listar_especialidad_editar(){
      var url ='<?=base_url("/alumno/obtener_especialidad")?>';    

      $.ajax({
        url: url,
        type: 'POST',
        data: {},
        success: function(data){
          //alert(data);
          data= JSON.parse(data);
          console.log(data); 
          opt='<option disabled value="0">seleccione especialidad......</option>'
                  
          $('#selecteditar_especialidad').html("");
          $.each(data, function(index, val) {                      
                          opt = opt + '<option value="'+val.id+'">'+val.nombre+'</option>';
                       });  
                          $('#selecteditar_especialidad').append(opt);
        }
      }) 
      $('#selecteditar_especialidad').select2();  
      
  }

  function listar_division(esp){
      var url ='<?=base_url("/alumno/obtener_division") ?>';    
  
      $.ajax({
        url: url,
        type: 'POST',
        data: {id_esp: esp},
        success: function(data){
          //alert(data);
          opt = '<option disabled value="0"> seleccione divisi&oacute;n</option>';
          data= JSON.parse(data);       
                  
          $('#select_division').html("");
          $.each(data, function(index, val) {
                          opt = opt + '<option value="'+val.id+'">'+val.nombre+'</option>';
                       });  
                          $('#select_division').append(opt);
        }
      }) 
      $('#select_division').select2();  
      
  }
    function listar_division_editar(esp){
      var url ='<?=base_url("/alumno/obtener_division")?>';
  
      $.ajax({
        url: url,
        type: 'POST',
        data: {id_esp: esp},
        success: function(data){
          //alert(data);
          data= JSON.parse(data);
                           
          opt="<option value='0'>selecione una divisi&oacute;n</option>";
          $('#selecteditar_division').html("");
         
          $.each(data, function(index, val) {                       
           opt= opt + '<option value="'+val.id+'">'+val.nombre+'</option>';                
                       });  
                          $('#selecteditar_division').append(opt);
        }
      }) 
      $('#selecteditar_division').select2();  
      
  }
  
   function guardar_inscripcion(user,div){

      var url ='<?=base_url("/alumno/guardar_inscripcion") ?>';         
      var datos=user+'_'+div;
      $.ajax({
        url: url,
        type: 'POST',
        data:{ userdiv:datos },
         beforeSend: function() 
          {
            $('#btn_buscar').after("<img  id='loading_alta_user' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");
            $.notify("Prosesando...","info");
           
          },
        success: function(data){


           $.notify("Guardado con exito....","success");
           window.location="<?=base_url("/alumno")?>";
         
        }
      }) 
      
      
  }
  function traer_categoria(fecha,divis,indice){
    var url ='<?=base_url("/alumno/obtener_categoria") ?>';
    var edad = Edad(fecha);
    var categoria;
    $.ajax({
        url: url,
        type: 'POST',
        data: {edad:edad,
               div:divis},
               success: function(data){
          //alert(data);
          opt = '<option disabled value="0"> seleccione categoria</option>';
          data= JSON.parse(data);       
                  
          $('#select_categoria').html("");
          $.each(data, function(index, val) {
                          opt = opt + '<option value="'+val.id+'">'+val.nombre+'</option>';
                       });  
                          $('#select_categoria').append(opt);
        }
      /*success: function(data){
        //alert(data);
        data= JSON.parse(data);
        //$("#categoria").html("");
        $.each(data, function(index, val) {
           $("#cate_"+indice).html("<b>"+val.descripcion+"</b>");               
          
        });        
      }*/
    });
 
   
  }
 
  function listar_inscriptos(id){
      var url ='<?=base_url("/alumno/listar_inscriptos")?>';    
       var categoria="";
        
      $.ajax({
        url: url,
        type: 'POST',
        data: {club:id},
        success: function(data){
          //alert(data);
          data= JSON.parse(data);
          //console.log(data); 
             i=0;
           $("#listainscriptos").html("");
          $.each(data, function(index, val) {
          traer_categoria(val.fechanac,val.divisiones_id,i);
             
           
             $("#listainscriptos").append(
              '<tr>'
                +'<th scope="row">'+val.documento+'</th><td style="text-transform: uppercase">'+val.last_name+'</td><td style="text-transform: capitalize">'+val.first_name+'</td><td>'+val.nombre+'</td><td>'+val.nombreclub+'</td><td><div id="cate_'+i+'"></div></td>'
                  +'<td>'
                    +'<div class="btn-group" role="group" aria-label="Basic example">'
                      +'<button type="button"  class="btn btn-warning" data-toggle="modal" data-target="#modalmodificar"  onclick="buscar_user('+val.id+','+val.divisiones_id+')">Modificar</button>'
                      +'<button data-toggle="modal" data-target="#modaleliminar" type="button" class="btn btn-primary" onclick="">Eliminar</button>'
                  +'</div>'
                  
                  +'</td>'
              +'</tr>'

              );
            i++;
          });

        }
      }) 
      
      
  }
  function buscar_user(id,division){
   /* alert(id);*/
    var url ='<?=base_url("/alumno/buscar_user") ?>';    

      $.ajax({
        url: url,
        type: 'POST',
        data: {user:id,div:division},
        
        success: function(data){
         
          data= JSON.parse(data);
          console.log(data);                   
          
          $.each(data, function(index, val) {
            $("#editar_documento").val(val.documento);
            $("#editar_nombre").val(val.first_name);
            $("#editar_apellido").val(val.last_name);
            $("#editar_idisncripcion").val(val.id_inscripcion);

            
          });
        }
      }) 
      
    
    
  }
  function update_inscripcion(idins,div){
     var url ='<?=base_url("/alumno/update_inscripcion") ?>';         
      
      $.ajax({
        url: url,
        type: 'POST',
        data:{ iscrip:idins, division:div },
         beforeSend: function() 
          {
            
            $.notify("Prosesando...","info");
           
          },
        success: function(data){


           $.notify("Guardado con exito....","success");
           window.location="<?=base_url("/alumno")?>";
         
        }
      }) 
  }
  function imprimelistado(id){  
    var parametros = id;
    var url = '<?=base_url("/index.php/alumno/listado")?>'; 
    console.log(parametros);
    console.log(url);
    window.open(url+'/'+parametros,'_blank');
  }
 


$(document).ready(function() {
    $("#grupoEspecialidad").hide();
    $("#grupoDivision").hide();
    $("#datosuser").hide();
    listar_especialidad_editar();

    $("#btn_buscar").click(function(event) {
       $("#grupoEspecialidad").show();
       $("#datosuser").show(); 
       var dni=$("#txt_documento").val();
       obtener_datos_user(dni);
       
    });
    crear_select_colegios('listado_usuarios');

    $("#select_colegio_edit").change(function(){  
      var club = $('#select_colegio_edit').val();
      console.log(club);
      listar_especialidad();
      listar_inscriptos(club);

    });    
    
    $("#select_especialidad").change(function(){   
      //alert('entro');
      $("#grupoDivision").show();   
      var especialidad = $('#select_especialidad').val();
      listar_division(especialidad);
    });
    $("#select_eficiencia").change(function(){
      alert("entro");
    });
    $("#btn_guardar").click(function(event) {
     var user=$("#txt_id_user").val();
     var div=$("#select_division").val();
     guardar_inscripcion(user,div);
    });

    $("#selecteditar_especialidad").change(function(){  
    //alert('entro');
      var especialidad = $('#selecteditar_especialidad').val();
      listar_division_editar(especialidad);
    });
     $("#btn_guardar_editar").click(function(event) {
     var inscrip=$("#editar_idisncripcion").val();
     var div=$("#selecteditar_division").val();
     update_inscripcion(inscrip,div);
    });
     $("#btn_imprimir").click(function(event) {
     //alert("holas");
    });
    


});
    
    
     


    
</script>