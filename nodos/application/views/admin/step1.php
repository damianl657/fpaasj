 
<script type="text/javascript">

var cantNivelesAnt=0;
var cantNiveles=0;

	var arreTerminadoP1=Array();
	//FUNCION PARA capturar momento en que terminan los dos procesos. regreso de las 2 peticiones iniciales
	function terminadoP1(pet){ //alert(pet);					
		if(pet==1)
			arreTerminadoP1[0]=1; //cargo cualquier cosa
		if(pet==2)
			arreTerminadoP1[1]=1;

		if(arreTerminadoP1.length==2)
		{
			$('#cuerpo img:last-child').remove();
			$('#paso1').css('display','block');
		}

		$('#paso1').css('display','block');
	}


	//FUNCION PARA VACIAR LA TABLA HTML NIVELES
	function vaciar_nivelesP1(){				
			$(".soltableN").remove(); //li
	}

	//FUNCION PARA VACIAR LA TABLA HTML contenedores
	function vaciar_contenedoresP1(){				
			$(".sueltaP1").parent('div').remove();
	}



	function baja_nodonivel(idnodonivel)
	{
			var urlRequest = urlApi+"/nivel/baja_nivel";																  		   		   		 
		  	$.ajax({
                url: urlRequest,
                type: 'POST',
                headers: {		
			        'APIKEY' : passApiKey,
			        'userid' : idUser
			    },
			    data: {idnodonivel: idnodonivel},
                success: function(data){
                    //console.log(data);
					if(data['status']==false)
                   	{
                   		if(data['message'] == 'NO LOGUIN')		                                  	
		                    location.href = "<?php echo base_url('login'); ?>";
		                else
		                {           			                  
	                     	$('.top-right').notify({
		                        message: { text: data['message'] },
		                        fadeOut: { enabled: true, delay: 5000 },
		                        type: 'danger2'
		                    }).show(); 
		                }
		            }	                    	              	
                },
                error: function(response){
                    console.log(response);                  
                }
            });
        
	}


	function listar_niveles_cargados()
	{ 
			vaciar_contenedoresP1();
			var url = urlApi+"/nivel/niveles_cargados";		

			$.ajax({
	                url: url,
	                type: 'GET',
	                headers: {				      
				        'APIKEY' : passApiKey,
				        'userid' : idUser
				    },
				    data: {idcolegio: idcolegio},
				    beforeSend: function() {
	                  $('#divNivelesSelec').after("<img id='loadingNivC' src='<?=base_url('assets/images/loading.gif');?>' height='30' width='30' />");
	                },
	                success: function(data){ //console.log(data);               	                	
	                	
	                	if(data['status']==false)
	                	{	
                   		 	if(data['message'] == 'NO LOGUIN')		                                  	
		                     	location.href = "<?php echo base_url('login'); ?>";
               			}
                   		else
                   		{							

		                	var datos = jQuery.parseJSON( data );
		                	//console.log(datos);
		         			
		                    for(var i=0; i <  (datos.length) ; i++)
							{
							 	cantNiveles++;
							 	
							 	var text="<div class='box-drag'>"+
											"<h4>Arrastre nivel aqu&iacute;</h4>"+
											"<div class='inner-box-drag sueltaP1' id='suelta_"+i+"' name='suelta'>"+
												"<ul><li>"+
													"<span>"+
														"<div id='nivel_"+i+"' class='' name='arrastrable'>"+datos[i]['nombre_nivel']+
												 			"<input type='hidden' value='"+datos[i]['niveles_id']+"'/>"+
												 		"</div>"+
													"</span>"+
													"<span class='delete' id='quitarP1_"+i+"'><i title='quitar' class='fa fa-trash-o' value='"+datos[i]['idnodonivel']+"'></i> </span>"+
									 			"</li></ul>"+
											"</div>"+
										 "</div>";
			    
			    				$('#topeConte').before(text);

			    				//$("#suelta_"+i).data("numsoltar", 1);
							    						 
							    $('#quitarP1_'+i).click(function(){ 
									var idnodonivel = $(this).find('i').attr('value');	

									var ul = $(this).parent('li').parent('ul');		
									//var ul = $(this).parent('span').parent('li').find('[name="arrastrable"]').parent('span');											
									bootbox.confirm("¿Desea Eliminar Nivel?", function(result)
									{ 
										console.log(result);
										if(result)
										{
											baja_nodonivel(idnodonivel);				
											$(ul).remove();
											//cantNivelesAnt--;
											//$('#cantN').val(cantNivelesAnt);
										}
									});

								});


								$("#suelta_"+i).droppable({
							        activeClass: 'ui-state-hover',
							        hoverClass: 'ui-state-active',
							        drop: function( event, ui ) { 				      					       				        	
									        var elem = $(this);

									        var lleno = $(this).find('[name="arrastrable"]');
									       					         					        				   
									        if ( $(lleno).length == 0 ) //no puedo soltar mas de una cajita
									        {        				           
										        $(this).find('ul').remove();	

										        var ul = "<ul><li></ul></li>";					        
										        $(this).append(ul);

										        var li = $(this).find('li');
										        ui.draggable.appendTo(li).css({
										            top: '0px',
										            left: '0px',
										            width: '90%',
										            float: 'left'						            
										        });

										        var tacho = "<span class='delete' style='float: right; margin-left:-5px'><i id='' title='quitar' class='fa fa-trash-o' </i> </span>";
										        var lleno2 = $(this).find('[name="arrastrable"]');
										        $(lleno2).after(tacho);

										        
										        $('.delete').click(function(){
											    	//siempre vuelve al origen
													var tr = $(this).parent('li');
													var drag = $(tr).find('.arrastrableP1');
													var contenedorOrigen = $(tr).find('.arrastrableP1').data("idDivorigen") ;	
																	
													drag.appendTo( $('#'+contenedorOrigen) ).css({				         
											        	width: '100%',
											            float: ''						            
											        });
													           												
													//$(tr).find('.arrastrableP1').remove();	
													$(tr).parent('ul').remove();
												});
										        	
					                            return;					      			 				       					      	
									        } else 
									        {
									        	// Cancel drag operation (make it always revert)		 
									        	ui.draggable.draggable('option','revert',true);
									            return;
									        }					         	
									       						       				               					   
									     } ,
							        out: function(event, ui) {
							        		//var elem = $(this); 
										    //elem.data("numsoltar", 0); 
										    				
							       		 }
									 });
									

							}//fin del for

							cantNivelesAnt = datos.length;
							$('#cantN').val(datos.length);
						
							terminadoP1(2);
						}
						$('#loadingNivC').remove(); // eliminar ruedita.

					},
                error: function(response){
                    console.log(response);
                    
                }
					 	
	       });				 										
	}
			

	//FUNCION PARA TRAER  MOSTRAR TODOS LOS NIVELES
	function mostrar_nivelesP1()
	{		
		  var url = urlApi+"/nivel/niveles";		
		  //var datos = QueryAPIget(urlRequest, passApiKey);
		  $.ajax({
                url: url,
                type: 'GET',
                headers: {			   
			        'APIKEY' : passApiKey,
			        'userid' : idUser
			    },
			    beforeSend: function() {
	                $('#nivelesP1').after("<img id='loadingNiveles' src='<?=base_url('assets/images/loading.gif');?>' height='50' width='50' />");
	            },
                success: function(data){ //console.log(data);        

                		if(data['status']==false)
                   		{ 	
                   			if(data['message'] == 'NO LOGUIN')		                                  	
		                     	location.href = "<?php echo base_url('login'); ?>";
                   		}
                   		else
                   		{
		                	vaciar_nivelesP1();

		                	var datos = jQuery.parseJSON( data );
		         			//console.log(datos);
		                    for(var i=0; i <  (datos.length) ; i++)
							 {						 											 	
							 	text="<li id='sueltaN_"+i+"' class='soltableN'>"+
							 			"<div id='nivel_"+i+"' class='arrastrableP1' name='arrastrable'>"+
							 				 datos[i]['nombre']+
							 				 "<input type='hidden' value='"+datos[i]['id']+"'/>"+ 	
							 			"</div>"+							 			
							 		  "</li>";
							    
							    $('#ListNivelesP1 li:last').after(text);

							    $('#nivel_'+i).data("idDivorigen", 'sueltaN_'+i); 
							 	//$('.arrastrableP1').data("idDivorigen", 'sueltaN_'+i);										 
							 }
							 	 
							    //$('#nivel_'+i).draggable({ 
							    $('.arrastrableP1').draggable({ 	
							    		//containment: '#well',							   
							    		start: function(ui) { $(this).data("soltado", false); 
							    							  $(this).addClass('ui-state-hover'); 
							    		},
							    		stop: function(ui) { $(this).data("soltado", true );    
							    							 $(this).removeClass('ui-state-hover');	   
							    		},
							    		//drag: function(ui) { console.log( $(ui.target).parent('div') ); },
								        
								        revert:  function(dropped) { 
								        					          
								           var ban = dropped &&  dropped[0].getAttribute("name") == "suelta";  //solo puedo soltar en la clase suelta
							       								           
								           								         
								           return !ban;
									    }						    
								}).each(function() {
									    var top = $(this).position().top;
									    var left = $(this).position().left;
									    $(this).data('orgTop', top);
									    $(this).data('orgLeft', left);
								});			

						terminadoP1(1);
						}	
						$('#loadingNiveles').remove();	     
                },
                error: function(response){
                    console.log(response);
                }
            });
     	
	}


	//FUNCTION PARA INSERTAR UN NUEVO NIVEL
	function add_nivelP1(newn){
		  var urlRequest = urlApi+"/nivel/nivel";														
		  var parametros = {
            'nombre': newn     
        	};
          var stringJson = JSON.stringify(parametros);	
		  
		  $.ajax({
                url: urlRequest,
                type: 'POST',
                headers: {		
			        'APIKEY' : passApiKey,
			        'userid' : idUser
			    },
			    data: {data: stringJson},
			    beforeSend: function() {
                  	$('#addnivel').after("<img id='loadingNiv' src='<?=base_url('assets/images/loading.gif');?>' height='30' width='30' />");
                },
                success: function(data){

                	$('#loadingNiv').remove(); // eliminar el reloj de loading..
                    
                    //console.log(data);
					if(data['status']==false)
                   	{	 //alert(data['message']);
                   		if(data['message'] == 'NO LOGUIN')		                                  	
		                     	location.href = "<?php echo base_url('login'); ?>";
                   		
                   		if(data['message'] == 'existe')                                              	
                   		{ 
                   		 	$('.top-right').notify({
		                        message: { text: 'El Nivel ya Existe' },
		                        fadeOut: { enabled: true, delay: 5000 },
		                        type: 'warning2'
		                    }).show();		
                     	}
                     }
                   	else{	                   					    
					    	  $('.top-right').notify({
		                        	message: { text: 'Guardado' },
		                       	 	fadeOut: { enabled: true, delay: 5000 },
		                       		type: 'success2'
		                    	}).show();

					    	   text="<li id='sueltaN_"+data['id']+"' class='soltableN'>"+
							 			"<div id='nivel_"+data['id']+"' class='arrastrableP1' name='arrastrable'>"+newn+
							 				"<input type='hidden' value='"+data['id']+"'/>"+ 
							 			"</div>"+
							 		 "</li>";	
				   						   
							    
							    $('#ListNivelesP1 li:last').after(text);

							 	$('#nivel_'+data['id']).data("idDivorigen", 'sueltaN_'+data['id']); 


							    $('#nivel_'+data['id']).draggable({ 
							    		//containment: '#well',	
							    		start: function(ui) { $('#nivel_'+data['id']).data("soltado", false);
							    			$(this).addClass('ui-state-hover'); 
							    		},
							    		stop: function(ui) { $('#nivel_'+data['id']).data("soltado", true ); 
							    			$(this).removeClass('ui-state-hover');
							    													   
							    		},
							    		//drag: function(ui) { console.log( $(ui.target).parent('div') ); },
								        
								        revert:  function(dropped) { 
								        					          
								           var ban = dropped &&  dropped[0].getAttribute("name") == "suelta";  //solo puedo soltar en la clase suelta

								           /*if(ban==true)
								           {
								           		if( $(dropped[0]).data("numsoltar") == 1)
									            {							       
									           		ban=0;								           		
									            }
									            else $(dropped[0]).data("numsoltar",1);
								           }*/								          

								           return !ban;
									       }						    
									    }).each(function() {
									        var top = $(this).position().top;
									        var left = $(this).position().left;
									        $(this).data('orgTop', top);
									        $(this).data('orgLeft', left);
									    });				
							 
                   	}
                   	$('#loadingNiv').remove();
                },
                error: function(response){
                    //console.log(response);
                    
                }
            });
	}	
	

	function Listar_contenedoresP1()
	{
		var cantN = $('#cantN').val();
	
		if(cantN < cantNivelesAnt)  //si disminuye la cantidad. por ej de 3 a 2
		{	
			for(var j=0; j < (cantNivelesAnt-cantN) ; j++)
			{	
				var tr = $('.sueltaP1'); 
				var long = tr.length - 1;
				var todo = $(tr[long]).parent('div');
				
				var drag = $(tr[long]).find('.arrastrableP1');
				//console.log(drag);
				if(drag.length>0)
				{
					var contenedorOrigen = $(drag).data("idDivorigen") ;				
					drag.appendTo( $('#'+contenedorOrigen) ).css({				         
			        	width: '100%',
			            float: ''						            
					});
				}

				$(todo).remove();
			}
		}
		else{

			 for(var i=cantNivelesAnt; i < (cantN) ; i++)
			 {			
			 	cantNiveles++;			 					

			 	var text="<div class='box-drag'>"+
								"<h4>Arrastre nivel aqu&iacute;</h4>"+
								"<div class='inner-box-drag sueltaP1' id='suelta_"+cantNiveles+"' name='suelta'>"+									
								"</div>"+									
							"</div>";	 				
			    
			    $('#topeConte').before(text);
			   
				
				$("#suelta_"+cantNiveles).droppable({
			        activeClass: 'ui-state-hover',
			        hoverClass: 'ui-state-active',
			        drop: function( event, ui ) { 				      					       				        	
					        var elem = $(this);

					        var lleno = $(this).find('[name="arrastrable"]');
					       					         					        				   
					        if ( $(lleno).length == 0 ) //no puedo soltar mas de una cajita
					        {        				           
						        $(this).find('ul').remove();	

						        var ul = "<ul><li></ul></li>";					        
						        $(this).append(ul);

						        var li = $(this).find('li');
						        ui.draggable.appendTo(li).css({
						            top: '0px',
						            left: '0px',
						            width: '90%',
						            float: 'left'						            
						        });

						        var tacho = "<span class='delete' id='quitarP1_"+cantNiveles+"' style='float: right; margin-left:-5px'><i   title='quitar' class='fa fa-trash-o' </i> </span>";
						        var lleno2 = $(this).find('[name="arrastrable"]');
						        $(lleno2).after(tacho);

						        
						        //$('.delete').click(function(){
						        $('#quitarP1_'+cantNiveles).click(function(){ 
							    	//siempre vuelve al origen
									var tr = $(this).parent('li');
									var drag = $(tr).find('.arrastrableP1');
									var contenedorOrigen = $(tr).find('.arrastrableP1').data("idDivorigen") ;	
													
									drag.appendTo( $('#'+contenedorOrigen) ).css({				         
							        	width: '100%',
							            float: ''						            
							        });
									           												
									//$(tr).find('.arrastrableP1').remove();
									$(tr).parent('ul').remove();
									//cantNivelesAnt--;
									//$('#cantN').val(cantNivelesAnt);
								});
						        	
	                            return;					      			 				       					      	
					        } else 
					        {
					        	// Cancel drag operation (make it always revert)			 
					        	ui.draggable.draggable('option','revert',true);
					            return;
					        }					         	
					       						       				               					   
					     } ,
			        out: function(event, ui) {
			        		/*var elem = $(this); 
						    elem.data("numsoltar", 0); 
						    */				
			       		 }
					 });

			
			 } //fin for
		}//fin else
		
		cantNivelesAnt = cantN;
	}

	
	//FUNCTION PARA GUARDAR DATOS DEL PASO 1
	function guardar_paso1(arre){
		  var urlRequest = urlApi+"/nivel/insertniveles";																  
		  var parametros = Array(); 
		  
		  for(var i=0; i<(arre.length); i++)
		  {
		  		var fila = {
			      //'nodocolegio_id': colegio,
			      'niveles_id': arre[i]
			    };
			    parametros.push( fila );
		  }
		  	
          var stringJson = JSON.stringify(parametros);

          //console.log(stringJson);	
		  
		  $.ajax({
                url: urlRequest,
                type: 'POST',
                headers: {		
			        'APIKEY' : passApiKey,
			        'userid' : idUser
			    },
			    data: {data: stringJson, idcolegio:idcolegio, idusergrupo:idusergrupo},
                success: function(data){
                    console.log(data);
					if(data['status']==true)
                   	{	 
                   		$('.top-right').notify({
		                        message: { text: data['message'] },
		                        fadeOut: { enabled: true, delay: 5000 },
		                        type: 'success2'
		                }).show(); 		                              		
                   	}
                   	else{
                   		if(data['message'] == 'NO LOGUIN')		                                  	
		                     location.href = "<?php echo base_url('login'); ?>";
		                else                                  	
	                     	$('.top-right').notify({
		                        message: { text: data['message'] },
		                        fadeOut: { enabled: true, delay: 5000 },
		                        type: 'danger2'
		                    }).show();	                    	
                   	}
                },
                error: function(response){
                    console.log(response);
                    
                }
            });
	}


	//FUNCION PARA RESETEAR TODO EL FORMULARIO = F5
	function resetP1()
	{
		mostrar_nivelesP1();
		$('#cantN').val(0);
		cantNivelesAnt = 0;
		listar_niveles_cargados();
	}


	$(document).ready(function(){	
	
		//$("#derecho").css('position','static');

		$('#addnivel').click(function(e){ 
			e.preventDefault();
			
			$('#newnivel').css("display",'block');
			$('#newnivel').focus();
		 });

		$('#newnivel').keypress(function(e) {
			    if(e.which == 13) {
			       var newn = $('#newnivel').val();
			       if(newn != '')
			       	{					       		 				       			
			       		  add_nivelP1(newn);
			       		  $('#newnivel').css("display",'none');
			       		  $('#newnivel').val('');
			       	}
			       	else alert('ingrese nivel');
			    }
			});

		/*PASO 1 - ELEGIR NIVELES */
		$('#cantN').change(function(){ 
					
				Listar_contenedoresP1();
		});



		/*PASO 1 - ENVIAR NIVELES para guardar y pasar al PASO 2 */

		$('#next1').click(function(e){
			e.preventDefault(); 
					
			var arre = Array(); 

			var hiddens = $('#divNivelesSelec').find('[name="arrastrable"]').find('input');

			for(var i=0; i <  (hiddens.length) ; i++)
			{
				//alert( $(hiddens[i]).val() );
				arre.push( $(hiddens[i]).val() );
			}				

			guardar_paso1(arre);

            mostrar_especialidad_step2();
            listar_especialidades_cargadas();
            
            //slide steps
            $('#paso1').slideUp();
            $('#paso2').slideDown();   			
		});


	});         			 				   
			
</script>


	<div class = "paso1" id="paso1" style='display:none'>

		<!-- Indicadores de Pasos -->
		<div class="container-fluid nopadding">
			<div class="row nopadding center indicadores-pasos">
				<div id="paso-nivel" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding">
					Paso 1: Niveles <span><i class="fa fa-check-circle-o"></i></span>
				</div>
				<div id="paso-especialidad" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding">
					Paso 2: Especialidades <span><i class="fa fa-check-circle-o"></i></span>
				</div>
				<div id="paso-ano" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding">
					Paso 3: A&ntilde;os <span><i class="fa fa-check-circle-o"></i></span>
				</div>
				<div id="paso-division" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding">
					Paso 4: Divisiones <span><i class="fa fa-check-circle-o"></i></span>
				</div>
			</div>
		</div>
		
		<div class="container-fluid">
			
			<!-- Descripción Paso -->
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center vspace2">
					<h3>Paso 1</h3>
					<h6>Defina cantidad de niveles de la instituci&oacute;n</h6>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center vspace">
					<p>Defina la cantidad de niveles que tiene la instituci&oacute;n.<br>
					Selccione y arrastre los niveles correspondientes a su instituci&oacute;n de la columna de la izquierda hacia la derecha.</p>
				</div>
			</div>

			<div style="overflow: hidden; height:auto">
				
			    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" >
					<div class="box-config">
						<h3>Niveles Disponibles</h3>
						<div class="inner-box-config"  id='nivelesP1'  >
							<ul id="ListNivelesP1" >
								<li></li>
								<!-- AQUI AGREGO LOS LI CON JQUERY -->     	    
							</ul>							
						</div>
						<a id='addnivel' class="btn-principal-azul" href="#">Agregar Nivel</a>
						<input id='newnivel' placeholder='Ingrese Nivel luego Presione Enter' style='display:none'/>
					</div>
				</div>

		        		    

			    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="box-config">
						<h3>Niveles de Instituci&oacute;n</h3>
						<div class="inner-box-config" id='divNivelesSelec'>
							<div>
								<div class="row">
									<div class="col-lg-5 col-md-5 col-sm-6 col-xs-5">
										<form>
											<label>Cantidad de niveles:</label>
										</form>
									</div>
									<div class="col-lg-7 col-md-7 col-sm-6 col-xs-7">
										<form>
										  <input type="number" placeholder="Ingrese un n&uacute;mero y presione enter" id="cantN">
										</form>
									</div>
								</div>
							</div>
							
							<!-- AQUI AGREGO LOS DIV CON JQUERY -->							
							
							<div  id="topeConte"></div>

						</div>
						<a id='addnivel' class="btn-principal-azul" href="#" align='center' onclick="resetP1();">Reset</a>
					</div>
				</div>

		    </div>
		</div>

	<!--<button id='next1' class="btn btn-danger" style='float:right; margin-right:5%'>Siguiente</button>-->

	<!-- Anterior/Siguiente -->
	<div class="row vspace4" id='next1'>
		<div class="col-lg-offset-5 col-lg-2 col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 col-xs-12">
			<a class="btn-principal-azul" href="#">Siguiente Paso</a>
		</div>
	</div>
	    
    </div>
   
    

<style type="text/css">
   
    
    .sueltaP1{
        /*padding: 10px;
        margin: 10px;
        border: 1px solid #ddd;
        width: 200px;
        height: auto;
        float: right;
        text-align: center;*/

        min-height: 50px;
        
    }

    .soltableN{
        /*padding: 10px;
        border: 1px solid #ddd;*/
        width: 80%;
        /*min-height: 30%;*/
        height: auto;
        /*float: right;*/
        text-align: center;
        margin: 10px;
    }
 
 	
 	.arrastrableP1{
 		/*width: 80%;
 		height: 20%;
 		min-height: 70px;
	
 		background-color: #f99;*/
 		width: 100%;
 		min-height: 40px;
 		height: auto;
 		text-align: center;
 		padding-top: 2%;
 		/*margin-left: 8%;*/
 		border: 1.5px solid #CCC;
 		border-radius: 2px;
 		cursor: move;
 		z-index: 100;

 	}

 	/*.arrastrableP1-2{
 		/*width: 80%;
 		height: 20%;
 		/*min-height: 70px;
 		border: 1px solid #CCC;
 		text-align: center;*
 		position: relative;
 		min-height: 40%;
 		border: 1px solid #CCC;
 		background-color: #f99;
 		z-index: 100;

 	}*/

 	

	.glyphicon-trash
	{
		cursor:pointer;
	}
    
    </style>