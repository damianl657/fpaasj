 <? include("application/views/template/cabecera.php"); ?>
<style>
	canvas {
		-moz-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
	}
	.chart-container {
		width: 800px;
		
	}
	.container_graficos {
	
		flex-direction: row;
		flex-wrap: wrap;
		justify-content: center;
	}
</style>
<!-- page content -->
<div class="right_col" role="main">
<form id="cursos" autocomplete="on" method="post" class="form-horizontal form-label-left">
  
  <div class="form-group">
    <label class="col-sm-3 control-label">Seleccione Divisiones</label>

    <div class="col-sm-9">
                          
      <div class="input-group">
      
                            
        </select>
        <span class="input-group-btn">
        <button id="calcular" type="button" class="btn btn-primary">Calcular</button>
        </span>
      </div>
    </div>
  </div>
</form>
<!-- top tiles -->
<div class="row tile_count">
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Total de Alumnos</span>
    <div class="count"><p id="contador_de_alumnos"> </p></div>
    
   
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Total de Varones</span>
    <div class="count"><p id="contador_varones_division"> </p></div>
   
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Total de Mujeres</span>
    <div class="count"><p id="contador_mujeres_division"> </p></div>
   
  </div>
 
  <!--<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Total Females</span>
    <div class="count">4,567</div>
    <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last Week</span>
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Total Collections</span>
    <div class="count">2,315</div>
    <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
  </div>
  <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-user"></i> Total Connections</span>
    <div class="count">7,325</div>
    <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
  </div>-->
</div>
<!-- /top tiles -->


<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="dashboard_graph">

      <div class="row x_title">
        <div class="col-md-6">
          <h3>Rendimiento <small>por division</small></h3>
        </div>
        <div class="col-md-6">
          
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xs-12">
        <div id="placeholder33" style="height: 460px; display: none" class="demo-placeholder"></div>
        <div >
          <div id="grafico_promedios_trimestres" class="container_graficos" style=" height:470px;"></div>
        </div>
      </div>
      

      <div class="clearfix"></div>
    </div>
  </div>

</div>
<br />
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="dashboard_graph">

      <div class="row x_title">
        <div class="col-md-6">
          <h3>Inasistencias <small>por division</small></h3>
        </div>
        <div class="col-md-6">
          
        </div>
      </div>

      <div class="col-md-12 col-sm-12 col-xs-12">
        <div id="placeholder33" style="height: 460px; display: none" class="demo-placeholder"></div>
        <div >
          <div id="grafico_inasistencias" class="container_graficos" style=" height:470px;"></div>
        </div>
      </div>
      

      <div class="clearfix"></div>
    </div>
  </div>

</div>
<br />
<div class="row">


  <!--<div class="col-md-4 col-sm-4 col-xs-12">
    <div class="x_panel tile fixed_height_320">
      <div class="x_title">
        <h2>App Versions</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <h4>App Usage across versions</h4>
        <div class="widget_summary">
          <div class="w_left w_25">
            <span>0.1.5.2</span>
          </div>
          <div class="w_center w_55">
            <div class="progress">
              <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 66%;">
                <span class="sr-only">60% Complete</span>
              </div>
            </div>
          </div>
          <div class="w_right w_20">
            <span>123k</span>
          </div>
          <div class="clearfix"></div>
        </div>

        <div class="widget_summary">
          <div class="w_left w_25">
            <span>0.1.5.3</span>
          </div>
          <div class="w_center w_55">
            <div class="progress">
              <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 45%;">
                <span class="sr-only">60% Complete</span>
              </div>
            </div>
          </div>
          <div class="w_right w_20">
            <span>53k</span>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="widget_summary">
          <div class="w_left w_25">
            <span>0.1.5.4</span>
          </div>
          <div class="w_center w_55">
            <div class="progress">
              <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 25%;">
                <span class="sr-only">60% Complete</span>
              </div>
            </div>
          </div>
          <div class="w_right w_20">
            <span>23k</span>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="widget_summary">
          <div class="w_left w_25">
            <span>0.1.5.5</span>
          </div>
          <div class="w_center w_55">
            <div class="progress">
              <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 5%;">
                <span class="sr-only">60% Complete</span>
              </div>
            </div>
          </div>
          <div class="w_right w_20">
            <span>3k</span>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="widget_summary">
          <div class="w_left w_25">
            <span>0.1.5.6</span>
          </div>
          <div class="w_center w_55">
            <div class="progress">
              <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 2%;">
                <span class="sr-only">60% Complete</span>
              </div>
            </div>
          </div>
          <div class="w_right w_20">
            <span>1k</span>
          </div>
          <div class="clearfix"></div>
        </div>

      </div>
    </div>
  </div>-->

  <!--<div class="col-md-4 col-sm-4 col-xs-12">
    <div class="x_panel tile fixed_height_320 overflow_hidden">
      <div class="x_title">
        <h2>Device Usage</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table class="" style="width:100%">
          <tr>
            <th style="width:37%;">
              <p>Top 5</p>
            </th>
            <th>
              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                <p class="">Device</p>
              </div>
              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                <p class="">Progress</p>
              </div>
            </th>
          </tr>
          <tr>
            <td>
              <canvas id="canvas1" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
            </td>
            <td>
              <table class="tile_info">
                <tr>
                  <td>
                    <p><i class="fa fa-square blue"></i>IOS </p>
                  </td>
                  <td>30%</td>
                </tr>
                <tr>
                  <td>
                    <p><i class="fa fa-square green"></i>Android </p>
                  </td>
                  <td>10%</td>
                </tr>
                <tr>
                  <td>
                    <p><i class="fa fa-square purple"></i>Blackberry </p>
                  </td>
                  <td>20%</td>
                </tr>
                <tr>
                  <td>
                    <p><i class="fa fa-square aero"></i>Symbian </p>
                  </td>
                  <td>15%</td>
                </tr>
                <tr>
                  <td>
                    <p><i class="fa fa-square red"></i>Others </p>
                  </td>
                  <td>30%</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>-->


  <!--<div class="col-md-4 col-sm-4 col-xs-12">
    <div class="x_panel tile fixed_height_320">
      <div class="x_title">
        <h2>Quick Settings</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="dashboard-widget-content">
          <ul class="quick-list">
            <li><i class="fa fa-calendar-o"></i><a href="#">Settings</a>
            </li>
            <li><i class="fa fa-bars"></i><a href="#">Subscription</a>
            </li>
            <li><i class="fa fa-bar-chart"></i><a href="#">Auto Renewal</a> </li>
            <li><i class="fa fa-line-chart"></i><a href="#">Achievements</a>
            </li>
            <li><i class="fa fa-bar-chart"></i><a href="#">Auto Renewal</a> </li>
            <li><i class="fa fa-line-chart"></i><a href="#">Achievements</a>
            </li>
            <li><i class="fa fa-area-chart"></i><a href="#">Logout</a>
            </li>
          </ul>

          <div class="sidebar-widget">
            <h4>Profile Completion</h4>
            <canvas width="150" height="80" id="foo" class="" style="width: 160px; height: 100px;"></canvas>
            <div class="goal-wrapper">
              <span class="gauge-value pull-left">$</span>
              <span id="gauge-text" class="gauge-value pull-left">3,200</span>
              <span id="goal-text" class="goal-value pull-right">$5,000</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>-->

</div>


<div class="row">
  


  <div class="col-md-8 col-sm-8 col-xs-12">



    
  </div>
</div>
</div>
<!-- /page content -->

<? include("application/views/template/pie.php"); ?>
<script>
      $(document).ready(function() {
        $(".select2_single").select2({
          placeholder: "Select a state",
          allowClear: true
        });
        $(".select2_group").select2({});
        $(".select2_multiple").select2({
          maximumSelectionLength: 40,
          placeholder: "Seleccione las divisiones",
          allowClear: true
        });
      });
    </script>
<script >
  $(document).ready(function(){
    $('#calcular').click(function(event){
        //alert('a'); 
        array_colores = [];  
        
        array_colores.push(window.chartColors.azul2);
        array_colores.push(window.chartColors.red);
        array_colores.push(window.chartColors.yellow);
        array_colores.push(window.chartColors.blue);
        array_colores.push(window.chartColors.otro);
        array_colores.push(window.chartColors.purple);
        array_colores.push(window.chartColors.teal);
        array_colores.push(window.chartColors.olivo);
        array_colores.push(window.chartColors.marron);
        array_colores.push(window.chartColors.lima);
        array_colores.push(window.chartColors.agua);
        array_colores.push(window.chartColors.orange);
        array_colores.push(window.chartColors.grey);
        array_colores.push(window.chartColors.green);

        array_meses = [];
        array_meses.push('Enero');
        array_meses.push('Febrero');
        array_meses.push('Marzo');
        array_meses.push('Abril');
        array_meses.push('Mayo');
        array_meses.push('Junio');
        array_meses.push('Julio');
        array_meses.push('Agosto');
        array_meses.push('Septiembre');
        array_meses.push('Octubre');
        array_meses.push('Noviembre');
        array_meses.push('Diciembre');
        
        
        $('#grafico_promedios_trimestres').empty();
        $('#grafico_inasistencias').empty();
        
        //ajax promedios 
       // console.log(array_colores);
        $.ajax
          ({
              url: '<?php echo site_url("gestion/gestion/traer_datos_x_trim_aprob_rep"); ?>',
                  
                  type: 'POST',
                  dataType: "JSON",
                  data: $('#cursos').serialize(),
                  beforeSend: function() {
           
                     $('#grafico_promedios_trimestres').append("<img  id='loading_scroll1' src='<?=base_url('vendors/loading.gif');?>' height='50' width='50' />");
                      //$('#loading_scroll').show();
                     
                  },
                  success: function(data)
                  {
                    
                    //contador_de_alumnos
                    //console.log(data);

                    $("#contador_de_alumnos").val(data['contador_alumnos']);
                    
                    document.getElementById("contador_de_alumnos").innerHTML=data['contador_alumnos'];
                    document.getElementById("contador_varones_division").innerHTML=data['contador_varones_division'];
                    document.getElementById("contador_mujeres_division").innerHTML=data['contador_mujeres_division'];

                    var array_grafico = [];
                    for (var i = 0; i < data['array_divisiones'].length; i++) 
                    {
                      //console.log(data['array_divisiones'][i]);
                      
			//	backgroundColor: color(newColor).alpha(0.5).rgbString()
                      array_grafico.push({
                        label: data['array_divisiones'][i]['division_nombre'], 
                        borderColor: array_colores[i], 
                        backgroundColor: array_colores[i], 
                        data: [
                          data['array_divisiones'][i]['aprobados_division_trim1_sin_prt'],
                          data['array_divisiones'][i]['aprobados_division_trim1'], 
                          data['array_divisiones'][i]['aprobados_division_trim2_sin_prt'],
                          data['array_divisiones'][i]['aprobados_division_trim2'], 
                          data['array_divisiones'][i]['aprobados_division_trim3_sin_prt'],
                          data['array_divisiones'][i]['aprobados_division_trim3']
                        ],fill: false})

                    }
                    //console.log(array_grafico);
                    //armar graficos
                    var container = document.querySelector('#grafico_promedios_trimestres');

                    ['average'].forEach(function(position) {
                      var div = document.createElement('div');
                      div.classList.add('chart-container');

                      var canvas = document.createElement('canvas');
                      div.appendChild(canvas);
                      container.appendChild(div);

                      var ctx = canvas.getContext('2d');
                      
                      new Chart(ctx,  
                      {
                        type: 'line',
                        data: {
                          labels: ['Sin PRT','1 Trim','Sin PRT', '2 Trim', 'Sin PRT','3 Trim'],
                          datasets: array_grafico
                        },
                        options: {
                          responsive: true,
                          title: {
                            display: true,
                            text: 'Porcentaje de Alumnos aprobados por Periodo: '
                          },
                          tooltips: {
                            position: position,
                            mode: 'index',
                           
                            intersect: false,
                          },
                        }
                      });
                    });
                    //
                    $('#loading_scroll1').remove();
                  },
                  error: function (jqXHR, textStatus, errorThrown,data)
                  {
                     //alert('error');
                      console.log(jqXHR);
                      console.log(textStatus);
                      console.log(errorThrown);
                  } 
          });
        //ajax asistencias
        $.ajax
          ({
              url: '<?php echo site_url("gestion/gestion/traer_asistencias_x_mes_x_cursos"); ?>',
                  
                  type: 'POST',
                  dataType: "JSON",
                  data: $('#cursos').serialize(),
                  beforeSend: function() {
           
                    $('#grafico_inasistencias').append("<img  id='loading_scroll2' src='<?=base_url('vendors/loading.gif');?>' height='50' width='50' />");
                      //$('#loading_scroll').show();
                    
                  },
                  success: function(data)
                  {
                    //grafico_inasistencias
                    var array_grafico = [];
                    for (var i = 0; i < data['array_divisiones'].length; i++) 
                    {
                      //console.log(data['array_divisiones'][i]);
                      //console.log('------------------------------');
                      //console.log(data['array_divisiones'][i].array_meses);
                      //console.log('------------------------------a');
                      //array_grafico.push({label: data['array_divisiones'][i]['division_nombre'], borderColor: array_colores[i], data: [data['array_divisiones'][i]['aprobados_division_trim1'], data['array_divisiones'][i]['aprobados_division_trim2'], data['array_divisiones'][i]['aprobados_division_trim3']],fill: false})
                      array_meses = [];
                      for (var j = 0; j < data['array_divisiones'][i].array_meses.length; j++)
                      {
                        
                        //console.log(data['array_divisiones'][i]['array_meses'][j]);
                        array_meses.push(data['array_divisiones'][i]['array_meses'][j].acumulador);
                      }
                      array_grafico.push({label: data['array_divisiones'][i]['division_nombre'], borderColor: array_colores[i], data: array_meses,fill: false}) 
                    }  
                    console.log('llego');
                    console.log(array_meses);
                    var container = document.querySelector('#grafico_inasistencias');
                    ['average'].forEach(function(position) {
                      var div = document.createElement('div');
                      div.classList.add('chart-container');

                      var canvas = document.createElement('canvas');
                      div.appendChild(canvas);
                      container.appendChild(div);

                      var ctx = canvas.getContext('2d');
                      
                      new Chart(ctx,  
                      {
                        type: 'line',
                        data: {
                          labels: ['Marzo', 'Abril', 'Mayo', 'Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
                          datasets: array_grafico
                        },
                        options: {
                          responsive: true,
                          title: {
                            display: true,
                            text: 'Inasistencias cuantificadas por Mes: '
                          },
                          tooltips: {
                            position: position,
                            mode: 'index',
                            intersect: false,
                          },
                        }
                      });
                    });
                    $('#loading_scroll2').remove();
                    
                    //console.log(array_grafico);
                    //armar graficos
                    

                  },
                  error: function (jqXHR, textStatus, errorThrown,data)
                  {
                     //alert('error');
                      console.log(jqXHR);
                      console.log(textStatus);
                      console.log(errorThrown);
                  } 
          }); 
                  
    });
  })
               
</script>

    

<script>
		

	/*	window.onload = function() 
    {
      //alert('a');
			var container = document.querySelector('#grafico_promedios_trimestres3');

			['average', 'nearest'].forEach(function(position) {
				var div = document.createElement('div');
				div.classList.add('chart-container');

				var canvas = document.createElement('canvas');
				div.appendChild(canvas);
				container.appendChild(div);

				var ctx = canvas.getContext('2d');
				
				new Chart(ctx,  
        {
          type: 'line',
          data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [{
              label: 'My First dataset',
              borderColor: window.chartColors.red,
              backgroundColor: window.chartColors.red,
              data: [10, 30, 46, 2, 8, 50, 0],
              fill: false,
            }, {
              label: 'My Second dataset',
              borderColor: window.chartColors.blue,
              backgroundColor: window.chartColors.blue,
              data: [7, 49, 46, 13, 25, 30, 22],
              fill: false,
            }]
          },
          options: {
            responsive: true,
            title: {
              display: true,
              text: 'Tooltip Position: ' + position
            },
            tooltips: {
              position: position,
              mode: 'index',
              intersect: false,
            },
          }
			  });
			});
		};*/
	</script>