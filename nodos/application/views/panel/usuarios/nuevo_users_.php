  <?php //echo "hola mundo";?>
<script src="<?=base_url('assets/js/bootstrap.min.js');?>"></script>

<!--<script src="<?//=base_url('assets/js/jquery.validate.min.js')?>"></script> -->

<script type="text/javascript">
  passApiKey = '<?php echo $passApiKey; ?>';
   urlApi = '<?php echo $urlApi; ?>';

   idUser = '<?php echo $idusuario; ?>';
   //idcolegio = '<?php //echo $idcolegio; ?>';
   //idgrupo = '<?php //echo $idgrupo; ?>';
   //nombregrupo = '<?php //echo $nombregrupo; ?>';
   //ordengrupo = '<?php //echo $ordengrupo; ?>';
   var colegios = JSON.parse('<?php echo json_encode($this->session->userdata("colegios")); ?>');


   var arre_activar_user = Array();
   var arre_alta_user = Array();
   var arre_admin_roles = Array();
   var arre_edita_user = Array();
   var arre_listar_user = Array();
   var arre_asigna_tutor = Array();




  if(colegios)
  {
      for(var i=0; i < colegios.length ; i++)
      {
        menu = colegios[i]["menu"];
        if(menu)
          for(var j=0; j < menu.length ; j++)
          {
            //console.log(menu[i]["nombre"]);
            if(menu[j]["nombre"] == "activar_user")
            {
                //colegios a los que puedo activar usuarios
                arre_activar_user.push(colegios[i]["id_colegio"]);
            }
            else
            if(menu[j]["nombre"] == "alta_usuarios")
            {
                //colegios a los que puedo cargar usuarios
                arre_alta_user.push(colegios[i]["id_colegio"]);
            }
            else
            if(menu[j]["nombre"] == "admin_roles")
            {
                //colegios a los que puedo administrar (alta y baja) roles
                arre_admin_roles.push(colegios[i]["id_colegio"]);
            }
            else
            if(menu[j]["nombre"] == "editar_user")
            {
                //colegios a los que puedo administrar (alta y baja) roles
                arre_edita_user.push(colegios[i]["id_colegio"]);
            }
            else
            if(menu[j]["nombre"] == "listado_usuarios")
            {
                //colegios a los que puedo administrar (alta y baja) roles
                arre_listar_user.push(colegios[i]["id_colegio"]);
            }
            else
            if(menu[j]["nombre"] == "agregar_tutor")
            {
                arre_asigna_tutor.push(colegios[i]["id_colegio"]);
            }
           
          }
       
      }

      var arre_alta_user_join = arre_alta_user.join();
      var arre_listar_user_join = arre_listar_user.join();

  }


</script>

<div class="wrapper wrapper-content animated fadeInRight" id="general2">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="ibox collapsed float-e-margins">
                        <div class="ibox-title collapse-link2" id="tituloaltaevento">
                            <h5>Usuarios <small>Detalles/Editar datos de Usuarios</small></h5>
                            <div class="ibox-tools">
                                <a class="">
                                    <i class="fa fa-chevron-down"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content" style="display: block">
                            <div class="row row2">
                                <div class="col-sm-12 b-r " id="contenido2">

                                      <form id="editar" autocomplete="on" method="post" class="form-horizontal form-label-left">

                                        <div id="form_roles_edit">        
                                          <div class="form-group" id ="colegios_edit"></div>
                                          <div class="form-group" id ="roles_col_edit"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-2">
                                            </div>
                                        </div>
                                    </form>

                                    <div class="panel-body table-responsive">
                                        <table id="table_usuarios" class="table table-striped table-bordered dt-responsive ">
                                            <thead>
                                                <tr>
                                                  <th>#</th>
                                                  <th>Apellido</th>
                                                  <th>Nombre</th>
                                                  <th>Documento</th>
                                                  <th>Email</th>
                                                  <th>Ingreso a Nodos</th>
                                                  <th>Activo</th>
                                                  <th width="300">Acciones</th>
                                                </tr>
                                              </thead>
                                              <tbody id="tbody_usuarios"></tbody>

                                        </table>

                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        

</div>
<div class="wrapper wrapper-content animated fadeInRight" id="general" style="display: none">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="ibox collapsed float-e-margins">
                        <div class="ibox-title collapse-link2" id="tituloaltaevento">
                            <h5>NUEVO USUARIO <small> Agregar un nuevo Usuario</small></h5>
                            <div class="ibox-tools">
                                <a class="">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="row row2">
                                <div class="col-sm-12 b-r" id="contenido">
                                    <form id="alta" class="form-horizontal form-label-left">
                                        <input type="hidden" name="id_colegio" value="<?php echo $idcolegio; ?>" />
                                        <input type="hidden" id="id_user_group" name="id_user_group" value="0" />
                                        <input type="hidden" id="user_id" name="user_id" value="0" />
                                        <div class="form-group">
                                          <div class="row">
                                             <label class="col-sm-2 control-label">Documento</label>

                                              <div class="col-sm-8"><input type="text" class="form-control" name="documento_txt" id="documento_txt"></div>
                                          </div>
                                         
                                        </div>
                                        <div id="form_ocultar">
                                          <div class="form-group">
                                            
                                            <label class="col-sm-2 control-label">Nombre</label>

                                            <div class="col-sm-8"><input type="text" class="form-control" name="nombre_txt" id="nombre_txt"></div>
                                        </div>

                                          <div class="form-group">
                                            <label class="col-sm-2 control-label">Apellido</label>

                                            <div class="col-sm-8"><input type="text" class="form-control" name="apellido_txt" id="apellido_txt"></div>
                                          </div>
                                          <div class="form-group">
                                            <label class="col-sm-2 control-label">Fechas de nacimiento</label>

                                            <div class="col-sm-8"><input type="data" id="email_txt" name="domicilio_txt"class="form-control"></div>
                                          </div>
                                           <div class="form-group">
                                            <label class="col-sm-2 control-label">Domicilio</label>

                                            <div class="col-sm-8"><input type="text" id="email_txt" name="domicilio_txt"class="form-control"></div>
                                          </div>
                                          <div class="form-group">
                                            <label class="col-sm-2 control-label">Email</label>

                                            <div class="col-sm-8"><input type="email" id="email_txt" name="email_txt"class="form-control"></div>
                                          </div>
                                          <div class="form-group">
                                            <label class="col-sm-2 control-label">Sexo <br/><small class="text-navy">Seleccione el Sexo</small></label>

                                            <div class="col-sm-8">
                                                  <div class="i-checks"><label> <input type="radio" value="M" name="sexo_txt" id="radio_masculino">  <i></i> Masculino </label></div>
                                                  <div class="i-checks"><label> <input type="radio" checked="" value="F" name="sexo_txt" id="radio_femenino"> <i></i> Femenino </label></div>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <label class="col-sm-2 control-label">Tipo de licencia</label>

                                            <div class="col-sm-8"><input type="text" id="email_txt" name="domicilio_txt"class="form-control"></div>
                                          </div> 
                                          <div class="form-group">
                                            <label class="col-sm-2 control-label">Rama</label>

                                            <div class="col-sm-8"><input type="text" id="email_txt" name="domicilio_txt"class="form-control"></div>
                                          </div>
                                          <div class="form-group">
                                            <label class="col-sm-2 control-label">Club</label>

                                            <div class="col-sm-8"><input type="text" id="email_txt" name="domicilio_txt"class="form-control"></div>
                                          </div>
                                          <div class="form-group">
                                            <label class="col-sm-2 control-label">Función</label>

                                            <div class="col-sm-8"><input type="text" id="email_txt" name="domicilio_txt"class="form-control"></div>
                                          </div>
                                  </div>
                                  <br>
                                      <br>
                                  <div id="roles_actuales">
                                    <div class="form-group">
                                      <div class="col-sm-10">
                                      <CENTER><label>ROLES ASIGNADOS</label></CENTER>
                                      
                                      <br>
                                      <CENTER>
                                          <table id="table" class="table table-striped table-bordered dt-responsive ">
                                            <thead>
                                                <tr>
                                                  <th>Colegio</th>
                                                  <th>Rol</th>
                                                  <th>Acciones</th>
                                                </tr>
                                              </thead>
                                              <tbody id="tbody_roles">

                                              </tbody>

                                              
                                            
                                          </table>
                                      </CENTER>
                                      </div>
                                    </div>
                                  </div>
                                  <div id="form_roles">        
                                          <div class="form-group" id ="colegios">
                                            
                                          </div>
                                          <div class="form-group" id ="roles_col">
                                            
                                          </div>
                                        </div>
                                        
                                        
                                  </div>
                                       
                                        <div class="form-group">
                                            <div class="col-sm-4 col-sm-offset-2">
                                                <!--<button class="btn btn-white" id ="cancelar">Cancelar</button>-->
                                                <button class="btn btn-primary" id ="guardar" >Guardar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
              
              
            </div>
            
</div>


<!-- modal editar usuario-->
<div class="modal fade" id="modal_editar_usuario" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Editar Usuario</h4>
      </div>


      <div class="modal-body  ">
        <form id="form_edit_users" autocomplete="on" method="post" class="">
          <input type="hidden" id="user_id_edit" name="user_id_edit" value="0" />
<!--
      <form id="form_edit_users" autocomplete="on" method="post" class="form-horizontal form-label-left">
        <input type="hidden" id="user_id_edit" name="user_id_edit" value="0" />
        <input type="hidden" id="group_id_user_edit" name="group_id_user_edit" value="0" />
        <input type="hidden" id="id_user_group_edit" name="id_user_group_edit" value="0" />
        <div class="modal-body">
 -->
        
          <div class="row">
        
            <input type="hidden" id="user_id" name="user_id" value="0" />
            
            <div class="col-sm-6"> <!--div izquierda-->

              <div class="form-group">
                 <label class="">Documento</label>
                  <input type="text" class="form-control" name="documento_txt_edit" id="documento_txt_edit">
              </div>

              <div class="form-group">
                <label class="">Nombre</label>
                  <input type="text" class="form-control" name="nombre_txt_edit" id="nombre_txt_edit">
              </div>

              <div class="form-group" >
                <label class="">Fecha de Creaci&oacute;n</label>
                <input type="text" class="form-control" name="fecha_alta_usu" id="fecha_alta_usu" readonly>
              </div>

              <div class="form-group">
                <label class="">Sexo</label>
                <div class="col-sm-12">
                  <div class="i-checks"><label> <input type="radio" value="M" name="sexo_txt_edit" id="radio_masculino_edit">  <i></i> Masculino </label></div>
                  <div class="i-checks"><label> <input type="radio" checked="" value="F" name="sexo_txt_edit" id="radio_femenino_edit"> <i></i> Femenino </label></div>
                </div>
              </div>

            </div>

            <div class="col-sm-6"> <!--div derecha-->
              
              <div class="form-group">
                <label class="">Email</label>
                <input type="email" id="email_txt_edit" name="email_txt_edit" class="form-control">
              </div>

              <div class="form-group">
                <label class="">Apellido</label>
                  <input type="text" class="form-control" name="apellido_txt_edit" id="apellido_txt_edit" >
              </div>

              <div class="form-group" >
                <label class="">Fecha del &uacute;ltimo Acceso</label>
                <input type="text" class="form-control" name="lastlogin_usu" id="lastlogin_usu" readonly>
              </div>

              <div id="div_activar" class="form-group" style="display: none">
                  <label class="" style="color:red">Activar/Desactivar</label>
                  <input type="checkbox" name="activo_usu" id="activo_usu" data-toggle="tooltip" title="Si tilda esta casilla el usuario será activado, de lo contrario, será desactivado. Un usuario desactivado no podrá ingresar al sitio de Nodos."/>  
                 
              </div>

              <div id="div_pago" class="form-group " style="display: none">
                <label class="" style="">Pag&oacute;/No Pag&oacute;</label>
                <input type="checkbox" name="pago_usu" id="pago_usu" data-toggle="tooltip" title="Tilde esta casilla para indicar que el alumno pagó, de lo contrario, destilde esta casilla. Si el alumno no pagó, sus tutores no podrán visualizar el contenido referido al mismo."/> 
              </div>

            </div>

            <br><br>
            <div id="roles_actuales2">
              <div class="form-group">
                <div class="col-sm-10">
                <CENTER><label>ROLES ASIGNADOS</label></CENTER>
                
                <br>
                <CENTER>
                    <table id="table2" class="table table-striped table-bordered dt-responsive ">
                      <thead>
                          <tr>
                            <th>Colegio</th>
                            <th>Rol</th>
                            <th>Acciones</th>
                          </tr>
                        </thead>
                        <tbody id="tbody_roles_edit">

                        </tbody>
                    </table>
                </CENTER>
                </div>
              </div>
            </div>
            
            
          </div>

        

      </div>
        
      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button class="btn btn-primary" id ="update_usuario" >Guardar</button>
      </div>

      </form>  <!-- form debe cerrar despues del botton que hace submit, porque sino el validate plugin falla -->
    </div>
  </div>
</div>
</div>

<!-- modal REsetear clave usuario-->
<div class="modal fade" id="modal_reset_clave" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Resetear Clave de Usuario</h4>
      </div>
      <center>
      <form id="form_reset" autocomplete="on" method="post" class="form-horizontal form-label-left">
        <input type="hidden" id="user_id_edit" name="user_id_edit" value="0" />
        <div class="modal-body">
        
           <input type="hidden" id="user_id_reset" name="user_id_reset" value="0" />
         
          
          
          <div class="form-group">
            <h3 class="col-sm-8 control-label">Seleccione la Clave Inicial</h3>

           
          </div>
          <div class="form-group">
           
            <center>
            <div class="col-sm-8 control-label">
                  <div class="i-checks"><label>DNI (La clave Inicial será el DNI)  <input type="radio" value="dni" name="clave_txt" id="radio_dni">  <i></i>  </label></div>
                  <div class="i-checks"><label>EMAIL (La clave Inicial será la primer parte del Email antes del @) <input type="radio" checked="" value="email" name="clave_txt" id="radio_email"> <i></i>  </label></div>
            </div>
            </center>
          </div>
              
          
        
      </div>
        <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                
                 <button class="btn btn-primary" id ="reset_clave_usuario" >Resetear Clave</button>
      </form>
      </center>
      </div>
    </div>
  </div>
</div>
</div>

<!-- modal hijos de tutores-->
<div class="modal fade" id="modal_hijos" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hijos</h4>
      </div>
      <center>
      <form id="form_hijos" autocomplete="on" method="post" class="form-horizontal form-label-left">
        <input type="hidden" id="user_id_edit" name="user_id_edit" value="0" />
        <div class="modal-body">
        
           <input type="hidden" id="user_id_tutor" name="user_id_tutor" value="0" />
           <input type="hidden" id="id_colegio_modal_hijos" name="id_colegio_modal_hijos" value="0" />
          <table class="table table-striped table-bordered dt-responsive nowrap" id="">
            <thead>
              <th>Apellido</th>
                 <th>Nombre</th>
                 <th>Curso</th>
            </thead>
            <tbody id="table_hijos">
                                                          
            </tbody>
          </table>
          
          
          <div class="form-group">
            
          </div>
              
          
        
      </div>
        <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                
                
      </form>
      </center>
      </div>
    </div>
  </div>
</div>
</div>

<!-- modal tutores de los alumnos-->
<div class="modal fade" id="modal_tutores" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Tutores</h4>
      </div>
      <center>
      <form id="form_tutores" autocomplete="on" method="post" class="form-horizontal form-label-left">
        <input type="hidden" id="user_id_edit" name="user_id_edit" value="0" />
        <div class="modal-body">
        
           <input type="hidden" id="user_id_hijo" name="user_id_hijo" value="0" />
           <input type="hidden" id="id_colegio_modal_tutores" name="id_colegio_modal_tutores" value="0" />
          <table class="table table-striped table-bordered dt-responsive nowrap" id="tabla_tutores">
            <thead>
              <th>Apellido</th>
                 <th>Nombre</th>
                 <th>DNI</th>
                 <th>E-mail</th>
                 <th></th>
            </thead>
            <tbody id="table_tutores">
                                                          
            </tbody>
          </table>
          
          
          <div id="div_agregar_tutores" style="display: none;">  

            <div class="form-group">
                <label class="col-sm-2 control-label">Seleccione el Tutor</label>
                <div class="col-sm-8">
                    <select class="form-control input-md" id="select_agregar_tutores" name="select_agregar_tutores">
                    </select>
                </div>
            </div>
            <div class="form-group">
              <center><a class="btn btn-primary" id ="guardar_registrar_asignacion" onclick="registrar_asignacion(this)">Asignar Tutor</a></center>
            </div>

          </div>
              
        
      </div>
        <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                
                
      </form>
      </center>
      </div>
    </div>
  </div>
</div>
</div>


<!-- Modal -->
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body">
                  <p id="mensaje"></p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
              </div>
            </div>
          </div>
<script type="text/javascript">
  $('#form_ocultar').hide();
  $('#form_roles').hide();
  $('#roles_actuales').hide();
  $('#div_agregar_tutores').hide();

</script>
<script type="text/javascript">

  function traer_tutores(id_colegio)
  {
    
    var aux = arre_asigna_tutor.indexOf(id_colegio);
    if(aux != -1){
        $('#div_agregar_tutores').show();
        var urlRequest = urlApi+'/usuario/tutores_colegio/'; 
    
        $.ajax({
            url: urlRequest,
              type: 'GET',
              headers: {              
               'APIKEY' : passApiKey,
               'userid' : idUser,
               'Access-Control-Allow-Origin': '*'
            },
            data :{id_colegio:id_colegio},
            success : function(data) 
            { 
              //console.log(data);
              options = '<option value="0">Seleccione...</option>';
              tutores = data.tutores;
              //alert(tutores.length);
              for(var i=0; i < tutores.length ; i++) //aca solo listo niveles y especialidades
              {        
                  documento = tutores[i]["documento"];
                  if(documento == null)
                    documento = '';
                  
                  options = options + '<option value="'+tutores[i]["user_id"]+'"  >'+tutores[i]["apellido"]+' '+tutores[i]["nombre"]+' DNI: '+documento+'</option>';
              }

              $('#select_agregar_tutores option').remove();

              $('#select_agregar_tutores').append(options);
            
              $('#select_agregar_tutores').select2({
                   theme: "bootstrap",
                   placeholder: "Seleccione Tutor",
                  allowClear: true
              }); 

            },
            error: function(response)
            {
                //hacer algo cuando ocurra un error
                console.log(response);
                //alert('error');

            }
        }); 
    }
    else $('#div_agregar_tutores').hide();
  }


  function registrar_asignacion(elem) //alumno id
  {
      //alert('joya');
      var user_id = $('#user_id_hijo').val();
      var id_tutor = $('#select_agregar_tutores').val();

      var urlRequest = urlApi+'/usuario/asignar_tutor_alumno/'; 
      //alert(id_tutor);

      if(id_tutor!=0)
        $.ajax({
            url: urlRequest,
              type: 'GET',
              headers: {              
               'APIKEY' : passApiKey,
               'userid' : idUser,
               'Access-Control-Allow-Origin': '*'
            },
            data :{user_id:user_id, id_tutor:id_tutor},
            beforeSend: function() 
            { 
                $(elem).hide();
                $(elem).after("<img  id='loading_asignatutor' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");
            },
            success : function(data) 
            { 
              $(elem).show();
              //console.log(data);

              if(data.status){

                  tutor = data.users;
                  fila_tutor = '<tr id="fila_tutor_'+tutor["id"]+'_alumno_'+user_id+'">'+
                                    '<td>'+tutor["last_name"]+'</td>'+
                                    '<td>'+tutor["first_name"]+'</td>'+
                                    '<td>'+tutor["documento"]+'</td>'+
                                    '<td>'+tutor["email"]+'</td>'+
                                    '<td><button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="btn_alumnos" onclick="eliminar_tutor_alumno(this,'+user_id+','+tutor["id"]+')" type="button"><i class="fa fa-trash-o"><i></button></td>'+
                                '</tr>';
                  $("#tabla_tutores tbody").append(fila_tutor); 
                  $('#select_agregar_tutores').val(0);
              }

              //id="estado_tutor_'+user_id+'"
              //$("#estado_tutor_"+user_id).removeClass("fa fa-times");
              //$("#estado_tutor_"+user_id).removeClass("fa fa-check");
              //$("#estado_tutor_"+user_id).addClass("fa fa-check");                   
              
              $('#loading_asignatutor').remove();              

            },
            error: function(response)
            {
                //hacer algo cuando ocurra un error
                console.log(response);
                //alert('error');

            }
        }); 
      //else alert(id_tutor);

  }

  function eliminar_tutor_alumno(elem, id_user, id_tutor)
  {
    //alert(id_user);
    //alert(id_tutor);
    bootbox.confirm("Desasignar Tutor?", function(result){ 
      if(result){

          var urlRequest = urlApi+'/usuario/eliminar_tutor_alumno/'; 
      
          $.ajax({
              url: urlRequest,
                type: 'GET',
                headers: {              
                 'APIKEY' : passApiKey,
                 'userid' : idUser,
                 'Access-Control-Allow-Origin': '*'
              },
              data :{id_alumno:id_user, id_tutor:id_tutor},
              beforeSend: function() 
              { 
                  $(elem).hide();
                  $(elem).after("<img  id='loading_deleteasignatutor' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");
              },
              success : function(data) 
              { 
                //console.log(data);
                
                if(data.status == true)
                  $(elem).parent('td').parent('tr').remove();
                else {
                  $(elem).show();
                  $('#loading_deleteasignatutor').remove();
                }

                /*$("#modal_eliminar_tutor_"+id_tutor+"_alumno_"+id_user).modal('hide');
                if(data.cont_tutores == false)
                {
                  $("#estado_tutor_"+id_user).removeClass("fa fa-times");
                  $("#estado_tutor_"+id_user).removeClass("fa fa-check");
                  $("#estado_tutor_"+id_user).addClass("fa fa-times");
                }*/
                //id="fila_tutor_'+tutor["id"]+'_alumno_'+user_id+'"

              },
              error: function(response)
              {
                  //hacer algo cuando ocurra un error
                  console.log(response);
                  //alert('error');
              }
          }); 

      }
    });
  }


  function delete_usergroup(idusergroup)
  {
      var urlRequest = urlApi+"/usuario/delete_usergroup/"; 

      bootbox.confirm({ 
        size: "small",
        message: "Dar de baja el rol?", 
        callback: function(result){ 
          if(result)
                $.ajax({
                  url: urlRequest,
                    type: 'POST',
                    headers: {              
                     'APIKEY' : passApiKey,
                     'userid' : idUser,
                     'Access-Control-Allow-Origin': '*'
                  },
                  data: {idusergroup: idusergroup},
                  success : function(data) 
                  { 
                    if(data.status)
                    {
                      $.notify(data.message, "success");
                      $('#user_group_'+idusergroup).remove();
                    }
                  },
                  error: function(response)
                  {
                      //hacer algo cuando ocurra un error
                      console.log(response);
                  }
              }); 
        }
      })
     
  }

  function buscar_users(dni, obj)
  { //console.log(arre_alta_user_join);
      var urlRequest = urlApi+"/usuario/verificar_users/"; 
      $.ajax({
              url: urlRequest,
                type: 'POST',
                headers: {              
                 'APIKEY' : passApiKey,
                 'userid' : idUser,
                 'Access-Control-Allow-Origin': '*'
              },
              data: {documento_txt: dni, colegiosId: arre_alta_user_join},
              beforeSend: function() 
              {
                $(obj).after("<img  id='loading_buscar_user' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");
              },
              success : function(data) 
              {   
                
                if(data.existe == true)
                {   
                    //console.log(data);
                    $('#nombre_txt').val(data.users['first_name']);
                    $('#apellido_txt').val(data.users['last_name']);
                    $('#email_txt').val(data.users['email']);
                   
                    $('#id_user_group').val(data.users['email']);
                    $('#user_id').val(data.users['id']);
                   
                    if(data.users['sexo'] == 'F')
                    {
                      $("#radio_femenino").prop('checked', true);
                    }
                    else
                    {
                      $("#radio_masculino").prop('checked', true);
                    }

                    $('#apellido_txt').attr('disabled', true);
                    $('#email_txt').attr('disabled', true);
                    $('#nombre_txt').attr('disabled', true);
                    $('#radio_femenino').attr('disabled', true);
                    $('#radio_masculino').attr('disabled', true);


                    //console.log('users groups: ');
                    $('#tbody_roles tr').remove();
                    for(var i=0; i < data.users_group.length ; i++)
                    {
                      /*estado = data.users_group[i].estado;
                      if(estado == 1)
                      {
                        estado = '<i class="fa fa-check"> </i>';
                      }
                      else
                      {
                        estado = '<i class="fa fa-times"> </i>';
                      }*/
                      
                      var aux = arre_admin_roles.indexOf(data.users_group[i].colegio_id);
                      if(aux != -1)
                        var boton = '<button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="btn_alumnos" title="borrar" onclick="delete_usergroup('+data.users_group[i].id+')" type="button"><i class="glyphicon glyphicon-trash"></i></button>';
                      else var boton = '';

                      fila = '<tr id="user_group_'+data.users_group[i].id+'"><td>'+data.users_group[i].colegio+'</td><td>'+data.users_group[i].grupo+'</td><td>'+boton+'</td></tr>';
                      $('#tbody_roles').append(fila);
                     
                    }

                    $('#form_ocultar').show();
                    $('#form_roles').show();
                    $('#roles_actuales').show();
                }
                else
                {
                  //crear_select_colegios_alta('alta_usuarios');
                  $('#form_roles').show();
                  $('#form_ocultar').show();
                  $('#roles_actuales').hide();
                  $('#user_id').val('0');

                  $('#apellido_txt').attr('disabled', false);
                  $('#email_txt').attr('disabled', false);
                  $('#nombre_txt').attr('disabled', false);
                  $('#radio_femenino').attr('disabled', false);
                  $('#radio_masculino').attr('disabled', false);
                }
                
                $('#select_colegio_alta').val('0');
                $('#select_rol').val(0);
                $('#loading_buscar_user').remove();


              },
              error: function(response)
              {
                  //hacer algo cuando ocurra un error
                  console.log(response);
                  $('#select_colegio_alta').val('0');
                  $('#select_rol').val(0);

                  $('#loading_buscar_user').remove();
              }
      }); 
  }

$(document).ready(function()
{
  $('[data-toggle="tooltip"]').tooltip();

  $(document).keyup(function(e) { 
      if(e.which == 27) {
          $("#modal_editar_usuario").modal('hide');
          $("#modal_reset_clave").modal('hide');
      }
  });

  $('#guardar').attr('disabled', true);

  $("#documento_txt").on("keyup", function(e)
  { 
    e.preventDefault();
    
    //if(e.which == 13) {
      $('#tbody_roles').empty();
      //limpiar form
      $('#nombre_txt').val('');
      $('#apellido_txt').val('');
      $('#email_txt').val('');
      $('#user_id').val('0');

      $('#form_roles').hide();

      $('#guardar').attr('disabled', false);
      
      if($('#documento_txt').val() != null)
        buscar_users($('#documento_txt').val(), this);
    /*}
    else 
    {
      $('#guardar').attr('disabled', true);
      $(this).notify("Escriba el numero de documento y presione ENTER para verificar existencia", {className:"info", autoHide: true});
      $(this).find('.notifyjs-wrapper').css('display','none');
    }*/
      
  });

})
</script>

<script type="text/javascript">
crear_select_colegios('listado_usuarios');
function crear_select_colegios(accion)
{   
      //verificar si puede editar datos de usuarios. LISTADO DE USUARIOS!!
      var cont_col = 0;
      var edit_users = [];
      //var colegios = JSON.parse('<?php echo json_encode($this->session->userdata("colegios")); ?>');
      for(var i=0; i < colegios.length ; i++)
      {
        var bandera = false;                 
        menu = colegios[i]["menu"];
        //console.log('menu');
        //console.log(colegios[i]);
        for(var j=0; j < menu.length ; j++)
        {
                            //console.log('menu: '+menu[j]["nombre"]);
          if((menu[j]["nombre"] == accion))
          {
            cont_col = cont_col + 1;
            bandera = true;
            //alert('entro');
          }
          
        }
        if(bandera == true)
        {
          edit_users[i] = 1; //1 SI PUEDE
          //alert('entro');
        } 
        else
        {
          edit_users[i] = 0; //1 SI PUEDE
        }
                         
      }
      //console.log('el arreglo de colegios quedo asi:');
     

      if( cont_col > 1)
      {
        options = '<option value="0" >Seleccione...</option>';
        for(var i=0; i < colegios.length ; i++)
        {
          if(edit_users[i] == 1)
          {
             options = options + '<option value="'+colegios[i]["id_colegio"]+'"  >'+colegios[i]["nombre_colegio"]+'</option>';
          }
        }
        select_edit = '<label class="col-sm-2 control-label"  >Colegio:</label><div class="col-sm-8">'+
        '<select class="form-control input-md" id="select_colegio_edit" name="select_colegio_edit">'+options+      
        '</select></div>';
        $("#colegios_edit").append(select_edit);
        $('#select_colegio_edit').select2({
           theme: "bootstrap",
           placeholder: "Seleccione Colegio",
          allowClear: true
        }); 
         //alert('entro');
       
      }
      if(cont_col == 1)
      { 

        //alert('entro');
        //console.log('1 solo colegio: ');
        //alert('entro');
       // console.log(colegios[0]["id_colegio"]);
        //var id_colegio = colegios[0]["id_colegio"];
        //traer_roles_colegio_activos(id_colegio,"editar");

        for(var i=0; i < colegios.length ; i++)
        {
         
          if(edit_users[i] == 1)
          {
            var id_colegio = colegios[i]["id_colegio"];
            var nombre_colegio = colegios[i]["nombre_colegio"];
            
            //console.log('1 solo colegio: '+id_colegio);
            cole = '<div class="form-group">'+
                      '<label class="col-sm-2 control-label">Colegio:</label>'+
                      '<div class="col-sm-8"><input id="" name="" class="form-control" value="'+nombre_colegio+'" disabled> <input id="colegioUnico" type="hidden" value="'+id_colegio+'"> </div>'+
                    '</div>';

            $("#colegios_edit").append(cole);

            traer_roles_colegio_activos(id_colegio,"editar");
          }
           
        }
      }
            
}

crear_select_colegios_alta('alta_usuarios');
function crear_select_colegios_alta(accion)
{     
      //$('#select_colegio_alta').parent('div').remove();

      //verificar si puede dar de alta usuarios
      var cont_col = 0;
      var alta_users = [];
      var colegios = JSON.parse('<?php echo json_encode($this->session->userdata("colegios")); ?>');
      for(var i=0; i < colegios.length ; i++)
      {
        var bandera = false;                 
        menu = colegios[i]["menu"];
        //console.log('menu');
        //console.log(colegios[i]);
        for(var j=0; j < menu.length ; j++)
        {
                            //console.log('menu: '+menu[j]["nombre"]);
          if((menu[j]["nombre"] == accion))
          {
            cont_col = cont_col + 1;
            bandera = true;
            //alert('entro');
          }
          
        }
        if(bandera == true)
        {
          alta_users[i] = 1; //1 SI PUEDE
          //alert('entro');
        } 
        else
        {
          alta_users[i] = 0; //1 SI PUEDE
        }
                         
      }
      //console.log('el arreglo de colegios quedo asi:');
      //console.log(edit_users);
      if( cont_col > 0) //puede dar alta, muestro formulario
          $('#general').css('display','block');


      if( cont_col > 1 )
      {
       
        options = '<option value="0" >Seleccione...</option>';
       
        for(var i=0; i < colegios.length ; i++)
        {
         
          if(alta_users[i] == 1)
          {
             options = options + '<option value="'+colegios[i]["id_colegio"]+'"  >'+colegios[i]["nombre_colegio"]+'</option>';
          }
         
           //options = options + '<option value="'+colegios[i]["id_colegio"]+'">'+colegios[i]["nombre_colegio"]+'</option>';
        }
        select = '<label class="col-sm-2 control-label"  >Colegio:</label><div class="col-sm-8">'+
        '<select class="form-control input-md" id="select_colegio_alta" name="select_colegio">'+options+      
        '</select></div>';
        $("#colegios").append(select);
         $('#select_colegio_alta').select2({
                     theme: "bootstrap",
                     placeholder: "Seleccione Colegio",
                    allowClear: true
                  }); 
       
      }
      else

      if(cont_col == 1)
      { 
        //alert('entro');
        for(var i=0; i < colegios.length ; i++)
        {
          if(alta_users[i] == 1)
          {
            var id_colegio = colegios[i]["id_colegio"];
            var nombre_colegio = colegios[i]["nombre_colegio"];

            
            //console.log('1 solo colegio: '+id_colegio);
            cole = '<div class="form-group">'+
                      '<label class="col-sm-2 control-label">Colegio: </label>'+
                      '<div class="col-sm-8"><input id="" name="" class="form-control" value="'+nombre_colegio+'" disabled></div>'+
                    '</div>';

            $("#colegios").append(cole);
            traer_roles_colegio_activos(id_colegio,"alta");
            //$('#general').css('display','block');

          }
        }
        
      }
      
}

  function traer_roles_colegio_activos(idcolegio,accion)
  {

    /*if(accion == 'alta')
      var urlRequest = urlApi+"/usuario/obtener_roles_colegio_menorOrden"; 
    else 
      var urlRequest = urlApi+"/usuario/obtener_roles_colegio"; */

     var urlRequest = urlApi+"/usuario/obtener_roles_colegio_menorOrden"; 
    
        $.ajax({
                url: urlRequest,
                  type: 'GET',
                  headers: {              
                   'APIKEY' : passApiKey,
                   'userid' : idUser,
                   'Access-Control-Allow-Origin': '*'
                },
                data:{idcolegio: idcolegio},
                
                success : function(data) 
                {   
                    
                  console.log(data);
                  if(data.status)
                  {  
                    data= data.data;
                    options = '<option value="0" >Seleccione...</option>';
                    options_edit = '<option value="0" >Seleccione...</option>';
                    /*for(var i=0; i < colegios.length ; i++)
                    {
                     
                      
                      options = options + '<option value="'+colegios[i]["id_colegio"]+'"  >'+colegios[i]["nombre_colegio"]+'</option>';
                       
                       //options = options + '<option value="'+colegios[i]["id_colegio"]+'">'+colegios[i]["nombre_colegio"]+'</option>';
                    }*/
                    
                    for(var i=0; i < data.length ; i++) //aca solo listo niveles y especialidades
                    { 
                      
                      if(accion == 'alta')
                      {
                        /*if((data[i]["name"] != 'Nodos') && (data[i]["name"] != 'Directivo') && (data[i]["name"] != 'Representante Legal') && (data[i]["name"] != 'Institucion') && (data[i]["name"] != 'Alumno'))
                        {
                          options = options + '<option value="'+data[i]["id"]+'"  >'+data[i]["name"]+'</option>';
                          //alert('llego al alta');
                        }*/
                        options = options + '<option value="'+data[i]["id"]+'"  >'+data[i]["name"]+'</option>';
                      }
                      if(accion == 'editar')
                      {
                        
                        /*if((data[i]["name"] != 'Nodos') && (data[i]["name"] != 'Directivo') && (data[i]["name"] != 'Representante Legal') && (data[i]["name"] != 'Institucion'))
                        {
                          options_edit = options_edit + '<option value="'+data[i]["id"]+'"  >'+data[i]["name"]+'</option>';
                        }*/
                          options_edit = options_edit + '<option value="'+data[i]["id"]+'"  >'+data[i]["name"]+'</option>';
                      }
                        

                    }


                    if(accion == 'alta')
                    {
                      //$('#form_roles').show();
                      var select='';
                      $("#roles_col").empty();
                      select = '<label class="col-sm-2 control-label" >Rol:</label><div class="col-sm-8">'+
                      '<select class="form-control input-md" id="select_rol" name="select_rol">'+options+      
                      '</select><input type="hidden" name="id_colegio_sel" value='+idcolegio+'></div>';
                      $("#roles_col").append(select);
                      $('#select_rol').select2({
                       theme: "bootstrap",
                       placeholder: "Seleccione el Rol",
                      allowClear: true
                      });
                      //console.log(select);
                     // alert(accion);
                      //alert('llego al alta');
                    }
                    else if(accion == 'editar')
                    {
                      $("#roles_col_edit").empty();
                      select_edit = '<label class="col-sm-2 control-label"  >Rol:</label><div class="col-sm-8">'+
                      '<select class="form-control input-md" id="select_rol_edit" name="select_rol_edit">'+options_edit+      
                      '</select><input type="hidden" name="id_colegio_sel" value='+idcolegio+'></div>';
                      $("#roles_col_edit").append(select_edit);
                      $('#select_rol_edit').select2({
                       theme: "bootstrap",
                       placeholder: "Seleccione el Rol",
                      allowClear: true
                      });
                    }

                  }
                  else
                  {
                    $("#roles_col_edit").empty();
                  }
                },
                error: function(response)
                {
                    //hacer algo cuando ocurra un error
                    console.log(response);
                }
        }); 
  }

  obtener_datos();
  function obtener_datos()
  {
    var urlRequest = urlApi+"/usuario/get_rolex_colegios_all_users/"; 
    $.ajax({
            url: urlRequest,
              type: 'GET',
              headers: {              
               'APIKEY' : passApiKey,
               'userid' : idUser,
               'Access-Control-Allow-Origin': '*'
            },
            data :{},
            
            success : function(data) 
            {   
              //console.log(data.roles_agrupados);
              roles_agrupados = JSON.parse(data.roles_agrupados);
              roles = JSON.parse(data.roles);
              //console.log('Los roles Asignados que tengo son:');
              //console.log(roles);
             /* contador
              for(var i=0; i < roles.length ; i++)
              {

              }*/

              

            },
            error: function(response)
            {
                //hacer algo cuando ocurra un error
                console.log(response);

            }
    }); 
  }

</script>
<script type="text/javascript">
    $(document).ready(function(){
      
      $("#select_colegio_alta").change(function(){
            //traer_roles_colegio($('select[id=select_colegio]').val());         
            if($(this).val() > 0)
              traer_roles_colegio_activos($(this).val(),'alta');
            else $('#select_rol').parent('div').remove();
      });

      $("#select_colegio_edit").change(function(){
            $('#roles_col_edit').empty();

            $("#table_usuarios").dataTable().fnDestroy();
            $("#tbody_usuarios").empty();
            //traer_roles_colegio_edit($('select[id=select_colegio_edit]').val());
            if($(this).val() > 0){
              traer_roles_colegio_activos($(this).val(),'editar');
            }
      });

      $("#roles_col_edit").change(function(){
            $("#table_usuarios").dataTable().fnDestroy();
            $("#tbody_usuarios").empty();
        
            var id = $('select[id=select_rol_edit]').val();
            if( id > 0 ){
              traer_usuarios_rol_select(id);

              var texto = $("#select_rol_edit option:selected").text();
              if(texto=="Alumno"){
                var idcole = $('[name="id_colegio_sel"]').val();
                traer_tutores(idcole);
              }
              
            }
      });
      
    });
</script>
<script>
  function traer_usuarios_rol_select(id_rol)
  {   
      if( $('#select_colegio_edit').length )
        var colegio = $('#select_colegio_edit').val();
      else var colegio = $('#colegioUnico').val() ;


      var urlRequest = urlApi+"/usuario/obtener_usuarios_rol/"; 
        $.ajax({
                url: urlRequest,
                  type: 'GET',
                  headers: {              
                   'APIKEY' : passApiKey,
                   'userid' : idUser,
                   'Access-Control-Allow-Origin': '*'
                },
                data :{id_rol : id_rol},
                beforeSend: function() 
                {
                  $('#table_usuarios').after("<img  id='loading_scroll_users' src='<?=base_url('assets/images/loading.gif');?>' height='30' width='30' />");
                //$('#loading_scroll').show();
                },
                success : function(data) 
                {   
                    //console.log(data);

                    var ban_edita = arre_edita_user.indexOf(colegio); //busco si en el colegio seleccionado puedo editar usuarios          


                    users = data.users;
                    var cont = 1;
                    var rol_seleccionado = data.rol;

                    for(var i=0; i < users.length ; i++) //aca solo listo niveles y especialidades
                    {
                      //var id_evento = datos[i]['id'];
                      //console.log(users[i]['last_name']);
                      //var prueba = 'datos[i]['last_name']'+'+datos[i]['titulo']';
                      

                      var estado_users = users[i]['active'];
                      var estado = '';
                      var user_id = users[i]['id'];
                      var ingreso_nodos = users[i]['iteration'];
                      var estado_ingreso ='';

                      var ver_hijos = '';
                      var id_colegio = users[i]['colegio_id'];
                      var group_id = users[i]['group_id'];
                      var id_users_groups = users[i]['id_users_groups'];

                      if(rol_seleccionado == 'Tutor')
                      {
                         ver_hijos = '<button class="btn btn-sm btn-info pull-left" id="btn_alumnos" title="Ver Hijos" onclick="abrir_modal_hijos('+user_id+','+id_colegio+',this)" type="button"><i class="glyphicon glyphicon-user"></i></button>';
                      }
                      else if(rol_seleccionado == 'Alumno')
                      {
                         ver_hijos = '<button class="btn btn-sm btn-info pull-left" id="btn_alumnos" title="Ver Tutores" onclick="abrir_modal_tutores('+user_id+','+id_colegio+',this)" type="button"><i class="glyphicon glyphicon-user"></i></button>';
                      }

                      if(estado_users == 1)
                      {
                        estado = '<i id="estado_usu_'+user_id+'" class="fa fa-check"> </i>';
                      }
                      else
                      {
                        estado = '<i id="estado_usu_'+user_id+'" class="fa fa-times"> </i>';
                      }


                      if(ingreso_nodos == 1)
                      {
                        estado_ingreso = '<i id="estado_ingreso'+user_id+'" class="fa fa-check"> </i>';
                      }
                      else
                      {
                        estado_ingreso = '<i id="estado_ingreso'+user_id+'" class="fa fa-times"> </i>';
                      }


                      if(ban_edita != -1)
                      {
                        botonEditar = '<button class="btn btn-sm btn-primary pull-left" id="btn_alumnos" title="Editar" onclick="abrir_modal_editar_usuario('+user_id+',this)" type="button"><i class="glyphicon glyphicon-pencil"></i></button> ';

                        botonResetclave = '<button class="btn btn-sm btn-primary pull-left " id="btn_alumnos" title="Resetear Clave" onclick="abrir_modal_resetear_clave('+user_id+')" type="button"><i class="glyphicon glyphicon-retweet"></i></button>';
                      }
                      else
                      {
                        botonEditar = '<button class="btn btn-sm btn-primary pull-left" id="btn_alumnos" title="Detalles" onclick="abrir_modal_editar_usuario('+user_id+',this)" type="button"><i class="glyphicon glyphicon-search"></i></button> ';

                        /*botonResetclave = '<button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="btn_alumnos" title="Resetear Clave" onclick="abrir_modal_resetear_clave('+user_id+')" type="button" disabled><i class="glyphicon glyphicon-retweet"></i></button>';*/
                        botonResetclave = '';
                      }

                      var fila = '<tr id="fila_usuario_'+user_id+'">'+
                                    '<td>'+cont+'</td>'+
                                    '<td>'+users[i]['last_name']+'</td>'+
                                    '<td>'+users[i]['first_name']+'</td>'+
                                    '<td>'+users[i]['documento']+'</td>'+
                                    '<td>'+users[i]['email']+'</td>'+
                                    '<td>'+estado_ingreso+'</td>'+
                                    '<td><center>'+estado+'</center></td>'+
                                    '<td><center> '+botonEditar+'&nbsp;'+botonResetclave+'&nbsp;'+ver_hijos+'  </center></td>'+
                                  '</tr>';
                      $("#tbody_usuarios").append(fila);

                      cont++;
                  }

                  $('#loading_scroll_users').remove();
                  generar_datatable_usuarios();
                  
              },
              error: function(response)
              {
                  //hacer algo cuando ocurra un error
                  console.log(response);
              }
      }); 
  }


  
  //abrir_modal_editar_usuario(9);
  id_fila_ant = 0;
  //function abrir_modal_editar_usuario(id_user,group_id,id_user_group)
  function abrir_modal_editar_usuario(id_user, obj)
  {

    var primertr = $('#tbody_usuarios tr:first').attr('id');
    var estetr = $('#fila_usuario_'+id_user).attr('id');
    if(primertr == estetr)
      id_fila_ant = -1; //indica que se debe colocar en la primer fila
    else 
      id_fila_ant = $('#fila_usuario_'+id_user).prev().attr('id');


    $("#documento_txt_edit").val();
    $("#apellido_txt_edit").val();
    $("#nombre_txt_edit").val();
    $("#email_txt_edit").val();
    $("#user_id_edit").val();
    //$("#group_id_user_edit").val(group_id);
    $("#id_user_group_edit").val(id_user_group);
    $("#radio_masculino_edit").attr('checked', false);
    $("#radio_femenino_edit").attr('checked', false);

    //var colegioselect = $("#select_colegio_edit option:selected").val();
     if( $('#select_colegio_edit').length )
        var colegioselect = $('#select_colegio_edit').val();
      else var colegioselect = $('#colegioUnico').val() ;
    //busco si en el colegio seleccionado, tengo permiso para activar usuario
    var activar_user = arre_activar_user.indexOf(colegioselect);
    if(activar_user != -1)  
        $("#div_activar").css('display', 'block');
    else
        $("#div_activar").css('display', 'none');

    var rolselect = $("#select_rol_edit option:selected").text();
    if( (activar_user != -1) && (rolselect=='Alumno') ) 
        $("#div_pago").css('display', 'block');
    else
        $("#div_pago").css('display', 'none');

    
    var urlRequest = urlApi+"/usuario/obtener_user2/"; 
        $.ajax({
                url: urlRequest,
                  type: 'POST',
                  headers: {              
                   'APIKEY' : passApiKey,
                   'userid' : idUser,
                   'Access-Control-Allow-Origin': '*'
                },
                data :{id_user : id_user, colegiosId: arre_listar_user_join},
                beforeSend: function() 
                {
                  $(obj).css('display', 'none');
                  $(obj).before("<img  id='loading_edit_user' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");
                },
                success : function(response) 
                {   
                  if(response.status)
                  {

                    var roles = response.roles;
                    var data = response.usuario;

                  
                   //data["created_on"] -> timestamp
                    if( data["created_on"]!='0' )
                    {
                      var date = new Date(data["created_on"]*1000);
                      var hours = date.getHours();
                      var minutes = "0" + date.getMinutes();
                      var day = date.getDate();
                      var month = date.getMonth()+1;
                      var year = date.getFullYear();
                      var formattedTime = day+'/'+month+'/'+year+ ' ' +hours + ':' + minutes.substr(-2);
                    }
                    else var formattedTime = '-';

                    if(data["last_login"]!=null)
                    {
                      var date = new Date(data["last_login"]*1000);
                      var hours = date.getHours();
                      var minutes = "0" + date.getMinutes();
                      var day = date.getDate();
                      var month = date.getMonth()+1;
                      var year = date.getFullYear();
                      var formattedTime2 = day+'/'+month+'/'+year+ ' ' +hours + ':' + minutes.substr(-2);
                    }
                    else var formattedTime2 = '-';
                     

                    $("#documento_txt_edit").val(data["documento"]);
                    $("#apellido_txt_edit").val(data["last_name"]);
                    $("#nombre_txt_edit").val(data["first_name"]);
                    $("#email_txt_edit").val(data["email"]);
                    $("#user_id_edit").val(data["id"]);
                    $("#fecha_alta_usu").val(formattedTime);
                    $("#lastlogin_usu").val(formattedTime2);

                    if(data["active"]==1)
                      $("#activo_usu").prop('checked', true);
                    else  $("#activo_usu").prop('checked', false);

                    if(data["pago"]==1)
                      $("#pago_usu").prop('checked', true);
                    else  $("#pago_usu").prop('checked', false);
                     

                    if(data['sexo'] == 'F')
                        {
                          jQuery("#radio_femenino_edit").prop('checked', true);
                          //jQuery("#radio_masculino_edit").attr('checked', false);
                        }
                    else if(data['sexo'] == 'M')
                        {
                          jQuery("#radio_masculino_edit").prop('checked', true);
                         // jQuery("#radio_femenino_edit").attr('checked', false);
                        }


                    //ROLES
                    //console.log('users groups: ');
                    $('#tbody_roles_edit tr').remove();

                    for(var i=0; i < roles.length ; i++)
                    {
                      var aux = arre_admin_roles.indexOf(roles[i].colegio_id);
                      if(aux != -1)
                        var boton = '<button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="btn_alumnos" title="borrar" onclick="delete_usergroup('+roles[i].id+')" type="button"><i class="glyphicon glyphicon-trash"></i></button>';
                      else var boton = '';

                      fila = '<tr id="user_group_'+roles[i].id+'"><td>'+roles[i].colegio+'</td><td>'+roles[i].grupo+'</td><td>'+boton+'</td></tr>';
                      $('#tbody_roles_edit').append(fila);
                     
                    }
                    $('#roles_actuales2').show();

                    //solo guarda cambios el qe puede editar
                    if( $('#select_colegio_edit').length )
                      var colegio = $('#select_colegio_edit').val();
                    else var colegio = $('#colegioUnico').val() ;

                    var ban_edita = arre_edita_user.indexOf(colegio); //busco si en el colegio seleccionado puedo editar usuarios
                    if(ban_edita == -1)
                    {
                      $("#update_usuario").css('display','none');
                      $("#documento_txt_edit").attr('disabled',true);
                      $("#apellido_txt_edit").attr('disabled',true);
                      $("#nombre_txt_edit").attr('disabled',true);
                      $("#email_txt_edit").attr('disabled',true);
                      $("#radio_femenino_edit").attr('disabled',true);
                      $("#radio_masculino_edit").attr('disabled',true);
                    }else
                    { 
                      $("#update_usuario").css('display','inline');
                      $("#documento_txt_edit").attr('disabled',false);
                      $("#apellido_txt_edit").attr('disabled',false);
                      $("#nombre_txt_edit").attr('disabled',false);
                      $("#email_txt_edit").attr('disabled',false);
                      $("#radio_femenino_edit").attr('disabled',false);
                      $("#radio_masculino_edit").attr('disabled',false);
                    }

                    $("#modal_editar_usuario").modal('show');

                  }
                  else //error
                     console.log(data);
                  
                  $('#loading_edit_user').remove();
                  $(obj).css('display', 'block'); 
                    
                },
                error: function(response)
                {
                    //hacer algo cuando ocurra un error
                    console.log(response);
                    $('#loading_edit_user').remove();
                    $(obj).css('display', 'block'); 
                }

                
        }); 

  }
  function abrir_modal_resetear_clave(id_user)
  {
    //alert('aa');
    $("#user_id_reset").val(id_user);
    $("#modal_reset_clave").modal('show');
  }

  function abrir_modal_hijos(id_user, id_colegio, obj)
  {
    //alert('aa');
    $("#user_id_tutor").val(id_user);
    //var id_colegio = $('select[id=select_colegio]').val();
    //alert(id_colegio);
    $("#id_colegio_modal_hijos").val(id_colegio);
    
    var urlRequest = urlApi+"/Usuario/obtener_hijos_tutor";
    $.ajax
        ({
                url: urlRequest,
                type: 'POST',
                headers: {              
                 'APIKEY' : passApiKey,
                 'userid' : idUser,
                 'Access-Control-Allow-Origin': '*'},
                data: $('#form_hijos').serialize(),
                beforeSend: function() 
                {
                  $(obj).css('display', 'none');
                  $(obj).before("<img  id='loading_traer_hijos' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");
                },
                success: function(data)
                {
                  $("#table_hijos").empty();
                  $("#modal_hijos").modal('show');
                  var fila_hijo = '';
                  if(data.estado == true)
                  { 
                    //console.log(data.hijos);  
                    var hijos = data.hijos;   
                    var curso = '';                         
                    for(var i=0; i < hijos.length ; i++) 
                    {                    
                      var curso = hijos[i]["anio"]+' '+hijos[i]["division"];
                      fila_hijo = '<tr>'+
                                    '<td>'+hijos[i]["apellido_alumno"]+'</td>'+
                                    '<td>'+hijos[i]["nombre_alumno"]+'</td>'+
                                    '<td>'+curso+'</td>'+
                                '</tr>';
                      $("#table_hijos").append(fila_hijo);
                    }
                  
                    
                  }
                  else
                  {
                      fila_hijo = '<tr><td>No tiene Hijos Asignados </td></tr>';
                      $("#table_hijos").append(fila_hijo);
                  }

                  $('#loading_traer_hijos').remove();
                  $(obj).css('display', 'block'); 
                                                                   
                },
                error: function (jqXHR, textStatus, errorThrown,data)
                {
                  // document.getElementById("myModalLabel").innerHTML = 'Registro de Usuarios';
                        //document.getElementById("mensaje").innerHTML = 'ERROR';
                        //$("#myModal").modal('show');
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                    //console.log(data);

                    $('#loading_traer_hijos').remove();
                    $(obj).css('display', 'block'); 

                }
        });

  }

  function abrir_modal_tutores(id_user, id_colegio, obj)
  {
    $("#user_id_hijo").val(id_user);
    //var id_colegio = $('select[id=select_colegio]').val();
    //alert(id_colegio);
    $("#id_colegio_modal_tutores").val(id_colegio);
    
    var urlRequest = urlApi+"/Usuario/obtener_tutores_de_un_alumno";
    $.ajax
        ({
                url: urlRequest,
                type: 'POST',
                headers: {              
                 'APIKEY' : passApiKey,
                 'userid' : idUser,
                 'Access-Control-Allow-Origin': '*'},
                data: $('#form_tutores').serialize(),
                beforeSend: function() 
                {
                  $(obj).css('display', 'none');
                  $(obj).before("<img  id='loading_traer_tutores' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");
                },
                success: function(data)
                {
                  $("#table_tutores").empty();
                  $("#modal_tutores").modal('show');
                  var fila_tutor = '';
                  if(data.estado == true)
                  { 
                    //console.log(data.hijos);  
                    var tutores = data.tutores;   


                                            
                    for(var i=0; i < tutores.length ; i++) 
                    {                    
                      //console.log(tutores[i]);
                      //console.log(id_colegio);
                      var aux = arre_asigna_tutor.indexOf(""+id_colegio);
                      //console.log(aux);
                      if(aux != -1)
                        var delet = '<button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="btn_alumnos" onclick="eliminar_tutor_alumno(this,'+id_user+','+tutores[i]["id"]+')" type="button"><i class="fa fa-trash-o"><i></button>'
                      else var delet = '';
                     
                      fila_tutor = '<tr>'+
                                    '<td>'+tutores[i]["last_name"]+'</td>'+
                                    '<td>'+tutores[i]["first_name"]+'</td>'+
                                    '<td>'+tutores[i]["documento"]+'</td>'+
                                    '<td>'+tutores[i]["email"]+'</td>'+
                                   '<td>'+delet+'</td>'+
                                '</tr>';
                      $("#table_tutores").append(fila_tutor);
                    }
                  
                    
                  }
                  else
                  {
                      fila_tutor = '<tr><td>No tiene Tutores Asignados </td></tr>';
                      $("#table_tutores").append(fila_tutor);
                  }


                  $('#loading_traer_tutores').remove();
                  $(obj).css('display', 'block'); 
                                                                   
                },
                error: function (jqXHR, textStatus, errorThrown,data)
                {
                  // document.getElementById("myModalLabel").innerHTML = 'Registro de Usuarios';
                        //document.getElementById("mensaje").innerHTML = 'ERROR';
                        //$("#myModal").modal('show');
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                    //console.log(data);

                    $('#loading_traer_tutores').remove();
                    $(obj).css('display', 'block');
                }
        });
  }

</script>
<script type="text/javascript">
              $(document).ready(function() 
              { 

                  $('#guardar').click(function()
                  { //alert('guardar');   
                      var urlRequest = urlApi+"/Usuario/guardar_usuario";  
                      $('#alta').validate
                      ({
                          rules: {
                              nombre_txt: { required: true},
                              apellido_txt: { required: true},
                             
                              documento_txt: { required: true},
                              
                          },
                          messages: {
                              nombre_txt:  {required: "Debe introducir el Nombre."}, 
                              apellido_txt:  {required: "Debe Introducir el Apellido."},
                              documento_txt:  {required: "Debe Introducir el Documento."},
                             
                          },
                          submitHandler: function(form)
                          {
                            var rol = $('#select_rol').val();  
                            if( $('#select_rol') && $('#select_rol').val()>0 )
                                  $.ajax
                                  ({
                                      url: urlRequest,
                                      type: 'POST',
                                      headers: {              
                                       'APIKEY' : passApiKey,
                                       'userid' : idUser,
                                       'Access-Control-Allow-Origin': '*'},
                                      data: $('#alta').serialize(),
                                      beforeSend: function() 
                                      {
                                        $('#guardar').after("<img  id='loading_alta_user' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");

                                        $('#guardar').css('display','none');
                                      },
                                      success: function(data)
                                      {
                                          //alert('entro');
                                              console.log(data);

                                             $('#loading_alta_user').remove();
                                             $('#guardar').css('display','inline');

                                              $('#tbody_roles').empty();
                                              if(data.status == true)
                                              { 
                                                  $('#nombre_txt').val('');
                                                  $('#documento_txt').val('');
                                                  $('#apellido_txt').val('');
                                                  $('#email_txt').val('');
                                                  $('#user_id').val('0');
                                                  $('#tbody_roles').empty();
                                                  $('#form_ocultar').hide();
                                                  $('#form_roles').hide();
                                                  $('#roles_actuales').hide();
                                                  $('#tbody_roles').empty();
                                                  $('#tbody_roles').empty();
                                                  $('#select_colegio_alta').val(0);
                                                  $('#select_rol').val(0);

                                                  //limpiar form
                                                  /*if(data.existe == true)
                                                  {
                                                    console.log('El dni existe, se inserto en users group');
                                                  }
                                                  bootbox.alert({
                                                      message: data.message,
                                                      size: 'small'
                                                  });*/



                                                  $.notify(data.message, "success");

                                              }
                                              else
                                              {
                                                  //bootbox.alert(data.message);
                                                  $.notify(data.message, "warning");
                                                  $('#nombre_txt').val('');
                                                  $('#documento_txt').val('');
                                                  $('#apellido_txt').val('');
                                                  $('#email_txt').val('');
                                                  $('#user_id').val('0');
                                                  $('#tbody_roles').empty();
                                                  $('#form_ocultar').hide();
                                                  $('#form_roles').hide();
                                                  $('#roles_actuales').hide();
                                                  $('#tbody_roles').empty();
                                                  $('#select_colegio_alta').val(0);
                                                  $('#select_rol').val(0);
                                              }

                                              $('#select_colegio_alta').select2();
                                              $('#select_rol').select2();

                                          
                                      },
                                      error: function (jqXHR, textStatus, errorThrown,data)
                                      {
                                        // document.getElementById("myModalLabel").innerHTML = 'Registro de Usuarios';
                                              //document.getElementById("mensaje").innerHTML = 'ERROR';
                                              //$("#myModal").modal('show');
                                          console.log(jqXHR);
                                          console.log(textStatus);
                                          console.log(errorThrown);
                                          //console.log(data);
                                          $('#select_rol').val(0);

                                      }
                                  });

                            else
                               bootbox.alert({
                                                message: "Ingrese un Rol",
                                                size: 'small'
                                            });
                          }
                      });

                  
                  });
              });
              </script>
              <script type="text/javascript">
          /*$(document).ready(function()
            {
            $('#cancelar').click(function() 
            {
              //alert('hola');
              document.location.href = "<?//php echo site_url('Login/home')?>/";

            });
          }); */   
        </script>
        <script type="text/javascript">
 

 </script>

 <script type="text/javascript">
              $(document).ready(function() 
              {
                  $('#reset_clave_usuario').click(function()
                  { //alert('hola');
                    var urlRequest = urlApi+"/Usuario/reset_clave_usuario";  
                    $('#modal_reset_clave').modal('hide');  
                    $('#form_reset').validate
                          ({
                              rules: {
                                  user_id_reset: { required: true},
                                 
                                  
                              },
                              messages: {
                                  user_id_reset:  {required: "Debe introducir el Nombre."}, 
                                  
                              },
                             
                              submitHandler: function(form)
                              {
                                  
                                  $.ajax
                                  ({
                                          url: urlRequest,
                                          type: 'POST',
                                          headers: {              
                                           'APIKEY' : passApiKey,
                                           'userid' : idUser,
                                           'Access-Control-Allow-Origin': '*'},
                                          data: $('#form_reset').serialize(),
                                          beforeSend: function() 
                                          {
                                            $('#reset_clave_usuario').before("<img  id='loading_edit_pass' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");

                                            $('#reset_clave_usuario').css('display','none');
                                          },
                                     
                                          success: function(data)
                                          {
                                                  //console.log(data);
                                                  $('#loading_edit_pass').remove();
                                                  $('#reset_clave_usuario').css('display','inline');

                                                  if(data.status == true)
                                                  {
                                                      //limpiar form
                                                    bootbox.alert({
                                                        message: data.message,
                                                        size: 'small'
                                                    });
                                                  }
                                                  else
                                                  {
                                                    bootbox.alert(data.message);
                                                   

                                                  }
                                                  
                                              
                                          },
                                          error: function (jqXHR, textStatus, errorThrown,data)
                                          {
                                            // document.getElementById("myModalLabel").innerHTML = 'Registro de Usuarios';
                                                  //document.getElementById("mensaje").innerHTML = 'ERROR';
                                                  //$("#myModal").modal('show');
                                              console.log(jqXHR);
                                              console.log(textStatus);
                                              console.log(errorThrown);
                                              //console.log(data);

                                          }
                                  });
                              }
                          });
                  });
              })
 
</script>
 

 <script type="text/javascript"> //scroller: true,
 function generar_datatable2(id_rol)
            {var oTable;
              //console.log(id_rol);
                var handleDataTableButtons2 = function() {
                  if ($("#datatable-nodos"+id_rol).length) {
                    oTable = $("#datatable-nodos"+id_rol).DataTable({

                      "scroller": {
                        "displayBuffer": 2
                      },
                       
                      responsive: true,
                      keys: true,
                      deferRender: true,
                      scrollCollapse: true,
                      
                      
                      "language": {
                                    "sProcessing":     "Procesando...",
                                    "sLengthMenu":     "Mostrar _MENU_ registros",
                                    "sZeroRecords":    "No se encontraron resultados",
                                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                                    "sInfoPostFix":    "",
                                    "sSearch":         "Buscar:",
                                    "sUrl":            "",
                                    "sInfoThousands":  ",",
                                    "sLoadingRecords": "Cargando...",
                                    "oPaginate": {
                                        "sFirst":    "Primero",
                                        "sLast":     "Último",
                                        "sNext":     "Siguiente",
                                        "sPrevious": "Anterior"
                                    },
                                    "oAria": {
                                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                    } 
                                  }
                    });
                  }
                };
                TableManageButtons2 = function() {
                  "use strict";
                  return {
                    init: function() {

                      handleDataTableButtons2();
                    }
                  };
                }();
                var table = $('#datatable-fixed-header').DataTable({
                  fixedHeader: true,

                });
                
                TableManageButtons2.init();

                /*oTable.$('td').editable( '../example_ajax.php', {
                "callback": function( sValue, y ) {
                    var aPos = oTable.fnGetPosition( this );
                    oTable.fnUpdate( sValue, aPos[0], aPos[1] );
                },
                "submitdata": function ( value, settings ) {
                    return {
                        "row_id": this.parentNode.getAttribute('id'),
                        "column": oTable.fnGetPosition( this )[2]
                    };
                },

                "width": "90%",
                "height": "100%"
            } );*/
            }
 </script>
 <script type="text/javascript"> //scroller: true,
 function generar_datatable_usuarios()
            {
              var oTable;
              //console.log(id_rol);
                var handleDataTableButtons3 = function() {
                  if ($("#table_usuarios").length) {
                    oTable = $("#table_usuarios").DataTable({

                      "scroller": {
                        "displayBuffer": 2
                      },
                      "columnDefs": [
                      { 
                        "targets": [-1], //last column
                        "orderable": false, //set not orderable
                         "width": "100%",
                      },
                     
                      ],
                       
                      responsive: true,
                      keys: true,
                      deferRender: true,
                      scrollCollapse: true,
                      
                      
                      "language": {
                                    "sProcessing":     "Procesando...",
                                    "sLengthMenu":     "Mostrar _MENU_ registros",
                                    "sZeroRecords":    "No se encontraron resultados",
                                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                                    "sInfoPostFix":    "",
                                    "sSearch":         "Buscar:",
                                    "sUrl":            "",
                                    "sInfoThousands":  ",",
                                    "sLoadingRecords": "Cargando...",
                                    "oPaginate": {
                                        "sFirst":    "Primero",
                                        "sLast":     "Último",
                                        "sNext":     "Siguiente",
                                        "sPrevious": "Anterior"
                                    },
                                    "oAria": {
                                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                    } 
                                  }
                    });
                  }
                };
                TableManageButtons3 = function() {
                  "use strict";
                  return {
                    init: function() {

                      handleDataTableButtons3();
                    }
                  };
                }();
                 TableManageButtons3.init();
                var table = $('#datatable-fixed-header').DataTable({
                  fixedHeader: true,

                });
                
               

                /*oTable.$('td').editable( '../example_ajax.php', {
                "callback": function( sValue, y ) {
                    var aPos = oTable.fnGetPosition( this );
                    oTable.fnUpdate( sValue, aPos[0], aPos[1] );
                },
                "submitdata": function ( value, settings ) {
                    return {
                        "row_id": this.parentNode.getAttribute('id'),
                        "column": oTable.fnGetPosition( this )[2]
                    };
                },

                "width": "90%",
                "height": "100%"
            } );*/
            }
 </script>

 <script>
$(document).ready(function() 
{
  $('#update_usuario').click(function()
  { 
    
    var urlRequest = urlApi+"/Usuario/update_usuario";  
    
    $('#form_edit_users').validate
    ({
        rules: {
            nombre_txt_edit: { required: true},
            apellido_txt_edit: { required: true},
             // email_txt_edit: { required: true},
              documento_txt_edit: { required: true},
            
        },
        messages: {
            nombre_txt_edit:  {required: "Debe introducir el Nombre."}, 
            apellido_txt_edit:  {required: "Debe Introducir el Apellido."},
            documento_txt_edit:  {required: "Debe Introducir el Documento."},
           // email_txt_edit:  {required: "Debe Introducir el E-mail.", email: "Debe Introducir un E-mail Válido"},
        },
        submitHandler: function(form)
        {
            
            if( $('#activo_usu').is(':checked') )
              var activo=1;
            else
              var activo=0;

            if( $('#pago_usu').is(':checked') )
              var pago=1;
            else
              var pago=0;
           
            if( $('#radio_masculino_edit').is(':checked') )
              var sexo='M';
            else
              var sexo='F';

            var parametros = {
                  'first_name': $('#nombre_txt_edit').val(),
                  'last_name': $('#apellido_txt_edit').val(),     
                  'email': $('#email_txt_edit').val(),   
                  'sexo': sexo,
                  'documento':  $('#documento_txt_edit').val(),
                  'active':  activo,
                  'pago':  pago,
            };
            var parametros = JSON.stringify(parametros);

            $.ajax
            ({
                    url: urlRequest,
                    type: 'POST',
                    headers: {              
                     'APIKEY' : passApiKey,
                     'userid' : idUser,
                     'Access-Control-Allow-Origin': '*'},
                    data: {data: parametros, user_id_edit:$('#user_id_edit').val(), idfila:id_fila_ant},
                    beforeSend: function() 
                    { 
                      $('#update_usuario').before("<img  id='loading_edit_user' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");

                      $('#update_usuario').css('display','none');
                    },
                    success: function(data)
                    {
                            //alert('entro');
                            //console.log(data);
                            $('#loading_edit_user').remove();
                            $('#update_usuario').css('display','inline');

                            $('#tbody_roles').empty();
                            
                            if(data.status == true)
                            {
                              $.notify(data.message, "success");

                              if(data.existe == false)
                              {
                                  var user_id = data.id_user;
                                  $('#nombre_txt_edit').val('');
                                  $('#documento_txt_edit').val('');
                                  $('#apellido_txt_edit').val('');
                                  $('#email_txt_edit').val('');
                                  $('#user_id_edit').val('');

                                  $("#table_usuarios").dataTable().fnDestroy();
                                  $("#fila_usuario_"+user_id).remove();

                                  var users = data.usuario;
                                  var estado_user = users['active'];
                                  var estado = '';
                                  
                                  var ingreso_nodos = users['iteration'];
                                  var estado_ingreso ='';

                                  if(estado_user == 1)
                                  {
                                    estado = '<i id="estado_usu_'+user_id+'" class="fa fa-check"> </i>';
                                  }
                                  else
                                  {
                                    estado = '<i id="estado_usu_'+user_id+'" class="fa fa-times"> </i>';
                                  }

                                  if(ingreso_nodos == 1)
                                  {
                                    estado_ingreso = '<i id="estado_ingreso'+user_id+'" class="fa fa-check"> </i>';
                                  }
                                  else
                                  {
                                    estado_ingreso = '<i id="estado_ingreso'+user_id+'" class="fa fa-times"> </i>';
                                  }

                                  var idtr = users['idfila'];
                                  if(idtr == -1)//colocar en la primer fila
                                    var num = 1
                                  else
                                  {
                                    var num = $('#'+idtr+' td:first').text();
                                    num = parseInt(num)+1;
                                  }


                                
                                      var rol_seleccionado = $('#select_rol_edit option:selected').text();
                                      //var id_users_groups = users['id_user_group'];
                                      var id_colegio = $('#select_colegio_edit').val();
                                      var ver_hijos = '';
                                      if(rol_seleccionado == 'Tutor')
                                      {
                                        ver_hijos = '<button class="btn btn-sm btn-info pull-left" id="btn_alumnos" title="Ver Hijos" onclick="abrir_modal_hijos('+user_id+','+id_colegio+',this)" type="button"><i class="glyphicon glyphicon-user"></i></button>';
                                      }
                                      else if(rol_seleccionado == 'Alumno')
                                      {
                                        ver_hijos = '<button class="btn btn-sm btn-info pull-left" id="btn_alumnos" title="Ver Tutores" onclick="abrir_modal_tutores('+user_id+','+id_colegio+',this)" type="button"><i class="glyphicon glyphicon-user"></i></button>';
                                      }
                                      
                                      
                                      /*var fila = '<tr id="fila_usuario_'+user_id+'">'+
                                            '<td>Editado</td>'+
                                            '<td>'+users['last_name']+'</td>'+
                                            '<td>'+users['first_name']+'</td>'+
                                            '<td>'+users['documento']+'</td>'+
                                            '<td>'+users['email']+'</td>'+
                                            '<td>'+estado_ingreso+'</td>'+
                                            '<td><center>'+estado+'</center></td>'+
                                            '<td><button class="btn btn-sm btn-primary" id="btn_alumnos" title="Editar" onclick="abrir_modal_editar_usuario('+user_id+','+group_id+','+id_users_groups+')" type="button"><i class="glyphicon glyphicon-pencil"></i></button>      <button class="btn btn-sm btn-danger" id="btn_alumnos" title="Resetear Clave" onclick="abrir_modal_resetear_clave('+user_id+')" type="button"><i class="glyphicon glyphicon-retweet"></i></button>       '+ver_hijos+'</td>'+
                                        '</tr>';*/
                                  

                                  var fila = '<tr id="fila_usuario_'+user_id+'">'+
                                                '<td>'+num+'</td>'+
                                                '<td>'+users['last_name']+'</td>'+
                                                '<td>'+users['first_name']+'</td>'+
                                                '<td>'+users['documento']+'</td>'+
                                                '<td>'+users['email']+'</td>'+
                                                '<td>'+estado_ingreso+'</td>'+
                                                '<td><center>'+estado+'</center></td>'+
                                                '<td><center><button class="btn btn-sm btn-primary pull-left " id="btn_alumnos" title="Editar" onclick="abrir_modal_editar_usuario('+user_id+',this)" type="button"><i class="glyphicon glyphicon-pencil"></i></button>   &nbsp;&nbsp;   <button class="btn btn-sm btn-primary pull-left" id="btn_alumnos" title="Resetear Clave" onclick="abrir_modal_resetear_clave('+user_id+')" type="button"><i class="glyphicon glyphicon-retweet"></i></button> '+ver_hijos+'</center></td>'+
                                            '</tr>';
                                if(idtr == -1)
                                  $('#tbody_usuarios tr:first').before(fila);
                                else
                                  $('#'+idtr).after(fila);
                                
                                generar_datatable_usuarios();
                                   
                                $("#modal_editar_usuario").modal('hide');
                              }
                              
                            }
                            else
                            {
                                switch(data.campo)
                                { 
                                    case 'dni': $('#documento_txt_edit').val('');
                                                $('#documento_txt_edit').focus();
                                                break;
                                    case 'username': $('#username_txt_edit').val('');
                                                    $('#username_txt_edit').focus();
                                                break;
                                    case 'email': $('#email_txt_edit').val('');
                                                  $('#email_txt_edit').focus();
                                                break;
                                }

                                //bootbox.alert(data.message);

                                bootbox.alert({
                                    message: data.message,
                                    size: 'small'
                                });

                            }
                            
                        
                    },
                    error: function (jqXHR, textStatus, errorThrown,data)
                    {
                      // document.getElementById("myModalLabel").innerHTML = 'Registro de Usuarios';
                            //document.getElementById("mensaje").innerHTML = 'ERROR';
                            //$("#myModal").modal('show');
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                        //alert('error');

                        $('#loading_edit_user').remove();
                        $('#update_usuario').css('display','inline');
                        //console.log(data);

                    }
            });
        }
    });


  });
});
 </script>
 <script type="text/javascript">
  setInterval('validar_eventos_nuevos()',5000);
  var base_url = '<?php echo base_url('index.php/Login/home'); ?>';
  function validar_eventos_nuevos(){
    //alert('hola');
    var urlRequest = urlApi+"/evento/nuevos_recientes";
    $.ajax
    ({
        url: urlRequest,
        type: 'GET',
        headers: 
        {              
          'APIKEY' : passApiKey,
          'userid' : idUser,
          'Access-Control-Allow-Origin': '*'
        },
        data :{id: idUser},
        success: function(data)
        {
            if(data.status == true)
                  {
                    var eventos = data.push_recientes
                        for(var i=0; i < eventos.length ; i++) //aca solo listo niveles y especialidades
                        {
                          Push.create("NODOS",{
                          body:"As recibido un Nuevo Evento",
                          icon: "http://e-nodos.com/nodosweb/assets/images/i/icon-isologo-nodos.png",
                          timeout: 4000,
                          onClick: function () {
                            window.focus();
                            //document.location.href = base_url+'/#form_evento'+eventos[i]['id'];
                            document.location.href = base_url;
                            this.close();
                          },
                        });

                            
                        }
                        //console.log(ultimo_id);
                        
                        //console.log(data.length);
                        
                  }                  
                                

        },
        error: function(response)
         {
            //hacer algo cuando ocurra un error
            console.log(response);
        }
    });

    
}
</script>