<?php 
 
class Usuario extends CI_Controller {	
      
	function __construct(){
		parent::__construct();
		
			//$this->output->enable_profiler(TRUE);
        if(!$this->session->userdata("logged_in")){
            //echo "pierde sesion"; die();
               redirect("login");
         }

	}
		
    public function index()
    { 
    $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
        $passApiKey          = $this->variables->get_apikey();
        $data['urlApi']      = $urlApi;
        $data['passApiKey']  = $passApiKey;
        $data['idusuario']   = $this->session->userdata('idusuario');
        $data['idcolegio']   = $this->session->userdata('colegio');      
        $data['idgrupo']     = $this->session->userdata('idgrupo');
        $data['nombregrupo'] = $this->session->userdata('nombregrupo');
       // $data['club']        = $this->mis_funciones->get_club();
        $data['localidad']   = $this->mis_funciones->get_localidad();
        $data['rama']        = $this->mis_funciones->get_rama();
        $data['licencia']    = $this->mis_funciones->get_licencia();
        
       

        $data['opcion_menu'] = 'Usuario';
        $band = 0;
        $controlador = "Usuario";
        foreach ($this->session->userdata('menu') as $menu) //validar permiso de controlador y metodo
                {
                    if(($menu->controlador == $controlador) && ($menu->metodo == 'index'))
                    {
                        $band = 1;// encontro el controlador y el metodo en la sesion, el user tiene permiso
                    }
                }
        if($band == 1)
        {
                //echo "hola tutores";
                
                
                $data['opcion_menu'] = 'usuarios';
                $data['contenido'] = 'panel/usuarios/index';
                //$data['contenido'] = 'panel/vista_horarios';
                $this->load->view('include/template_colegio',$data);
              
        }
        else redirect(404);        

    }
    public function obtener_roles($urlApi, $passApiKey, $idUser)  
    {  
        $url = $urlApi."/usuario/obtener_todos_roles/";
        $ch = curl_init();
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser") ,
            CURLOPT_SSL_VERIFYPEER => false,
           
        );
        curl_setopt_array( $ch, $options);
        $response = curl_exec($ch);
        curl_close($ch);
        
        return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
    }
    public function usuarios()
    {
        $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
        $passApiKey = $this->variables->get_apikey();
        $idUser = $this->session->userdata('idusuario');
        
        $idcolegio = $this->session->userdata('idcolegio');
        //$data['titulo'] = "Listado de Alumnos";
        $data['urlApi'] = $urlApi;
        $data['passApiKey'] = $passApiKey;
        $data['idusuario'] = $this->session->userdata('idusuario');
        $data['idcolegio'] = $this->session->userdata('idcolegio');
        $data['idgrupo'] = $this->session->userdata('idgrupo');
        $data['nombregrupo'] = $this->session->userdata('nombregrupo');
        $data['ordengrupo'] = $this->session->userdata('ordengrupo');
        $data['nombrecolegio'] = $this->session->userdata('nombrecolegio');
        
        $band = 0;
        $controlador = "Usuario";    
        foreach ($this->session->userdata('menu') as $menu) //validar permiso de controlador y metodo
                {
                    if(($menu->controlador == $controlador) && ($menu->metodo == 'usuarios'))
                    {
                        $band = 1;// encontro el controlador y el metodo en la sesion, el user tiene permiso
                    }
                }
        if($band == 1)
        {
            $response = $this->obtener_roles($urlApi, $passApiKey, $idUser);
           // var_dump($response);
           // $data['roles'] = json_decode($response);
            $data['contenido'] = 'panel/usuarios/nuevo_users';
            $this->load->view('include/template_colegio',$data);
        }
        else redirect(404); 
    }

    public function importar_alumnos(){
    	//VARIABLES
    	$tabla = "alumnos_1a";
    	$division_id = 1;
    	$group_id = 6;
    	$colegio_id = 1;
    	$plan_estudio_id = 4;

		//SCRIPT    	
    	$sql = "SELECT * FROM ".$tabla;
    	$query = $this->db->query($sql);
    	if($query->num_rows() > 0){
    		foreach ($query->result() as $key => $value) {
    			$insert_alumno = "INSERT INTO users (first_name, last_name, active, documento, sexo)
    						VALUES ('$value->nombre', '$value->apellido', '$value->dni', $value->sexo' )";
    			if($this->db->query($insert_alumno)){
    				$id_alumno = $this->db->insert_id();    				
    				//insertar en users_groups
    				$insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id)
    						VALUES ($id_alumno, $group_id, $colegio_id)";
    				if($this->db->query($insert_users_groups)){
    					//insertar en inscripcion
	    				$insert_inscripcion = "INSERT INTO inscripcion (alumno_id, planesestudio_id, divisiones_id)
	    						VALUES ($id_alumno, $plan_estudio_id, $division_id)";
	    						if($this->db->query($insert_users_groups)){
	    							echo "todo OK";
	    						}
    				}    
    			}
    		}
    	}
    }

    public function actualizar_emails(){
        $tabla = "alumnos_email";
        $alumnos = $this->db->query("SELECT * FROM $tabla");
        $cant = 0;
        $sin_doc = array();
        foreach($alumnos->result() as $alu){
            $email = "";
            if(!empty($alu->email)){
                $email = $alu->email;
            }
            if(!empty($alu->documento)){
                $update = "UPDATE users 
                            SET email = '$email', pago = $alu->pago
                            WHERE users.documento = '$alu->documento'";                
                //echo $update."<br>";
                
                if($this->db->query($update)){
                    echo $alu->apellido." ".$alu->nombre." OK <br>";
                }
                else{
                    echo $alu->apellido." ".$alu->nombre." NO <br>";   
                }                            
            }
            else{
                $sin_doc[$cant]["nombre"] = $alu->nombre;
                $sin_doc[$cant]["apellido"] = $alu->apellido;
                $sin_doc[$cant]["pago"] = $alu->pago;
                $cant++;
                echo "Documento vacio<br>";
            }
        }
        echo "/************************************************************/<br>";
        echo "Cantidad de alumnos que no tienen documento ".$cant."<br>";
        foreach($sin_doc as $sd){
            echo $sd["nombre"]." ".$sd["apellido"]." ".$sd["pago"]."<br>";
        }
    }
    public function perfil_user()
    {
        $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
        $passApiKey = $this->variables->get_apikey();
        $data['urlApi'] = $urlApi;
        $data['passApiKey'] = $passApiKey;
        $data['idusuario'] = $this->session->userdata('idusuario');
        $data['idcolegio'] = $this->session->userdata('idcolegio');
        $data['idgrupo'] = $this->session->userdata('idgrupo');
        $data['nombregrupo'] = $this->session->userdata('nombregrupo');
        $data['ordengrupo'] = $this->session->userdata('ordengrupo');
        //$data['users'] = $this->modelo_usuario->get_users_id($this->session->userdata('user_id'));


        //var_dump($this->session->userdata);
        $tutor = true;
        $alumno = true;
        foreach ($this->session->userdata('colegios') as $colegio) 
        {
            //var_dump($colegio['rol']);
            if($colegio['rol'] != 'Tutor')
            {
                $tutor = false;
                //die();
            }
            if($colegio['rol'] != 'Alumno')
            {
                $alumno = false;
                //die();
            }

        }
            # code...
            
        if($tutor == true)
        {
            $data['editaHijo'] = 1;
            $data['contenido'] = 'panel/perfil/perfil.php';
            $data['contenido2'] = 'panel/perfil/perfil_mishijos.php'; //depende de perfil, porque comparten funciones
            $this->load->view('include/template_tutores',$data);
        }
        else
        if($alumno == true)
        {   
            $data['editaHijo'] = 0;
            $data['contenido'] = 'panel/perfil/perfil.php';
            
            $this->load->view('include/template_tutores',$data);
        }
        else
        {
            $data['editaHijo'] = 0;
            
            $data['contenido'] = 'panel/perfil/perfil.php';
            $this->load->view('include/template_colegio',$data);
        }
    }    
    /**
     *Armo un arreglo con las divisiones y creo una tabla aux por cada division
     */
    /*public function crear_tabla_divisiones()
    {
        $divisiones = array("1º 1ra","1º 2da","1º 3ra","1º 4ta","1º 5ta","1º 6ta","1º 7ma","2º 1ra","2º 2da","2º 3ra","2º 4ta","2º 5ta","2º 6ta","2º 7ma","3º 1ra","3º 2da","3º 3ra","3º 4ta","3º 5ta","3º 6ta","3º 7ma","4º Automotores","4º Construcciones","4º Electrónica","4º Equipos e Instalaciones" ,"4º Industrias de Proceso","4º Minas","4º Química","4º Vial","5º Automotores","5º Construcciones","5º Electrónica","5º Equipos e Instalaciones","5º Industrias de Proceso","5º Minas","5º Química","5º Vial","6º Automotores","6º Construcciones","6º Electrónica","6º Equipos e Instalaciones","6º Industrias de Proceso","6º Minas","6º Química","6º Vial","7º Automotores","7º Construcciones","7º Electrónica","7º Equipos e Instalaciones","7º Industrias de Proceso","7º Minas","7º Química","7º Vial");

        foreach ($divisiones as $value) 
        {
            $sql = "CREATE TABLE IF NOT EXISTS `bf_".$value."` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `user_id` int(11) NOT NULL,
                    `rol` varchar(100) NOT NULL,
                    `apellido` varchar(100) NOT NULL,
                    `nombre` varchar(100) NOT NULL,
                    `documento` int(11) NOT NULL,
                    `email` varchar(200) NOT NULL,
                    `division` varchar(200) NOT NULL,
                    PRIMARY KEY (`id`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
            if($this->db->query($sql))
            {
                echo " ok ".$value."<br>";           
            }
        }
    }*/

    /**
     *Funcion que inserta cada alumno en otra tabla dependiendo la division aux en la que pertenezca
     */
    /*public function insertar_alumnos_divisiones($offset, $length)
    {
        //Obtengo los alumnos y los recorro
        $sql = "SELECT * FROM bf_alumnos LIMIT ".$offset.",".$length;
        $alumnos = $this->db->query($sql);
        $count = 0;
        $count_error = 0;
        foreach($alumnos->result() as $key=>$value){
            $div = trim(mb_strtolower($value->division,'UTF-8'));
            $sql_insert = "INSERT INTO `bf_".$div."` 
            (apellido, nombre, documento) VALUES ('".$value->apellido."','".$value->nombre."','".$value->documento."')";
            if($this->db->query($sql_insert)){
                echo "ok ".$value->documento."<br>";
                $count++;
            }
            else{
                echo "error ".$value->documento."<br>";
                $count_error++;                
            }
        }

        echo "Se insertaron ".$count." alumnos y ".$count_error." erroneos";
    }*/

    /**
     *Funcion vacia las tablas de las divisiones auxiliares
     */
    /*public function vaciar_divisiones(){
        $divisiones = array("1º 1ra","1º 2da","1º 3ra","1º 4ta","1º 5ta","1º 6ta","1º 7ma","2º 1ra","2º 2da","2º 3ra","2º 4ta","2º 5ta","2º 6ta","2º 7ma","3º 1ra","3º 2da","3º 3ra","3º 4ta","3º 5ta","3º 6ta","3º 7ma","4º Automotores","4º Construcciones","4º Electrónica","4º Equipos e Instalaciones" ,"4º Industrias de Proceso","4º Minas","4º Química","4º Vial","5º Automotores","5º Construcciones","5º Electrónica","5º Equipos e Instalaciones","5º Industrias de Proceso","5º Minas","5º Química","5º Vial","6º Automotores","6º Construcciones","6º Electrónica","6º Equipos e Instalaciones","6º Industrias de Proceso","6º Minas","6º Química","6º Vial","7º Automotores","7º Construcciones","7º Electrónica","7º Equipos e Instalaciones","7º Industrias de Proceso","7º Minas","7º Química","7º Vial");
        $count = 0;
        $count_error = 0;
        foreach ($divisiones as $value) {
            $sql = "TRUNCATE `bf_".mb_strtolower($value,'UTF-8')."`";
            if($this->db->query($sql)){
                echo "ok <br>";
                $count++;
            }
            else{
                echo "error <br>";
                $count_error++; 
            }
        }
        echo "Se vaciaron ".$count." tablas y ".$count_error." erroneos";
    } */

    //damian
    public function importar_docentes_st()
    {
        //VARIABLES
        $tabla = "st_doc_prim"; //--modificar por la division
        
        $group_id = 34;
        $colegio_id = 4;
        $plan_estudio_id = 1;

        //SCRIPT        
        $sql = "SELECT * FROM ".$tabla;
        $cont = 0;
        $query = $this->db->query($sql);
        if($query->num_rows() > 0)
        {
            foreach ($query->result() as $key => $value) 
            {
                $sql_check= "SELECT * FROM users WHERE documento='".$value->dni."'";
                $query_ch = $this->db->query($sql_check);
                if($query_ch->num_rows() == 0)
                {
                    $pass_alu = $this->crear_pass($value->dni);
                    if($value->mail == null)
                            {
                                $email = "";
                            }
                            else
                            {
                                $email = $value->mail;
                            }

                    $insert_docente = "INSERT INTO users (first_name, last_name, active, documento, sexo, password,email)
                            VALUES ('$value->nombre', '$value->apellido',1, '$value->dni', '$value->sexo','$pass_alu','$email')";
                                
                   if($this->db->query($insert_docente))
                   {
                        $id_user_docente = $this->db->insert_id();                    
                        //insertar en users_groups
                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id)
                                VALUES ($id_user_docente, $group_id, $colegio_id)";
                        $this->db->query($insert_users_groups);
                         $cont = $cont + 1;
                           
                    }
                } // fin if el alumno ya existe.
                else{
                    $docente = $query_ch->row();
                    $id_doc = $docente->id;

                    echo "EL Docente".$value->dni." ".$value->apellido." ".$value->nombre." Existe en USERS <br>";
                    $sql_check_user_group= "SELECT * FROM users_groups WHERE user_id='".$id_doc."'and group_id = $group_id";
                    $query_ch_user_group = $this->db->query($sql_check_user_group);
                    if($query_ch_user_group->num_rows() > 0)
                    {
                        echo "EL Docente YA ES DOCENTE EN LA ESCUELA SELECCIONADA<br>";
                    }
                    else
                    {
                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id)
                                VALUES ($id_doc, $group_id, $colegio_id)";
                        $this->db->query($insert_users_groups);
                         //$cont = $cont + 1;
                    }
                    
                }
                
            } // fin for recorrido de los alumnos.
            echo "se insertaron: ".$cont." Docentes<br>";
        }
    } // fin table importar_docentes_st
    //2017
    public function importar_alumnos_m_2017()
    {
        //VARIABLES
        $tabla = "aux_modelo_2017"; //--modificar por la division
       // $division_id = 63;
        $group_id = 6;
        $colegio_id = 1;
        $plan_estudio_id = 2;

        $sql = "SELECT * FROM ".$tabla;
        $cont = 0;
        $query = $this->db->query($sql);
        if($query->num_rows() > 0)
            foreach ($query->result() as $key => $value) 
            {
                $sql_check= "SELECT * FROM users WHERE documento='".$value->dni."'";
                $query_ch = $this->db->query($sql_check);
                if($query_ch->num_rows() == 0)
                {
                    $pass_alu = $this->crear_pass($value->dni);
                    $insert_alumno = "INSERT INTO users (first_name, last_name, active, documento, sexo, password, recibir_notificacion)
                            VALUES ('$value->nombre', '$value->apellido',1, '$value->dni', '$value->sexo','$pass_alu',1)";
                    if($this->db->query($insert_alumno))
                    {
                        $id_user_alumno = $this->db->insert_id();                    
                        //insertar en users_groups
                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id)
                                VALUES ($id_user_alumno, $group_id, $colegio_id)";
                        if($this->db->query($insert_users_groups))
                        {
                            //insertar en inscripcion
                            $id_alumno = $this->db->insert_id();
                            $insert_inscripcion = "INSERT INTO inscripciones (alumno_id,activo, planesestudio_id,divisiones_id)
                                    VALUES ($id_alumno,1,$plan_estudio_id,$value->id_division)";
                                   //echo $insert_inscripcion."<br>";        

                                    if($this->db->query($insert_inscripcion))
                                    {
                                        echo "OK -".$value->dni." ".$value->apellido." ".$value->nombre."<br>";
                                        $cont = $cont + 1;
                                    }else
                                    {
                                      echo "NoInsertoAlumno: ".$value->dni." ".$value->apellido." ".$value->nombre."<br>";
                                      //echo "NoInsertoAlumno: <br>";
                                    }
                        }    
                    }


                }
                else{
                    echo "ELAlumnoExiste: ".$value->dni." ".$value->apellido." ".$value->nombre." <br>";
                }
            echo "se insertaron: ".$cont." alumnos";
            }
    }
    public function importar_alumnos_st_2017()
    {
        //VARIABLES
        $tabla = "aux_st_2017"; //--modificar por la division
       // $division_id = 63;
        $group_id = 36;
        $colegio_id = 4;
        $plan_estudio_id = 2;

        $sql = "SELECT * FROM ".$tabla;
        $cont = 0;
        $query = $this->db->query($sql);
        if($query->num_rows() > 0)
            foreach ($query->result() as $key => $value) 
            {
                $sql_check= "SELECT * FROM users WHERE documento='".$value->dni."'";
                $query_ch = $this->db->query($sql_check);
                if($query_ch->num_rows() == 0)
                {
                    $pass_alu = $this->crear_pass($value->dni);
                    $insert_alumno = "INSERT INTO users (first_name, last_name, active, documento, sexo, password, recibir_notificacion)
                            VALUES ('$value->nombre', '$value->apellido',1, '$value->dni', '$value->sexo','$pass_alu',1)";
                    if($this->db->query($insert_alumno))
                    {
                        $id_user_alumno = $this->db->insert_id();                    
                        //insertar en users_groups
                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id)
                                VALUES ($id_user_alumno, $group_id, $colegio_id)";
                        if($this->db->query($insert_users_groups))
                        {
                            //insertar en inscripcion
                            $id_alumno = $this->db->insert_id();
                            $insert_inscripcion = "INSERT INTO inscripciones (alumno_id,activo, planesestudio_id,divisiones_id)
                                    VALUES ($id_alumno,1,$plan_estudio_id,$value->id_division)";
                                   //echo $insert_inscripcion."<br>";        

                                    if($this->db->query($insert_inscripcion))
                                    {
                                        echo "OK -".$value->dni." ".$value->apellido." ".$value->nombre."<br>";
                                        $cont = $cont + 1;
                                    }else
                                    {
                                      echo "NoInsertoAlumno: ".$value->dni." ".$value->apellido." ".$value->nombre."<br>";
                                      //echo "NoInsertoAlumno: <br>";
                                    }
                        }    
                    }


                }
                else{
                    echo "ELAlumnoExiste: ".$value->dni." ".$value->apellido." ".$value->nombre." <br>";
                }
            echo "se insertaron: ".$cont." alumnos";
            }
    }

    public function alumno_tutor_st_2017()
    {
        $colegio_id = 4;
        $group_id = 35;
        /*$tabla_alumnos = "st_alu_6_anio_b";
        $sql = "SELECT * FROM ".$tabla_alumnos;
        $query_alumnos = $this->db->query($sql);*/

        $tabla_tutores = "aux_st_t_varios_2017";
        $sql2 = "SELECT * FROM ".$tabla_tutores;
        $query_tutores = $this->db->query($sql2);

        /*if($query_tutores->num_rows() > 0)
        {
            //print_r($query_tutores->num_rows())."<br>";
            foreach ($query_tutor->result() as $Tutor)
            {

            }
        }*/
        if($query_tutores->num_rows() > 0)
        {
            //print_r($query_alumnos->num_rows())."<br>";
            foreach ($query_tutores->result() as $tutor_t)
            {
                
                $sql_alumno = "SELECT * FROM users WHERE documento = $tutor_t->dni_a";
                $sql_alumno = $this->db->query($sql_alumno);

                if($sql_alumno->num_rows() > 0)
                {
                    $alumno = $sql_alumno->row();
                    $id_alumno = $alumno->id;

                    $sql_tutor = "SELECT * FROM users WHERE documento = $tutor_t->dni";
                      
                    $sql_tutor = $this->db->query($sql_tutor);
                    if($sql_tutor->num_rows() > 0)
                    {
                        $tutor = $sql_tutor->row();

                        $users_groups_sql ="SELECT * FROM users 
                                                            JOIN users_groups on users_groups.user_id =  users.id 
                                                            WHERE users.id = $tutor->id and users_groups.colegio_id = $colegio_id";
                        $users_groups_sql = $this->db->query($users_groups_sql);
                        if($users_groups_sql->num_rows() > 0)//existe en el colegio
                        {
                            $sql_tutor_alumno_aux = "SELECT * FROM tutoralumno where alumno_id = $id_alumno and tutor_id = $tutor->id";
                                    $query_tutor_alumno_aux = $this->db->query($sql_tutor_alumno_aux);
                                    if($query_tutor_alumno_aux->num_rows() > 0)
                                    {
                                        echo "EXISTE Relacion alumno tutor, TUTOR: ".$tutor->documento." ".$tutor->last_name." ".$tutor->first_name."  Alumno:(".$alumno->documento." ".$alumno->last_name." ".$alumno->first_name.") <br>";
                                    }
                                    else
                                    {
                                        $table = 'tutoralumno';
                                    
                                        $insert_relacion = "INSERT INTO tutoralumno (alumno_id,tutor_id)
                                        VALUES ($id_alumno,$tutor->id)";

                                       if($this->db->query($insert_relacion))
                                       {
                                        echo "ok Tutor: ".$tutor->documento." ".$tutor->last_name." ".$tutor->first_name." Insertado en la Relacion alumno tutor, Alumno:".$alumno->documento." ".$alumno->last_name." ".$alumno->first_name." <br>";
                                       }
                                    }
                        }
                        else
                        {
                            $sql_tutor_alumno_aux = "SELECT * FROM tutoralumno where alumno_id = $id_alumno and tutor_id = $tutor->id";
                                    $query_tutor_alumno_aux = $this->db->query($sql_tutor_alumno_aux);
                                    if($query_tutor_alumno_aux->num_rows() > 0)
                                    {
                                        echo "Existe Relacion alumno tutor <br>";
                                    }
                                    else
                                    {
                                        //-- USERS_GRUPS Insert ---
                                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id)
                                                                VALUES ($tutor->id, $group_id, $colegio_id)";
                                        if($this->db->query($insert_users_groups)){
                                            $id_user_group = $this->db->insert_id();
                                        } 
                                        //--- TUTORALUMNO insert
                                        $table = 'tutoralumno';
                                        
                                            $insert_relacion = "INSERT INTO tutoralumno (alumno_id,tutor_id)
                                            VALUES ($id_alumno,$tutor->id)";

                                           if($this->db->query($insert_relacion))
                                           {
                                            echo "ok Padre(tutor: ".$tutor->documento." ".$tutor->last_name." ".$tutor->first_name.") Insertado en la Relacion alumno(alumno: ".$alumno->documento." ".$alumno->last_name." ".$alumno->first_name.") tutor<br>";
                                           }   
                                    }
                        }

                    }
                    else
                    {
                        $pass_tutor= $this->crear_pass($tutor_t->dni);
                            $active = 1; 
                            $pago = 1;
                            if($tutor_t->email == null)
                            {
                                $email = "";
                            }
                            else
                            {
                                $email = $tutor_t->email;
                            }
                            
                            $insert_tutor = "INSERT INTO users (first_name, last_name, active, email, password, pago, documento)
                                                VALUES ('$tutor_t->nombre', '$tutor_t->apellido',$active,'$email', '$pass_tutor',1, '$tutor_t->dni')";

                            if($this->db->query($insert_tutor))
                            {
                               $id_tutor = $this->db->insert_id();
                               echo "ok Padre nuevo Insertado <br>";
                               //-- USERS_GRUPS Insert ---
                                $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id)
                                                        VALUES ($id_tutor, $group_id, $colegio_id)";
                                if($this->db->query($insert_users_groups))
                                {
                                    $id_user_group = $this->db->insert_id();
                                }    
                   
                               //--- TUTORALUMNO insert
                                $table = 'tutoralumno';
                                
                                    $insert_relacion = "INSERT INTO tutoralumno (alumno_id,tutor_id)
                                    VALUES ($id_alumno,$id_tutor)";

                                   if($this->db->query($insert_relacion))
                                   {
                                    echo "ok Padre: (".$tutor_t->dni." ".$tutor_t->apellido." ".$tutor_t->nombre.") Insertado en la Relacion alumno_(".$alumno->documento." ".$alumno->last_name." ".$alumno->first_name.") _tutor<br>";
                                   }
                                
                                else
                                {
                                   echo "YaExiste-R-P-A <br>";
                                }   
                            
                            }
                            else
                            {
                              echo "PadreYaInsertado <br>";
                            }
                    }
                }

                    
                
            }

        }
    }
    public function tutor_modelo_2017()
    {
        $colegio_id = 1;
        $group_id = 5;
        /*$tabla_alumnos = "st_alu_6_anio_b";
        $sql = "SELECT * FROM ".$tabla_alumnos;
        $query_alumnos = $this->db->query($sql);*/

        $tabla_tutores = "tutores_varios";
        $sql2 = "SELECT * FROM ".$tabla_tutores;
        $query_tutores = $this->db->query($sql2);

        /*if($query_tutores->num_rows() > 0)
        {
            //print_r($query_tutores->num_rows())."<br>";
            foreach ($query_tutor->result() as $Tutor)
            {

            }
        }*/
        if($query_tutores->num_rows() > 0)
        {
            //print_r($query_alumnos->num_rows())."<br>";
            foreach ($query_tutores->result() as $tutor_t)
            {
                $email = $tutor_t->username.'@'.$tutor_t->aux;

                $sql_tutor_aux = "SELECT * FROM users WHERE email = '$email'";
                $sql_tutor_aux = $this->db->query($sql_tutor_aux);
                if($sql_tutor_aux->num_rows() > 0)
                {
                    echo "YaExiste TUTOR:".$tutor_t->apellido.", ".$tutor_t->nombre." <br>";
                }
                else
                {
                    $pass = $this->crear_pass($tutor_t->username);
                    
                    $insert_tutor = "INSERT INTO users (first_name, last_name, active, email, password, pago)
                                                VALUES ('$tutor_t->nombre', '$tutor_t->apellido',1,'$email', '$pass',1)";

                            if($this->db->query($insert_tutor))
                            {
                               $id_tutor = $this->db->insert_id();
                               echo "ok Insertado <br>";
                               //-- USERS_GRUPS Insert ---
                                $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)
                                                        VALUES ($id_tutor, $group_id, $colegio_id, 1)";
                                if($this->db->query($insert_users_groups))
                                {

                                }
                            }
                            else
                            {
                                echo "NO INSERTADO :".$tutor_t->apellido.", ".$tutor_t->nombre." <br>";
                            }


                }
                

                    
                
            }

        }
    }
    //
    public function importar_alumnos_st()
    {
        //VARIABLES
        $tabla = "st_alu_6_anio_b"; //--modificar por la division
        $division_id = 63;
        $group_id = 46;
        $colegio_id = 5;
        $plan_estudio_id = 1;

        //SCRIPT        
        $sql = "SELECT * FROM ".$tabla;
        $cont = 0;
        $query = $this->db->query($sql);
        if($query->num_rows() > 0)
        {
            foreach ($query->result() as $key => $value) 
            {
                $sql_check= "SELECT * FROM users WHERE documento='".$value->dni."'";
                $query_ch = $this->db->query($sql_check);
                if($query_ch->num_rows() == 0)
                {
                    $pass_alu = $this->crear_pass($value->dni);

                    $insert_alumno = "INSERT INTO users (first_name, last_name, active, documento, sexo, password)
                            VALUES ('$value->nombre', '$value->apellido',1, '$value->dni', '$value->sexo','$pass_alu')";
                                
                   if($this->db->query($insert_alumno))
                   {
                        $id_user_alumno = $this->db->insert_id();                    
                        //insertar en users_groups
                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id)
                                VALUES ($id_user_alumno, $group_id, $colegio_id)";
                        if($this->db->query($insert_users_groups))
                        {
                            //insertar en inscripcion
                            $id_alumno = $this->db->insert_id();
                            $insert_inscripcion = "INSERT INTO inscripciones (alumno_id,activo, planesestudio_id,divisiones_id)
                                    VALUES ($id_alumno,1,$plan_estudio_id,$division_id)";
                                   //echo $insert_inscripcion."<br>";        

                                    if($this->db->query($insert_inscripcion))
                                    {
                                        echo "OK -".$value->dni." ".$value->apellido." ".$value->nombre."<br>";
                                        $cont = $cont + 1;
                                    }else
                                    {
                                      echo "NoInsertoAlumno: ".$value->dni." ".$value->apellido." ".$value->nombre."<br>";
                                      //echo "NoInsertoAlumno: <br>";
                                    }
                        }    
                      }
                } // fin if el alumno ya existe.
                else{
                    echo "ELAlumnoExiste<br>";
                }
                
            } // fin for recorrido de los alumnos.
            echo "se insertaron: ".$cont." alumnos";
        }
    } // fin table importar_alumnos


    
    
    public function crear_pass($pass)
    {
        $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
                    $passApiKey = $this->variables->get_apikey();
                    //$correo = "nodos";
                    $url = $urlApi."/login/prueba_encriptar";
                    $ch = curl_init();
                    $options = array(
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_URL => $url,
                                CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey"),
                                CURLOPT_POST => true,
                                //CURLOPT_POSTFIELDS => json_encode($fields),
                                //CURLOPT_POSTFIELDS => $fields,
                                CURLOPT_POSTFIELDS => array(
                                                            'data' => $pass
                                                    ),  
                                CURLOPT_SSL_VERIFYPEER=> false,
                            );
                      curl_setopt_array( $ch, $options);
                      $response = curl_exec($ch);
                      curl_close($ch);
                     $response = json_decode($response, True);
                    
                     $arre = (array) $response; //convierto el obj en un arreglo.
                     $pass_encriptado = $arre['pass'];
                     return $pass_encriptado;
    }
    public function prueba_convertir()
    {
        
        $consulta = "SELECT * FROM users WHERE id = 3868";
        $consulta = $this->db->query($consulta);
        $consulta = $consulta->row();
        //print_r($consulta->last_name);

        $cadena = $consulta->last_name;
        $cadena_conver = utf8_decode($cadena); 
        echo $cadena_conver;



    }
    public function update_tutores_st_2017()
    {
        $tabla_aux_tutores = "aux_tutores";
        $sql = "SELECT * FROM ".$tabla_aux_tutores;
        $tabla_aux_tutores = $this->db->query($sql);
        foreach ($tabla_aux_tutores->result() as $tutor_aux)
        {
            $documento = $tutor_aux->documento;

            $users = "SELECT * FROM users where documento = '$documento'";
            $query_users = $this->db->query($users);
            if($query_users->num_rows() > 0)
            {
                $users = $query_users->row();
                $update = "UPDATE users 
                            SET users.email = '$tutor_aux->email'
                            WHERE users.id = $users->id";                
                //echo $update."<br>";
                
                if($this->db->query($update))
                {
                    echo "UPDATED Users: ".$documento." <br>";
                }
            }
            else
            {
                echo "NO Existe Users: ".$documento." <br>";
            }
        }

    }
    public function alumno_tutor_st()
    {
        $colegio_id = 5;
        $group_id = 45;
        $tabla_alumnos = "st_alu_6_anio_b";
        $sql = "SELECT * FROM ".$tabla_alumnos;
        $query_alumnos = $this->db->query($sql);

        $tabla_tutores = "tutor_alumno_st";
        $sql2 = "SELECT * FROM ".$tabla_tutores;
        $query_tutores = $this->db->query($sql2);

        /*if($query_tutores->num_rows() > 0)
        {
            //print_r($query_tutores->num_rows())."<br>";
            foreach ($query_tutor->result() as $Tutor)
            {

            }
        }*/
        if($query_alumnos->num_rows() > 0)
        {
            //print_r($query_alumnos->num_rows())."<br>";
            foreach ($query_alumnos->result() as $alumno)
            {
                
                $sql_alumno_aux = "SELECT * FROM users WHERE documento = $alumno->dni";
                $query_alumno_aux = $this->db->query($sql_alumno_aux);
                $alumno_aux = $query_alumno_aux->row();
                
                //print_r($alumno_aux->id);

                //die();
                $id_alumno = $alumno_aux->id;
                foreach ($query_tutores->result() as $tutor)
                {
                    if($tutor->dni_alu == $alumno->dni)//buscar en tutor_alumno el alumno
                    {
                        //echo "encontrado";
                        //validar si existe en users el tutor
                        $sql_users = "SELECT * FROM users WHERE documento = $tutor->dni_tutor";
                  
                        $query_users = $this->db->query($sql_users);
                        if($query_users->num_rows() > 0)
                        {
                            foreach ($query_users->result() as $user)
                            {
                                echo "tutor encontrado";
                                //ver si es padre esta en el mismo colegio
                                $sql_users_group_colegio = "SELECT * FROM users 
                                                            JOIN users_groups on users_groups.user_id =  users.id 
                                                            WHERE users.documento = $tutor->dni_tutor and users_groups.colegio_id = $colegio_id";
                                $query_users_groups = $this->db->query($sql_users_group_colegio);
                                if($query_users_groups->num_rows() > 0)//existe en el colegio
                                {
                                    //verificar que no exista la relacion en tutoralumno
                                    $sql_tutor_alumno_aux = "SELECT * FROM tutoralumno where alumno_id = $id_alumno and tutor_id = $user->id";
                                    $query_tutor_alumno_aux = $this->db->query($sql_tutor_alumno_aux);
                                    if($query_tutor_alumno_aux->num_rows() > 0)
                                    {
                                        echo "Existe Relacion alumno tutor (user_id: ".$user->id.")<br>";
                                    }
                                    else
                                    {
                                        $table = 'tutoralumno';
                                    
                                        $insert_relacion = "INSERT INTO tutoralumno (alumno_id,tutor_id)
                                        VALUES ($id_alumno,$user->id)";

                                       if($this->db->query($insert_relacion))
                                       {
                                        echo "ok Padre(".$tutor->dni_tutor." ".$tutor->apellido_tutor." ".$tutor->nombre_tutor.") Insertado en la Relacion alumno(".$alumno->dni." ".$tutor->apellido_alu." ".$tutor->nombre_alu.") tutor<br>";
                                       }
                                    }
                                    //--- TUTORALUMNO insert
                                    
                                    
                                }
                                else
                                {
                                    
                                    $sql_tutor_alumno_aux = "SELECT * FROM tutoralumno where alumno_id = $id_alumno and tutor_id = $user->id";
                                    $query_tutor_alumno_aux = $this->db->query($sql_tutor_alumno_aux);
                                    if($query_tutor_alumno_aux->num_rows() > 0)
                                    {
                                        echo "Existe Relacion alumno tutor <br>";
                                    }
                                    else
                                    {
                                        //-- USERS_GRUPS Insert ---
                                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id)
                                                                VALUES ($user->id, $group_id, $colegio_id)";
                                        if($this->db->query($insert_users_groups)){
                                            $id_user_group = $this->db->insert_id();
                                        } 
                                        //--- TUTORALUMNO insert
                                        $table = 'tutoralumno';
                                        
                                            $insert_relacion = "INSERT INTO tutoralumno (alumno_id,tutor_id)
                                            VALUES ($id_alumno,$user->id)";

                                           if($this->db->query($insert_relacion))
                                           {
                                            echo "ok Padre(".$tutor->dni_tutor." ".$tutor->apellido_tutor." ".$tutor->nombre_tutor.") Insertado en la Relacion alumno(".$alumno->dni." ".$tutor->apellido_alu." ".$tutor->nombre_alu.") tutor<br>";
                                           }   
                                    }
                                    
                                    

                                    //die();
                                }

                                
                            }
                        }
                        else
                        {
                            $pass_tutor= $this->crear_pass($tutor->dni_tutor);
                            $active = 1; 
                            $pago = 1;
                            if($tutor->mail == null)
                            {
                                $email = "";
                            }
                            else
                            {
                                $email = $tutor->mail;
                            }
                            if($tutor->padre_madre == 'Padre')
                            {
                                $sexo_tutor = "M";
                            }
                            else
                            {
                                $sexo_tutor = "F";
                            }
                            $insert_tutor = "INSERT INTO users (first_name, last_name, active, email, password, sexo, pago, documento)
                                                VALUES ('$tutor->nombre_tutor', '$tutor->apellido_tutor',$active,'$email', '$pass_tutor', '$sexo_tutor',1, '$tutor->dni_tutor')";
                            
                            if($this->db->query($insert_tutor))
                            {
                               $id_tutor = $this->db->insert_id();
                               echo "ok Padre nuevo Insertado <br>";
                               //-- USERS_GRUPS Insert ---
                                $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id)
                                                        VALUES ($id_tutor, $group_id, $colegio_id)";
                                if($this->db->query($insert_users_groups))
                                {
                                    $id_user_group = $this->db->insert_id();
                                }    
                   
                               //--- TUTORALUMNO insert
                                $table = 'tutoralumno';
                                
                                    $insert_relacion = "INSERT INTO tutoralumno (alumno_id,tutor_id)
                                    VALUES ($id_alumno,$id_tutor)";

                                   if($this->db->query($insert_relacion))
                                   {
                                    echo "ok Padre(".$tutor->dni_tutor." ".$tutor->apellido_tutor." ".$tutor->nombre_tutor.") Insertado en la Relacion alumno(".$alumno->dni." ".$tutor->apellido_alu." ".$tutor->nombre_alu.") tutor<br>";
                                   }
                                
                                else
                                {
                                   echo "YaExiste-R-P-A <br>";
                                }   
                            
                            }
                            else
                            {
                              echo "PadreYaInsertado <br>";
                            }
                            
                        }
                        //print_r($this->db->last_query());
                        //die();

                    }
                
                }
            }

        }
    }
    public function eliminar_listausuarios()
    {
        $tabla1="users";
        $tabla2="users_groups";

        $sql1= "SELECT * FROM `users`WHERE funcion in(3608,3609,3610,3611)";
        $result1=$this->db->query($sql1);
        foreach ( $result1->result() as $key) {
            // echo $key->id." --- ".$key->last_name;
            $borrausergroup="DELETE FROM $tabla2 WHERE user_id=$key->id";
            $queryborra1=$this->db->query($borrausergroup);
            $borraruser="DELETE FROM $tabla1 WHERE id=$key->id";
            $queryborra2=$this->db->query($borraruser);
        }

    }
    public function agregargroups( $idcolegio)
    {   
       ;
        $tabla1= "groups";

        $groups1=array(
            'id'          => NULL , 
            'name'        => "FEDERACIÓN", 
            'description' => "" , 
            'orden'       => 2, 
            'id_colegio'  => $idcolegio, 
            'aliasM'      => "", 
            'aliasF'      => "", 
            'estado'      => 1, 
        );/*$groups2=array(
            'id'          => NULL , 
            'name'        => "DEPORTISTA", 
            'description' => "", 
            'orden'       => 6, 
            'id_colegio'  => $idcolegio, 
            'aliasM'      => "", 
            'aliasF'      => "", 
            'estado'      => 1, 
        );$groups3=array(
            'id'          => NULL , 
            'name'        => "SECRETARÍA", 
            'description' => "", 
            'orden'       => 3, 
            'id_colegio'  => $idcolegio, 
            'aliasM'      => "", 
            'aliasF'      => "", 
            'estado'      => 1, 
        );$groups4=array(
            'id'          => NULL , 
            'name'        => "NODOS", 
            'description' => "", 
            'orden'       => 0, 
            'id_colegio'  => $idcolegio, 
            'aliasM'      => "", 
            'aliasF'      => "", 
            'estado'      => 1, 
        );*/
        
      $this->db->insert( $tabla1, $groups1);
     /* $this->db->insert( $tabla1, $groups2);
      $this->db->insert( $tabla1, $groups3);
      $this->db->insert( $tabla1, $groups4);*/
    }
    public function cargagroupsxcolegio($colegio)
    {   
    $band=0;

       
        $sql1="SELECT * FROM `groups`";
        $query1=$this->db->query($sql1);
       
        //print_r($query1->result());
       foreach ($query1->result() as $key) 
         {
            
           if ($key->id_colegio==$colegio) 
           {
            if ($band==0) {

                 $this->agregargroups($colegio);
                $band=1;
                echo "string";
            }
            

           }
             
        }
         
            
         
        
     }
     public  function guardarcolerol()
     {
        $sql="SELECT * FROM `nodocolegio`";
        $query=$this->db->query($sql);
        foreach ($query->result() as $key) {
           $this->cargagroupsxcolegio($key->id);
        }
     } 
     public  function guardarmenuaccion()
     {
       $sql="SELECT * FROM `groups`"; 
       $query=$this->db->query($sql);
       foreach ($query->result() as $key) {
        /*print_r($key->name);
        die();*/
           /*if ($key->name=="DEPORTISTA") {
               $idgroup=$key->id;
               $club=$key->id_colegio;
               $arre = array(
                             'colegio_id'        => $club,
                             'group_id'          => $idgroup,
                             'roles_id'          => "3,4,5,6,7,8,10",
                             'roles_eventos'     => "",
                             'supervisado'       => "",
                             'id_menus_acciones' => "1,2,4,6,7",
                             'div_todas'         => 0
                              );
               $this->db->insert('reglas_comunicacion', $arre);
           }else
           if ($key->name=="JUEZ") {
               $idgroup=$key->id;
               $club=$key->id_colegio;
               $arre = array(
                             'colegio_id'        => $club,
                             'group_id'          => $idgroup,
                             'roles_id'          => "",
                             'roles_eventos'     => "",
                             'supervisado'       => "",
                             'id_menus_acciones' => "1,2,4,5,6,7",
                             'div_todas'         => ""
                              );
               $this->db->insert('reglas_comunicacion', $arre);
           }else*/
           if ($key->name=="FEDERACIÓN") {
               $idgroup=$key->id;
               $club=$key->id_colegio;
               $arre = array(
                             'colegio_id'        => $club,
                             'group_id'          => $idgroup,
                             'roles_id'          => "7,6,8,4,76,9,3,2,5",
                             'roles_eventos'     => "",
                             'supervisado'       => "",
                             'id_menus_acciones' => "1,2,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,36,48,49,50",
                             'div_todas'         => ""
                              );
               $this->db->insert('reglas_comunicacion', $arre);
               echo "string";
           }/*else
           if ($key->name=="SECRETARÍA") {
               $idgroup=$key->id;
               $club=$key->id_colegio;
               $arre = array(
                             'colegio_id'        => $club,
                             'group_id'          => $idgroup,
                             'roles_id'          => "",
                             'roles_eventos'     => "",
                             'supervisado'       => "",
                             'id_menus_acciones' => "1,2,4,5,6,7",
                             'div_todas'         => ""
                              );
               $this->db->insert('reglas_comunicacion', $arre);
           }else
           if ($key->name=="CALCULISTA") {
               $idgroup=$key->id;
               $club=$key->id_colegio;
               $arre = array(
                             'colegio_id'        => $club,
                             'group_id'          => $idgroup,
                             'roles_id'          => "",
                             'roles_eventos'     => "",
                             'supervisado'       => "",
                             'id_menus_acciones' => "1,2,4,5,6,7",
                             'div_todas'         => ""
                              );
               $this->db->insert('reglas_comunicacion', $arre);
           } 
           if ($key->name=="CALCULISTA") {
               $idgroup=$key->id;
               $club=$key->id_colegio;
               $arre = array(
                             'colegio_id'        => $club,
                             'group_id'          => $idgroup,
                             'roles_id'          => "",
                             'roles_eventos'     => "",
                             'supervisado'       => "",
                             'id_menus_acciones' => "1,2,4,5,6,7",
                             'div_todas'         => ""
                              );
               $this->db->insert('reglas_comunicacion', $arre);
           }*/
       }

     }
}