<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_division extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }


        public function insertar_nododivision($data)
        {
            $query = $this->db->insert("divisiones",$data);
            return $query;
        }


        public function get_division($idanio,$nombre, $planA)
        {
            $sql = "SELECT *
                    FROM divisiones
                    WHERE anios_id='$idanio' 
                    AND nombre='$nombre'
                    AND planesestudio_id = $planA
                    ";

            $query = $this->db->query($sql);
            return $query->result();
        }
        public function get_division_x_especialidad($idesp)
        {
            $sql = "SELECT *
                    FROM divisiones
                    WHERE especializacion_id ='$idesp' 
                   
                    ";

            $query = $this->db->query($sql);
            return $query->result();
        }
         public function get_eficiencia_x_division($iddiv)
        {   
            $sql = "SELECT * FROM eficiencia WHERE id_divisional = ".$iddiv;
            //print_r($sql);
            $query = $this->db->query($sql);
            return $query->result();
        }


        public function update_estado_division($divisionid, $estadoNew) 
        {
            $sql = "UPDATE divisiones SET estado='$estadoNew' WHERE id=$divisionid ";
            $query = $this->db->query($sql);
            return $query;
        }

        public function obtener_division_id($id)
        {
          $sql = "SELECT divisiones.nombre as nombdiv, anios.nombre as nombanio
                    FROM divisiones
                    JOIN anios ON anios.id = divisiones.anios_id
                    WHERE divisiones.id='$id'
                    ";

            $query = $this->db->query($sql);
            return $query->row();
        }


        /*public function divisionesXdocentes($colegio_id, $planA, $userid)
        {
            $sql="SELECT divisiones_id
                  FROM horariosmaterias
                  JOIN users_groups
                  WHERE planesestudio_id = '$planA'
                  AND horariosmaterias.docente_id = users_groups.id
                  AND users_groups.colegio_id = $colegio_id  
                  AND  users_groups.user_id IN ($userid)
                  GROUP BY divisiones_id
                 ";
            $query = $this->db->query($sql);
            return $query->result();
        }*/
        public function divisionesXdocentes($colegio_id, $planA, $userid)
        {
            $sql="SELECT  nodo_doc_div_mat_alu.id_division as divisiones_id
                  FROM nodo_doc_div_mat_alu
                  JOIN users_groups on users_groups.id = nodo_doc_div_mat_alu.id_docente
                  join nodocolegio on nodocolegio.id = nodo_doc_div_mat_alu.id_colegio
                  WHERE nodo_doc_div_mat_alu.ciclo_lectivo =  nodocolegio.cicloa
                  AND nodo_doc_div_mat_alu.id_docente = users_groups.id
                  AND users_groups.colegio_id = $colegio_id  
                  AND  users_groups.user_id IN ($userid) and nodo_doc_div_mat_alu.id_alumno = 0
                  GROUP BY divisiones_id
                 ";
            $query = $this->db->query($sql);
            return $query->result();
        }

        public function divisionesXpreceptores($idcolegio, $planA, $userid)
        {
            $sql="SELECT preceptores.division_id
                  FROM preceptores
                  JOIN users_groups ON  preceptores.user_id = users_groups.id
                  WHERE planesestudio_id = $planA
                  AND users_groups.colegio_id = $idcolegio 
                  AND users_groups.user_id  IN ($userid)
                  AND preceptores.activo=1
                  GROUP BY division_id
                 ";
            $query = $this->db->query($sql);
            return $query->result();
        }

        public function get_divisionesXanios($idsAnios, $planEst)
        {
            $sql = "SELECT *
                    FROM divisiones
                    WHERE anios_id IN ($idsAnios)
                    AND planesestudio_id = $planEst
                    AND estado=1
                    GROUP BY id
                    ";

            $query = $this->db->query($sql);           
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }

        public function get_divisionesXNiveles($idsNiveles, $planEst)
        {
            $sql = "SELECT divisiones.*
                    FROM divisiones
                    JOIN nodoanios ON nodoanios.id = divisiones.anios_id
                    WHERE nodoanios.nodoniv_id IN ($idsNiveles)
                    AND divisiones.planesestudio_id = $planEst
                    AND divisiones.estado=1
                    GROUP BY divisiones.id
                    ";

            $query = $this->db->query($sql);           
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }


      public function get_divisiones_ConEspecialidad($idcolegio, $anioA) //$anioA = id de plan de estudio
      {           
          $sql ="SELECT nodoanios.id as nodoanios_id,niveles.nombre as nombre_nivel,especialidades.nombre as nombre_especialidad,anios.nombre as nombre_anio, divisiones.nombre as nombre_division, divisiones.id as division_id
              FROM `nodoanios` 
                JOIN divisiones ON nodoanios.id = divisiones.anios_id 
                JOIN anios ON anios.id= nodoanios.anio_id 
                JOIN nodonivel ON nodonivel.id= nodoanios.nodoniv_id 
                JOIN niveles ON niveles.id = nodonivel.niveles_id 
                JOIN especialidades ON especialidades.id = nodonivel.esp_id
                WHERE nodonivel.nodocolegio_id= $idcolegio 
                AND nodoanios.cicloa_id = $anioA 
                AND  nodonivel.estado = 1 
          "; 
            
          $query = $this->db->query($sql);           
          if($query->num_rows()>0){
              return $query->result();
          }
          else{
              return false;
          }
      }
    
    //-------------------------------------------------------------
  
      public function get_divisiones_SinEspecialidad($idcolegio, $anioA) //$anioA = id de plan de estudio
      {
          $sql ="SELECT nodoanios.id as nodoanios_id,niveles.nombre as nombre_nivel,anios.nombre as nombre_anio, divisiones.nombre as nombre_division, divisiones.id as division_id
                FROM `nodoanios` 
                JOIN divisiones ON nodoanios.id = divisiones.anios_id 
                JOIN anios ON anios.id= nodoanios.anio_id 
                JOIN nodonivel ON nodonivel.id= nodoanios.nodoniv_id 
                JOIN niveles ON niveles.id = nodonivel.niveles_id 
                WHERE nodonivel.nodocolegio_id= $idcolegio 
                AND nodoanios.cicloa_id = $anioA 
                AND  nodonivel.estado = 1 
                AND nodonivel.esp_id IS null
                ";
        
          $query = $this->db->query($sql);           
          if($query->num_rows()>0){
              return $query->result();
          }
          else{
              return false;
          }
      }

    //-------------------------------------------------------------
  
      public function get_divisiones_SinEspecialidad_Xdivision($idcolegio, $divisiones,$anioA)
      {
          $sql ="SELECT nodoanios.id as nodoanios_id,niveles.nombre as nombre_nivel,anios.nombre as nombre_anio, divisiones.nombre as nombre_division, divisiones.id as division_id
                FROM `nodoanios` 
                JOIN divisiones ON nodoanios.id = divisiones.anios_id 
                JOIN anios ON anios.id= nodoanios.anio_id 
                JOIN nodonivel ON nodonivel.id= nodoanios.nodoniv_id 
                JOIN niveles ON niveles.id = nodonivel.niveles_id 
                WHERE nodonivel.nodocolegio_id= $idcolegio 
                AND nodoanios.cicloa_id = $anioA 
                AND  nodonivel.estado = 1 
                AND nodonivel.esp_id IS null
                AND divisiones.id IN ($divisiones)
                ";
        
          $query = $this->db->query($sql);           
          if($query->num_rows()>0){
              return $query->result();
          }
          else{
              return false;
          }
      }

      public function get_divisiones_ConEspecialidad_Xdivision($idcolegio, $divisiones, $anioA)
      {           
          $sql ="SELECT nodoanios.id as nodoanios_id,niveles.nombre as nombre_nivel,especialidades.nombre as nombre_especialidad,anios.nombre as nombre_anio, divisiones.nombre as nombre_division, divisiones.id as division_id
              FROM `nodoanios` 
                JOIN divisiones ON nodoanios.id = divisiones.anios_id 
                JOIN anios ON anios.id= nodoanios.anio_id 
                JOIN nodonivel ON nodonivel.id= nodoanios.nodoniv_id 
                JOIN niveles ON niveles.id = nodonivel.niveles_id 
                JOIN especialidades ON especialidades.id = nodonivel.esp_id
                WHERE nodonivel.nodocolegio_id= $idcolegio 
                AND nodoanios.cicloa_id = $anioA 
                AND  nodonivel.estado = 1 
                AND divisiones.id IN ($divisiones) 
          ";
            
          $query = $this->db->query($sql);           
          if($query->num_rows()>0){
              return $query->result();
          }
          else{
              return false;
          }
      }


      /*public function get_division_new($id) //se usa para el pase de anio
      {
          $sql = "SELECT *
                  FROM divisiones 
                  WHERE nododivision = $id
                  AND estado=1   
                  ";
          $query = $this->db->query($sql);
          
          if($query->num_rows()>0){
              return $query->result();
          }
          else{
              return false;
          }    
      }
    */

      public function get_division_datos($datos)
      {
          $query = $this->db->get_where('divisiones', $datos);
          
          if($query->num_rows()>0){
              return $query->result();
          }
          else{
              return false;
          }    
      }
      

      public function obtener_division_anio_nivel($id)
      {
        /*$sql = "SELECT divisiones.id,divisiones.nombre, divisiones.div_siguiente,
                  nodoanios.id as nodoanio_id,
                  nodoanios.anio_id,
                  nodoanios.ultimo as ultimo_anio,
                  nodoanios.cicloa_id,

                  nodonivel.id as nodoniv_id,
                  nodonivel.nodocolegio_id,
                  nodonivel.ultimo as ultimo_niv,

                  nodonivel.niveles_id,
                  niveles.orden as orden_niv
                  FROM divisiones
                  JOIN nodoanios ON nodoanios.id = divisiones.anios_id
                  JOIN nodonivel ON nodonivel.id = nodoanios.nodoniv_id
                  JOIN niveles ON niveles.id = nodonivel.niveles_id
                  WHERE divisiones.id='$id'
                  "; */

          $sql = "SELECT divisiones.id,divisiones.nombre, divisiones.div_siguiente,
                  nodoanios.id as nodoanio_id,
                  nodoanios.anio_id,
                  nodoanios.ultimo as ultimo_anio,
                  nodoanios.cicloa_id,

                  nodonivel.id as nodoniv_id,
                  nodonivel.nodocolegio_id,
                  nodonivel.ultimo as ultimo_niv,

                  nodonivel.niveles_id
                 
                  FROM divisiones
                  JOIN nodoanios ON nodoanios.id = divisiones.anios_id
                  JOIN nodonivel ON nodonivel.id = nodoanios.nodoniv_id
                  WHERE divisiones.id='$id'
                  "; 

          $query = $this->db->query($sql);
          if($query->num_rows()>0){
              return $query->row();
          }
          else{
              return false;
          }
      }


      public function get_divi_sig($nodoanio, $ciclo) //se usa para pase de anio
      { 
          $sql = "SELECT * FROM divisiones
                  WHERE anios_id = $nodoanio  
                  AND planesestudio_id = $ciclo
                  LIMIT 1  
                  ";
          $query = $this->db->query($sql);
          
          if($query->num_rows()>0){
              return $query->row();
          }
          else{
              return false;
          }    
      }
      public function get_divisiones_x_evento_ConEspecialidad($idcolegio, $anioA,$idevento) //$anioA = id de plan de estudio
      {           
          $sql ="SELECT nodoanios.id as nodoanios_id,niveles.nombre as nombre_nivel,especialidades.nombre as nombre_especialidad,anios.nombre as nombre_anio, divisiones.nombre as nombre_division, divisiones.id as division_id
              FROM `nodoanios` 
                JOIN divisiones ON nodoanios.id = divisiones.anios_id 
                JOIN anios ON anios.id= nodoanios.anio_id 
                JOIN nodonivel ON nodonivel.id= nodoanios.nodoniv_id 
                JOIN niveles ON niveles.id = nodonivel.niveles_id 
                JOIN especialidades ON especialidades.id = nodonivel.esp_id
                JOIN inscripciones on inscripciones.divisiones_id = divisiones.id
                JOIN users_groups on users_groups.id = inscripciones.alumno_id
                JOIN eventosusers on eventosusers.destinatario = users_groups.user_id
                WHERE nodonivel.nodocolegio_id= $idcolegio 
                AND nodoanios.cicloa_id = $anioA 
                AND  nodonivel.estado = 1 
                and eventosusers.eventos_id = $idevento 
                and inscripciones.cicloa = 2019
                GROUP BY divisiones.id
          "; 
            
          $query = $this->db->query($sql);           
          if($query->num_rows()>0){
              return $query->result();
          }
          else{
              return false;
          }
      }
    
    //-------------------------------------------------------------
  
      public function get_divisiones_x_evento_SinEspecialidad($idcolegio, $anioA,$idevento) //$anioA = id de plan de estudio
      {
          $sql ="SELECT nodoanios.id as nodoanios_id,niveles.nombre as nombre_nivel,anios.nombre as nombre_anio, divisiones.nombre as nombre_division, divisiones.id as division_id
                FROM `nodoanios` 
                JOIN divisiones ON nodoanios.id = divisiones.anios_id 
                JOIN anios ON anios.id= nodoanios.anio_id 
                JOIN nodonivel ON nodonivel.id= nodoanios.nodoniv_id 
                JOIN niveles ON niveles.id = nodonivel.niveles_id
                JOIN inscripciones on inscripciones.divisiones_id = divisiones.id
                JOIN users_groups on users_groups.id = inscripciones.alumno_id
                JOIN eventosusers on eventosusers.destinatario = users_groups.user_id 
                WHERE nodonivel.nodocolegio_id= $idcolegio 
                AND nodoanios.cicloa_id = $anioA 
                AND  nodonivel.estado = 1 
                AND nodonivel.esp_id IS null 
                and eventosusers.eventos_id = $idevento 
                and inscripciones.cicloa = 2019
                  GROUP BY divisiones.id
                ";
        
          $query = $this->db->query($sql);           
          if($query->num_rows()>0){
              return $query->result();
          }
          else{
              return false;
          }
      }
      public function obtener_docentes_x_div_x_colegio($id_division,$id_colegio, $id_materia)
      {
          $sql = "SELECT nodo_doc_div_mat_alu.*, users.id as user_id, users.first_name, users.last_name, users.documento FROM nodo_doc_div_mat_alu JOIN users_groups on users_groups.id = nodo_doc_div_mat_alu.id_docente JOIN users on users.id = users_groups.user_id join nodocolegio on nodocolegio.id = nodo_doc_div_mat_alu.id_colegio where nodo_doc_div_mat_alu.id_division = $id_division and nodo_doc_div_mat_alu.id_colegio = $id_colegio and nodocolegio.cicloa = nodo_doc_div_mat_alu.ciclo_lectivo and nodo_doc_div_mat_alu.id_alumno = 0 and nodo_doc_div_mat_alu.id_materia = $id_materia";
          $query = $this->db->query($sql); 
          return $query; 
      }
      

	}