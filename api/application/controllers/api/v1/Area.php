<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';


    class Area extends REST_Controller {

    function __construct()
    {
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }

        parent::__construct();
        $this->load->model("modelo_areas");

    }


    public function obtener_areas_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $idcolegio = $this->post('idcolegio');
            $idgrupo = $this->post('idgrupo');
            $nombregrupo = $this->post('nombregrupo');


            $this->load->model("modelo_usuario");

            $data0 = array(
                'colegio_id'=>$idcolegio,
                'user_id'=>$userid,
                'group_id'=>$idgrupo,
            );
            $usergroup = $this->modelo_usuario->verificar_user_group($data0);


            switch ($nombregrupo)
            {
                case 'Jefe Area':
                            $data = array(
                                'colegio_id'=>$idcolegio,
                                'nivel_id'=>null,
                                'jefe_id'=>$usergroup->id,
                            );
                            $result = $this->modelo_areas->obtener_areas($data);
                            break;

                case 'Regente Basico':
                           $data3 = array(
                                'colegio_id'=>$idcolegio,
                                'nivel_id'=>1,
                                'jefe_id'=>$usergroup->id,
                            );
                            $result2 = $this->modelo_areas->obtener_areas($data3);
                            //print_r($result2);
                            $data = array(
                                'colegio_id'=>$idcolegio,
                                'nivel_id'=>null,
                                'area_padre'=>$result2[0]->id,
                            );
                            $result = $this->modelo_areas->obtener_areas($data);
                            break;

                case 'Regente Orientado':
                            $data3 = array(
                                'colegio_id'=>$idcolegio,
                                'nivel_id'=>2,
                                'jefe_id'=>$usergroup->id,
                            );
                            $result2 = $this->modelo_areas->obtener_areas($data3);
                            $data = array(
                                'colegio_id'=>$idcolegio,
                                'nivel_id'=>null,
                                'area_padre'=>$result2[0]->id,
                            );
                            $result = $this->modelo_areas->obtener_areas($data);
                            break;

                case 'Docente':
                            $anioA = $this->utilidades->obtener_planestudioaXcolegio( $idcolegio );
                            $planest_Id = $this->utilidades->get_planestudio( $anioA );
                            $result = $this->modelo_areas->obtener_areas_By_docente($usergroup->id,$planest_Id);
                            break;

                default:
                        $data = array(
                            'colegio_id'=>$idcolegio,
                            'nivel_id'=>null, //no traer las regencias
                        );

                        $result = $this->modelo_areas->obtener_areas($data);
                        break;
            }



            if ($result)
            {
                $arre = array(
                    'status' => 1,
                    'result' => $result,
                );
                $data = json_encode($arre);
                $this->response($data, REST_Controller::HTTP_OK);
            }
            else
            {
                /*$this->response([
                    'status' => FALSE,
                    'message' => 'no_areas'
                ], REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code*/

                $arre = array(
                    'status' => 0,
                    'message' => 'no_areas'
                );
                $data = json_encode($arre);
                $this->response($data, REST_Controller::HTTP_OK);
            }
        }
    }

    public function obtener_materias_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $this->load->model("modelo_usuario");

            $idcolegio = $this->post('idcolegio');
            $idgrupo = $this->post('idgrupo');
            $nombregrupo = $this->post('nombregrupo');

            $areas = $this->post('areas');
            //print_r($areas); echo "---";
            $areas = json_decode($areas);
            //print_r($areas); echo "---";


            if(count($areas)>1)
                $idsAreas = join(',',$areas);
            else $idsAreas = $areas[0];

            //print_r($idsAreas); die();

            $data0 = array(
                'colegio_id'=>$idcolegio,
                'user_id'=>$userid,
                'group_id'=>$idgrupo,
            );
            $usergroup = $this->modelo_usuario->verificar_user_group($data0);

            $anioA = $this->utilidades->obtener_planestudioaXcolegio( $idcolegio );
            $planest_Id = $this->utilidades->get_planestudio( $anioA );

            //echo $nombregrupo;
            //print_r( $usergroup );
            if($nombregrupo === 'Docente')
                $result = $this->modelo_areas->obtener_materias_By_docente($idsAreas, $usergroup->id,$planest_Id);
            else
                $result = $this->modelo_areas->obtener_materias_By_areas($idsAreas,$planest_Id);


            if ($result)
            {
                $arre = array(
                    'status' => TRUE,
                    'result' => $result,
                );
                $data = json_encode($arre);
                $this->response($data, REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'no_areas'
                ], REST_Controller::HTTP_OK);
            }
        }
    }

    /*public function materia_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
                //$data = json_decode($_POST['data']) ;
                $materiaName = $this->post('materiaName');
                if($materiaName){

                    $result0 = $this->modelo_materias->obtener_materia($materiaName);
                    if (!$result0) // para NO insertar filas repetidas
                    {
                        $data = array(
                            'nombre'=>$materiaName,
                        );
                        $result = $this->modelo_materias->insertar_materia($data);
                        if ($result)
                        {
                            $ultimoId=$this->db->insert_id();
                            $this->response([
                                    'status' => TRUE,
                                    'message' => 'alta_ok',
                                    'id' => $ultimoId
                                ], REST_Controller::HTTP_OK);
                        }
                        else
                            {
                                $this->response([
                                    'status' => FALSE,
                                    'message' => 'error_alta'
                                ]); // NOT_FOUND (404) being the HTTP response code
                            }
                    }
                    else
                        {
                            $this->response([
                                'status' => FALSE,
                                'message' => 'existe'
                            ]); // NOT_FOUND (404) being the HTTP response code
                        }

                }
                else
                    {
                        $this->response([
                            'status' => FALSE,
                            'message' => 'vacio'
                        ]); // NOT_FOUND (404) being the HTTP response code
                    }

        }
    }


    public function materias_anio_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
                $colegioId = $this->post('colegioId');
                $nodoanioId = $this->post('nodoanioId');

                $materiasId = json_decode($_POST['materiasId']) ;



                if (count($materiasId)>0)
                {
                    //PASO 1
                    //materias que ya tiene asignadas el anio
                    $resultmaterias = $this->modelo_materias->obtener_aniosmaterias($colegioId, $nodoanioId);
                    $materiasAct = array();
                    //print_r($resultmaterias);
                    if($resultmaterias)
                        foreach ($resultmaterias as $key)
                            $materiasAct[] = $key->idmateria;


                    $planest= $this->utilidades->get_planestudio( date('Y') );


                    //PASO 2; inserto las materias nuevas
                    for ($i=0; $i < count($materiasId); $i++) {
                        $mateId = $materiasId[$i];

                        $clave = array_search($mateId, $materiasAct);

                        if(!$clave) {//solo inserta si no encuentra

                            $data = array(
                                'nodoanios_id'=>$nodoanioId,
                                'nombresmateria_id'=>$mateId,
                                'colegio_id'=>$colegioId,
                                'cicloa'=>$planest,
                            );

                            $data2 = array(
                                'nodoanios_id'=>$nodoanioId,
                                'nombresmateria_id'=>$mateId,
                                'colegio_id'=>$colegioId,
                                'cicloa'=>$planest,
                                'anio_materia'=> 0,
                            );

                            $result = $this->modelo_materias->insertar_aniosmaterias2($data, $data2);
                        }
                    }


                    //PASO 3: borrar (en cascada) los registros que ya no corresponden
                    $materiasBorrar = array_diff($materiasAct, $materiasId);
                    foreach ($materiasBorrar as $value) {
                        $data = array(
                            'nodoanios_id'=>$nodoanioId,
                            'nombresmateria_id'=>$value,
                            'colegio_id'=>$colegioId,
                            'cicloa'=>$planest,
                        );

                        $aniomat = $this->modelo_materias->get_aniomateria($data);
                        if($aniomat)
                        {
                            $data2 = array(
                                'materias_id'=>$aniomat->id,
                            );
                            $horariomat = $this->modelo_materias->get_horariosmateria($data2);
                            if(!$horariomat){ //solo elimina si no tiene horarios cargados
                                $data3 = array(
                                    'id'=>$aniomat->id,
                                );
                                $this->modelo_materias->delete_aniosmaterias($data3);
                            }
                        }
                    }

                    $data = array(
                        'status' => TRUE,
                        'message' => 'Materias asignadas'
                    );

                    $data = json_encode($data);

                    $this->response($data, REST_Controller::HTTP_OK);

                }
                else
                {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'no_materias'
                    ]); // NOT_FOUND (404) being the HTTP response code
                }
        }
    }


    public function delete_aniomateria_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);
        }
        else
        {
            $idaniomat = $this->post('idaniomat');

            $data2 = array(
                'materias_id'=>$idaniomat,
            );

            $horariomat = $this->modelo_materias->get_horariosmateria($data2);

            if(!$horariomat){ //solo elimina si no tiene horarios cargados
                $data3 = array(
                    'id'=>$idaniomat,
                );
                $this->modelo_materias->delete_aniosmaterias($data3);

                $data = array(
                    'status' => TRUE,
                    'message' => 'Materia Eliminada'
                );
                $data = json_encode($data);
                $this->response($data, REST_Controller::HTTP_OK);
            }
            else
                {
                    $data = array(
                        'status' => FALSE,
                        'message' => 'error'
                    );
                    $data = json_encode($data);
                    $this->response($data, REST_Controller::HTTP_OK);
                }
        }
    }*/

}
