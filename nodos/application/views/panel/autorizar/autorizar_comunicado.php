﻿<?include ('application/views/panel/eventosfilesjs.html');?>
<?include ('application/views/panel/funcionesvariasjs.html');?>


<script src="<?=base_url('assets/js/bootstrap.min.js');?>"></script>

<script type="text/javascript">


  //BORRAR

 /*urlApi = '<?php //echo $this->variables->get_urlapi(); ?>';
  passApiKey = '<?php //echo $this->variables->get_apikey(); ?>';
idUser = '<?//echo $idusuario;?>';
function reenviar()
{
    var url = urlApi+"/evento/reenviar_evento2";

    console.log("PROCESANDO...");
    $.ajax({
          url: url,
          type: 'POST',
          headers: {
              'APIKEY' : passApiKey,
              'userid' : idUser,
              'Access-Control-Allow-Origin': '*'
          },
          data:{idevento:5363, pin:105},
          success: function(data){

            console.log(data);

          },
          error: function(response){
              console.log(response);
          }
    });
}*/


$(document).ready(function(){

    //reenviar();

    generar_datatable();

  });

function datepicker()
{
   $.datetimepicker.setLocale('es');


      $('#fechaI2').datetimepicker({
          //dayOfWeekStart : 1,
          format:'d/m/Y H:i',
          step:30,
          lang:'es',
          //mask:true,
          //disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
          //startDate:  '1986/01/05'
      });


      $('#fechaF2').datetimepicker({
          format:'d/m/Y H:i',
          step:30,
          lang:'es',
      });

}

//FUNCTION PARA ACTUALIZAR UN EVENTO
function modificar_evento(datos, idevento, pin, idcole){ //alert(idevento); console.log(datos);

    var urlRequest = '<?=base_url("/index.php/autorizar_eventos/editar_evento_ajax")?>';

    $.ajax({
          url: urlRequest,
          type: 'POST',
          headers: {
           'Access-Control-Allow-Origin': '*'
        },
        data: {data: datos, idevento:idevento, idcolegio:idcole, pin:pin, idsfiles:'', edita_files:0},
        beforeSend: function() {
            $('.modal-footer .btn-success').after("<img id='loadingNivE' src='<?=base_url('assets/images/loading.gif');?>' height='30' width='30' />");
        },
        success: function(data){

              console.log(data);
              var data = jQuery.parseJSON( data );
              $('#loadingNivE').remove();
              if(data['status']===false)
              {  //alert(data['message']);
                  if(data['message'] == 'NO LOGUIN')
                      location.href = "<?php echo base_url('login'); ?>";
                  else  if(data['message'] == 'pininvalido')
                            {    //alert('PIN INVALIDO');
                                bootbox.alert("PIN INVALIDO!", function() { });
                        }
                        else //bootbox.hideAll();
                          $.notify(data['message'], "danger");
              }
              else{

                      bootbox.hideAll();
                      $.notify("Evento Modificado", "success");

                  }
        },

        error: function(response){
            console.log(response);
            $('#loadingNivE').remove();
            bootbox.hideAll();

          }

    });
}


function abrir_dialogDetalle(evento)
{
    if(evento.confirm==1)
        var confirmar='Si';
    else
      var confirmar='No';

    bootbox.dialog({
      backdrop: true,
      title: "Detalle del Comunicado...",
      message: '<div class="row ">'+
            '<div class="col-lg-12">'+
                    '<form class="form-horizontal">'+

                       '<div class="form-group">'+
                        '<div class="col-xs-10">'+
                          '<label class="" id=""  >Titulo: </label> <label class="label control-label" id="titulo" style="white-space:inherit;line-height: 2;" width="100">'+evento.titulo+'</label>'+
                        '</div>'+
                      '</div>'+


                      '<div class="form-group">'+
                        '<div class="col-xs-10">'+
                          '<label class="" id=""  >Descripción: </label><div>'+evento.descripcion+'</div>'+
                        '</div>'+
                      '</div>'+


                      '<div class="form-group">'+
                         '<div class="col-xs-10">'+
                          '<label class="" id="">Fecha de Creacion: </label> <label class="label  control-label" id="fechaCreacion">'+evento.fechaAlta+'</label>'+
                        '</div>'+
                      '</div>'+

                      '<div class="form-group">'+
                        '<div class="col-xs-10">'+
                          '<label class="" id="">Fecha de Inicio: </label> <label class="label  control-label" id="fechaInicio">'+evento.fechaInicio+'</label>'+
                        '</div>'+
                      '</div>'+

                      '<div class="form-group">'+
                       '<div class="col-xs-10">'+
                          '<label class="" id="">Fecha de Fin: </label> <label class="label  control-label" id="fechafin">'+evento.fechaFin+'</label>'+
                        '</div>'+
                      '</div>'+

                      '<div class="form-group">'+
                        '<div class="col-xs-10">'+
                          '<label class="" id="">Lugar: </label> <label class="label  control-label" id="lugar">'+evento.lugar+'</label>'+
                        '</div>'+
                      '</div>'+

                       '<div class="form-group">'+
                       '<div class="col-xs-10">'+
                          '<label class="" id="">Creador: </label> <label class="label  control-label" id="fechafin">'+evento.creador+' - '+evento.creadorrol+'</label>'+
                        '</div>'+
                      '</div>'+

                       '<div class="form-group">'+
                       '<div class="col-xs-10">'+
                          '<label class="" id="">Colegio: </label> <label class="label  control-label" id="fechafin">'+evento.colegio+'</label>'+
                        '</div>'+
                      '</div>'+

                      '<div class="form-group">'+
                        '<div class="col-xs-10">'+
                          '<label class="" id="">Con confirmacion: </label> <label class="label  control-label" id="fechafin">'+confirmar+'</label>'+

                        '</div>'+
                      '</div>'+

                      '<div class="form-group">'+
                        '<div class="col-xs-10">'+
                          '<label class="" id="">Archivos Adjuntos: </label>'+
                          '<ul id="listarchivosD" style="padding-top:4px"></ul>'+
                        '</div>'+
                      '</div>'+

                 '</form>'+
              '</div>  </div> ',
        buttons: {
            success: {
                label: "Aceptar",
                className: "btn-success",
                callback: function (e) {
                        return ;
                }//fin calback
            }//fin success
        },//fin Buttons

        onEscape: function() {return ;},
    });  //FIN DIALOG



    listar_archivos(evento.idevento, 1, 'D'); //D: detalle. debo diferenciar del alta. el 2do paramatro (1) indica que no puede aliminar los archivos.

}

function abrir_dialogDetalle_edit(evento, idcole)
{
    if(evento.confirm==1)
    {
          //$("#confirmYes").prop('checked', true);
          var confirmYes = 'checked';
          var confirmNo = '';
    }
    else
    {
          var confirmYes = '';
          var confirmNo = 'checked';
    }


    bootbox.dialog({
      backdrop: true,
      title: "Detalle del Comunicado...",
      message: //'<div class="row ">'+
            //'<div class="col-lg-12">'+
            '<div class="ibox-content">'+
                    '<div class="row">'+
                    '<form role="form" id="Nevento2" name="Nevento2">'+

                        '<div id="DivDatos">'+
                           //div de la izquierda
                          '<div class="col-sm-7 b-r">'+

                            '<div class="form-group"><label>Título</label> <input type="text" placeholder="Ingrese título" class="form-control" id="titulo" name="titulo" required value="'+evento.titulo+'"/></div>'+

                            '<div class="form-group"><label>Descripción</label> <textarea rows="4" cols="50"  placeholder="Ingrese Descripción" class="form-control " id="desc2" name="desc">'+evento.descripcion+'</textarea></div>'+


                           '<div class="form-group">'+
                                '<label class="" id="">Archivos Adjuntos: </label>'+
                                //'<input type="file" name="files[]" id="filer_input2" multiple="multiple" placeholder="Browse computer" style="color: transparent; max-width:120px" data-toggle="tooltip" title="Seleccione o arrastre hasta 5 archivos" />'+
                                '<ul id="listarchivosD" style="padding-top:4px"></ul>'+
                            '</div>'+

                          '</div>'+

                           //div de la derecha
                          '<div class="col-sm-5">'+

                            '<div class="form-group">'+
                                '<label>Fecha de Inicio</label> <input id="fechaI2" name="fechaI" type="text" placeholder="dd/mm/aaaa hh:mm" value="'+evento.fechaInicio+'" class="form-control"/>'+
                            '</div>'+

                            '<div class="form-group">'+
                              '<label>Fecha de Fin</label> <input id="fechaF2" name="fechaF" type="text" placeholder="dd/mm/aaaa hh:mm" value="'+evento.fechaFin+'" class="form-control"/>'+
                            '</div>'+

                            '<div class="form-group"><label>Lugar</label> <input id="lugar" name="lugar" type="text" class="form-control" value="'+evento.lugar+'"></div>'+

                            '<div class="form-group"> ' +
                              '<label>Fecha de Creacion</label><input size="30" id="f_alta" name="f_alta" type="text" class="form-control input-md" required value="'+evento.fechaAlta+'" disabled/> ' +
                            '</div> ' +

                            '<div class="form-group"> ' +
                              '<label>Colegio</label> <input size="30" id="colegio" name="colegio" type="text" class="form-control input-md" required value="'+evento.colegio+'" disabled/>' +
                            '</div> ' +

                            '<div class="form-group"> ' +
                              '<label>Creador</label> <input id="creador" size="30" name="creador" type="text" class="form-control input-md" required value="'+evento.creador+' - '+evento.creadorrol+'" disabled/>' +
                            '</div> ' +

                            '<div class="form-group"><label>Con confirmación</label> </div>'+
                                '<div class="form-group"><label>Si</label> <input id="confirmYes" name="confirm" type="radio" data-toggle="tooltip" title="El destinatario deber&aacute; aceptar o rechazar el comunicado" '+confirmYes+'/> &nbsp;&nbsp;&nbsp; <label>No</label><input id="confirmNo" name="confirm" type="radio" '+confirmNo+'/></div>'+

                          '</div>'+

                        '</div>'+ //fin DivDatos

                 '</form>'+
              '</div>  </div> ',
        buttons: {
            success: {
                label: "Guardar",
                className: "btn-success",
                callback: function (e) {
                        var titulo = $('#Nevento2 #titulo').val();

                        if(titulo != '')
                        {
                          var titulonew = $('#Nevento2 #titulo').val();
                          var confirmarnew = $('#Nevento2 #confirmYes').is(':checked');

                          if(!confirmarnew)
                            confirmarnew = 0;
                          else  confirmarnew = 1;
                          //var descripcionnew = $('#Nevento2 #desc2').val();

                          var editor = CKEDITOR.instances['desc2'];
                          var descripcionnew = editor.getData();

                          var lugarnew = $('#Nevento2 #lugar').val();

                          var fechainew = $('#Nevento2 #fechaI2').val();
                          if(fechainew!='')
                              fechainew = invertir_fecha2(fechainew);
                          else
                              fechainew = '0000-00-00 00:00:00';


                          var fechafnew = $('#Nevento2 #fechaF2').val();
                          if(fechafnew!='')
                              fechafnew = invertir_fecha2(fechafnew);
                          else
                              fechafnew = '0000-00-00 00:00:00';



                          bootbox.prompt({
                            title: "Ingrese su PIN",
                            value: "",
                            inputType: "password",
                            maxlength: 4,
                            callback: function(pin) {
                              if (pin === null) {
                                 return true;
                              }
                              else {
                                //agregar a la base de datos el nuevo evento
                                var parametros = {
                                  'titulo': titulonew,
                                  'confirm': confirmarnew,
                                  'descripcion': descripcionnew,
                                  'lugar': lugarnew,
                                  'fechaInicio':  fechainew,
                                  'fechafin':  fechafnew
                                };
                                var parametros = JSON.stringify(parametros);

                                //console.log(parametros);
                                modificar_evento(parametros, evento.idevento, pin, idcole);
                              }
                            }
                          });//fin del prompt
                          return false; //para que no se cierre el popup
                        }
                        else {
                              //bootbox.alert("Ingrese los campos obligatorios", function() { });
                              $('#Nevento2 #titulo').notify("Ingrese un titulo", {className:"error", autoHide: false});
                              $('#Nevento2 #titulo').focus();
                              return false;
                              }
                }//fin calback
            }//fin success
        },//fin Buttons

        onEscape: function() {return ;},
    });  //FIN DIALOG


    datepicker();

    $('[data-toggle="tooltip"]').tooltip();

     $('#Nevento2 #titulo').keyup(function(){
        var valor = $(this).val();
        if(valor == '')
          $('#Nevento2 #titulo').notify("Ingrese un titulo", {className:"error", autoHide: false});
        else
          $('#Nevento2 #titulo').parent('div').find('.notifyjs-wrapper').css('display','none');
      });


     CKEDITOR.replace( 'desc2', {

   toolbarGroups: [
    { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
    { name: 'colors', groups: [ 'colors' ] },
    { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
    { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
    { name: 'styles', groups: [ 'styles' ] },
    { name: 'links', groups: [ 'links' ] },
    { name: 'tools', groups: [ 'tools' ] },
    { name: 'others', groups: [ 'others' ] },

  ],

   removeButtons :'Print,Preview,NewPage,Save,Templates,Source,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Subscript,Superscript,Outdent,Indent,CreateDiv,BidiRtl,BidiLtr,Language,Image,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Maximize,About,Font,ShowBlocks,Cut,Copy,Paste,PasteText,PasteFromWord,Find,FontSize',
   // uiColor: '#DF0101'
  } );
    //fin
    $('.modal-content').css({'width':'110%'});


    listar_archivos(evento.idevento,1, 'D'); //D: detalle. debo diferenciar del alta

}



function detalles_evento(id2, elem, editDatos, idcole)
{
  //alert(id2);
    var urlRequest = '<?=base_url("/index.php/eventos/get_eventodatos_ajax")?>';


    $.ajax({
        url: urlRequest,
          type: 'GET',
          headers: {

           'Access-Control-Allow-Origin': '*'
      },
      beforeSend: function()
      {
          var loading_vista_evento_img = 'loading_vista_evento_img_'+id2;

          $(elem).css('display','none');
          $(elem).after("<img  id='"+loading_vista_evento_img+"' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");
      },
      data :{id: id2},//la última id
      success : function(datos)
      {//console.log(datos);
             var datos = jQuery.parseJSON( datos );
          //console.log(datos);

            $('#loading_vista_evento_img_'+id2).remove();
            $(elem).css('display','inline');

            if( (datos['fechaInicio']!=null )&&(datos['fechaInicio']!='0000-00-00 00:00:00') )
            {
              var inicio = datos['fechaInicio'].split(" ");
              var fechaaux=(inicio[0]).split('-');
              inicio = fechaaux[2]+'/'+fechaaux[1]+'/'+fechaaux[0]+' '+inicio[1];
            }
            else
              {
                inicio='';
              }

            if( (datos['fechafin']!=null )&&(datos['fechafin']!='0000-00-00 00:00:00') )
            {
              var fin = datos['fechafin'].split(" ");
              fechaaux=(fin[0]).split('-');
              fin = fechaaux[2]+'/'+fechaaux[1]+'/'+fechaaux[0]+' '+fin[1];
            }
            else
              {
                fin='';
              }

            if( (datos['fechaCreacion']!=null )&&(datos['fechaCreacion']!='0000-00-00 00:00:00') )
            {
              var alta=datos['fechaCreacion'].split(' ');
              fechaaux=(alta[0]).split('-');
              alta = fechaaux[2]+'/'+fechaaux[1]+'/'+fechaaux[0]+' '+alta[1];
            }
            else
              {
                alta='';
              }


           var parametros =
            {
              'idevento': datos['eventid'],
              'titulo': datos['titulo'],
              'confirm': datos['confirm'],
              'descripcion': datos['descripcion'],
              'lugar': datos['lugar'],
              'propietario': datos['owner'],
              'creadorid': datos['userid'],
              'creador': datos['last_name']+' '+datos['first_name'],
              'creadorrol': datos['name'],
              'colegio': datos['nombrecolegio'],
              'fechaAlta':  alta,
              'fechaInicio':  inicio,
              'fechaFin':  fin
            };

            console.log(parametros);

            if(!editDatos)
                abrir_dialogDetalle(parametros);
            else abrir_dialogDetalle_edit(parametros, idcole);

      },
      error: function(response)
      {
          //hacer algo cuando ocurra un error
          console.log(response);
      }
    });

}



function publicar_evento(id, elem)
{
 
  bootbox.prompt({
    title: "Ingrese su PIN",
    value: "",
    inputType: "password",
    maxlength: 4,
    callback: function(pin) {

      if (pin === null) {
         return true;
      } else {

          var urlRequest = '<?=base_url("/index.php/autorizar_eventos/autorizar_ajax")?>';

          $.ajax({
              url: urlRequest,
                type: 'POST',
                headers: {
                 'Access-Control-Allow-Origin': '*'
            },
            beforeSend: function()
            {
                var loading_vista_evento_img = 'loading_vista_evento_img_'+id;

                $(elem).css('display','none');
                $(elem).after("<img  id='"+loading_vista_evento_img+"' src='<?=base_url('assets/images/loading.gif');?>' height='20' width='20' />");
            },
            data :{id: id, pin:pin},
            success : function(datos)
            {
                var data = jQuery.parseJSON( datos );
                //console.log(data);

                if(!data['status'])
                {
                    if(data['message'] == 'pininvalido')
                    {
                      $.notify('error: Pin Incorrecto!', "warning");
                      $('#loading_vista_evento_img_'+id).remove();
                      $(elem).css('display','inline');
                    }
                }
                else{   //alert('OK');
                      $.notify(data['message'], "success");

                      tableEventos.row('#evento_'+id).remove().draw();
                }




            },
            error: function(response)
            {
                //hacer algo cuando ocurra un error
                console.log(response);
            }
          });

      }

    }
  });
}

</script>


<script >

var tableEventos = null;
function generar_datatable()
{

            var handleDataTableButtons2 = function() {
              if ($('.table').length) {
                tableEventos=$('.table').DataTable({

            //"scrollX": true,
            //"scrollY": true,

            "scroller": {
            "displayBuffer": 2
            },


                  responsive: true,
                  keys: true,
                  deferRender: true,
                  scrollCollapse: true,

                  "language": {
                                "sProcessing":     "Procesando...",
                                "sLengthMenu":     "Mostrar _MENU_ registros",
                                "sZeroRecords":    "No se encontraron resultados",
                                "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
                                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Buscar:",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":     "Ãšltimo",
                                    "sNext":     "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                              }
                });
              }
            };
            TableManageButtons2 = function() {
              "use strict";
              return {
                init: function() {

                  handleDataTableButtons2();
                }
              };
            }();
            TableManageButtons2.init();
            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true,

            });


    }


/*function generar_datatable2()
{
  $('.table').DataTable();
}*/
</script>



<div class="wrapper wrapper-content animated fadeInRight" id="">
    <div class="row">
          <div class="col-lg-12 " id="usuarios_borrar">
              <div class="ibox collapsed2 float-e-margins">
                  <div class="ibox-title" id="tituloaltaevento">
                      <h5>Comunicados por Autorizar</h5>
                      <div class="ibox-tools">
                          <a class="">
                              <!--<i class="fa fa-chevron-up"></i>-->
                          </a>
                      </div>
                  </div>
                  <div class="ibox-content">
                      <div class="row row2">
                          <div class="col-sm-12 b-r" id="">
                              <div class="tabs-container">
                                <ul class="nav nav-tabs" id="colegios_dir_prec">

                                  <?
                                  $cont=0;
                                  if($colegios)
                                    foreach ($colegios as $cole)
                                    {
                                      $idcole = $cole['idcole'];
                                      $url = base_url()."Horarios/index#tab_dir-".$idcole;
                                      $name = $cole['namecole'];
                                      if($cont==0)
                                        echo "<li class='active'><a data-toggle='tab' href='$url'>$name</a></li>";
                                      else
                                        echo "<li class=''><a data-toggle='tab' href='$url'>$name</a></li>";

                                      $cont++;
                                    }


                                  ?>
                                </ul>

                                <div class="tab-content " id="contenido_tab_dir_prec">


                                  <?
                                  $cont=0;
                                  if($colegios)
                                    foreach ($colegios as $cole)
                                    {
                                      $idcole = $cole['idcole'];
                                      //print_r($idcole); die();
                                      if($cont==0)
                                        $clase = 'active';
                                      else
                                        $clase = '';

                                      ?>
                                          <div id="tab_dir-<?=$idcole?>" class="tab-pane <?=$clase?>">
                                            <div class="panel-body table-responsive">

                                              <?if(isset($eventos[$idcole])) {
                                                ?>
                                                <table class="table table-striped table-bordered dt-responsive nowrap" id="tabladatos_<?=$idcole?>">
                                                    <thead>
                                                      <th>#</th>
                                                      <th>Titulo</th>
                                                      <!--<th>Fecha Inicio</th>
                                                      <th>Fecha Fin</th>-->
                                                      <th>Fecha Creacion</th>
                                                      <th>Autor</th>
                                                      <th>Rol Autor</th>
                                                      <th></th>

                                                    </thead>
                                                    <tbody id="tabladatos_body_<?=$idcole?>">
                                                      <?
                                                        $i = 1;

                                                          foreach($eventos[$idcole] as $evento)
                                                          {

                                                            //print_r($evento);
                                                            $editDatos=0;
                                                            if(count($colegiosEdit)>0)
                                                                if (in_array($idcole, $colegiosEdit))
                                                                  $editDatos=1;

                                                      ?>
                                                          <tr id="evento_<?=$evento['id']?>">
                                                              <td> <?=$i?> </td>
                                                              <td> <?=$evento['titulo']?> </td>

                                                              <!--<td> <?//=$this->varias->invertir_fecha($evento['fechaInicio'])?> </td>
                                                              <td> <?//=$this->varias->invertir_fecha($evento['fechafin'])?> </td>-->

                                                              <td> <?=$this->varias->invertir_fecha($evento['fechaCreacion'])?> </td>
                                                              <td> <?= $evento['first_name']." ".$evento['last_name']?> </td>
                                                              <td> <?=$evento['rolname']?> </td>
                                                              <td>
                                                                <button type="button" class="btn btn-success" title="Ver Detalles del Evento" onclick="detalles_evento(<?=$evento['id']?>,this,<?=$editDatos?>,<?=$idcole?>)" id="">Detalles</button>

                                                               <button type="button" class="btn btn-danger" title="Publicar Evento" onclick="publicar_evento(<?=$evento['id']?>,this)" id="">Publicar</button>
                                                              </td>

                                                          </tr>
                                                      <?
                                                          $i++;
                                                          }
                                                      ?>
                                                    </tbody>
                                                </table>
                                                <script> //generar_datatable2('<?=$idcole?>');</script>
                                              <?}?>
                                            </div>
                                          </div>

                                    <?
                                    $cont++;
                                    }?>

                                </div>
                             </div>
                          </div>
                      </div>

                  </div>
              </div>

          </div>

      </div>
</div>

