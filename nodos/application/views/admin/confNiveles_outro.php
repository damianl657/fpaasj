<script> 

	$(document).ready(function(){
	
		$('#niv_finalizar').click(function(e)
		{ 
			e.preventDefault();
			location.href = "<?php echo base_url('login/principal/'); ?>"+idUser;
		});      

	});  			 				   
			
</script>

		
	<div id="outro_nivel" style='display:none;'>	
		<!-- Indicadores de Pasos -->
		<div class="container-fluid nopadding">
			<div class="row nopadding center indicadores-pasos">
				<div id="paso-nivel" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding ok">
					Paso 1: Niveles <span><i class="fa fa-check-circle-o"></i></span>
				</div>
				<div id="paso-especialidad" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding ok">
					Paso 2: Especialidades <span><i class="fa fa-check-circle-o"></i></span>
				</div>
				<div id="paso-ano" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding ok">
					Paso 3: A&ntilde;os <span><i class="fa fa-check-circle-o"></i></span>
				</div>
				<div id="paso-division" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding ok">
					Paso 4: Divisiones <span><i class="fa fa-check-circle-o"></i></span>
				</div>
			</div>
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center vspace4">
					<h1>Configuraci&oacute;n de 4 pasos</h1>
					<h6>de instituci&oacute;n en sistema nodos</h6>
				</div>
				<div class="col-lg-offset-3 col-lg-6 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-12 center vspace4">
					<h4>Felicitaciones!</h4>
					<p>Ha configurado su instituci&oacute;n en el sistema de Nodos.<br>
Ahora podr&aacute; generar cada nodo que la compone.</p>
				</div>
				<div class="col-lg-offset-3 col-lg-6 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-12 center vspace3">
					<a id="niv_finalizar" href="" class="btn-principal">Ir al panel de control</a>
				</div>
			</div>
		</div>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) 
		<script src="js/jquery-latest.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed 
		<script src="bootstrap-3.3.6/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/main.js"></script>-->
		
</div>