	        <div class="footer">	                
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center">
                     <?php 
                        date_default_timezone_set('America/Argentina/San_Juan');?>
                         
                           <center><img src=<?=base_url("assets/images/i/icon-user.png")?> width="40" alt="NODOS"/><p>&copy; <? echo date("Y"); ?></p></center>
                    </div>
	        </div>

	    </div>

	</div><!-- wrapper -->

    <!-- Mainly scripts -->
     <script src="<?=base_url('tpl/js/plugins/metisMenu/jquery.metisMenu.js');?>"></script>
     <script src="<?=base_url('tpl/js/plugins/slimscroll/jquery.slimscroll.min.js');?>"></script>
  
    <!-- Data Tables -->
     <script src="<?=base_url('tpl/js/plugins/dataTables/jquery.dataTables.js');?>"></script>
     <script src="<?=base_url('tpl/js/plugins/dataTables/dataTables.bootstrap.js');?>"></script>
     <script src="<?=base_url('tpl/js/plugins/dataTables/dataTables.responsive.js');?>"></script>     
     <script src="<?=base_url('tpl/js/plugins/dataTables/dataTables.tableTools.min.js');?>"></script>

  
      <!-- Custom and plugin javascript -->
    <script src="<?=base_url('tpl/js/inspinia.js');?>"></script>  <!-- usa -->
	
    <script src="<?=base_url('tpl/js/plugins/pace/pace.min.js');?>"></script>

    <?php 
     if ((isset($carga_select_dt)) && ($carga_select_dt == true)) { ?>
    <link href="<?php echo base_url('tpl/css/plugins/dataTables/select.dataTables.min.css') ?>" rel="stylesheet"> 
    
    <script src="<?=base_url('tpl/js/plugins/dataTables/dataTables.select.min.js');?>"></script>
   
    <?php } ?>

</body>
</html>