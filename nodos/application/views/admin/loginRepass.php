<script>

    var passApiKey = '<?php echo $passApiKey; ?>';
    var urlApi = '<?php echo $urlApi; ?>';
    var idUser = '<?php echo $idusuario; ?>';
 
    function prep_datos( password, pin){
     
        var urlRequest = urlApi+"/login/actualizar_contrasenia";                           
   
        $.ajax({
            url: urlRequest,
            type: 'POST',
            headers: {    
                      'userid' : idUser,
					  'APIKEY' : passApiKey
            },
            data: {password:password, pin: pin },
            beforeSend: function() {
                    $('.top-right').notify({
                        message: { text: 'Cargando...' },
                        fadeOut: { enabled: true, delay: 2500 },
                        type: 'success2'
                    }).show(); 
					//$('.login').after("<img id='login src='<?=base_url('assets/images/loading.gif');?>' height='30' width='30' />");
            },
            success: function(data){
				$('#idusuario').val(idUser); 
				/*$('.top-right').notify({
                            message: { text: data['message'] },
                            fadeOut: { enabled: true, delay: 4500 },
                            type: 'success2'
                }).show(); */
		           
				$.notify(data['message'], function() { 
				     document.forms['formChangePass'].submit();
				});
	
            },
            error: function(response){
                  //console.log(response);
                  if(response['responseText'])
                  { 
                    response = JSON.parse(response['responseText'],true);
                   
                    $('.top-right').notify({
                          message: { text: response['message']},
                          fadeOut: { enabled: true, delay: 3000 },
                           type: 'danger2'
                        }).show();                       
                    }
            }
        });

}


$(document).ready(function(){
	 
    $.noConflict();  
	/*$(".solo-numero").keyup(function(){
        var numpin = (this.value + '').replace(/[^0-9]/g, '');
        if(numpin == ""){     // valida que no sea campo vacio.
		    //$("#ErrorPin").fadeOut();
            //var msj = "El pin debe ser numérico";
            $('#pin').val(''); 
            $("#pin").focus()			
            //$("#pin").focus().after("<span class='ErrorPin'>"+msj+"</span>");  
        }else{ 
			  var length_pin = $('#pin').val().length;
			  if(length_pin == 4){  // valida que tenga 4 digitos.
			     $("#ErrorPin").fadeOut();
				  return true;
				  var msj = "Pin cambiado con Exito.";
				 
  			  }else{
  					$("#ErrorPin").fadeOut();
					var msj = "El pin debe tener 4 digitos numéricos";
  			 		$('#pin').val('');  
  					$('#pin').focus(); 
					$("#pin").focus().after("<span class='ErrorPin'>"+msj+"</span>");  
  			  }   
        }    
     });//fin keyup
	 */
   
    //-------------------------------------------//
    //----------- Click en LOGIN ---------------//
	//-------------------------------------------//
    $('#login').click(function(event){    
       
        event.preventDefault();
        var pass = $('#clave').val();
        var repass = $('#repite_clave').val();
        var pin = $('#pin').val();
		var repin = $('#re_pin').val();
        $('.error').fadeOut();
	   //$('.ErrorPin').fadeOut();
        if( pass != "")
        {
            if($('#clave').val().length > 4)
            {

            
	            if( repass != ""){
	                 if(pass != repass){
	                    var msj = "Las contraseñas no coinciden";
	                    $('#repite_clave').val(''); 
	                    $("#repite_clave").focus().after("<span class='error'>"+msj+"</span>");      
	                
	                    return false;
	                  }else{
						 $('.error').fadeOut(); 
	                     // Si las contraseñas coinciden -- Verifica Pin  //
	                      if( $("#pin").val() == ""){// Verificar si el pin esta cargado.
						      var msj = "Ingrese pin de 4 digitos";
	                          $("#pin").focus().after("<span class='ErrorPin'>"+msj+"</span>"); 
							  return false
	                      }else{
								if ( $('#pin').val().length == 4 ){
									if(!isNaN($('#pin').val())){
										$('.ErrorPin').fadeOut();
										if(repin != ""){  // Repetir Pin.
											
											if($('#re_pin').val().length == 4 ){
												if(!isNaN($('#re_pin').val())){
													if( $('#pin').val() == $('#re_pin').val()){
														$('.error').fadeOut();
														$('.ErrorPin').fadeOut();    
														prep_datos( pass, pin);
														return true;  
													}else{
														$('.ErrorPin').fadeOut(); 
														var msj = "El Pin no coincide";
														$("#re_pin").val('');
														$("#re_pin").focus().after("<span class='ErrorPin'>"+msj+"</span>");   
														return false;  
													} 
												}else{
													$('.ErrorPin').fadeOut();
													var msj = "Ingrese Pin numérico";
													$("#re_pin").val('');
													$("#re_pin").focus().after("<span class='ErrorPin'>"+msj+"</span>");   
													return false;
												}
												
											}else{
												$('.ErrorPin').fadeOut();
												var msj = "Repita Pin de 4 digitos";
												
												$("#re_pin").val('');
												$("#re_pin").focus().after("<span class='ErrorPin'>"+msj+"</span>");   
												return false;
											}									
											  
										}else{
											$('.ErrorPin').fadeOut();
											var msj = "Repita el Pin";
											$("#re_pin").val('');
											$("#re_pin").focus().after("<span class='ErrorPin'>"+msj+"</span>");   
											return false; 
										}
									}else{
										$('.ErrorPin').fadeOut();
									    var msj = "El Pin debe ser numérico";
										$("#pin").val('');
										$("#pin").focus().after("<span class='ErrorPin'>"+msj+"</span>");   
										return false; 
									}	
								}else{
									 $('.ErrorPin').fadeOut();
									 var msj = "Ingrese Pin de 4 digitos";
									 $("#pin").val();
									 $("#pin").focus().after("<span class='ErrorPin'>"+ msj +"</span>");      
									return false;
								} 					  
	                      }
	          		
	               }
				}else{
					    $('.error').fadeOut(); 
					    var msj = "Repita la Contraseña";
						$('#repite_clave').val('');
					    $("#repite_clave").focus().after("<span class='error'>"+ msj +"</span>");
						
						return false;
				}
			}
			else
			{
				var msj = "La Contraseña debe tener mínimo 6 caracteres";
                $('#clave').val('');
	  		    $('.error').fadeOut(); 
                $("#clave").focus().after("<span class='error'>"+ msj +"</span>");
	  		    return false;
			}
      }else{
      	    var msj = "Complete la Contraseña";
            $('#clave').val('');
			$('.error').fadeOut(); 
            $("#clave").focus().after("<span class='error'>"+ msj +"</span>");
			return false;
      }

	  /*
        //-- input 4 numerico.
        if(pin != ""){
            var numpin = (pin).replace(/[^0-9]/g, '');
            alert(numpin);
            if ($('#pin').val().lenght == 4){
      				var msj = "El Pin es correcto";
                          $('.top-right').notify({
                                message: { text: msj },
                                fadeOut: { enabled: true, delay: 3000 },
                                type: 'warning2'
                          }).show();
                            $("#repass").css({"background":"#8F8"}); //El input se ponen verde
                            $("#msjErrorPass").fadeOut();
                             return true;  
      			}else{
      				    var msj = "El Pin debe tener 4 caracteres";
                          $('.top-right').notify({
                                message: { text: msj },
                                fadeOut: { enabled: true, delay: 3000 },
                                type: 'warning2'
                          }).show();
                          
                         // $("#msjErrorPass").fadeOut();
                          $("#pin").focus().after("<span class='ErrorPin'>Ingrese su nombre</span>");
                          return false;
      			}             
		   
        }else{
              $("#pin").focus().after("<span class='error'>Ingrese su nombre</span>")      
		      $('#pin').val('lola');
             
        }*/

       // ingresar();

    }); // fin function Login
	
	//-------------------------------------------------
	//--------- VAlidar Formulario --------------------
	//-------------------------------------------------
	function validar_formulario(){
		
		var pass = $('#clave').val();
        var repass = $('#repite_clave').val();
        var pin = $('#pin').val();
		var repin = $('#re_pin').val();
        $('.error').fadeOut();
	   //$('.ErrorPin').fadeOut();
        if( pass != "")
        {
            if($('#clave').val().length > 5 )
            {
	            if( repass != "")
	            {
	                if(pass != repass)
	                {
	                    var msj = "Las contraseñas no coinciden";
	                    $('#repite_clave').val(''); 
	                    $("#repite_clave").focus().after("<span class='error'>"+msj+"</span>");      
	                
	                    return false;
	                }
	                else
	                {
						$('.error').fadeOut(); 
	                    // Si las contraseñas coinciden -- Verifica Pin  //
	                    if( $("#pin").val() == "")
	                    {// Verificar si el pin esta cargado.
						    var msj = "Ingrese pin de 4 digitos";
	                         
	                        $("#pin").focus().after("<span class='ErrorPin'>"+msj+"</span>"); 
							return false
	                    }
	                    else
	                    {
							if ( $('#pin').val().length == 4 )
							{
								if(!isNaN($('#pin').val()))
								{
									$('.ErrorPin').fadeOut();
									if(repin != "")
									{  // Repetir Pin.
									
										if($('#re_pin').val().length == 4 )
										{
											if(!isNaN($('#re_pin').val()))
											{
												if( $('#pin').val() == $('#re_pin').val())
												{
													$('.error').fadeOut();
													$('.ErrorPin').fadeOut();    
													prep_datos( pass, pin);
													return true;  
												}
												else
												{
													$('.ErrorPin').fadeOut(); 
													var msj = "El Pin no coincide";
													$("#re_pin").val('');
													$("#re_pin").focus().after("<span class='ErrorPin'>"+msj+"</span>");   
													return false;  
												} 
											}else
											{
												$('.ErrorPin').fadeOut();
												var msj = "Ingrese Pin numérico";
												$("#re_pin").val('');
												$("#re_pin").focus().after("<span class='ErrorPin'>"+msj+"</span>");   
												return false;
											}
												
										}else
										{
											$('.ErrorPin').fadeOut();
											var msj = "Repita Pin de 4 digitos";
												
											$("#re_pin").val('');
											$("#re_pin").focus().after("<span class='ErrorPin'>"+msj+"</span>");   
											return false;
										}									
											  
									}
									else
									{
										$('.ErrorPin').fadeOut();
										var msj = "Repita el Pin";
										$("#re_pin").val('');
										$("#re_pin").focus().after("<span class='ErrorPin'>"+msj+"</span>");   
										return false; 
									}
								}
								else
								{
									$('.ErrorPin').fadeOut();
									var msj = "El Pin debe ser numérico";
								    $("#pin").val('');
									$("#pin").focus().after("<span class='ErrorPin'>"+msj+"</span>");   
									return false; 
								}	
							}
							else
							{
								$('.ErrorPin').fadeOut();
							    var msj = "Ingrese Pin de 4 digitos";
								$("#pin").val();
								$("#pin").focus().after("<span class='ErrorPin'>"+ msj +"</span>");      
								return false;
							} 					  
	                    }
	                }
				}
				else
				{
					$('.error').fadeOut(); 
					var msj = "Repita la Contraseña";
					$('#repite_clave').val('');
					$("#repite_clave").focus().after("<span class='error'>"+ msj +"</span>");
						
					return false;
				}
			}
			else
			{
				var msj = "La Contraseña debe tener mínimo 6 caracteres";
                $('#clave').val('');
	  		    $('.error').fadeOut(); 
                $("#clave").focus().after("<span class='error'>"+ msj +"</span>");
	  		    return false;
			}	
        }
        else
        {
        	    var msj = "Complete la Contraseña";
                $('#clave').val('');
	  		    $('.error').fadeOut(); 
                $("#clave").focus().after("<span class='error'>"+ msj +"</span>");
	  		    return false;
        }
		
	}

    $(document).keypress(function(e) {
        if(e.which == 13) {
            //ingresar();  // arreglar.
			validar_formulario();
        }
    });

  }); //-- fin Document Ready.

 //-----------------------------------
	function enableSubmit (idForm) {
	  $(idForm + " button.submit").removeAttr("disabled");
	}
	 
	function disableSubmit (idForm) {
	  $(idForm + " button.submit").attr("disabled", "disabled");
	}

</script>
	

<body>

	<!-- count particles -->
	<div class="count-particles">
		<span class="js-count-particles">80</span> particles
	</div>

	<!-- particles.js container -->
	<div id="particles-js"><canvas class="particles-js-canvas-el" width="1059" height="599" style="width: 100%; height: 100%;"></canvas></div>

	<link rel="shortcut icon" href="http://works.sietedefebrero.com/nodos/dev/favicon.ico?v=2" type="image/x-icon">
	<link rel="icon" href="http://works.sietedefebrero.com/nodos/dev/favicon.ico?v=2" type="image/x-icon">
	
	<link href="<?=base_url('assets/css/style.css');?>" rel="stylesheet">	
	<!-- Google Fonts -->
	<link href="<?=base_url('assets/css/css');?>" rel="stylesheet" type="text/css"> 

	<!-- scripts -->
	<script src="<?=base_url('assets/js/particles/particles.js');?>"></script>
	<script src="<?=base_url('assets/js/particles/app.js');?>"></script>

	<!-- stats.js -->
	<script src="<?=base_url('assets/js/particles/stats.js');?>"></script>
	<script>
	var count_particles, stats, update;
	stats = new Stats;
	stats.setMode(0);
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.left = '0px';
	stats.domElement.style.top = '0px';
	document.body.appendChild(stats.domElement);
	count_particles = document.querySelector('.js-count-particles');
	update = function() {
		stats.begin();
		stats.end();
		if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
			count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
		}
		requestAnimationFrame(update);
	};
	requestAnimationFrame(update);
	</script>
	
	
	<div class="container">
		<div class="row">
			<div class="col-lg-offset-4 col-lg-4 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-12">
				
				<div class="login" style="margin-top: 100px;">
				 <img src="<?=base_url('assets/images/logo-login.png');?>" width="90" alt="NODOS">

				<form method="post" action="principal" id="formChangePass">
			          
                 <input id='clave' type="password" name="password" required placeholder="Nueva Contraseña">
			     <input id='repite_clave' type="password" name="repite_password" required placeholder="Repita Contraseña">
                 <input id='pin' type="text" name="pin" class="solo-numero" placeholder="Ingrese Pin de 4 dígitos"  maxlength="4">
				 <input id='re_pin' type="text" name="re_pin" placeholder="Repita Pin "  maxlength="4">

                  <input id='idusuario' type="hidden" name="idusuario" >
			            
                  <!--<span class="warning2">This field is required</span> -->
                  <input id='login' type="button" value="Actualizar Contraseñas">

			    </form>
				</div>
				
			</div>
		</div>
	</div>
	
  <div class='notifications top-right'></div>
 <style>
  .error{
        
        padding: 8px 10px;
        /*
        background-color: #BC1010;
        border-radius: 4px;
        color: white;*/
          color: #c09853;
          text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
          background-color: white;
          border: 1px solid #c09853;
          -webkit-border-radius: 4px;
          -moz-border-radius: 4px;
          border-radius: 4px;
        font-weight: bold;
        font-size: 14px;
        margin-left: 16px;
        margin-top: 6px;
        width: 150px;
        position: absolute;
  }
   .error:before{ /* Este es un truco para crear una flechita */
        content: '';
        border-top: 10px solid transparent;
        border-bottom: 10px solid transparent;
        border-right: 10px solid #fff;
        border-left: 10px solid transparent;
        left: -16px;
        position: absolute;
        top: 5px;
   }

   /*********************************************/
   .ErrorPin{
        
        padding: 8px 10px;
        /*
        background-color: #BC1010;
        border-radius: 4px;
        color: white;*/
          color: #c09853;
          text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
          background-color: white;
          border: 1px solid #c09853;
          -webkit-border-radius: 4px;
          -moz-border-radius: 4px;
          border-radius: 4px;
        font-weight: bold;
        font-size: 14px;
        margin-left: 16px;
        margin-top: 6px;
        width: 150px;
        position: absolute;
  }
   .ErrorPin:before{ /* Este es un truco para crear una flechita */
        content: '';
        border-top: 10px solid transparent;
        border-bottom: 10px solid transparent;
        border-right: 10px solid #fff;
        border-left: 10px solid transparent;
        left: -16px;
        position: absolute;
        top: 5px;
   }

 </style>
</body></html>