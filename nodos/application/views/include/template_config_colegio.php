<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Nodos</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Nodos Comunicaci&oacute;n escolar m&aacute;s simple</title>
    <meta name="description" content="Configuración en 4 pasos de NODOS." />
    <meta name="resource-type" content="Document">
    <meta name="author" content="sietedefebrero.com"> 
    <!-- Favicon -->
    <!--<link rel="shortcut icon" href="favicon.ico?v=2" type="image/x-icon">
    <link rel="icon" href="favicon.ico?v=2" type="image/x-icon"> -->
    <link href="<?= base_url('assets/images/favicon.ico') ?>" rel="shortcut icon" type="image/x-icon" /> 
    
    <!-- Google Fonts 
    <link href='https://fonts.googleapis.com/css?family=Abel|Roboto+Condensed:300,700,400,300italic,400italic' rel='stylesheet' type='text/css'> -->
    
    <!-- Font Awesome -->
     <link href="<?=base_url('assets/font-awesome-4.5.0/css/font-awesome.min.css');?>" rel="stylesheet"> 
    
    <!-- Bootstrap -->
  <!--  <link href="bootstrap-3.3.6/css/bootstrap.min.css" rel="stylesheet"> -->
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- -->
    
  <link href="<?=base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet"> 
    <link href="<?=base_url('assets/css/custom.min.css');?>" rel="stylesheet"> 
    <link href="<?=base_url('assets/css/jquery-ui.min.css');?>" rel="stylesheet">
  <link href="<?=base_url('assets/css/jquery-ui.css');?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/general.css');?>" rel="stylesheet">
  

    <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">-->

  
  <link href="<?=base_url('assets/css/bootstrap-notify.css');?>" rel="stylesheet">

  <link href="<?=base_url('assets/css/jquery-ui-slider-pips.css');?>" rel="stylesheet">
  <link href="<?=base_url('assets/css/jquery.ptTimeSelect.css');?>" rel="stylesheet">

  <!-- Stilos Nodos -->
  <link href="<?=base_url('assets/css/main-nodos.css');?>" rel="stylesheet">
  <link href="<?=base_url('assets/css/nodos-config.css');?>" rel="stylesheet">
                      
    
  
    <script src="<?=base_url('assets/js/jquery-2.1.4.min.js')?>" type="text/javascript"></script>
  <script src="<?=base_url('assets/js/bootstrap.min.js');?>"></script>
    <script src="<?=base_url('assets/js/jquery-ui.js');?>"></script>
    <script src="<?=base_url('assets/js/jquery.dataTables.min.js');?>"></script>

  <script src="<?=base_url('assets/js/bootstrap-notify.js');?>"></script>

  <script src="<?=base_url('assets/js/bootbox.min.js');?>"></script> 

  <script src="<?=base_url('assets/js/jquery-ui-slider-pips.js');?>"></script> 
  <script src="<?=base_url('assets/js/jquery.ptTimeSelect.js');?>"></script> 
  <!-- JS NODOS -->

<script src="<?=base_url('assets/js/main.js');?>"></script> 
<script src="<?=base_url('assets/js/jquery-latest.min.js');?>"></script> 
<script src="<?=base_url('assets/js/respond.min.js');?>"></script> 

  
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../bower_components/html5shiv/dist/html5shiv.js"></script>
      <script src="../bower_components/respond/dest/respond.min.js"></script>
    <![endif]-->
</head>

<body>	
    <div id="wrapper">
        <?php
        if(!empty($contenido))
            $this->load->view($contenido,$data);
        ?>    	
		
	    <footer>
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center">
                <?php date_default_timezone_set('America/Argentina/San_Juan');?>
	            <center><img src=<?=base_url("assets/images/i/icon-user.png")?> width="40" alt="NODOS"/><p>&copy; <? echo date("Y"); ?></p></center>
	        </div>
	    </footer>
    </div><!-- wrapper -->
    <div class='notifications top-right' style="margin-top: 40px"> </div>
  </body>
</html>