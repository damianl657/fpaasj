<?php 
 
class Migracion_industrial extends CI_Controller 
{	
      
	function __construct(){
		parent::__construct();
		
			//$this->output->enable_profiler(TRUE);
        if(!$this->session->userdata("logged_in")){
            //echo "pierde sesion"; die();
               redirect("login");
         }

	}

	public function crear_pass($pass)
    {
        $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
                    $passApiKey = $this->variables->get_apikey();
                    //$correo = "nodos";
                    $url = $urlApi."/login/prueba_encriptar";
                    $ch = curl_init();
                    $options = array(
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_URL => $url,
                                CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey"),
                                CURLOPT_POST => true,
                                //CURLOPT_POSTFIELDS => json_encode($fields),
                                //CURLOPT_POSTFIELDS => $fields,
                                CURLOPT_POSTFIELDS => array(
                                                            'data' => $pass
                                                    ),  
                                CURLOPT_SSL_VERIFYPEER=> false,
                            );
                      curl_setopt_array( $ch, $options);
                      $response = curl_exec($ch);
                      curl_close($ch);
                     $response = json_decode($response, True);
                    
                     $arre = (array) $response; //convierto el obj en un arreglo.
                     $pass_encriptado = $arre['pass'];
                     return $pass_encriptado;
    }
    //damian
    public function importar_docentes()
    {
        //VARIABLES
        $tabla = "bf_docentes_nodos"; //--modificar por la division
        
        $group_id = 102;
        $colegio_id = 9;
        //$plan_estudio_id = 1;

        //SCRIPT        
        $sql = "SELECT * FROM ".$tabla;
        $cont = 0;
        $query = $this->db->query($sql);
        if($query->num_rows() > 0)
        {
            foreach ($query->result() as $key) 
            {
                $sql_check= "SELECT * FROM users WHERE documento='".$key->dni."'";
                $query_ch = $this->db->query($sql_check);
                if($query_ch->num_rows() == 0)
                {
                    $pass_alu = $this->crear_pass($key->dni);
                   

                    $insert_docente = "INSERT INTO users (first_name, last_name, active, documento, password)
                            VALUES ('$key->nombre', '$key->apellido',1, '$key->dni','$pass_alu')";
                                
                   if($this->db->query($insert_docente))
                   {
                        $id_user_docente = $this->db->insert_id();                    
                        //insertar en users_groups
                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)
                                VALUES ($id_user_docente, $group_id, $colegio_id, 1)";
                        $this->db->query($insert_users_groups);
                         $cont = $cont + 1;
                           
                    }
                } // fin if el alumno ya existe.
                else{
                    $docente = $query_ch->row();
                    $id_doc = $docente->id;

                    echo "EL Docente".$key->dni." ".$key->apellido." ".$key->nombre." Existe en USERS <br>";
                    $sql_check_user_group= "SELECT * FROM users_groups WHERE user_id='".$id_doc."'and group_id = $group_id";
                    $query_ch_user_group = $this->db->query($sql_check_user_group);
                    if($query_ch_user_group->num_rows() > 0)
                    {
                        echo "EL Docente YA ES DOCENTE EN LA ESCUELA SELECCIONADA<br>";
                    }
                    else
                    {
                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)
                                VALUES ($id_doc, $group_id, $colegio_id, 1)";
                        $this->db->query($insert_users_groups);
                         //$cont = $cont + 1;
                    }
                    
                }
                
            } // fin for recorrido de los alumnos.
            echo "se insertaron: ".$cont." Docentes<br>";
        }
    } 

    public function importar_alumnos()
    {
        //VARIABLES
        $tabla = "bf_alumnos_nodos"; //--modificar por la division
       // $division_id = 63;
        $group_id = 104;
        $colegio_id = 9;
        $plan_estudio_id = 4;
        $cicloa = 2019;

        $sql = "SELECT * FROM ".$tabla." WHERE id_div_ind <> 0";
        $cont = 0;
        $query = $this->db->query($sql);
        if($query->num_rows() > 0)
            foreach ($query->result() as $key => $value) 
            {
                $sql_check= "SELECT * FROM users WHERE documento='".$value->dni."'";
                $query_ch = $this->db->query($sql_check);
                if($query_ch->num_rows() == 0)
                {
                    $pass_alu = $this->crear_pass($value->dni);
                   // $insert_alumno = "INSERT INTO users (first_name, last_name, active, documento, password, recibir_notificacion)
                          //  VALUES ('$value->nombre', '$value->apellido',1, '$value->dni','$pass_alu',1)";
                    $insert_alumno = "INSERT INTO users (last_name, active, documento, password, recibir_notificacion)
                            VALUES ('$value->apellido',1, '$value->dni','$pass_alu',1)";
                    if($this->db->query($insert_alumno))
                    {
                        $id_user_alumno = $this->db->insert_id();                    
                        //insertar en users_groups
                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)
                                VALUES ($id_user_alumno, $group_id, $colegio_id,1)";
                        if($this->db->query($insert_users_groups))
                        {
                            //insertar en inscripcion
                            $id_alumno = $this->db->insert_id();
                            $insert_inscripcion = "INSERT INTO inscripciones (alumno_id,activo, cicloa,divisiones_id)
                                    VALUES ($id_alumno,1,$cicloa,$value->id_div_nodos)";
                                   //echo $insert_inscripcion."<br>";        

                                    if($this->db->query($insert_inscripcion))
                                    {
                                        echo "OK -".$value->dni." ".$value->apellido."<br>";
                                        //echo "OK -".$value->dni." ".$value->apellido." ".$value->nombre."<br>";
                                        $cont = $cont + 1;
                                    }else
                                    {
                                      echo "NoInsertoAlumno: ".$value->dni." ".$value->apellido." ".$value->nombre."<br>";
                                      //echo "NoInsertoAlumno: <br>";
                                    }
                        }    
                    }


                }
                else{
                    echo "ELAlumnoExiste: ".$value->dni." ".$value->apellido." ".$value->nombre." <br>";
                }
            echo "se insertaron: ".$cont." alumnos";
            }
    }
    public function importar_preceptores()
    {
        $colegio_id = 9;
        $group_id = 101;
      
        $bf_preceptores_nodos = "bf_preceptores_nodos";
        $sql2 = "SELECT * FROM ".$bf_preceptores_nodos;
        $query_preceptores = $this->db->query($sql2);

       
        if($query_preceptores->num_rows() > 0)
        {
            //print_r($query_alumnos->num_rows())."<br>";
            foreach ($query_preceptores->result() as $key)
            {
                $dni = $key->dni;

                $sql_preceptor_aux = "SELECT * FROM users WHERE documento = '$dni'";
                $sql_preceptor_aux = $this->db->query($sql_preceptor_aux);
                if($sql_preceptor_aux->num_rows() > 0)
                {
                    echo "YaExiste Preceptor:".$key->apellido.", ".$key->nombre." <br>";
                    $users_preceptor = $sql_preceptor_aux->row();
                    //averiguar si ya esta en user_groups
                    $sql_users_group_colegio = "SELECT users_groups.* FROM users 
                                                            JOIN users_groups on users_groups.user_id =  users.id 
                                                            WHERE users.documento = $key->dni and users_groups.colegio_id = $colegio_id and users_groups.group_id = $group_id";
                    $query_users_groups = $this->db->query($sql_users_group_colegio);
                    if($query_users_groups->num_rows() > 0)//existe en el colegio y tiene el mismo rol
                    {
                        $query_users_groups = $query_users_groups->row();

                        $insert_preceptor_tabla_pre = "INSERT INTO preceptores (user_id, division_id, colegio_id, planesestudio_id, activo)
                        VALUES ($query_users_groups->id, $key->id_div_nodos, $colegio_id, 3, 1)";
                        if($this->db->query($insert_preceptor_tabla_pre))
                        {
                            echo "ok Insertado en preceptores<br>";
                        }
                    }
                    else
                    {
                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)
                                                        VALUES ($users_preceptor->id, $group_id, $colegio_id, 1)";
                                if($this->db->query($insert_users_groups))
                                {
                                    $id_user_group = $this->db->insert_id();
                                    $insert_preceptor_tabla_pre = "INSERT INTO preceptores (user_id, division_id, colegio_id, planesestudio_id, activo)
                                                        VALUES ($id_user_group, $key->id_div_nodos, $colegio_id, 3, 1)";
                                    if($this->db->query($insert_preceptor_tabla_pre))
                                    {
                                        echo "ok Insertado en preceptores<br>";
                                    }

                                    
                                }
                    }
                }
                else
                {
                    $pass = $this->crear_pass($dni);
                    
                    $insert_preceptor = "INSERT INTO users (first_name, last_name, active, password, pago, documento)
                                                VALUES ('$key->nombre', '$key->apellido',1, '$pass',1, $dni)";

                            if($this->db->query($insert_preceptor))
                            {
                               $user_id = $this->db->insert_id();
                               echo "ok Insertado en users_groups<br>";
                               //-- USERS_GRUPS Insert ---
                                $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)
                                                        VALUES ($user_id, $group_id, $colegio_id, 1)";
                                if($this->db->query($insert_users_groups))
                                {
                                    $id_user_group = $this->db->insert_id();
                                    $insert_preceptor_tabla_pre = "INSERT INTO preceptores (user_id, division_id, colegio_id, planesestudio_id, activo)
                                                        VALUES ($id_user_group, $key->id_div_nodos, $colegio_id, 3, 1)";
                                    if($this->db->query($insert_preceptor_tabla_pre))
                                    {
                                        echo "ok Insertado en preceptores<br>";
                                    }

                                    
                                }
                            }
                            else
                            {
                                echo "NO INSERTADO :".$key->apellido.", ".$key->nombre." <br>";
                            }


                }
                

                    
                
            }

        }
    }
    public function importar_administrativos()
    {
        //VARIABLES
        $tabla = "bf_administrativos_nodos"; //--modificar por la division
        
        $group_id = 105;
        $colegio_id = 9;
        //$plan_estudio_id = 1;

        //SCRIPT        
        $sql = "SELECT * FROM ".$tabla;
        $cont = 0;
        $query = $this->db->query($sql);
        if($query->num_rows() > 0)
        {
            foreach ($query->result() as $key) 
            {
                $sql_check= "SELECT * FROM users WHERE documento='".$key->dni."'";
                $query_ch = $this->db->query($sql_check);
                if($query_ch->num_rows() == 0)
                {
                    $pass_alu = $this->crear_pass($key->dni);
                   

                    $insert_docente = "INSERT INTO users (first_name, last_name, active, documento, password)
                            VALUES ('$key->nombre', '$key->apellido',1, '$key->dni','$pass_alu')";
                                
                   if($this->db->query($insert_docente))
                   {
                        $id_user_docente = $this->db->insert_id();                    
                        //insertar en users_groups
                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)
                                VALUES ($id_user_docente, $group_id, $colegio_id, 1)";
                        $this->db->query($insert_users_groups);
                         $cont = $cont + 1;
                           
                    }
                } // fin if el alumno ya existe.
                else{
                    $docente = $query_ch->row();
                    $id_doc = $docente->id;

                    echo "EL Docente".$key->dni." ".$key->apellido." ".$key->nombre." Existe en USERS <br>";
                    $sql_check_user_group= "SELECT * FROM users_groups WHERE user_id='".$id_doc."'and group_id = $group_id";
                    $query_ch_user_group = $this->db->query($sql_check_user_group);
                    if($query_ch_user_group->num_rows() > 0)
                    {
                        echo "EL administrativo YA ES administrativo EN LA ESCUELA SELECCIONADA<br>";
                    }
                    else
                    {
                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)
                                VALUES ($id_doc, $group_id, $colegio_id, 1)";
                        $this->db->query($insert_users_groups);
                         //$cont = $cont + 1;
                    }
                    
                }
                
            } // fin for recorrido de los alumnos.
            echo "se insertaron: ".$cont." administrativo<br>";
        }
    }
    public function importar_tutores()
    {
        $colegio_id = 9;
        $group_id = 103;
       

        $tabla_tutores = "bf_tutores_nodos";
        $sql2 = "SELECT * FROM ".$tabla_tutores;
        $query_tutores = $this->db->query($sql2);
        
        foreach ($query_tutores->result() as $tutor)
                {
                    
                    $sql_alumno_aux = "SELECT * FROM users WHERE documento = $tutor->documento_alumno";
                    $query_alumno_aux = $this->db->query($sql_alumno_aux);
                    $alumno_aux = $query_alumno_aux->row();
                    $id_alumno = $alumno_aux->id;
                    if($id_alumno != null)
                    {
                    //echo "encontrado";
                    //validar si existe en users el tutor
                        $sql_users = "SELECT * FROM users WHERE documento = '$tutor->documento'";
                  
                        $query_users = $this->db->query($sql_users);
                        print_r($this->db->last_query());
                        if($query_users->num_rows() > 0)
                        {
                            foreach ($query_users->result() as $user)
                            {
                                echo "tutor encontrado";
                                //ver si es padre esta en el mismo colegio
                                $sql_users_group_colegio = "SELECT * FROM users 
                                                            JOIN users_groups on users_groups.user_id =  users.id 
                                                            WHERE users.documento = $tutor->documento and users_groups.colegio_id = $colegio_id";
                                $query_users_groups = $this->db->query($sql_users_group_colegio);
                                if($query_users_groups->num_rows() > 0)//existe en el colegio
                                {
                                    //verificar que no exista la relacion en tutoralumno
                                    $sql_tutor_alumno_aux = "SELECT * FROM tutoralumno where alumno_id = $id_alumno and tutor_id = $user->id";
                                    $query_tutor_alumno_aux = $this->db->query($sql_tutor_alumno_aux);
                                    if($query_tutor_alumno_aux->num_rows() > 0)
                                    {
                                        echo "Existe Relacion alumno tutor (user_id: ".$user->id.")<br>";
                                    }
                                    else
                                    {
                                        $table = 'tutoralumno';
                                    
                                        $insert_relacion = "INSERT INTO tutoralumno (alumno_id,tutor_id)
                                        VALUES ($id_alumno,$user->id)";

                                       if($this->db->query($insert_relacion))
                                       {
                                        echo "ok Padre(".$tutor->documento." ".$tutor->apellido." ".$tutor->nombre.") Insertado en la Relacion alumno(".$tutor->documento_alumno." ".$alumno_aux->first_name." ".$alumno_aux->last_name.") tutor<br>";
                                       }
                                    }
                                    //--- TUTORALUMNO insert
                                    
                                    
                                }
                                else
                                {
                                    
                                    $sql_tutor_alumno_aux = "SELECT * FROM tutoralumno where alumno_id = $id_alumno and tutor_id = $user->id";
                                    $query_tutor_alumno_aux = $this->db->query($sql_tutor_alumno_aux);
                                    if($query_tutor_alumno_aux->num_rows() > 0)
                                    {
                                        echo "Existe Relacion alumno tutor <br>";
                                    }
                                    else
                                    {
                                        //-- USERS_GRUPS Insert ---
                                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)
                                                                VALUES ($user->id, $group_id, $colegio_id, 1)";
                                        if($this->db->query($insert_users_groups)){
                                            $id_user_group = $this->db->insert_id();
                                        } 
                                        //--- TUTORALUMNO insert
                                        $table = 'tutoralumno';
                                        
                                            $insert_relacion = "INSERT INTO tutoralumno (alumno_id,tutor_id)
                                            VALUES ($id_alumno,$user->id)";

                                           if($this->db->query($insert_relacion))
                                           {
                                            echo "ok Padre(".$tutor->documento." ".$tutor->apellido." ".$tutor->nombre.") Insertado en la Relacion alumno(".$tutor->documento_alumno." ".$alumno_aux->last_name." ".$alumno_aux->first_name.") tutor<br>";
                                           }   
                                    }
                                    
                                    

                                    //die();
                                }

                                
                            }
                        }
                        else
                        {
                            $pass_tutor= $this->crear_pass($tutor->documento);
                            $active = 1; 
                            $pago = 1;
                            
                                $email = $tutor->email;
                            
                            
                                $sexo_tutor = $tutor->sexo;
                           
                            $insert_tutor = "INSERT INTO users (first_name, last_name, active, email, password, sexo, pago, documento)
                                                VALUES ('$tutor->nombre', '$tutor->apellido',$active,'$email', '$pass_tutor', '$sexo_tutor',1, '$tutor->documento')";
                            
                            if($this->db->query($insert_tutor))
                            {
                               $id_tutor = $this->db->insert_id();
                               echo "ok Padre nuevo Insertado <br>";
                               //-- USERS_GRUPS Insert ---
                                $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)
                                                        VALUES ($id_tutor, $group_id, $colegio_id, 1)";
                                if($this->db->query($insert_users_groups))
                                {
                                    $id_user_group = $this->db->insert_id();
                                }    
                               //--- TUTORALUMNO insert
                                $table = 'tutoralumno';
                                
                                    $insert_relacion = "INSERT INTO tutoralumno (alumno_id,tutor_id)
                                    VALUES ($id_alumno,$id_tutor)";

                                   if($this->db->query($insert_relacion))
                                   {
                                    echo "ok Padre(".$tutor->documento." ".$tutor->apellido." ".$tutor->nombre.") Insertado en la Relacion alumno(".$tutor->documento_alumno." ".$alumno_aux->last_name." ".$alumno_aux->first_name.") tutor<br>";
                                   }
                                
                                else
                                {
                                   echo "YaExiste-R-P-A <br>";
                                }   
                            
                            }
                            else
                            {
                              echo "PadreYaInsertado <br>";
                            }
                            
                        }
                        //print_r($this->db->last_query());
                        //die();
                    }
                   
                
                }
        
    }
    public function importar_materias()
    {
        //VARIABLES
        $tabla = "bf_materia_nodos"; //--modificar por la division
        
        $group_id = 102;
        $colegio_id = 9;
        //$plan_estudio_id = 1;

        //SCRIPT        
        $sql = "SELECT * FROM ".$tabla;
        $cont = 0;
        $query = $this->db->query($sql);
        if($query->num_rows() > 0)
        {
            foreach ($query->result() as $key) 
            {
                $sql_check= "SELECT * FROM nombresmateria WHERE nombre='".$key->materia."'";
                $query_ch = $this->db->query($sql_check);
                if($query_ch->num_rows() == 0)
                {
                   
                   

                    $insert_docente = "INSERT INTO nombresmateria (nombre) VALUES ('$key->materia')";
                    $this->db->query($insert_docente);
                    echo "insertado: ".$key->materia;
                    echo "<br>";   
                    $cont++;        
                   
                } // fin if el alumno ya existe.
                else
                {
                    echo "Existe: ".$key->materia;
                    echo "<br>";
                    
                }
                
            } // fin for recorrido de los alumnos.
            echo "se insertaron: ".$cont." Materias<br>";
        }
    }
    public function poner_pago_en_1()
    {
        $sql = "SELECT * FROM `users` where id in (SELECT users.id from users JOIN users_groups on users_groups.user_id = users.id and users_groups.colegio_id = 9)";
        $cont = 0;
        $query = $this->db->query($sql);
        if($query->num_rows() > 0)
        {
            foreach ($query->result() as $key) 
            {
                if($key->active == 0)
                {
                    $this->db->query("UPDATE users SET active = '1' WHERE users.id=".$key->id); 
                    echo "EXITO ACTIVE";
                    echo "<br>";
                }
                if($key->pago == 0)
                {
                    $this->db->query("UPDATE users SET pago = '1' WHERE users.id=".$key->id);   echo "EXITO PAGO";
                    echo "<br>"; 
                }
            }
        }
    }

    public function buscar_docentes_que_no_estan()
    {
        $sql = "SELECT * FROM dni_docentes";
        $cont = 0;
        $query = $this->db->query($sql);
        if($query->num_rows() > 0)
        {
            foreach ($query->result() as $keya) 
            {
                $encontro = 0;
                foreach ($query->result() as $keyb) 
                {
                    if($keya->dnia == $keyb->dnib)
                    {
                        $encontro = 1;
                    }
                }
                if($encontro == 0)
                {
                    if($keyb->dnib != '')
                    {
                        echo ",".$keyb->dnia;
                    }
                    
                }
            }
        }
    }  
    public function pasar_alumnos_St()
    {
        //VARIABLES
        $tabla = "inscripciones"; //--modificar por la division
        $division_id = 168;
        $group_id = 36;
        $colegio_id = 4;
        $plan_estudio_id = 2019;

        $sql = "SELECT inscripciones.*, users_groups.user_id as user_id FROM `inscripciones` 
join users_groups on users_groups.id = inscripciones.alumno_id
WHERE inscripciones.divisiones_id = $division_id
GROUP by inscripciones.alumno_id";
        $cont = 0;
        $query = $this->db->query($sql);
        if($query->num_rows() > 0)
            foreach ($query->result() as $key ) 
            {                     
                        $id_user_alumno = $key->user_id;                    
                        //insertar en users_groups
                        $insert_users_groups = "INSERT INTO users_groups (user_id, group_id, colegio_id, estado)
                                VALUES ($id_user_alumno, $group_id, $colegio_id,1)";
                        if($this->db->query($insert_users_groups))
                        {
                            //insertar en inscripcion
                            $id_alumno = $this->db->insert_id();
                            $insert_inscripcion = "INSERT INTO inscripciones (alumno_id, activo, cicloa, divisiones_id)
                                    VALUES ($id_alumno,1,$plan_estudio_id,$division_id)";
                                   //echo $insert_inscripcion."<br>";        
                                   $this->db->query($insert_inscripcion);
                        }    
            //echo "se insertaron: alumnos";
            }
    }
    public function pase_anio_x_division()
    {
      
        $plan = 11;
        $idplan_2019 = 12;
        $division = 235;//2018
        $div_sig = 243;//2019
        
        $sql = "SELECT inscripciones.* 
        FROM `inscripciones` 
        where inscripciones.cicloa = 2018
        and inscripciones.divisiones_id  in (236,237,238,239,240,241,242) ";

         $anio_entero = $this->db->query($sql);
         print_r($this->db->last_query());
        
        foreach ($anio_entero->result() as $key ) 
        {
            $inscripcion = array(
                            'cicloa' => 2019,
                            'divisiones_id' => $div_sig ,
                            'alumno_id' => $key->alumno_id,
                            'activo' => 1,
                        );
            $this->db->insert('inscripciones', $inscripcion);
                            echo " Insertado ";
                            echo "<br>";
            
            
            //die();
            
        }
        
        //}//fin for
                
        echo "finish";

    }
    public function borrar_division_cero()
    {
      
        $plan = 11;
        $idplan_2019 = 12;
        $division = 222;//2018
        $div_sig = 229;//2019
        
        $sql = "SELECT inscripciones.id FROM `inscripciones`
join users_groups on users_groups.id = inscripciones.alumno_id
where divisiones_id = 0 and cicloa = 2019 and users_groups.colegio_id = 9 ";
         $anio_entero = $this->db->query($sql);
         print_r($this->db->last_query());
        
        foreach ($anio_entero->result() as $key ) 
        {
            $delete = "DELETE FROM `inscripciones` WHERE `inscripciones`.`id`= $key->id";
            $delete = $this->db->query($delete);
            
            //die();
            
        }
        
        //}//fin for
                
        echo "finish";

    }
    //

}