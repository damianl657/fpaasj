<?php 
 
class Colegios extends CI_Controller {	
      
	function __construct(){
		parent::__construct();
			
		if(!$this->session->userdata("logged_in")){
               redirect("login");
         }
	}
	//-------------------------------------------------------
	public function get_all_colegios_curl($urlApi, $passApiKey,$idUser)
	{
		$url = $urlApi."/colegio/get_colegios/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			//CURLOPT_POST => true,
			//CURLOPT_POSTFIELDS => array('rol' => $rol,'id_colegio' =>$idcolegio),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		//var_dump($response);
		//$response = substr($response, 3);
		$response = json_decode($response);

		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	public function registrar_colegio_curl($urlApi, $passApiKey, $idusuario, $post)
	{
		$url = $urlApi."/colegio/registrar_colegio/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('nombre'=>$_POST['nombre'], 'cicloa' => $_POST['ciclo_lectivo'],'planestudio' => $_POST['plan_estudio'],'tipo' => $_POST['tipo'],'area' => $_POST['area']),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		//var_dump($response);
		//$response = substr($response, 3);
		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	public function subir_foto_colegio_curl($urlApi, $passApiKey, $idusuario, $data_file)
	{
		$url = $urlApi."/colegio/subir_foto_colegio/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $_FILES,
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		//var_dump($response);
		//$response = substr($response, 3);
		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	public function registrar_foto_colegio_curl($urlApi, $passApiKey, $idusuario, $fileName,$idfile)
	{
		$url = $urlApi."/colegio/registrar_foto_colegio/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('fileName'=>$fileName, 'idfile' => $idfile),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		//var_dump($response);
		//$response = substr($response, 3);
		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	public function get_colegio_curl($urlApi, $passApiKey, $idusuario, $idcolegio)
	{
		$url = $urlApi."/colegio/get_colegio/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('idcolegio'=>$idcolegio),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		//var_dump($response);
		//$response = substr($response, 3);
		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	public function get_all_anios_curl($urlApi, $passApiKey, $idusuario)
	{
		$url = $urlApi."/anio/get_all_anios/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
			CURLOPT_POST => true,
			//CURLOPT_POSTFIELDS => array('idcolegio'=>$idcolegio),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		//var_dump($response);
		//$response = substr($response, 3);
		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	public function get_all_niveles_curl($urlApi, $passApiKey, $idusuario)
	{
		$url = $urlApi."/nivel/get_all_niveles/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
			CURLOPT_POST => true,
			//CURLOPT_POSTFIELDS => array('idcolegio'=>$idcolegio),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		//var_dump($response);
		//$response = substr($response, 3);
		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	public function get_all_especialidades_curl($urlApi, $passApiKey, $idusuario)
	{
		$url = $urlApi."/especialidad/get_all_especialidades/";

		$ch = curl_init();

		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idusuario"),
			CURLOPT_POST => true,
			//CURLOPT_POSTFIELDS => array('idcolegio'=>$idcolegio),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		//var_dump($response);
		//$response = substr($response, 3);
		//$response = json_decode($response);
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
    public function index()
    { 

	    $urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		//$data['urlApi'] = $urlApi;
		//$data['passApiKey'] = $passApiKey;
		$data['idusuario'] = $this->session->userdata('idusuario');
		$idusuario = $this->session->userdata('idusuario');
		

		$band = 0;
		$controlador = "Materias";
		$metodo = "index";
		$accion1 = "add_coegios";

        $colegios = $this->session->userdata("colegios"); 
        $colegiosPermi = array();
        $acciones = array();
        foreach ($colegios as $cole)
		{
			$idcolegio = $cole["id_colegio"];
			$menus = $cole["menu"];
			 $menu_encontrado = false;
		 	foreach ($menus as $menu)
			{	
				if( ($menu->controlador === $controlador) && ($menu->metodo === $metodo))
				{
				    $band = 1;
				    $menu_encontrado = true;
					    //echo "permiso de ver <br>";
				    $key = array_search($idcolegio, array_column($colegiosPermi, 'idcole'));

				    if(!$key)
				    {
					    $arre = array(
					    	'idcole'=>$idcolegio,
					    	'namecole'=>$cole["nombre_colegio"],
					    	'idrol'=>$cole["id_grupo"],
					    	'namerol'=>$cole["rol"],
					    );
					    $colegiosPermi[]=$arre;

					}
				}
			}
			if($menu_encontrado == true)
			{
			//	$divisiones = $this->get_dvisiones_colegio_rol($urlApi, $passApiKey, $idusuario, $idcolegio, $cole["rol"]);
				$colegios_all = $this->get_all_colegios_curl($urlApi, $passApiKey,$idusuario);
				$puede_editar = false;
				foreach ($menus as $menu)
				{	
					if( $menu->nombre === $accion1)
					{
						//echo "permiso de editar <br>";
						$banAddMat = 1;
						$puede_editar = true;
					}
					    
				}
				if($puede_editar == true)
				{
					
					$accion_cole  = array(
								'idcole'=>$idcolegio,
						    	'namecole'=>$cole["nombre_colegio"],
						    	'idrol'=>$cole["id_grupo"],
						    	'namerol'=>$cole["rol"], 
						    	'editar' => 1,
						    //	'divisiones' => $divisiones
						    	
						    	);
					$acciones[]=$accion_cole;
				}
				else
				{
					$accion_cole  = array(
								'idcole'=>$idcolegio,
						    	'namecole'=>$cole["nombre_colegio"],
						    	'idrol'=>$cole["id_grupo"],
						    	'namerol'=>$cole["rol"], 
						    	'editar' => 0,
						    //	'divisiones' => $divisiones
						    	);
					$acciones[]=$accion_cole;
				}
			}
			//print_r($acciones);
			//die();
			//$data['anios'][$idcolegio]=$this->get_anios_x_colegio_curl($urlApi, $passApiKey, $data['idusuario'], $idcolegio, $cole["rol"]);
			
		}
		//print_r($acciones);
		


		
        if($band == 1)
        {
    		$data['colegios'] = $colegiosPermi;
    		$data['acciones'] = $acciones;
    		$data['colegios_all'] = $colegios_all->colegios;
    		$data['ciclos_lectivos'] = $colegios_all->ciclos_lectivos;
    		$data['planes_estudio'] = $colegios_all->planes_estudio;
    		$data['tipos_colegios'] = $colegios_all->tipos_colegios;
    		//print_r($colegios_all->colegios);
			//die();
		    $data['contenido'] = 'panel/colegios/index';
			$this->load->view('include/template_colegio',$data);
				
		}
		else redirect(404);
		
		
	}
	public function registrar_colegio()
	{
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$urlApi_foto = $this->variables->get_url_img_colegios();
		
		$passApiKey = $this->variables->get_apikey();
		$idusuario = $this->session->userdata('idusuario');
		//print_r($_POST);
		//$id_colegio = $_GET["id_colegio"];
		//print_r($_FILES['foto']);

		$request = $this->registrar_colegio_curl($urlApi, $passApiKey, $idusuario, $_POST);
		$request = json_decode($request);

		//print_r($request->ultimoId);
		$idcolegio = $request->ultimoId;//id de colegio
		if(isset($_FILES['foto']))
		{
			//echo $urlApi_foto;
			//echo $_SERVER['DOCUMENT_ROOT'] ;
			//die();
			$this->load->helper(array('form', 'url'));
            //$config['upload_path'] = './uploads/';
            $config['allowed_types'] = '*';
            $config['max_size']    = '30000';
            $this->load->library('upload', $config);
			if (!empty($_FILES['archivo']['name']))
        	{
            	$nombre_file = $_FILES['archivo']['name'];
            	$ruta = $urlApi_foto."/".$nombre_file;
            //$ruta = base_url('uploads/'.$nombre_file);
	            if (file_exists($ruta)) 
	            {
	                    unlink($ruta);
	            }
        	}

        	// directory in which the uploaded file will be moved
        	$fileTmpPath = $_FILES['foto']['tmp_name'];
			$fileName = $idcolegio."_".$_FILES['foto']['name'];
			
            $fileName = str_replace(" ", "_", $fileName);
			$fileSize = $_FILES['foto']['size'];
			$fileType = $_FILES['foto']['type'];
        	$fileNameCmps = explode(".", $fileName);
			$fileExtension = strtolower(end($fileNameCmps));
			$uploadFileDir = $urlApi_foto.'/';
			
			$dest_path = $uploadFileDir . $fileName;
			//echo $dest_path;
			$allowedfileExtensions = array('jpg', 'gif', 'png', 'zip', 'txt', 'xls', 'doc','jpeg');
			if (in_array($fileExtension, $allowedfileExtensions)) 
			{
				if(move_uploaded_file($fileTmpPath, $dest_path))
				{
				  //$message ='exito';
					$request = $this->registrar_foto_colegio_curl($urlApi, $passApiKey, $idusuario, $fileName,$idcolegio);

				}
				else
				{
				  //$message = 'fallo';
				}
				//echo $message;
				//echo $dest_path;
			}
				

			//die();
			
		}

		//obtener colegio insertado
		$colegio = $this->get_colegio_curl($urlApi, $passApiKey, $idusuario, $idcolegio);
		print_r($colegio);
	}

	public function divisiones()
	{
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		//$data['urlApi'] = $urlApi;
		//$data['passApiKey'] = $passApiKey;
		$data['idusuario'] = $this->session->userdata('idusuario');
		
		$idusuario = $this->session->userdata('idusuario');
		
		//$idcolegio = $_POST['colegio'];
       // $idanio = $_POST['anio'];
      //  $materias = $_POST['materias'];

       
		
		

		$band = 0;
		$controlador = "Colegios";
		$metodo = "divisiones";
		$accion1 = "add_divisiones";

        $colegios = $this->session->userdata("colegios"); 
        $colegiosPermi = array();
        $acciones = array();
        $banAddMat = 0;
		foreach ($colegios as $cole)
		{
			$idcolegio = $cole["id_colegio"];
			$menus = $cole["menu"];
			 $menu_encontrado = false;
		 	foreach ($menus as $menu)
			{	
				if( ($menu->controlador === $controlador) && ($menu->metodo === $metodo))
				{
				    $band = 1;
				    $menu_encontrado = true;
					    //echo "permiso de ver <br>";
				    $key = array_search($idcolegio, array_column($colegiosPermi, 'idcole'));

				    if(!$key)
				    {
					    $arre = array(
					    	'idcole'=>$idcolegio,
					    	'namecole'=>$cole["nombre_colegio"],
					    	'idrol'=>$cole["id_grupo"],
					    	'namerol'=>$cole["rol"],
					    );
					    $colegiosPermi[]=$arre;

					}
				}
			}
			if($menu_encontrado == true)
			{
			//	$divisiones = $this->get_dvisiones_colegio_rol($urlApi, $passApiKey, $idusuario, $idcolegio, $cole["rol"]);
				$puede_editar = false;
				foreach ($menus as $menu)
				{	
					if( $menu->nombre === $accion1)
					{
						//echo "permiso de editar <br>";
						$banAddMat = 1;
						$puede_editar = true;
					}
					    
				}
				if($puede_editar == true)
				{
					
					$accion_cole  = array(
								'idcole'=>$idcolegio,
						    	'namecole'=>$cole["nombre_colegio"],
						    	'idrol'=>$cole["id_grupo"],
						    	'namerol'=>$cole["rol"], 
						    	'editar' => 1,
						    //	'divisiones' => $divisiones
						    	
						    	);
					$acciones[]=$accion_cole;
				}
				else
				{
					$accion_cole  = array(
								'idcole'=>$idcolegio,
						    	'namecole'=>$cole["nombre_colegio"],
						    	'idrol'=>$cole["id_grupo"],
						    	'namerol'=>$cole["rol"], 
						    	'editar' => 0,
						    //	'divisiones' => $divisiones
						    	);
					$acciones[]=$accion_cole;
				}
			}
			//print_r($acciones);
			//die();
			//$data['anios'][$idcolegio]=$this->get_anios_x_colegio_curl($urlApi, $passApiKey, $data['idusuario'], $idcolegio, $cole["rol"]);
			
		}
		//print_r($acciones);
		if($banAddMat == 1){
			/*$arre2 = array(
		    	'accion'=>$accion1,
		    );*/
		    
		}


		
        if($band == 1)
        {
    		$data['colegios'] = $colegiosPermi;
    		$data['acciones'] = $acciones;
    		
			//$data['nombresmaterias']=$this->get_nombres_materias_curl($urlApi, $passApiKey, $data['idusuario']);
		    $data['contenido'] = 'panel/colegios/divisiones';
			$this->load->view('include/template_colegio',$data);
				
		}
		else redirect(404);
	}
	public function get_all_anios_ajax()
	{
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		$data['idusuario'] = $this->session->userdata('idusuario');
		$idusuario = $this->session->userdata('idusuario');
		$request = $this->get_all_anios_curl($urlApi, $passApiKey, $idusuario);
		//$request json_encode($request);
		print_r($request);
		//$data = array('request' =>  $request);
		//print_r($data);

	}
	public function get_all_niveles_ajax()
	{
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		$data['idusuario'] = $this->session->userdata('idusuario');
		$idusuario = $this->session->userdata('idusuario');
		$request = $this->get_all_niveles_curl($urlApi, $passApiKey, $idusuario);
		//$request json_encode($request);
		print_r($request);
	}
	public function get_all_especialidades_ajax()
	{
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		$data['idusuario'] = $this->session->userdata('idusuario');
		$idusuario = $this->session->userdata('idusuario');
		$request = $this->get_all_especialidades_curl($urlApi, $passApiKey, $idusuario);
		//$request json_encode($request);
		print_r($request);
	}

}