<?php

    

    defined('BASEPATH') OR exit('No direct script access allowed');



    class Modelo_usuario extends CI_Model {



        public function __construct()

        {

            // Call the CI_Model constructor

            parent::__construct();

            $this->load->database();

        }



        public function guardar_registerid_gcm($userid, $token)// gcm = google cloud message

        {

            //me fijo si ya esta guardado

            if($this->existe_registerid_gcm($userid, $token))

            {

                return false; //si ya existe devuelvo false

            }

            else

            {

                $sql = "INSERT INTO push (userid, token) 

                        VALUES ($userid, '$token')";

                $query = $this->db->query($sql);

                return $query;

            }

        }

        

        public function obtener_email($correo)

        {

            $this->db->where('email',$correo);

            $query = $this->db->get('users');

            return $query->row();

        }

        public function obtener_documento($dni)

        {

            /*$this->db->where('documento',$dni);

            $query = $this->db->get('users');*/



            $sql = "SELECT * FROM users where documento = '$dni'";

                $query = $this->db->query($sql);

            return $query->row();

            



            /*if($query->result() > 0)

               {

                    //return null;

                    return $query->row();

               } 

               else 

               {

                //return $query->row();

                return null;

               }*/

        }

       

        public function registrar_pago($userid,$data)

        {

            $this->db->where("id",$userid);

            $this->db->update("users",$data);

        }

        public function borrar_pago($userid,$data)

        {

            $this->db->where("id",$userid);

            $this->db->update("users",$data);

        }

        public function activar_users($userid,$data)

        {

            $this->db->where("id",$userid);

            $this->db->update("users",$data);

        }

        public function update_users($userid,$data)

        {

            $this->db->where("id",$userid);

            $this->db->update("users",$data);

        }

        public function verificar_user_group($data)

        {

            $this->db->where($data);

            $query = $this->db->get('users_groups');

            return $query->row();

        }

        public function desactivar_users($userid,$data)

        {

            $this->db->where("id",$userid);

            $this->db->update("users",$data);

        }

        public function obtener_users_colegio($id_colegio, $id_rol)

        {

            $sql = "SELECT users.id as id, users.last_name as apellido, users.first_name as nombre, users.documento, users.pago, users.email,users.active, users_groups.id as id_users_groups, users_groups.group_id as group_id, users_groups.colegio_id, groups.name as rol, groups.id as id_rol

            FROM users_groups

            join users on users.id = users_groups.user_id

            join groups on groups.id = users_groups.group_id   

            where colegio_id = $id_colegio and groups.id = $id_rol

            AND users_groups.estado = 1";



            $query = $this->db->query($sql);

            return $query->result();

        }

        public function delete_alu_tutor($id_alumno, $id_tutor)

        {

            $sql ="DELETE FROM tutoralumno WHERE tutoralumno.alumno_id = $id_alumno AND tutoralumno.tutor_id = $id_tutor";

            $query = $this->db->query($sql);

            return;



        }

        public function get_tutores_alu($id_alumno)

        {

            $sql ="SELECT * FROM `tutoralumno` where alumno_id = $id_alumno";

            $query = $this->db->query($sql);

            return $query->result();

        }

        public function obtener_id_rol_alumno_colegio($id_colegio)

        {

            $sql ="SELECT * FROM `groups` where name ='Alumno' and id_colegio = $id_colegio";

            $query = $this->db->query($sql);

            return $query->row()->id;

        }

        public function get_tutores_colegio($id_colegio)

        {

            $sql="SELECT users.id as user_id, users.first_name as nombre, users.last_name as apellido, users.documento as documento

                FROM users 

                JOIN users_groups ON users_groups.user_id = users.id

                JOIN groups ON groups.id = users_groups.group_id

                WHERE users_groups.colegio_id = $id_colegio

                AND users_groups.estado = 1

                AND groups.name = 'Tutor'

                ORDER BY users.last_name";

            $query = $this->db->query($sql);

            return $query->result();

        }

        public function set_tutor_alumno($id_alumno,$id_tutor)

        {

            $sql0 = "SELECT * FROM tutoralumno WHERE alumno_id = $id_alumno AND tutor_id = $id_tutor";

            $query0 = $this->db->query($sql0);



            if($query0->num_rows()>0)

                return false;

            else{

                $sql = "INSERT INTO tutoralumno (alumno_id, tutor_id) VALUES ($id_alumno, $id_tutor)";

                return $this->db->query($sql);

            }

        }

        public function obtener_users_group($user_id, $idcolegios)

        {

            $sql = "SELECT users_groups.*, groups.name as grupo, nodocolegio.nombre as colegio

            FROM users_groups

            JOIN groups ON groups.id = users_groups.group_id

            JOIN nodocolegio ON nodocolegio.id = users_groups.colegio_id

            WHERE users_groups.user_id = $user_id

            AND users_groups.colegio_id IN ($idcolegios)

            AND users_groups.estado=1

            ORDER BY nodocolegio.nombre, groups.name

            ";

            $query = $this->db->query($sql);

            return $query->result();

        }

        public function obtener_users_group_alumno($user_id)

        {

            $sql = "SELECT *

            FROM users_groups

            where users_groups.user_id = $user_id";

            $query = $this->db->query($sql);

            return $query->row();

        }



        public function obtener_users_group_alumno2($user_id, $colegio_id)

        {

            $sql = "SELECT *

            FROM users_groups

            WHERE users_groups.user_id = $user_id

            AND users_groups.colegio_id = $colegio_id LIMIT 1";

            $query = $this->db->query($sql);

            if($query->num_rows()>0)

                return $query->row();

            else return false;

        }

        

        public function obtener_users_group2($ids)

        {

            $sql = "SELECT *

            FROM users_groups

            where id IN ($ids)

            AND estado =1 ";



            $query = $this->db->query($sql);



            if($query->num_rows()>0)

                return $query->result(); 

            else

                return false;

        }



        public function get_roles_colegio_users($userid,$idcolegio)
        {
            $sql = "SELECT groups.name as nombre_grupo, groups.id as id_grupo, nodocolegio.id as colegio_id, nodocolegio.nombre as nombre_colegio, groups.orden as orden_grupo 
            FROM users 
            JOIN users_groups ON users_groups.user_id = users.id 
            JOIN groups ON groups.id = users_groups.group_id 
            JOIN nodocolegio ON nodocolegio.id = users_groups.colegio_id 
            WHERE users.id = $userid
            AND nodocolegio.id = $idcolegio
            AND users_groups.estado = 1";
            $query = $this->db->query($sql);
            return $query->result();
           // $sql = "CALL get_roles_colegio_users(".$userid.",".$idcolegio.")";
            //$query = $this->db->query($sql);
            $resultado = $query->result();
           // mysqli_next_result( $this->db->conn_id );
            return $resultado;
        }

        public function get_obtener_colegios_x_users($userid)
        {

            $sql = "SELECT groups.name as nombre_grupo, groups.id as id_grupo, nodocolegio.id as colegio_id, nodocolegio.nombre as nombre_colegio, groups.orden as orden_grupo, nodocolegio.areas

            FROM users JOIN users_groups ON users_groups.user_id = users.id 
            JOIN groups ON groups.id = users_groups.group_id 
            JOIN nodocolegio ON nodocolegio.id = users_groups.colegio_id 
            WHERE users.id = $userid
            AND users_groups.estado = 1
            GROUP BY nodocolegio.id";
            $query = $this->db->query($sql);
            return $query->result();

            //$sql = "CALL get_obtener_colegios_x_users(".$userid.")";
            //$query = $this->db->query($sql);
            //$resultado = $query->result();
            //mysqli_next_result( $this->db->conn_id );
            return $resultado;
        }

        public function rolex_colegios_all_users($userid)

        {

            $sql = "SELECT users.id as id_user, users.last_name as apellido, users.first_name as nombre, groups.id as id_grupo, groups.name as nombre_grupo, nodocolegio.id as id_colegio, nodocolegio.nombre as nombre_colegio, nodocolegio.color as color_colegio 

            FROM users 

            join users_groups on users_groups.user_id = users.id 

            join groups on groups.id = users_groups.group_id 

            join nodocolegio on nodocolegio.id = users_groups.colegio_id

            where users.id = $userid

            AND users_groups.estado = 1";

            $query = $this->db->query($sql);

            return $query->result();

        }



        public function get_escuelas_all_userd($userid)

        {

            $sql = "SELECT nodocolegio.id as colegio_id, nodocolegio.nombre as nombre_colegio, nodocolegio.color as color_colegio 

            FROM users 

            JOIN users_groups ON users_groups.user_id = users.id 

            JOIN nodocolegio ON nodocolegio.id = users_groups.colegio_id 

            WHERE users.id = $userid

            AND users_groups.estado = 1

            GROUP BY nodocolegio.id";



            $query = $this->db->query($sql);

            return $query->result();

        }



        public function roles_colegios_all_users_group_by($userid)

        {

            $sql = "SELECT groups.id as id_grupo, groups.name as nombre_grupo,groups.name as nombre_grupo, nodocolegio.id as id_colegio, nodocolegio.nombre as nombre_colegio, nodocolegio.color as color_colegio

                    FROM users

                    join users_groups on users_groups.user_id = users.id

                    join groups on groups.id = users_groups.group_id

                    join nodocolegio on nodocolegio.id = users_groups.colegio_id

                    where users.id = $userid

                    AND users_groups.estado = 1

                    GROUP BY  groups.name";

            $query = $this->db->query($sql);

            return $query->result();

        }



        public function obtener_roles_delcolegio($id_colegio)

        {

            $sql = "SELECT *

            FROM groups

            where id_colegio = $id_colegio and estado = 1

            ORDER BY name";

            $query = $this->db->query($sql);

            return $query->result();

        }



        public function obtener_rol_mayorPrioridad($id_colegio,$userid)

        {

            $sql = "

                SELECT MIN(orden) as orden

                FROM users_groups

                JOIN groups ON groups.id = users_groups.group_id

                WHERE users_groups.user_id = $userid

                AND users_groups.colegio_id = $id_colegio

                ";

            $query = $this->db->query($sql);

            return $query->result();

        }



        public function obtener_roles_delcolegio_menorOrden($id_colegio,$orden)

        {

            $sql = "SELECT * FROM groups WHERE id_colegio = $id_colegio AND orden >= $orden ORDER BY name";

            $query = $this->db->query($sql);

            return $query->result();

        }



        public function set($data)

        {

            $this->db->insert('users', $data);

            $error = $this->db->error();

            if($error['code']=="0")

            {

                return $this->db->error();

            }   

            else return $this->db->error();

        }

        public function set_user_group($data)

        {

            $this->db->insert('users_groups', $data);

            $error = $this->db->error();

            if($error['code']=="0")

            {

                return $this->db->error();

            }   

            else return $this->db->error();

        }

       



        public function existe_registerid_gcm($userid, $token)

        {            

            $sql = "SELECT id 

                    FROM push 

                    WHERE token = '".$token."'

                    AND userid = ".$userid;

            $query = $this->db->query($sql);

            return $query->result();

        }



        /*Actualiza los datos del celular para un token y usuario especifico*/

        public function guardar_datos_cel($datos,$userid, $token){

            $this->db->where("userid",$userid);

            $this->db->where("token",$token);

            return $this->db->update("push",$datos);

        }



        /*public function update_pasos($idusergrupo,$paso)

        {

            $sql = "UPDATE users_groups SET configuracion='$paso' WHERE id='$idusergrupo' ";

            $query = $this->db->query($sql);

            return $query;

        }*/



        public function update_usergroup($were,$set)

        {

           return $this->db->update('users_groups', $set, $were);

        }



        public function obtener_pasos($idusergrupo)

        {

            $sql = "SELECT configuracion FROM users_groups WHERE id='$idusergrupo' ";

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query;

            }

            else{

                return false;

            }

        }

        



        public function get_rolesXidusuario($Iduser)

        {

            $sql = "SELECT users_groups.id,  groups.id as idgrupo, groups.name as nombregrupo, groups.description as descripciongrupo, nodocolegio.id as idcolegio, nodocolegio.nombre as nombrecolegio, users_groups.configuracion, groups.orden

                    FROM users_groups JOIN groups, nodocolegio

                    WHERE users_groups.user_id='$Iduser' 

                    AND users_groups.group_id = groups.id

                    AND users_groups.colegio_id = nodocolegio.id

                    AND users_groups.estado = 1

                    ORDER BY orden

                    ";

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        }



        public function get_permisosXidusuario($Iduser)

        {

            $sql = "SELECT users_groups.id,  groups.id as idgrupo, groups.name as nombregrupo, nodocolegio.id as idcolegio, nodocolegio.nombre as nombrecolegio, groups.orden, reglas_comunicacion.id as reglaid,reglas_comunicacion.roles_id, reglas_comunicacion.id_menus_acciones

                    FROM users_groups 

                    JOIN groups  ON users_groups.group_id = groups.id

                    JOIN nodocolegio ON users_groups.colegio_id = nodocolegio.id

                    JOIN reglas_comunicacion ON users_groups.colegio_id = reglas_comunicacion.colegio_id AND users_groups.group_id = reglas_comunicacion.group_id



                    WHERE users_groups.user_id='$Iduser' 

                    AND users_groups.estado = 1

                    ORDER BY orden

                    ";

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        }



        

        public function get_rolesXidusuario_orden($Iduser)

        {

            $sql = "

                    SELECT * FROM

                    (SELECT users_groups.id,  groups.id as idgrupo, groups.name as nombregrupo, groups.description as descripciongrupo, nodocolegio.id as idcolegio, nodocolegio.nombre as nombrecolegio, users_groups.configuracion, groups.orden

                    FROM users_groups JOIN groups, nodocolegio

                    WHERE users_groups.user_id='$Iduser' 

                    AND users_groups.group_id = groups.id

                    AND users_groups.colegio_id = nodocolegio.id

                    AND users_groups.estado = 1

                    ORDER BY idcolegio, orden

                    )T

                    GROUP BY idcolegio

                    ";

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        }



        public function get_rolesXrolesname($nombres, $idcolegio)

        {

            $sql = "SELECT *

                    FROM groups

                    WHERE name IN ($nombres)

                    AND id_colegio = $idcolegio

                    ORDER BY name

                    ";

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        }



        public function get_menu($ids)

        {

            $sql = "SELECT *

                    FROM menu_acciones

                    WHERE id_menu IN ($ids) and estado = 1

                    ORDER BY orden

                    ";

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        }





        public function get_menu2($ids,$key)
        {
            $bd = $this->db->database;
            $sql = "SELECT *
                    FROM menu_acciones
                    JOIN $bd.keys on $bd.keys.id = menu_acciones.id_key
                    WHERE menu_acciones.id_menu IN ($ids) and estado = 1 and $bd.keys.key = '$key'
                    ORDER BY orden
                    ";
            $query = $this->db->query($sql);
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }

        public function get_menu_general($key)

        {

            $bd = $this->db->database;

            $sql = "SELECT *

                    FROM menu_acciones

                    JOIN $bd.keys on $bd.keys.id = menu_acciones.id_key

                    WHERE tipo= 'menu' or tipo= 'menu_padre' or tipo= 'sub_menu' and estado = 1 and $bd.keys.key = '$key'

                    ORDER BY orden

                    ";

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        }

        public function get_rolesXrolesid($ids, $idcolegio)

        {

            $sql = "SELECT *

                    FROM groups

                    WHERE id IN ($ids)

                    AND id_colegio = $idcolegio

                    ORDER BY name

                    ";

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        }

        public function get_reglas2($idcolegio,$idgrupo,$key)//prueba no borrar. damian

        {

            $sql = "SELECT *

                    FROM reglas_comunicacion 

                    WHERE colegio_id = $idcolegio AND group_id = $idgrupo

                    "; //WHERE name <> 'Nodos'

            $query = $this->db->query($sql);

 

            return $query->row();

        }

        



        public function get_rolesTodos($idcolegio)

        {

            $sql = "SELECT *

                    FROM groups

                    WHERE id_colegio = $idcolegio

                    AND estado=1

                    ORDER BY name

                    "; //WHERE name <> 'Nodos'

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        }



        public function get_usuariosColegio($idcolegio) //todos los usuarios del colegio

        {

            $sql = "SELECT user_id, 

                           first_name, 

                           last_name, 

                           recibir_notificacion, 

                           group_id, email, active

                    FROM users_groups 

                    JOIN users ON users_groups.user_id = users.id

                    WHERE users_groups.colegio_id = '$idcolegio' 

                    AND users_groups.estado = 1

                    GROUP BY user_id

                    ORDER BY users.last_name, users.first_name ASC

                    ";

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        }





        public function get_usuariosColegio_2($idcolegio) //todos los usuarios del colegio

        {

            $sql = "SELECT user_id

                    FROM users_groups 

                    WHERE users_groups.colegio_id = '$idcolegio' 

                    AND users_groups.estado = 1

                    GROUP BY user_id

                    ";

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        }



        

        public function get_usuariosXrol($idcolegio,$grupo) //todos los usuarios del colegio y rolname

        {

            $sql = "SELECT users_groups.id, users.first_name, users.last_name, users_groups.user_id

                    FROM users_groups JOIN users, groups

                    WHERE users_groups.colegio_id = '$idcolegio' 

                    AND users_groups.user_id = users.id

                    AND users_groups.group_id = groups.id

                    AND users_groups.estado = 1

                    AND groups.name = '$grupo' 

                    GROUP BY id

                    ORDER BY users.last_name, users.first_name  ASC

                    "; //AND users.active=1

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        }



        public function get_usuariosXrol2($idcolegio,$grupo,$ids) //lo uso para guardar evento dirigido
        {
            // $sql = "SELECT users.id, users.first_name, users.last_name
            //         FROM users_groups JOIN users, groups
            //         WHERE users_groups.colegio_id = '$idcolegio' 
            //         AND users_groups.user_id = users.id
            //         AND users_groups.group_id = groups.id
            //         AND users_groups.estado = 1
            //         AND users.id IN ($ids)
            //         AND groups.name = '$grupo' 
            //         GROUP BY id
            //         ORDER BY users.last_name, users.first_name 
            //         "; //AND users.active=1
            // $query = $this->db->query($sql);
            /*
            14/11/2019
            CREADO EL SIGUIENTE PROCEDIMIENTO ALMACENADO Q REEMPLAZA LA FUNCION ANTERIOR
            */
            $sql = 'CALL get_usuariosXrol2('.$idcolegio.',"'.$grupo.'","'.$ids.'")';
            $query = $this->db->query($sql);
            $resultado = $query;  
            mysqli_next_result( $this->db->conn_id );
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }





        public function get_usuariosXroles($idcolegio, $idroles) //todos los usuarios del colegio

        {

            $sql = "SELECT user_id, 

                           first_name, 

                           last_name, 

                           recibir_notificacion, 

                           group_id, email, active

                    FROM users_groups JOIN users

                    WHERE colegio_id = '$idcolegio' 

                    AND group_id IN ($idroles)

                    AND users_groups.user_id=users.id

                    AND users_groups.estado = 1

                    AND users.active=1

                    GROUP BY user_id

                    ORDER BY users.last_name, users.first_name ASC

                    ";

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        }







        public function get_users_id($id)
        {
            $sql = "SELECT *
                    FROM users
                    WHERE users.id = $id
                    ";
           $query = $this->db->query($sql);
           return $query->row();
           /* $sql = "CALL get_users_id(".$id.")";
            $query = $this->db->query($sql);
            $resultado = $query;
            mysqli_next_result( $this->db->conn_id );
            return $resultado->row();*/
        }





        public function get_users_ids($ids)

        {

            $sql = "SELECT id,

                    documento,

                    email,

                    fecha_pago,

                    first_name,

                    last_name,

                    foto,

                    iteration,

                    pago,

                    phone,

                    recibir_notificacion,

                    sexo,

                    username,

                    created_on,

                    last_login,

                    active

                    FROM users

                    WHERE id IN ($ids) 

                    ";

            $query = $this->db->query($sql);

            if($query->num_rows()>0)

                return $query->result();

            else

                return false;

        }





        public function obtener_usersgroup_users_roles($ids)

        {

            $sql = "SELECT  users.id,

                            users.documento,

                            users.first_name,

                            users.last_name,

                            groups.name as rol

            FROM users_groups

            JOIN groups ON groups.id = users_groups.group_id

            JOIN users ON users.id = users_groups.user_id



            where users_groups.id IN ($ids)

            AND users_groups.estado = 1";



            $query = $this->db->query($sql);



            if($query->num_rows()>0)

                return $query->result(); 

            else

                return false;

        }





        public function obtener_correo($email)

        {

            $sql = "SELECT *

                    FROM users

                    WHERE email = '$email' 

                    ";

            $query = $this->db->query($sql);

            //return $query->row();

            if($query->num_rows()>0)

            {

                return true;

            }

            else

            {

                return false;

            }



        }

        public function get_rol_id($id)

        {

            $sql = "SELECT *

                    FROM groups

                    WHERE id = '$id' 

                    ";

            $query = $this->db->query($sql);

            return $query->row();

        }

        public function get_usuariosColegio2($idcolegio, $ids_roles) //usuarios por roles

        {

                $sql = "SELECT DISTINCT group_id, 

                               user_id, 

                               first_name, 

                               last_name, 

                               recibir_notificacion, 

                               group_id, email, name as rol, active

                        FROM users_groups JOIN users, groups

                        WHERE users_groups.colegio_id = '$idcolegio' 

                        AND users_groups.group_id IN ($ids_roles) 

                        AND users_groups.group_id = groups.id

                        AND users_groups.user_id = users.id

                        AND users_groups.estado = 1

                        ORDER BY groups.name, last_name, first_name

                        "; //AND users.active=1 //AND users.email <> ''

          

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        }



        /*public function get_usuariosEspecificos_cole($ids, $colegio) //se usa para el editar evento

        {

            $sql = "SELECT user_id, 

                           first_name, 

                           last_name, 

                           recibir_notificacion, 

                           group_id, email, active, groups.name

                    FROM users_groups 

                    JOIN users ON users_groups.user_id = users.id

                    WHERE users_groups.user_id IN ($ids)

                    AND users_groups.colegio_id = $colegio

                    AND users_groups.estado = 1

                    GROUP BY user_id

                    "; //JOIN groups ON users_groups.group_id = groups.id

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        }*/     



        /**
         * Acepta un string que contiene los "id" de usuarios y retorna
         * solamente aquellos que tienen el campo "users_groups.estado = 1"
         *
         * @param	string	$ids (cadena de texto separada por "," con los id de usuarios)
         * @return	string  id de usuarios con "users_groups.estado = 1"
         */
        public function get_usuariosEspecificos($ids) //algunos usuarios del colegio
        {
            // $sql = "SELECT user_id,first_name,last_name,recibir_notificacion,group_id, email, active, groups.name
            //         FROM users_groups 
            //         JOIN users ON users_groups.user_id = users.id
            //         JOIN groups ON users_groups.group_id = groups.id
            //         WHERE users_groups.user_id IN ($ids)
            //         AND users_groups.estado = 1
            //         GROUP BY user_id"; //AND users.active=1
            //--------------------------------------------------------------
            //19-11-2019 
            //Creado procedimiento almacenado para hacer la consulta anterior
            //usada en el envio de notificaciones
            //---------------------------------------------------------------
            $sql = 'CALL get_usuariosEspecificos("'.$ids.'")';
            $query = $this->db->query($sql);
            $resultado = $query;  
            mysqli_next_result( $this->db->conn_id );
            if($query->num_rows()>0){
                return $query->result();
            }
            else{
                return false;
            }
        }



        public function get_usuariosEspecificos2($idcolegio, $ids, $ids_roles) //algunos usuarios del colegio

        {

            $sql = "SELECT DISTINCT group_id, 

                               user_id, 

                               first_name, 

                               last_name, 

                               recibir_notificacion, 

                               group_id, email, name as rol, active

                        FROM users_groups JOIN users, groups

                        WHERE users_groups.colegio_id = '$idcolegio' 

                        AND users_groups.user_id IN ($ids) 

                        AND users_groups.group_id IN ($ids_roles)

                        AND users_groups.group_id = groups.id

                        AND users_groups.user_id = users.id

                        AND users_groups.estado = 1

                        ORDER BY groups.name, last_name, first_name

                        "; 

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        }



        public function get_usuariosParaEnviar($todos, $alumnos, $idcolegio) //los usuarios a los que debo enviar, devuelve todo preparado :) NO TOCAR!!

        {

            $sql = "SELECT user_id, 

                           first_name, 

                           last_name, 

                           recibir_notificacion, 

                           group_id, email, active, groups.name, T.alumno_id, pago, pago_alumno

                    FROM users_groups 

                    JOIN users ON users_groups.user_id = users.id

                    JOIN groups ON users_groups.group_id = groups.id

                    LEFT JOIN 

                        (

                         SELECT tutor_id, alumno_id, pago as pago_alumno

                         FROM tutoralumno JOIN users ON tutoralumno.alumno_id = users.id

                         WHERE tutoralumno.alumno_id IN ($alumnos)

                        )T

                        ON users_groups.user_id = T.tutor_id AND groups.name='Tutor'

                    WHERE users_groups.user_id IN ($todos)

                    AND users_groups.colegio_id = $idcolegio

                    AND users_groups.estado = 1

                    AND groups.name<>'Tutor'

                    GROUP BY user_id

                    ORDER BY groups.name

                    "; //group by users_groups.user_id, alumno_id



            // "GROUP BY user_id, alumno_id" ---> esto hace que NO traiga usuarios duplicados, la unica manera que puede hacerlo, es para los tutores cuando tienen mas de un hijo.



            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        } //(3,6,10,20,21,15,16,17,11)



        public function get_usuariosParaEnviar2($todos, $alumnos, $idcolegio) //los usuarios a los que debo enviar, devuelve todo preparado :) NO TOCAR!!

        {

            $sql = "SELECT user_id, 

                           first_name, 

                           last_name, 

                           recibir_notificacion, 

                           group_id, email, active, groups.name, T.alumno_id, pago, pago_alumno

                    FROM users_groups 

                    JOIN users ON users_groups.user_id = users.id

                    JOIN groups ON users_groups.group_id = groups.id

                    LEFT JOIN 

                        (

                         SELECT tutor_id, alumno_id, pago as pago_alumno

                         FROM tutoralumno JOIN users

                         WHERE tutoralumno.alumno_id = users.id

                         AND tutoralumno.alumno_id IN ($alumnos)

                        )T

                        ON users_groups.user_id = T.tutor_id AND groups.name='Tutor'

                    WHERE users_groups.user_id IN ($todos)

                    AND users_groups.colegio_id = $idcolegio

                    AND users_groups.estado = 1

                    AND groups.name='Tutor'

                    GROUP BY user_id, alumno_id

                    ORDER BY groups.name

                    "; //group by users_groups.user_id



            // "GROUP BY user_id, alumno_id" ---> esto hace que NO traiga usuarios duplicados, la unica manera que puede hacerlo, es para los tutores cuando tienen mas de un hijo.



            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        } //(3,6,10,20,21,15,16,17,11)



        public function get_usuariosParaEnviar3($alumnos, $idcolegio) //los usuarios a los que debo enviar, devuelve todo preparado :) NO TOCAR!!

        {

            $sql = "SELECT user_id, 

                           first_name, 

                           last_name, 

                           recibir_notificacion, 

                           group_id, email, active, groups.name, T.alumno_id, pago, pago_alumno

                    FROM users_groups 

                    JOIN users ON users_groups.user_id = users.id

                    JOIN groups ON users_groups.group_id = groups.id

                    LEFT JOIN 

                        (

                         SELECT tutor_id, alumno_id, pago as pago_alumno

                         FROM tutoralumno JOIN users ON tutoralumno.alumno_id = users.id

                         WHERE tutoralumno.alumno_id IN ($alumnos)

                        )T

                        ON users_groups.user_id = T.tutor_id AND groups.name='Tutor'

                    WHERE users_groups.colegio_id = $idcolegio

                    AND users_groups.estado = 1

                    GROUP BY user_id

                    ORDER BY groups.name

                    ";  //users_groups.user_id IN ($todos)



            //print_r($sql); die();



            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        } //(3,6,10,20,21,15,16,17,11)



        public function get_usuariosParaEnviar3_1($excentos, $alumnos, $idcolegio) //los los usuarios del colegio menos los indicados. NO TOCAR!!

        {

            $sql = "SELECT user_id, 

                           first_name, 

                           last_name, 

                           recibir_notificacion, 

                           group_id, email, active, groups.name, T.alumno_id, pago, pago_alumno

                    FROM users_groups 

                    JOIN users ON users_groups.user_id = users.id

                    JOIN groups ON users_groups.group_id = groups.id

                    LEFT JOIN 

                        (

                         SELECT tutor_id, alumno_id, pago as pago_alumno

                         FROM tutoralumno JOIN users ON tutoralumno.alumno_id = users.id

                         WHERE tutoralumno.alumno_id IN ($alumnos)

                        )T

                        ON users_groups.user_id = T.tutor_id AND groups.name='Tutor'

                    WHERE users_groups.user_id NOT IN ($excentos)

                    AND users_groups.colegio_id = $idcolegio

                    AND users_groups.estado = 1

                    GROUP BY user_id

                    ORDER BY groups.name

                    ";  //



            //print_r($sql); die();



            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        } 





        public function get_usuarioInstitucion($idcolegio) //usuario Institucion del colegio

        {

            $sql = "SELECT users_groups.user_id

                    FROM users_groups JOIN groups

                    WHERE users_groups.colegio_id = '$idcolegio' 

                    AND users_groups.group_id = groups.id

                    AND users_groups.estado = 1

                    AND groups.name='Institucion' LIMIT 1

                    ";

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        }









        public function get_push($Iduser) //Obtengo los datos necesario para el push para un usuario

        {

            $sql = "SELECT * FROM push WHERE userid = $Iduser";

            

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        }



        //Elimina un token_gcm para que no le lleguen notificaciones

        //Generalmente esto va a ocurrir cuando el usuario se desconecte

        //como usuario en la app movil

        public function delete_push($iduser, $token_gcm)

        {

            $sql = "SELECT * 

                    FROM push 

                    WHERE userid = $iduser

                    AND token = '$token_gcm'";            

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                $this->db->where('userid', $iduser);

                $this->db->where('token', $token_gcm);

                return $this->db->delete("push"); 

            }

            else{

                return true;

            }           

        }

        

        //------------------------------------------------------------/

        /********* Obtner username del usuario por  id_usser ********/

        /********* return nombre apellido   ************************/

        //---------------------------------------------------------/

        public function get_nombre_usuario($id_usser) //algunos usuarios del colegio
        {
            $sql = "SELECT first_name, 
                           last_name,
                           foto
                    FROM  users
                    WHERE users.id = $id_usser
                    "; //AND users.active=1
            //$query = $this->db->query($sql);

            //$sql = "CALL get_nombre_usuario(".$id_usser.")";
            
            $query = $this->db->query($sql);
            $resultado = $query;
            //mysqli_next_result( $this->db->conn_id );
            

            if($resultado->num_rows()>0){
                return $resultado->result();
            }
            else{
                return false;
            }
        }



        public function get_users_rol($id_rol)
        {

            $sql = "SELECT users.*, users_groups.estado as estado_users_group, users_groups.colegio_id, users_groups.group_id, users_groups.id as id_users_groups

                    FROM users

                    JOIN users_groups ON users_groups.user_id = users.id

                    WHERE users_groups.group_id = $id_rol

                    AND users_groups.estado=1

                    "; 

                    $query = $this->db->query($sql);

            

                if($query->num_rows()>0){

                    return $query->result();

                }

                else{

                    return false;

                }

        }



        public function get_users_todos()

        {

            $sql = "SELECT * FROM  users"; 

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                return $query->result();

            }

            else{

                return false;

            }

        }





        public function verifica_dni($dni, $userId)

        {

            $sql = "SELECT id FROM  users WHERE documento = '$dni'"; 

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                if($query->num_rows()==1){

                    if( $userId != $query->row()->id )

                        return true;

                    else return false;

                }

            }

            else{

                return false;

            }

        }





        public function verifica_email($email, $userId)

        {

            $sql = "SELECT id FROM users WHERE email = '$email'"; 

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                if($query->num_rows()==1){

                    if( $userId != $query->row()->id )

                        return true;

                    else return false;

                }

            }

            else{

                return false;

            }

        }





        public function verifica_username($name, $userId)

        {

            $sql = "SELECT id FROM users WHERE username = '$name'"; 

            $query = $this->db->query($sql);

            

            if($query->num_rows()>0){

                if($query->num_rows()==1){

                    if( $userId != $query->row()->id )

                        return true;

                    else return false;

                }

            }

            else{

                return false;

            }

        }

        public function get_user_x_id_user_group($id_user_group)
        {
            $sql = "SELECT users.* FROM users JOIN users_groups on users_groups.user_id = users.id where users_groups.id = $id_user_group";
            $query = $this->db->query($sql);
            return $query->row();
        }
        public function get_users_id_x_documento($dni)//esta funcion devuelve la query, para validar mas adelante si vienen filas
        {
            $sql = "SELECT * FROM users where documento = '$dni'";
                $query = $this->db->query($sql);
            return $query;

        }

        /*---------------------------------------------------------------------------
        Se realiza 1 de 2 posibles "updates" y retorna la cantidad de registros afectados
        
        1)  Cuando el alumno NO tiene: otros roles, hermanos y sus tutores no tienen otros roles
        update de los siguientes campos para el Alumno (rol "Alumno") y sus padres (rol "tutores"):
            -users.groups.estado = 0 
            -inscripciones.activo = 0 
            -users.active = 0
            
        2) Si el alumno tiene hermanos que dependen de los mismos tutores, o 
        tutores con otros roles, solo actualiza los campos anteriores
        para el alumno y escuela solicitados para el rol "Alumno" y division solicitada
        -----------------------------------------------------------------------------*/
        public function desactivar_alumno_tutores($data = null)
        {
            if($data == null)
            {
                return false;
            }
            else
            {
                $id_usuario  = $data['idAlumno'];
                $id_division = $data['idDivision'];
                $id_colegio  = $data['idColegio'];
                
                $sql_spHnos = "CALL bajaAlumnoTutores($id_colegio,$id_division,$id_usuario)";
                $query = $this->db->query($sql_spHnos);
                $rowsUpdated = $this->db->affected_rows();
                if(count($rowsUpdated)>0) 
                    return $rowsUpdated;
                else
                    return $rowsUpdated;
            }
        }//Fin funcion "desactivar_alumno_tutores"
         public function obtener_datos_user($dni)
        {
            $sql = "SELECT users.*, users_groups.id as id_depo FROM `users` JOIN users_groups ON users_groups.user_id=users.id WHERE users.documento=$dni";
            $query= $this->db->query($sql);
            if ($query->num_rows()!= 0) {
                return $query->result();
            }
            else return false;
        }//Fin funcion "desactivar_alumno_tutores"
        
        

            

    } // fin Model_usuario