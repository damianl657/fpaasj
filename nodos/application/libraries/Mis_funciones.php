<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mis_funciones
{

    /***************************************************************************************************
    /*************************** FUNCIONES DE USO REITERADO ********************************************
    /**************************************************************************************************/
    public function get_cicloa()
    {
        $CI     = &get_instance();
        $sql    = "SELECT * FROM cicloa";
        $cicloa = $CI->db->query($sql);
        if ($cicloa->num_rows() > 0) {
            foreach ($cicloa->result() as $row) {
                return $row->cicloa;
            }
        }
    }

    public function get_ciclo_inscripcion()
    {
        $CI     = &get_instance();
        $sql    = "SELECT * FROM cicloa";
        $cicloa = $CI->db->query($sql);
        if ($cicloa->num_rows() > 0) {
            foreach ($cicloa->result() as $row) {
                return $row->ciclo_inscripcion;
            }
        }
    }  

    function get_nombre_colegio(){
        $CI    = &get_instance();
        $sql   = "SELECT nombre FROM colegio WHERE id = 1";
        $meses = $CI->db->query($sql);
        if ($meses->num_rows() > 0) {
            foreach ($meses->result() as $row) {
                return $row->nombre;
            }
        }
        else{
            return "Colegio";
        }        
    }    

    /*************************************************************************************************
     ********************************** Funciones Varias **********************************************
     *************************************************************************************************/
    
    public function get_fecha_arg($fecha)
    {
        $res = substr($fecha, 8, 2) . "-" . substr($fecha, 5, 2) . "-" . substr($fecha, 0, 4);
        return $res;
    }

    public function get_fecha_arg_barras($fecha)
    {
        if($fecha!='')
            $res = substr($fecha, 8, 2) . "/" . substr($fecha, 5, 2) . "/" . substr($fecha, 0, 4);
        else $res='';
        return $res;
    }

    public function get_fecha_hora_arg($fechahora)
    {
        $res = substr($fechahora, 8, 2) . "-" . substr($fechahora, 5, 2) . "-" . substr($fechahora, 0, 4) . ' ' . substr($fechahora, 11, 8);
        return $res;
    }

    public function get_fecha_yanki($fecha)
    {
        $res = substr($fecha, 6, 4) . "-" . substr($fecha, 3, 2) . "-" . substr($fecha, 0, 2);
        return $res;
    }

    public function get_fecha_mysql($fecha)
    {
        $res = substr($fecha, 6, 4) . "-" . substr($fecha, 0, 2) . "-" . substr($fecha, 3, 2);
        return $res;
    }

    public function get_fecha_actual()
    {
        $CI  = &get_instance();
        $fec = getdate();
        $res = $fec['year'] . "-" . $CI->mis_funciones->llena($fec['mon'] - 1, 2) . "-" . $CI->mis_funciones->llena($fec['mday'] - 1, 2);
        return $res;
    }

    public function get_fecha_my_to_arg($mysql_fecha)
    {
        $CI = &get_instance();
        return substr($mysql_fecha, 8, 2) . "-" . substr($mysql_fecha, 5, 2) . "-" . substr($mysql_fecha, 0, 4);
    }

    public function get_fecha_mmddaaaa_to_my($mmddaaaa)
    {
        $CI  = &get_instance();
        $fec = substr($mmddaaaa, 6, 4) . "-" . substr($mmddaaaa, 0, 2) . "-" . substr($mmddaaaa, 3, 2);
        return $fec;
    }

    public function mostrarFecha()
    {
        // Obtenemos y traducimos el nombre del da
        $dia = date("l");
        if ($dia == "Monday") {
            $dia = "lunes";
        }

        if ($dia == "Tuesday") {
            $dia = "martes";
        }

        if ($dia == "Wednesday") {
            $dia = "mi&eacute;rcoles";
        }

        if ($dia == "Thursday") {
            $dia = "jueves";
        }

        if ($dia == "Friday") {
            $dia = "viernes";
        }

        if ($dia == "Saturday") {
            $dia = "sabado";
        }

        if ($dia == "Sunday") {
            $dia = "domingo";
        }

        // Obtenemos el numero del dia
        $dia2 = date("d");

        // Obtenemos y traducimos el nombre del mes
        $mes = date("F");
        if ($mes == "January") {
            $mes = "enero";
        }

        if ($mes == "February") {
            $mes = "febrero";
        }

        if ($mes == "March") {
            $mes = "marzo";
        }

        if ($mes == "April") {
            $mes = "abril";
        }

        if ($mes == "May") {
            $mes = "mayo";
        }

        if ($mes == "June") {
            $mes = "junio";
        }

        if ($mes == "July") {
            $mes = "julio";
        }

        if ($mes == "August") {
            $mes = "agosto";
        }

        if ($mes == "September") {
            $mes = "setiembre";
        }

        if ($mes == "October") {
            $mes = "octubre";
        }

        if ($mes == "November") {
            $mes = "noviembre";
        }

        if ($mes == "December") {
            $mes = "diciembre";
        }

        // Obtenemos el ao
        $ano = date("Y");

        // Imprimimos la fecha completa
        return "$dia $dia2 de $mes de $ano";
    }

    public function getFechaHoraActualMysql()
    {
        $CI         = &get_instance();
        $tiempo     = getdate();
        $t_dia      = $CI->mis_funciones->llena($tiempo['mday'] - 1, 2);
        $t_mes      = $CI->mis_funciones->llena($tiempo['mon'] - 1, 2);
        $t_anio     = $CI->mis_funciones->llena($tiempo['year'] - 1, 2);
        $t_hora     = $CI->mis_funciones->llena($tiempo['hours'] - 1, 2);
        $t_min      = $CI->mis_funciones->llena($tiempo['minutes'] - 1, 2);
        $t_seg      = $CI->mis_funciones->llena($tiempo['seconds'] - 1, 2);
        $fecha_hora = $t_anio . "-" . $t_mes . "-" . $t_dia . " " . $t_hora . ":" . $t_min . ":" . $t_seg;
        return $fecha_hora;
    }

    public function get_numero_mes($fecha)
    {
        $CI    = &get_instance();
        $sql   = "SELECT MONTH('$fecha') AS mes";
        $meses = $CI->db->query($sql);
        if ($meses->num_rows() > 0) {
            foreach ($meses->result() as $row) {
                return $row->mes;
            }
        }
    }

    public function llena($codigo, $longitud)
    {
        $num = $codigo + 1;
        $res = strval($num);
        while (strlen($res) < $longitud) {
            $res = "0" . $res;
        }
        return $res;
    }

    /*RECIBE UN ARREGLO DE NOTAS Y DEVUELVE LA MEDIANA*/
    public function mediana($array)
    {
        $sizeof = sizeof($array);
        sort($array);
        $resto   = $sizeof % 2;
        $mediana = 0;
        //si la cantidad de elementos es impar
        if ($resto != 0) {
            $mediana = $array[($sizeof / 2)];
        }
        //si la cantidad de elementos es par
        else {
            $mediana = ($array[($sizeof / 2)] + $array[($sizeof / 2) - 1]) / 2;
        }
        return $mediana;
    }

    /*public function encriptar($cadena)
    {
        //print_r(openssl_get_cipher_methods());
        //exit();
        //$cipher = MCRYPT_RIJNDAEL_256;// el 256 se refiere al tamaño del bloque
        $key       = md5('7c4a8d09ca3762af61e59520943dc26494f8941b'); // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
        //$cadena = $cadena;
        //$mode = MCRYPT_MODE_CBC;
        $iv = md5(md5($key));//por ahora no se usa

        //$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $cadena, MCRYPT_MODE_CBC, md5(md5($key))));
        /*$encrypted = base64_encode(mcrypt_encrypt(
            $cipher, 
            $key, 
            $cadena, 
            $mode, 
            $iv));*/

       /* $encrypted = base64_encode(openssl_encrypt($cadena, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv));

        return $encrypted; //Devuelve el string encriptado
    }*/
     public function encriptar($cadena)
    {
        $key       = '7c4a8d09ca3762af61e59520943dc26494f8941b'; // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
        $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $cadena, MCRYPT_MODE_CBC, md5(md5($key))));
        return $encrypted; //Devuelve el string encriptado

    }

    public function desencriptar($cadena)
    {
        $key       = '7c4a8d09ca3762af61e59520943dc26494f8941b'; // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
        $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($cadena), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
        return $decrypted; //Devuelve el string desencriptado
    }


    //QUITAR ACENTOS
    function sanear_string($string)
    {
     
        $string = trim($string);
     
        $string = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $string
        );
     
        $string = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $string
        );
     
        $string = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $string
        );
     
        $string = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $string
        );
     
        $string = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $string
        );
     
        $string = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C',),
            $string
        );
     
        //Esta parte se encarga de eliminar cualquier caracter extraño
        $string = str_replace(
            array("\\", "¨", "º", "-", "~",
                 "#", "@", "|", "!", '"',
                 "·", "$", "%", "&", "/",
                 "(", ")", "?", "'", "¡",
                 "¿", "[", "^", "<code>", "]",
                 "+", "}", "{", "¨", "´",
                 ">", "< ", ";", ",", ":",
                 ".", " "),
            '',
            $string
        );
     
     
        return $string;
    }

    function sumarDiasFecha($fecha,$dia){
        list($day,$mon,$year) = explode('-',$fecha);
        return date('d-m-Y',mktime(0,0,0,$mon,$day+$dia,$year));
    }
     function anio_a_letras($grado){
        $valor = '';
       switch ($grado) {
           case 1:
              $valor = 'Primero';
               break;
            case 2:
              $valor = 'Segundo';
               break;
            case 3:
              $valor = 'Tercero';
               break;
            case 4:
              $valor = 'Cuarto';
               break;
            case 5:
              $valor = 'Quinto';
               break;
            case 6:
              $valor = 'Sexto';
               break;
           
           default:
                $valor = '';
               break;
        }
        
        return $valor;
    }
    public function get_club(){
        $CI= &get_instance();
        $sql="SELECT * FROM `nodocolegio`";
        $club=$CI->db->query($sql);
       
       if ($club->num_rows()> 0) {
            
                return $club->result();
            
        }
        
    }
    public function get_localidad(){
         $CI= &get_instance();
        $sql="SELECT * FROM `localidad`";
        $localidad=$CI->db->query($sql);
       
       if ($localidad->num_rows()> 0) {
            
                return $localidad->result();
            
        }

    }
     public function get_rama(){
        $CI= &get_instance();
        $sql="SELECT * FROM `rama`";
        $rama=$CI->db->query($sql);
       
       if ($rama->num_rows()> 0) {
            
                return $rama->result();
            
        }
            
        }
        public function get_licencia(){
        $CI= &get_instance();
        $sql="SELECT * FROM `licencia`";
        $lic=$CI->db->query($sql);
       
       if ($lic->num_rows()> 0) {
            
                return $lic->result();
            
        }
        }
         public function get_funcion($id){
        

        $CI= &get_instance();
        $sql="SELECT * FROM `groups` WHERE id_colegio =".$id;
        $aux=$CI->db->query($sql);
       
       if ($aux->num_rows()> 0) {
            
                return $aux->result();
            
        }
        }
        function obtener_categorias(){
             $CI= &get_instance();
            $sql = "SELECT * FROM `categorias`";
            $sql = $CI->db->query($sql);
            if ($sql->num_rows()> 0) {
                
                return $sql->result();

            }
        }

}
