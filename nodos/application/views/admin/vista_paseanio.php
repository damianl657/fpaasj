<? //include("application/views/include/template_colegio.php"); ?>
<script type="text/javascript">
  var opcion_menu ='<?php echo $opcion_menu;?>';
</script>

<script src="<?=base_url('tpl/js/bootstrap.min.js');?>"></script> <!-- con esto hago que los tooltip se vean rojos-->
<script src="<?php echo base_url('tpl/js/general.js');?>"></script>


<script type="text/javascript">

var passApiKey = '<?php echo $passApiKey; ?>';
var urlApi = '<?php echo $urlApi; ?>';
var idUser = '<?php echo $idusuario; ?>';

/*
function pase1()
{

      //console.log('funcion traer nivles: '+idcolegio+' grupo: '+nombregrupo);
      var url = urlApi+"/paseanio/pase_niveles";

      $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'APIKEY' : passApiKey,
                'userid' : idUser,
                'Access-Control-Allow-Origin': '*'
            },
            data:{},
            beforeSend: function() {

            },
            success: function(data){
              console.log("PASE1");
              console.log(data);

              $('#btn_pase2').attr('disabled', false);

            },
            error: function(response){
              console.log("PASE1");
              console.log(response);
              $('#btn_pase2').attr('disabled', false);

            }
      });
}

function pase2()
{

      //console.log('funcion traer nivles: '+idcolegio+' grupo: '+nombregrupo);
      var url = urlApi+"/paseanio/pase_materias";

      $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'APIKEY' : passApiKey,
                'userid' : idUser,
                'Access-Control-Allow-Origin': '*'
            },
            data:{},
            beforeSend: function() {

            },
            success: function(data){
              console.log("PASE2");
              console.log(data);
              $('#btn_pase3').attr('disabled', false);
            },
            error: function(response){
              console.log("PASE2");
              console.log(response);
              $('#btn_pase3').attr('disabled', false);
            }
      });
}


function pase3()
{

      //console.log('funcion traer nivles: '+idcolegio+' grupo: '+nombregrupo);
      var url = urlApi+"/paseanio/pase_horarios";

      $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'APIKEY' : passApiKey,
                'userid' : idUser,
                'Access-Control-Allow-Origin': '*'
            },
            data:{},
            beforeSend: function() {

            },
            success: function(data){
              console.log("PASE3");
              console.log(data);
              $('#btn_pase4').attr('disabled', false);
            },
            error: function(response){
              console.log("PASE3");
              $('#btn_pase4').attr('disabled', false);
              console.log(response);
            }
      });
}

function pase5()
{
      //console.log('funcion traer nivles: '+idcolegio+' grupo: '+nombregrupo);
      var url = urlApi+"/paseanio/pase_preceptores";

      $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'APIKEY' : passApiKey,
                'userid' : idUser,
                'Access-Control-Allow-Origin': '*'
            },
            data:{},
            beforeSend: function() {

            },
            success: function(data){
              console.log("PASE5");
              console.log(data);


            },
            error: function(response){
              console.log("PASE5");
                console.log(response);
            }
      });
}*/

function pase4()
{

      //console.log('funcion traer nivles: '+idcolegio+' grupo: '+nombregrupo);
      var url = urlApi+"/paseanio/pase_inscripciones";

      $.ajax({
            url: url,
            type: 'POST',
            headers: {
                'APIKEY' : passApiKey,
                'userid' : idUser,
                'Access-Control-Allow-Origin': '*'
            },
            data:{},
            beforeSend: function() {

            },
            success: function(data){
              console.log("PASE4");
              //$('#btn_pase5').attr('disabled', false);
              console.log(data);

            },
            error: function(response){
              console.log("PASE4");
                console.log(response);
                $('#btn_pase5').attr('disabled', false);
            }
      });
}


/*function reenviar()
{
    var url = urlApi+"/evento/reenviar_evento2";

    console.log("PROCESANDO...");
    $.ajax({
          url: url,
          type: 'POST',
          headers: {
              'APIKEY' : passApiKey,
              'userid' : idUser,
              'Access-Control-Allow-Origin': '*'
          },
          data:{idevento:3804, pin:105},
          success: function(data){

            console.log(data);

          },
          error: function(response){
              console.log(response);
          }
    });
}*/



$(document).ready(function(){
   $.noConflict();

   //reenviar();

   $('#btn_pase4').click(function(){
      pase4();
   });

});

</script>



    <!-- Comienzo HTML -->
     <div class="wrapper wrapper-content animated fadeInRight">

    <!-- Funcionalidad -->
    <div class="row">
        <div class="col-lg-12">

          <div class="row">
            <div class="col-lg-10 col-md-8 col-sm-10 col-xs-12 col-centered">
               <div class="ibox-content">


                      <div class="row" id="">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center vspace4">
                          <h1>Pase de Anio en 1 paso</h1>
                          <h6>solo usuarios NODOS</h6>
                        </div>
                        <br>

                        <div class="col-lg-offset-3 col-lg-6 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-12 center vspace3">
                          <!--<p>Ir haciendo click de a uno por uno. Esto hace tareas que requieren que se cumpla el orden. Esto es para todos los colegios activos.</p>-->
                        </div>
                        <div class="col-lg-offset-3 col-lg-6 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-12 center vspace3">
                        <br>
                        <br>
                          <table>

                            <!--<tr>
                              <td><button class="btn  btn-primary" id="btn_pase1" align="left" ><strong>Pase 1</strong></button></td>
                              <td> duplica todos los niveles anios y divisiones </td>
                            </tr>

                            <tr>
                              <td><button class="btn  btn-primary" id="btn_pase2" align="left" disabled><strong>Pase 2</strong></button></td>
                              <td> duplica todas las materias por anio </td>
                            </tr>

                            <tr>
                              <td><button class="btn  btn-primary" id="btn_pase3" align="left" disabled><strong>Pase 3</strong></button></td>
                              <td> duplica todos los horarios y horariosmaterias </td>
                            </tr>

                            <tr>
                              <td><button class="btn  btn-primary" id="btn_pase5" align="left" disabled><strong>Pase 5</strong></button></td>
                              <td> hace pase de anio de preceptores</td>
                            </tr>-->

                            <tr>
                              <td><button class="btn  btn-primary" id="btn_pase4" align="left" ><strong>Pase Anio</strong></button></td>
                              <td> hace pase de anio de todos los inscriptos del ciclo anterior</td>
                            </tr>

                          </table>

                        </div>

                      </div>


              </div>

            </div>
        </div>
      </div>
    </div>
    <br>

    </div>




<style type="text/css">



  .row2{
    min-height: 411px;
  }

  select
  {
    min-width: 100px;
  }



</style>


