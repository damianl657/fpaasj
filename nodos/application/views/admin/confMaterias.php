﻿
<script src="<?=base_url('assets/js/bootstrap.min.js');?>"></script>

<script type="text/javascript">

var idUser =  '<?php echo $idusuario; ?>';

$(document).ready(function(){ 

      $('.select2').select2({
           theme: "bootstrap",
           placeholder: "Seleccione un A&ntilde;0",
          allowClear: true
      }); 

      generar_datatable_mat();

  });
  

 
  function get_materias_x_anio(id_colegio){

        var aux = $('#dir_prec_'+id_colegio).val();
        var arre = Array();
        arre=aux.split('_');
        var idanio = arre[0]; 
        //var idanio = arre[1]; 

           
        $("#tabladatos_"+arre[1]).dataTable().fnDestroy();
        $("#tabladatos_body_"+arre[1]).empty();
        
        var url = '<?=base_url("/index.php/materias/get_materias_x_anio_ajax")?>';   
        
        //console.log(url);
        $.ajax({
          url: url,
          type: 'GET',
          
          data:{idcolegio: arre[1], nodoanioid: arre[0] },
          success: function(data)
          { 
            var data = jQuery.parseJSON( data );
            if( data['status']===0){
              var string = "<p class='text-danger'>No hay materias asignadas</p>";
              //console.log(string);
              $("#tabladatos_body_"+arre[1]).html(string);

              tableMatAsig=null;
            }
            else
            {
              //var data = jQuery.parseJSON( data );

              var cont = 1 ;
    
              data.forEach(function(currentValue,index){
                //console.log(currentValue);
                var id = currentValue.id;
                var nombre = currentValue.nombre;
                var idmat = currentValue.idmateria;

            
        

                var fila = '<tr id="materia_'+id+'_'+idmat+'">'+
                              '<td>'+cont+'</td>'+'<td>'+nombre+'</td>'+

                              '<td><div align="center"><button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="" title="borrar materia" onclick="delete_aniomateria(this,'+id+','+id_colegio+')" type="button"><i class="glyphicon glyphicon-trash"></i></button></div></td>'+

                          '</tr>';

                cont++;

                $("#tabladatos_body_"+arre[1]).append(fila);

              });
              generar_datatable2(arre[1]);
            }
          },
          error: function(response){
            console.log(response);
              var string = "No hay alumnos asignados";
              $(".alumnos_tutores").html(string);         
          }
        });
  } 
</script>


<script >
var tableMat=null;
var tableMatAsig = null;
function generar_datatable2(id_colegio)
{
            var handleDataTableButtons2 = function() {
              if ($("#tabladatos_"+id_colegio).length) {
                tableMatAsig=$("#tabladatos_"+id_colegio).DataTable({
                  
            //"scrollX": true,
            //"scrollY": true,

            "scroller": {
            "displayBuffer": 2
            },
              

                  responsive: true,
                  keys: true,
                  deferRender: true,
                  scrollCollapse: true,
                 
                  "language": {
                                "sProcessing":     "Procesando...",
                                "sLengthMenu":     "Mostrar _MENU_ registros",
                                "sZeroRecords":    "No se encontraron resultados",
                                "sEmptyTable":     "NingÃºn dato disponible en esta tabla",
                                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Buscar:",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":     "Ãšltimo",
                                    "sNext":     "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                } 
                              }
                });
              }
            };
            TableManageButtons2 = function() {
              "use strict";
              return {
                init: function() {

                  handleDataTableButtons2();
                }
              };
            }();
            TableManageButtons2.init();
            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true,

            });
                
                
    }

  function generar_datatable_mat()
  {
        var handleDataTableButtons2 = function() {
        if ($("#materias_tabladatos").length) {
            tableMat = $("#materias_tabladatos").DataTable({
              
                //"scrollX": true,
                //"scrollY": true,

                "scroller": {
                "displayBuffer": 2
                },
              

                  responsive: true,
                  keys: true,
                  deferRender: true,
                  scrollCollapse: true,

                  "ordering": true,
                 
                  "language": {
                                "sProcessing":     "Procesando...",
                                "sLengthMenu":     "Mostrar _MENU_ registros",
                                "sZeroRecords":    "No se encontraron resultados",
                                "sEmptyTable":     "NingÃºn dato disponible en esta tabla",
                                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Buscar:",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":     "Ãšltimo",
                                    "sNext":     "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                } 
                              }
                });
              }
            };

        TableManageButtons2 = function() {
          "use strict";
          return {
            init: function() {

              handleDataTableButtons2();
            }
          };
        }();

        TableManageButtons2.init();
        /*var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true,
        });     */         
  }

</script>


<script>

  function delete_aniomateria(este, idaniomat, colegioId)
  {
    bootbox.confirm("Eliminar Materia?", function(result){ 
      if(result){

        var urlRequest = '<?=base_url("/index.php/materias/delete_aniomat_ajax")?>';

        $.ajax({
              url: urlRequest,
              type: 'POST',
              headers: {    
                  
              },
              data: {idaniomat: idaniomat},
              beforeSend: function() {
                  $(este).after("<img id='loadingdelete' src='<?=base_url('assets/images/loading.gif');?>' height='15' width='15' />");
              },
              success: function(data){
                    var data = jQuery.parseJSON( data );
                    //console.log(data);
                    if(data['status']==false)
                    {  
                        if(data['message'] == 'tiene_horarios')                                               
                        {
                           $.notify('error: elimine todos los horarios asignados', "warning");
                        }
                        else 
                          if(data['message'] == 'tiene_areas')                                               
                          {
                             $.notify('error: elimine todas las areas asignadas', "warning");
                          }
                     }
                    else{   //alert('OK');
                          $.notify('Materia eliminada', "success");
                          
                          get_materias_x_anio(colegioId);
                    }
                    $('#loadingdelete').remove();
                    
              },
              error: function(response){
                  //console.log(response);               
              }
          });
        
      }

    });

  }

  function add_materias()
  {
      var colegioId = $('#modal_materias_idcolegio').val();


      var aux = $('#dir_prec_'+colegioId).val();
      var arre = Array();
      arre=aux.split('_');
      var anioId = arre[0]; 


      var selected = [];
     
      if(tableMat!=null)
        tableMat.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            //var data = this.data();
            var node = this.node();
            var check = $(node).find('.checkmat');
            if($(check).is(':checked'))
              selected.push($(check).val());
        } );

      var materias = JSON.stringify(selected);  

      //console.log(materias);
      var urlRequest = '<?=base_url("/index.php/materias/add_materias_x_anio_ajax")?>';

      $.ajax({
            url: urlRequest,
            type: 'POST',
            headers: {    
                
            },
            data: {materias: materias, colegio: colegioId, anio:anioId},
            beforeSend: function() {
                $('#guardar_mat').after("<img id='loadingNiv' src='<?=base_url('assets/images/loading.gif');?>' height='15' width='15' />");
            },
            success: function(data){
                  var data = jQuery.parseJSON( data );
                  console.log(data);
                  if(data['status']==false)
                  {  
                      if(data['message'] == 'no_materias')                                               
                      {
                        $.notify('No se seleccionaron materias', "warning"); 
                      }
                   }
                  else{ 

                        $.notify('Materias Asignadas', "success"); 

                        get_materias_x_anio(colegioId);

                  }
                  $('#loadingNiv').remove();
                  $('#modal_modal_materias').modal('hide');
            },
            error: function(response){
                //console.log(response);               
            }
        });

  }

  function abrir_modal_materias(id_colegio)
  {
    //alert('hola');
    //$('#modal_modal_materias').empty();
    
    $("#modal_materias_idcolegio").val(id_colegio);


    if(tableMat!=null)
      tableMat.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
          //var data = this.data();
          var node = this.node();
          $(node).find('.checkmat').prop('checked',false);
         
      } );


    if(tableMatAsig!=null)
      tableMatAsig.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
          //var data = this.data();
          var node = this.node();
          var idtr = $(node).attr('id');
          //console.log(idtr);

          var arre = Array();
          arre=idtr.split('_');
          var idaniomat = arre[1]; 
          var idmat = arre[2]; 

          var row = tableMat.row('#rowmat_'+idmat).node();
          
          $(row).find('.checkmat').prop('checked',true);
         
      } );

    
    $('#modal_modal_materias').modal('show');
  } 


  function add_materia()
  {
      var nombremat = $('#nombremat').val();

      if(nombremat!='') {

        var urlRequest = '<?=base_url("/index.php/materias/alta_materia_ajax")?>';

        $.ajax({
              url: urlRequest,
              type: 'POST',
              headers: {    
                  
              },
              data: {nombremat: nombremat},
              beforeSend: function() {
                  $('#add_mat').after("<img id='loadingAddMat' src='<?=base_url('assets/images/loading.gif');?>' height='15' width='15' />");
              },
              success: function(data){
                    var data = jQuery.parseJSON( data );
                    console.log(data);
                    if(!data['status'])
                    {   
                      //console.log(data['message']);
                      $.notify(data['message'], "warning");
                    }
                    else {
                        $.notify(data['message'], "success");

                        var id = data['id'];
                        var trid ="rowmat_"+id;

                        var tdcheck = '<td>'+
                                '<div align="center" class="custom-control custom-checkbox">'+
                                    '<input type="checkbox" class="custom-control-input checkmat" name="checkmat" value="'+id+'" id="idmat_'+id+'" checked>'+
                                '</div>'+
                              '</td>';

                        var tdname = '<td name="mat_'+id+' id="mat_'+id+' ">'+nombremat+'</td>';

                        var tr = '<tr id="rowmat_"'+id+'>'+
                                tdcheck+
                              tdname+
                          '</tr>';
                        

                        $('#materias_tabladatos_body').append(tr);
                        tableMat.row.add( [
                            tdcheck,
                            tdname,
                        ] ).id = trid;

                        tableMat.draw( false );

                        $('#mat_'+id).parent('tr').attr('id',trid);
                    }

                    $('#loadingAddMat').remove();
                    $('#modal_modal_addmateria').modal('hide');
              },
              error: function(response){
                  console.log(response);               
              }
        });

      }
      else 
        $('#nombremat').notify("Ingrese un nombre de materia", {className:"error", autoHide: true});
  }


  function abrir_modal_addmateria(id_colegio)
  {
   
   $('#nombremat').val('');

   $('#nombremat').focus();

    $('#modal_modal_addmateria').modal('show');
  } 
</script>



<!-- modal agregar materias-->
<div class="modal fade" id="modal_modal_materias" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Materias</h4>
      </div>
      
      <div class="modal-body">
        <form id="asignar_materias" autocomplete="on" method="post" class="form-horizontal form-label-left">
      
            <input type="hidden" id="modal_materias_idcolegio" name="modal_materias_idcolegio" value="" />
            
            <div class="panel-body table-responsive">
                <table class="table table-striped table-bordered dt-responsive nowrap" id="materias_tabladatos">
                    <thead>
                      <th></th>
                      <th>Nombre Materia</th>
                    </thead>
                    <tbody id="materias_tabladatos_body">
                      <?
                      $cont=0; 
                      if($nombresmaterias)
                            foreach ($nombresmaterias as $mat)
                            {
                              $nombremat = $mat->nombre;
                              $idmat = $mat->id;

                              ?>

                              <tr id="rowmat_<?=$idmat?>">
                                  <td>
                                    <div align="center" class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input checkmat" name="checkmat" value="<?=$idmat?>" id="idmat_<?=$idmat?>">
                                    </div>
                                  </td>
                                  <td name="mat_<?=$idmat?>"><?=$nombremat?></td>
                              </tr>
                              

                              <?
                              $cont++;
                            }
                      ?>
                    </tbody>
                </table>

                <?
                  $clave = in_array('add_materia', $acciones);
                  if($clave)  { ?>
                      <button class="btn btn-sm btn-info pull-left m-t-n-xs" id="btn_alumnos" onclick="abrir_modal_addmateria()" type="button"><i class="glyphicon glyphicon-plus"></i> Agregar Materia<strong></strong></button>
                <?}?>


            </div>

        </form>

      </div>

      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button class="btn btn-primary" id ="guardar_mat" onclick="add_materias()">Agregar</button>
      </div>
      
    </div>
  </div>
</div>



<!-- modal alta materia-->
<div class="modal fade" id="modal_modal_addmateria" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Agregar Nueva Materia</h4>
      </div>
      
      <div class="modal-body">
        <form id="add_materia" autocomplete="on" method="post" class="form-horizontal form-label-left">
            
            <div class="panel-body">
                
                <div class="form-group">
                  <label>Materia</label> 
                  <input type="text" placeholder="Ingrese nombre de materia" class="form-control" id="nombremat" name="nombremat" required value=""/>
                </div>


            </div>

        </form>


      </div>

      <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button class="btn btn-primary" id ="add_mat" onclick="add_materia()">Guardar</button>
      </div>
      
    </div>
  </div>
</div>


<div class="wrapper wrapper-content animated fadeInRight" id="">
    <div class="row">
          <div class="col-lg-12 " id="usuarios_borrar">
              <div class="ibox collapsed2 float-e-margins">
                  <div class="ibox-title" id="tituloaltaevento">
                      <h5>Materias por A&ntilde;o </h5>
                      <div class="ibox-tools">
                          <a class="">
                              <!--<i class="fa fa-chevron-up"></i>-->
                          </a>
                      </div>
                  </div>
                  <div class="ibox-content">
                      <div class="row row2">
                          <div class="col-sm-12 b-r" id="">
                              <div class="tabs-container">
                                <ul class="nav nav-tabs" id="colegios_dir_prec">

                                  <?
                                  $cont=0;
                                  if($colegios)
                                    foreach ($colegios as $cole)
                                    {
                                      $idcole = $cole['idcole'];
                                      $url = base_url()."Horarios/index#tab_dir-".$idcole;
                                      $name = $cole['namecole'];
                                      if($cont==0)
                                        echo "<li class='active'><a data-toggle='tab' href='$url'>$name</a></li>";
                                      else 
                                        echo "<li class=''><a data-toggle='tab' href='$url'>$name</a></li>";

                                      $cont++;
                                    }

                                
                                  ?>
                                </ul>

                                <div class="tab-content " id="contenido_tab_dir_prec">

                                  <?
                                  $cont=0;
                                  if($colegios)
                                    foreach ($colegios as $cole)
                                    {
                                      $idcole = $cole['idcole'];
                                      //print_r($anios[$idcole]); die();
                                      if($cont==0)
                                        $clase = 'active';
                                      else
                                        $clase = ''
                                      ?>

                                        <div id="tab_dir-<?=$idcole?>" class="tab-pane <?=$clase?>">
                                          <div class="panel-body table-responsive">
                                              <div class="row">
                                                  <div class="col-sm-6">
                                                    <form id="form_dir<?=$idcole?>" autocomplete="on" method="post" class="form-horizontal form-label-left">
                                                        <div class="form-group ">
                                                              <label>Seleccione una A&ntilde;o: </label>
                                                        </div>
                                                        <div class="form-group ">
                                                          <select name="divisiones" id="dir_prec_<?=$idcole?>" class="select2 form-control">
                                                              <?

                                                              if($anios[$idcole]) 
                                                                foreach ($anios[$idcole] as $anio)
                                                                {
                                                                  $anioId = $anio->nodoanios_id;
                                                                  if(isset($anio->nombre_especialidad))
                                                                    $nombre_nivel = $anio->nombre_nivel." ".$anio->nombre_especialidad." ".$anio->nombre_anio; 
                                                                  else 
                                                                    $nombre_nivel = $anio->nombre_nivel." ".$anio->nombre_anio; 

                                                                  $value = $anioId."_".$idcole;
                                                                  echo "<option value='$value'>$nombre_nivel</option>" ;
                                                                }
                                                               

                                                              ?>  

                                                          </select>
                                                        </div> 
                                                    </form>
                                                  </div>
                                                  <div class="col-sm-6" style="margin-top:35px">
                                                      <button class="btn btn-sm btn-primary pull-left m-t-n-xs" id="btn_alumnos" onclick="get_materias_x_anio(<?=$idcole?>)" type="button"><strong>Listar</strong></button>

                                                      <button class="btn btn-sm btn-info pull-right m-t-n-xs" id="btn_alumnos" onclick="abrir_modal_materias(<?=$idcole?> )" type="button"><i class="glyphicon glyphicon-plus"></i> Asignar Materias<strong></strong></button>

                                                      <div type="hidden" id="loading_horarios_dir_esc_<?=$idcole?>"></div>
                                                  </div>
                                              </div>
                                              <table class="table table-striped table-bordered dt-responsive nowrap" id="tabladatos_<?=$idcole?>">
                                                  <thead>
                                                    <th>#</th>
                                                    <th>Nombre Materia</th>
                                                    <th></th>
                                                  </thead>
                                                  <tbody id="tabladatos_body_<?=$idcole?>"></tbody>
                                              </table>
                                          </div>
                                        </div>

                                    <?$cont++;
                                    }?>

                                </div>
                             </div>
                          </div>
                      </div>

                  </div>
              </div>

          </div>
           
      </div>
</div>

