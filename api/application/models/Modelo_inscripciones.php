<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Modelo_inscripciones extends CI_Model {

        public function __construct()
        {
            // Call the CI_Model constructor
            parent::__construct();
            $this->load->database();
        }
        public function guardar_inscripcion($arre){

            $this->db->insert('inscripcion_div', $arre);
            $error = $this->db->error();
            if($error['code']=="0")
            {
                return $this->db->error();
            }   
            else return $this->db->error();
        }

        public function set($data)
        {
            $this->db->insert('inscripciones', $data);
            $error = $this->db->error();
            if($error['code']=="0")
            {
                return $this->db->error();
            }   
            else return $this->db->error();
        }

        public function get($data)
        {
            $this->db->where($data);
            $query = $this->db->get('inscripciones');
            return $query->row();
        }

        public function update($id,$data)
        {
            $this->db->where("id",$id);
            $this->db->update("inscripciones",$data);
        }
        public function obtener_inscriptos($club)
        {
            if ($club==1) {
                 $sql="SELECT users.documento,users.first_name,users.last_name,users_groups.colegio_id, users.rama,divisiones.nombre, users_groups.user_id, users_groups.id, nodocolegio.nombre as nombreclub, inscripciones_div.divisiones_id,users.fechanac 
             FROM `inscripciones_div`
             JOIN users_groups on users_groups.id=inscripciones_div.user_id 
             JOIN users on users.id= users_groups.user_id 
             JOIN divisiones on divisiones.id=inscripciones_div.divisiones_id 
             JOIN especialidades on especialidades.id=divisiones.especializacion_id
             JOIN nodocolegio on nodocolegio.id=users_groups.colegio_id";
             $query=$this->db->query($sql);
             
             return $query->result();
            }else
            {
                 $sql="SELECT users.documento,users.first_name,users.last_name,users_groups.colegio_id, users.rama,divisiones.nombre,nodocolegio.nombre as nombreclub, inscripciones_div.divisiones_id, users.fechanac
             FROM `inscripciones_div`
             JOIN users_groups on users_groups.id=inscripciones_div.user_id 
             JOIN users on users.id= users_groups.user_id 
             JOIN divisiones on divisiones.id=inscripciones_div.divisiones_id 
             JOIN especialidades on especialidades.id=divisiones.especializacion_id
             JOIN nodocolegio on nodocolegio.id=users_groups.colegio_id
             where users_groups.colegio_id=$club";
             $query=$this->db->query($sql);
             return $query->result();

            }
              
                
             
        }
        public function obtener_dato_inscriptos($data,$club)
        {   //echo "club".$club;

            if ($club==1) {

            $sql="SELECT users.documento,users.first_name,users.last_name,users_groups.colegio_id, users.rama, users_groups.user_id, users_groups.id as id_usergroup,users.fechanac,nodocolegio.nombre as nombreclub
             FROM users             
             JOIN users_groups on users_groups.user_id = users.id
             JOIN nodocolegio on nodocolegio.id=users_groups.colegio_id
             JOIN groups ON groups.id = users_groups.group_id 
             WHERE (users.documento LIKE '%".$data."%'
             OR users.first_name LIKE '%".$data."%'
             OR users.last_name LIKE '%".$data."%') and groups.name='DEPORTISTA'"
             ;

            }
            else{

             $sql="SELECT users.documento,users.first_name,users.last_name,users_groups.colegio_id, users.rama, users_groups.user_id, users_groups.id,users.fechanac  
             FROM users             
             JOIN users_groups on users_groups.user_id = users.id
             JOIN nodocolegio on nodocolegio.id=users_groups.colegio_id
             JOIN groups ON groups.id = users_groups.group_id 
             WHERE (users.documento LIKE '%".$data."%'
             OR users.first_name LIKE '%".$data."%'
             OR users.last_name LIKE '%".$data."%')
             AND users_groups.colegio_id = $club and groups.name='DEPORTISTA'" ;
            }
            
             $query=$this->db->query($sql);
             
             return $query->result();
         }
       public function obtener_categoria($div,$edad){
            $sql = "SELECT *  FROM `categorias` WHERE `divisional` = $div AND `edad` <= $edad";
            $query = $this->db->query($sql);
            
           if($query->num_rows()!=0){
            return $query->result();
           }
           else return false;

       }
           
         public function get_datos_para_modificar($id,$div)
        {
            
            $sql="SELECT inscripciones_div.id as id_inscripcion ,users.documento,users.first_name,users.last_name,users_groups.colegio_id, users.rama,divisiones.nombre
             FROM `inscripciones_div`
             JOIN users_groups on users_groups.id=inscripciones_div.user_id 
             JOIN users on users.id= users_groups.user_id 
             JOIN divisiones on divisiones.id=inscripciones_div.divisiones_id 
             JOIN especialidades on especialidades.id=divisiones.especializacion_id
             where inscripciones_div.user_id=$id and inscripciones_div.divisiones_id=$div";
             $query=$this->db->query($sql);

             
             return $query->result();
         }
         public function update_inscripcion($div,$inscrip){
            $sql="UPDATE `inscripciones_div` SET `divisiones_id`= $div WHERE id= $inscrip ";
            $query=$this->db->query($sql);


         } 
          public function traer_categoria_inscripcion($id)
        {
            $sql="SELECT categorias.descripcion FROM `inscripcion_a_torneo`
             JOIN categorias ON categorias.id = inscripcion_a_torneo.categoria
             
             where inscripcion_a_torneo.categoria= $id";
             $query=$this->db->query($sql);

             
             return $query->result();
        }       
           
          

       

	}