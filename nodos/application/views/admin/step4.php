
<script type="text/javascript">

	var arreTerminadoP4=Array();
	//FUNCION PARA capturar momento en que terminan los dos procesos. regreso de las 2 peticiones iniciales
	function terminadoP4(pet)
	{ //alert(pet);					
		if(pet==1)
			arreTerminadoP4[0]=1; //cargo cualquier cosa
		if(pet==2)
			arreTerminadoP4[1]=1;
		if(pet==3)
			arreTerminadoP4[2]=1;

		if(arreTerminadoP4.length==3)
		{   //console.log('muestro');
			$('#cuerpo img:last-child').remove();
			$('#paso4').css('display','block');
			arreTerminadoP4 = Array();
			$('#paso4').slideDown();
		}

		//console.log(arreTerminado);
	}

	//FUNCION PARA VACIAR LA TABLA HTML
	function vaciar_tablaP4(){			
		var rowCount = $('#ListInicioP4 tr').length;

		if(rowCount > 1)
			for(var i=0; i<(rowCount - 1) ; i++)
				$('#ListInicioP4 tr:last').remove();
	}



	function listar_nivelesEspecialidades()
	{ 			
			vaciar_tablaP4();

			var url = urlApi+"/nivel/obtener_NivEspcargadas";		
			var nombregrupo = "Institucion";

			$.ajax({
	                url: url,
	                type: 'GET',
	                headers: {				      
				        'APIKEY' : passApiKey,
				        'userid' : idUser
				    },
				    data:{idcolegio:idcolegio, nombregrupo:nombregrupo},
	                success: function(data){  
	                	if(data['status']==false)
                   		{ 
                   		 	//console.log(data['message']);
                   		 	if(data['message']=='NO LOGUIN')
                   		 		location.href = "<?php echo base_url('login'); ?>"; //no està logueado
                   		 	//terminadoP4(1); terminadoP4(2); terminadoP4(3);
                   		}
                   		else
                   		{ 
                   			var data = jQuery.parseJSON( data );   

							var filaclass = 'par'
							for(var i=0; i < data.length ; i++) //aca solo listo niveles y especialidades
							{																			
								if(i%2==0)
						    		 filaclass = 'par';
						    	else filaclass = 'impar';

								if(data[i]['esp_id'])								
									var nombre_nivel = data[i]['nombre_nivel']+ ": &nbsp<span>"+data[i]['nombre_especialidad']+"</span>";
								else
									var nombre_nivel = data[i]['nombre_nivel'];

								var texto = "<tr class='"+filaclass+"' id='fila_"+i+"'>"+
													"<td id='nivel_"+i+"' rowspan=''>"+ nombre_nivel + "</td>"+		
											"</tr>"+
											"<tr id='tope_"+i+"'></tr> ";	

								$('#ListInicioP4 tbody tr:last').after(texto);					    	
							}
							
							for(var i=0; i < data.length ; i++)
							{	//console.log(i);														
						    	if(i%2==0)
						    		 filaclass = 'par';
						    	else filaclass = 'impar';

						    	var idnodonivel = data[i]['nodonivel_id'];
								if(data[i]['esp_id'])
									var esp_id = data[i]['esp_id'];		
								else  var esp_id = -1;															
								
								//console.log(i +' -- '+ (data.length-1));
							    if(i == data.length-1) //en la ultima iteracion mando un 1.  para saber cuando termina
								{   //alert('son iuales');
									traer_anios(idnodonivel, esp_id, i, 1, filaclass); // i: "indice" lo uso para que sepa en que fila hay que agregar los años
								}								
								else
									traer_anios(idnodonivel, esp_id, i, 0, filaclass); // i: "indice" lo uso para que sepa en que fila hay que agregar los años
							}

						terminadoP4(1);
                   		}                							
					},
                error: function(response){
                    console.log(response);
                    
                }
					 	
	       });		
	}

	function traer_anios(idnodonivel, indice, fin, filaclass)
	{ 
			//vaciar_contenedoresP1();
			var parametros = Array();
	        var par = Array({idnodonivel:idnodonivel, nombrenodonivel:""});
	        parametros.push(par);
	        var parametros = JSON.stringify(parametros);
			var url = urlApi+"/anio/obtener_aniosBynodoniv";
			var todos = 0;
			$.ajax({
	                url: url,
	                type: 'GET',
	                headers: {				      
				        'APIKEY' : passApiKey,
				        'userid' : idUser
				    },
				    data: {parametros:parametros,todos:todos,nombregrupo:nombregrupo,idcolegio:idcolegio },
	                success: function(data){ 
	                	           	
	                	if(data['status']==false)
                   		{
                   		 	//console.log(data['message']);
                   		 	if(data['message']=='NO LOGUIN')
                   		 		location.href = "<?php echo base_url('login'); ?>"; //no està logueado    

                   		 	if(fin)
                   		 	{	
                   		 		terminadoP4(2);	terminadoP4(3);	
                   			}
                   		}                   		
                   		else
                   		{
                   			var datos = jQuery.parseJSON( data );             
                   			datos = jQuery.parseJSON(data);
                            datos = datos['resul'][0]['anios'];
                            console.log(datos);
                   			for(var i=0; i <  (datos.length) ; i++)
							{
								console.log(datos[i]['id']);
								var uldivision = "<form><input type='text' placeholder='1RA O A' id='newdivision_"+indice+"_"+i+"' /></form>";
                 					
                 				$('#ListInicioP4 #fila_'+indice+' #nivel_'+indice).attr('rowspan',datos.length);

								if(i==0)
								{	var texto = "<td class='tdanio' name='tdanio' id='anio_"+datos[i]['id']+"'>"+datos[i]['nombre']+				
							 					"</td> <td class='tddiv'>"+uldivision+"</td>";	
							 		$('#ListInicioP4 #fila_'+indice).append(texto);			
								}
								else {
									var texto = "<tr id='filatd_"+i+"' class='"+filaclass+"'><td class='tdanio' name='tdanio' id='anio_"+datos[i]['id']+"'>"+datos[i]['nombre']+
							 					"</td> <td class='tddiv'>"+uldivision+"</td></tr>";
							 		
							 		$('#ListInicioP4 #tope_'+indice).before(texto);	
							 		}	
		
								if( (i == datos.length-1) && (fin) )//en la ultima iteracion mando un 1.
									listar_divisiones_cargadas(datos[i]['id'], 1);  //divisiones por idaño
								else
									listar_divisiones_cargadas(datos[i]['id'], 0);  //divisiones por idaño	
							}
						   

						    for(var i=0; i <  (datos.length) ; i++)
							{
								$('#newdivision_'+indice+'_'+i).keypress(function(e) {

								    if(e.which == 13) {
								    	e.preventDefault();
								       var newD = $(this).val();
								       if(newD != '')
								       	{	
								       		 var newli =  "&nbsp;<span>"+newD+
								       		  					"<a class='close'> </a>"+
								       		  				"</span>";

								       		 $(this).parent('form').parent('td').append(newli);
								       		 
								       		 $(this).val('');
								       		 $(this).focus();

								       		 $('.close').click(function(e) {
												$(this).parent('span').remove();	
											 });
								       	}
								       	else alert('ingrese division');
								    }
								});										
							}

						if(fin)
							terminadoP4(2);	

                   		}              

	                },
	                error: function(response){
	                    console.log(response);	                    
	                }
					 	
	       });	
	
    }

	function listar_divisiones_cargadas(idanio, fin) 
	{  
			//vaciarP4();
			var url = urlApi+"/division/divisiones_cargadas";

			$.ajax({
	                url: url,
	                type: 'GET',
	                headers: {				      
				        'APIKEY' : passApiKey,
				        'userid' : idUser
				    },
				    data:{idanio:idanio},
	                success: function(data){  console.log();
	                	
		                	if(data['status']==false)	                   		 	
                   			{		            
		                		if(data['message'] == 'NO LOGUIN')		                                  	
		                     		 location.href = "<?php echo base_url('login'); ?>";
		                		if(fin) //si esta en 1
		                			terminadoP4(3);
							}
							else
							{
								var datos = jQuery.parseJSON( data );	
								for(var i=0; i < datos.length ; i++)
								{
								    var newli = " <span>"+datos[i]['nombre']+
								       		  		"<a class='close' id='"+datos[i]['id']+"'></a>"+
								       		  	"</span>";

								    $('#anio_'+idanio).parent('tr').find('form').after(newli);


								    $('#'+datos[i]['id']).click(function(e) {	
								    	var idD = $(this).attr('id'); 
										var divi = $(this).parent('span');	

										bootbox.confirm("¿Desea Eliminar la División?", function(result)
										{ 
											if(result)
											{
												BajaDivision(idD);
												$(divi).remove();
											}
										});										
									});

								}	
							if(fin) //si esta en 1	
								terminadoP4(3);	
							}									
					},
                error: function(response){
                    console.log(response);
                    
                }
					 	
	       });			
	}

			

	//FUNCTION PARA GUARDAR DATOS DEL PASO 4
	function guardar_paso4(arre){
		  var urlRequest = urlApi+"/division/insertdivisiones";	

		  $.ajax({
                url: urlRequest,
                type: 'POST',
                headers: {		
			        'APIKEY' : passApiKey,
			        'userid' : idUser
			    },
			    data: {data: arre, idusergrupo: idusergrupo},
                success: function(data){
                    //console.log(data);
					if(data['status']==true)
                   	{	 //console.log(data['message']);
                   		$('.top-rightt').notify({
		                        message: { text: data['message'] },
		                        fadeOut: { enabled: true, delay: 5000 },
		                        type: 'success2'
		                }).show();
                   	}
                   	else{
	                     //console.log(data['message']);
	                     if(data['message'] != 'NO LOGUIN')
	                     	$('.top-right').notify({
		                        message: { text: data['message'] },
		                        fadeOut: { enabled: true, delay: 5000 },
		                        type: 'danger2'
		                    }).show();
	                     else
	                     	 location.href = "<?php echo base_url('login'); ?>";

                   		}
                },
                error: function(response){
                    console.log(response);
                    
                }
            });
	}

	
	function BajaDivision(idD)
	{
			//alert('bajaD');
			var url = urlApi+"/division/baja_division";

			$.ajax({
	                url: url,
	                type: 'POST',
	                headers: {				      
				        'APIKEY' : passApiKey,
				        'userid' : idUser
				    },
				    data:{iddiv:idD},
	                success: function(data){ //console.log(data);
	                	
		                	if(data['status']==false)	                   		 	
                   			{
		                		if(data['message'] == 'NO LOGUIN')		                                  	
		                     		 location.href = "<?php echo base_url('login'); ?>";
		                     	else 
		                     		$('.top-right').notify({
			                       		message: { text: data['message'] },
			                        	fadeOut: { enabled: true, delay: 5000 },	
			                        	type: 'danger2'                     
			                    	}).show();
							}
							else
							{
								$('.top-right').notify({
			                        message: { text: data['message'] },
			                        fadeOut: { enabled: true, delay: 3000 },
			                        type: 'success2'		                     
			                    }).show();
							}
					},
                error: function(response){
                    console.log(response);
                    
                }
					 	
	       });			
	}


	$(document).ready(function(){
		
		//$('#nivelesP4 tbody').hide();
	
		$('#atras4').click(function(e)
		{               
            e.preventDefault();
         
            listar_nivelesEspecialidadesAnios();

            //slide steps
            $('#paso4').slideUp();
            //$('#cuerpo').append("<img src='<?=base_url('assets/images/loading.gif');?>' height='60' width='60' />");
            $('#paso3').slideDown();            
            $('#paso4').css('display','none'); 

        });
 

		/*PASO 4 - ENVIAR divisiones para guardar y finalizar */
		$('#next4').click(function(e)
		{ 
			e.preventDefault();
			var parametros = Array();
			
			$(".tdanio").each(function (index) 
			{
	            var idanio = $(this).attr('id');
				idanio = idanio.split('_');                          	 
                idanio = idanio[1];

                var tddiv = $(this).parent('tr').find('.tddiv').find('span');

                if($(tddiv).length > 0)
                {
		           $(tddiv).each(function (index2){ 
	                    var nombdiv = $(this).text();
						console.log(nombdiv);
						var elemento = {
							  'nombre': nombdiv,
						      'anios_id': idanio,
						      'orden': index2+1
						    };
						parametros.push( elemento );
			        });
			     }	        
	            
	        })

			var stringJson = JSON.stringify(parametros);	
			//console.log(stringJson);
			guardar_paso4(stringJson);		
			vaciar_tablaP4();

			$('#paso4').slideUp();   
            $('#outro_nivel').slideDown();            
            $('#paso4').css('display','none');
		});      

	});  			 				   
			
</script>


	<div class = "paso4" id="paso4" style='display:none; ' >
		<!--<div style="overflow: auto; height:auto">	-->
			
			<!-- Indicadores de Pasos -->
			<div class="container-fluid nopadding">
				<div class="row nopadding center indicadores-pasos">
					<div id="paso-nivel" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding ok">
						Paso 1: Niveles <span><i class="fa fa-check-circle-o"></i></span>
					</div>
					<div id="paso-especialidad" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding ok">
						Paso 2: Especialidades <span><i class="fa fa-check-circle-o"></i></span>
					</div>
					<div id="paso-ano" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding ok">
						Paso 3: A&ntilde;os <span><i class="fa fa-check-circle-o"></i></span>
					</div>
					<div id="paso-division" class="col-lg-3 col-md-3 col-sm-3 col-xs-6 nopadding">
						Paso 4: Divisiones <span><i class="fa fa-check-circle-o"></i></span>
					</div>
				</div>
			</div>
			
			<div class="container-fluid">
				<!-- Descripción Paso -->
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center vspace2">
						<h3>Paso 4</h3>
						<h6>Asigne las divisiones</h6>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center vspace">
						<p>Asigne las diviones seg&uacute;n cada a&ntilde;o.<br>
						Genere la cantidad de divisiones ingresando de a una (Ej: &ldquo;1&ordm;1ra&rdquo; o &ldquo;1&ordm;A&rdquo;).</p>
					</div>
				</div>

				<!-- Funcionalidad -->
				<div class="row">
					
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						
						<table class="pasos" id="ListInicioP4">
							<tr>
								<th>Nivel / Especialidad</th>
								<th>A&ntilde;o</th>
								<th>Divisiones: Ingrese divisi&oacute;n y presione enter</th>
							</tr>
							<!--<tr>
								<td>Educación Primaria: <span>Miner&iacute;a</span></td>
								<td>1º Grado</td> 
								<td>
									<form>
										<input type="text" placeholder="“1ra” o “A”">
									</form>
									<span>A</span>
									<span>B</span>
									<span>C</span>
								</td>
							</tr>	-->
						</table>
					</div>
									
				</div>
				
				<!-- Anterior/Siguiente -->
				<div class="row vspace4">
					<div id="atras4" class="col-lg-offset-3 col-lg-3 col-md-offset-3 col-md-3 col-sm-offset-3 col-sm-3 col-xs-6">
						<a class="btn-principal-azul" href="">Anterior Paso</a>
					</div>
					<div id="next4" class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
						<a class="btn-principal-azul" href="">Finalizar</a>
					</div>				
				</div>

				
				
			</div>

    <!--</div>-->
</div>
     



<style type="text/css">


.par{ background: #f1f1f1;  }

.impar{background: #ccc;}


.close {
  width: 12px;
  background: url('<?=base_url('assets/images/chosen-sprite.png'); ?>') -42px 1px no-repeat;
  cursor:hand;
  /*position: absolute;*/
  top: 2px;
  margin-right: -10px;
  margin-top: -3px;
  display: block;
  height: 12px;
  font-size: 1px;
  background-position: -42px -10px;
}

.close:hover {
  background-position: -42px -10px;
}

</style>
