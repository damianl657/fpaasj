<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';


    class Paseanio extends REST_Controller {

    function __construct()
    {
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        
        parent::__construct();
       
        
        // Configurar limites para cada uno de los metodos, no solo controlador
        // La tabla limits tiene que estar creada y la opcion limits TRUE en application/config/rest.php
        
        //$this->methods['tutores_get']['limit'] = 1; // 500 peticiones por hora por usuario/key
        //$this->methods['user_post']['limit'] = 100; // 100 peticiones por hora por usuario/key
        //$this->methods['user_delete']['limit'] = 50; // 50 peticiones por hora por usuario/key
    }



    /*public function pase_niveles_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            //$data = json_decode($_POST['data']) ;
            $nodos = $this->utilidades->IsRol($userid, 'Nodos'); //pregunto si es usuario nodos      

            if($nodos['sies'])
            {
                $planest_New = $this->utilidades->insertar_planestudio();  //verifica e inserta si es necesario
                
                $anioPasado = date('Y') - 1;
                $planest_Old = $this->utilidades->get_planestudio( $anioPasado );  

                
                $this->load->model("modelo_colegio");
                $this->load->model("modelo_nivel");
                $this->load->model("modelo_anio");
                $this->load->model("modelo_division");



            //RECORRE COLEGIOS
                $colegios = $this->modelo_colegio->obtener_colegios_activos();

                if($colegios)
                {
                    foreach ($colegios as $col) 
                    {
                        $colegioId = $col->id;

                        $nivelesNew = $this->modelo_nivel->obtener_niveles_cargados($colegioId, $planest_New); //deberia llegar vacio, se supone que todavia no se cargan

                        if(! $nivelesNew) //debe llegar vacio, de lo contrario significa que ya se cargaron por lo que no debe entrar.
                        {
            //RECORRE NIVELES    
                            $nivelesOld = $this->modelo_nivel->obtener_niveles_cargados($colegioId, $planest_Old);

                            if($nivelesOld)
                                foreach ($nivelesOld as $niv) 
                                {
                                    $nivel = $niv->niveles_id;
                                    $nodoNivelOld = $niv->idnodonivel;

                                    $data = array(
                                        'nodocolegio_id'=>$colegioId,
                                        'niveles_id'=>$nivel,
                                        'esp_id'=>$niv->esp_id,
                                        'ultimo'=> $niv->ultimo,
                                        'cicloa_id'=>$planest_New,
                                        'estado'=>1,
                                        'nodonivel'=>$nodoNivelOld,
                                    );

                                    if( $this->modelo_nivel->insertar_nodonivel($data) )
                                        $nodoNivelNew =  $this->db->insert_id();

            //RECORRE AÑOS 
                                    $aniosOld = $this->modelo_anio->get_nodoanios($nodoNivelOld);

                                    if( $aniosOld )
                                        foreach ($aniosOld as $anioO) 
                                        {
                                            $anio = $anioO->anio_id;
                                            $nodoAnioOld = $anioO->id;

                                            $data = array(
                                                'anio_id'=>$anio,
                                                'nodoniv_id'=>$nodoNivelNew,
                                                'ultimo'=> $anioO->ultimo,
                                                'estado'=>1,
                                                'cicloa_id'=>$planest_New,
                                                'nodoanio'=>$nodoAnioOld,
                                            );
                                            //agregar cicloa_id a la tabla nodoanio!!!!

                                            if( $this->modelo_anio->insertar_nodoanio($data) )
                                                $nodoAnioNew = $this->db->insert_id();

            //RECORRE DIVISIONES                                            
                                            $divisOld = $this->modelo_division->get_divisionesXanios($nodoAnioOld,$planest_Old); //tambien podria enviar el ciclo Actual

                                            if( $divisOld )
                                                foreach ($divisOld as $divi) 
                                                {
                                                    $nombre = $divi->nombre;
                                                    $divisionOld = $divi->id;
                                                    $orden = $divi->orden;
                                                    $siguiente = $divi->div_siguiente;

                                                    $data = array(
                                                        'anios_id'=>$nodoAnioNew,
                                                        'nombre'=>$nombre,
                                                        'orden'=>$orden,
                                                        'planesestudio_id'=>$planest_New,
                                                        'estado'=>1,
                                                        'div_siguiente'=>$siguiente,
                                                        'nododivision'=>$divisionOld,
                                                    );

                                                    if( $this->modelo_division->insertar_nododivision($data) )
                                                        $nodoDiviNew = $this->db->insert_id();
                                                }
                                        }

                                }

                        }
                        else print_r("Ya se hizo antes");

                    }
                }
                
            }



            if (isset($planest_New) )
            {
                $this->response([
                    'status' => TRUE,
                    'message' => "PaseDeAnio:OK:$planest_New:$planest_Old",
                ], REST_Controller::HTTP_OK);  
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'no tiene permisos'
                ]); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }

    public function pase_materias_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            //$data = json_decode($_POST['data']) ;
            $nodos = $this->utilidades->IsRol($userid, 'Nodos'); //pregunto si es usuario nodos      

            if($nodos['sies'])
            {
                $planest_New = $this->utilidades->insertar_planestudio();  //verifica e inserta si es necesario
                
                $anioPasado = date('Y') - 1;
                $planest_Old = $this->utilidades->get_planestudio( $anioPasado );  
        
                $this->load->model("modelo_colegio");
                //$this->load->model("modelo_nivel");
                $this->load->model("modelo_anio");
                $this->load->model("modelo_materias");
                //$this->load->model("modelo_division");


            //RECORRE COLEGIOS
                $colegios = $this->modelo_colegio->obtener_colegios_activos();

                if($colegios)
                {
                    foreach ($colegios as $col) 
                    {
                        $colegioId = $col->id;

                        $aniosmateriaNew = $this->modelo_materias->obtener_aniosmaterias_pase1($colegioId, $planest_New); //deberia llegar vacio, se supone que todavia no se cargan

                        if(! $aniosmateriaNew) //debe llegar vacio, de lo contrario significa que ya se cargaron por lo que no debe entrar.
                        {   
                            $anisomateriaOld = $this->modelo_materias->obtener_aniosmaterias_pase1($colegioId, $planest_Old);

                            if($anisomateriaOld)
                                foreach ($anisomateriaOld as $row) 
                                {
                                    $estereg = $row->id;
                                    $nodoanios_idOld = $row->nodoanios_id;
                                    
                                    $nodoanioOld = $this->modelo_anio->get_nodoanio_new($nodoanios_idOld);

                                    if($nodoanioOld)
                                        foreach ($nodoanioOld as $fila) 
                                        {
                                            $nodoanioNew = $fila->id;
                                        }


                                    $data = array(
                                        'nodoanios_id'=>$nodoanioNew,
                                        'materias_id'=>$row->materias_id,
                                        'nombresmateria_id'=>$row->nombresmateria_id,
                                        'colegio_id'=>$row->colegio_id,
                                        'cicloa'=>$planest_New,
                                        'anio_materia'=> $estereg,
                                    );

                                    $this->modelo_materias->insertar_aniosmaterias($data);
                                }

                        }
                        else print_r("Ya se hizo antes");

                    }
                }
                
            }



            if (isset($planest_New) )
            {
                $this->response([
                    'status' => TRUE,
                    'message' => "PaseDeAnio:OK:$planest_New:$planest_Old",
                ], REST_Controller::HTTP_OK);  
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'no tiene permisos'
                ]); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }


    public function pase_horarios_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            //$data = json_decode($_POST['data']) ;
            $nodos = $this->utilidades->IsRol($userid, 'Nodos'); //pregunto si es usuario nodos      

            if($nodos['sies'])
            {
                $planest_New = $this->utilidades->insertar_planestudio();  //verifica e inserta si es necesario
                
                $anioPasado = date('Y') - 1;
                $planest_Old = $this->utilidades->get_planestudio( $anioPasado );  
        
                $this->load->model("modelo_colegio");
                //$this->load->model("modelo_nivel");
                //$this->load->model("modelo_anio");
                $this->load->model("modelo_materias");
                $this->load->model("modelo_division");
                $this->load->model("modelo_horarios");



                //RECORRE COLEGIOS - inserta  horarios
                $colegios = $this->modelo_colegio->obtener_colegios_activos();

                if($colegios)
                {
                    foreach ($colegios as $col) 
                    {
                        $colegioId = $col->id;

                        $horariosNew = $this->modelo_horarios->obtener_horarios_pase1($colegioId, $planest_New); //deberia llegar vacio, se supone que todavia no se cargan

                        if(! $horariosNew) //debe llegar vacio, de lo contrario significa que ya se cargaron por lo que no debe entrar.
                        {   
                            $horariosOld = $this->modelo_horarios->obtener_horarios_pase1($colegioId, $planest_Old);

                            if($horariosOld)
                                foreach ($horariosOld as $row) 
                                {
                                    $data = array(
                                        'hora_inicio'=>$row->hora_inicio,
                                        'hora_fin'=>$row->hora_fin,
                                        'numero'=>$row->numero,
                                        'colegio_id'=>$row->colegio_id,
                                        'estado'=>$row->estado,
                                        'planesestudio_id'=>$planest_New,
                                        'horario_id'=> $row->id,
                                    );

                                    $this->modelo_horarios->insertar_horario($data);
                                }

                        }
                        else print_r("Ya se hizo antes:horarios");

                    }
                }


                //RECORRE COLEGIOS - inserta  horarios_materias
                $colegios = $this->modelo_colegio->obtener_colegios_activos();

                if($colegios)
                {
                    foreach ($colegios as $col) 
                    {
                        $colegioId = $col->id;

                        $horariosMatNew = $this->modelo_horarios->obtener_horariosmaterias_pase1($colegioId, $planest_New); //deberia llegar vacio, se supone que todavia no se cargan

                        if(! $horariosMatNew) //debe llegar vacio, de lo contrario significa que ya se cargaron por lo que no debe entrar.
                        {   
                            $horariosMatOld = $this->modelo_horarios->obtener_horariosmaterias_pase1($colegioId, $planest_Old);

                            if($horariosMatOld)
                                foreach ($horariosMatOld as $row) 
                                {
                                    //trae el mismo horario, pero del ciclo actual
                                    $horarioNew = $this->modelo_horarios->get_horario_new($row->horarios_id);
                                    if($horarioNew)
                                        foreach ($horarioNew as $fila) 
                                        {
                                            $horarioIdNew = $fila->id;
                                        }


                                    $divisionNew = $this->modelo_division->get_division_new($row->divisiones_id);
                                    if($divisionNew)
                                        foreach ($divisionNew as $fila) 
                                        {
                                            $divisionIdNew = $fila->id;
                                        }


                                    $aniomateriaNew = $this->modelo_materias->get_aniomateria_new($row->materias_id);
                                    if($aniomateriaNew)
                                        foreach ($aniomateriaNew as $fila) 
                                        {
                                            $aniomateriaIdNew = $fila->id;
                                        }



                                    $data = array(
                                        'dias_id'=>$row->dias_id,
                                        'docente_id'=>$row->docente_id,
                                        'lugar'=>$row->lugar,
                                        'colegio_id'=>$row->colegio_id,

                                        'planesestudio_id'=>$planest_New,

                                        'horarios_id'=>$horarioIdNew,
                                        'materias_id'=>$aniomateriaIdNew,
                                        'divisiones_id'=>$divisionIdNew,

                                        'horariomateria'=>$row->id,
                                    
                                    );

                                    $this->modelo_horarios->insertar_horario_materia($data);
                                }

                        }
                        else print_r("Ya se hizo antes:horariomateria");

                    }
                }
                
            }



            if (isset($planest_New) )
            {
                $this->response([
                    'status' => TRUE,
                    'message' => "PaseDeAnio:OK:$planest_New:$planest_Old",
                ], REST_Controller::HTTP_OK);  
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'no tiene permisos'
                ]); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }

    public function pase_preceptores_post() 
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            //$data = json_decode($_POST['data']) ;
            $nodos = $this->utilidades->IsRol($userid, 'Nodos'); //pregunto si es usuario nodos      

            if($nodos['sies'])
            {
                $planest_New = $this->utilidades->insertar_planestudio();  //verifica e inserta si es necesario
                
                $anioPasado = date('Y') - 1;
                $planest_Old = $this->utilidades->get_planestudio( $anioPasado );  
        
               
                $this->load->model("modelo_preceptor");


                //RECORRE preceptores del ciclo actual - debe llegar vacio
                $preceptores = $this->modelo_preceptor->get_preceptoresXciclo($planest_New);

                if(! $preceptores) //debe llegar vacio, de lo contrario significa que ya se cargaron por lo que no debe entrar.
                {
                    $preceptores = $this->modelo_preceptor->get_preceptoresXciclo($planest_Old);

                    foreach ($preceptores as $reg) 
                    { 
                        $datos = array(
                                'user_id' => $reg->user_id, //mismo valor
                                'division_id' => $reg->division_id, //mismo valor
                                'colegio_id'=> $reg->colegio_id, //mismo valor
                                'planesestudio_id' => $planest_New, //nuevo
                                'activo' => $reg->activo, //mismo valor
                                'preceptor_id'=> $reg->id, //nuevo
                        );

                        $this->modelo_preceptor->insert_preceptor_div($datos); 
                    }

                }else print_r("Ya se hizo antes:preceptores");
                
            }



            if (isset($planest_New) )
            {
                $this->response([
                    'status' => TRUE,
                    'message' => "PaseDeAnio:OK:$planest_New:$planest_Old",
                ], REST_Controller::HTTP_OK);  
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'no tiene permisos'
                ]); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }*/

    public function pase_inscripciones_post()
    {
        $userid = $this->utilidades->verifica_userid();
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            //$data = json_decode($_POST['data']) ;
            $nodos = $this->usuarios->IsRol($userid, 'Nodos'); //pregunto si es usuario nodos      

            if($nodos['sies'])
            {
                //$planest_New = $this->utilidades->insertar_planestudio();  //verifica e inserta si es necesario
                $ciclo_new = date('Y');
                $ciclo_old = date('Y') - 1;
                //$planest_Old = $this->utilidades->get_planestudio( $anioPasado );  
        
                //$this->load->model("modelo_colegio");
                $this->load->model("modelo_alumno");
                $this->load->model("modelo_anio");
                $this->load->model("modelo_division");
                //$this->load->model("modelo_nivel");




                //RECORRE inscripciones del ciclo actual - debe llegar vacio
                $inscripciones = $this->modelo_alumno->get_inscripcionesXciclo($ciclo_new);

                if(! $inscripciones) //debe llegar vacio, de lo contrario significa que ya se cargaron por lo que no debe entrar.
                {
                    $inscripciones = $this->modelo_alumno->get_inscripcionesXciclo($ciclo_old);

                    foreach ($inscripciones as $reg) 
                    {
                        
                        //$diviNew = $this->modelo_division->get_division_new($reg->divisiones_id); //le mando la division vieja.
                        $divi = $this->modelo_division->obtener_division_anio_nivel($reg->divisiones_id); 

                        if($divi)
                        {
                            
                            $nombreDiviSig = $divi->div_siguiente;
                            $nombreDiv = $divi->nombre;
                            //$anioNew = $this->modelo_anio->get_nodoanio($divi->nodoanio_id); 
                            
            
                            //VERIFICAR SI ES EL ULTIMO ANIO, debe pasar a egresados
                            $ultimoAnio = $divi->ultimo_anio;
                            $ultimoNivel = $divi->ultimo_niv;
                            $colegioId = $divi->nodocolegio_id;
                            $nodonivelId = $divi->nodoniv_id;
                            $nodoanioId = $divi->nodoanio_id;
                            $anioId = $divi->anio_id;
                            $nivelId = $divi->niveles_id;
                            $nivelOrden = $divi->orden_niv;

                            //$planEstudioId = $divi->cicloa_id;

                            $planEstudio = $this->utilidades->obtener_planestudioaXcolegio($colegioId);
                            $planEstudioId = $this->utilidades->get_planestudio($planEstudio);
    
                            
                            if( $ultimoAnio == 0) //no es el ultimo anio del nivel
                            {    
                                $inscribir=1;
                            }
                            else {  $inscribir=0;
                                    //insertar en egresados
                                    
                                    /*$anioOld = $this->modelo_anio->get_nodoanio($fila->nodoanio);
                                    if($anioOld)
                                        foreach ($anioOld as $row) 
                                        {
                                            $nivelOld = $row->nodoniv_id;
                                            $anioAnt = $row->id;
                                        }*/

                                    $datos = array(
                                        'usergroup' => $reg->alumno_id,
                                        'nodocolegio' => $colegioId, 
                                        'nodonivel'=> $nodonivelId, //nivel viejo
                                        'nodoanio' => $nodoanioId, //anio viejo
                                        'nododivision' => $reg->divisiones_id, //division viejo
                                        'ultimo'=> $ultimoNivel,
                                        'ciclo'=> $ciclo_old,
                                    );
                                    $this->modelo_alumno->insert_egresado($datos); 

                                    

                                    /*if( $ultimoNivel == 0 )
                                    //inserar en la primer division que encuentre del nivel y anio siguiente, cualquier especialidad.
                                    {
                                        $nivelSig=$this->modelo_nivel->get_nivel_sig($nivelOrden);

                                        if($nivelSig){  

                                            $nivelSigId = $nivelSig->id;

                                            $nodonivelSig = $this->modelo_nivel->get_nodonivel_sig($nivelSigId, $colegioId, $planEstudioId); //el primero que agarra
                                            
                                            if($nodonivelSig){

                                                $nodonivelSigId = $nodonivelSig->id;

                                                $nodoanioSig=$this->modelo_anio->get_nodoanio_sig($nodonivelSigId, $planEstudioId); //el primero que agarra

                                                
                                                if($nodoanioSig){

                                                    $nodoanioSigId = $nodoanioSig->id;

                                                    $divisionSig=$this->modelo_division->get_divi_sig($nodoanioSigId, $planEstudioId); //el primero que agarra

                                                    $data = array(
                                                        'alumno_id'=>$reg->alumno_id,
                                                        'activo'=>$reg->activo,
                                                        'cicloa'=>$ciclo_new,
                                                        'divisiones_id'=> $divisionSig->id,
                                                    );
                                                    $this->modelo_alumno->insert_inscripcion($data);
                                                }

                                            }
                                        } 
                                    }
                                    else TRATAR CUANDO PASA A OTRO NIVEL Y OTRO COLEGIO*/

                                    //LOS ANIOS PRIMEROS QUEDAN VACIOS, DEL PRIMER NIVEL

                                }




                            if($inscribir==1)
                            {
                                $anio_sig = $this->modelo_anio->get_anio_sig($anioId); 


                                $datos = array(
                                    'anio_id' => $anio_sig,
                                    'nodoniv_id'=> $nodonivelId,
                                    'estado'=> 1,
                                    'cicloa_id'=> $planEstudioId,
                                );

                                //print_r($datos);

                                $anioSig = $this->modelo_anio->get_nodoanio_datos($datos); 
                                if($anioSig)
                                    foreach ($anioSig as $aux2) 
                                    { 
                                        $nodoAnioSigId = $aux2->id;
                                    }
                                else $nodoAnioSigId = 0;
                                //else print_r('-1');
                                //die();

                                $datos = array(
                                    'nombre'=> $nombreDiviSig,
                                    'anios_id'=> $nodoAnioSigId,
                                    'planesestudio_id'=> $planEstudioId,
                                    'estado'=> 1,
                                );

                                //print_r($datos);
                                $diviSig = $this->modelo_division->get_division_datos($datos); 
                                if($diviSig)
                                {
                                    foreach ($diviSig as $aux3) 
                                    { 
                                        //print_r($aux2->id.',');
                                        $diviSigId = $aux3->id;
                                    }
                                }else $diviSigId=0;

                                //die();

                                $data = array(
                                    'alumno_id'=>$reg->alumno_id,
                                    'activo'=>$reg->activo,
                                    'cicloa'=>$ciclo_new,
                                    'divisiones_id'=> $diviSigId,
                                );

                                $this->modelo_alumno->insert_inscripcion($data);
                            }
                            /*else 
                                {
                                    //insertar en tabla temporal
                                    $data = array(
                                        'alumno_id'=>$reg->alumno_id,
                                        'division_old'=>$reg->divisiones_id,
                                        'colegio_id'=> $colegioId,
                                        'ciclo_id'=>$planest_New,
                                    );

                                    $this->modelo_alumno->insert_inscripcion_temp($data);
                                }*/
                        
                        }
                    

                    }
                }else print_r("Ya se hizo antes:inscripciones");

                
            }



            if (isset($ciclo_new) )
            {
                $this->response([
                    'status' => TRUE,
                    'message' => "PaseDeAnio:OK:$ciclo_new:$ciclo_old",
                ], REST_Controller::HTTP_OK);  
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'no tiene permisos'
                ]); // NOT_FOUND (404) being the HTTP response code
            }
        }
    }




}
