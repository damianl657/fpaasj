<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    require APPPATH . '/libraries/REST_Controller.php';


    class Horarios extends REST_Controller {

    function __construct()
    {
        if (isset($_SERVER["HTTP_ORIGIN"])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Max-Age: 86400");
        }
        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
            if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if(isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        
        parent::__construct();
        $this->load->model("modelo_horarios");
        $this->load->model("modelo_horario");
       
        // Configurar limites para cada uno de los metodos, no solo controlador
        // La tabla limits tiene que estar creada y la opcion limits TRUE en application/config/rest.php
        
        //$this->methods['tutores_get']['limit'] = 1; // 500 peticiones por hora por usuario/key
        //$this->methods['user_post']['limit'] = 100; // 100 peticiones por hora por usuario/key
        //$this->methods['user_delete']['limit'] = 50; // 50 peticiones por hora por usuario/key
    }
    
    public function inserthorarios_post()
    {
        $userid = $this->utilidades->verifica_userid();
    
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $data = json_decode($_POST['data']) ;
          
            $idusergrupo = $_POST['idusergrupo']; 
            $idcolegio = $_POST['idcolegio']; 


            $this->modelo_horarios->baja_horarios($idcolegio);
      
            foreach ($data as $key ) 
            { 
                //var_dump($key);
                $res = $this->modelo_horarios->get_horario($key->hora_inicio, $key->hora_fin, $key->colegio_id); 
                
                if (!$res) //si no existe inserto                
                    $this->modelo_horarios->insertar_horario($key);
                else{ //existe
                    foreach ($res as $row )
                        if ($row->estado==0) //si existe pero fue dada de baja, vuelvo a habilitar
                        {    
                            $result = $this->modelo_horarios->update_estado_horario($row->id,1);
                            $result = $this->modelo_horarios->update_numero_horario($row->id, $key->numero);
                        }
                    } 
                
            }
            
            $this->load->model("modelo_usuario");
            $result2 = $this->modelo_usuario->update_pasos($idusergrupo,'5'); 

            if ($result2)
            {
                $this->response([
                        'status' => TRUE,
                        'message' => 'Configuracion de Horarios Completa', 
                    ], REST_Controller::HTTP_OK);  
            }
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Error al Guardar Horarios'
                ]); // NOT_FOUND (404) being the HTTP response code
            }   
        }    
    }
    public function obtener_alumnos_get()
    {
        $userid = $this->utilidades->verifica_userid();
    
        if ($userid == -1)
        {
            $this->response([
                'status' => FALSE,
                'message' => 'NO LOGUIN'
            ], REST_Controller::HTTP_OK);            
        }
        else
        {
            $this->load->model("modelo_tutor");
            $alumnos = $this->modelo_tutor->obtener_alumnos_del_tutor($userid); // 
            $consulta_alu = $this->db->last_query();
            $horarios = $this->modelo_horarios->get_horarios_all_user_id_colegio_all($userid);
            // Si existe mas de un resultado, lo mando
            if ($alumnos)
            {
                $alumnos = json_encode($alumnos);
                $horarios = json_encode($horarios);
                //print_r($eventos);
                //$eventos["query"] = $this->db->last_query();
                //$this->response($alumnos, REST_Controller::HTTP_OK);
                $this->response([
                    'status' => TRUE,
                    'alumnos' => $alumnos,
                    'consulta' => $consulta_alu,
                    'horarios' => $horarios,
                ], REST_Controller::HTTP_OK); 

            }
            // Sino, envio respuesta con 404
            else
            {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No Tiene Hijos Asignado a su Cargo',
                    'query' => $consulta_alu
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

    }
}
