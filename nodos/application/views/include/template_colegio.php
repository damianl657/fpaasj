<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	
	<title>Nodos sports</title>
	<meta name="description" content="App para una comunicaci&oacute;n escolar m&aacute;s simple" />
	<meta name="author" content="sietedefebrero.com">
	<!-- Favicon -->
	<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
	<link href="<?= base_url('assets/images/favicon.ico') ?>" rel="shortcut icon" type="image/x-icon" />
	
	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic|Abel' rel='stylesheet' type='text/css'>
	<!-- Font Awesome -->
    <link href="<?=base_url('tpl/css/bootstrap.min.css');?>" rel="stylesheet"> 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">

	<link href="<?=base_url('tpl/font-awesome/css/font-awesome.css');?>" rel="stylesheet">	
    
    
    <script src="<?=base_url('assets/js/jquery-2.1.4.min.js')?>" type="text/javascript"></script>
    <script src="<?=base_url('assets/js/jquery-ui.js');?>"></script>
	<script src="<?=base_url('tpl/js/bootstrap.min.js');?>"></script>
    <script src="<?=base_url('assets/js/bootbox.min.js');?>"></script> 
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />


	
		  <!-- barra para seleccionar -->
		<script src="<?=base_url('assets/js/jquery-ui-slider-pips.js');?>"></script> 
		<link href="<?=base_url('assets/css/jquery-ui-slider-pips.css');?>" rel="stylesheet">
        
          <!-- JS NODOS -->
		<script src="<?=base_url('assets/js/main.js');?>"></script> 
		<script src="<?=base_url('assets/js/jquery-latest.min.js');?>"></script> 
		<script src="<?=base_url('assets/js/respond.min.js');?>"></script> 
		 

	    <!-- Mainly scripts -->
        <script src="<?=base_url('tpl/js/plugins/metisMenu/jquery.metisMenu.js');?>"></script>
        <script src="<?=base_url('tpl/js/plugins/slimscroll/jquery.slimscroll.min.js');?>"></script>
      

		<link rel="stylesheet" href="<?=base_url('assets/datatables.net-bs/css/dataTables.bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')?>">


    
		<link href="<?=base_url('tpl/css/plugins/toastr/toastr.min.css');?>" rel="stylesheet">
        <script src="<?=base_url('tpl/js/plugins/toastr/toastr.min.js');?>"></script>
       
		<link href="<?=base_url('tpl/css/animate.css');?>" rel="stylesheet">
		<!-- todo el estilo de la plantilla -->
        <link href="<?=base_url('tpl/css/style.css');?>" rel="stylesheet">	
		
		<!-- nodos css --> 
		<link href="<?=base_url('assets/css/jquery-ui.min.css');?>" rel="stylesheet">
		<link href="<?=base_url('assets/css/jquery-ui.css');?>" rel="stylesheet">
        <script src="<?=base_url('assets/js/jquery-ui.js');?>"></script>

		<link href="<?=base_url('assets/css/general_tutores.css');?>" rel="stylesheet">
		<!--Stilos Nodos estilos eze -->
		<style type="text/css" media="screen">
            .datepicker {
                z-index:2051 !important;
            }   
        </style>
        
      
        
        <link href="<?=base_url('assets/css/fullcalendar/fullcalendar.print.css');?>" rel="stylesheet"  media='print'>
        <script src="<?=base_url('assets/js/fullcalendar/moment.min.js');?>"></script>
        <script src="<?=base_url('assets/js/scrollspy/scrollspy.js');?>"></script>

        
        <!--<script type="text/javascript" src="<?//=base_url('tpl/js/plugins/ckeditor/ckeditor.js')?>"></script>-->
        <link rel="stylesheet" href="<?php echo base_url('tpl/js/plugins/fullcalendar/fullcalendar.min.css')?>" media="screen">
        <script src="<?php echo base_url('tpl/js/plugins/fullcalendar/fullcalendar.min.js')?>"></script>
        <script src="<?php echo base_url('tpl/js/plugins/fullcalendar/lang/es.js')?>"></script> 

          <!--<script src="<?php echo base_url('tpl/js/plugins/moment/moment-with-locales.min.js')?>"></script> -->


        <!--Notify-->
        <link href="<?=base_url('assets/css/bootstrap-notify.css');?>" rel="stylesheet">
        <script src="<?=base_url('assets/js/notify.js');?>"></script>

        
		<!-- Mainly scripts -->
        <script src="<?=base_url('tpl/js/plugins/metisMenu/jquery.metisMenu.js');?>"></script>
        <script src="<?=base_url('tpl/js/plugins/slimscroll/jquery.slimscroll.min.js');?>"></script>
       
    
        
         <!--SELECT2 v3 (v4 no funca)-->
        <script src="<?=base_url('tpl/js/plugins/select2/select2-3.5.4/select2.js');?>"></script>
        <link rel="stylesheet" href="<?=base_url('tpl/js/plugins/select2/select2-3.5.4/select2.css');?>" type="text/css" />
        <link href="<?=base_url('tpl/css/select2-bootstrap.css');?>" rel="stylesheet">


       
       <!-- DataTime Master Picker -->
        <script src="<?=base_url('tpl/js/plugins/datatimepicker/jquery.datetimepicker.full.js');?>"></script>
         <link href="<?=base_url('tpl/js/plugins/datatimepicker/jquery.datetimepicker.css');?>" rel="stylesheet">


        <!--slider pips-->
        <link href="<?=base_url('assets/css/jquery-ui-slider-pips.css');?>" rel="stylesheet">


        <script src="<?=base_url('tpl/js/jsb/segment.min.js');?>"></script>
        <script src="<?=base_url('tpl/js/jsb/ease.min.js');?>"></script>
    
    
        <script src="<?=base_url('assets/js/jquery.validate.min.js')?>"></script>
        <script src="<?=base_url('assets/js/push.min.js')?>"></script>


        <script type="text/javascript">
            jQuery(document).ready(function($) {
                
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "progressBar": true,
                    "preventDuplicates": false,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "showDuration": "400",
                    "hideDuration": "1000",
                    "timeOut": "7000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };        
                <?php 
                    $mensaje_exito = $this->session->flashdata("mensaje_exito");
                    if ((isset($mensaje_exito)) && (!empty($mensaje_exito)))
                    {
                ?>
                        toastr.success("<?php echo $mensaje_exito; ?>","¡Perfecto!");
                <?php 
                    } 
                ?>
                <?php 
                    $mensaje_error = $this->session->flashdata("mensaje_error");
                    if ((isset($mensaje_error)) && (!empty($mensaje_error)))
                    {
                ?>
                        toastr.error("<?php echo $mensaje_error; ?>","¡Atencion!");
                <?php 
                    } 
                ?>
            });
        </script>
		<?php //$this->load->view("include/particicles");
  //$this->load->view("include/navbar_tutores");?>
  <?php $this->load->helper('html');
            $this->load->helper('url');
            echo link_tag(base_url('assets/css/burguer.css'));
            //echo"holla";
            echo link_tag(base_url('assets/css/webapp.css'));
        ?>

</head>

<body>	
    <div id="wrapper">
        <?php //$this->load->view("include/particicles");
		 $this->load->view("include/vista_inc_menu_colegio"); 
		 $this->load->view("include/particicles");
		 ?>
         <div style="margin-top: 50px;">
        <?php
        if(!empty($contenido))
                            $this->load->view($contenido);
        
        /*if(!empty($contenido2))
                            $this->load->view($contenido2);*/
        ?>
    	</div>
		
	    <footer>
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 center">
	                     <?php 
	                        date_default_timezone_set('America/Argentina/San_Juan');?>
	                         
	                           <center><img src=<?=base_url("assets/images/i/icon-user.png")?> width="40" alt="NODOS"/><p>&copy; <? echo date("Y"); ?></p></center>
	        </div>
	    </footer>
    </div><!-- wrapper -->
    <!-- Mainly scripts -->
     <script src="<?=base_url('tpl/js/plugins/metisMenu/jquery.metisMenu.js');?>"></script>
     <script src="<?=base_url('tpl/js/plugins/slimscroll/jquery.slimscroll.min.js');?>"></script>
     
     <script src="<?=base_url('assets/js/jeditable/jquery.jeditable.js');?>"></script>
  
   
     <!-- Datatables -->
        <script src="<?=base_url('assets/datatables.net/js/jquery.dataTables.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-buttons/js/dataTables.buttons.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-buttons/js/buttons.flash.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-buttons/js/buttons.html5.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-buttons/js/buttons.print.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-keytable/js/dataTables.keyTable.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-responsive/js/dataTables.responsive.min.js')?>"></script>
        <script src="<?=base_url('assets/datatables.net-responsive-bs/js/responsive.bootstrap.js')?>"></script>
        <!--<script src="<?//=base_url('assets/datatables.net-scroller/js/dataTables.scroller.min.js')?>"></script>-->
        <script src="<?=base_url('assets/jszip/dist/jszip.min.js')?>"></script>
        <script src="<?=base_url('assets/pdfmake/build/pdfmake.min.js')?>"></script>
        <script src="<?=base_url('assets/pdfmake/build/vfs_fonts.js')?>"></script>
         
        
       
      <!-- Custom and plugin javascript -->
    <script src="<?=base_url('tpl/js/inspinia.js');?>"></script>  <!-- usa -->
	
    <script src="<?=base_url('tpl/js/plugins/pace/pace.min.js');?>"></script>

    <?php 
     if ((isset($carga_select_dt)) && ($carga_select_dt == true)) { ?>
   <!-- <link href="<?php echo base_url('tpl/css/plugins/dataTables/select.dataTables.min.css') ?>" rel="stylesheet"> 
    
    <script src="<?=base_url('tpl/js/plugins/dataTables/dataTables.select.min.js');?>"></script>-->
   
    <?php } ?>

    
   <script src="<?php echo base_url('tpl/js/bootstrap.min.js');?>"></script> <!-- lo puse aca tambien para que ande el wysihtml5x -->

<script src="<?=base_url('assets/ckeditor/ckeditor.js')?>"></script>


  
 <script>
     
        
    </script>
    <script>
   
  </script>
  <script>
   
  </script>
    <script src="<?php echo base_url('tpl/js/general.js');?>"></script>
    
    <!--Para mostrar los mensajes notify 
    <div class='notifications top-right' style="margin-top: 40px"> </div>-->
   

    <style>
        .notifyjs-corner{
            margin-top:53px;
        }
    </style>
    
  </body>
</html>