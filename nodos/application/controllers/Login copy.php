<?php 
 
class Login extends CI_Controller {
      
	function __construct(){
		parent::__construct();
			//$this->output->enable_profiler(TRUE);
	}
		
    public function index(){         
        //$data['action'] = base_url('auth/validar');
        //$this->load->view('admin/niveles',$data);	
        
        $this->load->view('include/header');
		//$this->load->view('include/navbar');
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		$data['urlApi'] = $urlApi;
		$data['passApiKey'] = $passApiKey;
		$this->load->view('admin/loginView',$data);	
		//$this->load->view('include/footer');		
		
		//redirect('/');
    }
    public function olvido_pass()
    {
    	$this->load->view('include/header');
		//$this->load->view('include/navbar');
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		$data['urlApi'] = $urlApi;
		$data['passApiKey'] = $passApiKey;
		$this->load->view('admin/olvido_pass',$data);
    }
    public function verificar_codigo($urlApi, $passApiKey, $code)  
    {  
    	$url = $urlApi."/usuario/verificar_codigo_recuperacion";
		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","code: $code"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('code' => $code),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
	
		//var_dump($response);
		//die();	
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
    public function reset_password($code = NULL)
    {
    	if (!$code)
		{
			show_404();
		}
		else
		{
			$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
			$passApiKey = $this->variables->get_apikey();
			
			$user= $this->verificar_codigo($urlApi,$passApiKey,$code);
			$user = json_decode($user);
			if($user)
			{
				//echo "true";
				//print_r($user);
				//llamar a la vista de cambiar clave
				$this->load->view('include/header');
				$data['urlApi'] = $urlApi;
				$data['passApiKey'] = $passApiKey;
				$data['users'] = $user;
				$data['code'] =$code;
				$this->load->view('admin/cambiar_pass',$data);

			}
			else echo $user;
		} 
    }


    /**
    * Obtiene los roles de la api y verifica si es institucion o no. 
    * Peticion controller web a controller api. uso de curl 
    * @params $idUser que es el id del usuario
	* return si es institucion muestra la vista de configuracion (pasos)
	* 
    */
    public function get_roles($urlApi, $passApiKey, $idUser)  
    {  
    	$url = $urlApi."/usuario/obtener_roles";
		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser") ,
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
	
		//var_dump("ROLES: ".$response);	
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}

    //-------------------------------------------------------
	/**
    * Obtiene de la APi el Nombre y apellido de un usuario. 
    * Peticion controller web a controller api. uso de curl 
    * @params $idUser que es el id del usuario
	* return cadena con apellido, nombre usser 
	* 
    */
	 public function get_nombre_usuario($urlApi, $passApiKey, $idUser)  
    {  
    	$url = $urlApi."/usuario/obtener_nombre_usuario";
		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser") ,
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
		
		//var_dump($response);
		//var_dump (json_decode($response)); //($response);
			//die();
		return $response; //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	//menues y opcioens habilitadas para el users x
	public function get_menu($urlApi, $passApiKey, $idUser, $id_menus)
	{
		$url = $urlApi."/usuario/obtener_menu";
		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('id_menus' => $id_menus,'key'=>$passApiKey),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);

		//echo"antes ";
		//print_r($response);
		curl_close($ch);
	
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien

	}
	public function get_reglas_comunicacion($urlApi, $passApiKey, $idUser, $rol, $idcolegio)
	{
		$url = $urlApi."/usuario/obtener_reglas_comunicacion";
		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('rol' => $rol,'id_colegio' =>$idcolegio),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		//echo"antes ";
		//print_r($response);
		curl_close($ch);
	
		//var_dump($response);	
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	//--------------------------------------------------------
	/*public function loginApi($urlApi, $passApiKey, $idUser)  
    {  
    	$url = $urlApi."/usuario/obtener_roles";
		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser") ,
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
	
		//var_dump($response);	
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}*/

    public function principal()  
    {   //echo "principal";
    	//$idUser = str_replace("***", "/", $idUser);  //quitar caracter de escape!!    	

    	if(isset($_POST['idusuario']) )
    	{
	    	$idUser = $_POST['idusuario']; 

	    	//echo $idUser;
	    	//$this->session->sess_destroy();
			//$this->session->sess_create();
			$this->session->set_userdata( array('logged_in' => true, 'idusuario' => $idUser) ); 

			$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
			$passApiKey = $this->variables->get_apikey();
			
			$response = $this->get_roles( $urlApi, $passApiKey, $idUser);				
			$response_usser = $this->get_nombre_usuario( $urlApi, $passApiKey, $idUser);
			//var_dump($response_usser);
			//die();
			if( (isset($response_usser->status)) && ($response_usser->status==false) && ($response_usser->message=='NO LOGUIN') ){
				
			}else{

				var_dump($response_usser);
				$response_usser = json_decode($response_usser);
				if($response_usser)
					foreach ($response_usser as $key_user) {
						$first_name = $key_user->first_name;
					    $second_name = $key_user->last_name;
					    $foto_users = $key_user->foto;
							
					}
			}
			$nombre = $first_name.", ".$second_name;
			$usser_name = ucwords(strtolower($nombre)); //convertir la primer letra en mayuscula.
			
			
			if( (isset($response->status)) && ($response->status==false) && ($response->message=='NO LOGUIN') )
			{	
				//echo 'NO LOGUIN';
				redirect(base_url('/index.php/Login'));
			}
			else
			{	
				$response = json_decode($response);
				//var_dump($response);	
				$rolAdmin='Institucion'; //valores fijos DB
				$pasosFin= 6;
				$confHorasSteep=4;
				$confMateriasSteep=5;

				$contInst = 0; $i=0;
				foreach ($response as $key)
				{
					if($key->nombregrupo==$rolAdmin)
					{
						$contInst++;

						$idUserGrupoInst = $key->id;

						$idColegioInst = $key->idcolegio;

						$data['idUserGrupoInst'] = $idUserGrupoInst;
						$data['idColegioInst'] = $idColegioInst;
					}

					//ELEGIR EL DE MAYOR JERARQUIA
					if($i==0)
					{
						$rolesNomb = $key->nombregrupo;
						$rolesId = $key->idgrupo;
						$rolesOrden = $key->orden;
					}	

					$i++;
					/*$rolesNomb[]=$key->nombregrupo;
					$rolesId[]=$key->idgrupo;*/
				}
				/*$rolesNomb = join(',',$rolesNomb); 
				$rolesId = join(',',$rolesId);*/

				
				$response_permisos = $this->get_reglas_comunicacion( $urlApi, $passApiKey, $idUser,$rolesId, $response[0]->idcolegio);

				$response_permisos = json_decode($response_permisos);

				$response_menu = $this->get_menu($urlApi, $passApiKey, $idUser,$response_permisos->id_menus_acciones);
				$response_menu = json_decode($response_menu);
				//var_dump($response_menu); die();
				//echo "envio: ".$rolesId." ".$response[0]->idcolegio." ";
				//echo "reglas : ";
				//var_dump($response_reglas_comunicacion);
				//echo "ids ".$response_reglas_comunicacion->id_menus_acciones;
				//print_r($response_reglas_comunicacion[0]->id_menus_acciones);

				//$this->load->view('include/header');
				$data['idusuario'] = $this->session->userdata('idusuario');
				$data['urlApi'] = $urlApi;
				$data['passApiKey'] = $passApiKey;
				/*$data['idcolegio'] = $response[0]->idcolegio;
				$data['idgrupo'] = $rolesId;
				$data['nombregrupo'] = $rolesNomb;  
				$data['ordengrupo'] = $rolesOrden;  
				$data['nombrecolegio'] = $response[0]->nombrecolegio;*/
				$data['nombreusuario'] = $usser_name;
				$data['id_menus_acciones'] = $response_permisos->id_menus_acciones;
				$data['menu'] = $response_menu;
				



				$this->traer_colegios($idUser);

				
				//$this->session->set_userdata( array('id_menus_acciones'=>$data['id_menus_acciones'],'menu'=>$data['menu'],'idcolegio' => $data['idcolegio'], 'idgrupo' => $data['idgrupo'], 'nombregrupo' => $data['nombregrupo'], 'ordengrupo' => $data['ordengrupo'], 'nombrecolegio' => $data['nombrecolegio'] ,'nombreusuario' => $data['nombreusuario'] ) ); 

				$this->session->set_userdata( array('foto'=>$foto_users,'id_menus_acciones'=>$data['id_menus_acciones'],'menu'=>$data['menu'],'nombreusuario' => $data['nombreusuario'] ) ); 
				//var_dump($this->session->userdata());
				//die();

				//var_dump($this->session->userdata('email'));
				//var_dump($this->session->userdata('identity'));
				//var_dump($this->session->userdata());
				//die();
				//echo $contInst;
				if($contInst==1)
				{						
					/*$this->session->set_userdata( array('idcolegio' => $data['idcolegio'], 'idgrupo' => $data['idgrupo'], 'nombregrupo' => $data['nombregrupo'], 'nombrecolegio' => $data['nombrecolegio']  ) ); */
					//echo "cont institucion: ".$key->configuracion;
					if($key->configuracion!=$pasosFin)
					{
						if($key->configuracion<$confHorasSteep) //<4
						{
							//var_dump($this->session->userdata);
							$data['contenido'] = 'admin/confNiveles';
							$data['data'] = $data;
							$this->load->view('include/template_config_colegio', $data);
							//$this->load->view('admin/confNiveles',$data);
						}
						else 
							if($key->configuracion==$confHorasSteep) //==4
							{
								$this->load->view('admin/confHoras',$data);
							}
						else 
							if($key->configuracion==$confMateriasSteep) //==4
							{
								$this->load->view('admin/confMaterias',$data);
							}
					}
					else{
						//$this->load->view('panel/panel',$data);
						/*redirect("calendario");
						$data['opcion_menu'] = 'home';
					    $this->load->view('panel/vista_principal',$data);*/
					    redirect('Login/home');
					} 
				}
				else{
					//$this->load->view('panel/panel',$data);// Version anterior Eze.
					/*if($this->session->userdata('idgrupo') == 5)
					{
						//echo "hola tutores";
						$data['contenido'] = 'panel/vista_principal_tutores';
						$this->load->view('include/template_tutores',$data);
						//$this->load->view('panel/vista_principal_tutores',$data);
					}
					else
					{
						/*redirect("calendario");
						$data['opcion_menu'] = 'home';
						$this->load->view('panel/vista_principal',$data);*/
						/*$data['contenido2'] = 'panel/funciones_calendario';
						$data['contenido'] = 'panel/vista_principal';
						$this->load->view('include/template_colegio',$data);
					}*/
					redirect('Login/home');
					
				} 
					
			}
		}

		else redirect("login"); 
		 	
    }		


    public function logout(){  
    	$newdata = array(
                'idusuario'  =>'',
                'idcolegio' => '',
                'logged_in' => FALSE,
                'idgrupo'  =>'',
                'nombregrupo' => '',
                'nombrecolegio' => '',
               );

     	$this->session->unset_userdata($newdata);
    	$this->session->sess_destroy(); 

    	redirect('login');
	}

	public function home(){
		if(!$this->session->userdata("logged_in")){
			//nombregrupoecho "pierde sesion"; die();
               redirect("login");
         }
        // redirect("calendario");
		//print_r($this->session->userdata());
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		
		$passApiKey = $this->variables->get_apikey();	
	  	$data['idusuario'] = $this->session->userdata('idusuario');
		$data['urlApi'] = $urlApi;
		$data['passApiKey'] = $passApiKey;

		//$data['idcolegio'] = $this->session->userdata('idcolegio');
		//$data['idgrupo'] = $this->session->userdata('idgrupo');
		//$data['nombregrupo'] = $this->session->userdata('nombregrupo');
		//$data['nombrecolegio'] = $this->session->userdata('nombrecolegio');
		$data['ordengrupo'] = $this->session->userdata('ordengrupo');
		$data['opcion_menu'] = 'home';
		//validar permiso
		$band = 0;
		$controlador = "Login";
		//print_r($this->session->userdata('menu'));
		foreach ($this->session->userdata('menu') as $menu) //validar permiso de controlador y metodo
                {
                	if(($menu->controlador == $controlador) && ($menu->metodo == 'home'))
                	{
                		$band = 1;// encontro el controlador y el metodo en la sesion, el user tiene permiso
                	}
                }
        
        if($band == 1)
        {
			//die();
			$tutor = true;
			$alumno = true;
			foreach ($this->session->userdata('colegios') as $colegio) 
			{
				//var_dump($colegio['rol']);
				if($colegio['rol'] != 'Tutor')
				{
					$tutor = false;
					//die();
				}
				if($colegio['rol'] != 'Alumno')
				{
					$alumno = false;
					//die();
				}

			}
				# code...
			
			if($tutor == true)
			{
				//echo "hola tutores";
				$data['contenido2'] = 'panel/funciones_calendario';
				$data['contenido'] = 'panel/vista_principal_tutores';
				$this->load->view('include/template_tutores',$data);
				//$this->load->view('panel/vista_principal_tutores',$data);
			}
			else if($alumno == true)
			{
				
							//echo "hola tutores";
							$data['contenido2'] = 'panel/funciones_calendario';
							$data['contenido'] = 'panel/vista_principal_tutores';
							$this->load->view('include/template_tutores',$data);
							//$this->load->view('panel/vista_principal_tutores',$data);
				
			}

			/*if(($this->session->userdata('nombregrupo') == 'Tutor') ||($this->session->userdata('nombregrupo') == 'Alumno'))
						{
							//echo "hola tutores";
							$data['contenido2'] = 'panel/funciones_calendario';
							$data['contenido'] = 'panel/vista_principal_tutores';
							$this->load->view('include/template_tutores',$data);
							//$this->load->view('panel/vista_principal_tutores',$data);
						}*/
			else {
				$data['contenido2'] = 'panel/funciones_calendario';
				$data['contenido'] = 'panel/vista_principal';
				$this->load->view('include/template_colegio',$data);
				/*$this->load->view("panel/funciones_calendario",$data); 
				$this->load->view('panel/vista_principal',$data);*/
			}
		}
		else redirect("login");
		

	}
	//---------------------------------------------------------------------
	//-------- Cambio de Contraseña y de Pin ------------------------------
	//---------------------------------------------------------------------
	
	public function actualizar_contrasenia(){
		if(isset($_POST['idusuario']) )
		{
			$idUser = $_POST['idusuario']; 
			$this->session->set_userdata( array('logged_in' => true, 'idusuario' => $idUser) ); 
			$this->load->view('include/header');
			$urlApi = $this->variables->get_urlapi(); 
			$passApiKey = $this->variables->get_apikey();
			$data['urlApi'] = $urlApi;
			$data['passApiKey'] = $passApiKey;
		    $data['idusuario'] = $this->session->userdata('idusuario');
			$this->load->view('admin/loginRepass',$data);
		}
		else echo 'NO LLEGA';	
	}

	//multicolegio
	public function get_menu_general($urlApi, $passApiKey, $idUser)
	{
		$url = $urlApi."/usuario/obtener_menu_general";
		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('key'=>$passApiKey),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);

		//echo"antes ";
		
		curl_close($ch);
		//print_r($response);
		//die();
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien

	}
	public function get_colegios($urlApi, $passApiKey, $idUser)  
	{  
    	$url = $urlApi."/usuario/obtener_colegios";
		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser") ,
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
	
		//var_dump($response);	die();
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	public function get_roles_x_colegio($urlApi, $passApiKey, $idUser, $id_colegio)  
	{  
    	$url = $urlApi."/usuario/get_roles_x_colegio";
		$ch = curl_init();
		/*$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser","idcolegio: $id_colegio"),
			CURLOPT_SSL_VERIFYPEER=> false,
			);*/
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url,
			CURLOPT_HTTPHEADER => array("APIKEY: $passApiKey","userid: $idUser"),
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => array('idcolegio' => $id_colegio),
			CURLOPT_SSL_VERIFYPEER=> false,
			);
		curl_setopt_array( $ch, $options);
		$response = curl_exec($ch);
		curl_close($ch);
	
		/*var_dump($response);	
		die();*/
		return json_decode($response); //dos decode si o si. porque la api lo codifica y curl por defecto tambien
	}
	public function traer_colegios($user_id)
	{
		//echo $user_id; die();
		$urlApi = $this->variables->get_urlapi(); //cramos una libreria "variables" donde mantenemos y obtenemos estas variables globales
		$passApiKey = $this->variables->get_apikey();
		$colegios = $this->get_colegios( $urlApi, $passApiKey, $user_id);
		$colegios = json_decode($colegios);
		//echo 'traer colegios';
		//print_r($colegios);die();
		//$arre_colegios = array();
		$arre_colegios;
		$col =0;
		//$min = 100;
		foreach ($colegios as $col) 
		{
			//echo $col->nombre_colegio;
			$roles = $this->get_roles_x_colegio( $urlApi, $passApiKey, $user_id, $col->colegio_id);
			//echo $col->colegio_id;
			$roles = json_decode($roles);
			$min = 100; 
			foreach ($roles as $rol) 
			{
				if ($rol->orden_grupo < $min)
				{
					$aux_id_grupo = $rol->id_grupo;
					$aux_nomb_grupo= $rol->nombre_grupo;
					$min = $rol->orden_grupo;
				}
			}
			//$min = 100;
					$response_permisos = $this->get_reglas_comunicacion( $urlApi, $passApiKey, $user_id,$aux_id_grupo, $col->colegio_id);
					$response_permisos = json_decode($response_permisos);

					$response_menu = $this->get_menu($urlApi, $passApiKey, $user_id,$response_permisos->id_menus_acciones);
					$response_menu = json_decode($response_menu);
					
					$arre_colegios[] = array('id_colegio' => $col->colegio_id,
							'nombre_colegio' => $col->nombre_colegio,
							'id_grupo' => $aux_id_grupo,
							'rol' => $aux_nomb_grupo,
							'menu' => $response_menu
						);
					$aux_id_grupo = 'nada';
					$aux_nomb_grupo= 'nada';
		}

		//print_r($arre_colegios);
		foreach ($arre_colegios as $col) 
		{
			//echo ' /Institucion: '.$col['nombre_colegio'].' Rol: '.$col['rol'].' id_group: '.$col['id_grupo'];

			//print_r($col);
			foreach ($col['menu'] as $menu) 
			{

				if(($menu->nombre == 'editar_calendario'))
			        {
			          echo $band = 1; 
			        }
			}
		}
		$menu_general = $this->get_menu_general($urlApi, $passApiKey, $user_id);
		$menu_general = json_decode($menu_general);
		$this->session->set_userdata( array('colegios' => $arre_colegios, 'menu_general' => $menu_general ) ); 
		//print_r($menu_general);
		//die();
	}
}
?>
